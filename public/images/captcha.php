<?php
session_start();

$num1=rand(1,9); //Generate First number between 1 and 9  
$num2=rand(1,9); //Generate Second number between 1 and 9  
$captcha_total = $num1+$num2;
$captcha_code = "$num1"." + "."$num2"." = ?";

$_SESSION["captcha"] = $captcha_total;

$target_layer = imagecreatetruecolor(100,25);
$captcha_background = imagecolorallocate($target_layer, 253, 253, 253);
imagefill($target_layer,0,0,$captcha_background);
$captcha_text_color = imagecolorallocate($target_layer, 0, 0, 0);
imagestring($target_layer, 5, 10, 5, $captcha_code, $captcha_text_color);
header("Content-type: image/jpeg");
imagejpeg($target_layer,null,100);