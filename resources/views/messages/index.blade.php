
<style>
    .side-setting-nav li{
       display:inline-block;
    }
   
    
</style>

<div class="message-wrapper" style="position: relative;">
   <div class="row mb-3" style="background:#e2e2e2;width:100%;">
   
        <div class="col-md-10 col-10">
            <h3 class="p-3" style="font-weight:bolder;">JCERT GROUP</h3>
           </div>
           <div class="col-md-2 col-2" >
               

              <ul class="list-unstyled side-setting-nav mt-3 ml-2">
                  <li class="mr-2"><a class="text-dark" href="{{url('/group/edit')}}/{{$group->id}}" title="Edit User"><i class="fa fa-edit fa-2x"></i></a></li>
                  <li class="mr-2"><a href="{{url('/group/delete')}}/{{$group->id}}"  class="text-dark" onclick="return confirm('Are you sure want to delete group ?')" title="Delete User"><i class="far fa-trash-alt fa-2x"></i></a></li></li>
                  <li><a class="text-dark" href="{{url('/group/members_list')}}/{{$group->id}}" title="Remove Users"><i class="fas fa-user-slash fa-2x"></i></a></li>

              </ul>
           </div>
   
      
      
   </div>
    <ul class="messages">
        @foreach($messages as $message)
        @php
       $checkType= ($message->from == Auth::id()) ? 'sent' : 'received';
       @endphp
       @if($checkType=='sent')
        <div class="row">
            <div class="col-md-12">
            @if(!empty($message->image))
               @php
               $supported_image = array('gif','jpg','jpeg','png');
               $ext = strtolower(pathinfo($message->image, PATHINFO_EXTENSION));
               @endphp
               @if(in_array($ext, $supported_image))
                  <a href="{{asset('/groupchat/image')}}/{{$message->image}}" download>
                    <img src="{{asset('/groupchat/image')}}/{{$message->image}}"  width="104" height="142" style="float:right;margin-bottom:15px;">
                  </a>
               @elseif($ext=='pdf')
                     <a style="float:right;margin-bottom:15px;" href="{{asset('/groupchat/image')}}/{{$message->image}}"  download="{{asset('/groupchat/image')}}/{{$message->image}}">  <img src="{{asset('/groupchat/image/1.png')}}"  width="104" height="142"></a>
                @else
                <a style="float:right;margin-bottom:15px;" href="{{asset('/groupchat/image')}}/{{$message->image}}"  download="{{asset('/groupchat/image')}}/{{$message->image}}">  <img src="{{asset('/groupchat/image/2.png')}}"  width="104" height="142"></a> 
               @endif
            @endif

            </div>
        </div>
       @else
        <div class="row">
            <div class="col-md-12">
                @if(!empty($message->image))
                @php
                $supported_image = array('gif','jpg','jpeg','png');
                $ext = strtolower(pathinfo($message->image, PATHINFO_EXTENSION));
                @endphp
                @if(in_array($ext, $supported_image))
                   <a href="{{asset('/groupchat/image')}}/{{$message->image}}" download>
                     <img src="{{asset('/groupchat/image')}}/{{$message->image}}"  width="104" height="142" style="float:right;margin-bottom:15px;">
                   </a>
                @elseif($ext=='pdf')
                      <a style="float:right;margin-bottom:15px;" href="{{asset('/groupchat/image')}}/{{$message->image}}"  download="{{asset('/groupchat/image')}}/{{$message->image}}">  <img src="{{asset('/groupchat/image/1.png')}}"  width="104" height="142"></a>
                 @else
                    <a style="float:right;margin-bottom:15px;" href="{{asset('/groupchat/image')}}/{{$message->image}}"  download="{{asset('/groupchat/image')}}/{{$message->image}}">  <img src="{{asset('/groupchat/image/2.png')}}"  width="104" height="142"></a> 
                @endif
                @endif

            </div>
        </div>
       @endif
       
        <li class="message clearfix mesgbox" style="position: relative;">

            {{--if message from id is equal to auth id then it is sent by logged in user --}}
            <div class="{{ ($message->from == Auth::id()) ? 'sent' : 'received' }}" >
                {{-- <h4 > <a href="/ShowMassage/{{$message->id}}" style="color: black;"> {{$group->name}} </a> </h4> --}}
                    <p style="letter-spacing: 1px;" style="position:"><i class="fa fa-user"></i>&nbsp;&nbsp;{{ $message->name }}</p>
                    <p><i class="fa fa-envelope"></i>&nbsp;&nbsp;{{ $message->message }}</p>
                    <p class="date" style="letter-spacing: 1px;">{{ date('d M y, h:i a', strtotime($message->created_at)) }}</p>
            </div>
        </li>
      
        @endforeach
    </ul>
</div>

<style>
    
</style>
<!-- this is for add new message -->
<form action="javascript:void(0)" id="frm">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="row">
    <!-- @php
    $user=\DB::table('users')->where('user_role','user')->first();
    @endphp
    @if($user->chat_status==1)
     -->
    <div class="col-md-9">
        <input type="text" name="message" id='message' class="submit">
        <input type="hidden" name="id" id='id' class="submit" value='{{$group->id }}'>
    </div>
    @endif
 
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-2">
                <i class="fas fa-arrow-circle-right fa-3x mt-3" style="display: inline-block;" title="Send Message"> </i>
            </div>
            <div class="col-md-3">
                <input class="mt-4 file-input" type="file" name="image" id="image">
            </div>
        </div>
      
    </div>

</div>
</form>

</div>

@if ($group->admin_id == auth()->user()->id)
<div class="row">
    <div class="col-md-2">
        <p>
            <a class="btn btn-info mt-2" href="{{url('/group/edit')}}/{{$group->id}}" style="color:white;">Edit</a>
        </p>
    </div>
    <div class="col-md-2">
        <form action="{{url('/group/delete')}}/{{$group->id}}" method="POST">
            @csrf
            @method('Delete')
            <button type="submit" class="btn btn-danger mt-2" onclick="return confirm('Are you sure want to delete group ?')">Delete group</button>
        </form>
    </div>
    <div class="col-md-2">
        <p>
            <a class="btn btn-warning mt-2" href="{{url('/group/members_list')}}/{{$group->id}}" style="color:white;">Remove users</a>
        </p>
    </div>
</div>
@else
<!-- this is for unFollow -->
<form action="{{url('/unFollow')}}/{{ $group->id }}" method="POST">
    @csrf
    @method('Delete')
    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to unFollow ?')">unFollow</button>
</form>
@endif