@include('header')
@include('sidenav')
@include('topbar')
<style>
    .btnnext {
        margin: 5px 6px;
    }

    .btnPrevious {
        float: right !important;
        margin: 5px 6px;
    }

    .btnsavesubmit {
        border-radius: 30px;
    }

    @media (max-width:768px) {
        .btnsavesubmit {
            margin-left: 0 !important;
        }

        .prevtab {
            position: relative;
            top: 54px;
        }
    }
</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Schools</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">School Application</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 text-right pr-4">
                        <!-- <button type="button" class="btn btn-sm btn-light btn-offset"><a class="text-success" title="Export "> <i class="fa fa-file-excel text-success"> </i> </a></button>
                        <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button> -->
                    </div>
                </div>
            </div>
            
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <form id="tabs" name="application_form" action="{{url('addEdit/applicationData')}}" onsubmit="return validation()" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="card">
                        @csrf
                        <nav class="navbar navbar-expand-lg" style="padding:2px;">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" aria-controls="collapsibleNavbar" style="outline:none;">
                                <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
                            </button>
                            <div class="collapse navbar-collapse pull-right" id="collapsibleNavbar">
                                <ul class="nav navbar-nav nav-tabs " id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active ui-tabs-panel first_tab" id="school-tab" data-toggle="tab" href="#school" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>School Details</h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link ui-tabs-panel ui-tabs-hide middle_tab" id="General-Information-tab" data-toggle="tab" href="#General-Information" role="tab" aria-controls="General-Information" aria-selected="false">
                                            <h5>General Information</h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide middle_tab" id="School-Area-and-Format-Details-tab" data-toggle="tab" href="#School-Area-and-Format-Details" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>School Area and Format Details </h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide  middle_tab" id="Financial-Details-tab" data-toggle="tab" href="#Financial-Details" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>Financial Details </h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide middle_tab " id="Enrollment-tab" data-toggle="tab" href="#Enrollment" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>Enrollment </h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide middle_tab " id="Infrastructure-tab" data-toggle="tab" href="#Infrastructure" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>Infrastructure Details</h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide middle_tab " id="Other-Convenience-tab" data-toggle="tab" href="#Other-Convenience" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>Other Convenience Details</h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide middle_tab " id="Teaching-Staff-Specialties-tab" data-toggle="tab" href="#Teaching-Staff-Specialties" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>Teaching Staff Specialties</h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class=" nav-link ui-tabs-panel ui-tabs-hide  last_tab" id="Curriculum-and-Syllabus-tab" data-toggle="tab" href="#Curriculum-and-Syllabus" role="tab" aria-controls="school" aria-selected="true">
                                            <h5>Curriculum and Syllabus </h5>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane ui-tabs-panel   fade show active first" id="school" role="tabpanel" aria-labelledby="school-tab">
                                <div class="card-body">
                                    <!-- <div class="container"> -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                <input type="hidden" name="id" value="{{@$user->id}}">
                                                <input type="hidden" name="application_number" value="{{@$user->apllication_id}}">
                                                <label for="">School Name </label>
                                                <input type="text" maxlength="48" value="{{@$user->school_name}}" name="school_name" class="form-control rte_input">
                                                <span class="msg" id="school_name_id"></span>
                                                @if ($errors->has('school_name'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_name') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="">Academic Session From Which Recognition Proposed</label>
                                                <input type="text" value="{{@$user->recognised_by}}" name="recognised_by" maxlength="38" class="form-control rte_input">
                                                <span class="msg" id="recognised_by_id"></span>
                                                @if ($errors->has('recognised_by'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('recognised_by') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Post Office </label>
                                                <input type="text" value="{{@$user->post_office}}" name="post_office" maxlength="18" class="form-control rte_input">
                                                <span class="msg" id="post_office_id"></span>
                                                @if ($errors->has('post_office'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('post_office') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Village / Town </label>
                                                <input type="text" value="{{@$user->village_city}}" name="village_town" maxlength="18" class="form-control rte_input">
                                                <span class="msg" id="village_town_id"></span>
                                                @if ($errors->has('village_town'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('village_town') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Pin Code</label>
                                                <input type="text" value="{{@$user->pincode}}" pattern="\d*" maxlength="6" name="pin_code" class="form-control rte_input">
                                                <span class="msg" id="pin_code_id"></span>
                                                @if ($errors->has('pin_code'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('pin_code') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Ph./Mb no. With STD Code </label>
                                                <input type="text" value="{{@$user->phone_with_std_code}}" name="phone_with_std_code" maxlength="32" class="form-control rte_input">
                                                <span class="msg" id="phone_with_std_code_id"></span>
                                                @if ($errors->has('phone_with_std_code'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('phone_with_std_code') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">FAX No. With STD Code </label>
                                                <input type="text" value="{{@$user->fax_with_std_code}}" name="fax_with_std_code" maxlength="32" class="form-control rte_input">
                                                <span class="msg" id="fax_with_std_code_id"></span>
                                                @if ($errors->has('fax_with_std_code'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('fax_with_std_code') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Email ID</label>
                                                <input type="email" value="{{@$user->email}}" name="email" maxlength="48" class="form-control rte_input">
                                                <span class="msg" id="email_id"></span>
                                                @if ($errors->has('email'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Nearest Police Station</label>
                                                <input type="text" value="{{@$user->police_station}}" maxlength="20" name="police_station" class="form-control rte_input">
                                                <span class="msg" id="police_station_id"></span>
                                                @if ($errors->has('police_station'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('police_station') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="General-Information" role="tabpanel" aria-labelledby="General-Information-tab">
                                <div class="card-body">
                                    <!-- <div class="container"> -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Establishment Year </label>
                                                <select class="form-control" name="estd_year" id="" style="display:block;">
                                                    @isset($user->estd_year)
                                                    <option value="{{@$user->estd_year}}">{{@$user->estd_year}}</option>
                                                    @else
                                                    <option value="">-- Select ---</option>
                                                    @endisset
                                                    <?php
                                                        $currently_selected = date('Y'); 
                                                        $earliest_year = 1980; 
                                                        $latest_year = date('Y'); 
                                                      
                                                        foreach ( range( $latest_year, $earliest_year ) as $i ) { ?>
                                                    <option value="{{$i}}">{{$i}}</option>
                                                    <?php }
                                                        ?>
                                                </select>
                                                <span class="msg" id="estd_year_id"></span>
                                                @if ($errors->has('estd_year'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('estd_year') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">School Opening Date </label>
                                                <input type="date" value="{{@$user->opening_date}}" name="opening_date" class="form-control rte_input">
                                                <span class="msg" id="opening_date_id"></span>
                                                @if ($errors->has('opening_date'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('opening_date') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">trust/Society/Management Committee Name </label>
                                                <input type="text" value="{{@$user->society_name}}" maxlength="48" name="society_name" class="form-control rte_input">
                                                <span class="msg" id="society_name_id"></span>
                                                @if ($errors->has('society_name'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('society_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">is trust/society/management comittee registered?</label>
                                                <div>
                                                    <input type="radio" value="Yes" name="is_society_registered" class="" {{ @$user->is_society_registered == 'Yes' ? 'checked' : ''}}> Yes
                                                    <input type="radio" value="No" name="is_society_registered" class="" style="margin-left:20px;" {{ @$user->is_society_registered == 'No' ? 'checked' : ''}}> No
                                                    <span class="msg" id="is_society_registered_id"></span>
                                                    @if ($errors->has('is_society_registered'))
                                                    <span class="text-danger">
                                                        <strong>{{ $errors->first('is_society_registered') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="">The period until the registration of the Trust / Society / Management Committee is valid</label>
                                                <!-- <label for="">The period until the registration  valid</label> -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select type="date" name="school_validation_till" class="form-control school_validation_till rte_input">
                                                            @isset($user->school_validation_till)
                                                            @if($user->school_validation_till==1)
                                                            <option selected value="{{@$user->school_validation_till}}">Life Time </option>
                                                            <option value="2">Defined Date </option>
                                                            @endif
                                                            @if($user->school_validation_till==2)
                                                            <option value="1">Life Time </option>
                                                            <option selected value="{{@$user->school_validation_till}}">Defined Date </option>
                                                            @endif
                                                            @else
                                                            <option value="">--Select--</option>
                                                            <option value="1">Life Time </option>
                                                            <option value="2">Defined Date </option>
                                                            @endisset
                                                        </select>
                                                        <span class="msg" id="school_validation_till_id"></span>
                                                        @if ($errors->has('school_validation_till'))
                                                        <span class="text-danger">
                                                            <strong>{{ $errors->first('school_validation_till') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6 hideValid">
                                                        <input type="date" name="society_registration_valid_upto" value="{{@$user->society_registration_valid_upto}}" class="form-control rte_input">
                                                        <span class="msg" id="society_registration_valid_upto_id"></span>
                                                        @if ($errors->has('society_registration_valid_upto'))
                                                        <span class="text-danger">
                                                            <strong>{{ $errors->first('society_registration_valid_upto') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of members including their addresses on affidavit ?</label>
                                                <div>
                                                    <input type="radio" class="" value="Yes" name="evidence_of_non_proprietary_nature" {{ @$user->evidence_of_non_proprietary_nature1 == 'Yes' ? 'checked' : ''}} "> Yes
                                                    <input type="radio" class="" value="Yes" name="evidence_of_non_proprietary_nature" {{ @$user->evidence_of_non_proprietary_nature1 == 'No' ? 'checked' : ''}} style="margin-left:20px;"> No
                                                    <span class="msg" id="evidence_of_non_proprietary_nature_id"></span>

                                                    <br>
                                                    <label for="">Add relevant evidence attachement </label>
                                                    @if($user->evidence_of_non_proprietary_nature_ex == null || $user->evidence_of_non_proprietary_nature_ex == '')
                                                    <input type="file" id="evidence_of_non_proprietary_nature_files" name="evidence_of_non_proprietary_nature_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                                    <input type="hidden" class=" file_upload_check form-control rte_input" name="evidence_of_non_proprietary_nature_ex_check" id="evidence_of_non_proprietary_nature_ex_check" value="0" />
                                                    @else
                                                    <input type="hidden" class=" file_upload_check form-control rte_input" name="evidence_of_non_proprietary_nature_ex_check" id="evidence_of_non_proprietary_nature_ex_check" value="1" />
                                                    @if(count($user->evidence_of_non_proprietary_nature_ex) > 2)
                                                    <div style="width: 550px;height: 70px; overflow: scroll;">
                                                        @endif
                                                        @foreach($user->evidence_of_non_proprietary_nature_ex as $key => $val)
                                                        <div>
                                                            <form style="display: inline;">
                                                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                            </form>
                                                            @if($key==0)
                                                            <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                            @endif
                                                            <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                            <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                                <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                                <input type="hidden" class='text-block field_name' name="field_name" value="evidence_of_non_proprietary_nature">
                                                                <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                            </form>
                                                        </div>
                                                        @endforeach
                                                        @if(count($user->evidence_of_non_proprietary_nature_ex) > 2)
                                                    </div>
                                                    @endif
                                                    @endif
                                                    <span class="msg" id="evidence_of_non_proprietary_nature_ex_check_id"></span>
                                                    @if ($errors->has('evidence_of_non_proprietary_nature_files[]'))
                                                    <span class="text-danger">
                                                        <strong>{{ $errors->first('evidence_of_non_proprietary_nature_files[]') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <h5 style="margin:20px 0;">School Chairman Information</h5>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Chairman Name </label>
                                                <input type="text" value="{{@$user->school_chairman_name}}" maxlength="24" name="school_chairman_name" class="form-control rte_input">
                                                <span class="msg" id="school_chairman_name_id"></span>
                                                @if ($errors->has('school_chairman_name'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_chairman_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for=""> Designation</label>
                                                <input type="text" value="{{@$user->school_chairman_post}}" name="school_chairman_post" maxlength="19" class="form-control rte_input">
                                                <span class="msg" id="school_chairman_post_id"></span>
                                                @if ($errors->has('school_chairman_post'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_chairman_post') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Address</label>
                                                <input type="text" value="{{@$user->school_chairman_address}}" name="school_chairman_address" maxlength="78" class="form-control rte_input">
                                                <span class="msg" id="school_chairman_address_id"></span>
                                                @if ($errors->has('school_chairman_address'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_chairman_address') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Phone No.</label>
                                                <input type="number" value="{{@$user->school_chairman_phone_number}}" name="school_chairman_phone_number" class="form-control rte_input">
                                                <span class="msg" id="school_chairman_phone_number_id"></span>
                                                @if ($errors->has('school_chairman_phone_number'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_chairman_phone_number') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Office</label>
                                                <input type="text" value="{{@$user->school_chairman_office}}" maxlength="15" name="school_chairman_office" class="form-control rte_input">
                                                <span class="msg" id="school_chairman_office_id"></span>
                                                @if ($errors->has('school_chairman_office'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_chairman_office') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Email ID</label>
                                                <input type="email" value="{{@$user->school_chairman_email}}" maxlength="30" name="school_chairman_email" class="form-control rte_input">
                                                <span class="msg" id="school_chairman_email_id"></span>
                                                @if ($errors->has('school_chairman_email'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('school_chairman_email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h5 style="margin:20px 0;">Last 3 Years Total Income/Expenses/Surplus/Loss</h5>
                                            <hr>
                                        </div>
                                        <?php $three_year_to = date('Y');$three_year_from = $three_year_to - 3; ?>
                                        @for ($i = 0,$y=1; $i < 3; $i++,$y++) <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Year</label>
                                                @isset($user->session_year)
                                                <input readonly type="text" name="session_year[]" id="session_year_<?php echo $i+1 ?>" class="form-control" value="{{@$user->session_year[$i]}}" />
                                                @else
                                                <input readonly type="text" name="session_year[]" id="session_year_<?php echo $i+1 ?>" class="form-control" value="<?php echo $three_year_from+$i ?>-<?php echo $three_year_from+$y?>" />
                                                @endisset
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Income </label>
                                            <input type="number" value="{{@$user->income[$i]}}" id="income_<?php echo $i+1 ?>" name="income[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Expenses</label>
                                            <input type="number" value="{{@$user->expenses[$i]}}" id="expenses_<?php echo $i+1 ?>" name="expenses[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Surplus Money</label>
                                            <input readonly type="number" value="{{@$user->surplus_money[$i]}}" id="surplus_money_<?php echo $i+1 ?>" name="surplus_money[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Reduce</label>
                                            <input readonly type="number" value="{{@$user->reduced_money[$i]}}" id="reduced_money_<?php echo $i+1 ?>" name="reduced_money[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Add relevant evidence attachement</label>
                                            @php
                                            $Y = $i+1;
                                            $File_name = 'last_three_year_tot_income_files_'.$Y;
                                            $File_name_validation = 'last_three_year_tot_income_files_'.$Y.'[]';
                                            @endphp
                                            @if($user->$File_name == null || $user->$File_name == '')
                                            <input type="file" id="last_three_year_tot_income_files_<?php echo $i+1 ?>" name="last_three_year_tot_income_files_<?php echo $i+1 ?>[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="last_three_year_tot_income_files_check_<?php echo $i+1 ?>" id="last_three_year_tot_income_files_check_<?php echo $i+1 ?>" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="last_three_year_tot_income_files_check_<?php echo $i+1 ?>" id="last_three_year_tot_income_files_check_<?php echo $i+1 ?>" value="1" />
                                            @if(count($user->$File_name) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->$File_name as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="last_three_year_tot_income_files_<?php echo $i+1 ?>">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->$File_name) > 2)
                                            </div>
                                            @endif

                                            @endif
                                            <div class="col-12"><span style="display:inline" class="msg" id="last_three_year_tot_income_files_check_<?php echo $Y ?>_id"></span><br></div>
                                        </div>
                                        @if($errors->has($File_name_validation))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first($File_name_validation) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    @endfor
                                    <div class="col-12"><span style="display:inline" class="msg income_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg expenses_id"></span><br></div>

                                    @if ($errors->has('income.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('income.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('expenses.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('expenses.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('surplus_money.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('surplus_money.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('reduced_money.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('reduced_money.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif

                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="School-Area-and-Format-Details" role="tabpanel" aria-labelledby="School-Area-and-Format-Details-tab">
                            <div class="card-body">
                                <!-- <div class="container"> -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Medium Of Education </label>
                                            <input type="text" value="{{@$user->medium}}" maxlength="50" name="medium" class="form-control rte_input">
                                            <span class="msg" id="medium_id"></span>
                                            @if ($errors->has('medium'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('medium') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="">Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </label>
                                            <select name="type_of_school" id="" class="form-control" style="display:block;">
                                                @isset($user->type_of_school)
                                                <option value="{{@$user->type_of_school}}">{{@$user->type_of_school}}</option>
                                                @else
                                                <option value="">--Select--</option>
                                                @endisset
                                                <option value="1-5">1-5</option>
                                                <option value="1-8">1-8</option>
                                            </select>
                                            <span class="msg" id="type_of_school_id"></span>
                                            @if ($errors->has('type_of_school'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('type_of_school') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Agency Name (if school has support) </label>
                                            <input type="text" value="{{@$user->supported_agency_name}}" maxlength="38" name="supported_agency_name" class="form-control rte_input">
                                            <span class="msg" id="supported_agency_name_id"></span>
                                            @if ($errors->has('supported_agency_name'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('supported_agency_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Support Percentage</label>
                                            <input type="number" value="{{@$user->agency_supported_percent}}" maxlength="4" name="agency_supported_percent" class="form-control rte_input">
                                            <span class="msg" id="agency_supported_percent_id"></span>
                                            @if ($errors->has('agency_supported_percent'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('agency_supported_percent') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="">If School Is Recognised Mention Authority Name</label>
                                            <input type="text" value="{{@$user->authority_name}}" maxlength="30" name="authority_name" class="form-control rte_input">
                                            <span class="msg" id="authority_name_id"></span>
                                            @if ($errors->has('authority_name'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('authority_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Recognition Number</label>
                                            <input type="number" value="{{@$user->recognised_number}}" name="recognised_number" class="form-control rte_input">
                                            <span class="msg" id="recognised_number_id"></span>
                                            @if ($errors->has('recognised_number'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('recognised_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">The School Has Its Own Building Or Is Working In A Rented Building ?</label>
                                            <div>
                                                <input type="radio" {{ @$user->is_school_on_rented == 'Own' ? 'checked' : ''}} value="Own" name="is_school_on_rented" class=""> Own
                                                <input type="radio" {{ @$user->is_school_on_rented == 'Rented' ? 'checked' : ''}} value="Rented" name="is_school_on_rented" class="" style="margin-left:20px;"> Rented
                                                <span class="msg" id="is_school_on_rented_id"></span>
                                                @if ($errors->has('is_school_on_rented'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('is_school_on_rented') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Add Relevant evidence attachement</label>
                                            @if($user->school_rent_deatail == null || $user->school_rent_deatail == '')
                                            <input type="file" id="school_rent_deatail" name="school_rent_deatail[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_rent_deatail_check" id="school_rent_deatail_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_rent_deatail_check" id="school_rent_deatail_check" value="1" />
                                            @if(count($user->school_rent_deatail) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_rent_deatail as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_rent_deatail">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_rent_deatail) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_rent_deatail_check_id"></span>
                                            @if ($errors->has('school_rent_deatail[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_rent_deatail[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development?</label>
                                            <div>
                                                <input type="radio" {{ @$user->are_school_building_used == 'Yes' ? 'checked' : ''}} value="Yes" class="" name="are_school_building_used"> Yes
                                                <input type="radio" {{ @$user->are_school_building_used == 'No' ? 'checked' : ''}} value="No" class="" name="are_school_building_used" style="margin-left:20px;"> No
                                                <span class="msg" id="are_school_building_used_id"></span>
                                                @if ($errors->has('are_school_building_used'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('are_school_building_used') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Total Area Of School ( Rakba In Acers)</label>
                                            <input type="text" value="{{@$user->school_total_area}}" name="school_total_area" class="form-control rte_input">
                                            <span class="msg" id="school_total_area_id"></span>
                                            @if ($errors->has('school_total_area'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_total_area') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Plot Number</label>
                                            <input type="text" value="{{@$user->plot_number}}" name="plot_number" class="form-control rte_input">
                                            <span class="msg" id="plot_number_id"></span>
                                            @if ($errors->has('plot_number'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('plot_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Khata Number</label>
                                            <input type="text" value="{{@$user->khata_number}}" name="khata_number" class="form-control rte_input">
                                            <span class="msg" id="khata_number_id"></span>
                                            @if ($errors->has('khata_number'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('khata_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Area Of School Building Only</label>
                                            <input type="text" value="{{@$user->school_building_area}}" name="school_building_area" class="form-control rte_input">
                                            <span class="msg" id="school_building_area_id"></span>
                                            @if ($errors->has('school_building_area'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_building_area') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Add Relevant Evidence Attachment
                                            </label>
                                            @if($user->school_area_deatail == null || $user->school_area_deatail == '')
                                            <input type="file" id="school_area_deatail" name="school_area_deatail[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_area_deatail_check" id="school_area_deatail_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_area_deatail_check" id="school_area_deatail_check" value="1" />
                                            @if(count($user->school_area_deatail) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_area_deatail as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_area_deatail">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_area_deatail) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_area_deatail_check_id"></span>
                                            @if ($errors->has('school_area_deatail[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_area_deatail[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Document for Fixed deposit of one lakh rupees in the name of school </label>
                                            @if($user->one_lac_fd_proof == null || $user->one_lac_fd_proof == '')
                                            <input type="file" id="one_lac_fd_proof" name="one_lac_fd_proof[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="one_lac_fd_proof_check" id="one_lac_fd_proof_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="one_lac_fd_proof_check" id="one_lac_fd_proof_check" value="1" />
                                            @if(count($user->one_lac_fd_proof) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->one_lac_fd_proof as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="one_lac_fd_proof">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->one_lac_fd_proof) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="one_lac_fd_proof_check_id"></span>
                                            @if ($errors->has('one_lac_fd_proof[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('one_lac_fd_proof[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="Financial-Details" role="tabpanel" aria-labelledby="Financial-Details-tab">
                            <div class="card-body">
                                <!-- <div class="container"> -->
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Select class from</label>
                                        <select name="from_class_finance_id" class="form-control" id="from_finance_cls">
                                            <option value="">From Class</option>
                                            @if(isset($user->allclasses))
                                            @foreach($user->allclasses as $data)
                                            <option value="{{$data->class_id}}" @if($data->class_id == @$user->from_class_finance) selected @endif>{{$data->class_name}}</option>
                                            @endforeach
                                            @else
                                            @foreach($allclasses as $data)
                                            <option value="{{$data->class_id}}" @if($data->class_id == @$user->from_class_finance) selected @endif>{{$data->class_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="msg" id="from_class_finance_id_id"></span>
                                        @if ($errors->has('from_class_finance_id'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('from_class_finance_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">Select class to</label>
                                        <select name="to_class_finance_id" class="form-control" id="to_finance_cls">
                                            <option value="">To Class</option>
                                            @if(isset($user->allclasses))
                                            @foreach($user->allclasses as $data)
                                            <option value="{{$data->class_id}}" @if($data->class_id == @$user->to_class_finance) selected @endif>{{$data->class_name}}</option>
                                            @endforeach
                                            @else
                                            @foreach($allclasses as $data)
                                            <option value="{{$data->class_id}}" @if($data->class_id == @$user->to_class_finance) selected @endif>{{$data->class_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="msg" id="to_class_finance_id_id"></span>
                                        @if ($errors->has('to_class_finance_id'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('to_class_finance_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="pl-4 pt-4 col-md-2">
                                        <input style="float: right;" type="button" class="btn btn-primary" onclick="clsFeeSearch()" value="Submit" />
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="add_class_fee_row">
                                        @if(isset($user->teaching_fee))
                                        @foreach($user->teaching_fee as $key => $val)
                                        <div class="row ">
                                            <lable><b>{{@$user->class_finance[$key]}}</b></lable>
                                            <input type="hidden" maxlength="48" value="{{@$user->class_finance[$key]}}" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Tuition Fee </label>
                                                    <input type="text" maxlength="48" value="{{@$user->teaching_fee[$key]}}" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Registration Fee </label>
                                                    <input type="text" maxlength="48" value="{{@$user->registration_fee[$key]}}" id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Admission Fee </label>
                                                    <input type="text" maxlength="48" value="{{@$user->enrollment_fee[$key]}}" id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Reserve Deposite </label>
                                                    <input type="text" maxlength="48" value="{{@$user->security_money[$key]}}" id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Development Fee </label>
                                                    <input type="text" maxlength="48" value="{{@$user->development_fee[$key]}}" id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Annual Charges </label>
                                                    <input type="text" maxlength="48" value="{{@$user->annual_charge[$key]}}" id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Other Charges </label>
                                                    <input type="text" maxlength="48" value="{{@$user->other_charges[$key]}}" id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Total </label>
                                                    <input type="text" maxlength="48" value="{{@$user->total[$key]}}" id="total" name="total[]" class="form-control rte_input total">
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <div class="col-12"><span style="display:inline" class="msg teaching_fee_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg registration_fee_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg enrollment_fee_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg security_money_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg development_fee_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg annual_charge_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg other_charges_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg total_id"></span><br></div>

                                    @if ($errors->has('teaching_fee.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teaching_fee.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('registration_fee.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('registration_fee.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('enrollment_fee.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('enrollment_fee.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('security_money.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('security_money.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('development_fee.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('development_fee.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('annual_charge.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('annual_charge.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('other_charges.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('other_charges.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('total.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('total.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="Enrollment" role="tabpanel" aria-labelledby="Enrollment-tab">
                            <div class="card">
                                <div class="card-header">
                                    Enrollment
                                </div>
                                <div class="card-body">
                                    <!-- <div class="container"> -->
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="">Select class from</label>
                                            <select name="form_class_id" class="form-control" id="from_cls">
                                                <option value="">From Class</option>
                                                @if(isset($user->allclasses))
                                                @foreach($user->allclasses as $data)
                                                <option value="{{$data->class_id}}" @if($data->class_id == @$user->form_class_id) selected @endif>{{$data->class_name}}</option>
                                                @endforeach
                                                @else
                                                @foreach($allclasses as $data)
                                                <option value="{{$data->class_id}}" @if($data->class_id == @$user->form_class_id) selected @endif>{{$data->class_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <span class="msg" id="form_class_id_id"></span>
                                            @if ($errors->has('form_class_id'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('form_class_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-5">
                                            <label for="">Select class to</label>
                                            <select name="to_class_id" class="form-control" id="to_cls">
                                                <option value="">To Class</option>
                                                @if(isset($user->allclasses))
                                                @foreach($user->allclasses as $data)
                                                <option value="{{$data->class_id}}" @if($data->class_id == @$user->to_class_id) selected @endif>{{$data->class_name}}</option>
                                                @endforeach
                                                @else
                                                @foreach($allclasses as $data)
                                                <option value="{{$data->class_id}}" @if($data->class_id == @$user->to_class_id) selected @endif>{{$data->class_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <span class="msg" id="to_class_id_id"></span>
                                            @if ($errors->has('to_class_id'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('to_class_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="pl-4 pt-4 col-md-2">
                                            <input style="float: right;" type="button" class="btn btn-primary" onclick="clsSearch()" value="Submit" />
                                        </div>
                                    </div>
                                    <div class="add_class_row">
                                        @if(isset($user->class_names))
                                        @foreach($user->class_names as $key => $val)
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">{{@$val}}</label>
                                                    <input readonly type="text" value="{{@$val}}" maxlength="10" name="className[]" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Number Of Section</label>
                                                    <input type="text" value="{{@$user->no_of_sections[$key]}}" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Number Of Students</label>
                                                    <input type="text" value="{{@$user->no_of_students[$key]}}" name="studentNo[]" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Teacher Student Ratio</label>
                                                    <input type="text" value="{{@$user->teacher_student_ratio[$key]}}" name="teacher_student_ratio[]" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Teacher Assigned </label>
                                                    <input type="text" value="{{@$user->teacher_student_assign[$key]}}" name="teacher_student_assign[]" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Number Of Teacher </label>
                                                    <input type="text" value="{{@$user->num_of_teacher[$key]}}" name="num_of_teacher[]" class="form-control rte_input">
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <div class="col-12"><span style="display:inline" class="msg SectionNo_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg studentNo_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_student_ratio_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_student_assign_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg num_of_teacher_id"></span><br></div>

                                    @if ($errors->has('className.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('className.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('SectionNo.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('SectionNo.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('studentNo.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('studentNo.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_student_ratio.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_student_ratio.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_student_assign.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_student_assign.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('num_of_teacher.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('num_of_teacher.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    <!-- <br> -->
                                    <!-- <div id="demo">
                                
                                                <a href="javascript:void(0)" class="btn btn-danger mx-3" id="remove1">Remove </a>
                                            </div> -->
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="Infrastructure" role="tabpanel" aria-labelledby="Infrastructure-tab">
                            <div class="card-body">
                                <!-- <div class="container"> -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Classes </label>
                                            <input type="text" value="{{@$user->no_of_class}}" maxlength="10" name="no_of_class" class="form-control rte_input">
                                            <span class="msg" id="no_of_class_id"></span>
                                            @if ($errors->has('no_of_class'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('no_of_class') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Classroom (in w*h)</label>
                                            <input type="text" value="{{@$user->avg_size_cls_room}}" maxlength="10" name="avg_size_cls_room" class="form-control rte_input">
                                            <span class="msg" id="avg_size_cls_room_id"></span>
                                            @if ($errors->has('avg_size_cls_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('avg_size_cls_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Office Rooms</label>
                                            <input type="text" value="{{@$user->no_of_office_room}}" maxlength="10" name="no_of_office_room" class="form-control rte_input">
                                            <span class="msg" id="no_of_office_room_id"></span>
                                            @if ($errors->has('no_of_office_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('no_of_office_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Office Rooms (in w*h)</label>
                                            <input type="text" value="{{@$user->avg_size_of_office_room}}" maxlength="10" name="avg_size_of_office_room" class="form-control rte_input">
                                            <span class="msg" id="avg_size_of_office_room_id"></span>
                                            @if ($errors->has('avg_size_of_office_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('avg_size_of_office_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Store Rooms</label>
                                            <input type="text" value="{{@$user->no_of_store_room}}" maxlength="10" name="no_of_store_room" class="form-control rte_input">
                                            <span class="msg" id="no_of_store_room_id"></span>
                                            @if ($errors->has('no_of_store_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('no_of_store_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Store Rooms (in w*h)</label>
                                            <input type="text" value="{{@$user->avg_size_of_store_room}}" maxlength="10" name="avg_size_of_store_room" class="form-control rte_input">
                                            <span class="msg" id="avg_size_of_store_room_id"></span>
                                            @if ($errors->has('avg_size_of_store_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('avg_size_of_store_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Principal Rooms</label>
                                            <input type="text" value="{{@$user->no_of_princpal_room}}" maxlength="10" name="no_of_princpal_room" class="form-control rte_input">
                                            <span class="msg" id="no_of_princpal_room_id"></span>
                                            @if ($errors->has('no_of_princpal_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('no_of_princpal_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Principal Rooms (in w*h)</label>
                                            <input type="text" value="{{@$user->avg_size_of_principal_room}}" maxlength="10" name="avg_size_of_principal_room" class="form-control rte_input">
                                            <span class="msg" id="avg_size_of_principal_room_id"></span>
                                            @if ($errors->has('avg_size_of_principal_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('avg_size_of_principal_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Kitchen Rooms</label>
                                            <input type="text" value="{{@$user->no_of_kitchen_room}}" maxlength="10" name="no_of_kitchen_room" class="form-control rte_input">
                                            <span class="msg" id="no_of_kitchen_room_id"></span>
                                            @if ($errors->has('no_of_kitchen_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('no_of_kitchen_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Kitchen Rooms (in w*h)</label>
                                            <input type="text" value="{{@$user->avg_size_of_kitchen_room}}" maxlength="10" name="avg_size_of_kitchen_room" class="form-control rte_input">
                                            <span class="msg" id="avg_size_of_kitchen_room_id"></span>
                                            @if ($errors->has('avg_size_of_kitchen_room'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('avg_size_of_kitchen_room') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Does the school has boundary</label>
                                            <div>
                                                <input type="radio" {{ @$user->is_school_has_boundary == 'Yes' ? 'checked' : ''}} value="Yes" name="is_school_has_boundary" class=""> Yes
                                                <input type="radio" {{ @$user->is_school_has_boundary == 'No' ? 'checked' : ''}} value="No" name="is_school_has_boundary" class="" style="margin-left:20px;"> No
                                                <span class="msg" id="is_school_has_boundary_id"></span>
                                                @if ($errors->has('is_school_has_boundary'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('is_school_has_boundary') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is_school_has_boundary_doc">
                                            <label for="">Size Of Boundry( W * H )</label>
                                            <div>
                                                <input type="text" value="{{ @$user->size_of_school_boundary}}" name="size_of_school_boundary" class="form-control">
                                                <span class="msg" id="size_of_school_boundary_id"></span>
                                                @if ($errors->has('size_of_school_boundary'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('size_of_school_boundary') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" class="">
                                        <div class="form-group is_school_has_boundary_doc">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->school_boundary_files == null || $user->school_boundary_files == '')
                                            <input type="file" id="school_boundary_files" name="school_boundary_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_boundary_files_check" id="school_boundary_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_boundary_files_check" id="school_boundary_files_check" value="1" />
                                            @if(count($user->school_boundary_files) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_boundary_files as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_boundary_files">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_boundary_files) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_boundary_files_check_id"></span>
                                            @if ($errors->has('school_boundary_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_boundary_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Does school has CCTV ?</label>
                                            <div>
                                                <input type="radio" {{ @$user->is_school_cctv == 'Yes' ? 'checked' : ''}} value="Yes" name="is_school_cctv" class=""> Yes
                                                <input type="radio" {{ @$user->is_school_cctv == 'No' ? 'checked' : ''}} value="No" name="is_school_cctv" class="" style="margin-left:20px;"> No
                                                <span class="msg" id="is_school_cctv_id"></span>
                                                @if ($errors->has('is_school_cctv'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('is_school_cctv') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is_school_cctv_doc">
                                            <label for="">No. of CCTV </label>
                                            <div>
                                                <input type="text" value="{{ @$user->no_of_school_cctv}}" name="no_of_school_cctv" class="form-control">
                                                <span class="msg" id="no_of_school_cctv_id"></span>
                                                @if ($errors->has('no_of_school_cctv'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('no_of_school_cctv') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group is_school_cctv_doc">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->school_cctv_file == null || $user->school_cctv_file == '')
                                            <input type="file" id="school_cctv_file" name="school_cctv_file[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_cctv_file_check" id="school_cctv_file_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_cctv_file_check" id="school_cctv_file_check" value="1" />
                                            @if(count($user->school_cctv_file) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_cctv_file as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_cctv_file">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_cctv_file) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_cctv_file_check_id"></span>
                                            @if ($errors->has('school_cctv_file[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_cctv_file[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Sport ground ?</label>
                                            <div>
                                                <input type="radio" {{ @$user->is_school_ground == 'Yes' ? 'checked' : ''}} value="Yes" name="is_school_ground" class=""> Yes
                                                <input type="radio" {{ @$user->is_school_ground == 'No' ? 'checked' : ''}} value="No" name="is_school_ground" class="" style="margin-left:20px;"> No
                                                <span class="msg" id="is_school_ground_id"></span>
                                                @if ($errors->has('is_school_ground'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('is_school_ground') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is_school_ground_doc">
                                            <label for="">Size Of Sport ground ( W * H )</label>
                                            <div>
                                                <input type="text" value="{{ @$user->size_of_school_ground}}" name="size_of_school_ground" class="form-control">
                                                <span class="msg" id="size_of_school_ground_id"></span>
                                                @if ($errors->has('size_of_school_ground'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('size_of_school_ground') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group is_school_ground_doc">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->school_ground_file == null || $user->school_ground_file == '')
                                            <input type="file" id="school_ground_file" name="school_ground_file[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_ground_file_check" id="school_ground_file_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_ground_file_check" id="school_ground_file_check" value="1" />
                                            @if(count($user->school_ground_file) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_ground_file as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_ground_file">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_ground_file) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_ground_file_check_id"></span>
                                            @if ($errors->has('school_ground_file[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_ground_file[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Dose the school has 3 start rating in water and hygene facilities ?</label>
                                            <div>
                                                <input type="radio" {{ @$user->is_school_water_hygene_facilities == 'Yes' ? 'checked' : ''}} value="Yes" name="is_school_water_hygene_facilities" class=""> Yes
                                                <input type="radio" {{ @$user->is_school_water_hygene_facilities == 'No' ? 'checked' : ''}} value="No" name="is_school_water_hygene_facilities" class="" style="margin-left:20px;"> No
                                                <span class="msg" id="is_school_water_hygene_facilities_id"></span>
                                                @if ($errors->has('is_school_water_hygene_facilities'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('is_school_water_hygene_facilities') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is_school_water_hygene_facilities_doc">
                                            <label for="">No. of water and hygene facilitie</label>
                                            <div>
                                                <input type="text" value="{{ @$user->no_of_school_water_hygene_facilities}}" name="no_of_school_water_hygene_facilities" class="form-control">
                                                <span class="msg" id="no_of_school_water_hygene_facilities_id"></span>
                                                @if ($errors->has('no_of_school_water_hygene_facilities'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('no_of_school_water_hygene_facilities') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group is_school_water_hygene_facilities_doc">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->school_water_hygene_facilities_files == null || $user->school_water_hygene_facilities_files == '')
                                            <input type="file" id="school_water_hygene_facilities_files" name="school_water_hygene_facilities_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_water_hygene_facilities_files_check" id="school_water_hygene_facilities_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_water_hygene_facilities_files_check" id="school_water_hygene_facilities_files_check" value="1" />
                                            @if(count($user->school_water_hygene_facilities_files) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_water_hygene_facilities_files as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_water_hygene_facilities_files">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_water_hygene_facilities_files) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_water_hygene_facilities_files_check_id"></span>
                                            @if ($errors->has('school_water_hygene_facilities_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_water_hygene_facilities_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Dose the school has fire safty facilities ?</label>
                                            <div>
                                                <input type="radio" {{ @$user->is_school_fire_safty_facilities == 'Yes' ? 'checked' : ''}} value="Yes" name="is_school_fire_safty_facilities" class=""> Yes
                                                <input type="radio" {{ @$user->is_school_fire_safty_facilities == 'No' ? 'checked' : ''}} value="No" name="is_school_fire_safty_facilities" class="" style="margin-left:20px;"> No
                                                <span class="msg" id="is_school_fire_safty_facilities_id"></span>
                                                @if ($errors->has('is_school_fire_safty_facilities'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('is_school_fire_safty_facilities') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group is_school_fire_safty_facilities_doc">
                                            <label for="">No. of fire safty facilities </label>
                                            <div>
                                                <input type="text" value="{{ @$user->no_of_fire_safty}}" name="no_of_fire_safty" class="form-control">
                                                <span class="msg" id="no_of_fire_safty_id"></span>
                                                @if ($errors->has('no_of_fire_safty'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('no_of_fire_safty') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group is_school_fire_safty_facilities_doc">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->school_fire_safty_facilities_files == null || $user->school_fire_safty_facilities_files == '')
                                            <input type="file" id="school_fire_safty_facilities_files" name="school_fire_safty_facilities_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_fire_safty_facilities_files_check" id="school_fire_safty_facilities_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="school_fire_safty_facilities_files_check" id="school_fire_safty_facilities_files_check" value="1" />
                                            @if(count($user->school_fire_safty_facilities_files) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->school_fire_safty_facilities_files as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="school_fire_safty_facilities_files">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->school_fire_safty_facilities_files) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="school_fire_safty_facilities_files_check_id"></span>
                                            @if ($errors->has('school_fire_safty_facilities_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_fire_safty_facilities_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="Other-Convenience" role="tabpanel" aria-labelledby="Other-Convenience-tab">
                            <div class="card-body">
                                <!-- <div class="container"> -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Does all facilities have access without interruption? </label>
                                            <div>
                                                <input type="radio" {{ @$user->facilities_access_without_interrupted == 'Yes' ? 'checked' : ''}} value="Yes" class="" name="facilities_access_without_interrupted"> Yes
                                                <input type="radio" {{ @$user->facilities_access_without_interrupted == 'No' ? 'checked' : ''}} value="No" class="" name="facilities_access_without_interrupted" style="margin-left:20px;"> No
                                                <span class="msg" id="facilities_access_without_interrupted_id"></span>
                                                @if ($errors->has('facilities_access_without_interrupted'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('facilities_access_without_interrupted') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">List All Teaching Materials</label>
                                            <textarea type="text" name="all_teaching_material_list" class="form-control rte_input">{{@$user->all_teaching_material_list}}</textarea>
                                            <span class="msg" id="all_teaching_material_list_id"></span>
                                            @if ($errors->has('all_teaching_material_list'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('all_teaching_material_list') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Add relevant evidence attachement </label>
                                            @if($user->all_teaching_material_list_files_ex == null || $user->all_teaching_material_list_files_ex == '')
                                            <input type="file" id="all_teaching_material_list_files" name="all_teaching_material_list_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="all_teaching_material_list_files_check" id="all_teaching_material_list_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="all_teaching_material_list_files_check" id="all_teaching_material_list_files_check" value="1" />
                                            @if(count($user->all_teaching_material_list_files_ex) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->all_teaching_material_list_files_ex as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="all_teaching_material_list_files">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->all_teaching_material_list_files_ex) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="all_teaching_material_list_files_check_id"></span>
                                            @if ($errors->has('all_teaching_material_list_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('all_teaching_material_list_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">List All Sports Equipments</label>
                                            <textarea type="text" name="all_sports_equipment_list" class="form-control rte_input">{{@$user->all_sports_equipment_list}}</textarea>
                                            <span class="msg" id="all_sports_equipment_list_id"></span>
                                            @if ($errors->has('all_sports_equipment_list'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('all_sports_equipment_list') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->all_sports_equipment_list_file == null || $user->all_sports_equipment_list_file == '')
                                            <input type="file" id="all_sports_equipment_list_file" name="all_sports_equipment_list_file[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="all_sports_equipment_list_file_check" id="all_sports_equipment_list_file_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="all_sports_equipment_list_file_check" id="all_sports_equipment_list_file_check" value="1" />
                                            @if(count($user->all_sports_equipment_list_file) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->all_sports_equipment_list_file as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="all_sports_equipment_list_file">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->all_sports_equipment_list_file) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="all_sports_equipment_list_file_check_id"></span>
                                            @if ($errors->has('all_sports_equipment_list_file[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('all_sports_equipment_list_file[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Books Facilities In Library</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Books In Library</label>
                                            <input type="text" value="{{@$user->books}}" maxlength="200" name="books" class="form-control rte_input">
                                            <span class="msg" id="books_id"></span>
                                            @if ($errors->has('books'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('books') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Newspaper & Magazine</label>
                                            <input type="text" value="{{@$user->magazines}}" maxlength="200" name="magazines" class="form-control rte_input">
                                            <span class="msg" id="magazines_id"></span>
                                            @if ($errors->has('magazines'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('magazines') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Add Relevant evidence attachement</label>
                                            @if($user->books_in_library == null || $user->books_in_library == '')
                                            <input type="file" id="books_in_library_files" name="books_in_library_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="books_in_library_files_check" id="books_in_library_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="books_in_library_files_check" id="books_in_library_files_check" value="1" />
                                            @if(count($user->books_in_library) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->books_in_library as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="books_in_library">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->books_in_library) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="books_in_library_files_check_id"></span>
                                            @if ($errors->has('books_in_library_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('books_in_library_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Water Facilities</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Types Of Water Facilities</label>
                                            <input type="text" value="{{@$user->type_of_water_facilities}}" maxlength="30" name="type_of_water_facilities" class="form-control rte_input">
                                            <span class="msg" id="type_of_water_facilities_id"></span>
                                            @if ($errors->has('type_of_water_facilities'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('type_of_water_facilities') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Water Supply</label>
                                            <input type="text" value="{{@$user->no_of_water_supply}}" maxlength="30" name="no_of_water_supply" class="form-control rte_input">
                                            <span class="msg" id="no_of_water_supply_id"></span>
                                            @if ($errors->has('no_of_water_supply'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('no_of_water_supply') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Add Relevant evidence attachement</label>
                                            @if($user->water_facilities == null || $user->water_facilities == '')
                                            <input type="file" id="water_facilities_files" name="water_facilities_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="water_facilities_files_check" id="water_facilities_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="water_facilities_files_check" id="water_facilities_files_check" value="1" />
                                            @if(count($user->water_facilities) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->water_facilities as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="water_facilities">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->water_facilities) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="water_facilities_files_check_id"></span>
                                            @if ($errors->has('water_facilities_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('water_facilities_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Cleanliness Related Details</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Type Of Toilets</label>
                                            <input type="text" value="{{@$user->type_of_toilet}}" maxlength="10" name="type_of_toilet" class="form-control rte_input">
                                            <span class="msg" id="type_of_toilet_id"></span>
                                            @if ($errors->has('type_of_toilet'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('type_of_toilet') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Separate Toilet For Boys</label>
                                            <input type="number" value="{{@$user->gents_toilet}}" maxlength="10" name="gents_toilet" class="form-control rte_input">
                                            <span class="msg" id="gents_toilet_id"></span>
                                            @if ($errors->has('gents_toilet'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('gents_toilet') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Separate Toilet For Girls</label>
                                            <input type="number" value="{{@$user->ladies_toilet}}" maxlength="10" name="ladies_toilet" class="form-control rte_input">
                                            <span class="msg" id="ladies_toilet_id"></span>
                                            @if ($errors->has('ladies_toilet'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('ladies_toilet') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Add Relevant evidence attachement</label>
                                            @if($user->cleanliness == null || $user->cleanliness == '')
                                            <input type="file" id="cleanliness_files" name="cleanliness_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="cleanliness_files_check" id="cleanliness_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="cleanliness_files_check" id="cleanliness_files_check" value="1" />
                                            @if(count($user->cleanliness) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->cleanliness as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="cleanliness">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->cleanliness) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="cleanliness_files_check_id"></span>
                                            @if ($errors->has('cleanliness_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('cleanliness_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade" id="Teaching-Staff-Specialties" role="tabpanel" aria-labelledby="Teaching-Staff-Specialties-tab">
                            <div class="card-body">
                                <!-- <div class="container"> -->
                                <div class="row">
                                    <!-- <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Does all facilities have access without interrupted? </label>
                                                    <div>
                                                        <input  type="checkbox" class="  "> Yes
                                                        <input  type="checkbox" class="" style="margin-left:20px;"> No
                                                    </div>
        
                                                </div>
                                            </div> -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Name Of Principal</label>
                                            <input type="text" value="{{@$user->principle_name}}" maxlength="100" name="principle_name" class="form-control rte_input">
                                            <span class="msg" id="principle_name_id"></span>
                                            @if ($errors->has('principle_name'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('principle_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Father / Husband Or Wife Name</label>
                                            <input type="text" value="{{@$user->p_f_h_w_name}}" name="p_f_h_w_name" maxlength="100" class="form-control rte_input">
                                            <span class="msg" id="p_f_h_w_name_id"></span>
                                            @if ($errors->has('p_f_h_w_name'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('p_f_h_w_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Date Of Birth</label>
                                            <input type="date" value="{{@$user->p_date_of_birth}}" name="p_date_of_birth" class="form-control rte_input">
                                            <span class="msg" id="p_date_of_birth_id"></span>
                                            @if ($errors->has('p_date_of_birth'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('p_date_of_birth') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Educational Qualification</label>
                                            <input type="text" value="{{@$user->p_education_qualification}}" maxlength="100" name="p_education_qualification" class="form-control rte_input">
                                            <span class="msg" id="p_education_qualification_id"></span>
                                            @if ($errors->has('p_education_qualification'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('p_education_qualification') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Trainee Qualification</label>
                                            <input type="text" value="{{@$user->pri_trainee_qualification}}" maxlength="100" name="trainee_qualification" class="form-control rte_input">
                                            <span class="msg" id="trainee_qualification_id"></span>
                                            @if ($errors->has('trainee_qualification'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('trainee_qualification') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teaching Experience</label>
                                            <input type="text" value="{{@$user->pri_teaching_experience}}" maxlength="100" name="teaching_experience" class="form-control rte_input">
                                            <span class="msg" id="teaching_experience_id"></span>
                                            @if ($errors->has('teaching_experience'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('teaching_experience') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Class Handed Over</label>
                                            <input type="text" value="{{@$user->class_handed_over}}" maxlength="100" name="class_handed_over" class="form-control rte_input">
                                            <span class="msg" id="class_handed_over_id"></span>
                                            @if ($errors->has('class_handed_over'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('class_handed_over') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Date Of Appointment</label>
                                            <input type="date" value="{{@$user->pri_date_of_appointment}}" name="date_of_appointment" class="form-control rte_input">
                                            <span class="msg" id="date_of_appointment_id"></span>
                                            @if ($errors->has('date_of_appointment'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('date_of_appointment') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Trained Or Untrained</label>
                                            <input type="text" value="{{@$user->trained_untrained}}" maxlength="100" name="trained_untrained" class="form-control rte_input">
                                            <span class="msg" id="trained_untrained_id"></span>
                                            @if ($errors->has('trained_untrained'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('trained_untrained') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Add relevant evidence attachement</label>
                                            @if($user->educational_qualification_files == null || $user->educational_qualification_files == '')
                                            <input type="file" id="educational_qualification_files" name="educational_qualification_files[]" multiple class="file_upload form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="educational_qualification_files_check" id="educational_qualification_files_check" value="0" />

                                            @else
                                            <input type="hidden" class=" file_upload_check form-control rte_input" name="educational_qualification_files_check" id="educational_qualification_files_check" value="1" />
                                            @if(count($user->educational_qualification_files) > 2)
                                            <div style="width: 550px;height: 70px; overflow: scroll;">
                                                @endif
                                                @foreach($user->educational_qualification_files as $key => $val)
                                                <div>
                                                    <form style="display: inline;">
                                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> {{$val}} </a>
                                                    </form>
                                                    @if($key==0)
                                                    <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                    @endif
                                                    <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                    <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                        <input type="hidden" class='text-block file_name' name="file_name" value="{{$val}}">
                                                        <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                        <input type="hidden" class='text-block field_name' name="field_name" value="educational_qualification_files">
                                                        <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                    </form>
                                                </div>
                                                @endforeach
                                                @if(count($user->educational_qualification_files) > 2)
                                            </div>
                                            @endif
                                            @endif
                                            <span class="msg" id="educational_qualification_files_check_id"></span>
                                            @if ($errors->has('educational_qualification_files[]'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('educational_qualification_files[]') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @php
                                    $Last_index = 1;
                                    @endphp
                                    @if(isset($user->application_teacher_data))
                                    @foreach($user->application_teacher_data as $key => $val)
                                    @if($key == 0)
                                    <div class="row pt-3 px-3" id="dynamic_form_create">
                                        <div class="col-md-12">
                                            <h5> Teacher Information {{$Last_index}}</h5>
                                        </div>
                                        <hr>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Name Of Teacher</label>
                                                <input hidden type="text" value="{{@$val->id}}" name="teacher_id[]" id="teacher_id" class="form-control rte_input">
                                                <input type="text" value="{{@$val->teacher_name}}" name="teacher_name[]" id="teacher_name" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Father / Husband Or Wife Name</label>
                                                <input type="text" value="{{@$val->teacher_f_h_w_name}}" name="teacher_f_h_w_name[]" id="teacher_f_h_w_name" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Date Of Birth</label>
                                                <input type="date" value="{{@$val->date_of_birth}}" name="teacher_date_of_birth[]" id="teacher_date_of_birth" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Educational Qualification</label>
                                                <input type="text" value="{{@$val->education_qualification}}" name="teacher_education_qualification[]" id="teacher_education_qualification" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Trainee Qualification</label>
                                                <input type="text" value="{{@$val->trainee_qualification}}" name="teacher_trainee_qualification[]" id="teacher_trainee_qualification" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Teaching Experience</label>
                                                <input type="text" value="{{@$val->teaching_experience}}" name="teacher_teaching_experience[]" id="teacher_teaching_experience" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Class Handed Over</label>
                                                <input type="text" value="{{@$val->cls_handed_over}}" name="teacher_class_handed_over[]" id="teacher_class_handed_over" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Date Of Appointment</label>
                                                <input type="date" value="{{@$val->date_of_appointment}}" name="teacher_appointment_date[]" id="teacher_appointment_date" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Trained Or Untrained</label>
                                                <input type="text" value="{{@$val->trained_or_untrained}}" name="teacher_trained_or_untrained[]" id="teacher_trained_or_untrained" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Add relevant evidence attachement </label>
                                                @if($val->teacher_educational_qualification_files == null || $val->teacher_educational_qualification_files == '' || $val->teacher_educational_qualification_files == '[]' )
                                                <input type="file" id="teacher_educational_qualification_files" name="teacher_educational_qualification_files[{{$key}}][]" multiple class="file_upload form-control teacher_educational_qualification_files" accept=".pdf, .jpg, .png, jpeg" />
                                                <input type="hidden" class=" file_upload_check form-control rte_input teacher_educational_qualification_files_check" name="teacher_educational_qualification_files_check[]" id="teacher_educational_qualification_files_check" value="0" />
                                                @else
                                                <input type="hidden" class=" file_upload_check form-control rte_input teacher_educational_qualification_files_check" name="teacher_educational_qualification_files_check[]" id="teacher_educational_qualification_files_check" value="1" />
                                                @if(isset($val->teacher_educational_qualification_files))
                                                @php $val->teacher_educational_qualification_files_n = json_decode($val->teacher_educational_qualification_files, true); @endphp
                                                @if(count($val->teacher_educational_qualification_files_n) > 2)
                                                <div style="width: 550px;height: 70px; overflow: scroll;">
                                                    @endif
                                                    @foreach($val->teacher_educational_qualification_files_n as $key_file => $val_file)
                                                    <div>
                                                        <form style="display: inline;">
                                                            <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val_file}}" target="_blank"> {{$val_file}} </a>
                                                        </form>
                                                        @if($key_file==0)
                                                        <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                        @endif
                                                        <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                        <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <input type="hidden" class='text-block file_name' name="file_name" value="{{$val_file}}">
                                                            <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                            <input type="hidden" class='text-block teacher_id' name="teacher_id" value="{{@$val->id}}">
                                                            <input type="hidden" class='text-block field_name' name="field_name" value="teacher_educational_qualification_files">
                                                            <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                        </form>
                                                    </div>
                                                    @endforeach
                                                    @endif
                                                    @if(count($val->teacher_educational_qualification_files_n) > 2)
                                                </div>
                                                @endif
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-right pt-4">
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-success" onclick="teacherRowAdd()" id="add">Add</a>
                                            </div>
                                            <!-- <div class="button-group">
                                                    <a href="javascript:void(0)" class="btn btn-primary" id="plus5">Add More</a>
                                                    <a href="javascript:void(0)" class="btn btn-danger" id="minus5">Remove</a>
                                                </div>  -->
                                        </div>
                                    </div>
                                    @else
                                    @php
                                    $Last_index = $Last_index+1;
                                    @endphp
                                    <div class=" removeTeacherRow">
                                        <div class="row pt-3 px-3">
                                            <div class="col-md-12">
                                                <h5> Teacher Information {{$Last_index}}</h5>
                                            </div>
                                            <hr>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Name Of Teacher</label>
                                                    <input hidden type="text" value="{{@$val->id}}" name="teacher_id[]" id="teacher_id" class="form-control rte_input">
                                                    <input type="text" value="{{@$val->teacher_name}}" name="teacher_name[]" id="teacher_name" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Father / Husband Or Wife Name</label>
                                                    <input type="text" value="{{@$val->teacher_f_h_w_name}}" name="teacher_f_h_w_name[]" id="teacher_f_h_w_name" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date Of Birth</label>
                                                    <input type="date" value="{{@$val->date_of_birth}}" name="teacher_date_of_birth[]" id="teacher_date_of_birth" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Educational Qualification</label>
                                                    <input type="text" value="{{@$val->education_qualification}}" name="teacher_education_qualification[]" id="teacher_education_qualification" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Trainee Qualification</label>
                                                    <input type="text" value="{{@$val->trainee_qualification}}" name="teacher_trainee_qualification[]" id="teacher_trainee_qualification" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Teaching Experience</label>
                                                    <input type="text" value="{{@$val->teaching_experience}}" name="teacher_teaching_experience[]" id="teacher_teaching_experience" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Class Handed Over</label>
                                                    <input type="text" value="{{@$val->cls_handed_over}}" name="teacher_class_handed_over[]" id="teacher_class_handed_over" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date Of Appointment</label>
                                                    <input type="date" value="{{@$val->date_of_appointment}}" name="teacher_appointment_date[]" id="teacher_appointment_date" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Trained Or Untrained</label>
                                                    <input type="text" value="{{@$val->trained_or_untrained}}" name="teacher_trained_or_untrained[]" id="teacher_trained_or_untrained" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Add relevant evidence attachement </label>
                                                    @if($val->teacher_educational_qualification_files == null || $val->teacher_educational_qualification_files == '' || $val->teacher_educational_qualification_files == '[]' || $val->teacher_educational_qualification_files == '.*')
                                                    <input type="file" id="teacher_educational_qualification_files" name="teacher_educational_qualification_files[{{$key}}][]" multiple class="file_upload form-control teacher_educational_qualification_files" accept=".pdf, .jpg, .png, jpeg" />
                                                    <input type="hidden" class=" file_upload_check form-control rte_input teacher_educational_qualification_files_check" name="teacher_educational_qualification_files_check[]" value="0" />
                                                    @else
                                                    <input type="hidden" class=" file_upload_check form-control rte_input teacher_educational_qualification_files_check" name="teacher_educational_qualification_files_check[]" value="1" />
                                                    @if(isset($val->teacher_educational_qualification_files))
                                                    @php $val->teacher_educational_qualification_files_n = json_decode($val->teacher_educational_qualification_files, true);
                                                    $Count = count($val->teacher_educational_qualification_files_n);
                                                    @endphp
                                                    @if( $Count > 2)
                                                    <div style="width: 550px;height: 70px; overflow: scroll;">
                                                        @endif
                                                        @foreach($val->teacher_educational_qualification_files_n as $key_file => $val_file)
                                                        <div>
                                                            <form style="display: inline;">
                                                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val_file}}" target="_blank"> {{$val_file}} </a>
                                                            </form>
                                                            @if($key_file==0)
                                                            <button class="btn btn-white AddFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#NewAdd"><i class="fa fa-plus text-success"></i></button>
                                                            @endif
                                                            <button class="btn btn-white UpdateFile btn-sm " type="button" data-toggle="modal" title="Updtae File" data-target="#Update"><i class="fa fa-edit text-primary"></i></button>
                                                            <form style="display: inline;" method="post" action="{{url('remove-application-file')}}" enctype="multipart/form-data">
                                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                <input type="hidden" class='text-block file_name' name="file_name" value="{{$val_file}}">
                                                                <input type="hidden" class='text-block application_id' name="application_id" value="{{$user->id}}">
                                                                <input type="hidden" class='text-block teacher_id' name="teacher_id" value="{{@$val->id}}">
                                                                <input type="hidden" class='text-block field_name' name="field_name" value="teacher_educational_qualification_files">
                                                                <button type="submit" class="btn btn-sm btn-white btn-offset text-block" title=""><i class="fa fa-times text-danger " title="Remove File"> </i></button>
                                                            </form>
                                                        </div>
                                                        @endforeach
                                                        @if( $Count > 2)
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3 text-right pt-4">
                                                <div>
                                                    <a href="javascript:void(0)" class="btn btn-danger" onclick="remove_teacher_row(this)" id="remove">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    @else
                                    <div class="row pt-3 px-3" id="dynamic_form_create">
                                        <div class="col-md-12">
                                            <h5> Teacher Information </h5>
                                        </div>
                                        <hr>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Name Of Teacher</label>
                                                <input type="text" name="teacher_id[]" id="teacher_id" class="form-control rte_input">
                                                <input type="text" name="teacher_name[]" id="teacher_name" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Father / Husband Or Wife Name</label>
                                                <input type="text" name="teacher_f_h_w_name[]" id="teacher_f_h_w_name" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Date Of Birth</label>
                                                <input type="date" name="teacher_date_of_birth[]" id="teacher_date_of_birth" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Educational Qualification</label>
                                                <input type="text" name="teacher_education_qualification[]" id="teacher_education_qualification" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Trainee Qualification</label>
                                                <input type="text" name="teacher_trainee_qualification[]" id="teacher_trainee_qualification" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Teaching Experience</label>
                                                <input type="text" name="teacher_teaching_experience[]" id="teacher_teaching_experience" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Class Handed Over</label>
                                                <input type="text" name="teacher_class_handed_over[]" id="teacher_class_handed_over" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Date Of Appointment</label>
                                                <input type="date" name="teacher_appointment_date[]" id="teacher_appointment_date" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Trained Or Untrained</label>
                                                <input type="text" name="teacher_trained_or_untrained[]" id="teacher_trained_or_untrained" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Add relevant evidence attachement</label>
                                                <input type="file" id="teacher_educational_qualification_files" name="teacher_educational_qualification_files[0][]" multiple class="file_upload form-control teacher_educational_qualification_files" accept=".pdf, .jpg, .png, jpeg" />
                                                <input type="hidden" class=" file_upload_check form-control rte_input teacher_educational_qualification_files_check" name="teacher_educational_qualification_files_check[]" value="0" />

                                            </div>
                                        </div>
                                        <div class="col-md-3 text-right pt-4">
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-success" onclick="teacherRowAdd()" id="add">Add</a>
                                            </div>
                                            <!-- <div class="button-group">
                                                            <a href="javascript:void(0)" class="btn btn-primary" id="plus5">Add More</a>
                                                            <a href="javascript:void(0)" class="btn btn-danger" id="minus5">Remove</a>
                                                        </div>  -->
                                        </div>
                                    </div>
                                    @endif
                                    <div class="addTeacherRow">
                                    </div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_name_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_f_h_w_name_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_date_of_birth_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_education_qualification_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_trainee_qualification_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_teaching_experience_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_class_handed_over_id"></span><br></div>
                                    <div class="col-12"><span style="display:inline" class="msg teacher_appointment_date_id"></span><br></div>
                                    <div class="col-12"><span class="msg teacher_trained_or_untrained_id"></span></div>
                                    <div class="col-12"><span class="msg teacher_educational_qualification_files_check_id"></span></div>
                                    @if ($errors->has('teacher_name.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_name.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_f_h_w_name.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_f_h_w_name.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_date_of_birth.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_date_of_birth.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_education_qualification.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_education_qualification.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_trainee_qualification.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_trainee_qualification.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_teaching_experience.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_teaching_experience.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_class_handed_over.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_class_handed_over.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_appointment_date.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_appointment_date.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_trained_or_untrained.*'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_trained_or_untrained.*') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    @if ($errors->has('teacher_educational_qualification_files[][]'))
                                    <div class="col-12">
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('teacher_educational_qualification_files[][]') }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="tab-pane ui-tabs-panel ui-tabs-hide fade last" id="Curriculum-and-Syllabus" role="tabpanel" aria-labelledby="Curriculum-and-Syllabus-tab">
                            <div class="card-body">
                                <!-- <div class="container"> -->
                                @if(isset($user->details_of_curriculum))
                                @foreach($user->details_of_curriculum as $key => $val)
                                @if($key == 0)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6) </label>
                                            <input value="{{@$val}}" type="text" maxlength="100" name="details_of_curriculum[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Method Of Inspection Of Students</label>
                                            <input value="{{@$user->method_of_inspection[$key]}}" type="text" maxlength="100" name="method_of_inspection[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pt-4">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="addcurrandsyllabus()" id="addx">Add</a>
                                    </div>
                                </div>
                                @else
                                <div class="curr_syllabus_row">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6)</label>
                                                <input value="{{@$val}}" type="text" maxlength="100" name="details_of_curriculum[]" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Method Of Inspection Of Students</label>
                                                <input value="{{@$user->method_of_inspection[$key]}}" type="text" maxlength="100" name="method_of_inspection[]" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-2 pt-4">
                                            <a href="javascript:void(0)" class="btn btn-danger" onclick="remove_curr_syllabus_row(this)">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                @else
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6)</label>
                                            <input type="text" maxlength="100" name="details_of_curriculum[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Method Of Inspection Of Students</label>
                                            <input type="text" maxlength="100" name="method_of_inspection[]" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pt-4">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="addcurrandsyllabus()" id="addx">Add</a>
                                    </div>
                                </div>
                                @endif
                                <div class="add_currandsyllabus_row">
                                </div>
                                @if ($errors->has('details_of_curriculum.*'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('details_of_curriculum.*') }}</strong>
                                </span>
                                @endif
                                @if ($errors->has('method_of_inspection.*'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('method_of_inspection.*') }}</strong>
                                </span>
                                @endif
                                <span class="msg details_of_curriculum_id"></span><br>
                                <span class="msg method_of_inspection_id"></span>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Are the students of the school expected to take any board examination till class 6 ?</label>
                                        <div>
                                            <input value="Yes" {{ @$user->school_board_exam_till_cls_eight == 'Yes' ? 'checked' : ''}} type="radio" name="school_board_exam_till_cls_eight" class=" "> Yes
                                            <input value="No" {{ @$user->school_board_exam_till_cls_eight == 'No' ? 'checked' : ''}} type="radio" name="school_board_exam_till_cls_eight" class=" " style="margin-left:20px;"> No
                                            <span class="msg" id="school_board_exam_till_cls_eight_id"></span>
                                            @if ($errors->has('school_board_exam_till_cls_eight'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('school_board_exam_till_cls_eight') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <button class="btnnext  prevtab btn btn-primary" type="button" style="float:left;">Previous</button>
                        </div>
                        <div class="col-md-4" style="text-align: center;">
                            <button class="btn btn-success btn_submit mx-3 btnsavesubmit" name="save_as_data" value="2" type="submit">Save as Draft</button>
                            <!-- <button class="btn btn-primary btn_submit btnsavesubmit " style="float: right;" name="save_data" value="1" type="submit">Submit</button> -->
                        </div>
                        <div class="col-md-4">
                            <button class="btnPrevious nexttab btn btn-primary" type="button" style="float:right;">Next</button>
                            <!-- <button class="btn btn-primary btn_submit btnsavesubmit final_submit_button" style="float: right;" name="save_data" value="1" type="submit">Submit</button> -->
                            <button class="btn btn-primary btn_submit btnsavesubmit final_submit_button" type="button" data-toggle="modal" style="float: right;" data-target="#AlertMsg">Submit</button>
                            <div class="modal fade bd-example-modal-lg" id="AlertMsg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelAlertMsg" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-primary">
                                            <h5 class="modal-title " id="exampleModalLabelAlertMsg">Disclaimer </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div>
                                                a) <b>This is to Certify that an officer from state authority can also inspects the school.</b><br><br><br>
                                                b) <b>It is certified that the school undertakes that it shall submit such review reports and denials as may be required by the District Superintendent of Education from time to time and shall comply with all such instructions of the State Authority or District Education Superintendent, which are subject to the conditions of recognition. Issues will be issued to ensure continuous compliance or to remove deficiencies in the school's work space.</b>
                                                <br><br><br>
                                                c) <b>It is certified that the records of the school related to the implementation of this Act will be available for inspection from time to time by the District Superintendent of Education or any authority authorized by the State Authority and the school shall submit such review information. Which is necessary to enable the State Government or local authority or government to discharge its obligations to the Parliament / Legislative Assembly / Panchayat / Municipality as the case may be.</b>
                                                <br><br><br>
                                            </div>
                                            <button class="btn btn-primary btn_submit btnsavesubmit final_submit_button" style="float: right;" name="Payment" value="3" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
</div>
</form>
@include('footer')
<div class="modal fade bd-example-modal-lg" id="NewAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelAdd" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabelAdd">Add Document </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{url('insert-application-file')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block insert_application_id' name="application_id">
                    <input type="hidden" class='text-block insert_filed_name' name="field_name" />
                    <input type="hidden" class='text-block insert_teacher_id' name="teacher_id" />
                    <label>Upload Document </label><br>
                    <input type="file" class='form-control' required name="file" accept=".pdf, .jpg, .png, jpeg" /><br>
                    <button style="float: right;" type="submit" class="btn btn-primary btn-offset text-block" name="Add" value="Add">Add </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="Update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelUpdate" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabelUpdate">Update Document </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{url('update-application-file')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block update_file_name' name="file_name" value="{{$val}}">
                    <input type="hidden" class='text-block update_application_id' name="application_id">
                    <input type="hidden" class='text-block update_teacher_id' name="teacher_id" />
                    <input type="hidden" class='text-block update_filed_name' name="field_name" />
                    <label>Upload Document </label><br>
                    <input type="file" class='form-control' required name="file" accept=".pdf, .jpg, .png, jpeg" /><br>
                    <button style="float: right;" type="submit" class="btn btn-primary btn-offset text-block" name="Update" value="Update">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript" src="http://docs.jquery.com/Plugins/livequery"></script> -->
<script>
    $(document).ready(function () {
        var value = $('.school_validation_till').val();
        if (value == 1) {
            $('.hideValid').hide();
        } else {
            $('.hideValid').show();
        }
        $('.school_validation_till').on('change', function () {
            var value = $(this).val();
            // alert(value);
            if (value == 1) {
                $('.hideValid').hide();
            } else {
                $('.hideValid').show();
            }
        });
        // $('#evidence_of_non_proprietary_nature_files').on('change', function () {
        //     var file = $("#evidence_of_non_proprietary_nature_files").val();
        //     var value = $("#evidence_of_non_proprietary_nature_ex_check").val();
        //     if (file != '' || file != null) {
        //         $("#evidence_of_non_proprietary_nature_ex_check").val(1);
        //     } else {
        //         $("#evidence_of_non_proprietary_nature_ex_check").val(0);
        //     }
        // });


        // $('.file_upload').on('click', function () {
        //     var file = $(this).closest('div').find(".file_upload").val();
        //     var value = $(this).closest('div').find(".file_upload_check").val();
        //     alert(file);
        //     console.log(file);

        //     alert(value);
        //     if (file != '' || file != null) {
        //         $(this).closest('div').find(".file_upload_check").val(1);
        //         value = $(this).closest('div').find(".file_upload_check").val();
        //         alert('if = '+value);
        //     } else {
        //         $(this).closest('div').find(".file_upload_check").val(0);
        //          value = $(this).closest('div').find(".file_upload_check").val();
        //          alert('else ='+value);
        //     }
        // });
        // $('.file_upload').on('change', function () {
        //     var file = $(this).closest('div').find(".file_upload").val();
        //     var value = $(this).closest('div').find(".file_upload_check").val();
        //     alert(file);
        //     alert(value);
        //     if (file != '' || file != null) {
        //         $(this).closest('div').find(".file_upload_check").val(1);
        //         value = $(this).closest('div').find(".file_upload_check").val();
        //         alert('if = '+value);
        //     } else {
        //         $(this).closest('div').find(".file_upload_check").val(0);
        //          value = $(this).closest('div').find(".file_upload_check").val();
        //          alert('else ='+value);
        //     }
        // });
        $('.AddFile').on('click', function () {
            //alert('AddFile');
            var file_name = $(this).closest("div").find(".file_name").val();
            var application_id = $(this).closest("div").find(".application_id").val();
            var field_name = $(this).closest("div").find(".field_name").val();
            var teacher_id = $(this).closest("div").find(".teacher_id").val();
            console.log(teacher_id);
            $('.insert_application_id').val(application_id);
            $('.insert_filed_name').val(field_name);
            $('.insert_teacher_id').val(teacher_id);
        });
        $('.UpdateFile').on('click', function () {
            //alert('UpdateFile');
            var file_name = $(this).closest("div").find(".file_name").val();
            var application_id = $(this).closest("div").find(".application_id").val();
            var field_name = $(this).closest("div").find(".field_name").val();
            var teacher_id = $(this).closest("div").find(".teacher_id").val();
            console.log(teacher_id);
            $('.update_file_name').val(file_name);
            $('.update_application_id').val(application_id);
            $('.update_filed_name').val(field_name);
            $('.update_teacher_id').val(teacher_id);
        });
        if ($("input:radio[name='is_school_has_boundary'][value='Yes']").is(":checked"))
            $('.is_school_has_boundary_doc').show();
        if ($("input:radio[name='is_school_has_boundary'][value='No']").is(":checked"))
            $('.is_school_has_boundary_doc').hide();
        $("input:radio[name='is_school_has_boundary']").click(function () {
            if ($(this).val() == 'Yes')
                $('.is_school_has_boundary_doc').show();
            else
                $('.is_school_has_boundary_doc').hide();
        });
        if ($("input:radio[name='is_school_cctv'][value='Yes']").is(":checked"))
            $('.is_school_cctv_doc').show();
        if ($("input:radio[name='is_school_cctv'][value='No']").is(":checked"))
            $('.is_school_cctv_doc').hide();
        $("input:radio[name='is_school_cctv']").click(function () {
            if ($(this).val() == 'Yes')
                $('.is_school_cctv_doc').show();
            else
                $('.is_school_cctv_doc').hide();
        });
        if ($("input:radio[name='is_school_ground'][value='Yes']").is(":checked"))
            $('.is_school_ground_doc').show();
        if ($("input:radio[name='is_school_ground'][value='No']").is(":checked"))
            $('.is_school_ground_doc').hide();
        $("input:radio[name='is_school_ground']").click(function () {
            if ($(this).val() == 'Yes')
                $('.is_school_ground_doc').show();
            else
                $('.is_school_ground_doc').hide();
        });
        if ($("input:radio[name='is_school_water_hygene_facilities'][value='Yes']").is(":checked"))
            $('.is_school_water_hygene_facilities_doc').show();
        if ($("input:radio[name='is_school_water_hygene_facilities'][value='No']").is(":checked"))
            $('.is_school_water_hygene_facilities_doc').hide();
        $("input:radio[name='is_school_water_hygene_facilities']").click(function () {
            if ($(this).val() == 'Yes')
                $('.is_school_water_hygene_facilities_doc').show();
            else
                $('.is_school_water_hygene_facilities_doc').hide();
        });
        if ($("input:radio[name='is_school_fire_safty_facilities'][value='Yes']").is(":checked"))
            $('.is_school_fire_safty_facilities_doc').show();
        if ($("input:radio[name='is_school_fire_safty_facilities'][value='No']").is(":checked"))
            $('.is_school_fire_safty_facilities_doc').hide();
        $("input:radio[name='is_school_fire_safty_facilities']").click(function () {
            if ($(this).val() == 'Yes')
                $('.is_school_fire_safty_facilities_doc').show();
            else
                $('.is_school_fire_safty_facilities_doc').hide();
        });
        $('#income_1').on('keyup', function () {
            var income_1 = $('#income_1').val()
            var expenses_1 = $('#expenses_1').val()
            income_1 = (income_1 == '' || income_1 == null) ? 0 : parseFloat(income_1, 3);
            expenses_1 = (expenses_1 == '' || expenses_1 == null) ? 0 : parseFloat(expenses_1, 3);
            if (income_1 > expenses_1) {
                surplus_money = income_1 - expenses_1;
                // alert(surplus_money);
                $('#surplus_money_1').val(surplus_money);
                $('#reduced_money_1').val(0);
            } else if (income_1 < expenses_1) {
                reduced_money = income_1 - expenses_1;
                // alert(reduced_money);
                $('#surplus_money_1').val(0);
                $('#reduced_money_1').val(reduced_money);
            }
        });
        $('#expenses_1').on('keyup', function () {
            var income_1 = $('#income_1').val()
            var expenses_1 = $('#expenses_1').val()
            income_1 = (income_1 == '' || income_1 == null) ? 0 : parseFloat(income_1, 3);
            expenses_1 = (expenses_1 == '' || expenses_1 == null) ? 0 : parseFloat(expenses_1, 3);
            if (income_1 > expenses_1) {
                surplus_money = income_1 - expenses_1;
                // alert(surplus_money);
                $('#surplus_money_1').val(surplus_money);
                $('#reduced_money_1').val(0);
            } else if (income_1 < expenses_1) {
                reduced_money = income_1 - expenses_1;
                // alert(reduced_money);
                $('#surplus_money_1').val(0);
                $('#reduced_money_1').val(reduced_money);
            }
        });
        $('#income_2').on('keyup', function () {
            var income_2 = $('#income_2').val()
            var expenses_2 = $('#expenses_2').val()
            income_2 = (income_2 == '' || income_2 == null) ? 0 : parseFloat(income_2, 3);
            expenses_2 = (expenses_2 == '' || expenses_2 == null) ? 0 : parseFloat(expenses_2, 3);
            if (income_2 > expenses_2) {
                surplus_money = income_2 - expenses_2;
                // alert(surplus_money);
                $('#surplus_money_2').val(surplus_money);
                $('#reduced_money_2').val(0);
            } else if (income_2 < expenses_2) {
                reduced_money = income_2 - expenses_2;
                // alert(reduced_money);
                $('#surplus_money_2').val(0);
                $('#reduced_money_2').val(reduced_money);
            }
        });
        $('#expenses_2').on('keyup', function () {
            var income_2 = $('#income_2').val()
            var expenses_2 = $('#expenses_2').val()
            income_2 = (income_2 == '' || income_2 == null) ? 0 : parseFloat(income_2, 3);
            expenses_2 = (expenses_2 == '' || expenses_2 == null) ? 0 : parseFloat(expenses_2, 3);
            if (income_2 > expenses_2) {
                surplus_money = income_2 - expenses_2;
                // alert(surplus_money);
                $('#surplus_money_2').val(surplus_money);
                $('#reduced_money_2').val(0);
            } else if (income_2 < expenses_2) {
                reduced_money = income_2 - expenses_2;
                // alert(reduced_money);
                $('#surplus_money_2').val(0);
                $('#reduced_money_2').val(reduced_money);
            }
        });
        $('#income_3').on('keyup', function () {
            var income_3 = $('#income_3').val()
            var expenses_3 = $('#expenses_3').val()
            income_3 = (income_3 == '' || income_3 == null) ? 0 : parseFloat(income_3, 3);
            expenses_3 = (expenses_3 == '' || expenses_3 == null) ? 0 : parseFloat(expenses_3, 3);
            if (income_3 > expenses_3) {
                surplus_money = income_3 - expenses_3;
                // alert(surplus_money);
                $('#surplus_money_3').val(surplus_money);
                $('#reduced_money_3').val(0);
            } else if (income_3 < expenses_3) {
                reduced_money = income_3 - expenses_3;
                // alert(reduced_money);
                $('#surplus_money_3').val(0);
                $('#reduced_money_3').val(reduced_money);
            }
        });
        $('#expenses_3').on('keyup', function () {
            var income_3 = $('#income_3').val()
            var expenses_3 = $('#expenses_3').val()
            income_3 = (income_3 == '' || income_3 == null) ? 0 : parseFloat(income_3, 3);
            expenses_3 = (expenses_3 == '' || expenses_3 == null) ? 0 : parseFloat(expenses_3, 3);
            if (income_3 > expenses_3) {
                surplus_money = income_3 - expenses_3;
                // alert(surplus_money);
                $('#surplus_money_3').val(surplus_money);
                $('#reduced_money_3').val(0);
            } else if (income_3 < expenses_3) {
                reduced_money = income_3 - expenses_3;
                // alert(reduced_money);
                $('#surplus_money_3').val(0);
                $('#reduced_money_3').val(reduced_money);
            }
        });
        function bootstrapTabControl() {
            var i, items = $('.ui-tabs-panel');
            console.log(items.length)
            pane = $('.tab-pane');
            $('.prevtab').hide();
            $('.final_submit_button').hide();
            $('.first_tab').on('click', function () {
                //alert('first_tab');
                $('.prevtab').hide();
                $('.nexttab').show();
                $('.final_submit_button').hide();
            });
            $('.last_tab').on('click', function () {
                //alert('last_tab');
                $('.nexttab').hide();
                $('.final_submit_button').show();
            });
            $('.middle_tab').on('click', function () {
                //alert('middle_tab');
                $('.nexttab').show();
                $('.prevtab').show();
                $('.final_submit_button').hide();
            });
            $('.nexttab').on('click', function () {
                for (i = 0; i < items.length; i++) {
                    if ($(items[i]).hasClass('active') == true) {
                        if (i == 7) {
                            $('.nexttab').hide();
                            $('.final_submit_button').show();
                        }
                        break;
                    }
                }
                if (i < items.length - 1) {
                    $(items[i]).removeClass('active');
                    $(items[i + 1]).addClass('active');
                    // for pane
                    $(pane[i]).removeClass('show active');
                    $(pane[i + 1]).addClass('show active');
                    if ($(pane[i + 1]).hasClass('last') == true) {
                        $('.nexttab').hide();
                        $('.final_submit_button').show();
                    } else {
                        $('.prevtab').show();
                    }
                }
            });
            $('.prevtab').on('click', function () {
                $('.final_submit_button').hide();
                for (i = 0; i < items.length; i++) {
                    if ($(items[i]).hasClass('active') == true) {
                        if (i == 1) {
                            $('.prevtab').hide();
                        }
                        break;
                    }
                }
                if (i != 0) {
                    // for tab
                    $(items[i]).removeClass('active');
                    $(items[i - 1]).addClass('active');
                    // for pane
                    $(pane[i]).removeClass('show active');
                    $(pane[i - 1]).addClass('show active');
                }
                if ($(pane[i - 1]).hasClass('first') == true) {
                    $('.prevtab').hide();
                } else {
                    $('.nexttab').show();
                }
            });
        }
        bootstrapTabControl();
    });
</script>
<script>
    var i = 2;
    function AddclassIndv() {
        // var inp = $('#clsAdd');
        var html = `<div class="class_rows"><div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Class `+ i + `</label>
                                    <input  type="text" maxlength="11" name="className[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Number Of Section</label>
                                    <input  type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                    
                                    
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Number Of Students</label>
                                    <input  type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                    
                                    
                                    </div>
                            </div>
                            <div class="row">
                            <button type="button" class="btn btn-danger mx-3" onclick="remove_class_row(this)">Remove</button>
                            </div>
                        </div></div>`;
        // $('<div id="clsAdd" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input  type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input  type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input  type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" onclick="remove_class_row(this)" id="remove1">Remove </a></div></div>').appendTo(inp);
        $(".add_class_row").append(html);
        i++;
        if (i > 8) {
            document.getElementById('addClass').style.visibility = 'hidden';
        }
    }
    function remove_class_row(e) {
        console.log(e);
        $(e).parents('.class_rows').remove();
        i--;
        if (i <= 8) {
            document.getElementById('addClass').style.visibility = 'visible';
        }
    }
    // $(document).ready(function () {
    //     // $('#addClass').onclick(function () {
    //     // });
    //     $('body').on('click', '#remove1', function () {
    //         i--;
    //         if (i <= 8) {
    //             // document.getElementById('addClass').style.visibility = 'visible';
    //         }
    //         console.log(i);
    //         $(this).parent('div').remove();
    //         var teacherDataId = i - 1;
    //         document.getElementById("hiddenTeacherId").value = teacherDataId;
    //     });
    // });
</script>
<script>
    var j = 2;
    i = <?php echo $Last_index ?>;
    function teacherRowAdd() {
        // var inp = $('#clsAdd');
        var html = `<div class="removeTeacherRow"><div class="row pt-3 px-3">
                        <div class="col-md-12">
                            <h5> Teacher Information </h5>
                        </div>
                        <hr>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Name Of Teacher</label>
                                <input  type="hidden"  name="teacher_id[]" id="teacher_id" class="form-control rte_input">
                                <input  type="text" name="teacher_name[]" id="teacher_name" class="form-control rte_input">
                                     
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Father / Husband Or Wife Name</label>
                                <input  type="text" name="teacher_f_h_w_name[]" id="teacher_f_h_w_name" class="form-control rte_input">
                                     
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Date Of Birth</label>
                                <input  type="date" name="teacher_date_of_birth[]" id="teacher_date_of_birth" class="form-control rte_input">
                                      
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Educational Qualification</label>
                                <input  type="text" name="teacher_education_qualification[]" id="teacher_education_qualification" class="form-control rte_input">
                                     
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Trainee Qualification</label>
                                <input  type="text" name="teacher_trainee_qualification[]" id="teacher_trainee_qualification" class="form-control rte_input">
                                     
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Teaching Experience</label>
                                <input  type="text" name="teacher_teaching_experience[]" id="teacher_teaching_experience" class="form-control rte_input">
                                      
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Class Handed Over</label>
                                <input  type="text" name="teacher_class_handed_over[]" id="teacher_class_handed_over" class="form-control rte_input">
                                    
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Date Of Appointment</label>
                                <input  type="date" name="teacher_appointment_date[]" id="teacher_appointment_date" class="form-control rte_input">
                                    
                                </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Trained Or Untrained</label>
                                <input  type="text" name="teacher_trained_or_untrained[]" id="teacher_trained_or_untrained" class="form-control rte_input">
                                     
                                </div>
                        </div>
                        <div class="col-md-6">
                                                  
                            <div class="form-group">
                                <label for="">Add relevant evidence attachement</label>
                                <input type="file" id="teacher_educational_qualification_files" name="teacher_educational_qualification_files[`+ i + `][]" multiple class="file_upload form-control teacher_educational_qualification_files" accept=".pdf, .jpg, .png, jpeg" />
                                <input type="hidden" class=" file_upload_check form-control rte_input teacher_educational_qualification_files_check" name="teacher_educational_qualification_files_check[]" value="0" />
                                             
                            </div>
                        </div>
  
                          <div class="col-md-3 text-right pt-4">
                            <div>
                                <a href="javascript:void(0)" class="btn btn-danger" onclick="remove_teacher_row(this)" id="remove">Remove</a>
                            </div>
                        </div>
                    </div>`;
        // $('<div id="clsAdd" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input  type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input  type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input  type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" onclick="remove_class_row(this)" id="remove1">Remove </a></div></div>').appendTo(inp);
        $(".addTeacherRow").append(html);
        i++;
    }

    function remove_teacher_row(e) {
        var teacher_id = $(e).parents('.removeTeacherRow').closest("div").find("#teacher_id").val().trim();
        var q = confirm('Are you want to sure delete data ??');
        if (q == true) {
            if (teacher_id != '' || teacher_id != null) {
                if (teacher_id.length != 0) {

                    $.ajax({
                        url: 'delete-teacher/' + teacher_id,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            console.log('success ');
                            console.log(data);
                            $(e).parents('.removeTeacherRow').remove();
                            i--;
                        }
                    });
                } else {
                    $(e).parents('.removeTeacherRow').remove();
                    i--;
                }
            } else {
                $(e).parents('.removeTeacherRow').remove();
                i--;
            }

        }
    }
    // $(document).ready(function () {
    //     var i = 2;
    //     $('#add').click(function () {
    //         var inp = $('#dynamic_form_create');
    //         $('<div class="row pt-3 px-3" id="dynamic_form_create' + i + '"><div class="col-md-12"><h5> Teacher Information </h5></div><hr><div class="col-md-3"><div class="form-group"><label for="">Name Of Teacher</label><input  type="text" name="teacher_name' + i + '" id="teacher_name" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Father / Husband Or Wife Name</label><input  type="text" name="teacher_f_h_w_name' + i + '" id="teacher_f_h_w_name" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Date Of Birth</label><input  type="date" name="teacher_date_of_birth' + i + '" id="teacher_date_of_birth" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Educational Qualification</label><input  type="text" name="teacher_education_qualification' + i + '" id="teacher_education_qualification" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Trainee Qualification</label><input  type="text" name="teacher_trainee_qualification' + i + '" id="teacher_trainee_qualification" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Teaching Experience</label><input  type="text" name="teacher_teaching_experience' + i + '" id="teacher_teaching_experience" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Class Handed Over</label><input  type="text" name="teacher_class_handed_over' + i + '" id="teacher_class_handed_over" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Date Of Appointment</label><input  type="date" name="teacher_appointment_date' + i + '" id="teacher_appointment_date" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Trained Or Untrained</label><input  type="text" name="teacher_trained_or_untrained' + i + '" id="teacher_trained_or_untrained" class="form-control rte_input"></div></div><div class="col-md-3"></div><div class="col-md-3"></div><div class="col-md-3 text-right pt-4"></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" id="remove">Remove </a></div></div>').appendTo(inp);
    //         i++;
    //         var teacherDataId = i - 1;
    //         document.getElementById("hiddenTeacherId").value = teacherDataId;
    //     });
    //     $('body').on('click', '#remove', function () {
    //         i--;
    //         $(this).parent('div').remove();
    //         var teacherDataId = i - 1;
    //         document.getElementById("hiddenTeacherId").value = teacherDataId;
    //     });
    // });
</script>
<script>
    var k = 2;
    function addcurrandsyllabus() {
        // var inp = $('#clsAdd');
        var html = `<div class="curr_syllabus_row">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6)</label>
                                    <input  value="" type="text" maxlength="100" name="details_of_curriculum[]" class="form-control rte_input">
                                    @if ($errors->has('details_of_curriculum.*'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('details_of_curriculum.*') }}</strong>
                                            </span>
                                            @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Method Of Inspection Of Students</label>
                                    <input  value="" type="text" maxlength="100" name="method_of_inspection[]" class="form-control rte_input">
                                    @if ($errors->has('method_of_inspection.*'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('method_of_inspection.*') }}</strong>
                                            </span>
                                            @endif
                                </div>
                            </div>
                            <div class="col-md-2 pt-4">
                                <a href="javascript:void(0)" class="btn btn-danger" onclick="remove_curr_syllabus_row(this)" >Remove</a>
                            </div> 
                        </div></div>`;
        // $('<div id="clsAdd" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input  type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input  type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input  type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" onclick="remove_class_row(this)" id="remove1">Remove </a></div></div>').appendTo(inp);
        $(".add_currandsyllabus_row").append(html);
        k++;
        if (k > 10) {
            document.getElementById('addx').style.visibility = 'hidden';
        }
    }
    function remove_curr_syllabus_row(e) {
        $(e).parents('.curr_syllabus_row').remove();
        k--;
        if (k <= 10) {
            document.getElementById('addClass').style.visibility = 'visible';
        }
    }
</script>
<script type="text/javascript">
    function clsFeeSearch() {
        var fromCls = document.getElementById("from_finance_cls");
        var toCls = document.getElementById("to_finance_cls");
        var selectedText = fromCls.options[fromCls.selectedIndex].innerHTML;
        var fromcls = fromCls.value;
        var tocls = toCls.value;
        // var html = '';
        if (fromCls == -3) {
            var from = 0.1;
        } else if (fromCls == -2) {
            var from = 0.2;
        } else if (fromCls == -1) {
            var from = 0.3;
        } else {
            var from = fromCls;
        }
        if (tocls == -3) {
            var to = 0.1;
        } else if (tocls == -2) {
            var to = 0.2;
        } else if (tocls == -1) {
            var to = 0.3;
        } else {
            var to = tocls;
        }
        if (fromcls == '') {
            alert("Please choose the fromcls");
        }
        else if (tocls == '') {
            alert("Please choose the tocls");
        }
        else if (fromCls < tocls) {
            alert("to class in greter than from class !!! ");
        }
        else {
            $(".add_class_row").html('');
            var html = '';
            if (tocls == -3 || tocls == -2 || tocls == -1) {
                if (tocls == -3) {
                    y = 'Pre Elementery';
                    html = ` <div class="row">
                            <lable><b> `+ y + `</b></lable>
                            <input type="hidden" maxlength="48" value="`+ y + `" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                        </div>
                        <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>`;
                    $(".add_class_fee_row").append(html);
                }
                if (tocls == -2) {
                    y = 'Nursery';
                    html = `
                    <div class="row">
                            <lable><b> Pre Elementery</b></lable>
                            <input type="hidden" maxlength="48" value="Pre Elementery" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                        </div>
                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <lable><b> ` + y + `</b></lable>
                                        <input type="hidden" maxlength="48" value="` + y + `" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>
                                    `;
                    $(".add_class_fee_row").append(html);
                }
                if (tocls == -1) {
                    y = 'Kindergarten';
                    html = `
                    <div class="row">
                            <lable><b> Pre Elementery</b></lable>
                            <input type="hidden" maxlength="48" value="Pre Elementery" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                        </div>
                        <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>
                        <div class="row">
                            <lable><b> Nursery</b></lable>
                            <input type="hidden" maxlength="48" value="Nursery" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                        </div>
                        <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>
                        <div class="row">
                            <lable><b> ` + y + `</b></lable>
                            <input type="hidden" maxlength="48" value="` + y + `" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                        </div>
                        <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>
                                    `;
                    $(".add_class_fee_row").append(html);
                }
            } else {
                for (i = fromcls; i <= tocls; i++) {
                    var y;
                    console.log('loop = ' + i);
                    if (i == 0) {
                        continue;
                    }
                    if (i == -3) {
                        y = 'Pre Elementery';
                    }
                    if (i == -2) {
                        y = 'Nursery';
                    }
                    if (i == -1) {
                        y = 'Kindergarten';
                    }
                    if (i == 1) {
                        y = 'Class 1';
                    }
                    if (i == 2) {
                        y = 'Class 2';
                    }
                    if (i == 3) {
                        y = 'Class 3';
                    }
                    if (i == 4) {
                        y = 'Class 4';
                    }
                    if (i == 5) {
                        y = 'Class 5';
                    }
                    if (i == 6) {
                        y = 'Class 6';
                    }
                    if (i == 7) {
                        y = 'Class 7';
                    }
                    if (i == 8) {
                        y = 'Class 8';
                    }
                    // if (i !== "") {
                    //     document.getElementById('remove1').style.visibility = 'none';
                    // }
                    html += `<div class="row">
                            <lable><b> `+ y + `</b></lable>
                            <input type="hidden" maxlength="48" value="`+ y + `" id="class_finance" name="class_finance[]" class="form-control rte_input class_finance">
                        </div>
                            <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Tuition Fee </label>
                                                <input type="text" maxlength="48" id="teaching_fee`+ i + `" name="teaching_fee[]" class="form-control rte_input teaching_fee">
                                                <span class="msg" id="teaching_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Registration Fee </label>
                                                <input type="text" maxlength="48"  id="registration_fee" name="registration_fee[]" class="form-control rte_input registration_fee">
                                                <span class="msg" id="registration_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Admission Fee </label>
                                                <input type="text" maxlength="48"  id="enrollment_fee" name="enrollment_fee[]" class="form-control rte_input enrollment_fee">
                                                <span class="msg" id="enrollment_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Reserve Deposite </label>
                                                <input type="text" maxlength="48"  id="security_money" name="security_money[]" class="form-control rte_input security_money">
                                                <span class="msg" id="security_money_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Development Fee </label>
                                                <input type="text" maxlength="48"  id="development_fee" name="development_fee[]" class="form-control rte_input development_fee">
                                                <span class="msg" id="development_fee_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Annual Charges </label>
                                                <input type="text" maxlength="48"  id="annual_charge" name="annual_charge[]" class="form-control rte_input annual_charge">
                                                <span class="msg" id="annual_charge_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Other Charges </label>
                                                <input type="text" maxlength="48"  id="other_charges" name="other_charges[]" class="form-control rte_input other_charges">
                                                <span class="msg" id="other_charges_id"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Total </label>
                                                <input type="text" maxlength="48"  id="total" name="total[]" class="form-control rte_input total">
                                                <span class="msg" id="total_id"></span>
                                            </div>
                                        </div>
                                    </div>`;
                }
            }
            // alert("From Class: " + fromcls + " To Class: " + tocls);
            // $('body').on('click', '#remove1', function () {
            //     $(this).parent('div').remove();
            // });
        }
        $(".add_class_fee_row").html(html);
    }
    $(document).ready(function () {
        $('#teaching_fee2').on('blur', function () {
            console.log('data');
        });
        $('.teaching_fee').on('keyup', function () {
            console.log("fxgfg");
            var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val();
            console.log(teaching_fee);
            var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val();
            var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val();
            var security_money = $(this).parent().parent().parent().find('.security_money').val();
            var development_fee = $(this).parent().parent().parent().find('.development_fee').val();
            var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val();
            var other_charges = $(this).parent().parent().parent().find('.other_charges').val();
            var total = $(this).parent().parent().parent().find('.total').val();
            teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
            registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
            enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
            security_money = (security_money == '' || security_money == null) ? 0 : security_money;
            development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
            annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
            other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
            total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
            $(this).parent().parent().parent().find('.total').val(total);
        });
    });
</script>
<script type="text/javascript">
    function clsSearch() {
        var fromCls = document.getElementById("from_cls");
        var toCls = document.getElementById("to_cls");
        var selectedText = fromCls.options[fromCls.selectedIndex].innerHTML;
        var fromcls = fromCls.value;
        var tocls = toCls.value;
        if (fromCls == -3) {
            var from = 0.1;
        } else if (fromCls == -2) {
            var from = 0.2;
        } else if (fromCls == -1) {
            var from = 0.3;
        } else {
            var from = fromCls;
        }
        if (tocls == -3) {
            var to = 0.1;
        } else if (tocls == -2) {
            var to = 0.2;
        } else if (tocls == -1) {
            var to = 0.3;
        } else {
            var to = tocls;
        }
        if (fromcls == '') {
            alert("Please choose the fromcls");
        }
        else if (tocls == '') {
            alert("Please choose the tocls");
        }
        else if (fromCls < tocls) {
            alert("to class in greter than from class !!! ");
        }
        else {
            $(".add_class_row").html('');
            if (tocls == -3 || tocls == -2 || tocls == -1) {
                if (tocls == -3) {
                    y = 'Pre Elementery';
                    html = `<div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">` + y + `</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="` + y + `" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                    
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                    
                                    </div>
                                </div>
                                <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>`;
                    $(".add_class_row").append(html);
                }
                if (tocls == -2) {
                    y = 'Nursery';
                    html = `<div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Pre Elementery</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="Pre Elementery" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                 <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">` + y + `</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="` + y + `" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                 <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>`;
                    $(".add_class_row").append(html);
                }
                if (tocls == -1) {
                    y = 'Kindergarten';
                    html = `<div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Pre Elementery</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="Pre Elementery" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                 <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Nursery</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="Nursery" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                 <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">` + y + `</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="` + y + `" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                    
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                    
                                        </div>
                                </div>
                                <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>`;
                    $(".add_class_row").append(html);
                }
            } else {
                for (i = fromcls; i <= tocls; i++) {
                    var y;
                    if (i == 0) {
                        continue;
                    }
                    if (i == -3) {
                        y = 'Pre Elementery';
                    }
                    if (i == -2) {
                        y = 'Nursery';
                    }
                    if (i == -1) {
                        y = 'Kindergarten';
                    }
                    if (i == 1) {
                        y = 'Class 1';
                    }
                    if (i == 2) {
                        y = 'Class 2';
                    }
                    if (i == 3) {
                        y = 'Class 3';
                    }
                    if (i == 4) {
                        y = 'Class 4';
                    }
                    if (i == 5) {
                        y = 'Class 5';
                    }
                    if (i == 6) {
                        y = 'Class 6';
                    }
                    if (i == 7) {
                        y = 'Class 7';
                    }
                    if (i == 8) {
                        y = 'Class 8';
                    }
                    // if (i !== "") {
                    //     document.getElementById('remove1').style.visibility = 'none';
                    // }
                    html = `<div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">` + y + `</label>
                                        <input type="text" maxlength="11" name="className[]" readonly value="` + y + `" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Section</label>
                                        <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                      
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Number Of Students</label>
                                        <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Teacher Student Ratio</label>
                                        <input type="text" maxlength="10" name="teacher_student_ratio[]" class="form-control rte_input">
                                     
                                        </div>
                                </div>
                                 <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Teacher Assigned </label>
                                                            <input type="text" name="teacher_student_assign[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Number Of Teacher </label>
                                                            <input type="text" name="num_of_teacher[]" class="form-control rte_input">
                                                        </div>
                                                    </div>
                        </div>`;
                    $(".add_class_row").append(html);
                }
            }
            // alert("From Class: " + fromcls + " To Class: " + tocls);
            // $('body').on('click', '#remove1', function () {
            //     $(this).parent('div').remove();
            // });
        }
    }
</script>
<script>
    $('.teaching_fee').on('keyup', function () {
        console.log("fxgfg");
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val();
        console.log(teaching_fee);
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val();
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val();
        var security_money = $(this).parent().parent().parent().find('.security_money').val();
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val();
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val();
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val();
        var total = $(this).parent().parent().parent().find('.total').val();
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
    $('.registration_fee').on('keyup', function () {
        console.log("dfghf");
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val()
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val()
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val()
        var security_money = $(this).parent().parent().parent().find('.security_money').val()
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val()
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val()
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val()
        var total = $(this).parent().parent().parent().find('.total').val()
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
    $('.enrollment_fee').on('keyup', function () {
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val()
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val()
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val()
        var security_money = $(this).parent().parent().parent().find('.security_money').val()
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val()
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val()
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val()
        var total = $(this).parent().parent().parent().find('.total').val()
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
    $('.security_money').on('keyup', function () {
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val()
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val()
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val()
        var security_money = $(this).parent().parent().parent().find('.security_money').val()
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val()
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val()
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val()
        var total = $(this).parent().parent().parent().find('.total').val()
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
    $('.development_fee').on('keyup', function () {
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val()
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val()
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val()
        var security_money = $(this).parent().parent().parent().find('.security_money').val()
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val()
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val()
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val()
        var total = $(this).parent().parent().parent().find('.total').val()
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
    $('.annual_charge').on('keyup', function () {
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val()
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val()
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val()
        var security_money = $(this).parent().parent().parent().find('.security_money').val()
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val()
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val()
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val()
        var total = $(this).parent().parent().parent().find('.total').val()
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
    $('.other_charges').on('keyup', function () {
        var teaching_fee = $(this).parent().parent().parent().find('.teaching_fee').val()
        var registration_fee = $(this).parent().parent().parent().find('.registration_fee').val()
        var enrollment_fee = $(this).parent().parent().parent().find('.enrollment_fee').val()
        var security_money = $(this).parent().parent().parent().find('.security_money').val()
        var development_fee = $(this).parent().parent().parent().find('.development_fee').val()
        var annual_charge = $(this).parent().parent().parent().find('.annual_charge').val()
        var other_charges = $(this).parent().parent().parent().find('.other_charges').val()
        var total = $(this).parent().parent().parent().find('.total').val()
        teaching_fee = (teaching_fee == '' || teaching_fee == null) ? 0 : teaching_fee;
        registration_fee = (registration_fee == '' || registration_fee == null) ? 0 : registration_fee;
        enrollment_fee = (enrollment_fee == '' || enrollment_fee == null) ? 0 : enrollment_fee;
        security_money = (security_money == '' || security_money == null) ? 0 : security_money;
        development_fee = (development_fee == '' || development_fee == null) ? 0 : development_fee;
        annual_charge = (annual_charge == '' || annual_charge == null) ? 0 : annual_charge;
        other_charges = (other_charges == '' || other_charges == null) ? 0 : other_charges;
        total = parseFloat(teaching_fee, 3) + parseFloat(registration_fee, 3) + parseFloat(enrollment_fee, 3) + parseFloat(security_money, 3) + parseFloat(development_fee, 3) + parseFloat(annual_charge, 3) + parseFloat(other_charges, 3);
        $(this).parent().parent().parent().find('.total').val(total);
    });
</script>


<script>
    var button_val = 0;
    $('.btnsavesubmit').click(function () {
        console.log($(this).val());
        button_val = $(this).val();
    });
    function validation() {
        if (button_val == 3) {
            var count = 0;
            var x = y = f_name = '';
            // alert('save data ');
            //  Tab 1
            f_name = 'school_name';
            x = document.forms["application_form"]["school_name"].value;
            y = 'School Name';
            count += title_validate(x, y, f_name);
            f_name = 'recognised_by';
            x = document.forms["application_form"]["recognised_by"].value;
            y = 'Recognised By';
            count += title_validate(x, y, f_name);
            f_name = 'post_office';
            x = document.forms["application_form"]["post_office"].value;
            y = 'Post Office';
            count += title_validate(x, y, f_name);
            f_name = 'village_town';
            x = document.forms["application_form"]["village_town"].value;
            y = 'Village / Town';
            count += title_validate(x, y, f_name);
            f_name = 'pin_code';
            x = document.forms["application_form"]["pin_code"].value;
            y = 'Pin Code ';
            count += title_validate(x, y, f_name);
            f_name = 'phone_with_std_code';
            x = document.forms["application_form"]["phone_with_std_code"].value;
            y = 'Phone /Mobile No. With STD Code';
            count += title_validate(x, y, f_name);
            f_name = 'fax_with_std_code';
            x = document.forms["application_form"]["fax_with_std_code"].value;
            y = 'FAX No. With STD Code';
            count += title_validate(x, y, f_name);
            f_name = 'email';
            x = document.forms["application_form"]["email"].value;
            y = 'E-mail';
            count += title_validate(x, y, f_name);
            f_name = 'police_station';
            x = document.forms["application_form"]["police_station"].value;
            y = 'Nearest Police Station';
            count += title_validate(x, y, f_name);
            // Tab 2
            f_name = 'estd_year';
            x = document.forms["application_form"]["estd_year"].value;
            y = 'Establishment Year ';
            count += title_validate(x, y, f_name);
            f_name = 'school_validation_till';
            x = document.forms["application_form"]["school_validation_till"].value;
            y = 'School Validation Till ';
            count += title_validate(x, y, f_name);
            f_name = 'opening_date';
            x = document.forms["application_form"]["opening_date"].value;
            y = 'Opening Date ';
            count += title_validate(x, y, f_name);
            f_name = 'society_name';
            x = document.forms["application_form"]["society_name"].value;
            y = 'Society Name ';
            count += title_validate(x, y, f_name);
            f_name = 'is_society_registered';
            x = document.forms["application_form"]["is_society_registered"].value;
            y = 'Is Society Registered';
            count += title_validate(x, y, f_name);

            var selectedOption = $('.school_validation_till').val();
            if (selectedOption == '2' || selectedOption == 2) {
                f_name = 'society_registration_valid_upto';
                x = document.forms["application_form"]["society_registration_valid_upto"].value;
                y = 'Society Registration Valid Upto';
                count += title_validate(x, y, f_name);
            }
            f_name = 'evidence_of_non_proprietary_nature';
            x = document.forms["application_form"]["evidence_of_non_proprietary_nature"].value;
            y = 'Evidence Of Non Proprietary Nature';
            count += title_validate(x, y, f_name);
            f_name = 'evidence_of_non_proprietary_nature_ex_check';
            x_chexk = document.forms["application_form"]["evidence_of_non_proprietary_nature_ex_check"].value;
            y = 'Evidence Of Non Proprietary Nature File';
            if (x_chexk == 1) {
                count += 0;
                $('#evidence_of_non_proprietary_nature_ex_check_id').text('');
                $('#evidence_of_non_proprietary_nature_ex_check_id').hide('');
            } else {
                x = document.forms["application_form"]["evidence_of_non_proprietary_nature_files"].value;

                count += title_validate(x, y, f_name);
            }


            f_name = 'teacher_educational_qualification_files_check';
            y = 'Add Evidence Of Teacher Educational Qualification Files ';

            var chks = document.getElementsByName('teacher_educational_qualification_files_check[]');
            // var chks = document.getElementsByName('teacher_educational_qualification_files_check[]');
            for (var i = 0; i < chks.length; i++) {
                //    val_file = document.forms["application_form"]["teacher_educational_qualification_files_check[]"].value;
                val_file = chks[i].value;
                //    alert(val_file);
                if (val_file == 0 || val_file == '0') {

                    x = document.forms["application_form"]["teacher_educational_qualification_files"].value;
                    count += title_validate(x, y, f_name);
                }
            }

            f_name = 'last_three_year_tot_income_files_check_1';
            x_chexk = document.forms["application_form"]["last_three_year_tot_income_files_check_1"].value;
            y = 'Add Evidence Of Total Income Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#last_three_year_tot_income_files_check_1_id').text('');
                $('#last_three_year_tot_income_files_check_1_id').hide('');
            } else {
                x = document.forms["application_form"]["last_three_year_tot_income_files_1"].value;
                count += title_validate(x, y, f_name);
            }

            f_name = 'last_three_year_tot_income_files_check_2';
            x_chexk = document.forms["application_form"]["last_three_year_tot_income_files_check_2"].value;
            y = 'Add Evidence Of Total Income Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#last_three_year_tot_income_files_check_2_id').text('');
                $('#last_three_year_tot_income_files_check_2_id').hide('');
            } else {
                x = document.forms["application_form"]["last_three_year_tot_income_files_2"].value;
                count += title_validate(x, y, f_name);
            }

            f_name = 'last_three_year_tot_income_files_check_3';
            x_chexk = document.forms["application_form"]["last_three_year_tot_income_files_check_3"].value;
            y = 'Add Evidence Of Total Income Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#last_three_year_tot_income_files_check_3_id').text('');
                $('#last_three_year_tot_income_files_check_3_id').hide('');
            } else {
                x = document.forms["application_form"]["last_three_year_tot_income_files_3"].value;
                count += title_validate(x, y, f_name);
            }

            f_name = 'school_chairman_name';
            x = document.forms["application_form"]["school_chairman_name"].value;
            y = 'School Chairman Name ';
            count += title_validate(x, y, f_name);
            f_name = 'school_chairman_post';
            x = document.forms["application_form"]["school_chairman_post"].value;
            y = 'School Chairman Post ';
            count += title_validate(x, y, f_name);
            f_name = 'school_chairman_address';
            x = document.forms["application_form"]["school_chairman_address"].value;
            y = 'School Chairman Address ';
            count += title_validate(x, y, f_name);
            f_name = 'school_chairman_phone_number';
            x = document.forms["application_form"]["school_chairman_phone_number"].value;
            y = 'School Chairman Phone Number ';
            count += title_validate(x, y, f_name);
            f_name = 'school_chairman_office';
            x = document.forms["application_form"]["school_chairman_office"].value;
            y = 'School Chairman Office ';
            count += title_validate(x, y, f_name);
            f_name = 'school_chairman_email';
            x = document.forms["application_form"]["school_chairman_email"].value;
            y = 'School Chairman E-mail ';
            count += title_validate(x, y, f_name);
            f_name = 'plot_number';
            x = document.forms["application_form"]["plot_number"].value;
            y = 'Plot Number ';
            count += title_validate(x, y, f_name);
            f_name = 'khata_number';
            x = document.forms["application_form"]["khata_number"].value;
            y = 'Khata Number ';
            count += title_validate(x, y, f_name);


            // Tab 3
            f_name = 'medium';
            x = document.forms["application_form"]["medium"].value;
            y = 'Medium ';
            count += title_validate(x, y, f_name);
            f_name = 'type_of_school';
            x = document.forms["application_form"]["type_of_school"].value;
            y = 'Type Of School ';
            count += title_validate(x, y, f_name);
            f_name = 'supported_agency_name';
            x = document.forms["application_form"]["supported_agency_name"].value;
            y = 'Supported Agency Name ';
            count += title_validate(x, y, f_name);
            f_name = 'agency_supported_percent';
            x = document.forms["application_form"]["agency_supported_percent"].value;
            y = 'Agency Supported Percent ';
            count += title_validate(x, y, f_name);
            f_name = 'authority_name';
            x = document.forms["application_form"]["authority_name"].value;
            y = 'Authority Name ';
            count += title_validate(x, y, f_name);
            f_name = 'recognised_number';
            x = document.forms["application_form"]["recognised_number"].value;
            y = 'Recognised Number ';
            count += title_validate(x, y, f_name);
            f_name = 'is_school_on_rented';
            x = document.forms["application_form"]["is_school_on_rented"].value;
            y = 'Is School on Rented ';
            count += title_validate(x, y, f_name);
            f_name = 'is_school_has_boundary';
            x = document.forms["application_form"]["is_school_has_boundary"].value;
            y = 'Is School on Rented ';
            count += title_validate(x, y, f_name);
            f_name = 'is_school_cctv';
            x = document.forms["application_form"]["is_school_cctv"].value;
            y = 'Is School on Rented ';
            count += title_validate(x, y, f_name);
            f_name = 'is_school_ground';
            x = document.forms["application_form"]["is_school_ground"].value;
            y = 'Is School on Rented ';
            count += title_validate(x, y, f_name);
            f_name = 'is_school_water_hygene_facilities';
            x = document.forms["application_form"]["is_school_water_hygene_facilities"].value;
            y = 'Is School on Rented ';
            count += title_validate(x, y, f_name);
            f_name = 'is_school_fire_safty_facilities';
            x = document.forms["application_form"]["is_school_fire_safty_facilities"].value;
            y = 'Is School on Rented ';
            count += title_validate(x, y, f_name);
            var selectedOption = $("input:radio[name=is_school_has_boundary]:checked").val()
            if (selectedOption == 'Yes') {

                f_name = 'size_of_school_boundary';
                x = document.forms["application_form"]["size_of_school_boundary"].value;
                y = 'Size Of Boundry( W * H )   ';
                count += title_validate(x, y, f_name);

                f_name = 'school_boundary_files_check';
                x_chexk = document.forms["application_form"]["school_boundary_files_check"].value;
                y = 'Add Evidence Of Size Of Boundry( W * H )  Files ';
                if (x_chexk == 1) {
                    count += 0;
                    $('#school_boundary_files_check_id').text('');
                    $('#school_boundary_files_check_id').hide('');
                } else {
                    x = document.forms["application_form"]["school_boundary_files"].value;
                    count += title_validate(x, y, f_name);
                }
            }


            var selectedOption = $("input:radio[name=is_school_cctv]:checked").val()
            if (selectedOption == 'Yes') {
                f_name = 'no_of_school_cctv';
                x = document.forms["application_form"]["no_of_school_cctv"].value;
                y = 'No. Of CCTV   ';
                count += title_validate(x, y, f_name);

                f_name = 'school_cctv_file_check';
                x_chexk = document.forms["application_form"]["school_cctv_file_check"].value;
                y = 'Add Evidence Of No. Of CCTV Files ';
                if (x_chexk == 1) {
                    count += 0;
                    $('#school_cctv_file_check_id').text('');
                    $('#school_cctv_file_check_id').hide('');
                } else {
                    x = document.forms["application_form"]["school_cctv_file"].value;
                    count += title_validate(x, y, f_name);
                }
            }
            var selectedOption = $("input:radio[name=is_school_ground]:checked").val()
            if (selectedOption == 'Yes') {

                f_name = 'size_of_school_ground';
                x = document.forms["application_form"]["size_of_school_ground"].value;
                y = 'Size Of Sport Ground ( W * H )  ';
                count += title_validate(x, y, f_name);

                f_name = 'school_ground_file_check';
                x_chexk = document.forms["application_form"]["school_ground_file_check"].value;
                y = 'Add Evidence Of Size Of Sport Ground ( W * H ) Files ';
                if (x_chexk == 1) {
                    count += 0;
                    $('#school_ground_file_check_id').text('');
                    $('#school_ground_file_check_id').hide('');
                } else {
                    x = document.forms["application_form"]["school_ground_file"].value;
                    count += title_validate(x, y, f_name);
                }
            }
            var selectedOption = $("input:radio[name=is_school_water_hygene_facilities]:checked").val()
            if (selectedOption == 'Yes') {

                f_name = 'no_of_school_water_hygene_facilities';
                x = document.forms["application_form"]["no_of_school_water_hygene_facilities"].value;
                y = 'No. Of Water And Hygene Facilitie  ';
                count += title_validate(x, y, f_name);

                f_name = 'school_water_hygene_facilities_files_check';
                x_chexk = document.forms["application_form"]["school_water_hygene_facilities_files_check"].value;
                y = 'Add Evidence Of No. Of Water And Hygene Facilitie Files ';
                if (x_chexk == 1) {
                    count += 0;
                    $('#school_water_hygene_facilities_files_check_id').text('');
                    $('#school_water_hygene_facilities_files_check_id').hide('');
                } else {
                    x = document.forms["application_form"]["school_water_hygene_facilities_files"].value;
                    count += title_validate(x, y, f_name);
                }
            }
            var selectedOption = $("input:radio[name=is_school_fire_safty_facilities]:checked").val()
            if (selectedOption == 'Yes') {

                f_name = 'no_of_fire_safty';
                x = document.forms["application_form"]["no_of_fire_safty"].value;
                y = 'No. Of Fire Safty Facilities  ';
                count += title_validate(x, y, f_name);


                f_name = 'school_fire_safty_facilities_files_check';
                x_chexk = document.forms["application_form"]["school_fire_safty_facilities_files_check"].value;
                y = 'Add Evidence Of No. Of Fire Safty Facilities Files ';
                if (x_chexk == 1) {
                    count += 0;
                    $('#school_fire_safty_facilities_files_check_id').text('');
                    $('#school_fire_safty_facilities_files_check_id').hide('');
                } else {
                    x = document.forms["application_form"]["school_fire_safty_facilities_files"].value;
                    count += title_validate(x, y, f_name);
                }
            }


            f_name = 'all_teaching_material_list_files_check';
            x_chexk = document.forms["application_form"]["all_teaching_material_list_files_check"].value;
            y = 'Add Evidence Of No. Of Fire Safty Facilities Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#all_teaching_material_list_files_check_id').text('');
                $('#all_teaching_material_list_files_check_id').hide('');
            } else {
                x = document.forms["application_form"]["all_teaching_material_list_files"].value;
                count += title_validate(x, y, f_name);
            }

            f_name = 'all_sports_equipment_list_file_check';
            x_chexk = document.forms["application_form"]["all_sports_equipment_list_file_check"].value;
            y = 'Add Evidence Of No. Of Fire Safty Facilities Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#all_sports_equipment_list_file_check_id').text('');
                $('#all_sports_equipment_list_file_check_id').hide('');
            } else {
                x = document.forms["application_form"]["all_sports_equipment_list_file"].value;
                count += title_validate(x, y, f_name);
            }




            f_name = 'school_rent_deatail_check';
            x_chexk = document.forms["application_form"]["school_rent_deatail_check"].value;
            y = 'School Rent Deatail ';
            if (x_chexk == 1) {
                count += 0;
                $('#school_rent_deatail_check_id').text('');
                $('#school_rent_deatail_check_id').hide('');
            } else {
                x = document.forms["application_form"]["school_rent_deatail"].value;
                count += title_validate(x, y, f_name);
            }

            f_name = 'one_lac_fd_proof_check';
            x_chexk = document.forms["application_form"]["one_lac_fd_proof_check"].value;
            y = 'One Lakh Fixed Deposite Proof ';
            if (x_chexk == 1) {
                count += 0;
                $('#one_lac_fd_proof_check_id').text('');
                $('#one_lac_fd_proof_check_id').hide('');
            } else {
                x = document.forms["application_form"]["one_lac_fd_proof"].value;
                count += title_validate(x, y, f_name);
            }
            f_name = 'are_school_building_used';
            x = document.forms["application_form"]["are_school_building_used"].value;
            y = 'Are School Building Used ';
            count += title_validate(x, y, f_name);
            f_name = 'school_total_area';
            x = document.forms["application_form"]["school_total_area"].value;
            y = 'School Total Area ';
            count += title_validate(x, y, f_name);
            f_name = 'school_building_area';
            x = document.forms["application_form"]["school_building_area"].value;
            y = 'School Building Area ';
            count += title_validate(x, y, f_name);

            f_name = 'school_area_deatail_check';
            x_chexk = document.forms["application_form"]["school_area_deatail_check"].value;
            y = 'School Area Detail ';
            if (x_chexk == 1) {
                count += 0;
                $('#school_area_deatail_check_id').text('');
                $('#school_area_deatail_check_id').hide('');
            } else {
                x = document.forms["application_form"]["school_area_deatail"].value;
                count += title_validate(x, y, f_name);
            }
            // Tab 4
            f_name = 'form_class_id';
            x = document.forms["application_form"]["form_class_id"].value;
            y = 'From Class ';
            count += title_validate(x, y, f_name);
            f_name = 'to_class_id';
            x = document.forms["application_form"]["to_class_id"].value;
            y = 'To Class ';
            count += title_validate(x, y, f_name);
            f_name = 'SectionNo';
            var chks = document.getElementsByName('SectionNo[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Number Of Section';
                count += title_validate(x, y, f_name);
            }
            f_name = 'studentNo';
            var chks = document.getElementsByName('studentNo[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Number Of Students';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_student_ratio';
            var chks = document.getElementsByName('teacher_student_ratio[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Student Ratio';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_student_assign';
            var chks = document.getElementsByName('teacher_student_assign[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Assigned';
                count += title_validate(x, y, f_name);
            }
            f_name = 'num_of_teacher';
            var chks = document.getElementsByName('num_of_teacher[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Number Of Teacher';
                count += title_validate(x, y, f_name);
            }
            // TAb 5
            f_name = 'no_of_class';
            x = document.forms["application_form"]["no_of_class"].value;
            y = 'Number Of Class ';
            count += title_validate(x, y, f_name);
            f_name = 'avg_size_cls_room';
            x = document.forms["application_form"]["avg_size_cls_room"].value;
            y = 'Average Size Of Class Room ';
            count += title_validate(x, y, f_name);
            f_name = 'no_of_office_room';
            x = document.forms["application_form"]["no_of_office_room"].value;
            y = 'Number Of Office Room ';
            count += title_validate(x, y, f_name);
            f_name = 'avg_size_of_office_room';
            x = document.forms["application_form"]["avg_size_of_office_room"].value;
            y = ' Average Size Of Class Room ';
            count += title_validate(x, y, f_name);
            f_name = 'no_of_store_room';
            x = document.forms["application_form"]["no_of_store_room"].value;
            y = 'Number Of Store Room ';
            count += title_validate(x, y, f_name);
            f_name = 'avg_size_of_store_room';
            x = document.forms["application_form"]["avg_size_of_store_room"].value;
            y = 'Average Size of Store Room ';
            count += title_validate(x, y, f_name);
            f_name = 'no_of_princpal_room';
            x = document.forms["application_form"]["no_of_princpal_room"].value;
            y = 'Number Of Princpal Room ';
            count += title_validate(x, y, f_name);
            f_name = 'avg_size_of_principal_room';
            x = document.forms["application_form"]["avg_size_of_principal_room"].value;
            y = 'Average Size of Principal Room ';
            count += title_validate(x, y, f_name);
            f_name = 'no_of_kitchen_room';
            x = document.forms["application_form"]["no_of_kitchen_room"].value;
            y = 'Number Of Kitchen Room ';
            count += title_validate(x, y, f_name);
            f_name = 'avg_size_of_kitchen_room';
            x = document.forms["application_form"]["avg_size_of_kitchen_room"].value;
            y = 'Average Size of Kitchen Room ';
            count += title_validate(x, y, f_name);
            f_name = 'all_teaching_material_list';
            x = document.forms["application_form"]["all_teaching_material_list"].value;
            y = 'All Teaching Material List ';
            count += title_validate(x, y, f_name);
            f_name = 'facilities_access_without_interrupted';
            x = document.forms["application_form"]["facilities_access_without_interrupted"].value;
            y = 'Does all facilities have access without interruption? ';
            count += title_validate(x, y, f_name);
            f_name = 'all_sports_equipment_list';
            x = document.forms["application_form"]["all_sports_equipment_list"].value;
            y = 'All Sports Equipment List ';
            count += title_validate(x, y, f_name);
            f_name = 'books';
            x = document.forms["application_form"]["books"].value;
            y = 'Books ';
            count += title_validate(x, y, f_name);
            f_name = 'magazines';
            x = document.forms["application_form"]["magazines"].value;
            y = 'Magazines ';
            count += title_validate(x, y, f_name);

            f_name = 'books_in_library_files_check';
            x_chexk = document.forms["application_form"]["books_in_library_files_check"].value;
            y = 'Books In Library Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#books_in_library_files_check_id').text('');
                $('#books_in_library_files_check_id').hide('');
            } else {
                x = document.forms["application_form"]["books_in_library_files"].value;
                count += title_validate(x, y, f_name);
            }
            // Tab 6
            f_name = 'type_of_water_facilities';
            x = document.forms["application_form"]["type_of_water_facilities"].value;
            y = 'Type Of Water Facilities ';
            count += title_validate(x, y, f_name);
            f_name = 'no_of_water_supply';
            x = document.forms["application_form"]["no_of_water_supply"].value;
            y = 'Number Of Water Supply ';
            count += title_validate(x, y, f_name);

            f_name = 'water_facilities_files_check';
            x_chexk = document.forms["application_form"]["water_facilities_files_check"].value;
            y = 'Water Facilities Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#water_facilities_files_check_id').text('');
                $('#water_facilities_files_check_id').hide('');
            } else {
                x = document.forms["application_form"]["water_facilities_files"].value;
                count += title_validate(x, y, f_name);
            }

            f_name = 'educational_qualification_files_check';
            x_chexk = document.forms["application_form"]["educational_qualification_files_check"].value;
            y = 'Educational Qualification Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#educational_qualification_files_check_id').text('');
                $('#educational_qualification_files_check_id').hide('');
            } else {
                x = document.forms["application_form"]["educational_qualification_files"].value;
                count += title_validate(x, y, f_name);
            }
            f_name = 'type_of_toilet';
            x = document.forms["application_form"]["type_of_toilet"].value;
            y = 'Type Of Toilet ';
            count += title_validate(x, y, f_name);
            f_name = 'gents_toilet';
            x = document.forms["application_form"]["gents_toilet"].value;
            y = 'Gents Toilet ';
            count += title_validate(x, y, f_name);
            f_name = 'ladies_toilet';
            x = document.forms["application_form"]["ladies_toilet"].value;
            y = 'Ladies Toilet  ';
            count += title_validate(x, y, f_name);

            f_name = 'cleanliness_files_check';
            x_chexk = document.forms["application_form"]["cleanliness_files_check"].value;
            y = 'Cleanliness Files ';
            if (x_chexk == 1) {
                count += 0;
                $('#last_three_year_tot_income_files_check_id').text('');
                $('#last_three_year_tot_income_files_check_id').hide('');
            } else {
                x = document.forms["application_form"]["cleanliness_files"].value;
                count += title_validate(x, y, f_name);
            }
            // TAb 7
            f_name = 'principle_name';
            x = document.forms["application_form"]["principle_name"].value;
            y = 'Principle Name ';
            count += title_validate(x, y, f_name);
            f_name = 'p_f_h_w_name';
            x = document.forms["application_form"]["p_f_h_w_name"].value;
            y = 'Father / Husband Or Wife Name ';
            count += title_validate(x, y, f_name);
            f_name = 'p_date_of_birth';
            x = document.forms["application_form"]["p_date_of_birth"].value;
            y = 'Date Of Birth ';
            count += title_validate(x, y, f_name);
            f_name = 'p_education_qualification';
            x = document.forms["application_form"]["p_education_qualification"].value;
            y = 'Educational Qualification ';
            count += title_validate(x, y, f_name);
            f_name = 'trainee_qualification';
            x = document.forms["application_form"]["trainee_qualification"].value;
            y = 'Trainee Qualification ';
            count += title_validate(x, y, f_name);
            f_name = 'teaching_experience';
            x = document.forms["application_form"]["teaching_experience"].value;
            y = 'Teaching Experience ';
            count += title_validate(x, y, f_name);
            f_name = 'class_handed_over';
            x = document.forms["application_form"]["class_handed_over"].value;
            y = 'Class Handed Over ';
            count += title_validate(x, y, f_name);
            f_name = 'date_of_appointment';
            x = document.forms["application_form"]["date_of_appointment"].value;
            y = 'Date Of Aappointment ';
            count += title_validate(x, y, f_name);
            f_name = 'trained_untrained';
            x = document.forms["application_form"]["trained_untrained"].value;
            y = 'Trained / Untrained ';
            count += title_validate(x, y, f_name);
            f_name = 'teacher_name';
            var chks = document.getElementsByName('teacher_name[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Name ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_f_h_w_name';
            var chks = document.getElementsByName('teacher_f_h_w_name[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Father / Husband Or Wife  Name ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_date_of_birth';
            var chks = document.getElementsByName('teacher_date_of_birth[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Date Of Birth ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_education_qualification';
            var chks = document.getElementsByName('teacher_education_qualification[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Education Qualification ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_trainee_qualification';
            var chks = document.getElementsByName('teacher_trainee_qualification[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Trainee Qualification ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_teaching_experience';
            var chks = document.getElementsByName('teacher_teaching_experience[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Teaching Experience ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_class_handed_over';
            var chks = document.getElementsByName('teacher_class_handed_over[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Class Handed Over ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_appointment_date';
            var chks = document.getElementsByName('teacher_appointment_date[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Appointment Date ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teacher_trained_or_untrained';
            var chks = document.getElementsByName('teacher_trained_or_untrained[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teacher Trained / Untrained ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'teaching_fee';
            var chks = document.getElementsByName('teaching_fee[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Teaching Fee ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'registration_fee';
            var chks = document.getElementsByName('registration_fee[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Registration Fee ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'enrollment_fee';
            var chks = document.getElementsByName('enrollment_fee[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Enrollment Fee ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'security_money';
            var chks = document.getElementsByName('security_money[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Security Money ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'development_fee';
            var chks = document.getElementsByName('development_fee[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Development Fee ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'annual_charge';
            var chks = document.getElementsByName('annual_charge[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Annual Charge ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'other_charges';
            var chks = document.getElementsByName('other_charges[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Other Charges ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'total';
            var chks = document.getElementsByName('total[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Total ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'income';
            var chks = document.getElementsByName('income[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Income ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'expenses';
            var chks = document.getElementsByName('expenses[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Expenses ';
                count += title_validate(x, y, f_name);
            }
            // Tab 8
            f_name = 'details_of_curriculum';
            var chks = document.getElementsByName('details_of_curriculum[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Details Of Curriculum ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'method_of_inspection';
            var chks = document.getElementsByName('method_of_inspection[]');
            for (var i = 0; i < chks.length; i++) {
                x = chks[i].value;
                y = 'Method Of Inspection ';
                count += title_validate(x, y, f_name);
            }
            f_name = 'school_board_exam_till_cls_eight';
            x = document.forms["application_form"]["school_board_exam_till_cls_eight"].value;
            y = 'School Board Exam Till Class Eight ';
            count += title_validate(x, y, f_name);
            if (count != 0) {
                if (count == 1) {
                    alert('Must be filled out ' + count + ' field !!!');
                }
                else {
                    alert('Must be filled out ' + count + ' fields !!!');
                }
                return false;
            } else {
                return true;
            }
        } else {
            // alert('save as draft data ');
            return true;
        }
    }
    function title_validate(withoutSpace, Name, Field_Name) {
        var flag = 0;
        var strText = withoutSpace.trim();
        if (strText != "") {
            if (Field_Name == 'school_name' || Field_Name == 'post_office' || Field_Name == 'village_town' || Field_Name == 'police_station' || Field_Name == 'society_name'
                || Field_Name == 'is_school_has_boundary' || Field_Name == 'is_school_cctv' || Field_Name == 'is_school_ground' || Field_Name == 'is_school_water_hygene_facilities' || Field_Name == 'is_school_fire_safty_facilities'
                || Field_Name == 'size_of_school_boundary' || Field_Name == 'no_of_school_cctv' || Field_Name == 'size_of_school_ground' || Field_Name == 'no_of_school_water_hygene_facilities' || Field_Name == 'no_of_fire_safty'
                || Field_Name == 'school_chairman_name' || Field_Name == 'school_chairman_post'
                || Field_Name == 'school_chairman_address' || Field_Name == 'school_chairman_office' || Field_Name == 'medium' || Field_Name == 'supported_agency_name'
                || Field_Name == 'agency_supported_percent' || Field_Name == 'authority_name' || Field_Name == 'recognised_number' || Field_Name == 'school_total_area'
                || Field_Name == 'school_building_area' || Field_Name == 'no_of_class' || Field_Name == 'avg_size_cls_room' || Field_Name == 'no_of_office_room'
                || Field_Name == 'avg_size_of_office_room' || Field_Name == 'no_of_store_room' || Field_Name == 'avg_size_of_store_room' || Field_Name == 'no_of_princpal_room'
                || Field_Name == 'avg_size_of_principal_room' || Field_Name == 'no_of_kitchen_room' || Field_Name == 'avg_size_of_kitchen_room' || Field_Name == 'all_teaching_material_list'
                || Field_Name == 'all_teaching_material_list' || Field_Name == 'type_of_water_facilities' || Field_Name == 'no_of_water_supply' || Field_Name == 'principle_name' || Field_Name == 'p_f_h_w_name'
                || Field_Name == 'p_education_qualification' || Field_Name == 'trainee_qualification' || Field_Name == 'teaching_experience' || Field_Name == 'class_handed_over'
                || Field_Name == 'trained_untrained' || Field_Name == 'books' || Field_Name == 'magazines'
                || Field_Name == 'teacher_name' || Field_Name == 'teacher_f_h_w_name' || Field_Name == 'teacher_education_qualification' || Field_Name == 'teacher_trainee_qualification'
                || Field_Name == 'teacher_teaching_experience' || Field_Name == 'teacher_class_handed_over' || Field_Name == 'teacher_trained_or_untrained' || Field_Name == 'details_of_curriculum'
                || Field_Name == 'method_of_inspection' || Field_Name == 'all_sports_equipment_list' || Field_Name == 'teacher_date_of_birth'
                || Field_Name == 'teacher_trainee_qualification' || Field_Name == 'teacher_appointment_date' || Field_Name == 'income' || Field_Name == 'expenses' || Field_Name == 'teaching_fee'
                || Field_Name == 'registration_fee' || Field_Name == 'enrollment_fee' || Field_Name == 'security_money' || Field_Name == 'development_fee' || Field_Name == 'annual_charge'
                || Field_Name == 'other_charges' || Field_Name == 'total' || Field_Name == 'plot_number' || Field_Name == 'khata_number'
                || Field_Name == 'school_validation_till' || Field_Name == 'teacher_student_ratio' || Field_Name == 'teacher_student_assign' || Field_Name == 'num_of_teacher'
            ) {
                var strArr = new Array();
                strArr = strText.split(" ");
                if (strArr['0'] == '') {
                    // alert(strArr);
                    flag = 1;
                } else {
                    flag = 0;
                }
            } else {
                var strArr = new Array();
                strArr = strText.split(" ");
                if (strArr.length > 1) {
                    flag = 1;
                }
            }
        }
        var field_id = Field_Name + '_id';
        if (
            Field_Name == 'evidence_of_non_proprietary_nature_ex_check' || Field_Name == 'last_three_year_tot_income_files_check_1' ||
            Field_Name == 'educational_qualification_files_check' || Field_Name == 'teacher_educational_qualification_files_check' ||
            Field_Name == 'all_teaching_material_list_files' || Field_Name == 'all_sports_equipment_list_file' ||
            Field_Name == 'last_three_year_tot_income_files_check_2' || Field_Name == 'last_three_year_tot_income_files_check_3' || Field_Name == 'school_area_deatail_check'
            || Field_Name == 'school_rent_deatail_check' || Field_Name == 'books_in_library_files_check' || Field_Name == 'water_facilities_files_check' || Field_Name == 'cleanliness_files_check'
            || Field_Name == 'school_boundary_files' || Field_Name == 'school_cctv_file' || Field_Name == 'school_ground_file' || Field_Name == 'school_water_hygene_facilities_files' || Field_Name == 'school_fire_safty_facilities_files') {
            if (strText == 0) {
                withoutSpace = '';
                flag = 1;
            } else {
                withoutSpace = '1';
                flag = 0;
            }
        }
        if (withoutSpace == "" || flag == 1) { //document.getElementById('t1').style.display="block";//for span alert!!
            // alert(field_id);
            if (field_id == 'SectionNo_id' || field_id == 'studentNo_id' || Field_Name == 'teacher_name' || Field_Name == 'teacher_f_h_w_name' || Field_Name == 'teacher_education_qualification' || Field_Name == 'teacher_trainee_qualification'
                || Field_Name == 'teacher_teaching_experience' || Field_Name == 'teacher_class_handed_over' || Field_Name == 'teacher_trained_or_untrained' || Field_Name == 'details_of_curriculum'
                || Field_Name == 'method_of_inspection' || Field_Name == 'teacher_date_of_birth'
                || Field_Name == 'teacher_student_ratio' || Field_Name == 'teacher_student_assign' || Field_Name == 'num_of_teacher'
                || Field_Name == 'teacher_trainee_qualification' || Field_Name == 'income' || Field_Name == 'expenses' || Field_Name == 'teaching_fee' || Field_Name == 'registration_fee'
                || Field_Name == 'enrollment_fee' || Field_Name == 'security_money' || Field_Name == 'development_fee' || Field_Name == 'annual_charge' || Field_Name == 'other_charges'
                || Field_Name == 'total' || Field_Name == 'teacher_appointment_date') {
                // var text = $(this).hasClass(field_id).val();
                // alert(text);
                $('.' + field_id).show();
                $('.' + field_id).text('');
                $('.' + field_id).text(Name + " shouldn't be blank space!!");
                $('.' + field_id).css('fontsize', '13px');
                $('.' + field_id).css('color', 'red');
            }
            else if (Field_Name == 'teacher_educational_qualification_files_check') {
                $('.' + field_id).show();
                $('.' + field_id).text('');
                $('.' + field_id).text(Name + " Choose !!");
                $('.' + field_id).css('fontsize', '13px');
                $('.' + field_id).css('color', 'red');
            }
            else {
                $('#' + field_id).show();
                $('#' + field_id).text('');
                if (field_id == 'estd_year_id' || field_id == 'session_year_id' || field_id == 'type_of_school_id' || field_id == 'form_class_id_id' || field_id == 'to_class_id_id' || field_id == 'is_society_registered_id' || field_id == 'evidence_of_non_proprietary_nature_id' || field_id == 'is_school_on_rented' || field_id == 'are_school_building_used' || field_id == 'facilities_access_without_interrupted' || field_id == 'school_board_exam_till_cls_eight'
                    || field_id == 'is_school_has_boundary_id' || field_id == 'is_school_cctv_id' || field_id == 'is_school_ground_id' || field_id == 'is_school_water_hygene_facilities_id' || field_id == 'is_school_fire_safty_facilities_id' || field_id == 'is_society_registered_id_id' || field_id == 'evidence_of_non_proprietary_nature_id' || field_id == 'is_school_on_rented_id' || field_id == 'are_school_building_used_id' || field_id == 'facilities_access_without_interrupted_id') {
                    $('#' + field_id).text(Name + " select any one option !!");
                } else {
                    $('#' + field_id).text(Name + " shouldn't be blank space !!");
                }
                $('#' + field_id).css('fontsize', '13px');
                $('#' + field_id).css('color', 'red');
            }
            return 1;
        } else {
            if (field_id == 'SectionNo_id' || field_id == 'studentNo_id') {
                $('.' + field_id).show();
                $('.' + field_id).text('');
            } {
                $('#' + field_id).show();
                $('#' + field_id).text('');
            }
            return 0;
        }
    }
</script>