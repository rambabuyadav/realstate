@include('header')
@include('sidenav')
@include('topbar')
<style>
    .remove_button_css {
        border: 9px white solid;
        background: white;
        color: #4680ff;
        font-weight: 700;
        outline: none;
    }

    .remove_button_css:focus {
        border: 9px white solid;
        /* background-color:red; */
        outline: none;
    }
    .bg-secondary{
        /* background:rgb(202, 202, 202) !important; */
        background:#e6e4e4 !important;
    }
   
    canvas{
        
    }

</style>
    
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css">
    <script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- ========================= State Authority Start =====================================-->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Dashboard</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><i class="fa fa-file text-white" aria-hidden="true"></i>
                            <li class="breadcrumb-item text-white">State Dashboard</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include('message-flash')
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-lg-12">
                <!-- support-section start -->
                <div class="row">
                    <div class="col-sm-3">
                        <a href="{{url('report-all-applications')}}">
                            <div class="card support-bar overflow-hidden">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="text-c-blue text-secondary">Total Visits</h2>
                                            <span style="float:left;"><i class="far fa-chart-bar fa-2x text-success"></i></span>
                                            <span class=" bg-secondary p-2 mb-2 " style="float:right;border-radius:10px;color:green;font-weight:700;">+10%</span>
                                        </div>
                                        <div class="col-md-4">
                                            <h2 class="m-0 text-right"></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-primary text-white">
                                    <div class="row text-center">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="{{url('report-approved-applications')}}">
                            <div class="card support-bar overflow-hidden">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="text-c-blue text-secondary">Total Page Views</h2>
                                            <span style="float:left;"><i class="far fa-chart-bar fa-2x text-warning"></i></span>
                                            <span class=" bg-secondary p-2 mb-2 " style="float:right;border-radius:10px;color:green;font-weight:700;">7%</span>
                                        </div>
                                        <div class="col-md-4">
                                            <h2 class="m-0 text-right"></h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div  id="power-card-chart1"></div> -->
                                <div class="card-footer bg-primary text-white">
                                    <div class="row text-center">
                                        <!-- <div class="col">
                                    <h4 class="m-0 text-white">10</h4>
                                    <span>Open</span>
                                </div>
                                <div class="col">
                                    <h4 class="m-0 text-white">5</h4>
                                    <span>Running</span>
                                </div>
                                <div class="col">
                                    <h4 class="m-0 text-white">3</h4>
                                    <span>Solved</span>
                                </div> -->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="{{url('report-certified-applications')}}">
                            <div class="card support-bar overflow-hidden">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="text-c-blue text-secondary">Unique Visitor</h2>
                                            <span style="float:left;"><i class="far fa-chart-bar fa-2x text-info"></i></span>
                                            <span class=" bg-secondary p-2 mb-2 " style="float:right;border-radius:10px;color:green;font-weight:700;">12%</span>
                                        </div>
                                        <div class="col-md-4">
                                            <h2 class="m-0 text-right"></h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div  id="power-card-chart2"></div> -->
                                <div class="card-footer bg-primary text-white">
                                    <div class="row text-center">
                                        <!-- <div class="col">
                                    <h4 class="m-0 text-white">10</h4>
                                    <span>Open</span>
                                </div>
                                <div class="col">
                                    <h4 class="m-0 text-white">5</h4>
                                    <span>Running</span>
                                </div>
                                <div class="col">
                                    <h4 class="m-0 text-white">3</h4>
                                    <span>Solved</span>
                                </div> -->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="{{url('reportRejectedApplications')}}">
                            <div class="card support-bar overflow-hidden">
                                <div class="card-body pb-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="text-c-blue text-secondary">Bounce rate</h2>
                                            <span style="float:left;"><i class="far fa-chart-bar fa-2x text-primary"></i></span>
                                            <span class=" bg-secondary p-2 mb-2 " style="float:right;border-radius:10px;color:green;font-weight:700;">12%</span>
                                        </div>
                                        <div class="col-md-4">
                                            <h2 class="m-0 text-right"></h2>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div id="support-chart1"></div> -->
                                <div class="card-footer bg-primary text-white">
                                    <div class="row text-center">
                                        <!-- <div class="col">
                                    <h4 class="m-0 text-white">10</h4>
                                    <span>Open</span>
                                </div>
                                <div class="col">
                                    <h4 class="m-0 text-white">5</h4>
                                    <span>Running</span>
                                </div>
                                <div class="col">
                                    <h4 class="m-0 text-white">3</h4>
                                    <span>Solved</span>
                                </div> -->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- support-section end -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="shadow-lg mt-6 bg-white rounded">
                    <div class="card-header">
                        <h2 class="text-success text-center">Last 6 Month Visitor</h2>
                    </div>
                    <div class="card-body">
                        <div class="chartbox " style="position:relative;left:60px;width:auto;height:164px;width:auto;">
                            <canvas id="myChart"></canvas>
                            <ul class="list-unstyled">
                                <li></li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <script>
             //gender chart
             Chart.defaults.font.size = 12;
                    let genChart = document.getElementById('myChart').getContext('2d');
                    let genderChart = new Chart(genChart, {
                        type: 'bar',
                        data: {
                            labels: ['Jan','Feb','March','April','May','June'],
                            datasets: [{
                                label: 'No. Of Visitors',
                                data: [
                                    352334,
                                    234234,
                                    746473,
                                    876644,
                                    908976,
                                    774367
                                ],
                                backgroundColor: [
                                    '#0CB5FF',
                                    '#FBB27A',
                                    '#9CCC65',
                                    '#4680FF',
                                    '#FFBF00',
                                    '#DAF7A6'
                                ]
                                
                            }]
                        },
                        options: {
            
                            responsive: false,
                            animation: {
                                animateScale: true
                            }
                        }
                    })
        </script>
        {{-- <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="shadow-lg   mt--6 bg-white rounded">
                    <div class="bg-primary p-2 text-center ">
                        <h4 class="text-white">Division-wise Application</h4>
                    </div>
                    <table class="table table-bordered table-sm table-responsive">
                        <thead>
                            <tr style="width: 100%;">
                                <th style="width: 100%;">Division</th>
                                <th style="width: 100%;">All</th>
                                <th style="width: 100%;">Pending</th>
                                <th style="width: 100%;">Approved</th>
                                <th style="width: 100%;">Rejected</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 ">
                <div class="shadow-lg   mt--6 bg-white rounded">
                    <div class="bg-primary p-2 text-center ">
                        <h4 class="text-white">Latest Application</h4>
                    </div>
                    <table class="table table-bordered table-sm table-responsive">
                        <thead>
                            <tr style="width: 100%;">
                                <th style="width: 100%;">School Name</th>
                                <th style="width: 100%;">District</th>
                                <th style="width: 100%;">Block</th>
                                <th style="width: 100%;">Date</th>
                                <!-- <th style="width: 100%;">Remaining Days</th> -->
                            </tr>
                        </thead>
                        <tbody>
                              <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> --}}
        <!-- [ Main Content ] end -->
    </div>
</div>

@include('footer')