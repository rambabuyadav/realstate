@include('header')
@include('sidenav')
@include('topbar')
<!-- [ Header ] end -->
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">School Verify</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">School All Data Verify by DSE / DC</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">

                    <!-- general information -->
                    <input type="text" hidden value="{{$user->applicationId}}" name="applicationId" id="applicationId">
                    <input type="text" hidden value="{{$user->schoolId}}" name="schoolId" id="schoolId">
                    <div class="card-body">
                        <input type="hidden" name="id" id="school_id" value="{{$user->id}}">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>SL NO.</th>
                                <th>FIELD NAME</th>
                                <th>VALUE</th>
                                @if(Auth::user()->user_role=='school' )
                                <td> Remarks </td>
                                <td> Status </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <th style="width: 30px;">ACTION</th>
                                @endif
                            </tr>
                            @if($user->school_name_vrfy == 2)
                            <tr>
                                <td class="si_no">1</td>
                                <td class="lable_name">School Name</td>
                                <td class="field_name" style="display: none;"> school_name </td>
                                <td class="value_name">{{$user->school_name}}</td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_name_rmk}} </td>
                                <td>
                                    @if($user->school_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->school_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" disabled title="Already Approved" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" disabled  title="Already Rejected" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->recognised_by_vrfy == 2)
                            <tr>
                                <td class="si_no">2</td>
                                <td class="lable_name">Academic Session From Which Recognition Proposed</td>
                                <td class="field_name" style="display: none;"> recognised_by </td>
                                <td class="value_name">{{$user->recognised_by}}</td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->recognised_by_rmk}} </td>
                                <td>
                                    @if($user->recognised_by_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->recognised_by_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->recognised_by_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->recognised_by_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->recognised_by_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->recognised_by_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->district_vrfy == 2)
                            <tr>
                                <td class="si_no">3</td>
                                <td class="lable_name">District</td>
                                <td class="field_name" style="display: none;"> district </td>
                                <td class="value_name">{{$user->district}} </td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->district_rmk}} </td>
                                <td>
                                    @if($user->district_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->district_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->district_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->district_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->district_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->district_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->post_office_vrfy == 2)
                            <tr>
                                <td class="si_no">4</td>
                                <td class="lable_name">Post Office</td>
                                <td class="field_name" style="display: none;"> post_office </td>
                                <td class="value_name">{{$user->post_office}} </td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->post_office_rmk}} </td>
                                <td>
                                    @if($user->post_office_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->post_office_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->post_office_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->post_office_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->post_office_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->post_office_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->village_city_vrfy == 2)
                            <tr>
                                <td class="si_no">5</td>
                                <td class="lable_name">Village / Town</td>
                                <td class="field_name" style="display: none;"> village_city </td>
                                <td class="value_name"> {{$user->village_city}}</td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->village_city_rmk}} </td>
                                <td>
                                    @if($user->village_city_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->village_city_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->village_city_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->village_city_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->village_city_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->village_city_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pincode_vrfy == 2)
                            <tr>
                                <td class="si_no">6</td>
                                <td class="lable_name">Pin Code</td>
                                <td class="field_name" style="display: none;"> pincode </td>
                                <td class="value_name">{{$user->pincode}} </td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pincode_rmk}} </td>
                                <td>
                                    @if($user->pincode_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pincode_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pincode_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->pincode_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->pincode_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->pincode_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->phone_with_std_code_vrfy == 2)
                            <tr>
                                <td class="si_no">7</td>
                                <td class="lable_name">Ph./Mb no. With STD Code</td>
                                <td class="field_name" style="display: none;"> phone_with_std_code </td>
                                <td class="value_name"> {{$user->phone_with_std_code}}</td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->phone_with_std_code_rmk}} </td>
                                <td>
                                    @if($user->phone_with_std_code_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->phone_with_std_code_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->phone_with_std_code_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->phone_with_std_code_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->phone_with_std_code_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->phone_with_std_code_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->fax_with_std_code_vrfy == 2)
                            <tr>
                                <td class="si_no">8</td>
                                <td class="lable_name">FAX No. With STD Code</td>
                                <td class="field_name" style="display: none;"> fax_with_std_code </td>
                                <td class="value_name"> {{$user->fax_with_std_code}}</td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->fax_with_std_code_rmk}} </td>
                                <td>
                                    @if($user->fax_with_std_code_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->fax_with_std_code_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->fax_with_std_code_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->fax_with_std_code_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->fax_with_std_code_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->fax_with_std_code_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->email_vrfy == 2)
                            <tr>
                                <td class="si_no">9</td>
                                <td class="lable_name">Email ID</td>
                                <td class="field_name" style="display: none;"> email </td>
                                <td class="value_name">{{$user->email}} </td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->email_rmk}} </td>
                                <td>
                                    @if($user->email_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->email_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->email_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->email_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->email_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->email_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->police_station_vrfy == 2)
                            <tr>
                                <td class="si_no">10</td>
                                <td class="lable_name">Nearest Police Station</td>
                                <td class="field_name" style="display: none;"> police_station </td>
                                <td class="value_name">{{$user->police_station}} </td>

                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->police_station_rmk}} </td>
                                <td>
                                    @if($user->police_station_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->police_station_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->police_station_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->police_station_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->police_station_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->police_station_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->estd_year_vrfy != 2 && $user->opening_date_vrfy != 2 && $user->society_name_vrfy != 2 && $user->society_registration_valid_upto_vrfy == 2 && $user->evidence_of_non_proprietary_nature_vrfy == 2 && $user->evidence_of_non_proprietary_nature_vrfy == 2)
                            <tr>
                                <td colspan="3">

                                    <div class="card-header pt-0 pb-0">
                                        General Information
                                    </div>
                                </td>
                            </tr>
                            @endif

                            @if($user->estd_year_vrfy == 2)
                            <tr>
                                <td class="si_no">11</td>
                                <td class="lable_name">Establishment Year</td>
                                <td class="field_name" style="display: none;"> estd_year </td>
                                <td class="value_name">{{$user->estd_year}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->estd_year_rmk}} </td>
                                <td>
                                    @if($user->estd_year_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->estd_year_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->estd_year_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->estd_year_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->estd_year_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->estd_year_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->opening_date_vrfy == 2)
                            <tr>
                                <td class="si_no">12</td>
                                <td class="lable_name">School Opening Date </td>
                                <td class="field_name" style="display: none;"> opening_date </td>
                                <td class="value_name">{{$user->opening_date}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->opening_date_rmk}} </td>
                                <td>
                                    @if($user->opening_date_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->opening_date_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->opening_date_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->opening_date_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->opening_date_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->opening_date_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->society_name_vrfy == 2)
                            <tr>
                                <td class="si_no">13</td>
                                <td class="lable_name">trust/Society/Management Committee Name</td>
                                <td class="field_name" style="display: none;"> society_name </td>
                                <td class="value_name"> {{$user->society_name}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->society_name_rmk}} </td>
                                <td>
                                    @if($user->society_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->society_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->society_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->society_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->society_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->society_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->is_society_registered_vrfy == 2)
                            <tr>
                                <td class="si_no">14</td>
                                <td class="lable_name">is trust/society/management comittee registered?</td>
                                <td class="field_name" style="display: none;"> is_society_registered </td>
                                <td class="value_name">{{$user->is_society_registered}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->is_society_registered_rmk}} </td>
                                <td>
                                    @if($user->is_society_registered_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->is_society_registered_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->is_society_registered_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->is_society_registered_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->is_society_registered_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->is_society_registered_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->society_registration_valid_upto_vrfy == 2)
                            <tr>
                                <td class="si_no">15</td>
                                <td class="lable_name">The period until the registration of the Trust / Society / Management Committee is valid</td>
                                <td class="field_name" style="display: none;"> society_registration_valid_upto </td>
                                <td class="value_name"> {{$user->society_registration_valid_upto}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->society_registration_valid_upto_rmk}} </td>
                                <td>
                                    @if($user->society_registration_valid_upto_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->society_registration_valid_upto_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->society_registration_valid_upto_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->society_registration_valid_upto_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->society_registration_valid_upto_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->society_registration_valid_upto_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->evidence_of_non_proprietary_nature_vrfy == 2)
                            <tr>
                                <td class="si_no">16</td>
                                <td class="lable_name">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of <br> members including their addresses on affidavit ?</td>
                                <td class="field_name" style="display: none;"> evidence_of_non_proprietary_nature </td>
                                <td class="value_name"> {{$user->evidence_of_non_proprietary_nature1}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->evidence_of_non_proprietary_nature_rmk}} </td>
                                <td>
                                    @if($user->evidence_of_non_proprietary_nature_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->evidence_of_non_proprietary_nature_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->evidence_of_non_proprietary_nature_vrfy == 2)
                            <tr>
                                <td class="si_no"></td>
                                <td class="lable_name">Add Relevant Evidence Attachement</td>
                                <td class="field_name" style="display: none;"> evidence_of_non_proprietary_nature_ext </td>
                                <td class="value_name">
                                    @if(isset($user->evidence_of_non_proprietary_nature_ex))
                                    @foreach($user->evidence_of_non_proprietary_nature_ex as $key => $val)
                                    <a href="{{url('http://paatham.us/rte-application/public/applications/')}}{{$val}}" target="_blank"> {{$val}}</a><br>
                                    @endforeach
                                    @endif
                                </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->evidence_of_non_proprietary_nature_rmk}} </td>
                                <td>
                                    @if($user->evidence_of_non_proprietary_nature_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dc' || Auth::user()->user_role=='dse' )
                                <td>
                                    @if($user->evidence_of_non_proprietary_nature_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->evidence_of_non_proprietary_nature_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td colspan="3"><b> Chairman Information</b></td>
                                <!-- @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif -->
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td class="si_no">17</td>
                                <td class="lable_name">Chairman Name </td>
                                <td class="field_name" style="display: none;"> school_chairman_name </td>
                                <td class="value_name"> {{$user->school_chairman_name}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td class="si_no">18</td>
                                <td class="lable_name">Designation</td>
                                <td class="field_name" style="display: none;"> school_chairman_post </td>
                                <td class="value_name">{{$user->school_chairman_post}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td class="si_no">19</td>
                                <td class="lable_name">Address</td>
                                <td class="field_name" style="display: none;"> school_chairman_address </td>
                                <td class="value_name">{{$user->school_chairman_address}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td class="si_no">20</td>
                                <td class="lable_name">Phone No.</td>
                                <td class="field_name" style="display: none;"> school_chairman_phone_number </td>
                                <td class="value_name">{{$user->school_chairman_phone_number}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td class="si_no">21</td>
                                <td class="lable_name">Office</td>
                                <td class="field_name" style="display: none;"> school_chairman_office </td>
                                <td class="value_name"> {{$user->school_chairman_office}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_chairman_name_vrfy == 2)
                            <tr>
                                <td class="si_no">22</td>
                                <td class="lable_name">Email ID</td>
                                <td class="field_name" style="display: none;"> school_chairman_email </td>
                                <td class="value_name">{{$user->school_chairman_email}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if($user->school_chairman_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_chairman_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_chairman_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->session_year_vrfy != 2 && $user->income_vrfy == 2 && $user->income_vrfy == 2 && $user->expenses_vrfy == 2 && $user->reduced_money_vrfy == 2 && $user->last_three_year_tot_income_vrfy == 2)
                            <tr>
                                <td colspan="4"><b> 3 Years Total Income / Expenses / Surplus / Loss</b></td>
                            </tr>
                            @endif

                            @if($user->session_year_vrfy == 2)
                            <tr>
                                <td class="si_no">23</td>
                                <td class="lable_name"> Year</td>
                                <td class="field_name" style="display: none;"> session_year </td>
                                <td class="value_name"> {{$user->session_year}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->session_year_rmk}} </td>
                                <td>
                                    @if($user->session_year_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->session_year_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->session_year_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->session_year_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->session_year_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->session_year_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->income_vrfy == 2)
                            <tr>
                                <td class="si_no">24</td>
                                <td class="lable_name">Income</td>
                                <td class="field_name" style="display: none;"> income </td>
                                <td class="value_name">{{$user->income}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->income_rmk}} </td>
                                <td>
                                    @if($user->income_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->income_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->income_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->income_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->income_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->income_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->expenses_vrfy == 2)
                            <tr>
                                <td class="si_no">25</td>
                                <td class="lable_name">Expenses</td>
                                <td class="field_name" style="display: none;"> expenses </td>
                                <td class="value_name"> {{$user->expenses}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->expenses_rmk}} </td>
                                <td>
                                    @if($user->expenses_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->expenses_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->expenses_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->expenses_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->expenses_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->expenses_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->surplus_money_vrfy == 2)
                            <tr>
                                <td class="si_no">26</td>
                                <td class="lable_name">Surplus Money</td>
                                <td class="field_name" style="display: none;"> surplus_money </td>
                                <td class="value_name"> {{$user->surplus_money}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->surplus_money_rmk}} </td>
                                <td>
                                    @if($user->surplus_money_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->surplus_money_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->surplus_money_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->surplus_money_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->surplus_money_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->surplus_money_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->reduced_money_vrfy == 2)
                            <tr>
                                <td class="si_no">27</td>
                                <td class="lable_name">Reduce</td>
                                <td class="field_name" style="display: none;"> reduced_money </td>
                                <td class="value_name"> {{$user->reduced_money}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->reduced_money_rmk}} </td>
                                <td>
                                    @if($user->reduced_money_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->reduced_money_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->reduced_money_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->reduced_money_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->reduced_money_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->reduced_money_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->last_three_year_tot_income_vrfy == 2)
                            <tr>
                                <td class="si_no"></td>
                                <td class="lable_name">Add Relevant Evidence Attachement</td>
                                <td class="field_name" style="display: none;"> last_three_year_tot_income_ext </td>
                                <td class="value_name">
                                    @if(isset($user->last_three_year_tot_income))
                                    @foreach($user->last_three_year_tot_income as $key => $val)
                                    <a href="{{url('http://paatham.us/rte-application/public/applications/')}}{{$val}}" target="_blank"> {{$val}}</a><br>
                                    @endforeach
                                    @endif
                                </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->last_three_year_tot_income_rmk}} </td>
                                <td>
                                    @if($user->last_three_year_tot_income_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->last_three_year_tot_income_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->last_three_year_tot_income_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dc' || Auth::user()->user_role=='dse' )
                                <td>
                                    @if($user->last_three_year_tot_income_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->last_three_year_tot_income_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->last_three_year_tot_income_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->medium_vrfy == 2 && $user->type_of_school_vrfy == 2 && $user->supported_agency_name_vrfy == 2 && $user->authority_name_vrfy == 2 && $user->recognised_number_vrfy == 2 &&
                            $user->is_school_on_rented_vrfy == 2 && $user->are_school_building_used_vrfy == 2 && $user->school_total_area_vrfy == 2 && $user->school_building_area_vrfy == 2)
                            <tr>
                                <th colspan="3">
                                    <div class="card-header pb-0 pt-0">
                                        School Area and Format Details
                                    </div>

                                </th>
                            </tr>
                            @endif


                            @if($user->medium_vrfy == 2)

                            <tr>
                                <td class="si_no">28</td>
                                <td class="lable_name">Medium Of Education</td>
                                <td class="field_name" style="display: none;"> medium </td>
                                <td class="value_name">{{$user->medium}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->medium_rmk}} </td>
                                <td>
                                    @if($user->medium_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->medium_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->medium_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->medium_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->medium_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->medium_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->type_of_school_vrfy == 2)
                            <tr>
                                <td class="si_no">29</td>
                                <td class="lable_name">Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </td>
                                <td class="field_name" style="display: none;"> type_of_school </td>
                                <td class="value_name">{{$user->type_of_school}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->type_of_school_rmk}} </td>
                                <td>
                                    @if($user->type_of_school_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->type_of_school_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->type_of_school_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->type_of_school_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->type_of_school_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->type_of_school_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->supported_agency_name_vrfy == 2)
                            <tr>
                                <td class="si_no">30</td>
                                <td class="lable_name">Agency Name (if school has support) </td>
                                <td class="field_name" style="display: none;"> supported_agency_name </td>
                                <td class="value_name">{{$user->supported_agency_name}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->supported_agency_name_rmk}} </td>
                                <td>
                                    @if($user->supported_agency_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->supported_agency_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->supported_agency_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->supported_agency_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->supported_agency_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->supported_agency_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->agency_supported_percent_vrfy == 2)
                            <tr>
                                <td class="si_no">31</td>
                                <td class="lable_name">Support Percentage</td>
                                <td class="field_name" style="display: none;"> agency_supported_percent </td>
                                <td class="value_name"> {{$user->agency_supported_percent}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->agency_supported_percent_rmk}} </td>
                                <td>
                                    @if($user->agency_supported_percent_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->agency_supported_percent_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->agency_supported_percent_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->agency_supported_percent_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->agency_supported_percent_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->agency_supported_percent_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->authority_name_vrfy == 2)

                            <tr>
                                <td class="si_no">32</td>
                                <td class="lable_name">If School Is Recognised Mention Authority Name</td>
                                <td class="field_name" style="display: none;"> authority_name </td>
                                <td class="value_name">{{$user->authority_name}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->authority_name_rmk}} </td>
                                <td>
                                    @if($user->authority_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->authority_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->authority_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->authority_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->authority_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->authority_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->recognised_number_vrfy == 2)
                            <tr>
                                <td class="si_no">33</td>
                                <td class="lable_name">Recognition Number</td>
                                <td class="field_name" style="display: none;"> recognised_number </td>
                                <td class="value_name">{{$user->recognised_number}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->recognised_number_rmk}} </td>
                                <td>
                                    @if($user->recognised_number_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->recognised_number_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->recognised_number_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->recognised_number_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->recognised_number_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->recognised_number_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->is_school_on_rented_vrfy == 2)
                            <tr>
                                <td class="si_no">34</td>
                                <td class="lable_name">The School Has Its Own Building Or Is Working In A Rented Building ? </td>
                                <td class="field_name" style="display: none;"> is_school_on_rented </td>
                                <td class="value_name">{{$user->is_school_on_rented}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->is_school_on_rented_rmk}} </td>
                                <td>
                                    @if($user->is_school_on_rented_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->is_school_on_rented_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->is_school_on_rented_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->is_school_on_rented_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->is_school_on_rented_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->is_school_on_rented_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->are_school_building_used_vrfy == 2)
                            <tr>
                                <td class="si_no">35</td>
                                <td class="lable_name">Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development? </td>
                                <td class="field_name" style="display: none;"> are_school_building_used </td>
                                <td class="value_name">{{$user->are_school_building_used}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->are_school_building_used_rmk}} </td>
                                <td>
                                    @if($user->are_school_building_used_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->are_school_building_used_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->are_school_building_used_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->are_school_building_used_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->are_school_building_used_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->are_school_building_used_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_total_area_vrfy == 2)
                            <tr>
                                <td class="si_no">36</td>
                                <td class="lable_name">Total Area Of School</td>
                                <td class="field_name" style="display: none;"> school_total_area </td>
                                <td class="value_name">{{$user->school_total_area}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_total_area_rmk}} </td>
                                <td>
                                    @if($user->school_total_area_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_total_area_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_total_area_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->school_total_area_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_total_area_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_total_area_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->school_building_area_vrfy == 2)
                            <tr>
                                <td class="si_no">37</td>
                                <td class="lable_name">Area Of School Building Only</td>
                                <td class="field_name" style="display: none;"> school_building_area </td>
                                <td class="value_name">{{$user->school_building_area}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->school_building_area_rmk}} </td>
                                <td>
                                    @if($user->school_building_area_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->school_building_area_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->school_building_area_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->school_building_area_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_building_area_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->school_building_area_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            <!-- Enrollment -->

                            <tr>
                                @if($user->pre_elementary_class_vrfy == 2)
                                <td colspan="3">

                                    <div class="card-header pb-0 pt-0">
                                        Enrollment
                                    </div>

                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <span class="lable_name" style="display: none;">Enrollment</span>
                                    <span class="field_name" style="display: none;">pre_elementary_class</span>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>

                            @php
                            $tot = 38;
                            @endphp
                            @if(isset($user->class_names))
                            @foreach($user->class_names as $key => $val)
                            @if($key == 0)



                            @if($user->pre_elementary_class_vrfy == 2)


                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Pre-elementary class</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{$user->no_of_sections[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{$user->no_of_students[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @elseif($key == 1)
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Nursery</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{$user->no_of_sections[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{$user->no_of_students[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @elseif($key == 2)
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Kindergarder</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{$user->no_of_sections[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{$user->no_of_students[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @elseif($key == 3)
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Class 1</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{$user->no_of_sections[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{$user->no_of_students[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @else
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                @php
                                $kee = $key - 2;
                                @endphp
                                <td class="lable_name">Class {{$kee}}</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{$user->no_of_sections[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->pre_elementary_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{$user->no_of_students[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->pre_elementary_class_rmk}} </td>
                                <td>
                                    @if($user->pre_elementary_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->pre_elementary_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->pre_elementary_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif

                            @endif

                            @endforeach
                            @endif

                            <!-- <tr>
                                <td class="si_no">38</td>
                                <td class="lable_name">Pre-elementary class</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{$user->pre_elementary_class}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">39</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{$user->pre_no_of_section}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>

                            <tr>
                                <td class="si_no">40</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{$user->pre_no_of_student}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">41</td>
                                <td class="lable_name">class 1 to 5</td>
                                <td class="field_name" style="display: none;"> class_one_to_five </td>
                                <td class="value_name">{{$user->class_one_to_five}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">42</td>
                                <td class="lable_name">Number Of Section</td>
                                <td class="field_name" style="display: none;"> onefive_no_of_section </td>
                                <td class="value_name"> {{$user->onefive_no_of_section}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">43</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> onefive_no_of_student </td>
                                <td class="value_name">{{$user->onefive_no_of_student}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">44</td>
                                <td class="lable_name">class 6 to 8 </td>
                                <td class="field_name" style="display: none;"> class_six_to_eight </td>
                                <td class="value_name">{{$user->class_six_to_eight}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                   
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">45</td>
                                <td class="lable_name">Number Of Section</td>
                                <td class="field_name" style="display: none;"> sixeight_no_of_section </td>
                                <td class="value_name">{{$user->sixeight_no_of_section}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">46</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> sixeight_no_of_student </td>
                                <td class="value_name">{{$user->sixeight_no_of_student}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                   
                                </td>
                                @endif
                            </tr> -->


                            <!-- infrastructure details -->

                            <tr>
                                @if($user->no_of_class_vrfy == 2 && $user->avg_size_cls_room_vrfy == 2 && $user->no_of_office_room_vrfy == 2 && $user->avg_size_of_office_room_vrfy == 2
                                && $user->avg_size_of_store_room_vrfy == 2 && $user->no_of_princpal_room_vrfy == 2 && $user->avg_size_of_store_room_vrfy == 2
                                && $user->avg_size_of_principal_room_vrfy == 2 && $user->no_of_store_room_vrfy == 2 && $user->avg_size_of_store_room_vrfy == 2
                                && $user->avg_size_of_principal_room_vrfy == 2 && $user->no_of_princpal_room_vrfy == 2 && $user->avg_size_of_store_room_vrfy == 2 && $user->avg_size_of_principal_room_vrfy == 2
                                && $user->no_of_kitchen_room_vrfy == 2 && $user->avg_size_of_kitchen_room_vrfy == 2)
                                <td colspan="3">


                                    <div class="card-header pt-0 pb-0">
                                        Infrastructure Details
                                    </div>
                                </td>
                                @endif
                            </tr>

                            @if($user->no_of_class_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> No. Of Classes </td>
                                <td class="field_name" style="display: none;"> no_of_class </td>
                                <td class="value_name">{{$user->no_of_class}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->no_of_class_rmk}} </td>
                                <td>
                                    @if($user->no_of_class_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->no_of_class_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->no_of_class_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->no_of_class_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_class_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_class_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->avg_size_cls_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Average Size Of Classroom (in w*h)</td>
                                <td class="field_name" style="display: none;"> avg_size_cls_room </td>
                                <td class="value_name">{{$user->avg_size_cls_room}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->avg_size_cls_room_rmk}} </td>
                                <td>
                                    @if($user->avg_size_cls_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->avg_size_cls_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->avg_size_cls_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->avg_size_cls_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_cls_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_cls_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->no_of_office_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> No. Of Office Rooms</td>
                                <td class="field_name" style="display: none;"> no_of_office_room </td>
                                <td class="value_name">{{$user->no_of_office_room}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->no_of_office_room_rmk}} </td>
                                <td>
                                    @if($user->no_of_office_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->no_of_office_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->no_of_office_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->no_of_office_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_office_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_office_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->avg_size_of_office_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Average Size Of Office Rooms (in w*h)</td>
                                <td class="field_name" style="display: none;"> avg_size_of_office_room </td>
                                <td class="value_name">{{$user->avg_size_of_office_room}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->avg_size_of_office_room_rmk}} </td>
                                <td>
                                    @if($user->avg_size_of_office_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->avg_size_of_office_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->avg_size_of_office_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->avg_size_of_office_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_office_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_office_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->no_of_store_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> No. Of Store Rooms</td>
                                <td class="field_name" style="display: none;"> no_of_store_room </td>
                                <td class="value_name">{{$user->no_of_store_room}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->no_of_store_room_rmk}} </td>
                                <td>
                                    @if($user->no_of_store_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->no_of_store_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->no_of_store_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->no_of_store_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_store_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_store_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->avg_size_of_store_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Average Size Of Store Rooms (in w*h)</td>
                                <td class="field_name" style="display: none;"> avg_size_of_store_room </td>
                                <td class="value_name">{{$user->avg_size_of_store_room}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->avg_size_of_store_room_rmk}} </td>
                                <td>
                                    @if($user->avg_size_of_store_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->avg_size_of_store_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->avg_size_of_store_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->avg_size_of_store_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_store_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_store_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->no_of_princpal_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> No. Of Principal Rooms</td>
                                <td class="field_name" style="display: none;"> no_of_princpal_room </td>
                                <td class="value_name">{{$user->no_of_princpal_room}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->no_of_princpal_room_rmk}} </td>
                                <td>
                                    @if($user->no_of_princpal_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->no_of_princpal_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->no_of_princpal_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->no_of_princpal_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_princpal_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_princpal_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->avg_size_of_principal_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Average Size Of Principal Rooms (in w*h)</td>
                                <td class="field_name" style="display: none;"> avg_size_of_principal_room </td>
                                <td class="value_name">{{$user->avg_size_of_principal_room}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->avg_size_of_principal_room_rmk}} </td>
                                <td>
                                    @if($user->avg_size_of_principal_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->avg_size_of_principal_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->avg_size_of_principal_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->avg_size_of_principal_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_principal_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_principal_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->no_of_kitchen_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> No. Of Kitchen Rooms</td>
                                <td class="field_name" style="display: none;"> no_of_kitchen_room </td>
                                <td class="value_name">{{$user->no_of_kitchen_room}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->no_of_kitchen_room_rmk}} </td>
                                <td>
                                    @if($user->no_of_kitchen_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->no_of_kitchen_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->no_of_kitchen_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->no_of_kitchen_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_kitchen_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_kitchen_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->avg_size_of_kitchen_room_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Average Size Of Kitchen Rooms (in w*h)</td>
                                <td class="field_name" style="display: none;"> avg_size_of_kitchen_room </td>
                                <td class="value_name">{{$user->avg_size_of_kitchen_room}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->avg_size_of_kitchen_room_rmk}} </td>
                                <td>
                                    @if($user->avg_size_of_kitchen_room_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->avg_size_of_kitchen_room_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->avg_size_of_kitchen_room_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->avg_size_of_kitchen_room_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_kitchen_room_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->avg_size_of_kitchen_room_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif




                            <!-- other convienance -->
                            <tr>
                                @if($user->facilities_access_without_interrupted_vrfy == 2 && $user->all_teaching_material_list_vrfy == 2 && $user->all_sports_equipment_list_vrfy == 2)
                                <td colspan="3">

                                    <div class="card-header pb-0 pt-0">
                                        Other Convenience
                                    </div>

                                </td>
                                @endif
                            </tr>
                            @if($user->facilities_access_without_interrupted_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Does all facilities have access without interrupted? </td>
                                <td class="field_name" style="display: none;"> facilities_access_without_interrupted_ext </td>
                                <td class="value_name"> {{$user->facilities_access_without_interrupted}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->facilities_access_without_interrupted_rmk}} </td>
                                <td>
                                    @if($user->facilities_access_without_interrupted_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->facilities_access_without_interrupted_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->facilities_access_without_interrupted_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->facilities_access_without_interrupted_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->facilities_access_without_interrupted_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->facilities_access_without_interrupted_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->all_teaching_material_list_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">List All Teaching Materials</td>
                                <td class="field_name" style="display: none;"> all_teaching_material_list_ext </td>
                                <td class="value_name"> {{$user->all_teaching_material_list}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->all_teaching_material_list_rmk}} </td>
                                <td>
                                    @if($user->all_teaching_material_list_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->all_teaching_material_list_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->all_teaching_material_list_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->all_teaching_material_list_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->all_teaching_material_list_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->all_teaching_material_list_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->all_sports_equipment_list_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">List All Sports Equipments</td>
                                <td class="field_name" style="display: none;"> all_sports_equipment_list_ext </td>
                                <td class="value_name"> {{$user->all_sports_equipment_list}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->all_sports_equipment_list_rmk}} </td>
                                <td>
                                    @if($user->all_sports_equipment_list_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->all_sports_equipment_list_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->all_sports_equipment_list_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->all_sports_equipment_list_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->all_sports_equipment_list_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->all_sports_equipment_list_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->books_vrfy == 2)
                            <tr>
                                <th colspan="4">Books Facilities In Library</th>
                            </tr>
                            @endif
                            @if($user->books_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Books In Library</td>
                                <td class="field_name" style="display: none;"> books_ext </td>
                                <td class="value_name"> {{$user->books}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->books_rmk}} </td>
                                <td>
                                    @if($user->books_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->books_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->books_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->books_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->books_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->books_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif



                            @if($user->magazines_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Newspaper & Magzine</td>
                                <td class="field_name" style="display: none;"> magazines_ext </td>
                                <td class="value_name"> {{$user->magazines}}</td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->magazines_rmk}} </td>
                                <td>
                                    @if($user->magazines_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->magazines_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->magazines_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->magazines_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->magazines_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->magazines_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->type_of_water_facilities_vrfy == 2 && $user->no_of_water_supply_vrfy == 2 && $user->no_of_water_supply_vrfy == 2 && $user->no_of_water_supply_vrfy == 2)
                            <tr>
                                <th colspan="4">Water Facilities</th>
                            </tr>
                            @endif
                            @if($user->type_of_water_facilities_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Types Of Water Facilities</td>
                                <td class="field_name" style="display: none;"> type_of_water_facilities_ext </td>
                                <td class="value_name">{{$user->type_of_water_facilities}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->type_of_water_facilities_rmk}} </td>
                                <td>
                                    @if($user->type_of_water_facilities_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->type_of_water_facilities_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->type_of_water_facilities_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->type_of_water_facilities_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->type_of_water_facilities_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->type_of_water_facilities_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->no_of_water_supply_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Water Supply</td>
                                <td class="field_name" style="display: none;"> no_of_water_supply_ext </td>
                                <td class="value_name">{{$user->no_of_water_supply}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->no_of_water_supply_rmk}} </td>
                                <td>
                                    @if($user->no_of_water_supply_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->no_of_water_supply_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->no_of_water_supply_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->no_of_water_supply_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_water_supply_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->no_of_water_supply_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->water_facilities_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Add Relevant Evidence Attachement</td>
                                <td class="field_name" style="display: none;"> water_facilities_ext </td>
                                <td class="value_name">
                                    @if(isset($user->water_facilities))
                                    @foreach($user->water_facilities as $key => $val)
                                    <a href="{{url('http://paatham.us/rte-application/public/applications/')}}{{$val}}" target="_blank"> {{$val}}</a><br>
                                    @endforeach
                                    @endif
                                </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->water_facilities_rmk}} </td>
                                <td>
                                    @if($user->water_facilities_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->water_facilities_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->water_facilities_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dc' || Auth::user()->user_role=='dse' )
                                <td>
                                    @if($user->water_facilities_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->water_facilities_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->water_facilities_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->type_of_toilet_vrfy == 2 && $user->gents_toilet_vrfy == 2 && $user->ladies_toilet_vrfy == 2 && $user->cleanliness_vrfy == 2)

                            <tr>
                                <th colspan="4">Cleanliness Related Details</th>
                            </tr>
                            @endif
                            @if($user->type_of_toilet_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Type Of Toilets</td>
                                <td class="field_name" style="display: none;"> type_of_toilet_ext </td>
                                <td class="value_name">{{$user->type_of_toilet}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->type_of_toilet_rmk}} </td>
                                <td>
                                    @if($user->type_of_toilet_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->type_of_toilet_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->type_of_toilet_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->type_of_toilet_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->type_of_toilet_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->type_of_toilet_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->gents_toilet_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Seperate Toilet For Boys</td>
                                <td class="field_name" style="display: none;"> gents_toilet_ext </td>
                                <td class="value_name">{{$user->gents_toilet}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->gents_toilet_rmk}} </td>
                                <td>
                                    @if($user->gents_toilet_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->gents_toilet_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->gents_toilet_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->gents_toilet_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->gents_toilet_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->gents_toilet_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            @if($user->ladies_toilet_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Number Of Seperate Toilet For Girls</td>
                                <td class="field_name" style="display: none;"> ladies_toilet_ext </td>
                                <td class="value_name">{{$user->ladies_toilet}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->ladies_toilet_rmk}} </td>
                                <td>
                                    @if($user->ladies_toilet_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->ladies_toilet_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->ladies_toilet_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->ladies_toilet_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->ladies_toilet_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->ladies_toilet_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>

                            @endif
                            @if($user->cleanliness_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Add Relevant Evidence Attachement</td>
                                <td class="field_name" style="display: none;"> cleanliness_ext </td>
                                <td class="value_name">
                                    @if(isset($user->cleanliness))
                                    @foreach($user->cleanliness as $key => $val)
                                    <a href="{{url('http://paatham.us/rte-application/public/applications/')}}{{$val}}" target="_blank"> {{$val}}</a><br>
                                    @endforeach
                                    @endif
                                </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->cleanliness_rmk}} </td>
                                <td>
                                    @if($user->cleanliness_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->cleanliness_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->cleanliness_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dc' || Auth::user()->user_role=='dse' )
                                <td>
                                    @if($user->cleanliness_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->cleanliness_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->cleanliness_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif

                            <tr>
                                @if($user->principle_name_vrfy == 2)
                                <td colspan="3">

                                    <div class="card-header pt-0 pb-0">
                                        Teaching Staff Specialties
                                    </div>

                                </td>
                                @endif

                            </tr>
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Name Of Principal</td>
                                <td class="field_name" style="display: none;"> principle_name_ext </td>
                                <td class="value_name">{{$user->principle_name}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Father / Husband Or Wife Name</td>
                                <td class="field_name" style="display: none;"> p_f_h_w_name_ext </td>
                                <td class="value_name">{{$user->p_f_h_w_name}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)

                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Date Of Birth</td>
                                <td class="field_name" style="display: none;"> p_date_of_birth_ext </td>
                                <td class="value_name">{{$user->p_date_of_birth}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Educational Qualification</td>
                                <td class="field_name" style="display: none;"> p_education_qualification_ext </td>
                                <td class="value_name">{{$user->p_education_qualification}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Trainee Qualification</td>
                                <td class="field_name" style="display: none;"> trainee_qualification_ext </td>
                                <td class="value_name">{{$user->pri_trainee_qualification}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Teaching Experience</td>
                                <td class="field_name" style="display: none;"> teaching_experience_ext </td>
                                <td class="value_name">{{$user->pri_teaching_experience}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Class Handed Over</td>
                                <td class="field_name" style="display: none;"> class_handed_over_ext </td>
                                <td class="value_name">{{$user->class_handed_over}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Date Of Appointment</td>
                                <td class="field_name" style="display: none;"> date_of_appointment_ext </td>
                                <td class="value_name">{{$user->pri_date_of_appointment}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->principle_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Trained Or Untrained</td>
                                <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                <td class="value_name">{{$user->trained_untrained}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->principle_name_rmk}} </td>
                                <td>
                                    @if($user->principle_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->principle_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->principle_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif

                            <tr>
                                <td colspan="3">

                                    <div class="card-header pt-0 pb-0">
                                        Teachers Information
                                    </div>

                                </td>
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <span class="lable_name" style="display: none;">Teacher</span>
                                    <span class="field_name" style="display: none;">teacher_name_ext</span>
                                    @if($user->teacher_name_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->teacher_name_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->teacher_name_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @if(isset($user->teacher_name))
                            @foreach($user->teacher_name as $key => $val)
                            @if($user->teacher_name_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Name of Teacher</td>
                                <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->teacher_name_rmk}} </td>
                                <td>
                                    @if($user->teacher_name_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->teacher_name_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->teacher_name_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                            @endif

                            <tr>
                                <td colspan="3">

                                    <div class="card-header pt-0 pb-0">
                                        Curriculum and Syllabus
                                    </div>

                                </td>
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>
                                    <span class="lable_name" style="display: none;">Curriculum and Syllabus</span>
                                    <span class="field_name" style="display: none;">details_of_curriculum_ext</span>
                                    @if($user->details_of_curriculum_vrfy == 1)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->details_of_curriculum_vrfy == 2)
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif($user->details_of_curriculum_vrfy == 3)
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>

                            @if(isset($user->details_of_curriculum))
                            @foreach($user->details_of_curriculum as $key => $val)
                            @if($user->details_of_curriculum_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 8)</td>
                                <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                <td class="value_name">{{@$val}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->details_of_curriculum_rmk}} </td>
                                <td>
                                    @if($user->details_of_curriculum_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->details_of_curriculum_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->details_of_curriculum_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @if($user->details_of_curriculum_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Method Of Inspection Of Students</td>
                                <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                <td class="value_name">{{$user->method_of_inspection[$key]}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->details_of_curriculum_rmk}} </td>
                                <td>
                                    @if($user->details_of_curriculum_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->details_of_curriculum_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->details_of_curriculum_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif
                            @endforeach
                            @endif

                            @if($user->details_of_curriculum_vrfy == 2)
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Are the students of the school expected to take any board examination till class 8 ?</td>
                                <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                <td class="value_name">{{$user->school_board_exam_till_cls_eight}} </td>
                                @if(Auth::user()->user_role=='school' )
                                <td>{{$user->details_of_curriculum_rmk}} </td>
                                <td>
                                    @if($user->details_of_curriculum_vrfy == 1)
                                    <span>Pending</span>
                                    @elseif($user->details_of_curriculum_vrfy == 2)
                                    <span>Approved</span>
                                    @elseif($user->details_of_curriculum_vrfy == 3)
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                                <td>

                                </td>
                                @endif
                            </tr>
                            @endif



                            @if(Auth::user()->user_role=='dse' && ($user->application_status == 2 || $user->application_status == 3))
                            <tr>
                                <td colspan="3" style="text-align: right">
                                    <form action="{{url('application_fields_verified')}}" method="POST">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="text" hidden value="{{$user->applicationId}}" name="verified_applicationId" id="verified_applicationId">
                                        <input type="text" hidden value="{{$user->schoolId}}" name="verified_schoolId" id="verified_schoolId">
                                        <button class="btn-success" type="submit">Submit</button>
                                    </form>
                                </td>
                            </tr>
                            @endif
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
</div>
<div class="modal fade exampleModal1" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{url('acceptSchoolFields')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="exampleModalLabel1">Accept </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="message-text" class="col-form-label" id="acceptLabel">Remarks :</label>
                        <input type="text" hidden class="form-control" name="fieldName" id="acceptfieldName" />
                        <input type="hidden" class="form-control" name="schoolID" id="acceptschoolID" />
                        <input type="hidden" class="form-control" name="status" value="2" />

                        <input type="text" hidden value="" name="approve_applicationId" id="approve_applicationId">
                        <input type="text" hidden value="" name="approve_schoolId" id="approve_schoolId">
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_reason[]" type="text" class="form-control" multiple />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">

                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade exampleModal2" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel2">Reject </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('rejectSchoolFields')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                <div class="modal-body">

                    <div class="form-group">
                        <label for="message-text" class="col-form-label" id="rejectLabel">Remarks :</label>
                        <input type="text" hidden class="form-control" name="fieldName" id="rejectfieldName" />
                        <input type="hidden" class="form-control" name="schoolID" id="rejectschoolID" />
                        <input type="hidden" class="form-control" name="status" value="3" />

                        <input type="text" hidden value="" name="reject_applicationId" id="reject_applicationId">
                        <input type="text" hidden value="" name="reject_schoolId" id="reject_schoolId">

                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_reason[]" multiple type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">

                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>
</div>
<!-- <script src="../assets/js/vendor-all.min.js"></script>
    <script src="../assets/js/plugins/bootstrap.min.js"></script>
    <script src="../assets/js/ripple.js"></script>
    <script src="../assets/js/pcoded.min.js"></script> -->
@include('footer')
<!-- Apex Chart -->
<!-- <script src="../assets/js/plugins/apexcharts.min.js"></script> -->
<!-- custom-chart js -->
<!-- <script src="../assets/js/pages/dashboard-main.js"></script> -->
<script>
    $(".btn-success").click(function () {
        var text = $(this).closest("tr").find(".lable_name").text();
        var field_name = $(this).closest("tr").find(".field_name").text();
        var school_id = $('#school_id').val();
        var applicationId = $('#applicationId').val();
        var schoolId = $('#schoolId').val();


        // console.log(text);
        // console.log(field_name);
        // console.log(school_id);

        $('#acceptLabel').text(text);
        $('#acceptfieldName').val(field_name);
        $('#acceptschoolID').val(school_id)
        $('#approve_applicationId').val(applicationId)
        $('#approve_schoolId').val(schoolId)

    });
    $(".btn-danger").click(function () {
        var text = $(this).closest("tr").find(".lable_name").text();
        var field_name = $(this).closest("tr").find(".field_name").text();
        var school_id = $('#school_id').val();

        var applicationId = $('#applicationId').val();
        var schoolId = $('#schoolId').val();

        // console.log(text);
        // console.log(field_name);
        // console.log(school_id);

        $('#rejectLabel').text(text);
        $('#rejectfieldName').val(field_name);
        $('#rejectschoolID').val(school_id)

        $('#reject_applicationId').val(applicationId)
        $('#reject_schoolId').val(schoolId)


    });
</script>