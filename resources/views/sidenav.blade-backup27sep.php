<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar menu-light ">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div ">
            <div class="">
                <div class="main-menu-header">
                    @if(isset(Auth::user()->profile_photo_path) && Auth::user()->profile_photo_path != null)
                    <img class="img-radius" src="{{url(Auth::user()->profile_photo_path)}}" alt="User-Profile-Image" style="width:65px; height:62px;">
                    @else
                    <img class="img-radius" src="{{url('assets/images/default-user.png')}}" alt="User-Profile-Image" style="width:65px; height:62px;">
                    @endif
                    <div class="user-details pt-1">
                        <div id="more-details">{{Auth::user()->name}} <i class="fa fa-caret-down"></i></div>
                    </div>
                </div>
                <div class="collapse" id="nav-user-link">
                    <ul class="list-unstyled">
                        <li class="list-group-item">
                            <button data-toggle="modal" title="Change Password" id="ShowPasswordModal" data-target="#ChangePassword" class="btn btn-sm btn-light text-black btn-offset"><i class="feather icon-user m-r-5"></i>Change Password</i></button>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-fw fa-power-off"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="nav pcoded-inner-navbar ">
                @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='school' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='state' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa')
                <li class="nav-item">
                    <a href="{{url('dashboard')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-tachometer-alt"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='state' )
                <li class="nav-item">
                        <a href="{{url('report')}}" class="nav-link "><span class="pcoded-micon"><i class="fa fa-file" aria-hidden="true"></i></span><span class="pcoded-mtext">Reports</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='state' )
                <li class="" id="chat-details">
                        <a class="nav-link"><span class="pcoded-micon"><i class="fa fa-file" aria-hidden="true"></i></span><span class="pcoded-mtext">Chat </span><span class="mr-5 mt-1" style="float:right;"><i class="fa fa-caret-down"></i></span></a>
                </li>
                <ul id="nav-chat-link">
                    <li class="ml-2">
                            <a class="nav-link"><span class="pcoded-micon"><i class="fas fa-users" aria-hidden="true"></i></span><span class="pcoded-mtext">Create Group </span></a>
                    </li>
                    <li class="ml-2">
                            <a class="nav-link"><span class="pcoded-micon"><i class="fas fa-user-plus" aria-hidden="true"></i></span><span class="pcoded-mtext">Add User </span></a>
                    </li>
                    <li class="ml-2">
                            <a class="nav-link"><span class="pcoded-micon"><i class="fas fa-user" aria-hidden="true"></i></span><span class="pcoded-mtext">Users List </span></a>
                    </li>
                </ul>
                @endif
                @if(Auth::user()->user_role=='superadmin' )
                <br>
                <br>
                <li class="nav-item">
                    <a href="{{url('view-ftp')}}" class="nav-link "><span class="pcoded-micon"><i class="fa fa-file" aria-hidden="true"></i></span><span class="pcoded-mtext">FTP</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='dse')
                <li class="nav-item">
                    <a href="{{url('school')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-university"></i></span><span class="pcoded-mtext">Schools</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='state')
                <li class="nav-item">
                    <a href="{{url('userlist')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-map"></i></span><span class="pcoded-mtext">Users</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('fndSettings')}}" class="nav-link "><span class="pcoded-micon"><i class="fa fa-cog"></i></span><span class="pcoded-mtext">Setting</span></a>
                </li>
                @endif
                <!-- Group Chat Nav -->
                <li class="nav-item">
                @if(Auth::user()->user_role=='state')
                <a  class="nav-link " href="{{url('/group-user')}}">Add Group User</a>
                <a  class="nav-link " href="{{url('/users-list')}}">UsersList</a>
                <a  class="nav-link " href="{{url('/group/create')}}">Make group</a>
                <a  class="nav-link " href="{{url('/subscribe')}}">Join group</a>
              
                @php
                $user=\DB::table('users')->where('user_role','user')->first();
                if(@$user->chat_status==1)
                    $msg='EnabledChat';
                else
                   $msg='DisabledChat';
              
                @endphp
                <a class="btn btn-success" href="{{url('group/chat-disabled')}}">{{@$msg}}</a> 
                @endif
                </li>

               <!-- Group Chat Nav -->

                @if(Auth::user()->user_role=='school')
                <li class="nav-item">
                    <a href="{{url('application')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-envelope"></i></span><span class="pcoded-mtext">Application</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc')
                <li class="nav-item">
                    <a href="{{url('application-verify')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-check-square"></i></span><span class="pcoded-mtext">Applications</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='dse')
                <li class="nav-item">
                    <a href="{{url('payment-list')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-money-check-alt"></i></span><span class="pcoded-mtext">Payments</span></a>
                </li>
                @endif
                @if(Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='dse')
                <li class="nav-item">
                    <a href="{{url('appeal-list')}}" class="nav-link"><span class="pcoded-micon"><i class="fas fa-check-square"></i></span><span class="pcoded-mtext">Appeals</span></a>
                </li>
                @elseif(Auth::user()->user_role == 'school')
                @php $is_application_reject = DB::table('application_data')->where('school_id',Auth::user()->school_id)->whereBetween('application_status',[7,14] )->exists() @endphp
                @if($is_application_reject)
                <li class="nav-item">
                    <a href="{{url('appeal-list')}}" class="nav-link"><span class="pcoded-micon"><i class="fas fa-check-square"></i></span><span class="pcoded-mtext">Appeals</span></a>
                </li>
                @endif
                @endif
                @if(Auth::user()->user_role=='school')
                @php $is_school_certified = DB::table('application_data')->where('school_id', Auth::user()->school_id)->where('application_status', 6)->exists() @endphp
                @if($is_school_certified)
                <li class="nav-item">
                    <a href="{{url('school-certificate')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-address-card "></i></span><span class="pcoded-mtext">Certificate</span></a>
                </li>
                @endif
                @endif
                @if(Auth::user()->user_role=='student')
                <li class="nav-item">
                    <a href="{{url('student-application')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-envelope"></i></span><span class="pcoded-mtext">Application</span></a>
                </li>
                @elseif(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc')
                <li class="nav-item">
                    <a href="{{url('student-application')}}" class="nav-link "><span class="pcoded-micon"><i class="fas fa-envelope"></i></span><span class="pcoded-mtext">RTE Students</span></a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->
<div class="modal fade" id="exampleModal_image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Profile Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('User-Profile')}} " method="post"  enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="ml-2 col-sm-6">
                        <img src="https://placehold.it/80x80" id="preview" class="img-thumbnail">
                    </div>
                    <div class="ml-2 col-sm-6" style="max-width:100%">
                        <div id="msg"></div>
                       
                            <input id="profile-file-value" type="file" name="profile_pic_upload" class="file" accept="image/*" required>
                            <div class="input-group my-3">
                                <input type="text" class="form-control" name="profile_pic_upload"  disabled placeholder="Upload File" id="profile_pic_upload">
                                <div class="input-group-append">
                                    <button type="button" class="browse btn btn-primary">Browse...</button>
                                </div>
                                <p id="error1" style="display:none; color:#FF0000;">
                                    Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.
                                </p>
                                <p id="error2" style="display:none; color:#FF0000;">
                                    Maximum File Size Limit is 1MB.
                                </p>
            
                            </div>
                            <div class="alert alert-success" id="success" style="display:none">
                                <strong>Success!</strong> File submitted sucessfully .
                              </div>
                              <div class="alert alert-danger" id="error" style="display:none">
                                <strong>Error!</strong> File format not supported.
                              </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="upload" value="upload" id="profile_update" class="btn btn-block btn-dark"><i class="fa fa-fw fa-upload"></i> Upload</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
            </div>
        </div>
    </div>
</div>
<!-- %%%%%%%%%%%%%%%%%%%%% Model to change password  %%%%%%%%%%%%%%%%%%%%%%%%% -->
<div class="modal fade " id="ChangePassword" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true">
    <div class="modal-dialog ">
      <div class="modal-content">
        <form method="post" action="update-password" enctype="multipart/form-data">
          @csrf
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="myLargeModalLabel1">Change Password</h5>
            <button type="button" class="close" id="Close-Modal" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> Old Password:</label>
                <input type="text" name="id" hidden id="user_id">
                <input type="password" name="old_password" class="form-control rounded-0" />
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> New Password:</label>
                <input type="password" name="new_password" id="new_password" placeholder="Example@1234" class="form-control rounded-0" />
                <span id="msg_password" style="display: none;font-size: 15px;border-radius: 5px; text-align: center;"></span>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" id="SubmitPassword" value="Submit" class="btn btn-primary">
          </div>
        </form>
      </div>
    </div>
 </div>
  