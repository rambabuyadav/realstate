@include('header')
<style>
    @media print {
  #printPageButton {
    display: none;
  }
}
</style>
<!-- [ Main Content ] start -->
<div class="container-fluid" style="padding-top: 50px;">
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 shadow-lg   mt--6 bg-white rounded">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="card-header">
                    <!-- <h1>Here is all Payment Related Details</h1> -->
                    <div class="">
                        <a style="float: right; margin-left: 10px;" href="https://rte.jharkhand.gov.in/application"><button id="printPageButton" class="btn btn-sm btn-danger pull-right" title="cancel"><i class="fa fa-times" title="cancel"></i></button></a>
                        <button id="printPageButton" style="float: right;" onclick="window.print()" class="btn btn-sm btn-success pull-right" title="print document"><i class="fa fa-print"></i></button>
                        <span style="float: right;">
                            <form action="{{url('application-payment-form')}}" method="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="application_id" value="{{@$user->id}}">
                                <button id="printPageButton" style="margin-right: 10px;" type="submit" name="Payment" title="Payment" class="btn btn-sm btn-info pull-right"><i class="fa fa-credit-card"></i></button>
                            </form>
                        </span>
                        @if($count > 0 )
                        <span style="float: right;">
                            <!-- <form action="{{url('application-pending-form')}}" method="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="application_id" value="{{@$user->id}}"> -->
                                <button data-toggle="modal" data-target="#PendingPayment" style="margin-right: 10px;" type="submit" name="Pending_Paymnet" title="Pending Payment" class="btn btn-sm btn-warning pull-right"><i class="fas fa-money-check-alt"></i></button>
                                <!-- </form> -->
                            </span>
                            @endif
                        <span style="float: right;margin-right: 10px;">
                            <form action="{{url('application-challan-form')}}" method="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="application_id" value="{{@$user->id}}">
                                <button  style="float: right;"  data-toggle="modal" data-target="#Challan" style="margin-right: 10px;" type="submit" name="Challan" title="Challan" class="btn btn-sm btn-primary pull-right"><i class="far fa-file-alt"></i></button>
                            </form>
                        </span>
                    </div>
                </div>
                <input type="text" hidden value="{{@$user->application_data_id}}" name="applicationId" id="applicationId">
                <input type="text" hidden value="{{@$user->schoolId}}" name="schoolId" id="schoolId">
                <div class="card-body">
                    <input type="hidden" name="id" id="school_id" value="{{@$user->school_id}}">
                    <table class="table table-bordered table-sm  ">
                        <tr>
                            <th>SL NO.</th>
                            <th>FIELD NAME</th>
                            <th>VALUE</th>
                        </tr>
                        @php
                        $tot = 1;
                        $Y = 1;
                        @endphp
                        <tr>
<td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">School Name</td>
                            <td class="value_name">{{@$user->school_name}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Academic Session From Which Recognition Proposed</td>
                            <td class="value_name">{{@$user->recognised_by}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">District</td>
                            <td class="value_name">{{@$user->district}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Post Office</td>
                            <td class="value_name">{{@$user->post_office}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Village / Town</td>
                            <td class="value_name"> {{@$user->village_city}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Pin Code</td>
                            <td class="value_name">{{@$user->pincode}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Ph./Mb no. With STD Code</td>
                            <td class="value_name"> {{@$user->phone_with_std_code}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">FAX No. With STD Code</td>
                            <td class="value_name"> {{@$user->fax_with_std_code}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Email ID</td>
                            <td class="value_name">{{@$user->email}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Nearest Police Station</td>
                            <td class="value_name">{{@$user->police_station}} </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="card-header pt-0 pb-0">
                                    General Information
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Establishment Year</td>
                            <td class="value_name">{{@$user->estd_year}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">School Opening Date </td>
                            <td class="value_name">{{@$user->opening_date}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">trust/Society/Management Committee Name</td>
                            <td class="value_name"> {{@$user->society_name}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Is trust/society/management comittee registered?</td>
                            <td class="value_name">{{@$user->is_society_registered}}</td>
                            
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">The period until the registration of the Trust / Society / Management Committee is valid</td>
                            <td class="value_name"> @if($user->school_validation_till==1)
                                Life Time 
                            @endif
                            @if($user->school_validation_till==2)
                              Defined Date 
                            @endif</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Valid Date </td>
                            <td class="value_name"> {{@$user->society_registration_valid_upto}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of <br> members including their addresses on affidavit ?</td>
                            <td class="value_name"> {{@$user->evidence_of_non_proprietary_nature1}}</td>
                        </tr>
                        <!-- <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Add Relevant Evidence Attachement</td>
                            <td class="value_name">
                                @if(isset($user->evidence_of_non_proprietary_nature_ex))
                                @foreach($user->evidence_of_non_proprietary_nature_ex as $key => $val)
                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                @endforeach
                                @endif
                            </td>
                        </tr> -->
                        <tr>
                            <td colspan="3"><b> Chairman Information</b></td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Chairman Name </td>
                            <td class="value_name"> {{@$user->school_chairman_name}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Designation</td>
                            <td class="value_name">{{@$user->school_chairman_post}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Address</td>
                            <td class="value_name">{{@$user->school_chairman_address}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Phone No.</td>
                            <td class="value_name">{{@$user->school_chairman_phone_number}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Office</td>
                            <td class="value_name"> {{@$user->school_chairman_office}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Email ID</td>
                            <td class="value_name">{{@$user->school_chairman_email}} </td>
                        </tr>
                        <tr>
                            <td colspan="4"><b> 3 Years Total Income / Expenses / Surplus / Loss</b></td>
                        </tr>
                      
                        @for($i=0;$i < 3 ; $i++)
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Year</td>
                            <td class="value_name"> {{@$user->session_year[$i]}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Income</td>
                            <td class="value_name">{{@$user->income[$i]}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Expenses</td>
                            <td class="value_name"> {{@$user->expenses[$i]}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Surplus Money</td>
                            <td class="value_name"> {{@$user->surplus_money[$i]}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Reduce</td>
                            <td class="value_name"> {{@$user->reduced_money[$i]}}</td>
                        </tr>
                        <!-- <tr>
                            @php 
                                
                            $File_name = 'last_three_year_tot_income_files_'.$Y;
                            
                            @endphp
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Add Relevant Evidence Attachement</td>
                            <td class="value_name">
                                @if(isset($user->$File_name))
                                @foreach($user->$File_name as $key => $val)
                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                @endforeach
                                @endif
                            </td>
                        </tr> -->
                        @endfor
                        <tr>
                            <th colspan="4">
                                <div class="card-header pb-0 pt-0">
                                    School Area and Format Details
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Medium Of Education</td>
                            <td class="value_name">{{@$user->medium}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </td>
                            <td class="value_name">{{@$user->type_of_school}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Agency Name (if school has support) </td>
                            <td class="value_name">{{@$user->supported_agency_name}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Support Percentage</td>
                            <td class="value_name"> {{@$user->agency_supported_percent}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">If School Is Recognised Mention Authority Name</td>
                            <td class="value_name">{{@$user->authority_name}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Recognition Number</td>
                            <td class="value_name">{{@$user->recognised_number}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">The School Has Its Own Building Or Is Working In A Rented Building ? </td>
                            <td class="value_name">{{@$user->is_school_on_rented}}</td>
                        </tr>
                        <!-- <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Add relevent evidence attachement</td>
                            <td class="value_name">
                                @if(@isset($user->school_rent_deatail))
                                @foreach($user->school_rent_deatail as $key => $val)
                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                @endforeach
                                @endif
                            </td>
                        </tr> -->
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development? </td>
                            <td class="value_name">{{@$user->are_school_building_used}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Total Area Of School</td>
                            <td class="value_name">{{@$user->school_total_area}}</td>
                        </tr>
                        <tr>
                            
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Plot Number</td>
                                <td class="value_name">{{@$user->plot_number}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Khata Number</td>
                            <td class="value_name">{{@$user->khata_number}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Area Of School Building Only</td>
                            <td class="value_name">{{@$user->school_building_area}}</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            <div class="card-header pb-0 pt-0">
                                
                                Financial Details 
                            </div>
                            </td>
                        </tr>
                      
                        @if(isset($user->teaching_fee))
                        @foreach($user->teaching_fee as $key => $val)
                        <tr>
                            <th colspan="3" class="value_name">{{@$user->class_finance[$key]}}  </th>
                           </tr>
                           
                        <tr>
                            <td>{{$tot++}}</td>
                            <td class="lable_name">Tuition Fee </td>
                            <td class="value_name">{{@$user->teaching_fee[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Registration Fee </td>
                            <td class="value_name">{{@$user->registration_fee[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Admission Fee </td>
                            <td class="value_name">{{@$user->enrollment_fee[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Reserve Deposite </td>
                            <td class="value_name">{{@$user->security_money[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Annual Charges </td>
                            <td class="value_name">{{@$user->annual_charge[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Development Fee </td>
                            <td class="value_name">{{@$user->development_fee[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Other Charges </td>
                            <td class="value_name">{{@$user->other_charges[$key]}} </td>
                        </tr>
                        <tr>
                             <td>{{$tot++}}</td>
                            <td class="lable_name">Total </td>
                            <td class="value_name">{{@$user->total[$key]}} </td>
                        </tr>
                        <tr>
                            
                        </tr>
                          @endforeach
                            @endif
                        <!-- Enrollment -->
                        <tr>
                            <td colspan="3">
                                <div class="card-header pb-0 pt-0">
                                    Enrollment
                                </div>
                            </td>
                        </tr>
                       
                        @if(isset($user->className))
                        @foreach($user->className as $key => $val)
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                          
                            <td class="lable_name">{{$val}}</td>
                            <td class="value_name">{{@$val}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Section </td>
                            <td class="value_name">{{@$user->SectionNo[$key]}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Students</td>
                            <td class="value_name">{{@$user->studentNo[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Teacher Student Ratio</td>
                            <td class="value_name">{{@$user->teacher_student_ratio[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Teacher Student Assign</td>
                            <td class="value_name">{{@$user->teacher_student_assign[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Teacher</td>
                           
                            <td class="value_name">{{@$user->num_of_teacher[$key]}} </td>
                        </tr>
                       
                        @endforeach
                        @endif
                        <!-- infrastructure details -->
                        <tr>
                            <td colspan="4">
                                <div class="card-header pt-0 pb-0">
                                    Infrastructure Details
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> No. Of Classes </td>
                            <td class="value_name">{{@$user->no_of_class}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Average Size Of Classroom (in w*h)</td>
                            <td class="value_name">{{@$user->avg_size_cls_room}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> No. Of Office Rooms</td>
                            <td class="value_name">{{@$user->no_of_office_room}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Average Size Of Office Rooms (in w*h)</td>
                            <td class="value_name">{{@$user->avg_size_of_office_room}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> No. Of Store Rooms</td>
                            <td class="value_name">{{@$user->no_of_store_room}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Average Size Of Store Rooms (in w*h)</td>
                            <td class="value_name">{{@$user->avg_size_of_store_room}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> No. Of Principal Rooms</td>
                            <td class="value_name">{{@$user->no_of_princpal_room}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Average Size Of Principal Rooms (in w*h)</td>
                            <td class="value_name">{{@$user->avg_size_of_principal_room}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> No. Of Kitchen Rooms</td>
                            <td class="value_name">{{@$user->no_of_kitchen_room}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Average Size Of Kitchen Rooms (in w*h)</td>
                            <td class="value_name">{{@$user->avg_size_of_kitchen_room}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Does The School Has Boundary</td>
                            <td class="field_name" style="display: none;"> is_school_has_boundary </td>
                            <td class="value_name">{{@$user->is_school_has_boundary}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Does School Has CCTV ? </td>
                            <td class="field_name" style="display: none;"> is_school_cctv </td>
                            <td class="value_name">{{@$user->is_school_cctv}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Sport Ground ?
                            </td>
                            <td class="field_name" style="display: none;"> is_school_ground </td>
                            <td class="value_name">{{@$user->is_school_ground}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Dose The School Has 3 Start Rating In Water And Hygene Facilities ? </td>
                            <td class="field_name" style="display: none;"> is_school_water_hygene_facilities </td>
                            <td class="value_name">{{@$user->is_school_water_hygene_facilities}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Dose The School Has Fire Safty Facilities ? </td>
                            <td class="field_name" style="display: none;"> is_school_fire_safty_facilities </td>
                            <td class="value_name">{{@$user->is_school_fire_safty_facilities}} </td>
                        </tr>
                        
                        <!-- other convienance -->
                        <tr>
                            <td colspan="4">
                                <div class="card-header pb-0 pt-0">
                                    Other Convenience
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Does all facilities have access without interrupted? </td>
                            <td class="value_name"> {{@$user->facilities_access_without_interrupted}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">List All Teaching Materials</td>
                            <td class="value_name"> {{@$user->all_teaching_material_list}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">List All Sports Equipments</td>
                            <td class="value_name"> {{@$user->all_sports_equipment_list}}</td>
                        </tr>
                        <tr>
                            <th colspan="4">Books Facilities In Library</th>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Books In Library</td>
                            <td class="value_name"> {{@$user->books}}</td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Newspaper & Magzine</td>
                            <td class="value_name"> {{@$user->magazines}}</td>
                        </tr>
                        <!-- <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Add relevent evidence attachement</td>
                            <td class="value_name">
                                @if(isset($user->books_in_library))
                                @foreach($user->books_in_library as $key => $val)
                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                @endforeach
                                @endif
                            </td>
                        </tr> -->
                        <tr>
                            <th colspan="4">Water Facilities</th>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Types Of Water Facilities</td>
                            <td class="value_name">{{@$user->type_of_water_facilities}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Water Supply</td>
                            <td class="value_name">{{@$user->no_of_water_supply}} </td>
                        </tr>
                        <!-- <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Add Relevant Evidence Attachement</td>
                            <td class="value_name">
                                @if(isset($user->water_facilities))
                                @foreach($user->water_facilities as $key => $val)
                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                @endforeach
                                @endif
                            </td>
                        </tr> -->
                        <tr>
                            <th colspan="4">Cleanliness Related Details</th>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Type Of Toilets</td>
                            <td class="value_name">{{@$user->type_of_toilet}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Seperate Toilet For Boys</td>
                            <td class="value_name">{{@$user->gents_toilet}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Number Of Seperate Toilet For Girls</td>
                            <td class="value_name">{{@$user->ladies_toilet}} </td>
                        </tr>
                        <!-- <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Add Relevant Evidence Attachement</td>
                            <td class="value_name">
                                @if(isset($user->cleanliness))
                                @foreach($user->cleanliness as $key => $val)
                                <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                @endforeach
                                @endif
                            </td>
                        </tr> -->
                        <tr>
                            <td colspan="3">
                                <div class="card-header pt-0 pb-0">
                                    Principal Specialties
                                </div>
                            </td>
                            <td style="display: none;" class="lable_name"> Name of principal</td>
                            <td style="display: none;" class="value_name">{{@$user->principle_name}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Name of principal</td>
                            <td class="value_name">{{@$user->principle_name}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Father / Husband Or Wife Name</td>
                            <td class="value_name">{{@$user->p_f_h_w_name}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Date Of Birth</td>
                            <td class="value_name">{{@$user->p_date_of_birth}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name">Educational Qualification</td>
                            <td class="value_name">{{@$user->p_education_qualification}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Trainee Qualification</td>
                            <td class="value_name">{{@$user->pri_trainee_qualification}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Teaching Experience</td>
                            <td class="value_name">{{@$user->pri_teaching_experience}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Class Handed Over</td>
                            <td class="value_name">{{@$user->class_handed_over}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Date Of Appointment</td>
                            <td class="value_name">{{@$user->pri_date_of_appointment}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Trained Or Untrained</td>
                            <td class="value_name">{{@$user->trained_untrained}} </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="card-header pt-0 pb-0">
                                    Teachers Information
                                </div>
                            </td>
                        </tr>
                        @if(isset($user->teacher_name))
                        @foreach($user->teacher_name as $key => $val)
                        <tr>
                            <th colspan="4">
                                Tearcher {{$key+1}}
                            </th>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Name of Teacher</td>
                            <td class="value_name">{{@$val}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Teacher Father/Husband/Wife Name</td>
                            <td class="value_name">{{$user->teacher_f_h_w_name[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Date Of Birth</td>
                            <td class="value_name">{{$user->teacher_date_of_birth[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Education Qualification</td>
                            <td class="value_name">{{$user->teacher_education_qualification[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Trainee Qualification</td>
                            <td class="value_name">{{$user->teacher_trainee_qualification[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Teaching Experience</td>
                            <td class="value_name">{{$user->teacher_teaching_experience[$key]}} </td>
                        </tr>
                       
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Class Handed Over</td>
                            <td class="value_name">{{$user->teacher_class_handed_over[$key]}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Date of Appointment</td>
                            <td class="value_name">{{$user->teacher_appointment_date[$key]}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Trained or Untrained</td>
                            <td class="value_name">{{$user->teacher_trained_or_untrained[$key]}} </td>
                        </tr>
                      
                        
                        @endforeach
                        @endif
                        <tr>
                            <td colspan="3">
                                <div class="card-header pt-0 pb-0">
                                    Curriculum and Syllabus
                                </div>
                            </td>
                        </tr>
                        @if(isset($user->details_of_curriculum))
                        @foreach($user->details_of_curriculum as $key => $val)
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6)</td>
                            <td class="value_name">{{@$val}} </td>
                        </tr>
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Method Of Inspection Of Students</td>
                            <td class="value_name">{{@$user->method_of_inspection[$key]}} </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr>
                            <td class="si_no">{{$tot++}}</td>
                            <td class="lable_name"> Are the students of the school expected to take any board examination till class 6 ?</td>
                            <td class="value_name">{{@$user->school_board_exam_till_cls_eight}} </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    <!-- [ Main Content ] end -->
</div>


<div class="modal fade bd-example-modal-lg" id="PendingPayment" tabindex="-1" role="dialog" aria-labelledby="PendingPayment" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="PendingPayment">Pending Payment </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm ">
                    <thead>
                        <!-- <th>amount</th> -->
                        <!-- <th>status</th> -->
                        <!-- <th>ifms_office_code</th> -->
                        <!-- <th>security_code</th> -->
                        <!-- <th>payment_status_message</th> -->
                        <th>Tersury Code </th>
                        <th>Department Transaction Id </th>
                        <th>GRN </th>
                        <!-- <th> Date </th> -->
                        <th> Amount </th>
                        <th>Payment Mode </th>
                        <th>Payment </th>
                    </thead>
                    @if(isset($Transaction))
                    @foreach($Transaction as $key => $val)
                    <tr>
                        <input type="hidden" value="{{$Transaction[$key]->dept_tran_id}}" class="dept_tran_id" name="dept_tran_id" />
                       
                        <td>{{$Transaction[$key]->treas_code}}</td>
                        <td>{{$Transaction[$key]->dept_tran_id}}</td>
                        <td>
                            <input type="text" value="{{$Transaction[$key]->grn}}" class="grn" name="grn" class="form-control" />
                        </td>
                        <!-- <td>{{$Transaction[$key]->txn_date}}</td> -->
                        <td>{{$Transaction[$key]->txn_amount}}</td>
                        <td>{{$Transaction[$key]->pmode}}</td>
                        <td><button class="bg-success text-white Pending_Payment"> Verify </button> </td>
                    </tr>
                    @endforeach
                    @endif
                </table>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

@include('footer')

<script>
      $('.Pending_Payment').on('click', function (e) {
        // if(!confirm('Do you want to re-apply application ')){
        //       e.preventDefault();
        // }
         var dept_tran_id = $(this).closest("tr").find('.dept_tran_id').val();
          var grn = $(this).closest("tr").find('.grn').val();
          var dept_id = 'JEPC';
         // alert(add_info_5);
        $.ajax({
            url: '../pending_payment_paid',
            type: "POST",
            dataType: "json",
            data: {
                _token: '{!! csrf_token() !!}',
                'dept_tran_id': dept_tran_id,
                'grn': grn
             },
            success: function (encryption_code) {
                console.log('treasury ');
                console.log(encryption_code);
                // url: 'https://jkuberuat.gov.in/JEgras/JeGrasRestful.svc/SBIePayDoubleVerification',
                
                $.ajax({
                    url: 'https://finance.jharkhand.gov.in/jegrasDV/JeGrasRestful.svc/SBIePayDoubleVerificationED',
                    type: "POST",
                    dataType: "json",
                    data: {
                        _token: '{!! csrf_token() !!}',
                        'EncryptTxt':encryption_code['encryption'],
                        'REQDEPTID':dept_id
                    },
                    success: function (data) {
                        console.log('success');
                        console.log(data);
                    }
                });
               
                // $('#encpt').val(data['encryption']);
            }
        });
    });
  
</script>