@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
	<div class="loader-track">
		<div class="loader-fill"></div>
	</div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-content">
		<!-- [ breadcrumb ] start -->
		<div class="page-header">
			<div class="page-block">
				<div class="row align-items-center">
					<div class="col-md-12">
						<div class="page-header-title">
							<h5 class="m-b-10">User</h5>
						</div>
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a><i class="fa fa-file" aria-hidden="true"></i>
							<li class="breadcrumb-item">Edit User Profile</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- [ breadcrumb ] end -->
		<!-- [ Main Content ] start -->
		<div class="row">
			<div class="col-md-12">
				<form action="{{url('User-Profile')}}" method="POST" onsubmit="return validateForm()" name="myForm" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<!-- school details -->
					<div class="card">
						<div class="card-header">Change Password & Image</div>
						<div class="card-body">
							<div class="container">
								@if(count($errors) > 0)
									@foreach($errors->all() as $error)
										<div class="alert alert-danger">{{ $error }}</div>
									@endforeach
								@endif
								@foreach($userData as $user)
								@if( Session::has('success') )
								<div class="alert alert-success mx-3 mt-3" role="alert">
									{{ Session::get('success') }}
								</div>
								@endif
								<div class="row">
									<div class="col-md-4 col-4">
										<div class="form-group">
											<label for="file-input">
												@if(Auth::user()->profile_photo_path!='')
												<img class="img-radius" src="{{Auth::user()->profile_photo_path}}" alt="User-Profile-Image" style="width:65px; height:62px;">
												@else
												<img class="img-radius" src="/rte-application/assets/images/user/avatar.png" alt="User-Profile-Image" style="width:100px; height:100px;">
												@endif
											</label>
											<input id="file-input" class="form-control" name="profile_pic_upload" type="file" />
											<input type="hidden" name="id" value="{{$user->id}}">
										</div>
										<div class="alert alert-success" id="success" style="display:none">
											<strong>Success!</strong> File submitted sucessfully .
										  </div>
			
										  <div class="alert alert-danger" id="error" style="display:none">
											<strong>Error!</strong> File format not supported .
										  </div>
									</div>
									<!-- <div class="col-md-3">
											<div class="form-group">
												<label for="">User Name </label>
												<input class="form-control" type="text" value="{{$user->name}}" name="username" placeholder="Type your username">
															<span class="focus-input100" data-symbol="&#xf206;"></span>
											</div>
										</div> -->
									<div class="col-md-4 col-4">
										<div class="form-group">
											<label for="">Enter Old Password for Confirmation</label> </label>
											<input class="form-control" type="password" name="password_confirmation" placeholder="Example: Test@1234">
										</div>
									</div>
									<div class="col-md-4 col-4">
										<div class="form-group">
											<label for="">Enter New Password </label>
											<input class="form-control" type="password" name="password" placeholder="">
										</div>
									</div>
									<div class="col-md-6 col-6">
										<div class="form-group">
											<input type="submit" name="submit" value="Remove" class="btn btn-danger">
										</div>
									</div>
									<div class="col-md-6 col-6">
										<div class="form-group" style="text-align: center;float: right;">
											<input type="submit" id="submit_update" name="submit" value="Update" style="text-align: center;" class="btn btn-primary">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</form>
			</div>
		</div>
	</div>
</div>
@include('footer')
<div id="dropDownSelect1"></div>
<script>
	function validateForm() {
		var x = document.forms["myForm"]["password"].value;
		var y = document.forms["myForm"]["password_confirmation"].value;
		var z = document.forms["myForm"]["profile_pic_upload"].value;
		if (z == "") {
			document.getElementById("prof_pic").innerHTML = 'Please Enter Your Profile Picture';
			return false;
		}
		if (x !== y) {
			document.getElementById("conf-pass").innerHTML = 'Your conform password Not match';
			return false;
		}
	}
	// Rohit kumar code for user profile image validation
	(function ($) {
		$.fn.checkFileType = function (options) {
			var defaults = {
				allowedExtensions: [],
				success: function () { },
				error: function () { }
			};
			options = $.extend(defaults, options);
			return this.each(function () {
				$(this).on('change', function () {
					var value = $(this).val(),
						file = value.toLowerCase(),
						extension = file.substring(file.lastIndexOf('.') + 1);
					if ($.inArray(extension, options.allowedExtensions) == -1) {
						options.error();
						$(this).focus();
					} else {
						options.success();
					}
				});
			});
		};
	})(jQuery);
	$(function() {
		$('#file-input').checkFileType({
			allowedExtensions: ['png', 'jpeg', 'jpg','webp'],
			success: function() {
				$('#success').css('display', 'block');
       			 $('#error').css('display', 'none');
				$('#submit_update').removeAttr('disabled', 'false');
			},
			error: function() {
				$('#error').css('display', 'block');
       
        		$('#success').css('display', 'none');
				$('#submit_update').attr('disabled', 'true');
			}
		});
	});
</script>