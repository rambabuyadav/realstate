<style>
    .nav-item .nav-link {
        font-size: 15px;
    }
    .collapse ul.navbar-nav li a{
        color:white !important;
        font-size:16px;
    }
    .dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0; 
    }
</style>

<section id="nav" class="navigation" style="background:#228b22;padding:3px;">
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="navbar-container">
                </div>
                <nav id="navigation" class="navbar navbar-expand-lg navbar-light">
                    <div class="container-fluid">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse pl-5 nav-pl" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/index')}}">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/about-us')}}">About Us</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="organisation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Organisation
                                    </a>
                                    <div class="dropdown-menu bg-success" aria-labelledby="organisation">
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/organisation/management')}}">Management</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/organisation/senior-functionaries')}}">Senior Functionaries</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/organisation/disclosure-under-rti')}}">Disclosure Under RTI</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/organisation/programme-advisory-committee')}}">Programme Advisory Committee</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="constituents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Constituents and Departments
                                    </a>
                                    <div class="dropdown-menu constituents bg-success" aria-labelledby="constituents">
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/constituents/national-institute-education')}}">National Institute of Education</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/constituents/regional-institutes-education')}}">Regional Institutes of Education</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/constituents/central-institute-of-educational-technology')}}" target="">Central Institute of Educational Technology</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('/constituents/pss-vocational-education')}}">PSS Central Institute of Vocational Education</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="programmes" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Programmes
                                    </a>

                                    <div class="dropdown-menu programmes bg-success" aria-labelledby="programmes">
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('programmes/national-talent-examination')}}">National Talent Search Examination</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('programmes/all-india-school-education-survey')}}">All India School Education Survey</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('programmes/teacher-innovation-awards')}}">Teacher Innovation Awards</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('programmes/jn-national-science-exhibition')}}">Jawaharlal Nehru National Science Exhibition</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('programmes/media-programmes')}}">Media Programmes</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('programmes/research-grants-eric')}}">Research Grants (ERIC)</a>

                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="gallery" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Gallery
                                    </a>
                                    <div class="dropdown-menu gallery bg-success" aria-labelledby="gallery">
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('gallery/international-yoga')}}">International Yoga Day</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('gallery/kala-utsav')}}">Kala Utsav</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('gallery/foundationDay')}}">Foundation Day</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('gallery/eRaksha')}}">Launch of eRaksha</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('gallery/healthandwell')}}">Health and Wellness</a>
                                     </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="publication" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Publication
                                    </a>
                                    <div class="dropdown-menu publication mega-dropdown-menu bg-success" aria-labelledby="publication">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a class="dropdown-item  text-white p-2 bg-success" href="{{url('publication/list-publication')}}">List of Publication</a>
                                                <a class="dropdown-item  text-white p-2 bg-success" href="{{url('publication/ePub')}}">ePub</a>
                                                <a class="dropdown-item  text-white p-2 bg-success" href="{{url('publication/Flipbook')}}">Flipbook</a>
                                                <a class="dropdown-item  text-white p-2 bg-success" href="{{url('publication/textbook')}}">PDF(I-XII)</a>
                                                <a class="dropdown-item  text-white p-2 bg-success" href="{{url('publication/state-uts-eBook')}}">State/ Uts eBook (epub)</a>
                                                <a class="dropdown-item  text-white p-2 bg-success" href="{{url('publication/vocational-education')}}">Vocational Education</a>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="announcement" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Announcement
                                    </a>
                                    <div class="dropdown-menu announcement bg-success" aria-labelledby="announcement">
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('announcement/vacancies')}}">Vacancies</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('announcement/tenders')}}">Tenders</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('announcement/notices')}}">Notices</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('announcement/other_announcements')}}">Other Announcements</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('announcement/seminar')}}">Seminar/ Conference/ Workshop</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="contacts" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Contacts
                                    </a>
                                    <div class="dropdown-menu contacts bg-success" aria-labelledby="contacts">
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('contacts/persons-contact')}}">Persons to Contact</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('contacts/public-information-officers')}}">Public Information Officers</a>
                                        <a class="dropdown-item text-white p-2 bg-success" href="{{url('contacts/telephone-directory')}}">Telephone Directory</a>
                                    </div>
                                </li>
                                <li class="nav-item" style="border-right:none;">
                                    <a class="nav-link" href="{{url('/login')}}">Login</a>
                                </li>
                            </ul>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-dark my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<script>

</script>

