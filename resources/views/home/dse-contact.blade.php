@include('home.header')
<section class="dse-contactList">
    <div class="container">
        <h2 class="dse_headertxt">District Superintendent of Education (DSE) </h2>
        <table class="table table-bordered " style="font-size: 14px;">
            <thead>
                <th>SL. No</th>
                <th>District</th>
                <th>E-mail</th>
            </thead>
            <tbody>
                @foreach($DSE_contact as $k=>$DSE)
                <tr>
                    <td>{{$k+1}}.</td>
                    <td>{{$DSE->district_id}}</td>
                    <td>{{$DSE->email}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</section>
@include('home.footer')