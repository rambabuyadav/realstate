<style>
    #button {
        display: inline-block;
        background-color: #ccb618;
        width: 50px;
        height: 50px;
        text-align: center;
        border-radius: 4px;
        position: fixed;
        bottom: 12px;
        right: 30px;
        transition: background-color .3s, opacity .5s, visibility .5s;
        opacity: 0;
        visibility: hidden;
        z-index: 1000;
    }

    #button.show {
        opacity: 1;
        visibility: visible;
    }
    
    @media (min-width: 500px) {

        #button {
            margin: 30px;
        }
    }
</style>




<!-- <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 contactus">
                <h2>Contact Us</h2>
                <h4> </h4>
                <p> <i class="fa fa-phone"></i> Ph No: </p>
                <p> <i class="far fa-envelope"></i> Email ID: </p>
            </div>
            <div class="col-md-6 ">

                <div class="socialicons">
                    <h2>Social Links</h2>
                    <ul class="list-unstyled">
                        <li class="facebook"><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li class="instagram"><a href""><i class="fab fa-instagram"></i></a></li>
                        <li class="twitter"><a href=""><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer> -->
<section class="copyright text-center" style=" background: #2f2f2f; color: white; padding: 10px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p>Copyright © 2021. All Rights Reserved - Jharkhand Education Project Council | Developed By IT-Scient</p>

            </div>
        </div>
    </div>
</section>

<a id="button" class="show"><i class="fas fa-chevron-up" style="margin-top:20px;"></i></a>

<script>
    var btntop = $('#button');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btntop.addClass('show');
        } else {
            btntop.removeClass('show');
        }
    });

    btntop.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, '300');
    });
</script>