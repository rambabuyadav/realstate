<!DOCTYPE html>
<html lang="en">
<style>
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jharkhand Council For Education Research And Training | Home</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/home.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/aos.css')}}">
    <script type="text/javascript" src="{{url('assets/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
    <!--  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script> -->
    <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
    <!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <?php
    header('X-Content-Type-Options: nosniff');
    ?>
</head>
<style>

    .minusSign,
    .plusSign {
        cursor: pointer;
    }

    ul.list-inline .list-txt a {
        color: white;
    }

    body {
        overflow-x: hidden;
    }

    
    
</style>

<body>
    <section class="p-1" style="background:#2cb42c;">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <span class="text-white"><i class="fa fa-clock text-white"></i>&nbsp;<script type="text/javascript">
                        document.write ('<span id="date" style="">', new Date().toLocaleString(), ' </span>')
                        if (document.getElementById) onload = function ()
                        {
                            setInterval ("document.getElementById ('date').firstChild.data = new Date().toLocaleString()", 50)
                        }
                      </script> | </span>
                </div>
                <div class="col-md-9">
                    <ul class="list-inline pull-right mb-0">
                        <li class="list-inline-item list-txt"><a href="#mainContent"> Skip to main content |<a></li>
                        <li class="list-inline-item list-txt"><a href="#nav"> Skip to navigation |<a></li>
                        <li class="list-inline-item list-txt"><a href=""> Screen Reader Access |<a></li>
                        <li class="list-inline-item list-txt"><a href=""> Text size:<a><span class="minusSign" onclick="resizeText(-1)">A-</span>|<a href="">A |</a><span class="plusSign text-white" onclick="resizeText(1)">A+</span></li>
                        <li class="list-inline-item list-txt dropdown">
                            <a href="" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Language</a>
                            <div class="dropdown-menu">
                                <ul class="list">
                                    <li><a href="">en</a></li>
                                    <li><a href="">hi</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
    <script>
        
    </script>
    <header id="#myHeader" style="background:#f1f1f1;">
        <div class="container headerLogo">
            <div class="row">
                <div class="col-md-2 col-12">
                    <img class="logo display-img" src="{{url('assets/images/jcert-logo.jpg')}}" alt="logo-img">
                </div>
                <div class="col-md-10">
                    <h2 class="mt-4">Jharkhand Council For Educational & Research Training</h2>
                    <p class="text-center" style="font-size:20px;font-weight:600;margin-right:90px;">Department of School Education and Literacy, Jharkhand </p>
                </div>

                <!-- <div class="col-md-4 col-4">
                    <a href="https://www.jharkhand.gov.in/">
                        <img class="logo_emblem pull-right display-img" src="{{url('assets/images/jharkhand-govt-logo.png')}}" alt="logo-img">
                    </a>
                </div> -->
            </div>
        </div>
        @include('home.nav')
    </header>
    <script>
        function resizeText(multiplier) {
          if (document.body.style.fontSize == "") {
            document.body.style.fontSize = "1.0em";
          }
          document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.2) + "em";
        }
      </script>
    
