@include('home.header')
<style>
    .faq-bx {
        padding-bottom: 200px;
    }
    .faq-bx .panel {
        border: 1px solid #D0D0D0;
        padding: 12px 0px 12px 20px;
        border-radius: 4px;
        margin-bottom: 4px;
        cursor: pointer;
        background: #afc9f8;
    }
    .faq-bx .header-title {
        font-size: 20px;
    }
    .faq-bx .header-title a {
        padding: 0px 50px 0px 0px;
        border: 0;
        color: #000;
    }
    .faq-bx .content {
        margin: 10px 0 0 0;
    }
    .faq-bx .content p {
        font-size: 20px;
    }
    .faq-bx .content-body {
        border: 0;
    }
    .faq-bx .content-body a {
        font-size: 16px;
    }
    h2 {
        font-size: 25px;
    }
</style>
<div class="container">
    <br><br>
    <h2>FAQ'S</h2>
    <br><br>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="  m-b30 faq-bx" id="accordion1">
                        @foreach($FAQs as $k=>$FAQ)
                        <div class="panel">
                            <div>
                                <h6 class="header-title" data-toggle="collapse" href="#faq{{$k}}" class="collapsed" data-parent="#faq{{$k}}">
                                    <a> {{ $k+1}}) @php echo htmlspecialchars_decode(stripslashes($FAQ->heading)) @endphp </a>
                                </h6>
                            </div>
                            <div id="faq{{$k}}" class="content-body collapse">
                                <div class="content">
                                    @php echo htmlspecialchars_decode(stripslashes($FAQ->text)) @endphp
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('home.footer')