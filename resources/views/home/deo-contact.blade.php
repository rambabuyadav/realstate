@include('home.header')
<section class="dse-contactList">
    <div class="container">
        <h2 class="dse_headertxt">District Education Officer (DEO) </h2>
        <table class="table table-bordered " style="font-size: 14px;">
            <thead>
                <th>SL. No</th>
                <th>District</th>
                <th>E-mail</th>
            </thead>
            <tbody>
                @foreach($DEO_contact as $k=>$DEO)
                <tr>
                    <td>{{$k+1}}.</td>
                    <td>{{$DEO->district_id}}</td>
                    <td>{{$DEO->email}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</section>
@include('home.footer')