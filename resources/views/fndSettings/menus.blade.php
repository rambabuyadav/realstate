@include('header')
@include('sidenav')
@include('topbar')
<style>
.w-5 {
display: inline;
height: 30px;
}
</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Division </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings')}}"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="">Menus </a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 text-right pr-4">
                        <a href="" class="text-success" title="add "><button type="button" class="btn btn-sm btn-light btn-offset text-primary" title=" Add User"><i class="fa fa-plus text-success"> </i> </button></a>
                        <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
                        <button type="button" class="btn btn-danger btn-sm pull-right" onclick="window.location.reload()" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
                        <br>
                        <div class="form-block">
                            <div class="form-group">
                                <div class="form-content">

                                    <form action="{{url('fndSettings/menus/store')}}" method="post" >
                                        <input type="hidden" name="_token" value="{{ csrf_token()  }}">
                                        <div id="menu_detail">
                                            <div class="form-group row delete_row">
                                                <input type="hidden" name="menu_id[]" value="{{$data->id??''}}">    
                                                <div class="col-lg-3 col-md-3 col-sm-3" style=" width:20%;">
                                                    <!--group-->
                                                    <div class="form-group">
                                                        <label for="menu_title" class="control-label col-lg-8">Menu Title</label>
                                                        <input type="text" class="form-control" name="menu_title[]" value="{{$data->menu_title??''}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3" style=" width:20%;">
                                                    <!--group-->
                                                    <div class="form-group">
                                                    <label for="menu_url" class="control-label col-lg-8">Menu Url</label>
                                                    <input type="text" class="form-control" name="menu_url[]" value="{{$data->menu_url??''}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3" style=" width:20%;">
                                                    <!--group-->
                                                    <div class="form-group">
                                                        <label for="menu_url" class="control-label col-lg-8">Status</label>
                                                        <select class="form-control" name="status[]" id="status" >
                                                            @if(isset($data->gender))
                                                            <option value="{{$data->status}}" selected>{{($data->status==1)?'Active':'Inactive'}}</option>
                                                            @endif
                                                            <option value="1" >Active</option>
                                                            <option value="0" >Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @if (!isset($data))
                                                <div class="col-lg-3 col-md-3 col-sm-3" style=" width:20%;">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-white add_menu"  style="margin-bottom: -4rem;"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add Menu</button>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <br>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12" id="submenu_div" style="">

                                        </div>
                                        <div class="" style="float: right;">
                                            <!--group-->
                                            <!-- <div class="form-group">
                                                <label for="add_menu" class="control-label col-lg-4"><i class="fas fa-plus"></i>Add more menu</label>
                                            </div> -->
                                            
                                                <!--group-->
                                                <div class="form-group">
                                                    <input type="submit" name="submit" id="validatefrm" value="Save" class="btn btn-info" />
                                                </div>
                                            
                                        </div>
                                    </form>   
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    @if(count($menuList)!=0)
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
                        <table class="table table-bordered table-sm ">
                            <thead>
                                <tr>
                                    <th>Sl. </td>
                                    <th class="text-center">Navigation Pages</th>
                                    <th>Created At</th>
                                    <th style="text-align:center; width:8%;">Action</th>
                                </tr>
                            </thead>
                            <tbody id="resultTable">
                                @php //echo '<pre>'; print_r($menuList); exit; 
                                @endphp
                                @foreach($menuList as $key => $menu)
                                <tr>
                                    <td class='ID'> {{$key+1}}</td>
                                    <td class='Name'><a href="">{{$menu->menu_title}}</a></td>
                                    <td class='Date'>{{$menu->created_at}}</td>
                                    <td style="text-align:center;">
                                    <a href="{{url('fndSettings/menus/edit/'.$menu->id)}}"><button class="btn btn-light text-primary btn-sm " onclick=""><i class="fa fa-edit " title="Edit"></i></button></a>
                                    <a href="{{url('fndSettings/menus/delete/'.$menu->id)}}"><button class="btn btn-light text-danger btn-sm"><i class="fa fa-trash" title="Delete"></i></button></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div> 
            </div>
        </div>
        <!-- [ Main Content ] end -->
        <!-- [ Adv Search Model ] -->

        <!-- [ Adv Search End ] -->
    </div>
</div>
@include('footer')

<script type="text/javascript">
    $(document).ready(function() {
        $('#division').on('change', function() {
            var stateID = $(this).val();
            console.log(stateID);
            var options = '<option value="-1">Select District </option>';
            if (stateID) {
                $.ajax({
                    url: 'DGR/block/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log('Block ')
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                        var id = data[i]['id'];
                        var value_set_name = data[i]['display_value'];
                        //do something with k or data...
                        options += '<option value="' + id + '">' + value_set_name + '</option>';
                        }
                        $('#district').html(options);
                        $('#block').html('<option value="-1">Select Block  </option>');
                    }
                });
            } else {
                $('#district').empty();
            }
        });
    });
    var i = 1;
    $('.add_menu').on('click', function() {//alert('add submenu');
        i++;
        var data = '<div class="form-group row delete_row">'+
                        
                        '<input type="text" class="form-control" name="menu_title[]" value="" placeholder=Menu title" style="float: left; width:20%;margin-right: 10px;"/>'+
                        '<input type="text" class="form-control" name="menu_url[]" value="" placeholder="Menu Url" style="float: left; width:20%;margin-right: 10px;"/>'+
                        '<select class="form-control" name="status[]" id="status" style="float: left; width:20%;margin-right: 10px;">'+
                            '<option >--Status--</option>'+
                            '<option value="1">Active</option>'+
                            '<option value="0">Inactive</option>'+
                        '</select>'+
                        '<div class="col-lg-3 col-md-3 col-sm-3" style="float: left; width:20%;">'+
                            '<button type="button" class="btn btn-danger btn_remove" ><i class="fas fa-minus"></i>&nbsp;&nbsp;Remove</button>'+
                        '</div>'+
                    '</div>';
                    $('#menu_detail').append(data);
    })
    $(document).on('click', '.btn_remove', function () {
        //alert("jhgtjhgjhghj");
        $(this).closest('.delete_row').remove();
    });

    function editMenu(id){
        $("#menu_detail").html('');
            $.ajax({
                url:"{{url('fndSettings/menus/edit')}}"+'/'+this.value,
                type: "GET",
                dataType : 'json',
                success: function(result){//echo result;
                     
                }
            });
    }

</script>