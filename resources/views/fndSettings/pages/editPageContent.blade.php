@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href=""><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings')}}"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings/pages')}}">Pages</a></li>
                            <li class="breadcrumb-item"><a href="">Add PageData</a></li>
                        </ul>
                        <!-- <div class="alert alert-success">
                          
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('/fndSettings/pages/insertPageData')}}" method="POST" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="id" value="" />
                    <input type="hidden" class='text-block' name="route_id" value="{{$data->route_id??''}}" />
                    <input type="hidden" class='text-block' name="page_id" value="{{$data->page_id??''}}" />
                    <input type="hidden" class='text-block' name="page_name" value="{{$data->page_name??''}}" />
                    <input type="hidden" class='text-block' name="page_data_id" value="{{$data->page_data_id??''}}" />
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">Add Page Data</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="page_type">Page Type</label>
                                            <input required  name="page_type" class="form-control rte_input" value="{{$data->page_type??''}}" style="resize: none;">
                                        </div>
                                    </div>
                                    <hr style="background-color: #e2e5e8 !important; width: 100%;">    
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="icon_name">Icon Name</label>
                                            <input  type="text" name="icon_name" class="form-control rte_input" value="{{$data->icon_name??''}}">
                                        </div>
                                    </div>
                                    <hr style="background-color: #e2e5e8 !important; width: 100%;">    
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="image" class="control-label">Image</label>
                                            <input type="file" class="form-control" name="image[]" id="image" accept=".png,.jpg,.jpeg" multiple />
                                            <!-- 20,97,152 -->
                                            @php //echo $data->image_name;echo '====<pre>'; print_r($data); exit;
                                                if(isset($data->image_name)){ //echo 'eeee';exit;
                                                //$image = explode(",",$data->image_name);
                                                $image = json_decode($data->image_name);
                                                foreach($image as $key => $value){
                                                @endphp
                                                <a href="{{url('fndSettings/pages/getImage/').'/'.$data->id.'/'.$key}}" title="{{$value}}"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;</a>    
                                                @php    
                                                }
                                            }
                                            @endphp
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="link">Image Link</label>
                                            <input  type="text" name="link" class="form-control rte_input" value="{{$data->link??''}}">
                                        </div>
                                    </div>
                                    <hr style="background-color: #e2e5e8 !important; width: 100%;">    
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="heading">Heading</label>
                                            <input  type="text" name="heading" class="form-control rte_input" value="{{$data->heading??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="text">Text</label>
                                            <textarea   name="text" class="form-control rte_input" style="resize: none;">{{$data->text??''}}</textarea>
                                        </div>
                                    </div>
                                    <hr style="background-color: #e2e5e8 !important; width: 100%;">    
                                </div>
                                <div class="row" id="list_div">
                                    @php 
                                    $list_type = json_decode($data->list_type);
                                    $list_format = json_decode($data->list_format);
                                    $list_icon = json_decode($data->list_icon);
                                    $list_text = json_decode($data->list_text);
                                    $list_url = json_decode($data->list_url);
                                    $list_title = json_decode($data->list_title);
                                    @endphp
                                    @if(is_array($list_text)) 
                                    @foreach($list_text as $key => $list)
                                    <div class="form-group row delete_row" style="width:90%;">   
                                        <input  type="hidden" name="list_type[]" class="form-control rte_input" value="{{$list_type[$key]??''}}">
                                        <input  type="hidden" name="list_format[]" class="form-control rte_input" value="{{$list_format[$key]??''}}">
                                        <input  type="hidden" name="list_icon[]" class="form-control rte_input" value="{{$list_icon[$key]??''}}">
                                        <div class="col-md-3" style="">
                                            <div class="form-group">
                                                <label for="list_text">List Text</label>
                                                <input  type="text" name="list_text[]" class="form-control rte_input" value="{{$list_text[$key]??''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="">
                                            <div class="form-group">
                                                <label for="list_url">List Url</label>
                                                <input  type="text" name="list_url[]" class="form-control rte_input" value="{{$list_url[$key]??''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="">
                                            <div class="form-group">
                                                <label for="list_title">List Title</label>
                                                <input  type="text" name="list_title[]" class="form-control rte_input" value="{{$list_title[$key]??''}}">
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- <p><button type="button" id="btnAdd" class="btn btn-primary" style="margin-top:25px;">Add More&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></button></p> -->
                                    @else  
                                    <div class="form-group row delete_row" style="width:90%;">   
                                        <input  type="hidden" name="list_type[]" class="form-control rte_input" value="{{$data->list_type??''}}">
                                        <input  type="hidden" name="list_format[]" class="form-control rte_input" value="{{$data->list_format??''}}">
                                        <input  type="hidden" name="list_icon[]" class="form-control rte_input" value="{{$data->list_icon??''}}">
                                        <div class="col-md-3" style="">
                                            <div class="form-group">
                                                <label for="list_text">List Text</label>
                                                <input  type="text" name="list_text[]" class="form-control rte_input" value="{{old(' list_text[]')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="">
                                            <div class="form-group">
                                                <label for="list_url">List Url</label>
                                                <input  type="text" name="list_url[]" class="form-control rte_input" value="{{old(' list_url[]')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="">
                                            <div class="form-group">
                                                <label for="list_title">List Title</label>
                                                <input  type="text" name="list_title[]" class="form-control rte_input" value="{{old(' list_title[]')}}">
                                            </div>
                                        </div>
                                    </div>
                                    @endif 
                                    <p><button type="button" id="btnAdd" class="btn btn-primary" style="margin-top:25px;">Add More&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></button></p>
                                </div>
                                <div class="row">   
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select class="form-control" name="status" required >
                                                @if(isset($data->status))
                                                <option value="{{$data->status}}" selected>{{($data->status==1)?'Active':'Inactive'}}</option>
                                                @endif
                                                <option value="1" >Active</option>
                                                <option value="0" >Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-4"></div>
                                    <div class="col-md-12 col-4">
                                        <input class="btn btn-primary btn_submit " type="submit" name="submit" value="Save" style="display:flex;justify-content:center;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')
<script>
     $(document).ready(function () {
        var i = 1;
        $('#btnAdd').click(function () {
            i++;
            var data = `
            <div class="form-group row delete_row" style="width:90%;">   
                                    <input  type="hidden" name="list_type[]" class="form-control rte_input" value="{{old(' list_type[]')}}">
                                    <input  type="hidden" name="list_format[]" class="form-control rte_input" value="{{old(' list_format[]')}}">
                                    <input  type="hidden" name="list_icon[]" class="form-control rte_input" value="{{old(' list_icon[]')}}">
                                    <div class="col-md-3" style="">
                                        <div class="form-group">
                                            <label for="list_text">List Text</label>
                                            <input  type="text" name="list_text[]" class="form-control rte_input" value="{{old(' list_text[]')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="">
                                        <div class="form-group">
                                            <label for="list_url">List Url</label>
                                            <input  type="text" name="list_url[]" class="form-control rte_input" value="{{old(' list_url[]')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="">
                                        <div class="form-group">
                                            <label for="list_title">List Title</label>
                                            <input  type="text" name="list_title[]" class="form-control rte_input" value="{{old(' list_title[]')}}">
                                        </div>
                                    </div>
                                </div>
                                <p><button type="button" id="btnRemove" class="btn btn-danger btn_remove" style="margin-top:25px;">Remove</button>
                                </div>
										
   											
									
							         `;
            $('#list_div').append(data);
        });
    });
    $(document).on('click', '.btn_remove', function () {
        //alert("jhgtjhgjhghj");
        $(this).closest('div.delete_row').remove();
    });
</script>