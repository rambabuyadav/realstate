@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href=""><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings')}}"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings/pages')}}">Pages</a></li>
                            <li class="breadcrumb-item"><a href="">Add Page</a></li>
                        </ul>
                        <!-- <div class="alert alert-success">
                          
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('/fndSettings/pages/insertPageUrl')}}" method="POST" >
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="id" value="{{$data['page']->id??''}}" />
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">Add Route</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="route_name">Route Name</label>
                                            <input required  name="route_name" class="form-control rte_input" value="{{$data['route']->route_name??''}}" style="resize: none;">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="route_unique_code">Route Unique Code</label>
                                            <input required type="text" name="route_unique_code" class="form-control rte_input" value="{{$data['route']->unique_code??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="page_unique_code">Page Unique Code</label>
                                            <input required type="text" name="page_unique_code" class="form-control rte_input" value="{{$data['page']->unique_code??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="page_name">Page Name</label>
                                            <input required type="text" name="page_name" class="form-control rte_input" value="{{$data['page']->name??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="route_url">Url</label>
                                            <input required type="text" name="route_url" class="form-control rte_input" value="{{$data['route']->url??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="route_url">Description</label>
                                            <textarea name="description" class="form-control rte_input" style="resize: none;">{{$data['page']->description??''}}</textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="route_status">Status</label>
                                            <select class="form-control" name="status" >
                                                @if(isset($data['page']->status))
                                                <option value="{{$data['page']->status}}" selected>{{($data['page']->status==1)?'Active':'Inactive'}}</option>
                                                @endif
                                                <option value="1" >Active</option>
                                                <option value="0" >Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-4"></div>
                                    <div class="col-md-12 col-4">
                                        <input class="btn btn-primary btn_submit " type="submit" name="submit" value="Save" style="display:flex;justify-content:center;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')