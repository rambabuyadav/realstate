@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href=""><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings')}}"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings/pages')}}">Pages</a></li>
                            <li class="breadcrumb-item"><a href="">Add Page Content</a></li>
                        </ul>
                        <!-- <div class="alert alert-success">
                          
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('/fndSettings/pages/updatePageContent')}}" method="POST" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="id" value="{{$data->id??''}}" />
                    <input type="hidden" class='text-block' name="route_id" value="{{$data->route_id??''}}" />
                    <input type="hidden" class='text-block' name="page_id" value="{{$data->page_id??''}}" />
                    <input type="hidden" class='text-block' name="page_name" value="{{$data->name??''}}" />
                    <!--  -->
                    <input type="hidden" name="page_type" class="form-control rte_input" value="{{$data->page_type??''}}">
                    <input type="hidden" name="fa_icon" class="form-control rte_input" value="{{$data->fa_icon??''}}">
                    <input type="hidden" name="fa_class" class="form-control rte_input" value="{{$data->fa_class??''}}">
                    <input type="hidden" name="icon_name" class="form-control rte_input" value="{{$data->icon_name??''}}">

                    <div class="card">
                        <div class="card-header">Add Page Content</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="page_type">Page Type</label>
                                            <input required  name="" class="form-control rte_input" value="{{$data->page_type??''}}" style="resize: none;" disabled>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="image" class="control-label">Image</label>
                                            <input type="file" class="form-control" name="image[]" id="image" accept=".png,.jpg,.jpeg" multiple />
                                            <!-- 20,97,152 -->
                                            @php if(isset($data->image_name)){ 
                                                $image = explode(",",$data->image_name);
                                                foreach($image as $key => $value){
                                                @endphp
                                                <a href="{{url('fndSettings/pages/getImage/').'/'.$data->id.'/'.$key}}" title="{{$value}}"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;</a>    
                                                @php    
                                                }
                                            }
                                            @endphp
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="link">Link</label>
                                            <input  type="text" name="link" class="form-control rte_input" value="{{$data->link??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="heading">Heading</label>
                                            <input  type="text" name="heading" class="form-control rte_input" value="{{$data->name??''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="text">Text</label>
                                            <textarea   name="text" class="form-control rte_input" style="">{{$data->text??''}}</textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select class="form-control" name="status" required >
                                                @if(isset($data->status))
                                                <option value="{{$data->status}}" selected>{{($data->status==1)?'Active':'Inactive'}}</option>
                                                @endif
                                                <option value="1" >Active</option>
                                                <option value="0" >Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-4"></div>
                                    <div class="col-md-12 col-4">
                                        <input class="btn btn-primary btn_submit " type="submit" name="submit" value="Save" style="display:flex;justify-content:center;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')