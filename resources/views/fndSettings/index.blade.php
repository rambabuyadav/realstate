@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Settings </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">All Settings</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">
                    <div class="row" ng-show="views.list">
                        <div class="col-md-12 row" style="margin-top: 17px;">
                            <h3 class="card-title" style="margin-left: 20px;"></h3>
                        </div>
                        @if (Auth::user()->user_role == 'superadmin')
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('fndSettings/menus')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'administrator'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="schoolsList('StudentLoginStatsshow')">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Menu Settings</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('fndSettings/submenus')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="StaffLoginReportsshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-import"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Submenu Settings</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('fndSettings/pages')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="StaffLoginReportsshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-import"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Page Settings</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('fndSettings/pages/manage')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="StaffLoginReportsshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-import"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Manage Pages </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>
                <div class="modal fade" id="gfgmodal" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="gfgmodallabel">Selected row</h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="GFGclass" id="divGFG"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
@include('footer')