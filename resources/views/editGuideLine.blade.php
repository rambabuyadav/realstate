@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Edit Guide Line</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="../fndSettings"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="../GuideLine"> Guide Line</a></li>
                            <li class="breadcrumb-item"><a href="">Edit Guide Line</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('Update-GuideLine')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="id" value="{{@$GuideLine->id}}">
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">Edit Guide Line</div>
                        <div class="card-body">
                            @include('message-flash')
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><b>Heading</b></label>
                                    <input required type="text" name="heading" class="form-control rte_input" value="{{@$GuideLine->heading}}" style="height: 100%;width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><b>Document</b></label>
                                    <input type="file" accept=".pdf, .xls, .xlsx, .jpg, .png , .jpeg " name="document" class="form-control rte_input"  >
                                    
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{@$GuideLine->document}}" target="_blank"> {{@$GuideLine->document}} </a>
                                         
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9 col-4"></div>
                                <div class="col-md-3 col-4">
                                    <input class="btn btn-primary btn_submit" type="submit" name="submit" value="Update" style="float:right" />
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>