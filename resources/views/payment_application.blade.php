@include('header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Private School Recognition System | School Registration</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/registration.css')}}">
    <script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <!-- CSS -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500"> -->
    <!-- <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/form-elements.css"> -->
    <!-- <link rel="stylesheet" href="assets/school_register_assets/css/style.css"> -->
    <style>
        label {
            float: left;
        }
        .card-header {
            font-size: 35px;
            color: #000000;
            font-weight: 700 !important;
        }
    </style>
</head>
<body>
    <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <form id="tabs" name="application_form" action="{{url('application_payment')}}" onsubmit="return validation()" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card ">
                        <div class="card-header">
                            <strong>Application Payment</strong>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <input type="hidden" id="applicationId" name="applicationId" value="{{@$School_Data->id}}">
                                            <label for="">School Name </label>
                                            <input type="text" maxlength="48" value="{{@$School_Data->school_name}}" name="school_name" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Post Office </label>
                                            <input type="text" value="{{@$School_Data->post_office}}" name="post_office" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Village / Town </label>
                                            <input type="text" maxlength="48" value="{{@$School_Data->village_city}}" name="village_town" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Pin Code</label>
                                            <input type="text" value="{{@$School_Data->pincode}}" name="pin_code" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone / Mobile Number </label>
                                            <input type="text" value="{{@$School_Data->phone_with_std_code}}" name="phone_with_std_code" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""> Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </label>
                                            <input type="text" value="{{@$School_Data->type_of_school}}" name="type_of_school" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Amount </label>
                                            @if($School_Data->type_of_school=='1-5')
                                            <input type="text" value="12500" id="amount" name="amount" maxlength="38" class="form-control rte_input" readonly>
                                            @elseif($School_Data->type_of_school=='1-8')
                                            <input type="text" value="25000" id="amount"  name="amount" maxlength="38" class="form-control rte_input" readonly>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Select Treasury</label>
                                            <select class="form-control rte_input" id="treasury" required name="treasury">
                                                <option value="">--Select--</option>
                                                @foreach($Treasurys as $Treasury)
                                                <option value="{{$Treasury->id}}">{{$Treasury->treasury_title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">select DDO</label>
                                            <input type="hidden" id="Treasury_Code" name="Treasury_Code" />
                                            <select class="form-control rte_input" name="ddo" required id="ddo">
                                                <option value="">--Select--</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="button" class="btn btn-danger" name="Cancle" value="Cancel" />
                                        <input type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="Payment_Submit" name="Submit" value="Submit" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
<!-- phone
    position: fixed;
    top: 20%;
    left: 0%;
    width: 100%; -->
<!-- Button trigger modal -->
<div id="exampleModal_payment" style="position: fixed;top: 20%;left: 0%; width: 100%;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Alert </h5>
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- <form name="eGrassClient" method="post" action="https://jkuberuat.gov.in/jegras/DEPTUATTEST/UATpaymentPG.aspx"> -->
            <form name="eGrassClient" method="post" action="https://finance-jharkhand.gov.in/jegras/payment.aspx">
                <!-- <div class="modal-body"> -->
                    <input type="hidden" name="requestparam" id="encpt" />
                   
                <!-- </div> -->
                <div class="modal-footer" >
                    <center>
                        <input type="submit" name="submit" class="btn btn-primary" value="Proceed To Payment">
                    </center>
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                </div>
            </form>
        </div>
    </div>
</div>
    @include('footer')
    <script>
        $(document).ready(function () {
            $('#exampleModal_payment').hide();
            var path = window.location.pathname;
                    console.log(path);
                    var splitUrl = path.split('/');
                    console.log(splitUrl);
                    

            $('select[name="treasury"]').on('change', function () {
                var stateID = $(this).val();
                var options = '<option value="">---Select---</option>';
                if (stateID) {
                    if(splitUrl[2] == "payment_form"){
                        var url_for_ddo = '../DDO/'+stateID;
                     }else{
                        var url_for_ddo = 'DDO/'+stateID;
                    }
                    $.ajax({
                        url: url_for_ddo,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            console.log('treasury ')
                            console.log(data);
                            var DDO = data['DDO'];
                            var Treasurys = data['Treasurys'];
                            console.log(DDO);
                            console.log(Treasurys);
                            for (var i = 0; i < DDO.length; i++) {
                                var id = DDO[i]['id'];
                                var value_set_name = DDO[i]['ddo_name'];
                                var ddo_code = DDO[i]['ddo_code'];
                                //do something with k or data...
                                options += '<option value="' + ddo_code + '">' + value_set_name + '</option>';
                            }
                            $('#ddo').html(options);
                            $('#Treasury_Code').val(Treasurys);
                        }
                    });
                } else {
                    $('#ddo').empty();
                }
            });
       
            $('.close').on('click', function () {
                console.log('click');
                $('#exampleModal_payment').hide();
            });
            $('#Payment_Submit').on('click', function () {
           
                //  $('#exampleModal').hide();
                // $('#Payment_Submit').
                if(splitUrl[2] == "payment_form"){
                        var url_for_application_payment = '../application_payment';
                     }else{
                        var url_for_application_payment = 'application_payment';
                    }
                var applicationId = $('#applicationId').val();
                var Treasury_Code = $('#Treasury_Code').val();
                var treasury = $('#treasury').val();
                var ddo = $('#ddo').val();
                var treasury = $('#treasury').val();
                var Treasury_Code = $('#Treasury_Code').val();
                console.log(Treasury_Code);
                if(treasury==''){
                    alert('Select Treasury ');
                }
                if(ddo==''){
                    alert('Select DDO');
                }
                if(treasury!='' && ddo!='' && applicationId){
                //  $('#exampleModal_payment').show();
                 $('#exampleModal_payment').show();
                 
                 
           
                
                 if (applicationId) {
                     $.ajax({
                         url: url_for_application_payment,
                         type: "POST",
                         dataType: "json",
                         data:{
                             'applicationId':applicationId,
                             'Treasury_Code':Treasury_Code,
                             'treasury':treasury,
                             'ddo':ddo
                            },
                            success: function (data) {
                                
                                console.log('treasury ')
                                console.log(data);
                            $('#encpt').val(data['enc']);
                        }
                    });
                } else {
                    $('#ddo').empty();
                }
            }
            
            });
        });
    </script>