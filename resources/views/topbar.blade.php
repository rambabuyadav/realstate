
<!-- css for profile picture module -->
<style>
    .file {
        visibility: hidden;
        position: absolute;
        }
        .fade.show{
        display:block;
        }
</style>
<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
    <div class="container-fluid">
        <div class="col-md-4">
            <div class="pull-left" style="position:relative;">
                <!-- <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a> -->
                <!-- <a href="#!" class="b-brand"> -->
                <!-- ========   change your logo hear   ============ -->
                <!-- <img src="assets/images/logo.png" alt="" class="logo">
                    <img src="assets/images/logo-icon.png" alt="" class="logo-thumb"> -->
                @if(Auth::user()->user_role=='school')
                <span style="font-size:18px; font-weight:bold; color:white;">School</span>
                @endif
                @if(Auth::user()->user_role=='state')
                <span style="font-size:14px; font-weight:bold; color:white;">Primary Directorate of State </span>
                @endif
                @if(Auth::user()->user_role=='dse')
                <span style="font-size:14px; font-weight:bold; color:white;">District Superintendent of Education</span>
                @endif
                @if(Auth::user()->user_role=='dc')
                <span style="font-size:18px; font-weight:bold; color:white;">District Committee</span>
                @endif
                @if(Auth::user()->user_role=='faa')
                <span style="font-size:14px; font-weight:bold; color:white;">First Appellate Authority</span>
                @endif
                @if(Auth::user()->user_role=='saa')
                <span style="font-size:14px; font-weight:bold; color:white;">Second Appellate Authority</span>
                @endif
                @if(Auth::user()->user_role=='student')
                <span style="font-size:14px; font-weight:bold; color:white;">Student</span>
                @endif
                <!-- </a> -->
                <a href="#!" class="mob-toggler">
                    <i class="fa fa-plus"></i>
                </a>
                <span>
                    <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                </span>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-12">
            <div class="pull-right">
                <div id="google_translate_element" style="float: right;"></div>
                <a class="button-log-out" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="float: right;margin: 11px 50px;">
                    <i class="fa fa-fw fa-power-off"></i> LOGOUT
                </a>
            </div>
        </div>
    </div>
</header>