@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
       <!-- [ breadcrumb ] start -->
       <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Contact Us</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('district')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="fndSettings"> Settings</a></li>
                        <li class="breadcrumb-item"><a href="">Contact Us</a></li>
                    </ul>
                    @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-12">
                    <form action="{{url('Update-Contact-Us')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="id" value="{{@$Contact_us->id}}">
                        <!-- school details -->
                        <div class="card">
                            <div class="card-header">Contact Us</div>
                            <div class="card-body">
                        @include('message-flash')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Heading</b></label>
                                                <input type="text" class="form-control"  value="{{$Contact_us->heading}}" name="heading" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Phone Number</b></label>
                                                <input type="text" class="form-control" value="{{$Contact_us->phone}}" name="phone" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>E-mail</b></label>
                                                <input type="text" class="form-control" value="{{$Contact_us->email}}" name="email" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Facebook link</b></label>
                                                <input type="text" class="form-control" value="{{$Contact_us->facebook_link}}" name="facebook_link" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Twitter link</b></label>
                                                <input type="text" class="form-control" value="{{$Contact_us->twitter_link}}" name="twitter_link" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Instagram link</b></label>
                                                <input type="text" class="form-control" value="{{$Contact_us->instagram_link}}" name="instagram_link" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-4"></div>
                                        <div class="col-md-3 col-4">
                                            <input class="btn btn-primary btn_submit" type="submit" name="submit" value="Update" style="float:right" />
                                        </div>
                                    </div>                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>