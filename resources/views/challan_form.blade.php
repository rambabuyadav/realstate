@include('header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Private School Recognition System | School Registration</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/registration.css')}}">
    <script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <!-- CSS -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500"> -->
    <!-- <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/form-elements.css"> -->
    <!-- <link rel="stylesheet" href="assets/school_register_assets/css/style.css"> -->
    <style>
        label {
            float: left;
        }
        .card-header {
            font-size: 35px;
            color: #000000;
            font-weight: 700 !important;
        }
    </style>
</head>
<body>
    <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <form id="tabs" name="application_form" action="{{url('application_challan')}}" onsubmit="return validation()" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card ">
                        <div class="card-header">
                            <strong>Application Challan</strong>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <input type="hidden" id="applicationId" name="applicationId" value="{{@$School_Data->id}}">
                                            <input type="hidden" id="applicationId" name="school_id" value="{{@$School_Data->school_id}}">
                                            <label for="">School Name </label>
                                            <input type="text" maxlength="48" value="{{@$School_Data->school_name}}" name="school_name" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Post Office </label>
                                            <input type="text" value="{{@$School_Data->post_office}}" name="post_office" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Village / Town </label>
                                            <input type="text" maxlength="48" value="{{@$School_Data->village_city}}" name="village_town" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Pin Code</label>
                                            <input type="text" value="{{@$School_Data->pincode}}" name="pin_code" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone / Mobile Number </label>
                                            <input type="text" value="{{@$School_Data->phone_with_std_code}}" name="phone_with_std_code" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""> Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </label>
                                            <input type="text" value="{{@$School_Data->type_of_school}}" name="type_of_school" maxlength="38" class="form-control rte_input" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Amount </label>
                                            @if($School_Data->type_of_school=='1-5')
                                            <input type="text" value="12500" id="amount" name="amount" maxlength="38" class="form-control rte_input" readonly>
                                            @elseif($School_Data->type_of_school=='1-8')
                                            <input type="text" value="25000" id="amount"  name="amount" maxlength="38" class="form-control rte_input" readonly>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""> GRN Number</label>
                                            <input type="text" id="GRN" name="GRN" class="form-control rte_input" required/>

                                            
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Transaction ID</label>
                                            <input type="text" id="transaction_id" name="transaction_id" class="form-control rte_input" required/>
                                            
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Document</label>

                                            <input type="file" accept="" id="document" name="document[]" class="form-control rte_input" required multiple class="form-control" accept=".pdf, .jpg, .png, jpeg" />
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <input type="button" class="btn btn-danger" name="Cancle" value="Cancel" />
                                        <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="Challan_Submit" name="Submit" value="Submit" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    @include('footer')
   