











@include('header')
@include('sidenav')
@include('topbar')
<style>
  .w-5 {
    display: inline;
    height: 30px;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">District </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">All User Details</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <a href="{{url('addUser')}}" class="text-success" title="add "><button type="button" class="btn btn-sm btn-light btn-offset text-primary" title=" Add User"><i class="fa fa-plus text-success"> </i> </button></a>
            <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="window.location.reload()" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
         <div class="shadow-lg p-3 mt--6 bg-white rounded">
        

  <div class="card-body">
    @if (session('success'))
       <div class="alert alert-success" role="alert">
            {{ session('success') }}
       </div>
    @endif

   <table class="table table-bordered">
       <th>Id</th> 
       <th>Usersname</th> 
       <th>Email ID</th>   
       <th>Action</th>   
       <tbody>
           @foreach($UserALL as $key=>$user)
           <tr>
               <td>{{$key+1}}</td>
               <td>{{$user->name}}</td>
               <td>{{$user->email}}</td>
               <td> <a class="btn btn-danger" href="{{url('/user/delete')}}/{{$user->id}}" style="color:white;" onclick="return confirm('Are you sure want to delete user ?')">Delete</a></td>
              
           </tr>
           @endforeach
       </tbody>
   
   </table>    
  
 
</div>
         </div>
      </div>
    </div>
  </div>
</div>
<!-- [ Main Content ] end -->


@include('footer')




