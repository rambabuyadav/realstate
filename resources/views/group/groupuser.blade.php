











@include('header')
@include('sidenav')
@include('topbar')
<style>
  .w-5 {
    display: inline;
    height: 30px;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">District </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">Add Group User</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <a href="{{url('addUser')}}" class="text-success" title="add "><button type="button" class="btn btn-sm btn-light btn-offset text-primary" title=" Add User"><i class="fa fa-plus text-success"> </i> </button></a>
            <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="window.location.reload()" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
         <div class="shadow-lg p-3 mt--6 bg-white rounded">
        
            <div class="card-body">
                @if (session('success'))
                   <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                   </div>
                @endif

               <form method="POST" action="{{url('create-user')}}">
                  
                   @csrf
                   <div class="form-group row">
                       <div class="col-md-1">
                           <label for="code" class="col-form-label text-md-right "><b class="text-secondary">Username</b></label>

                       </div>
                       <div class="col-md-6 col-12">
                           <input type="text" class="form-control" name="name" placeholder="Enter Username...">
                       </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-1">
                          <label for="code" class="col-form-label text-md-right "><b class="text-secondary">Email ID</b></label>

                      </div>
                      <div class="col-md-6 col-12">
                          <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Id...">
                      </div>

                    </div>

                       <!-- <div class="col-md-3">
                           <label for="code" class="col-form-label text-md-right mt-3"><b class="text-secondary">Profile Photo</b></label>

                       </div>
                       <div class="col-md-9 col-12">
                           <input type="file" class="mt-4" placeholder="Enter Email Id..." name="profile_photo_path">
                       </div> -->
                       <div class="form-group row">
                         <div class="col-md-1">
                             <label for="code" class="col-form-label text-md-right "><b class="text-secondary">Password</b></label>
  
                         </div>
                         <div class="col-md-6 col-12">
                             <input type="text" class=" form-control" name="password" id="password" placeholder="Enter Password...">
                         </div>

                       </div>


                         <div class="col-md-6">
                       </div>
                   </div> <br>

                   <div class="form-group row mb-2">
                       <div class="col-md-12">
                           <center>
                               <button type="submit" class="btn btn-success">
                                   SUBMIT
                           </button>
                           </center>
                           
                       </div>
                   </div>
               </form>
             
           </div>
         </div>
      </div>
    </div>
  </div>
</div>
<!-- [ Main Content ] end -->


@include('footer')



