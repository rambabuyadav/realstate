@include('header')
@include('sidenav')
@include('topbar')
<!-- [ Header ] end -->
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Student Information</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">Student Information</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">
                    <div class="card-body">
                        @if(Auth::user()->user_role=='student')
                        <table class="table table-bordered  ">
                            <tr>
                                <th>SL NO.</th>
                                <th>FIELD NAME</th>
                                <th>VALUE</th>
                            </tr>
                            @php
                            $tot = 1;
                            $district_val = DB::table('fnd_values')->where('fnd_values.id', $students_data->district)->value('display_value');
                            $block_val = DB::table('fnd_values')->where('fnd_values.id', $students_data->block)->value('display_value');
                            @endphp
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Full Name</td>
                                <td class="value_name">{{@$students_data->full_name}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">District</td>
                                <td class="value_name">{{@$district_val}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Block</td>
                                <td class="value_name">{{@$block_val}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Post Office</td>
                                <td class="value_name">{{@$students_data->post_office}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Email</td>
                                <td class="value_name">{{@$students_data->email}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Address</td>
                                <td class="value_name">{{@$students_data->address}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Village</td>
                                <td class="value_name">{{@$students_data->village}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Pin Code</td>
                                <td class="value_name">{{@$students_data->pin}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Nearest Police Station</td>
                                <td class="value_name">{{@$students_data->nearest_police_station}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Religion</td>
                                <td class="value_name">{{@$students_data->religion}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Gender</td>
                                <td class="value_name">{{@$students_data->gender}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Caste</td>
                                <td class="value_name">{{@$students_data->caste}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">blood Group</td>
                                <td class="value_name">{{@$students_data->blood_group}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Disability if Any</td>
                                <td class="value_name">{{@$students_data->disability}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Name</td>
                                <td class="value_name">{{@$students_data->father_name}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Primary Phone</td>
                                <td class="value_name">{{@$students_data->father_primary_phone}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Secondary Phone</td>
                                <td class="value_name">{{@$students_data->father_secondary_phone}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Blood Group</td>
                                <td class="value_name">{{@$students_data->father_blood_group}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Income</td>
                                <td class="value_name">{{@$students_data->father_income}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Mother Name</td>
                                <td class="value_name">{{@$students_data->mother_name}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Mother Primary Phone</td>
                                <td class="value_name">{{@$students_data->mother_primary_phone}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Mother Secondary Phone</td>
                                <td class="value_name">{{@$students_data->mother_secondary_phone}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Mother Blood Group</td>
                                <td class="value_name">{{@$students_data->mother_blood_group}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Mother Income</td>
                                <td class="value_name">{{@$students_data->mother_income}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Previous School Name</td>
                                <td class="value_name">{{@$students_data->previous_school_name}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Last Class Studied</td>
                                <td class="value_name">{{@$students_data->last_class_studied}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Class Applied</td>
                                <td class="value_name">{{@$students_data->class_applied}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Referred School Name</td>
                                <td class="value_name">
                                    @if(isset($students_data->referred_school_name))
                                    @foreach($students_data->referred_school_name as $key => $val)
                                    {{@$val}}<br>
                                    @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Distance</td>
                                <td class="value_name">
                                    @if(isset($students_data->distance))
                                    @foreach($students_data->distance as $key => $val)
                                    {{@$val}}<br>
                                    @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Last Class Marksheet</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->last_class_marksheet}}" target="_blank"> {{@$students_data->last_class_marksheet}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Photo</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->photo}}" target="_blank"> {{@$students_data->photo}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Birth Certificate</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->birth_certificate}}" target="_blank"> {{@$students_data->birth_certificate}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Aadhaar Card</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->aadhaar_card}}" target="_blank"> {{@$students_data->aadhaar_card}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Aadhaar Card</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->father_aadhaar_card}}" target="_blank"> {{@$students_data->father_aadhaar_card}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Ration Card</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->ration_card}}" target="_blank"> {{@$students_data->ration_card}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Father Income Certificate</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->father_income_certificate}}" target="_blank"> {{@$students_data->father_income_certificate}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Mother Income Certificate</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->mother_income_certificate}}" target="_blank"> {{@$students_data->mother_income_certificate}}</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Community Certificate</td>
                                <td class="value_name">
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}{{@$students_data->community_certificate}}" target="_blank"> {{@$students_data->community_certificate}}</a>
                                </td>
                            </tr>
                        </table>
                        @elseif(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc')
                        <table class="table table-bordered  ">
                            <tr>
                                <th>Sl.</th>
                                <th>Full Name</th>
                                <th>District</th>
                                <th>Block</th>
                                <th>Address</th>
                                <th>Mobile No.</th>
                                <th>Action</th>
                            </tr>
                            @php
                            $i=0;
                            @endphp
                            @if(isset($students_data))
                            @foreach($students_data as $data)
                            <tr>
                                @php
                                $data->district = DB::table('fnd_values')->where('fnd_values.id', $data->district)->value('display_value');
                                $data->block = DB::table('fnd_values')->where('fnd_values.id', $data->block)->value('display_value');
                                @endphp
                                <td>{{$i + 1}}</td>
                                <td>{{$data->full_name}}</td>
                                <td>{{$data->district}}</td>
                                <td>{{$data->block}}</td>
                                <td>{{$data->address}}</td>
                                <td>{{$data->phone}}</td>
                                <td>
                                    <button class="btn-success" title="Information" data-toggle="modal" data-target=".exampleModal2" onclick="getdetails({{$data}})"><i class="fa fa-info"></i></button>
                                    <a role="button" class="text-success" href="download/pdf/{{$data->id}}"><button class="btn-success" title="Download PDF"><i class="fa fa-download"></i></button></a>
                                </td>
                                @php
                                $i++;
                                @endphp
                            </tr>
                            @endforeach
                            @endif
                            @if($i == 0)
                            <tr>
                                <th colspan="7" style="text-align: center;"><span>No student Found!!</span></th>
                            </tr>
                            @endif
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
<div class="modal fade exampleModal2" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 800px;" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel2">Student Information </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="append-content">
            </div>
        </div>
    </div>
</div>
@include('footer')
<script>
    function getdetails(data) {
        var html = `<table class="table table-bordered  ">
                <tr>
                    <th>SL NO.</th>
                    <th>FIELD NAME</th>
                    <th>VALUE</th>
                    
                </tr>
                @php
                $tot = 1;
                
                @endphp
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Full Name</td>
                    <td class="value_name">`+ data.full_name + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">District</td>
                    <td class="value_name">`+ data.district + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Block</td>
                    <td class="value_name">`+ data.block + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Post Office</td>
                    <td class="value_name">`+ data.post_office + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Email</td>
                    <td class="value_name">`+ data.email + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Address</td>
                    <td class="value_name">`+ data.address + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Village</td>
                    <td class="value_name">`+ data.village + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Pin Code</td>
                    <td class="value_name">`+ data.pin + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Nearest Police Station</td>
                    <td class="value_name">`+ data.nearest_police_station + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Religion</td>
                    <td class="value_name">`+ data.religion + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Gender</td>
                    <td class="value_name">`+ data.gender + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Caste</td>
                    <td class="value_name">`+ data.caste + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">blood Group</td>
                    <td class="value_name">`+ data.blood_group + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Disability if Any</td>
                    <td class="value_name">`+ data.disability + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Name</td>
                    <td class="value_name">`+ data.father_name + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Primary Phone</td>
                    <td class="value_name">`+ data.father_primary_phone + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Secondary Phone</td>
                    <td class="value_name">`+ data.father_secondary_phone + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Blood Group</td>
                    <td class="value_name">`+ data.father_blood_group + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Income</td>
                    <td class="value_name">`+ data.father_income + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Mother Name</td>
                    <td class="value_name">`+ data.mother_name + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Mother Primary Phone</td>
                    <td class="value_name">`+ data.mother_primary_phone + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Mother Secondary Phone</td>
                    <td class="value_name">`+ data.mother_secondary_phone + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Mother Blood Group</td>
                    <td class="value_name">`+ data.mother_blood_group + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Mother Income</td>
                    <td class="value_name">`+ data.mother_income + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Previous School Name</td>
                    <td class="value_name">`+ data.previous_school_name + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Last Class Studied</td>
                    <td class="value_name">`+ data.last_class_studied + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Class Applied</td>
                    <td class="value_name">`+ data.class_applied + `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Referred School Name</td>
                    <td class="value_name">`;
        if (data.referred_school_name != null) {
            var l = 1;
            for (var i = 0; i < data.referred_school_name.length; i++) {
                html += `` + l + `.` + data.referred_school_name[i] + `<br>`;
                l++;
            }
        }
        html += `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Distance</td>
                    <td class="value_name">`;
        if (data.distance != null) {
            var k = 1
            for (var j = 0; j < data.distance.length; j++) {
                html += ` ` + k + `.` + data.distance[j] + `<br>`;
                k++;
            }
        }
        html += `</td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Last Class Marksheet</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.last_class_marksheet + `" target="_blank"> ` + data.last_class_marksheet + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Photo</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.photo + `" target="_blank"> ` + data.photo + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Birth Certificate</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.birth_certificate + `" target="_blank"> ` + data.birth_certificate + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Aadhaar Card</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.aadhaar_card + `" target="_blank"> ` + data.aadhaar_card + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Aadhaar Card</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.father_aadhaar_card + `" target="_blank"> ` + data.father_aadhaar_card + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Ration Card</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.ration_card + `" target="_blank"> ` + data.ration_card + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Father Income Certificate</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.father_income_certificate + `" target="_blank"> ` + data.father_income_certificate + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Mother Income Certificate</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.mother_income_certificate + `" target="_blank"> ` + data.mother_income_certificate + `</a>
                    </td>
                </tr>
                <tr>
                    <td class="si_no">{{$tot++}}</td>
                    <td class="lable_name">Community Certificate</td>
                    <td class="value_name">
                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/student/')}}`+ data.community_certificate + `" target="_blank"> ` + data.community_certificate + `</a>
                    </td>
                </tr>
                
            </table>`;
        $('#append-content').html(html);
    }
</script>