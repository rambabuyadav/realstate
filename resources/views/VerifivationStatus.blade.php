@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div><!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">School Verify</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="">Verification Status</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> --> 
          <div class="shadow-lg p-3 mt--6 bg-white rounded">
    
  <!-- general information -->
  <div class="card-body">
      <input type="hidden" name="id" id="school_id" value="">
    <table class="table table-bordered  ">
        <tr>
            <th>SL NO.</th>
            <th>FIELD NAME</th>
            <th>VALUE</th>
            
            <th> Remarks </th>
            <th> Status </th>
          
           
        </tr>
        <tr>
            <td class="si_no">1</td>
            <td class="lable_name">School Name</td>
            <td class="field_name" style="display: none;"> school_name </td>
            <td class="value_name">Viday Bharati High School </td>
            
            
            <td>CYZ </td>
            <td>Under Verification</td>
          
        </tr>
        <tr>
            <td class="si_no">2</td>
            <td class="lable_name">Academic Session From Which Recognition Proposed</td>
            <td class="field_name" style="display: none;"> recognised_by </td>
            <td class="value_name">2010-2022 </td>
            
            
            <td>CYZ </td>
            <td>Under Verification</td>
          
        </tr>
        <tr>
            <td class="si_no">3</td>
            <td class="lable_name">District</td>
            <td class="field_name" style="display: none;"> district </td>
            <td class="value_name"> saraikella-kharaswan</td>
            
            
            <td>CYZ </td>
            <td>Under Verification</td>
          
        </tr>
        <tr>
            <td class="si_no">4</td>
            <td class="lable_name">Post Office</td>
            <td class="field_name" style="display: none;"> post_office </td>
            <td class="value_name"> Gamharia</td>
            
            
            <td>CYZ </td>
            <td>Under Verification</td>
          
        </tr>
        <tr>
            <td class="si_no">5</td>
            <td class="lable_name">Village / Town</td>
            <td class="field_name" style="display: none;"> village_city </td>
            <td class="value_name"> Jaganathpur </td>
            
            
            <td>CYZ </td>
            <td>Under Verification</td>
          
        </tr>
</table>
        
      </div>
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
</div>
@include('footer')
