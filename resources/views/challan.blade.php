@include('header')
<div class="container-fluid" style="padding-top: 50px;">
    <div class="row">
        <div class="col-md-12 shadow-lg   mt--6 bg-white rounded">
            <div class="card-header">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="">
                    <a style="float: right; margin-left: 10px;" href="https://rte.jharkhand.gov.in/application"><button class="btn btn-sm btn-danger pull-right"><i class="fa fa-times"></i></button></a>
                    <button style="float: right;" onclick="window.print()" class="btn btn-sm btn-success pull-right" title="print document"><i class="fa fa-print"></i></button>
                    
                </div>
            </div>
           
            <div class="card-body">
               
                <table class="table table-bordered table-sm  ">
                  
                    <tr>
                       <th>School Name </th>
                        <td class="">{{@$challan->school_name}} </td>
                    </tr>
                    <tr>
                       <th>Government Receipt Number (GRN)</th>
                        <td class="">{{@$challan->grn}}</td>
                    </tr>
                    <tr>
                       <th>Department ID</th>
                        <td class="">{{@$challan->dept_id}}</td>
                    </tr>
                    <tr>
                       <th>Receipt Head Code</th>
                        <td class="">{{@$challan->receipt_head_code}}</td>
                    </tr>
                    <tr>
                       <th>Department Transaction Number</th>
                        <td class="">{{@$challan->dept_tran_id}}</td>
                    </tr>
                    <tr>
                       <th>Depositor ID</th>
                        <td class="">{{@$challan->depositor_id}}</td>
                    </tr>
                    <tr>
                       <th>Phone Number</th>
                        <td class="">{{@$challan->phone_with_std_code}}</td>
                    </tr>
                    <tr>
                       <th>Depositor Name </th>
                        <td class="">{{@$challan->depositor_name}}</td>
                    </tr>
                    <tr>
                       <th>Application Number </th>
                        <td class="">{{@$challan->apllication_id}}</td>
                    </tr>
                  
                    <tr>
                       <th>School Type</th>
                        <td class="">{{@$challan->type_of_school}}</td>
                    </tr>
                    <tr>
                       <th>Challan Identification Number (CIN) </th>
                        <td class="">{{@$challan->cin}}</td>
                    </tr>
                    <tr>
                       <th>IMFS Office Code </th>
                        <td class="">{{@$challan->ifms_office_code}}</td>
                    </tr>
                    <tr>
                       <th>Security Code </th>
                        <td class="">{{@$challan->ref_no}}</td>
                    </tr>
                    <tr>
                       <th>Payment Status </th>
                        <td class="">{{@$challan->payment_status_message}}</td>
                    </tr>
                    <tr>
                       <th>Transaction Date </th>
                        <td class="">{{@$challan->txn_date}}</td>
                    </tr>
                    <tr>
                       <th>Treasury  Code </th>
                        <td class="">{{@$challan->treas_code}}</td>
                    </tr>
                    <tr>
                       <th>Payment Mode </th>
                        <td class="">
                           
                            @if($challan->pmode=='P')
                            PAYMENT GATEWAY
                            @elseif($challan->pmode=='M')
                            MANNUAL 
                            @elseif($challan->pmode=='G')
                            GBSS
                            @endif
                        </td>
                    </tr>
                    <tr>
                       <th>Reciept Head Code</th>
                        <td class="">{{@$challan->receipt_head_code}}</td>
                    </tr>
                    <tr>  
                    
                       <th>Amount </th>
                        <td class="">{{@$challan->txn_amount}}</td>
                    </tr>
                  
                </table>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->
</div>
</div>
</div>
@include('footer')