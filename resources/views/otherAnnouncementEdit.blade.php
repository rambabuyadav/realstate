@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href=""><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href=""> Settings</a></li>
                            <li class="breadcrumb-item"><a href="">Edit</a></li>
                        </ul>
                        <!-- <div class="alert alert-success">
                          
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="id" value="{{@$fnd_settingsget->id}}" />
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">OTHER ANNOUNCEMENTS</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="">Paragraph</label>
                                            <textarea required  name="verification_by_dse_limit" class="form-control rte_input" value="{{@$fnd_settingsget->verification_by_dse_limit}}" style="resize: none;"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="">List Items</label>
                                            <input required type="text" name="schedule_meeting_by_dc_limit" class="form-control rte_input" value="{{@$fnd_settingsget->schedule_meeting_by_dc_limit}}">
                                        </div>
                                    </div>
                                   
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-4"></div>
                                    <div class="col-md-12 col-4">
                                        <input class="btn btn-primary btn_submit " type="submit" name="submit" value="Update" style="display:flex;justify-content:center;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')