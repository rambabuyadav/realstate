@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div><!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">School Accept</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="">School All Data Accept by DSE / DC</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> --> 
          <div class="shadow-lg p-3 mt--6 bg-white rounded">
    
  <!-- general information -->
  <div class="card-body">
      <input type="hidden" name="id" id="school_id" value="">
    <table class="table table-bordered  ">
        <tr>
            <th>S No.</th>
            <th>Field Name</th>
            <th>Value</th>
            
            <th> Remarks </th>
            <th> Status </th>
          
           
        </tr>
        <tr>
            <td class="si_no">17</td>
            <td class="lable_name">Chairman Name </td>
        <td class="field_name" style="display: none;"> school_chairman_name </td>
                <td class="value_name"> Dr. Suresh Prasad</td>
                                     <td>
            Accept
        </td>
        <td>Accepted</td>
                    </tr>
        <tr>
            <td class="si_no">18</td>
            <td class="lable_name">Designation</td>
        <td class="field_name" style="display: none;"> school_chairman_post </td>
                <td class="value_name">sdfsf Academic Ses </td>
                                     <td>
            Accept
        </td>
        <td>Accepted</td>
                    </tr>
        <tr>
            <td class="si_no">19</td>
            <td class="lable_name">Address</td>
        <td class="field_name" style="display: none;"> school_chairman_address </td>
                <td class="value_name">gamharia</td>
                                     <td>
            Accept
        </td>
        <td>Accepted</td>
                    </tr>
        <tr>
            <td class="si_no">20</td>
            <td class="lable_name">Phone No.</td>
        <td class="field_name" style="display: none;"> school_chairman_phone_number </td>
                <td class="value_name">7678787989</td>
                                     <td>
            Accept
        </td>
        <td>Accepted</td>
                    </tr>
</table>
        
      </div>
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
</div>
@include('footer')
