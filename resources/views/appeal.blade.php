@include('header')
@include('sidenav')
@include('topbar')
@php $flag = 0 @endphp
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
@php $is_application_reject = DB::table('application_data')->where('school_id',Auth::user()->school_id)->whereIn('application_status', [7,8,11])->exists() @endphp
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">
                <div class="btn-group">
                  @if(Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa')
                  <a href="{{url('appeal-list')}}">
                    
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">All</button>
                  </a>
                  <form method="post" action="{{url('appeal-list')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="appeal_status" value="1">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Pending</button>
                  </form>
                  <form method="post" action="{{url('appeal-list')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="appeal_status" value="2">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Approved</button>
                  </form>
                  <form method="post" action="{{url('appeal-list')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="appeal_status" value="3">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Rejected</button>
                  </form>
                </div>
                @endif
              </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">Appeal Details</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <form method="post" action="{{url('appeal-list')}}" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" class='text-block' name="search_text" value="{{$adv_data['search_text']}}" />
              <input type="hidden" class='text-block' name="appeal_status" value="{{$adv_data['appeal_status']}}" />
              <input type="hidden" class='text-block' name="Block" value="{{$adv_data['block_id']}}" />
              <input type="hidden" class='text-block' name="division" value="{{$adv_data['division_id']}}" />
              <input type="hidden" class='text-block' name="district" value="{{$adv_data['district_id']}}" />
              <input type="hidden" class='text-block' name="fromDate" value="{{$adv_data['fromDate']}}" />
              <input type="hidden" class='text-block' name="toDate" value="{{$adv_data['toDate']}}" />
              <input type="hidden" class='text-block' name="excel" value="excel" />
              @if(Auth::user()->user_role=='school' && 2 > $count_appeals)
              @if($is_application_reject)
              <a href="{{url('addAppeal')}}" class="text-success" title="add "><button type="button" class="btn btn-sm btn-light btn-offset"> <i class="fa fa-plus text-success"> </i></button></a>
              @endif
              @endif
              @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa')
              <button type="submit" class="btn btn-sm btn-light btn-offset" title="Export Appleas list"><i class="fa fa-file-excel text-success"> </i></button>
              <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
              <a href="{{url('appeal-list')}}"> <button type="button" class="btn btn-danger btn-sm pull-right" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button></a>
              @endif
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <!-- <h1>Here is all Payment Related Details</h1> -->
        <div class="shadow-lg p-3 mt--6 bg-white rounded" style="overflow-x: scroll;">
          @include('message-flash')
          <table class="table table-bordered table-sm">
            <thead>
              <tr>
                <th>ID </td>
                <th>School Name </th>
                <!-- <th>Appeal By</th> -->
                <th>Appeal To </th>
                <th>Appeal Date</th>
                <th>Pending At</th>
                <!-- <th>Appeal Returned By</th> -->
                <th>Remaining Days</th>
                <th>Appeal Status</th>
                <th style="text-align:center; width:8%;background-color: white;"></th>
                <!-- <th>Document</th> -->
              </tr>
            </thead>
            <tbody>
              @php $i = 0 @endphp
              @foreach($appeals as $key => $appeal)
              @php $flag = 1 @endphp
              <tr>
                <td class='ID'>{{$appeal->id}} </td>
                <td class='appealSchoolName'>{{$appeal->school_name}} </td>
                <!-- <td class='appeal_by'>{{$appeal->appeal_by}}</td> -->
                <td class='appeal_to'>
                  @if ( $appeal->appellate_authority_type =='1')
                  RDDE
                  @elseif($appeal->appellate_authority_type =='2')
                  Director of Primary Education
                  @endif
                </td>
                <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->created_at)->format('d/m/Y')}}</td>
                <td>
                  @if($appeal->appeal_assign_to == null)
                  @if ( $appeal->appellate_authority_type =='1')
                  RDDE
                  @elseif($appeal->appellate_authority_type =='2')
                  Director of Primary Education
                  @endif
                  @else
                  @if($appeal->appeal_assign_to == 'dc')
                  District Committee
                  @elseif($appeal->appeal_assign_to == 'dse')
                  District Superintendent Of Education
                  @endif
                  @endif
                </td>
                <!-- <td class='CreatedBy'>{{$appeal->created_by}}</td> -->
                    
                    @if(Auth::user()->user_role=='school')
                      @if( $appeal->appeal_status =='3')
                        <td style="text-align: center;"> ------------ </td>
                      @elseif( $appeal->appeal_status =='2' )
                          @if(\Carbon\Carbon::now() > $appeal->appeal_deadline)
                            <td style="color: red;">{{\Carbon\Carbon::parse ($appeal->appeal_deadline)->format('d/m/Y')}}
                              <span>( Delayed )</span>
                            </td>
                          @else
                          <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->appeal_deadline)->format('d/m/Y')}}
                            <span>({{$count[$i]}} days)</span>
                          </td>
                          @endif
                      @else
                        @if($appeal->appellate_authority_type==1)
                          @if($count[$i] <= 0)
                            <td style="color: red;">{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_faa)->format('d/m/Y')}}
                              <span>( Delayed )</span>
                            </td>
                          @else
                            <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_faa)->format('d/m/Y')}}
                              <span>({{$count[$i]}} days)</span>
                            </td>
                          @endif
                        @endif
                        @if($appeal->appellate_authority_type==2)
                          @if($count[$i] <= 0 )
                            <td style="color: red;">{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_saa)->format('d/m/Y')}}
                              <span>( Delayed )</span>
                            </td>
                          @else
                            <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_saa)->format('d/m/Y')}}
                              <span>({{$count[$i]}} days)</span>
                            </td>
                          @endif
                        @endif
                      @endif
                    @endif
                    @if( Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' )
                      @if(\Carbon\Carbon::now() > $appeal->appeal_deadline)
                        <td style="color: red;">{{\Carbon\Carbon::parse ($appeal->appeal_deadline)->format('d/m/Y')}}
                            <span>( End )</span>
                        </td>
                      @else
                        <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->appeal_deadline)->format('d/m/Y')}}
                          <span>({{$count[$i]}} days)</span>
                        </td>
                      @endif
                    @endif
                    @if(Auth::user()->user_role=='faa')
                      @if( $appeal->appeal_status !='1')
                       <td style="text-align: center;"> ------------ </td>
                      @else
                        @if($count[$i] <= 0)
                          <td style="color: red;">{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_faa)->format('d/m/Y')}}
                            <span>( Delayed )</span>
                          </td>
                        @else
                          <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_faa)->format('d/m/Y')}}
                            <span>({{$count[$i]}} days)</span>
                          </td>
                        @endif
                      @endif
                    @endif
                    @if(Auth::user()->user_role=='saa')
                      @if( $appeal->appeal_status !='1')
                       <td style="text-align: center;"> ------------ </td>
                      @else
                        @if($count[$i] <= 0 )
                          <td style="color: red;">{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_saa)->format('d/m/Y')}}
                            <span>( Delayed )</span>
                          </td>
                        @else
                          <td class='created_at'>{{\Carbon\Carbon::parse ($appeal->appeal_date)->addDays($days_to_varified_by_saa)->format('d/m/Y')}}
                            <span>({{$count[$i]}} days)</span>
                          </td>
                        @endif
                      @endif
                    @endif
        
                <td class="appeal_status">
                  @if ( $appeal->appeal_status =='2')
                  <span class="text-success"> Accepted </span>
                  @elseif($appeal->appeal_status =='3')
                  <span class="text-danger"> Rejected </span>
                  @elseif($appeal->appeal_status =='1')
                  <span class="text-warning"> Requested </span>
                  @endif
                </td>
                <input type="hidden" class='text-block' id="appealTextHide" value="{{$appeal->appeal_text}}" />
                <input type="hidden" class='text-block' id="school_id" value="{{$appeal->school_id}}" />
                <input type="hidden" class='text-block' id="appealSchoolCode" value="{{$appeal->school_chairman_name}}" />
                <input type="hidden" class='text-block' id="appealSchoolEmail" value="{{$appeal->school_chairman_post}}" />
                <input type="hidden" class='text-block' id="appealSchoolType" value="{{$appeal->school_chairman_address}}" />
                <input type="hidden" class='text-block' id="appealWebsite" value="{{$appeal->school_chairman_phone_number}}" />
                <input type="hidden" class='text-block' id="appealBoard" value="{{$appeal->school_chairman_office}}" />
                <input type="hidden" class='text-block' id="appealSchoolAddress" value="{{$appeal->school_chairman_email}}" />
                <input type="hidden" class='text-block' id="appealBlock" value="{{$appeal->income}}" />
                <input type="hidden" class='text-block' id="appealPO" value="{{$appeal->expenses}}" />
                <input type="hidden" class='text-block' id="appealPS" value="{{$appeal->surplus_money}}" />
                <input type="hidden" class='text-block' id="appealCluster" value="{{$appeal->reduced_money}}" />
                <input type="hidden" class='text-block' id="appealCity" value="{{$appeal->medium}}" />
                <input type="hidden" class='text-block' id="appealDistrict" value="{{$appeal->type_of_school}}" />
                <input type="hidden" class='text-block' id="appealRemarks" value="{{$appeal->remarks}}" />
                <input type="hidden" class='text-block' id="appealRemarksBy" value="{{$appeal->remarks_by}}" />
                <input type="hidden" class='text-block' id="appealRemarksDate" value="{{$appeal->remarks_date}}" />
                <input type="hidden" class='text-block' id="appealRemarksStatus" value="{{$appeal->remarks_status}}" />
                <td style="text-align:center;">
                  @if(isset($appeal->school_file) && count($appeal->school_file) > 0)
                  <table style="display: none;" class="table table-sm Uploaded_document_link">
                    @foreach ($appeal->school_file as $files_key => $subData)
                    <tr class="">
                      <td>
                        <label for="message-text" class="col-form-label">{{$subData}} :</label>
                      </td>
                      <td>
                        <button type="button" class="btn btn-sm btn-light btn-offset">
                          <a download="" href="https://rte.jharkhand.gov.in/public/assets/school_send_appeal/{{$subData}}" title="{{$subData}}">
                            <i class="fa fa-download text-info"> </i> </a></button>
                      </td>
                    </tr>
                    @endforeach
                  </table>
                  @endif
                  @if(isset($appeal->appeal_conversations) && count($appeal->appeal_conversations) > 0)
                  <div style="display: none;" class=" Conversation_History">
                    <!-- $Appeal['appeal_conversations'][$i] -->
                    <div>
                      @foreach ($appeal->appeal_conversations as $conversation_key => $conversation_list)
                      <label for="message-text" class=""><b>Date:</b> {{$conversation_list->created_at}}</label><br>
                      <label for="message-text" class=""><b>From:</b> {{$conversation_list->Sender_name}}</label><br>
                      <label for="message-text" class=""><b>To:</b> {{$conversation_list->Reciever_name}} </label><br>
                      <label for="message-text" class=""><b>Remarks : </b> {{$conversation_list->remarks}}</label><br>
                      <label for="message-text" class="">
                        <!-- {{ $conversation_list }} -->
                        @if(isset($conversation_list->attachments) && count($conversation_list->attachments) > 0)
                        <table class="Uploaded_document_link">
                          <tr class="">
                            <td>
                              @foreach ($conversation_list->attachments as $conversation_image_key => $conversation_image_list)
                              @if($conversation_image_key > 0)
                              ,&nbsp;
                              @endif
                              <a href="https://rte.jharkhand.gov.in/public/assets/accept_appeal/{{$conversation_image_list}}" target="_blanck"><span>{{$conversation_image_list}}</span></a>
                              @endforeach
                            </td>
                          </tr>
                        </table>
                        @endif
                      </label>
                      <hr>
                      @endforeach
                    </div>
                  </div>
                  @endif
                  <button class="btn btn-light viewData text-success btn-sm " data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-eye " title="View"></i></button>
                  <button class="btn btn-light viewConversation text-primary btn-sm " data-toggle="modal" title="View Conversation" data-target="#Conversation"><i class="fa fa-users"></i>
                    <sup>
                      @if(@$appeal->appeal_conversation_unread_count>0)
                      {{@$appeal->appeal_conversation_unread_count}}
                      @endif
                    </sup>
                  </button>
                  @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa')
                  <button class="btn btn-light reply_appeal text-danger btn-sm" data-toggle="modal" data-target="#reply_appeal_modal" data-whatever="@replyAppeal"><i class="fa fa-reply" title="Reply"></i></button>
                  @endif
                  @if(Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa')
                  <button class="btn btn-light accept_appeal text-primary btn-sm " data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo"><i class="fa fa-check " title="Accept"></i></button>
                  <button class="btn btn-light reject_appeal text-danger btn-sm" data-toggle="modal" data-target="#exampleModal2" data-whatever="@fat"><i class="fa fa-times" title="Reject"></i></button>
                  @endif
                  <button class="btn btn-light viewDocument text-info btn-sm " data-toggle="modal" title="View School Uploaded Document " data-target="#AppealDocumentDownload"><i class="fa fa-download "></i></button>
                  <button class="btn btn-light viewAppealImages text-info btn-sm " data-toggle="modal" title="View Appeal Uploaded Image " data-target="#AppealImages"><i class="fa fa-image "></i></button>
                  <div class="AppealImagesData" style="display: none;">
                    @if(isset($appeal->appeal_document) && count($appeal->appeal_document) > 0)
                    @foreach ($appeal->appeal_document as $files_key => $subData)
                    <span> {{$subData}}</span>
                    <button type="button" class="btn btn-sm btn-light btn-offset">
                      <a download="" href="https://rte.jharkhand.gov.in/public/assets/accept_appeal/{{$subData}}" target="_blank" title="{{$subData}}">
                        <i class="fa fa-download text-info"> </i> </a></button>
                    <!-- <img src="../rte-application/public/accept_appeal/{{$subData}}" style="width: 90px; height:70px; padding: 0px;"><br> -->
                    @endforeach
                    @endif
                  </div>
                </td>
                @php $i++ @endphp
              </tr>
              @endforeach
              @if($flag == 0)
              <tr>
                <th colspan="8" style="text-align: center;"><span>No appeals Found!!</span></th>
              </tr>
              @endif
            </tbody>
          </table>
          {{-- Pagination --}}
          {{ $appeals->links() }}
        </div>
        <!-- Large modal -->
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title" id="exampleModalLabel">Appeal Details </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <!-- <form> -->
                <table class="table table-bordered table-condensed table-sm col-md-12">
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>School ID :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="SchoolID"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>Chairman Office :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_board"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b> School Name :</b></div>
                    <div for="message-text" class=" border p-1 col-9" id="appeal_school_name"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>Chairman Name :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_code"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>Chairman Post :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_email"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>Chairman Email :</b></div>
                    <div for="message-text" class=" border p-1 col-9">
                      <pre id="appeal_school_address"> </pre>
                    </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>School Type :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_district"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>School Surplus Money :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_ps"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>School Expense :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_po"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>School Income :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_block"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>School Reduced Money :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_cluster"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>School Medium :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_city"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>Chairman Address :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_type"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>Chairman Phone No :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appeal_school_website"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b>Appeal To :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appealTo"> </div>
                    <div for="message-text" class=" border p-1 col-3"><b>Appeal By :</b></div>
                    <div for="message-text" class=" border p-1 col-3" id="appealBy"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b> Appeal Text : </b></div>
                    <div style="width: 40%;" for="message-text" class=" border p-1 col-9">
                      <address id="appealText"> </address>
                    </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class=" border p-1 col-3"><b> Download Files : </b></div>
                    <div style="width: 40%;" for="message-text" class=" border p-1 col-9">
                      <address id="appealText"> </address>
                    </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class="border p-1 col-3"><b>Appeal Created At :</b></div>
                    <div for="message-text" class="border p-1 col-3" id="created_at"> </div>
                    <div for="message-text" class="border p-1 col-3"><b>Appeal Created By :</b></div>
                    <div for="message-text" class="border p-1 col-3" id="appeal_created_by"> </div>
                  </div>
                  <div class="row">
                    <div for="message-text" class="border p-1 col-3"><b> Remarks :</b></div>
                    <div for="message-text" class="border p-1 col-9" id="remarks"> Text:</div>
                    <div for="message-text" class="border p-1 col-3"><b> Remarks Status :</b></div>
                    <div for="message-text" class="border p-1 col-3" id="remarks_status"> Text:</div>
                  </div>
                  <div class="row">
                    <div for="message-text" class="border p-1 col-3"><b> Remarks Date :</b></div>
                    <div for="message-text" class="border p-1 col-3" id="remarks_date"> Text:</div>
                    <div for="message-text" class="border p-1 col-3"><b> Remarks Created By :</b></div>
                    <div for="message-text" class="border p-1 col-3" id="remarks_by"> Text:</div>
                  </div>
                  <!-- <tr>
            <th for="message-text" class="col-form-label">Appeal Status:</th>
            <td for="message-text" class="col-form-label" id="appeal_status"> </td>
            
          </div>   -->
                </table>
                <!-- </form> -->
              </div>
              <!-- <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Submit</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
            </div>
          </div>
        </div>
        <!-- -----------Advance search-------- -->
        <div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <form method="post" action="{{url('appeal-list')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-header bg-primary">
                  <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-6">
                      <label for="message-text" class="col-form-label"> Advance Search :</label>
                      @if(isset($adv_data['search_text']))
                      <input type="text" name="search_text" value="{{@$adv_data['search_text']}}" class="form-control rte_input" id="search_text" placeholder="School/ Appeal no./ E-mail/ Mobile">
                      @else
                      <input type="text" name="search_text" type="text" class="form-control" placeholder="School/ Appeals no./ E-mail/ Mobile">
                      @endif
                    </div>
                    <div class="col-6">
                      <label for="appeal_status" class="col-form-label"> Appeal Status</label>
                      <select class="form-control rte_input" name="appeal_status">
                        @isset($adv_data['appeal_status'])
                        <option value="{{@$adv_data['appeal_status']}}">{{@$adv_data['appeal_status']}}</option>
                        @endisset
                        <option value="" selected>---Select---</option>
                        <option value="1">Requested </option>
                        <option value="2">Approved </option>
                        <option value="3">Rejected </option>
                      </select>
                    </div>
                  </div>
                  @if(Auth::user()->user_role == 'faa')
                  <div class="row">
                    <div class="col-6">
                      <label for="message-text" class="col-form-label"> District :</label>
                      <select class="form-control rte_input" name="district" id="district">
                        @isset($adv_data['district_id'])
                        <option value="{{@$adv_data['district_id']}}">{{@$adv_data['districtName']}}</option>
                        @endisset
                        <option value="">---Select--- </option>
                        @foreach($district_list as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-6">
                      <label for="message-text" class="col-form-label"> Block :</label>
                      <select class="form-control rte_input" name="Block" id="block">
                        @isset($adv_data['block_id'])
                        <option value="{{@$adv_data['block_id']}}">{{@$adv_data['blockName']}}</option>
                        @endisset
                        <option value="">---Select---</option>
                      </select>
                    </div>
                  </div>
                  @elseif(Auth::user()->user_role == 'saa')
                  <div class="row">
                    <div class="col-4">
                      <label for="division" class="col-form-label"> Division :</label>
                      <select class="form-control rte_input" name="division" id="division">
                        @isset($adv_data['division_id'])
                        <option value="{{@$adv_data['division_id']}}">{{@$adv_data['divisionName']}}</option>
                        @endisset
                        <option value="" selected>---Select---</option>
                        @foreach($division_list as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-4">
                      <label for="district" class="col-form-label"> District :</label>
                      <select class="form-control rte_input" name="district" id="district">
                        @isset($adv_data['district_id'])
                        <option value="{{@$adv_data['district_id']}}">{{@$adv_data['districtName']}}</option>
                        @endisset
                        <option value="" selected>---Select--- </option>
                      </select>
                    </div>
                    <div class="col-4">
                      <label for="message-text" class="col-form-label"> Block :</label>
                      <select class="form-control rte_input" name="Block" id="block">
                        @isset($adv_data['block_id'])
                        <option value="{{@$adv_data['block_id']}}">{{@$adv_data['blockName']}}</option>
                        @endisset
                        <option value="">---Select--- </option>
                      </select>
                    </div>
                  </div>
                  @endif
                  <div class="row">
                    <div class="col-6">
                      <label for="message-text" class="col-form-label"> From :</label>
                      @if(isset($adv_data['fromDate']))
                      <input type="date" class="form-control" name="fromDate" id="from_date" value="{{$adv_data['fromDate']}}">
                      @else
                      <input type="date" class="form-control" name="fromDate" id="from_date">
                      @endif
                    </div>
                    <div class="col-6">
                      <label for="message-text" class="col-form-label"> To :</label>
                      @if(isset($adv_data['toDate']))
                      <input type="date" class="form-control" name="toDate" id="to_date" value="{{$adv_data['toDate']}}">
                      @else
                      <input type="date" class="form-control" name="toDate" id="to_date">
                      @endif
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- --------------end advance search------------ -->
        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabel">Accept Appeal </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="{{url('acceptRemark')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                  <input type="hidden" name="id" value="" class="form-control AppealID">
                  <input type="hidden" name="AppealSchoolID" value="" class="form-control AppealSchoolID">
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">DC/DSE :</label>
                    <select class="form-control" name="appeal_assign_to" id="appeal_assign_to" required>
                      <option value="">Select Sending Person</option>
                      <option value="dc">District Committee </option>
                      <option value="dse">District Superintendent of Education </option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Deadline Date :</label>
                    <input required name="appeal_deadline" type="date" class="form-control" id="appeal_deadline">
                  </div>
                  <div class="form-group">
                    <div class="row files form-group" id="files1">
                      <label for="message-text" class="col-form-label">Upload Document :</label>
                      <input type="file" accept=".pdf, .jpg, .png, jpeg" multiple name="accpet_document[]" class="form-control" id="message-text" />
                      </span>
                      <br />
                      <ul class="fileList"></ul>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Remarks :</label>
                    <textarea required name="appealRemarks" class="form-control" id="message-text"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal fade" id="reply_appeal_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabel">Reply Appeal </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="{{url('appeal-reply')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                  <input type="hidden" name="id" value="" class="form-control AppealID">
                  <input type="hidden" name="AppealSchoolID" value="" class="form-control AppealSchoolID">
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">DC/DSE/School : <span style="color: red;">*</span></label>
                    @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc')
                    <select class="form-control" required name="appeal_assign_to" id="appeal_reply_to">
                      <option value=""></option>
                    </select>
                    @else
                    <select class="form-control" name="appeal_assign_to" required>
                      <option value="">Select Sending Person</option>
                      <option value="dc">DC</option>
                      <option value="dse">DSE</option>
                      <option value="school">School</option>
                      @if(Auth::user()->user_role=='saa')
                      <option value="faa">RDDE</option>
                      @endif
                    </select>
                    @endif
                  </div>
                  <div class="form-group">
                    <div class="row files form-group" id="files2">
                      <label for="message-text" class="col-form-label">Upload Document :</label>
                      <input type="file" accept=".pdf, .jpg, .png, jpeg" multiple name="reply_document[]" class="form-control" id="message-text" />
                      </span>
                      <br />
                      <ul class="fileList"></ul>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Remarks :</label>
                    <textarea name="replyRemarks" class="form-control" id="message-text"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabel">Reject Appeal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="{{url('rejectRemark')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                  <input type="hidden" name="id" class="form-control AppealID ">
                  <input type="hidden" name="AppealSchoolID" class="form-control AppealSchoolID ">
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Remarks :</label>
                    <textarea required name="appealRejectRemarks" class="form-control" id="message-text"></textarea>
                  </div>
                  <div class="form-group">
                    <div class="row files form-group" id="files3">
                      <label for="message-text" class="col-form-label">Upload Document :</label>
                      <input type="file" accept=".pdf, .xls, .xlsx" multiple name="accpet_document[]" class="form-control" id="message-text" />
                      </span>
                      <br />
                      <ul class="fileList"></ul>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-primary" name="submit" value="sumbit">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal fade" id="AppealDocumentDownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabel">Document of Appeal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <table id="print_Document_links">
                  </table>
                </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="Conversation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelConversation" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabelConversation">Conversation of Appeal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <table id="Conversation_History_Data">
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="AppealImages" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelAppealImages" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h5 class="modal-title " id="exampleModalLabelConversation">Images of Appeal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <table id="Appeal_Images">
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- [ Main Content ] end -->
@include('footer')
<script>
  $(".viewDocument").click(function () {
    //  console.log('clicked');
    $('#print_Document_links').html('Document Not Present');
    var trData = $(this).closest('tr').find('.Uploaded_document_link').html();
    //  console.log(trData);
    $('#print_Document_links').html(trData);
    //  trData = '';
  });
  $(".reply_appeal").click(function () {
    var AppealID = $(this).closest("tr").find(".ID").text();
    var school_id = $(this).closest("tr").find("#school_id").val();
    $('.AppealID').val(AppealID)
    $('.AppealSchoolID').val(school_id)
    var appeal_to = $(this).closest("tr").find(".appeal_to").text().trim();;
    //  console.log(appeal_to);
    $('#appeal_reply_to').html(' <option value="-1">Select Sending Person</option> ');
    var option = ' <option value="-1">Select Sending Person</option> ';
    if (appeal_to == 'RDDE') {
      option += '<option value="faa">RDDE </option>';
    }
    if (appeal_to == 'Director of Primary Education') {
      option += '<option value="saa">Director of Primary Education </option>';
    }
    $('#appeal_reply_to').html(option);
    //  trData = '';
  });
  $(".viewAppealImages").click(function () {
    //  console.log('clicked');
    $('#Appeal_Images').html('Images Not Present');
    var trData = $(this).closest('tr').find('.AppealImagesData').html();
    //  console.log(trData);
    $('#Appeal_Images').html(trData);
    //  trData = '';
  });
  $(".viewConversation").click(function () {
    //  console.log('clicked');
    var AppealID = $(this).closest("tr").find(".ID").text();
    $.ajax({
                    url: 'update-appeal-status/'+AppealID,
                    type: "GET",
                    dataType: "json",
                 
                    success: function (data) {
                        console.log('success ');
                        }
                });
    $('#Conversation_History_Data').html('Conversation Not Present');
    var trData = $(this).closest('tr').find('.Conversation_History').html();
    //  console.log(trData);
    $('#Conversation_History_Data').html(trData);
    //  trData = '';
  });
  $(".accept_appeal").click(function () {
    var AppealID = $(this).closest("tr").find(".ID").text();
    $('.AppealID').val(AppealID)
    var school_id = $(this).closest("tr").find("#school_id").val();
    $('.AppealSchoolID').val(school_id)
  });
  $(".reject_appeal").click(function () {
    var AppealID = $(this).closest("tr").find(".ID").text();
    var school_id = $(this).closest("tr").find("#school_id").val();
    $('.AppealID').val(AppealID);
    $('.AppealSchoolID').val(school_id);
  });
  $(".viewData").click(function () {
    var text = $(this).closest("tr").find("#appealTextHide").val();
    var school_id = $(this).closest("tr").find("#school_id").val();
    var appeal_by = $(this).closest("tr").find(".appeal_by").text();
    var appeal_to = $(this).closest("tr").find(".appeal_to").text();
    var CreatedBy = $(this).closest("tr").find(".CreatedBy").text();
    var created_at = $(this).closest("tr").find(".created_at").text();
    var appeal_status = $(this).closest("tr").find(".appeal_status").text();
    var appealSchoolName = $(this).closest("tr").find(".appealSchoolName").text();
    var appealSchoolCode = $(this).closest("tr").find("#appealSchoolCode").val();
    var appealSchoolEmail = $(this).closest("tr").find("#appealSchoolEmail").val();
    var appealSchoolType = $(this).closest("tr").find("#appealSchoolType").val();
    var appealWebsite = $(this).closest("tr").find("#appealWebsite").val();
    var appealBoard = $(this).closest("tr").find("#appealBoard").val();
    var appealSchoolAddress = $(this).closest("tr").find("#appealSchoolAddress").val();
    var appealBlock = $(this).closest("tr").find("#appealBlock").val();
    var appealPO = $(this).closest("tr").find("#appealPO").val();
    var appealPS = $(this).closest("tr").find("#appealPS").val();
    var appealCluster = $(this).closest("tr").find("#appealCluster").val();
    var appealCity = $(this).closest("tr").find("#appealCity").val();
    var appealDistrict = $(this).closest("tr").find("#appealDistrict").val();
    var appealRemarksStatus = $(this).closest("tr").find("#appealRemarksStatus").val();
    var appealRemarks = $(this).closest("tr").find("#appealRemarks").val();
    var appealRemarksBy = $(this).closest("tr").find("#appealRemarksBy").val();
    var appealRemarksDate = $(this).closest("tr").find("#appealRemarksDate").val();
    if (appealRemarksStatus == '1') {
      appealRemarksStatus = 'Accepted';
    } else if (appealRemarksStatus == '2') {
      appealRemarksStatus = 'Rejected';
    } else {
      appealRemarksStatus = 'Requested';
    }
    $('#appealText').text(text);
    $('#SchoolID').text(school_id);
    $('#appealBy').text(appeal_by);
    $('#appealTo').text(appeal_to);
    $('#appealCreatedBy').text(CreatedBy);
    $('#created_at').text(created_at);
    $('#appeal_status').text(appeal_status);
    $('#appeal_school_name').text(appealSchoolName);
    $('#appeal_school_code').text(appealSchoolCode);
    $('#appeal_school_email').text(appealSchoolEmail);
    $('#appeal_school_type').text(appealSchoolType);
    $('#appeal_school_website').text(appealWebsite);
    $('#appeal_school_board').text(appealBoard);
    $('#appeal_school_address').text(appealSchoolAddress);
    $('#appeal_school_ps').text(appealPS);
    $('#appeal_school_po').text(appealPO);
    $('#appeal_school_block').text(appealBlock);
    $('#appeal_school_city').text(appealCity);
    $('#appeal_school_cluster').text(appealCluster);
    $('#appeal_school_district').text(appealDistrict);
    $('#remarks').text(appealRemarks);
    $('#remarks_by').text(appealRemarksBy);
    $('#remarks_date').text(appealRemarksDate);
    $('#remarks_status').text(appealRemarksStatus);
  });
</script>
<!-- %%%%%%%%%%%%%%%%%%%%%%% script to list division, district, and block %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
<script type="text/javascript">
  $(document).ready(function () {
    $('select[name="division"]').on('change', function () {
      var stateID = $(this).val();
      var options = '<option value="-1">Select District </option>';
      if (stateID) {
        $.ajax({
          url: 'district/district_list/' + stateID,
          type: "GET",
          dataType: "json",
          success: function (data) {
            console.log('district ')
            console.log(data);
            Object.entries(data).forEach(entry => {
              const [key, value] = entry;
              options += '<option value="' + key + '">' + value + '</option>';
              console.log(key, value);
            });
            $('#district').html(options);
            $('#block').html('<option value="-1">Select Block </option>');
          }
        });
      } else {
        $('#district').empty();
      }
    });
    $('select[name="district"]').on('change', function () {
      var stateID = $(this).val();
      var options = '<option value="-1">Select Block </option>';
      if (stateID) {
        $.ajax({
          url: 'block/block_list/' + stateID,
          type: "GET",
          dataType: "json",
          success: function (data) {
            console.log('block ');
            console.log(data);
            Object.entries(data).forEach(entry => {
              const [key, value] = entry;
              options += '<option value="' + key + '">' + value + '</option>';
              console.log(key, value);
            });
            $('#block').html(options);
          }
        });
      } else {
        $('#block').empty();
      }
    });
  });
</script>