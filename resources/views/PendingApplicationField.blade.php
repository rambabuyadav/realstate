@include('header')
@include('sidenav')
@include('topbar')
<!-- [ Header ] end -->
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">School Pending Fields</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href=""> Pending Fields</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">
                    <!-- general information -->
                    <!-- <div class="card-header"> -->

                    <!-- </div> -->
                    <div class="card-body">
                        <table class="table table-bordered table-sm ">
                            <thead>
                                <th>SL NO.</th>
                                <th>FIELD NAME</th>
                                <th>VALUE</th>
                                <!-- <th> Remarks </th> -->
                                <th> Status </th>
                            </thead>
                            @php $tot = 1; $Y = 1; @endphp
                            @if(@$user->school_name_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">School Name</td>
                                <td class="value_name">{{@$user->school_name}}</td>
                                <!-- <td>{{@$user->school_name_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->recognised_by_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Academic Session From Which Recognition Proposed</td>
                                <td class="value_name">{{@$user->recognised_by}}</td>
                                <!-- <td>{{@$user->recognised_by_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->district_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">District</td>
                                <td class="value_name">{{@$user->district}} </td>
                                <!-- <td>{{@$user->district_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->post_office_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Post Office</td>
                                <td class="value_name">{{@$user->post_office}} </td>
                                <!-- <td>{{@$user->post_office_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->village_city_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Village / Town</td>
                                <td class="value_name"> {{@$user->village_city}}</td>
                                <!-- <td>{{@$user->village_city_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->pincode_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Pin Code</td>
                                <td class="value_name">{{@$user->pincode}} </td>
                                <!-- <td>{{@$user->pincode_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->phone_with_std_code_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Ph./Mb no. With STD Code</td>
                                <td class="value_name"> {{@$user->phone_with_std_code}}</td>
                                <!-- <td>{{@$user->phone_with_std_code_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->fax_with_std_code_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">FAX No. With STD Code</td>
                                <td class="value_name"> {{@$user->fax_with_std_code}}</td>
                                <!-- <td>{{@$user->fax_with_std_code_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;"><span>Pending</span></td>
                            </tr>
                            @endif
                            @if(@$user->email_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Email ID</td>
                                <td class="value_name">{{@$user->email}} </td>
                                <!-- <td>{{@$user->email_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->police_station_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Nearest Police Station</td>
                                <td class="value_name">{{@$user->police_station}} </td>
                                <!-- <td>{{@$user->police_station_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->estd_year_vrfy == 1 || @$user->opening_date_vrfy == 1 || @$user->society_name_vrfy == 1 || @$user->is_society_registered_vrfy == 1 || @$user->society_registration_valid_upto_vrfy == 1 || @$user->society_registration_valid_upto_vrfy == 1 || @$user->evidence_of_non_proprietary_nature_vrfy == 1 || @$user->evidence_of_non_proprietary_nature_vrfy == 1 )
                            <tr>
                                <td colspan="3">
                                    <div class="card-header pt-0 pb-0">
                                        General Information
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->estd_year_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Establishment Year</td>
                                <td class="value_name">{{@$user->estd_year}} </td>
                                <!-- <td>{{@$user->estd_year_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->opening_date_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">School Opening Date </td>
                                <td class="value_name">{{@$user->opening_date}}</td>
                                <!-- <td>{{@$user->opening_date_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->society_name_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">trust/Society/Management Committee Name</td>
                                <td class="value_name"> {{@$user->society_name}}</td>
                                <!-- <td>{{@$user->society_name_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->is_society_registered_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">is trust/society/management comittee registered?</td>
                                <td class="value_name">{{@$user->is_society_registered}}</td>
                                <!-- <td>{{@$user->is_society_registered_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->society_registration_valid_upto_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">The period until the registration of the Trust / Society / Management Committee is valid</td>
                                <td class="value_name"> @if($user->school_validation_till==1)
                                    Life Time
                                    @endif
                                    @if($user->school_validation_till==2)
                                    Defined Date
                                    @endif</td>
                                <!-- <td>{{@$user->society_registration_valid_upto_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->society_registration_valid_upto_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Valid Date </td>
                                <td class="value_name"> {{@$user->society_registration_valid_upto}}</td>
                                <!-- <td>{{@$user->society_registration_valid_upto_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->evidence_of_non_proprietary_nature_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of <br> members including their addresses on affidavit ?</td>
                                <td class="value_name"> {{@$user->evidence_of_non_proprietary_nature1}}</td>
                                <!-- <td>{{@$user->evidence_of_non_proprietary_nature_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->evidence_of_non_proprietary_nature_vrfy == 1 )
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Add Relevant Evidence Attachement</td>
                                <td class="value_name">
                                    @if(isset($user->evidence_of_non_proprietary_nature_ex))
                                    @foreach($user->evidence_of_non_proprietary_nature_ex as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a><br>
                                    @endforeach
                                    @endif
                                </td>
                                <!-- <td>{{@$user->evidence_of_non_proprietary_nature_rmk}} </td> -->
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            @endif
                            @if(@$user->school_chairman_name_vrfy == 1 )
                            <tr>
                                <td colspan="3"><b> Chairman Information</b></td>
                                <!-- <td rowspan="7">{{@$user->school_chairman_name_rmk}} </td> -->
                                <td rowspan="7" style="vertical-align : middle;text-align:center;">
                                    <span>Pending</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Chairman Name </td>
                                <td class="value_name"> {{@$user->school_chairman_name}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Designation</td>
                                <td class="value_name">{{@$user->school_chairman_post}} </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Address</td>
                                <td class="value_name">{{@$user->school_chairman_address}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Phone No.</td>
                                <td class="value_name">{{@$user->school_chairman_phone_number}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Office</td>
                                <td class="value_name"> {{@$user->school_chairman_office}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Email ID</td>
                                <td class="value_name">{{@$user->school_chairman_email}} </td>
                            </tr>
                            @endif
                            @if(@$user->i_e_s_r_l_year1_vrfy == 1 || @$user->i_e_s_r_l_year2_vrfy == 1 || @$user->i_e_s_r_l_year3_vrfy == 1 )
                            <tr>
                                <td colspan="5"><b> 3 Years Total Income / Expenses / Surplus / Loss</b></td>
                            </tr>
                            @endif
                            @for($i=1;$i < 4 ; $i++) @php $vrfy='i_e_s_r_l_year' .$i.'_vrfy'; $rmk='i_e_s_r_l_year' .$i.'_rmk'; $files='i_e_s_r_l_year' .$i.'_files'; @endphp @if(@$user->$vrfy == 1 )
                                <tr>
                                    <td colspan="5"><b> Year {{$i}}</b></td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Year</td>
                                    <td class="value_name"> {{@$user->session_year[$i]}}</td>
                                    <!-- <td rowspan="6">{{@$user->$rmk}}</td> -->
                                    <td rowspan="6" style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Income</td>
                                    <td class="value_name">{{@$user->income[$i]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Expenses</td>
                                    <td class="value_name"> {{@$user->expenses[$i]}}</td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Surplus Money</td>
                                    <td class="value_name"> {{@$user->surplus_money[$i]}}</td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Reduce</td>
                                    <td class="value_name"> {{@$user->reduced_money[$i]}}</td>
                                </tr>
                                <tr>
                                    @php
                                    $File_name = 'last_three_year_tot_income_files_'.$Y;
                                    @endphp
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add Relevant Evidence Attachement</td>
                                    <td class="value_name">
                                        @if(isset($user->$File_name))
                                        @foreach($user->$File_name as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endfor
                                @if( @$user->medium_vrfy == 1 || @$user->type_of_school_vrfy == 1 || @$user->supported_agency_name_vrfy == 1 || @$user->agency_supported_percen_vrfy == 1 || @$user->authority_name_vrfy == 1 || @$user->recognised_number_vrfy == 1 || @$user->is_school_on_rente_vrfy == 1 || @$user->are_school_building_use_vrfy == 1 || @$user->school_total_are_vrfy == 1 || @$user->plot_number_vrfy == 1 || @$user->khata_number_vrfy == 1 || @$user->school_building_area_vrfy == 1 )
                                <tr>
                                    <th colspan="4">
                                        <div class="card-header pb-0 pt-0">
                                            School Area and Format Details
                                        </div>
                                    </th>
                                </tr>
                                @endif
                                @if(@$user->medium_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Medium Of Education</td>
                                    <td class="value_name">{{@$user->medium}} </td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->type_of_school_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </td>
                                    <td class="value_name">{{@$user->type_of_school}} </td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->supported_agency_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Agency Name (if school has support) </td>
                                    <td class="value_name">{{@$user->supported_agency_name}} </td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->agency_supported_percent_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Support Percentage</td>
                                    <td class="value_name"> {{@$user->agency_supported_percent}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->authority_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">If School Is Recognised Mention Authority Name</td>
                                    <td class="value_name">{{@$user->authority_name}} </td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->recognised_number_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Recognition Number</td>
                                    <td class="value_name">{{@$user->recognised_number}} </td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->is_school_on_rented_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">The School Has Its Own Building Or Is Working In A Rented Building ? </td>
                                    <td class="value_name">{{@$user->is_school_on_rented}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->school_rent_deatail_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_rent_deatail_ext </td>
                                    <td class="value_name">
                                        @if(@isset($user->school_rent_deatail))
                                        @foreach($user->school_rent_deatail as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->school_rent_deatail_rmk}} </td> -->

                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>

                                    </td>

                                </tr>
                                @endif
                                @if(@$user->are_school_building_used_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development? </td>
                                    <td class="value_name">{{@$user->are_school_building_used}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->school_total_area_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Total Area Of School</td>
                                    <td class="value_name">{{@$user->school_total_area}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif

                                @if(@$user->plot_number_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Plot Number</td>
                                    <td class="value_name">{{@$user->plot_number}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->khata_number_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Khata Number</td>
                                    <td class="value_name">{{@$user->khata_number}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->school_building_area_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Area Of School Building Only</td>
                                    <td class="value_name">{{@$user->school_building_area}}</td>
                                    <!-- <td></td> -->
                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->one_lac_fd_proof_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Document For Fixed Deposit Of One Lakh Rupees In The Name Of School</td>
                                    <td class="field_name" style="display: none;"> one_lac_fd_proof_ext </td>
                                    <td class="value_name">
                                        @if($user->one_lac_fd_proof != null )
                                        @foreach($user->one_lac_fd_proof as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->one_lac_fd_proof_rmk}} </td> -->

                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->school_area_deatail_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_area_deatail_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_area_deatail))
                                        @foreach($user->school_area_deatail as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->school_area_deatail_rmk}} </td> -->

                                    <td style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>

                                </tr>
                                @endif

                                @if(@$user->financial_details_vrfy == 1 )
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pb-0 pt-0">
                                            Financial Details
                                        </div>
                                    </td>
                                    <!-- <td rowspan="{{(count($user->teaching_fee)*10)+1}}" style="vertical-align : middle;text-align:center;">{{@$user->financial_details_rmk}} </td> -->
                                    <td rowspan="{{(count($user->teaching_fee)*10)+1}}" style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @if(isset($user->teaching_fee))
                                @foreach($user->teaching_fee as $key => $val)
                                <tr>
                                    <th colspan="3" class="value_name">{{@$user->class_finance[$key]}} </th>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Tuition Fee </td>
                                    <td class="value_name">{{@$user->teaching_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Registration Fee </td>
                                    <td class="value_name">{{@$user->registration_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Admission Fee </td>
                                    <td class="value_name">{{@$user->enrollment_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Reserve Deposite </td>
                                    <td class="value_name">{{@$user->security_money[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Annual Charges </td>
                                    <td class="value_name">{{@$user->annual_charge[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Development Fee </td>
                                    <td class="value_name">{{@$user->development_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Other Charges </td>
                                    <td class="value_name">{{@$user->other_charges[$key]}} </td>
                                </tr>
                                <tr>
                                    <td>{{$tot++}}</td>
                                    <td class="lable_name">Total </td>
                                    <td class="value_name">{{@$user->total[$key]}} </td>
                                </tr>
                                <tr>
                                </tr>
                                @endforeach
                                @endif
                                @endif
                                <!-- Enrollment -->
                                @if(@$user->pre_elementary_class_vrfy == 1 )
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pb-0 pt-0">
                                            Enrollment
                                        </div>
                                        <!-- <td rowspan="{{(count($user->class_names)*6)+1}}" style="vertical-align : middle;text-align:center;">{{@$user->pre_elementary_class_rmk}} </td> -->
                                    <td rowspan="{{(count($user->class_names)*6)+1}}" style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                    </td>
                                </tr>
                                @if(isset($user->class_names))
                                @foreach($user->class_names as $key => $val)
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">{{$val}}</td>
                                    <td class="value_name">{{@$val}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Section </td>
                                    <td class="value_name">{{@$user->no_of_sections[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Students</td>
                                    <td class="value_name">{{@$user->no_of_students[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Teacher Student Ratio</td>
                                    <td class="value_name">{{@$user->teacher_student_ratio[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Teacher Student Assign</td>
                                    <td class="value_name">{{@$user->teacher_student_assign[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Teacher</td>
                                    <td class="value_name">{{@$user->num_of_teacher[$key]}} </td>
                                </tr>
                                @endforeach
                                @endif
                                @endif
                                <!-- infrastructure details -->
                                @if(@$user->no_of_class_vrfy == 1 || @$user->avg_size_cls_room_vrfy == 1 || @$user->no_of_office_room_vrfy == 1 || @$user->avg_size_of_office_room_vrfy == 1 || @$user->no_of_store_room_vrfy == 1 || @$user->avg_size_of_store_room_vrfy == 1 || @$user->no_of_princpal_room_vrfy == 1 || @$user->avg_size_of_principal_room_vrfy == 1 || @$user->no_of_kitchen_room_vrfy == 1 || @$user->avg_size_of_kitchen_room_vrfy == 1 || @$user->is_school_has_boundary_vrfy == 1 || @$user->is_school_cctv_vrfy == 1 || @$user->no_of_school_cctv_vrfy == 1 || @$user->school_cctv_file_vrfy == 1 || @$user->is_school_ground_vrfy == 1 || @$user->size_of_school_ground_vrfy == 1 || @$user->school_ground_file_vrfy == 1 || @$user->is_school_water_hygene_facilities_vrfy == 1 || @$user->no_of_school_water_hygene_facilities_vrfy == 1 || @$user->school_water_hygene_facilities_files_vrfy == 1 || @$user->is_school_fire_safty_facilities_vrfy == 1 || @$user->school_fire_safty_facilities_files_vrfy == 1 )
                                <tr>
                                    <td colspan="4">
                                        <div class="card-header pt-0 pb-0">
                                            Infrastructure Details
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_class_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Classes </td>
                                    <td class="value_name">{{@$user->no_of_class}} </td>
                                    <!-- <td>{{@$user->no_of_class_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->avg_size_cls_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Classroom (in w*h)</td>
                                    <td class="value_name">{{@$user->avg_size_cls_room}} </td>
                                    <!-- <td>{{@$user->avg_size_cls_room_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_office_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Office Rooms</td>
                                    <td class="value_name">{{@$user->no_of_office_room}} </td>
                                    <!-- <td>{{@$user->no_of_office_room_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->avg_size_of_office_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Office Rooms (in w*h)</td>
                                    <td class="value_name">{{@$user->avg_size_of_office_room}} </td>
                                    <!-- <td>{{@$user->avg_size_of_office_room_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_store_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Store Rooms</td>
                                    <td class="value_name">{{@$user->no_of_store_room}}</td>
                                    <!-- <td>{{@$user->no_of_store_roo_rmkm}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->avg_size_of_store_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Store Rooms (in w*h)</td>
                                    <td class="value_name">{{@$user->avg_size_of_store_room}} </td>
                                    <!-- <td>{{@$user->avg_size_of_store_room_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_princpal_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Principal Rooms</td>
                                    <td class="value_name">{{@$user->no_of_princpal_room}}</td>
                                    <!-- <td>{{@$user->no_of_princpal_roo_rmkm}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->avg_size_of_principal_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Principal Rooms (in w*h)</td>
                                    <td class="value_name">{{@$user->avg_size_of_principal_room}}</td>
                                    <!-- <td>{{@$user->avg_size_of_principal_roo_rmkm}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->no_of_kitchen_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Kitchen Rooms</td>
                                    <td class="value_name">{{@$user->no_of_kitchen_room}} </td>
                                    <!-- <td>{{@$user->no_of_kitchen_room_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->avg_size_of_kitchen_room_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Kitchen Rooms (in w*h)</td>
                                    <td class="value_name">{{@$user->avg_size_of_kitchen_room}} </td>
                                    <!-- <td>{{@$user->avg_size_of_kitchen_room_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->is_school_has_boundary_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Does The School Has Boundary</td>
                                    <td class="value_name">{{@$user->is_school_has_boundary}} </td>
                                    <!-- <td>{{@$user->is_school_has_boundary_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->size_of_school_boundary_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Size Of Boundry( W * H ) </td>
                                    <td class="value_name">{{@$user->size_of_school_boundary}} </td>
                                    <!-- <td>{{@$user->size_of_school_boundary_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->school_boundary_files_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_boundary_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_boundary_files))
                                        @foreach($user->school_boundary_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->is_school_cctv_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Does School Has CCTV ? </td>
                                    <td class="value_name">{{@$user->is_school_cctv}} </td>
                                    <!-- <td>{{@$user->is_school_cctv_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_school_cctv_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of CCTV </td>
                                    <td class="field_name" style="display: none;"> no_of_school_cctv_ext </td>
                                    <td class="value_name">{{@$user->no_of_school_cctv}} </td>
                                    <!-- <td>{{@$user->no_of_school_cctv_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->school_cctv_file_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_cctv_file_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_cctv_file))
                                        @foreach($user->school_cctv_file as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->school_cctv_file_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->is_school_ground_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Sport Ground ? </td>
                                    <td class="value_name">{{@$user->is_school_ground}} </td>
                                    <!-- <td>{{@$user->is_school_ground_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->size_of_school_ground_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Size Of Sport Ground ( W * H )
                                    </td>
                                    <td class="field_name" style="display: none;"> size_of_school_ground_ext </td>
                                    <td class="value_name">{{@$user->size_of_school_ground}} </td>
                                    <!-- <td>{{@$user->size_of_school_ground_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->school_ground_file_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_ground_file_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_ground_file))
                                        @foreach($user->school_ground_file as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->school_ground_file_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->is_school_water_hygene_facilities_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Dose The School Has 3 Start Rating In Water And Hygene Facilities ? </td>
                                    <td class="value_name">{{@$user->is_school_water_hygene_facilities}} </td>
                                    <!-- <td>{{@$user->is_school_water_hygene_facilities_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_school_water_hygene_facilities_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Water And Hygene Facilitie</td>
                                    <td class="field_name" style="display: none;"> no_of_school_water_hygene_facilities_ext </td>
                                    <td class="value_name">{{@$user->no_of_school_water_hygene_facilities}} </td>
                                    <!-- <td>{{@$user->no_of_school_water_hygene_facilities_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->school_water_hygene_facilities_files_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement </td>
                                    <td class="field_name" style="display: none;"> school_water_hygene_facilities_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_water_hygene_facilities_files))
                                        @foreach($user->school_water_hygene_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->school_water_hygene_facilities_files_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->is_school_fire_safty_facilities_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Dose The School Has Fire Safty Facilities ? </td>
                                    <td class="value_name">{{@$user->is_school_fire_safty_facilities}} </td>
                                    <!-- <td>{{@$user->is_school_fire_safty_facilities_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->no_of_fire_safty_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Number Of fire safty
                                    </td>
                                    <td class="field_name" style="display: none;"> no_of_fire_safty_ext </td>
                                    <td class="value_name">{{@$user->size_of_school_ground}}</td>
                                    <!-- <td>{{@$user->no_of_fire_safty_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                @if(@$user->school_fire_safty_facilities_files_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement </td>
                                    <td class="field_name" style="display: none;"> school_fire_safty_facilities_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_fire_safty_facilities_files))
                                        @foreach($user->school_fire_safty_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->school_fire_safty_facilities_files_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending </td>
                                </tr>
                                @endif
                                <!-- other convienance -->
                                @if(@$user->facilities_access_without_interrupted_vrfy == 1 || @$user->all_teaching_material_list_vrfy == 1 || @$user->all_sports_equipment_list_vrfy == 1 || @$user->books_vrfy == 1 || @$user->magazines_vrfy == 1 || @$user->type_of_water_facilities_vrfy == 1 || $user->no_of_water_supply_vrfy == 1 || $user->type_of_toilet_vrfy == 1 || $user->gents_toilet_vrfy == 1 || $user->ladies_toilet_vrfy == 1 )
                                <tr>
                                    <td colspan="4">
                                        <div class="card-header pb-0 pt-0">
                                            Other Convenience
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @if(@$user->facilities_access_without_interrupted_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Does all facilities have access without interrupted? </td>
                                    <td class="value_name"> {{@$user->facilities_access_without_interrupted}}</td>
                                    <!-- <td> {{@$user->facilities_access_without_interrupted_rmk}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->all_teaching_material_list_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">List All Teaching Materials</td>
                                    <td class="value_name"> {{@$user->all_teaching_material_list}}</td>
                                    <!-- <td> {{@$user->all_teaching_material_list_rmk}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif

                                @if(@$user->all_teaching_material_list_files_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevant Evidence Attachement</td>
                                    <td class="value_name">
                                        @if(isset($user->all_teaching_material_list_files_ex))
                                        @foreach($user->all_teaching_material_list_files_ex as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->all_teaching_material_list_files_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif

                                @if(@$user->all_sports_equipment_list_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">List All Sports Equipments</td>
                                    <td class="value_name"> {{@$user->all_sports_equipment_list}}</td>
                                    <!-- <td> {{@$user->all_sports_equipment_list_rmk}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->all_sports_equipment_list_file_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevant Evidence Attachement</td>
                                    <td class="value_name">
                                        @if(isset($user->all_sports_equipment_list_file))
                                        @foreach($user->all_sports_equipment_list_file as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->books_vrfy == 1 || @$user->magazines_vrfy == 1 )
                                <tr>
                                    <th colspan="4">Books Facilities In Library</th>
                                </tr>
                                @endif
                                @if(@$user->books_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Books In Library</td>
                                    <td class="value_name"> {{@$user->books}}</td>
                                    <!-- <td> {{@$user->books_rmk}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->magazines_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Newspaper & Magzine</td>
                                    <td class="value_name"> {{@$user->magazines}}</td>
                                    <!-- <td> {{@$user->magazines_rmk}}</td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending {{$user->books_in_library_vrfy}}</td>
                                </tr>
                                @endif
                                @if(@$user->books_in_library_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> books_in_library_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->books_in_library))
                                        @foreach($user->books_in_library as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->books_in_library_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->type_of_water_facilities_vrfy == 1 || $user->no_of_water_supply_vrfy == 1 || $user->water_facilities_vrfy == 1 )
                                <tr>
                                    <th colspan="4">Water Facilities</th>
                                </tr>
                                @endif
                                @if(@$user->type_of_water_facilities_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Types Of Water Facilities</td>
                                    <td class="value_name">{{@$user->type_of_water_facilities}} </td>
                                    <!-- <td>{{@$user->type_of_water_facilities_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if($user->no_of_water_supply_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Water Supply</td>
                                    <td class="value_name">{{@$user->no_of_water_supply}} </td>
                                    <!-- <td>{{@$user->no_of_water_supply_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if($user->water_facilities_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Water Supply Document</td>
                                    <td class="value_name">

                                        @if(isset($user->water_facilities))
                                        @foreach($user->water_facilities as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->water_facilities_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if( $user->type_of_toilet_vrfy == 1 || $user->gents_toilet_vrfy == 1 || $user->ladies_toilet_vrfy == 1 )
                                <tr>
                                    <th colspan="4">Cleanliness Related Details</th>
                                </tr>
                                @endif
                                @if($user->type_of_toilet_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Type Of Toilets</td>
                                    <td class="value_name">{{@$user->type_of_toilet}} </td>
                                    <!-- <td>{{@$user->type_of_toilet_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if($user->gents_toilet_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Seperate Toilet For Boys</td>
                                    <td class="value_name">{{@$user->gents_toilet}} </td>
                                    <!-- <td>{{@$user->gents_toilet_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if($user->ladies_toilet_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Seperate Toilet For Girls</td>
                                    <td class="value_name">{{@$user->ladies_toilet}} </td>
                                    <!-- <td>{{@$user->ladies_toilet_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif
                                @if(@$user->cleanliness_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> cleanliness_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->cleanliness))
                                        @foreach($user->cleanliness as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <!-- <td>{{@$user->cleanliness_rmk}} </td> -->
                                    <td style="vertical-align : middle;text-align:center;">Pending</td>
                                </tr>
                                @endif

                                @if($user->principle_name_vrfy == 1 )
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pt-0 pb-0">
                                            Principal Specialties
                                        </div>
                                    </td>
                                    <!-- <td rowspan="10">{{@$user->principle_name_rmk}} </td> -->
                                    <td rowspan="10" style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                    <td style="display: none;" class="lable_name"> Name of principal</td>
                                    <td style="display: none;" class="value_name">{{@$user->principle_name}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Name of principal</td>
                                    <td class="value_name">{{@$user->principle_name}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Father / Husband Or Wife Name</td>
                                    <td class="value_name">{{@$user->p_f_h_w_name}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date Of Birth</td>
                                    <td class="value_name">{{@$user->p_date_of_birth}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Educational Qualification</td>
                                    <td class="value_name">{{@$user->p_education_qualification}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trainee Qualification</td>
                                    <td class="value_name">{{@$user->pri_trainee_qualification}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teaching Experience</td>
                                    <td class="value_name">{{@$user->pri_teaching_experience}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Class Handed Over</td>
                                    <td class="value_name">{{@$user->class_handed_over}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date Of Appointment</td>
                                    <td class="value_name">{{@$user->pri_date_of_appointment}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trained Or Untrained</td>
                                    <td class="value_name">{{@$user->trained_untrained}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevant Evidence Attachement</td>
                                    <td class="value_name">
                                        @if(isset($user->educational_qualification_files))
                                        @foreach($user->educational_qualification_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>

                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pt-0 pb-0">
                                            Teachers Information
                                        </div>
                                    </td>
                                    <!-- <td rowspan="{{(count($user->application_teacher_data)*11)+1}}" style="vertical-align : middle;text-align:center;">{{@$user->teacher_name_rmk}} </td> -->
                                    <td rowspan="{{(count($user->application_teacher_data)*11)+1}}" style="vertical-align : middle;text-align:center;">
                                        <span>Pending</span>
                                    </td>
                                </tr>
                                @if(isset($user->application_teacher_data))
                                @foreach($user->application_teacher_data as $key => $val)
                                <tr>
                                    <th colspan="3">
                                        Tearcher {{$key+1}}
                                    </th>
                                </tr>
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Name of Teacher</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->teacher_name}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teacher Father/Husband/Wife Name</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->teacher_f_h_w_name}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date Of Birth</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->date_of_birth}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Education Qualification</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->education_qualification}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trainee Qualification</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->trainee_qualification}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teaching Experience</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->teaching_experience}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Class Handed Over</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->cls_handed_over}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date of Appointment</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->date_of_appointment}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trained or Untrained</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->trained_or_untrained}} </td>
                                </tr>
                                @endif
                                @if(@$user->teacher_name_vrfy == 1 )
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teacher Educational Qualification Files</td>
                                    <td class="value_name">
                                        @if($user->application_teacher_data[$key]->teacher_educational_qualification_files != null)
                                        @php $user->application_teacher_data[$key]->teacher_educational_qualification_files = json_decode($user->application_teacher_data[$key]->teacher_educational_qualification_files, true); @endphp
                                        @foreach($user->application_teacher_data[$key]->teacher_educational_qualification_files as $key_1 => $val_1)
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val_1}}" target="_blank"> File {{$key_1+1}}</a>
                                        @endif
                                        @endforeach
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                                @endif
                                @endif
                                @if(@$user->details_of_curriculum_vrfy == 1 )
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pt-0 pb-0">
                                            Curriculum and Syllabus
                                        </div>
                                    </td>
                                    <!-- <td rowspan=" {{(count($user->details_of_curriculum)*2)+2}}">{{@$user->details_of_curriculum_rmk}} </td> -->
                                    <td rowspan=" {{(count($user->details_of_curriculum)*2)+2}}" style="vertical-align : middle;text-align:center;">
                                        <span>Pending
                                        </span>
                                    </td>
                                </tr>
                                @if(isset($user->details_of_curriculum))
                                @foreach($user->details_of_curriculum as $key => $val)
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6)</td>
                                    <td class="value_name">{{@$val}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Method Of Inspection Of Students</td>
                                    <td class="value_name">{{@$user->method_of_inspection[$key]}} </td>
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Are the students of the school expected to take any board examination till class 6 ?</td>
                                    <td class="value_name">{{@$user->school_board_exam_till_cls_eight}} </td>
                                </tr>
                                @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
</div>
@include('footer')