@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">
                <div class="btn-group">
                  <form method="post" action="{{url('school')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="school_status" value="0">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">All</button>
                  </form>
                  <form method="post" action="{{url('school')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="school_status" value="1">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Registered</button>
                  </form>
                  <form method="post" action="{{url('school')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="school_status" value="3">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Pending</button>
                  </form>
                  <form method="post" action="{{url('school')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="school_status" value="6">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Approved</button>
                  </form>
                  <form method="post" action="{{url('school')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="school_status" value="8">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Rejected by DSE</button>
                  </form>
                  <form method="post" action="{{url('school')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="school_status" value="7">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Rejected by DC</button>
                  </form>
                </div>
              </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">Schools Details</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <form action="{{url('school')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" class='text-block' name="search_text" value="{{@$adv_data['search_text']}}" />
              <input type="hidden" class='text-block' name="Block" value="{{$adv_data['block_id']}}" />
              <input type="hidden" class='text-block' name="to_date" value="{{$adv_data['to_date']}}" />
              <input type="hidden" class='text-block' name="from_date" value="{{@$adv_data['from_date']}}" />
              <button type="submit" name="submit" value="excel" class="btn btn-sm btn-light btn-offset pull-right" title="Download Schools Data &#10; in Export">
                <i class="fa fa-file-excel text-success"> </i></button>
              <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
              <a href="{{url('school')}}"> <button type="button" class="btn btn-danger btn-sm pull-right" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button></a>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <!-- <h1>Here is all Payment Related Details</h1> -->
        <div class="shadow-lg p-3 mt--6 bg-white rounded">
          <table class="table table-sm table-bordered table-responsive">
            <thead>
              <tr style="width: 100%;">
                <th style="width: 20%;">UDISE Code</th>
                <th style="width: 20%;">School Name</th>
                <th style="width: 20%;">Block Name</th>
                <th style="width: 20%;">School Email</th>
                <th style="width: 20%;">Username</th>
                <!--  <th style="width: 20%;">Block</th> -->
                <!-- <th style="width: 20%;">State</th> -->
                <!-- <th style="width: 20%;">Status</th> -->
                <th style="width: 20%;">User Email</th>
                <th style="width: 20%;">Created At</th>
              </tr>
            </thead>
            <tbody>
              @php
              $i=0;
              @endphp
              @foreach($Alldata as $key => $data)
              @php
              $i = 1;
              @endphp
              <tr>
              <td>{{@$data->school_code}}</td>
              <td>{{@$data->school_name}}</td>
              <td>
                {{$data->block_name}}</td>
              <td>{{@$data->school_email}}</td>
              <td>{{@$data->name}}</td>
              <td>{{@$data->useremail}}</td>
              <td>
                {{@$data->created_at}}
                <!-- date('d-m-Y', strtotime(@$data->created_at)); -->
              </td>
              </tr>
              @endforeach
              @if($i == 0)
              <tr>
                <th colspan="7" style="text-align: center;"><span>No School Found!!</span></th>
              </tr>
              @endif
              <!-- <tr>
                <td>Vidya Bharati High School</td>
                <td>Aman Kumar Jha</td>
                <td>7854021546</td>
                <td>Near NKS Field, Gamharia</td>
                <td>Balrampur</td>
                <td> Gamharia</td>
                <td>12/05/2020</td>
              </tr>
              <tr>
                <td>Vani Vidya School</td>
                <td>Rajiv Kumar Jha</td>
                <td>7857856346</td>
                <td>Road no. 4 Durga Maidan, Gamharia</td>
                <td>Balrampur</td>
                <td> Gamharia</td>
                <td>17/05/2020</td>
              </tr>
              <tr>
                <td>Kerala Public School</td>
                <td>Ankita Kumari</td>
                <td>8544021546</td>
                <td>Line 6, Near Kali mandir</td>
                <td>vishunpura</td>
                <td> Gamharia</td>
                <td>10/05/2020</td>
              </tr> -->
            </tbody>
          </table>
          {{-- Pagination --}}
          <style>
            .pl-4,
            .px-4 {
              padding-left: 1.5rem !important;
              display: none;
            }
          </style>
          <div class="d-flex justify-content-center">
    {!! $Alldata->links() !!}
</div>
          
        </div>
        <div class="modal fade" id="gfgmodal" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h2 class="modal-title" id="gfgmodallabel">Selected row</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="GFGclass" id="divGFG"></div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- [ Main Content ] end -->
  </div>
</div>
<!-- [ Adv Search Model ] -->
<div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form action="{{url('school')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for=""> Advance Search Index </label>
                @if(isset($adv_data['search_text']))
                <input type="text" name="search_text" value="{{@$adv_data['search_text']}}" class="form-control rte_input" id="search_text" placeholder="School name/ E-mail / Address/ Transaction id ">
                @else
                <input type="text" name="search_text" id="search_text" class="form-control rte_input" placeholder="School name/School Email/ E-mail/ Username/ UDISE Code">
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="Block"> Block </label>
                <select class="form-control rte_input" name="Block" id="Block">
                  @isset($adv_data['block_id'])
                  <option value="{{$adv_data['block_id']}}">{{$adv_data['block_name']}}</option>
                  @endisset
                  <option value="">--Select--</option>
                  @foreach(@$block_list as $key => $value)
                  <option value="{{$key}}">{{$value}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for=""> From Date </label>
                @if(isset($adv_data['search_text']))
                <input type="date" name="from_date" value="{{@$adv_data['from_date']}}" class="form-control rte_input">
                @else
                <input type="date" name="from_date" id="adv_search" class="form-control rte_input">
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for=""> To Date </label>
                @if(isset($adv_data['search_text']))
                <input type="date" name="to_date" value="{{@$adv_data['to_date']}}" class="form-control rte_input">
                @else
                <input type="date" name="to_date" id="adv_search" class="form-control rte_input">
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <input type="reset" name="Reset" value="Reset" id="Reset" class="btn btn-primary"> -->
          <input type="submit" name="submit" value="Submit" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- [ Adv Search End ] -->
@include('footer')