@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-10">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Report</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{url('report')}}">Report </a></li>
                        <li class="breadcrumb-item"><a href="">Demo Graphic Report </a></li>
                    </ul>
                </div>
                <div class="col-md-2 text-right pr-4">
                    <button type="button" class="btn btn-sm btn-light btn-offset" ><a class="text-success" title="add " > <i class="fa fa-file-excel text-success" > </i>  Export </a></button>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">Demo Graphic Report</div>
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for=""> Advance Search Index </label>
                                    <input required type="text" name="first_name"  class="form-control rte_input" placeholder="School/ Application no./ E-mail/ Mobile">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for=""> Division </label>
                                       <select class="form-control rte_input" name="division" required>
                                            <option value="-1">Select Division </option>
                                            @foreach($Districts as $District)
                                            <option value="{{$District->id}}">{{$District->value_set_name }}</option>
                                            @endforeach
                                       </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for=""> District </label>
                                       <select class="form-control rte_input" name="district" required id="district">
                                            <option value="-1">Select District </option>
                                            @foreach($Districts as $District)
                                            <option value="{{$District->id}}">{{$District->value_set_name }}</option>
                                            @endforeach
                                       </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for=""> Block </label>
                                       <select class="form-control rte_input" name="block" id="block" required>
                                            <option value="-1">Select Block</option>
                                            
                                       </select>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12 ">
        <div class="card">
            <div class="card-header"> Report</div>
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                        <table class="table table-bordered table-sm " >
                           <thead>
                               <tr>
                                   <th>ID </td>
                                   <th>School Name </th>
                                   <th>District</th>
                                   <th>Block</th>
                                   <th>Mobile Number </th>
                                   <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>   
                                    <td>Vidya Bharati High School</td>
                                    <td>Bokaro</td>
                                    <td>Chas</td>
                                    <td>7854021546</td>
                                    <td>pending</td>
                                </tr>
                                <tr>
                                    <td>2</td>   
                                    <td>Vidya Jyoti High School</td>
                                    <td>Chatra</td>
                                    <td>Itkhori</td>
                                    <td>7854021546</td>
                                    <td>pending</td>
                                </tr>
                                <tr>
                                    <td>3</td>   
                                    <td>Vani Vidya High School</td>
                                    <td>Deoghar</td>
                                    <td>Madhupur</td>
                                    <td>7854021546</td>
                                    <td>pending</td>
                                </tr>
                                <tr>
                                    <td>4</td>   
                                    <td>Gayatri Shikcha Niketan</td>
                                    <td>Dhanbad</td>
                                    <td>Baghmara</td>
                                    <td>7854021546</td>
                                    <td>pending</td>
                                </tr>
                                <tr>
                                    <td>5</td>   
                                    <td>Vidya Bharati High School</td>
                                    <td>Dumka</td>
                                    <td>Jarmundi</td>
                                    <td>7854021546</td>
                                    <td>pending</td>
                                </tr>
                                <tr>
                                    <td>6</td>   
                                    <td>Vidya Bharati High School</td>
                                    <td>East Singhbhum</td>
                                    <td>Ghatshila</td>
                                    <td>7854021546</td>
                                    <td>pending</td>
                                </tr>
                                <tr>
                                    <td>7</td>   
                                    <td>Gayan Jyoti High School</td>
                                    <td>Garhwa</td>
                                    <td>Dhurki</td>
                                    <td>7854021546</td>
                                    <td>Approved</td>
                                </tr>
                                
                                </tr>
                            </tbody>
           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- [ Main Content ] end -->
</div>
</div>
@include('footer')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="division"]').on('change', function() {
         
            var stateID = $(this).val();
           var options ='<option value="-1">Select District </option>';
            if(stateID) {
                $.ajax({
                    url: 'DGR/block/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        console.log('Block ')
                        console.log(data);
                        for(var i = 0; i < data.length; i++) {
    var id = data[i]['id'];
    var value_set_name = data[i]['display_value'];
    //do something with k or data...
    options+= '<option value="'+ id +'">'+ value_set_name +'</option>';
}
$('#district').html(options);
                    }
                });
            }else{
                $('#district').empty();
            }
        });
        $('select[name="district"]').on('change', function() {
            var stateID = $(this).val();
            var options ='<option value="-1">Select Block  </option>';
            if(stateID) {
                $.ajax({
                    url: 'DGR/village/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        console.log('Village ');
                        console.log(data);
                           
                        for(var i = 0; i < data.length; i++) {
    var id = data[i]['id'];
    var value_set_name = data[i]['display_value'];
    //do something with k or data...
    options+= '<option value="'+ id +'">'+ value_set_name +'</option>';
}
$('#block').html(options);
                }
            });        
            }else{
                $('#block').empty();
            }
        });
    });
</script>