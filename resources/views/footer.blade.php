<center>
    <h5 style="color:#ffffff;padding:15px;">Private School Recognition System | Developed By <a href="https://www.itscient.com" target="_blank" style="color:#ffffff;">IT-SCIENT</a></h5>

</center>
<script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
<!-- <script type="text/javascript" src="{{url('assets/js/popper.min.js')}}"></script> -->
<script type="text/javascript" src="{{url('assets/js/vendor-all.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/pcoded.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
<script>
    jQuery(document).ready(function() {
        $('#nav-chat-link').hide();
        $('.img-radius').click(function() {
            $('#exampleModal_image').toggleClass('show');
        });
        $('.close').click(function() {
            $('#exampleModal_image').removeClass('show');
        });
        $(document).on("click", ".browse", function() {
            var file = $(this).parents().find(".file");
            file.trigger("click");
        });
    });
    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#profile_pic_upload").val(fileName);
        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });
    (function($) {
        $.fn.checkFileType = function(options) {
            var defaults = {
                allowedExtensions: [],
                success: function() {},
                error: function() {}
            };
            options = $.extend(defaults, options);
            return this.each(function() {
                $(this).on('change', function() {
                    var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);
                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();
                    }
                });
            });
        };
    })(jQuery);
    $(function() {
        $('#profile-file-value').checkFileType({
            allowedExtensions: ['png', 'jpeg', 'webp', 'jpg'],
            success: function() {
                $('#success').css('display', 'block');
                $('#error').css('display', 'none');
                $('#profile_update').removeAttr('disabled', 'false');
            },
            error: function() {
                $('#error').css('display', 'block');
                $('#success').css('display', 'none');
                $('#profile_update').attr('disabled', 'true');
            }
        });
    });
    //  for changing password Model script and its validation also
    $('#ShowPasswordModal').click(function() {
        $('#ChangePassword').toggleClass('show');
    });
    $('#Close-Modal').click(function() {
        $('#ChangePassword').removeClass('show');
    });
    $('#new_password').on('keyup', function() {
        var password = $('#new_password').val();
        var len = password.length;
        if (len >= 15) {
            alert('password must be special charector / number / alphabates ');
        } else {
            var res;
            console.log('else');
            if (password.match(/[a-z]/g) && password.match(/[A-Z]/g) && password.match(/[0-9]/g) && password.match(/[^a-zA-Z\d]/g) && password.length >= 8) {
                res = "TRUE";
                console.log(res);
                $('#msg_password').text('Strong Password');
                $('#msg_password').css("display", "block");
                $('#msg_password').css("background", "green");
            } else {

                res = "FALSE";
                console.log(res);
                $('#msg_password').text('Weak Password !!!');
                $('#msg_password').css("display", "block");
                $('#msg_password').css("background", "red");
            }
        }
    });
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });
    $('#SubmitPassword').on('click', function() {
        var password = $('#new_password').val();
        if (password.match(/[a-z]/g) && password.match(/[A-Z]/g) && password.match(/[0-9]/g) && password.match(/[^a-zA-Z\d]/g) && password.length >= 8) {
            return true;
        } else {
            alert('Strong Password Required !!!');
            $('#new_password').focus();
            return false;
        }
    });
</script>
</body>

</html>