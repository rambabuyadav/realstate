<!DOCTYPE html>
<html lang="en">
<head>
  <title>Private School Recognition Sysytem</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="" />
  <meta name="keywords" content="">
  <meta name="author" content="" />
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
  <!-- <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
  <!-- <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}"> -->
  <?php
    header('X-Content-Type-Options: nosniff');
  ?>
  <style>
    .goog-logo-link {
      display: none !important;
    }
    .goog-te-gadget {
      color: #4680ff !important;
      margin-top: 0%;
      width: 100%;
      margin-right: 10%;
      z-index: 0 !important;
      height: 20px;
    }
    .goog-te-combo {
      display: block !important;
      margin-right: 10% !important;
      width: 100% !important;
      height: calc(1.5em + .75rem + 2px) !important;
      padding: .375rem .75rem !important;
      font-size: 1rem !important;
      font-weight: 400 !important;
      line-height: 1.5 !important;
      color: #495057 !important;
      background-color: #fff !important;
      background-clip: padding-box !important;
      border: 1px solid #ced4da !important;
      border-radius: .25rem !important;
      transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out
    }
    .goog-te-banner-frame.skiptranslate {
      display: none !important;
    }
    body {
      top: 0px !important;
    }
    
    @media print {
      #google_translate_element {
        display: none;
      }
    }
  </style>
</head>
<body class="">
  <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({ pageLanguage: 'en' }, 'google_translate_element');
    }
  </script>
  <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>