@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Report </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="">All Report Show</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> -->
          <div class="shadow-lg p-3 mt--6 bg-white rounded">
            <div class="card-header pt-0 pb-0">
              Demographic Report
                </div><hr>
                    <div class="row px-3">
                        <div class="col-sm-12 col-lg-3">
                            <label>Advance Search Index </label>
                            <input type="text" class="form-control" placeholder="School/ Application no./ E-mail/ Mobile">
                        </div>
                        <div class="col-sm-12 col-lg-3">
                            <label>Division </label>
                            <select name="" id="" class="form-control">
                                <option value="">Select Division</option>
                                <option value="">Jagarnathpur</option>
                                <option value="">Jalalpur</option>
                                <option value="">Adityanagar</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-lg-3">
                            <label> District </label>
                           <select name="" id="" class="form-control">
                               <option value="" >Select District</option>
                               <option value="">Seraikella-Kharaswan</option>
                               <option value="">Chappra</option>
                               <option value="">Patna</option>
                           </select>
                        </div>
                        <div class="col-sm-12 col-lg-3">
                            <label> Block </label>
                            <select name="" id=""  class="form-control">
                                <option value="">Select Block</option>
                                <option value="">Sonahatu block</option>
                                <option value="">Silli block</option>
                                <option value="">Tamar block</option>
                            </select>
                        </div>
                     
                    </div>
                    <br><br><br><br>
        </div>
     
      </div>
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
@include('footer')