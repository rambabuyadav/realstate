<style>
  .container {
    position: relative;
    text-align: center;
    color: black;
    padding: 25px;
  }
  .bottom-left {
    position: absolute;
    bottom: 19%;
    left: 19%;
  }
  .top-left {
    position: absolute;
    top: 16%;
    left: 12%;
  }
  .top-right {
    position: absolute;
    top: 16%;
    right: 11%;
  }
  .bottom-right {
    position: absolute;
    bottom: 19%;
    right: 19%;
  }
  .centered {
    position: absolute;
    top: 45%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .centered1 {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .std_info {
    position: absolute;
    top: 50%;
    right: 24%;
  }
  ::placeholder {
    color: red;
    opacity: 1;
    /* Firefox */
    font-size: 20px;
  }
  input {
    font-size: 20px;
  }
  input:focus {
    outline: none;
  }
  @media print {
    img {
      width: 595px;
      height: 8px;
    }
    .std_info {
      width: 700px;
      right: 21%;
    }
    .container {
      width: 1123px;
      height: 724px;
    }
  }
  .centerItem {
    display: flex;
    justify-content: center;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 pt-5">
        <div class="container " style="border:3px solid black;padding-bottom:0;">
          <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
          <div class="certifactebox" style="line-height:20px;">
            <img src="{{url('assets/images/Jharkhand emblem.png')}}" alt="" style="height:123px; ">
            <h1 class="centerItem" style="font-style:italic;font-size:49px;margin-bottom:-20px;">Goverment of Jharkhand</h1>
            <!-- <p class="centerItem" style="font-size:15px;line-height:20px;margin-bottom: 0px;">(An Autonomous organisation under the union ministry of Human Resource Development, Jharkhand)</p>
            <p class="centerItem" style="font-size:17px; ">Sector 3, Dhurwa, Ranchi-843004, Jharkhand</p> -->
          </div>
          <div class="centerHeading centerItem">
            <span style="margin:54px 0;font-family:Elephant;font-size:33px;border-top:2px solid red;border-bottom:2px solid red;">Certificate of Recognition</span>
          </div>
          <div class="centerHeading centerItem">
            <span style="margin:54px 0;font-family:Elephant;font-size:33px;border-top:2px solid red;border-bottom:2px solid red;">is being awarded to</span>
          </div>
          <div class="container main-cert-text" style="font-size:24px;line-height:34px;">
            <div class="col-md-12">
              <span style="font-family:cursive;"> &nbsp;</span><span style="border-bottom:1px solid black;font-size:19px;">{{strtoupper($schoolDetail->school_name)}}, {{ucfirst(strtolower($schoolDetail->panchayat))}} {{ucfirst(strtolower($schoolDetail->police_station))}} {{ucfirst(strtolower($schoolDetail->block))}},{{ucfirst(strtolower($schoolDetail->district))}} {{ucfirst(strtolower($schoolDetail->division))}}</span>
               </div>
          </div>
          <div>
            <span style="float:left;text-align:center;font-style:italic;font-weight:700; ">District Superintendent of Education</span><span style="float:right;text-align:center;font-style:italic;font-weight:700; ">District Committee</span>
          </div>
          <br>
          <div>
            <span style="float:left;margin-left:40px;">Saraikela kharaswan</span><span style="float:right;">Saraikela kharaswan</span>
          </div>
          <br>
          <div>
            <span style="float:left;margin-left:40px;"> Date: &nbsp;&nbsp;&nbsp;{{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('j F, Y')}}</span>
          </div>
          <div class="row">
            <div class="col-md-12">
              <center>
                <h6 style="margin:40px 0;border-top:1px solid grey;padding-top:20px ; ">For current status of affiliation of the school, please visit JEPC website : <span>www.jepc.gov.in</span></h6>
              </center>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- [ Main Content ] end -->
  </div>
</div>