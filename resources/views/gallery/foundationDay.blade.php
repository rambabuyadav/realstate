@include('home.header')
<style>
    .img-box {
        border: 1px solid #ddd;
        border-radius: px;
        box-shadow: 3px 4px 8px grey;
        margin-bottom: 15px;
    }

    .img-box a img {
        height: 200px;
        width: 100%;
    }

    .g-box {
        padding: 30px 0;
    }

    .gallery-subbox {
        padding: 40px;
    }
</style>

<section>
    <div class="g-box">
        <h1 class="text-center" style="font-size:65px;color:grey;">Foundation Day</h1>
        <div class="container">
            <br><br><br>

            <div class="galley-subbox">
                <div class="row">
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery1.webp" class="lightbox">
                                <img id="myImg" src="../assets/images/gallery1.webp" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery2.jpeg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery3.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery4.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery5.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery6.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery7.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery8.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery5.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery6.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery7.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="">
                                <img src="../assets/images/gallery8.jpg" alt="">
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
@include('home.footer')