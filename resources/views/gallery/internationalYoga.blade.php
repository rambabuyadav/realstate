@include('home.header')

<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
<script src="https://jsdelivr.com/projects/baguettebox.js"></script>


<style>
    .img-box {
        border: 1px solid #ddd;
        border-radius: px;
        box-shadow: 3px 4px 8px grey;
        margin-bottom: 15px;
    }

    .img-box a img {
        height: 200px;
        width: 100%;
    }

    .g-box {
        padding: 30px 0;
    }

    .gallery-subbox {
        padding: 40px;
    }
</style>


<section>
    <div class="g-box">
        <h1 class="text-center" style="font-size:65px;color:grey;">International Yoga Day</h1>
        <div class="container">
            <div class="gallery-subbox">
                <div class="row">
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery1.webp" class="lightbox">
                                <img  src="../assets/images/gallery1.webp" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery2.jpeg" class="lightbox">
                                <img src="../assets/images/gallery2.jpeg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery3.jpg" class="lightbox">
                                <img src="../assets/images/gallery3.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery4.jpg" class="lightbox">
                                <img src="../assets/images/gallery4.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery5.jpg" class="lightbox">
                                <img src="../assets/images/gallery5.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery6.jpg" class="lightbox">
                                <img src="../assets/images/gallery6.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery7.jpg" class="lightbox">
                                <img src="../assets/images/gallery7.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery8.jpg" class="lightbox">
                                <img src="../assets/images/gallery8.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery5.jpg" class="lightbox">
                                <img src="../assets/images/gallery5.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery6.jpg" class="lightbox">
                                <img src="../assets/images/gallery6.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery7.jpg" class="lightbox">
                                <img src="../assets/images/gallery7.jpg" alt="">

                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="img-box">
                            <a href="../assets/images/gallery8.jpg" class="lightbox">
                                <img src="../assets/images/gallery8.jpg" alt="">
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<br><br><br><br><br><br><br>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.gallery-subbox');
</script>

@include('home.footer')