@include('header')
@include('topbar')
@include('sidenav')
@php($i = 0)
<style>
  .w-5 {
    display: inline;
    height: 30px;
  }
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">
                <div class="btn-group">
                  <form method="post" action="{{url('payment-list')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="payment_amount" value="12500">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Class Group 1-5</button>
                  </form>
                  <form method="post" action="{{url('payment-list')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="payment_amount" value="25000">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Class Group 1-8</button>
                  </form>
                </div>
              </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="#!"> Payment Details</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <form action="{{url('payment-list')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" class='text-block' name="search_text" value="{{@$adv_data['search_text']}}" />
                <input type="hidden" class='text-block' name="Block" value="{{$adv_data['block_id']}}" />
                <input type="hidden" class='text-block' name="schoolList" value="{{$adv_data['school']}}" />
                <input type="hidden" class='text-block' name="paymentType" value="{{@$adv_data['paymentType']}}" />
                <input type="hidden" class='text-block' name="contact_number" value="{{@$adv_data['contactNumber']}}" />
                <input type="hidden" class='text-block' name="excel" value="excel" />
                <button type="submit" name="submit" class="btn btn-sm btn-light btn-offset pull-right" title="Download payments &#10; in Export">
                <i class="fa fa-file-excel text-success"> </i></button>
                <button type="button" class="btn btn-sm btn-light btn-offset pull-right" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
                <a href="{{url('payment-list')}}"> <button type="button" class="btn btn-danger btn-sm pull-right" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button></a>
              </form>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <!-- <h1>Here is all Payment Related Details</h1> -->
        @include('message-flash')
        <div class="shadow-lg p-3 mt--6 bg-white rounded" style="height: 760px;">
          <table class="table table-bordered table-responsive table-sm" id="example">
            <thead>
              <tr style="width: 100%;">
                <th style="width: 10%;">Sl No.</th>
                <th style="width: 15%;">School Name</th>
                <th style="width: 10%;">School No</th>
                <th style="width: 15%;">School Address</th>
                <th style="width: 15%;">Contact Person</th>
                <th style="width: 15%;">Contact Person No</th>
                <th style="width: 15%;">Transaction id</th>
                <th style="width: 15%;">Payment Type</th>
                <th style="width: 10%;">Payment Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach(@$payments as $key => $payment)
              <tr>
                <td>{{++$i}}</td>
                <td>{{$payment->school_name}}</td>
                <td>{{$payment->phone_no}}</td>
                <td title="{{$payment->village_city}}&#10; {{$payment->post_office}} {{$payment->panchayat}} &#10;;{{$payment->block}} &#10;{{$payment->district}} ">
                  @if($payment->block){{$payment->block}},&nbsp;
                  @endif
                  @if($payment->district){{$payment->district}}
                  @endif
                </td>
                <td>{{$payment->contact_name}}</td>
                <td>{{$payment->mobile_no}}</td>
                <td>{{$payment->t_id}}</td>
                <td>{{$payment->paid_method}}</td>
                <td>{{$payment->payment_amount}}</td>
                <input type="hidden" class='text-block' id="school_name" value="{{$payment->school_name}}" />
                <input type="hidden" class='text-block' id="created_by_name" value="{{$payment->created_by_name}}" />
                <input type="hidden" class='text-block' id="board_type" value="{{$payment->board_type}}" />
                <input type="hidden" class='text-block' id="school_type" value="{{$payment->school_type}}" />
                <input type="hidden" class='text-block' id="school_email" value="{{$payment->school_email}}" />
                <input type="hidden" class='text-block' id="school_website" value="{{$payment->school_website}}" />
                <input type="hidden" class='text-block' id="village_city" value="{{$payment->village_city}}" />
                <input type="hidden" class='text-block' id="post_office" value="{{$payment->post_office}}" />
                <input type="hidden" class='text-block' id="panchayat" value="{{$payment->panchayat}}" />
                <input type="hidden" class='text-block' id="block" value="{{$payment->block}}" />
                <input type="hidden" class='text-block' id="district" value="{{$payment->district}}" />
                <input type="hidden" class='text-block' id="division" value="{{$payment->division}}" />
                <input type="hidden" class='text-block' id="contact_name" value="{{$payment->contact_name}}" />
                <input type="hidden" class='text-block' id="mobile_no" value="{{$payment->mobile_no}}" />
                <input type="hidden" class='text-block' id="phone_no" value="{{$payment->phone_no}}" />
                <input type="hidden" class='text-block' id="t_id" value="{{$payment->t_id}}" />
                <input type="hidden" class='text-block' id="paid_method" value="{{$payment->paid_method}}" />
                <input type="hidden" class='text-block' id="payment_amount" value="{{$payment->payment_amount}}" />
                <input type="hidden" class='text-block' id="payment_title" value="{{$payment->payment_title}}" />
                <input type="hidden" class='text-block' id="payment_description" value="{{$payment->payment_description}}" />
                <input type="hidden" class='text-block' id="payment_user" value="{{$payment->payment_user}}" />
                <input type="hidden" class='text-block' id="payment_rows" value="{{$payment->payment_rows}}" />
                <input type="hidden" class='text-block' id="payment_status" value="{{$payment->payment_status}}" />
                <input type="hidden" class='text-block' id="payment_date" value="{{\Carbon\Carbon::parse ($payment->payment_date)->format('d/m/Y')}}" />
                <input type="hidden" class='text-block' id="created_by" value="{{$payment->created_by}}" />
                <input type="hidden" class='text-block' id="created_at" value="{{$payment->created_at}}" />
                <!-- <td>
                  <button class="btn btn-info viewData btn-sm " data-toggle="modal" data-target=".bd-example-modal-lg" title="View Payment Detail &#10; of {{$payment->contact_name}}"><i class="fa fa-eye " title="View Payment Detail &#10; of {{$payment->contact_name}}"></i></button>
                </td> -->
              </tr>
              @endforeach
              @if($total_amount == 0 || $total_amount == '0')
              <tr>
                <th colspan="10" style="text-align: center;"><span>No payments Found!!</span></th>
              </tr>
              @else
              <tr>
                <th colspan="8"><span style="float: right;"> Total</span></th>
                <th>{{$total_amount}}</th>
              </tr>
              @endif
            </tbody>
          </table>
          {!! $payments->links() !!}
          <!-- <p class="lead"><button id="json" class="btn btn-primary">TO JSON</button> <button id="csv" class="btn btn-info">TO CSV</button>  <button id="pdf" class="btn btn-danger">TO PDF</button></p> -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- %%%%%%%%%%%%%%%%%%%%%%%%%%% Payment Model view start %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title" id="exampleModalLabel">Payment Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <form> -->
        <table class="table table-bordered table-condensed table-sm col-md-12">
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b> School Name :</b></div>
            <div for="message-text" class=" border p-1 col-9" id="School_name"> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>School Type:</b></div>
            <div for="message-text" class=" border p-1 col-3" id="School_type"> </div>
            <div for="message-text" class=" border p-1 col-3"><b>School Board :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Board_type"> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>School Website :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="School_website"> </div>
            <div for="message-text" class=" border p-1 col-3"><b>School Email :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="School_email"> </div>
          </div>
          <!-- <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>School Address :</b></div>
            <div for="message-text" class=" border p-1 col-9">
              <pre id="appeal_school_address"> </pre>
            </div>
          </div> -->
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b> Division :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Division_show"> </div>
            <div for="message-text" class=" border p-1 col-3"><b> District :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="District"> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b> Post Office :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Post_office"> </div>
            <div for="message-text" class=" border p-1 col-3"><b> Block :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Block"> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>Panchayat :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Panchayat"> </div>
            <div for="message-text" class=" border p-1 col-3"><b> Village/City :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Village_city"> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>Contact Person :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Contact_name"> </div>
            <div for="message-text" class=" border p-1 col-3"><b>Mobile No. :</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Mobile_no"><span id="Mobile_no"></span> <span id="Phone_no"></span> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>payment Title:</b></div>
            <div for="message-text" class=" border p-1 col-9" id="Payment_title"> </div>
          </div>
          <div class="row">
            <div for="message-text" class=" border p-1 col-3"><b>Description:</b></div>
            <div for="message-text" class=" border p-1 col-3" id="Payment_description"> </div>
            <div for="message-text" class=" border p-1 col-3"><b> Amount: </b></div>
            <div for="message-text" class=" border p-1 col-3" id="Payment_amount"></div>
          </div>
      </div>
      <div class="row">
        <div for="message-text" class="border p-1 col-3"><b>Date:</b></div>
        <div for="message-text" class="border p-1 col-3" id="Payment_date"> </div>
        <div for="message-text" class="border p-1 col-3"><b>Payment method:</b></div>
        <div for="message-text" class="border p-1 col-3" id="Paid_method"> </div>
      </div>
        <!-- <div class="row">
            <div for="message-text" class="border p-1 col-3"><b> Remarks Date :</b></div>
            <div for="message-text" class="border p-1 col-3" id="remarks_date"> Text:</div>
            <div for="message-text" class="border p-1 col-3"><b> Remarks Created By :</b></div>
            <div for="message-text" class="border p-1 col-3" id="remarks_by"> Text:</div>
          </div> -->
      <!-- <tr>
    <th for="message-text" class="col-form-label">Appeal Status:</th>
    <td for="message-text" class="col-form-label" id="appeal_status"> </td>
    
  </div>   -->
      </table>
      <!-- </form> -->
    </div>
    <!-- <div class="modal-footer">
<button type="button" class="btn btn-success" data-dismiss="modal">Submit</button>
<button type="button" class="btn btn-primary">Send message</button>
</div> -->
  </div>
</div>
</div>
<!-- %%%%%%%%%%%%%%%%%%% End view model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
<!-- [ Adv Search Model ] -->
<div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form action="{{url('payment-list')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="search_text">Advance Search</label>
                @if(isset($adv_data['search_text']))
                <input type="text" name="search_text" value="{{@$adv_data['search_text']}}" class="form-control rte_input" id="search_text" placeholder="School name/ E-mail / Address/ Transaction id ">
                @else
                <input type="text" name="search_text" id="search_text" class="form-control rte_input" placeholder="School name/ E-mail/ Address/ Transaction id">
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="Block"> Block </label>
                <select class="form-control rte_input" name="Block" id="Block">
                  @isset($adv_data['block_id'])
                  <option value="{{$adv_data['block_id']}}">{{$adv_data['blockName']}}</option>
                  @endisset
                  <option value="">---Select--</option>
                  @foreach(@$block_list as $key => $value)
                  <option value="{{$key}}">{{$value}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="schoolList"> School Name </label>
                <select class="form-control rte_input" name="schoolList" id="schoolList">
                  @isset($adv_data['school'])
                  <option value="{{$adv_data['school']}}">{{$adv_data['schoolName']}}</option>
                  @endisset
                  <option value="">---Select--</option>
                  @foreach(@$school_list as $key => $value)
                  <option value="{{$key}}">{{$value}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="paymentType"> Payment Type </label>
                <select class="form-control rte_input" name="paymentType" id="paymentType">
                  @isset($adv_data['paymentType'])
                  <option value="{{@$adv_data['paymentType']}}">{{@$adv_data['paymentType']}}</option>
                  @endisset
                  <option value="">---Select--- </option>
                  <option value="Card"> Card </option>
                  <option value=" Net Banking"> Net Banking </option>
                  <option value="NEFT"> NEFT </option>
                  <option value="UPI"> UPI </option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="contact_number"> Contact No. </label>
                @if(isset($adv_data['contactNumber']))
                <input type="text" class="form-control rte_input" value="{{@$adv_data['contactNumber']}}" name="contact_number" id="contact_number">
                @else
                <input type="text" class="form-control rte_input" name="contact_number" id="contact_number">
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <input type="reset" name="Reset" value="Reset" id="Reset" class="btn btn-primary"> -->
          <input type="submit" name="submit" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- [ Adv Search End ] -->
@include('footer')
<script>
  $('#json').on('click', function () {
    $("#example").tableHTMLExport({ type: 'json', filename: 'sample.json' });
  })
  $('#csv').on('click', function () {
    $("#example").tableHTMLExport({ type: 'csv', filename: 'sample.csv' });
  })
  $('#pdf').on('click', function () {
    $("#example").tableHTMLExport({ type: 'pdf', filename: 'sample.pdf' });
  })
  // %%%%%%%%%%%%%%%%%%% View payment Detail %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  $(".viewData").click(function () {
    var schoolName = $(this).closest("tr").find("#school_name").val();
    var createdBy = $(this).closest("tr").find("#created_by_name").val();
    var schoolBoad = $(this).closest("tr").find("#board_type").text();
    var schoolType = $(this).closest("tr").find("#school_type").text();
    var schoolEmail = $(this).closest("tr").find("#school_email").text();
    var schoolWebsite = $(this).closest("tr").find("#school_website").text();
    var schoolCity = $(this).closest("tr").find("#village_city").text();
    var postOffice = $(this).closest("tr").find("#post_office").text();
    var panchayat = $(this).closest("tr").find("#panchayat").text();
    var block = $(this).closest("tr").find("#block").val();
    var district = $(this).closest("tr").find("#district").val();
    var Division = $(this).closest("tr").find("#division").val();
    var contactName = $(this).closest("tr").find("#contact_name").val();
    var mobileNo = $(this).closest("tr").find("#mobile_no").val();
    var phoneNo = $(this).closest("tr").find("#phone_no").val();
    var tId = $(this).closest("tr").find("#t_id").val();
    var paidMethod = $(this).closest("tr").find("#paid_method").val();
    var paymentAmount = $(this).closest("tr").find("#payment_amount").val();
    var paymentTitle = $(this).closest("tr").find("#payment_title").val();
    var paymentDescription = $(this).closest("tr").find("#payment_description").val();
    var paymentUser = $(this).closest("tr").find("#payment_user").val();
    var paymentRows = $(this).closest("tr").find("#payment_rows").val();
    var paymentStatus = $(this).closest("tr").find("#payment_status").val();
    var paymentDate = $(this).closest("tr").find("#payment_date").val();
    // var created_by = $(this).closest("tr").find("#created_by").val();
    var createdAt = $(this).closest("tr").find("#created_at").val();
    var paymentDate = $(this).closest("tr").find("#payment_date").val();
    $('#School_name').text(schoolName);
    // $('#SchoolID').text(createdBy);
    $('#Board_type').text(schoolBoad);
    $('#School_type').text(schoolType);
    $('#School_email').text(schoolEmail);
    $('#School_website').text(schoolWebsite);
    $('#Village_city').text(schoolCity);
    $('#Post_office').text(postOffice);
    $('#Panchayat').text(panchayat);
    $('#Block').text(block);
    $('#District').text(district);
    $('#Division_show').text(Division);
    $('#Contact_name').text(contactName);
    $('#Mobile_no').text(mobileNo);
    $('#Phone_no').text(phoneNo);
    $('#Payment_title').text(paymentTitle);
    $('#Payment_amount').text(paymentAmount);
    $('#Payment_description').text(paymentDescription);
    $('#Payment_date').text(paymentDate);
    $('#Paid_method').text(paidMethod);
    $('#Created_by_name').text(createdBy);
    // $('#Remarks_status').text(appealCluster);
    // $('#appeal_school_district').text(appealDistrict);
    // $('#remarks').text(appealRemarks);
    // $('#remarks_by').text(appealRemarksBy);
    // $('#remarks_date').text(appealRemarksDate);
    // $('#remarks_status').text(appealRemarksStatus);
  });
</script>