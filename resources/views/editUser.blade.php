@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">User</h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-12">
                    <form action="{{url('editUser')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="id" value="{{$user->user_id}}">
                        <!-- school details -->
                        <div class="card">
                            <div class="card-header">User Details</div>
                            <div class="card-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <!-- <label for="">Photo</label> -->
                                                <img src="../{{$user->profile_photo_path}}" id="profile-img-tag" width="200px" style="width:100px; height:100px; border-radius: 50%;" />
                                                <!-- <input type="file" class="form-control rte_input" value="{{$user->profile_photo_path}}" name="photo"> -->
                                                <input type="hidden" class="form-control rte_input" value="{{$user->profile_photo_path}}" name="photo_path" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Full Name </label>
                                                <input required type="text" name="full_name" value="{{$user->full_name}}" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">User Name </label>
                                                <input required type="text" name="user_name" value="{{$user->user_name}}" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">User Role</label>
                                                <select class="form-control rte_input" name="usertype" id="UserType" readonly>
                                                    <option selected value="{{$user->user_role}}">{{$user->user_role}} </option>
                                                    <!-- <option value="dc">DC</option>
                                                    <option value="dse">DSE</option>
                                                    <option value="faa">FAA</option>
                                                    <option value="saa">SAA</option>
                                                    <option value="state">State</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">E-Mail ID</label>
                                                <input required type="text" name="email" class="form-control rte_input" value="{{$user->email}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for=""> Password</label>
                                                <input type="password" name="password" class="form-control rte_input" value="" placeholder="Example Test@1234">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Contact</label>
                                                <input required type="text" class="form-control rte_input" value="{{$user->mobile}}" name="phonenumber1">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Address 1</label>
                                                <textarea col="3" type="text" class="form-control rte_input" name="address1" required>{{$user->school_address}} </textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="DivisionHide">
                                            <div class="form-group">
                                                <label for=""> Division </label>
                                                <select class="form-control rte_input" name="division" id="division" required>
                                                    <!-- <option value="-1">Select Division </option> -->
                                                    @foreach($Districts as $District)
                                                    @if($District->id==$user->division)
                                                    <option selected value="{{$District->id}}">{{$District->value_set_name }} </option>
                                                    @else
                                                    <option value="{{$District->id}}">{{$District->value_set_name }} </option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="DistrictHide">
                                            <div class="form-group">
                                                <label for=""> District </label>
                                                <select class="form-control rte_input" name="district" required id="district">
                                                    @if($user->user_role=='dse' || $user->user_role=='dc')
                                                    <option value="{{$DistrictName[0]['id']}}">{{$DistrictName[0]['display_value']}}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-4"></div>
                                        <div class="col-md-3 col-4">
                                            <input class="btn btn-primary btn_submit" type="submit" name="submit" value="UPDATE" style="float:right" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- [ Main Content ] end -->
            </div>
        </div>
    </div>
</div>
@include('footer')
<script>
    $(document).ready(function () {
        var UserType = $('#UserType').val();
        console.log(UserType);
        if (UserType == 'dse' || UserType == 'dc') {
            $('#DivisionHide').show();
            $('#DistrictHide').show();
            document.getElementById("division").required = true;
            document.getElementById("district").required = true;
        }
        else if (UserType == 'saa' || UserType == 'state') {
            $('#DivisionHide').hide();
            $('#DistrictHide').hide();
            document.getElementById("division").required = false;
            document.getElementById("district").required = false;
        }
        else if (UserType == 'faa') {
            $('#DivisionHide').show();
            $('#DistrictHide').hide();
            document.getElementById("division").required = true;
            document.getElementById("district").required = false;
        }
        $('select[name="division"]').on('change', function () {
            var stateID = $(this).val();
            var options = '<option value="">---Select---</option>';
            if (stateID) {
                $.ajax({
                    url: '../district/district_list/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('district ')
                        console.log(data);
                        Object.entries(data).forEach(entry => {
                            const [key, value] = entry;
                            options += '<option value="' + key + '">' + value + '</option>';
                            console.log(key, value);
                        });
                        $('#district').html(options);
                    }
                });
            } else {
                $('#district').empty();
            }
        });
        $('#UserType').on('change', function () {
            var UserType = $(this).val();
            console.log(UserType);
            if (UserType == 'dse' || UserType == 'dc') {
                $('#DivisionHide').show();
                $('#DistrictHide').show();
                document.getElementById("division").required = true;
                document.getElementById("district").required = true;
            }
            else if (UserType == 'saa' || UserType == 'state') {
                $('#DivisionHide').hide();
                $('#DistrictHide').hide();
                document.getElementById("division").required = false;
                document.getElementById("district").required = false;
            }
            else if (UserType == 'faa') {
                $('#DivisionHide').show();
                $('#DistrictHide').hide();
                document.getElementById("division").required = true;
                document.getElementById("district").required = false;
            }
        });
    });
</script>