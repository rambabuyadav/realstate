@include('header')
@include('sidenav')
@include('topbar')
<style>
  .w-5 {
    display: inline;
    height: 30px;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">
                <div class="btn-group">
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="application_status" value="0">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">All</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    @if(Auth::user()->user_role=='dse')
                    <input type="hidden" class='text-block' name="application_status" value="2">
                    @elseif(Auth::user()->user_role=='dc')
                    <input type="hidden" class='text-block' name="application_status" value="4">
                    @endif
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Pending</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    @if(Auth::user()->user_role=='dse')
                    <input type="hidden" class='text-block' name="application_status" value="4">
                    @elseif(Auth::user()->user_role=='dc')
                    <input type="hidden" class='text-block' name="application_status" value="6">
                    @endif
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Approved</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    @if(Auth::user()->user_role=='dse')
                    <input type="hidden" class='text-block' name="application_status" value="8">
                    @elseif(Auth::user()->user_role=='dc')
                    <input type="hidden" class='text-block' name="application_status" value="7">
                    @endif
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Rejected</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="application_status" value="19">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Delayed At DC (Meeting)</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="application_status" value="20">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Delayed At DC (Final Decision)</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="application_status" value="18">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Delayed At DSE</button>
                  </form>
                  <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="application_status" value="27">
                    <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Payment Not Verified </button>
                  </form>
                </div>
              </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">Total Applied Schools</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <form method="post" action="{{url('application-verify')}}" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" class='text-block' name="search_text" value="{{@$adv_data['search_text']}}" />
              <input type="hidden" class='text-block' name="appeal_status" value="{{@$adv_data['application_status']}}" />
              <input type="hidden" class='text-block' name="Block" value="{{@$adv_data['block_id']}}" />
              <input type="hidden" class='text-block' name="fromDate" value="{{@$adv_data['fromDate']}}" />
              <input type="hidden" class='text-block' name="toDate" value="{{@$adv_data['toDate']}}" />
              <input type="hidden" class='text-block' name="excel" value="excel" />
              <button type="submit" class="btn btn-sm btn-light btn-offset"><a class="text-success" title="Export "> <i class="fa fa-file-excel text-success"> </i> </a></button>
              <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
              <a href="{{url('application-verify')}}"> <button type="button" class="btn btn-danger btn-sm pull-right" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button></a>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <!-- <h1>Here is all Payment Related Details</h1> -->
        <div class="shadow-lg p-3 mt--6 bg-white rounded ">
          @if(Session::has('feedback'))
          <div class="alert alert-success">
            {{ Session::get('feedback') }}
          </div>
          @endif
          <table class="table table-bordered table-sm">
            <thead>
              <tr style="width: 100%;">
                <th style="width: 11%;">School Name</th>
                <th style="width: 11%;">Medium</th>
                <th style="width: 11%;">School Type</th>
                <th style="width: 11%;">District</th>
                <th style="width: 11%;">Post Office</th>
                <th style="width: 11%;">PIN-Code</th>
                <th style="width: 11%;">Phone</th>
                <th style="width: 11%;">Email ID</th>
                <th style="width: 11%;">School Chairman</th>
                <th style="width: 11%;" title="Remaining Days">End Date</th>
                @if(Auth::user()->user_role=='dc' || Auth::user()->user_role=='dse' )
                <th style="width: 11%;"> Status</th>
                <th style="width: 11%;">Operation</th>
                @endif
                <!-- <th style="width: 11%;">Chairman Phone No</th> -->
              </tr>
            </thead>
            <tbody>
              @php
              $i=0;
              @endphp
              @foreach($AllData as $key => $data)
              <tr>
                @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' )
                <td>
                  @php
                  $id = Crypt::encrypt(@$data->applicationdataid);
                  @endphp
                  <a href="show-data/{{$id}}">{{@$data->school_name}}</a>
                </td>
                @else
                <td>{{@$data->school_name}} </td>
                @endif
                <!-- <td>{{Auth::user()->name}}</td> -->
                <td class="application_id" style="display: none;">{{@$data->applicationdataid}}</td>
                <td class="school_id" style="display: none;">{{@$data->school_id}}</td>
                <td>{{@$data->medium}}</td>
                <td>{{@$data->from_class}}-{{@$data->upto_class}}</td>
                <td>{{@$data->disrict}}</td>
                <td>{{@$data->post_office}}</td>
                <td>{{@$data->pincode}}</td>
                <td>{{@$data->phone_with_std_code}}</td>
                <td>{{@$data->email}}</td>
                <td>{{@$data->school_chairman_name}}</td>
                @if(Auth::user()->user_role=='dse')
                <td>

                  @if($data->application_status == 1 || $data->application_status == 2 || $data->application_status == 3 || $data->application_status == 4 || $data->application_status == 5 || $data->application_status == 9 || $data->application_status == 12)
                  @if($data->application_status == 2 || $data->application_status == 3)
                  {{\Carbon\Carbon::parse ($last_update)->addDays($days_to_varified_by_dse)->format('d/m/Y')}}
                  @elseif($data->application_status == 4)
                  {{\Carbon\Carbon::parse ($last_update)->addDays($days_to_varified_by_dc)->format('d/m/Y')}}
                  @elseif($data->application_status == 5)
                  {{\Carbon\Carbon::parse($last_update)->addDays($days_to_varified_by_dc_after_meeting)->format('d/m/Y')}}
                  @elseif($data->application_status == 9)
                  {{\Carbon\Carbon::parse($last_update)->addDays($faa_decision_limit)->format('d/m/Y')}}
                  @elseif($data->application_status == 12)
                  {{\Carbon\Carbon::parse($last_update)->addDays($saa_decision_limit)->format('d/m/Y')}}
                  @endif

                  @if( $Count[$i] > 5 )
                  <span class="text-success"> ( {{ $Count[$i] }} days ) </span>
                  @elseif($Count[$i] < 5 && $Count[$i]>0)
                    <span class="text-danger"> ( {{ $Count[$i] }} days ) </span>
                    @else
                    <span class="text-warning"> ( End )</span>
                    @endif
                    @else
                    ----
                    @endif
                </td>
                @endif
                @if(Auth::user()->user_role=='dc')

                <td style="display: none;" class="Meeting_Date">
                  @php
                  if(DB::table('application_trackings')->where('application_id', $data->applicationdataid)->where('application_status_id',4)->exists()){
                  $date_meeting = DB::table('application_trackings')->where('application_id', $data->applicationdataid)->where('application_status_id',4)->pluck('created_at')->first();

                  $Meeting_date = date('Y-m-d', strtotime($date_meeting));
                  echo $Meeting_date;
                  }
                  @endphp
                </td>
                <td>


                  @if($data->application_status == 2 || $data->application_status == 3 || $data->application_status == 4 || $data->application_status == 5 || $data->application_status == 9 || $data->application_status == 12)
                  @if($data->application_status == 2 || $data->application_status == 3)
                  {{\Carbon\Carbon::parse ($last_update)->addDays($days_to_varified_by_dse)->format('d/m/Y')}}
                  @elseif($data->application_status == 4)
                  {{\Carbon\Carbon::parse ($last_update)->addDays($days_to_varified_by_dc)->format('d/m/Y')}}
                  @elseif($data->application_status == 5)
                  {{\Carbon\Carbon::parse($last_update)->addDays($days_to_varified_by_dc_after_meeting)->format('d/m/Y')}}
                  @elseif($data->application_status == 9)
                  {{\Carbon\Carbon::parse($last_update)->addDays($faa_decision_limit)->format('d/m/Y')}}
                  @elseif($data->application_status == 12)
                  {{\Carbon\Carbon::parse($last_update)->addDays($saa_decision_limit)->format('d/m/Y')}}
                  @endif
                  @if( $Count[$i] > 5 )
                  <span class="text-success"> ( {{ $Count[$i] }} days ) </span>
                  @elseif($Count[$i] < 5 && $Count[$i]>0)
                    <span class="text-danger"> ( {{ $Count[$i] }} days ) </span>
                    @else
                    <span class="text-warning"> Delayed </span>
                    @endif
                    @else
                    ----
                    @endif
                </td>
                @endif
                @if(Auth::user()->user_role=='dc' )
                <td>
                  @if(@$data->application_status==6)
                  <span class="text-success">Certified</span>
                  @elseif(@$data->application_status==7)
                  <span class="text-warning">Rejected</span>
                  @else
                  <span class="text-warning">{{@$data->status_name}}</span>
                  @endif
                </td>
                <td style="text-align: center;">
                  <!-- <button class="btn btn-warning btn-sm"><i class="fa fa-stopwatch"></i></button> -->
                  @if(isset($data->message_recode) && count(@$data->message_recode) > 0)
                  <div style="display: none;" class=" message_History">
                    <!-- $Appeal['appeal_messages'][$i] -->
                    <div>
                      @foreach (@$data->message_recode as $message => $message_list)
                      <label for="message-text" class=""><b>Date:</b> {{$message_list->created_at}}</label><br>
                      <label for="message-text" class=""><b>From:</b> {{$message_list->Sender_name}}</label><br>
                      <label for="message-text" class=""><b>To:</b> {{$message_list->Reciever_name}} </label><br>
                      <label for="message-text" class=""><b>Remarks : </b> {{$message_list->remarks}}</label><br>
                      <label for="message-text" class="">
                        <!-- {{ $message_list }} -->
                        @if(isset($message_list->attachments) && $message_list->attachments != null )
                        @if( count($message_list->attachments) > 0)
                        <table class="Uploaded_document_link">
                          <tr class="">
                            <td>
                              @foreach ($message_list->attachments as $message_image_key => $message_image_list)
                              @if($message_image_key > 0)
                              ,&nbsp;
                              @endif
                              @if($message_list->sender_role == 'dc' || $message_list->sender_role == 'dse')
                              <a href="https://rte.jharkhand.gov.in/public/assets/send_dse_to_school__correction/{{$message_image_list}}"><span>{{$message_image_list}}</span></a>
                              @elseif($message_list->sender_role == 'school')
                              <a href="https://rte.jharkhand.gov.in/public/assets/send_school_to_dc_dse_correction/{{$message_image_list}}"><span>{{$message_image_list}}</span></a>
                              @endif
                              @endforeach
                            </td>
                          </tr>
                        </table>
                        @endif
                        @endif
                      </label>
                      <hr>
                      @endforeach
                    </div>
                  </div>
                  @endif


                  @if(@$data->application_status==4 )
                  <button class="btn btn-warning CreateMeeting btn-sm " data-toggle="modal" title="Schedule Meeting" data-target="#Meeting"><i class="fa fa-stopwatch"></i></button>
                  @endif
                  @if(@$data->application_status == 5 || @$data->application_status == 4 )
                  @php
                  if(DB::table('application_meetings')->where('application_id', $data->applicationdataid)->exists()){
                  $date_meeting = DB::table('application_meetings')->where('application_id', $data->applicationdataid)->pluck('meeting_date')->first();
                  $date = strtotime($date_meeting);
                  $Meeting_date = date('Y-m-d', $date);
                  $today = date('Y-m-d');
                  if($Meeting_date < $today){ echo '   <button class="btn btn-warning CreateMeeting btn-sm " data-toggle="modal" title="Schedule Meeting" data-target="#Meeting"><i class="fa fa-stopwatch"></i></button>' ; }else{ echo '<button class="btn btn-success CreateAccept btn-sm " data-toggle="modal" title="Approve Application" data-target="#Accept"><i class="fa fa-check"></i></button>
								<button class="btn btn-danger CreateReject btn-sm " data-toggle="modal" title="Reject Application" data-target="#Reject"><i class="fa fa-times"></i></button> ' ; } } @endphp <button class="btn btn-danger send_message btn-sm" type="button" data-toggle="modal" data-target=".bd-message-school-modal-lg" title="Send Message to School"><i class="fa fa-reply" aria-hidden="true"></i></button>
                    <button class="btn btn-primary message_history_to_school btn-sm" type="button" data-toggle="modal" data-target=".bd-message-history-school-modal-lg" title="Message History">
                      <i class="fa fa-envelope" aria-hidden="true">

                      </i>
                      <sup>
                        @if(@$data->application_conversation_unread_count>0)
                        {{@$data->application_conversation_unread_count}}
                        @endif
                      </sup>
                    </button>
                    @endif
                </td>
                @endif
                @if(Auth::user()->user_role=='dse' )
                <td>
                  {{@$data->status_name}}

                </td>
                <td style="text-align: center;">
                  @if(isset($data->message_recode) && count(@$data->message_recode) > 0)
                  <div style="display: none;" class=" message_History">
                    <!-- $Appeal['appeal_messages'][$i] -->
                    <div>
                      @foreach (@$data->message_recode as $message => $message_list)
                      <label for="message-text" class=""><b>Date:</b> {{$message_list->created_at}}</label><br>
                      <label for="message-text" class=""><b>From:</b> {{$message_list->Sender_name}}</label><br>
                      <label for="message-text" class=""><b>To:</b> {{$message_list->Reciever_name}} </label><br>
                      <label for="message-text" class=""><b>Remarks : </b> {{$message_list->remarks}}</label><br>
                      <label for="message-text" class="">
                        <!-- {{ $message_list }} -->
                        @if(isset($message_list->attachments) && $message_list->attachments != null )
                        @if( count($message_list->attachments) > 0)
                        <table class="Uploaded_document_link">
                          <tr class="">
                            <td>
                              @foreach ($message_list->attachments as $message_image_key => $message_image_list)
                              @if($message_image_key > 0)
                              ,&nbsp;
                              @endif
                              <a href="http://localhost/rte-application/public/accept_appeal/{{$message_image_list}}"><span>{{$message_image_list}}</span></a>
                              @endforeach
                            </td>
                          </tr>
                        </table>
                        @endif
                        @endif
                      </label>
                      <hr>
                      @endforeach
                    </div>
                  </div>
                  @endif

                  @if(@$data->application_status < 4 ) <button class="btn btn-success btn-sm" type="button" data-toggle="modal" onclick="feedback_to_school('{{@$data->applicationdataid}}')" data-target=".bd-feedback-school-modal-lg" title="Set Verification Date"><i class="fa fa-check-circle" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-danger send_message btn-sm" type="button" data-toggle="modal" onclick="message_to_school('{{@$data->applicationdataid}}')" data-target=".bd-message-school-modal-lg" title="Send Message to School"><i class="fa fa-reply" aria-hidden="true"></i></button>
                    <button class="btn btn-primary message_history_to_school btn-sm" type="button" data-toggle="modal" onclick="message_history_to_school('{{@$data->applicationdataid}}')" data-target=".bd-message-history-school-modal-lg" title="Message History"><i class="fa fa-envelope" aria-hidden="true">
                      </i>
                      <sup>
                        @if(@$data->application_conversation_unread_count>0)
                        {{@$data->application_conversation_unread_count}}
                        @endif
                      </sup>
                    </button>
                    @elseif(@$data->application_status ==27 )

                    @php
                    $grn = DB::table('transactions')->where('application_id', $data->applicationdataid)->where('school_id', $data->school_id)->where('document','!=',NULL)->pluck('grn')->first();
                    $txn_amount = DB::table('transactions')->where('application_id', $data->applicationdataid)->where('school_id', $data->school_id)->where('document','!=',NULL)->pluck('amount')->first();
                    $security_code = DB::table('transactions')->where('application_id', $data->applicationdataid)->where('school_id', $data->school_id)->where('document','!=',NULL)->pluck('security_code')->first();

                    @endphp
                    <div>

  <form method="post" action="{{url('challan_paid_verify')}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

  <input type="hidden" value="{{$data->applicationdataid}}" class="challan_grn" name="applicationdataid" />
  <input type="hidden" value="{{$data->school_id}}" class="challan_grn" name="school_id" />
  <input type="hidden" value="{{$grn}}" class="challan_grn" name="grn" />
  <input type="hidden" value="{{$txn_amount}}" class="challan_txn_amount" name="add_info_4" />
  <input type="hidden" value="{{$security_code}}" class="challan_security_code" name="add_info_5" />
  <button type="submit" class="bg-success text-white ChallanVerify"> Payment Not Verify </button>
</form>
                </td>

        </div>


        @else
        ----

        @endif
        </td>
        @endif
        <!-- <td>{{@$data->school_chairman_phone_number}}</td> -->
        <!-- <td><a href = 'showData/{{@$data->id }}'>show all details</a></td> -->
        <!-- <td>pending</td> -->
        @php
        $i++;
        @endphp
        </tr>
        @endforeach
        @if($i == 0)
        <tr>
          <th colspan="11" style="text-align: center;"><span>No applications Found!!</span></th>
        </tr>
        @endif
        </tbody>
        </table>
        {{-- Pagination --}}
        {{ $AllData->links() }}
      </div>
    </div>
  </div>
  <!-- [ Main Content ] end -->
  <!-- [ Adv Search Model ] -->
  <div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="{{url('application-verify')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> Advance Search :</label>
                @if(isset($adv_data['search_text']))
                <input type="text" name="search_text" value="{{@$adv_data['search_text']}}" class="form-control rte_input" id="search_text" placeholder="School name/ Application no./ Email/ Phone">
                @else
                <input type="text" name="search_text" type="text" class="form-control" placeholder="School name/ Application no./ Email/ Phone">
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <label for="message-text" class="col-form-label"> Block :</label>
                <select class="form-control rte_input" name="Block">
                  @isset($adv_data['block_id'])
                  <option value="{{@$adv_data['block_id']}}">{{@$adv_data['blockName']}}</option>
                  @endisset
                  <option value="">---Select---</option>
                  @foreach($block_list as $key => $value)
                  <option value="{{$key}}">{{$value}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-6">
                <label for="application_status" class="col-form-label"> Status:</label>
                <!-- <input type="number" class="form-control rte_input" name="phone_number" required> -->
                <select class="form-control rte_input" name="application_status">
                  @isset($adv_data['application_status'])
                  <option value="{{@$adv_data['application_status']}}">{{@$adv_data['applicationStatus']}}</option>
                  @endisset
                  <option value="">---Select--- </option>
                  <option value="2">Application Filed </option>
                  <option value="3">Under Verification </option>
                  <option value="4">Verified By DSE </option>
                  <option value="5"> Meeting Scheduled</option>
                  <option value="6">Certified </option>
                  <option value="7"> Rejected By District Committee</option>
                  <option value="8">Rejected By DSE </option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <label for="message-text" class="col-form-label"> From:</label>
                @if(isset($adv_data['fromDate']))
                <input type="date" class="form-control" name="fromDate" id="" value="{{@$adv_data['fromDate']}}">
                @else
                <input type="date" class="form-control" name="fromDate" id="">
                @endif
              </div>
              <div class="col-6">
                <label for="message-text" class="col-form-label"> To:</label>
                @if(isset($adv_data['toDate']))
                <input type="date" class="form-control" name="toDate" id="" value="{{@$adv_data['toDate']}}">
                @else
                <input type="date" class="form-control" name="toDate" id="">
                @endif
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- [ Adv Search End ] -->
  <!-- [ Feedback to school Model ] -->
  <div class="modal fade bd-feedback-school-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <form method="post" action="feedback_to_school" enctype="multipart/form-data">
          @csrf
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="exampleModalLabel1">Physical Verification </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> File :</label>
                <input type="text" name="id" hidden id="feedback_update_id">
                <input type="file" name="feedback_file_upload" class="form-control rounded-0" />
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> Verification Date :</label>
                <input type="datetime-local" name="feedback_date" class="form-control rounded-0" />
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> Write Notes Here :</label>
                <textarea name="feedback_from_dsc_to_school" class="form-control rounded-0" rows="8" placeholder="Write Your Notes Here...!!!"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- [ Feedback to school End ] -->
  <!-- Meeting Modal start -->
  <div class="modal fade bd-example-modal-lg" id="Meeting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMeeting" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title " id="exampleModalLabelMeeting">Meeting For Aplication</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{url('aaplication-meeting')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="modal-body">
            <div class="form-group">
              <input type="hidden" name="ApplicationId" class="ApplicationId">
              <input type="hidden" name="SchoolId" class="SchoolId">
              <label for="message-text" class="col-form-label">Meeting Date :</label>
              <input required name="meeting_deadline" min="<?php echo date('Y-m-d');?>" max="" type="date" class="form-control" id="appeal_deadline">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Upload Document :</label>
              <input type="file" accept=".pdf, .jpg, .png, jpeg" multiple name="meeting_document[]" class="form-control" id="message-text" />
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Remarks :</label>
              <textarea name="meeting_remarks" class="form-control" id="message-text"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-primary" name="submit" value="submit">
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg" id="Accept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelAccept" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title " id="exampleModalLabelAccept">Certify Application</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{url('application-accept')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="modal-body">
            <div class="form-group">
              <label for="message-text" class="col-form-label">Certificate Number :</label>
              <input type="text" required name="certificte_number" class="form-control">
            </div>
            <div class="form-group">
              <input type="hidden" name="ApplicationId" class="ApplicationId">
              <input type="hidden" name="SchoolId" class="SchoolId">
              <label for="message-text" class="col-form-label">Upload Document :</label>
              <input type="file" accept=".pdf, .jpg, .png, jpeg" required multiple name="accpet_document[]" class="form-control" id="message-text" />
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Remarks :</label>
              <textarea required name="accept_remarks" class="form-control" id="message-text"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-primary" name="submit" value="submit">
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg" id="Reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelReject" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title " id="exampleModalLabelReject">Reject Application</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{url('application-reject')}}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="modal-body">
            <div class="form-group">
              <input type="hidden" name="ApplicationId" class="ApplicationId">
              <input type="hidden" name="SchoolId" class="SchoolId">
              <label for="message-text" class="col-form-label">Upload Document :</label>
              <input type="file" accept=".pdf, .jpg, .png, jpeg" required multiple name="reject_document[]" class="form-control" id="message-text" />
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Remarks :</label>
              <textarea required name="reject_remarks" class="form-control" id="message-text"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-primary" name="submit" value="submit">
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade bd-message-school-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <form method="post" action="send_message_to_school" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="exampleModalLabel1">Send message to school</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> File :</label>
                <input type="text" name="send_school_id" hidden id="send_school_id">
                <input type="text" name="send_application_id" hidden id="send_application_id">
                <input type="file" accept=".pdf, .jpg, .png, jpeg" multiple name="send_file_upload[]" class="form-control rounded-0" />
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> Response With in Date :</label>
                <input type="datetime-local" name="send_date" class="form-control rounded-0" />
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <label for="message-text" class="col-form-label"> Write Notes Here :</label>
                <textarea name="send_from_dsc_to_school" class="form-control rounded-0" rows="8" placeholder="Write Your Notes Here...!!!"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade bd-message-history-school-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel1">Histroy of messages</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table id="Record">
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Meeting Modal end -->

</div>
</div>
@include('footer')
<script>


  $('.ChallanVerify').on('click', function (e) {

    var challan_grn = $(this).closest("tr").find('.challan_grn').val();
    var challan_txn_amount = $(this).closest("tr").find('.challan_txn_amount').val();
    var challan_security_code = $(this).closest("tr").find('.challan_security_code').val();
    var dept_id = 'JEPC';
    console.log('challan_grn = ' + challan_grn);
    console.log('challan_txn_amount = ' + challan_txn_amount);
    console.log('challan_security_code = ' + challan_security_code);

    $.ajax({
      url: 'challan_verify_paid',
      type: "POST",
      dataType: "json",
      data: {
        _token: '{!! csrf_token() !!}',
        'amount': challan_txn_amount,
        'grn': challan_grn,
        'security_code': challan_security_code
      },
      success: function (encryption_code) {
        console.log('treasury ');
        console.log(encryption_code);

        $.ajax({
          url: 'https://rte.jharkhand.gov.in/assets/api/challan-auth-token.php',
          type: "POST",
          dataType: "json",
          data: {
            'token': '{!! csrf_token() !!}',
            'EncryptTxt': encryption_code['enc'],
            'REQDEPTID': dept_id
          },
          success: function (data) {
            console.log('success');
            console.log(data);
            data = JSON.parse(data);
            if (data.status == 200) {
              console.log(data);
            }
            else {
              alert('No data Found .');
            }
          }
        });

        // $('#encpt').val(data['encryption']);
      }
    });
  });






  $(".CreateMeeting").click(function () {
    var application_id = $(this).closest("tr").find(".application_id").text();
    var school_id = $(this).closest("tr").find(".school_id").text();
    var Meeting_Date = $(this).closest("tr").find(".Meeting_Date").text().trim();
    $('#appeal_deadline').attr('max', Meeting_Date);
    $('.ApplicationId').val(application_id);
    $('.SchoolId').val(school_id);
  });
  $(".CreateAccept").click(function () {
    var application_id = $(this).closest("tr").find(".application_id").text();
    var school_id = $(this).closest("tr").find(".school_id").text();
    $('.ApplicationId').val(application_id);
    $('.SchoolId').val(school_id);
  });
  $(".CreateReject").click(function () {
    var application_id = $(this).closest("tr").find(".application_id").text();
    var school_id = $(this).closest("tr").find(".school_id").text();
    $('.ApplicationId').val(application_id);
    $('.SchoolId').val(school_id);
  });
  function feedback_to_school($id) {
    document.getElementById("feedback_update_id").value = $id;
  }
  // School send massege 

  $(".send_message").click(function () {
    var application_id = $(this).closest("tr").find(".application_id").text();
    var school_id = $(this).closest("tr").find(".school_id").text();
    $('#send_application_id').val(application_id);
    $('#send_school_id').val(school_id);
  });
  $(".message_history_to_school").click(function () {
    console.log('clicked');
    var applicationId = $(this).closest("tr").find(".application_id").text();
    var schoolId = $(this).closest("tr").find(".school_id").text();

    $.ajax({
      url: 'update-message-status/' + applicationId,
      type: "GET",
      dataType: "json",

      success: function (data) {
        console.log('success ');
      }
    });
    $('#Record').html('Record Not Present');
    var trData = $(this).closest('tr').find('.message_History').html();
    //  console.log(trData);
    $('#Record').html(trData);
    //  trData = '';
  });


</script>