@include('home.header')
<style>
    .background-box table td {
        padding: 0.35rem !important;
    }
</style>
<div class="container">
    <br><br><br>
    <div class="row">

        <!-- <div class="col-md-4">

        </div> -->
        <!-- <div class="col-md-8">
            <iframe loading="lazy" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.7300678286397!2d86.15204389999998!3d22.812463900000008!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f5e45f796cd2a1%3A0x5b6d6f08c92e94ab!2sDuplex+No.2+Vijaya's+Heritage%2C+Vijaya+Heritage%2C+Uliyan%2C+Jamshedpur%2C+Jharkhand+831005!5e0!3m2!1sen!2sin!4v1427898047741" width="400" height="300" frameborder="0" data-gmtmxk-handled="1"></iframe>

        </div> -->
        <div class="col-md-12">
            <div class="background-box mb-4 ">
                <table class="table table-bordered ">
                    <thead>
                        <tr class="text-center top bg-primary">
                            <th colspan="5" class="text-white">CONTACT PERSONS</th>
                        </tr>
                        <tr>
                            <th scope="col">S.NO.</th>
                            <th scope="col">Concern Issue</th>
                            <th scope="col">Officer</th>
                            <th scope="col">Phone/Fax </th>
                            <th scope="col">Email Id</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Publications: Textbooks, Journals, etc.</td>
                            <td>Prof. Anup Kumar Rajput</td>
                            <td>+91-11-26852261</td>
                            <td>pd.ncert@nic.in</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Availability of Printed Textbooks, Journals, etc.</td>
                            <td>-</td>
                            <td>+91-11-26519245<br> +91-11-26592334</td>
                            <td>cbm.ncert@nic.in</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Textbooks (Online) </td>
                            <td>Dr. Amarendra Prasad Behera</td>
                            <td>+91-11-26962580</td>
                            <td>dceta.ncert@nic.in</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>National Talent Search<br>a. NTS Examination Inquiry<br>b. NTS Scholarship Inquiry</td>
                            <td>-</td>
                            <td>+91-11-26515382<br> +91-11-26560464<br> +91-11-26562704</td>
                            <td>deme.ncert@nic.in<br>esdhead@gmail.com<br>esdhead.ncert@gov.in ntse2@yahoo.co.in</td>
                        </tr>
                        <tr>
                            <th scope="row">5</th>
                            <td>NIE Guest House</td>
                            <td>-</td>
                            <td>+91-11-26961921<br> +91-11-26961921<br> +91-11-26864732</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">6</th>
                            <td>Library</td>
                            <td>-</td>
                            <td>+91-11-26961745</td>
                            <td>chairperson.ldd@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">7</th>
                            <td>Examinations / Evaluation</td>
                            <td>-</td>
                            <td>+91-11-26536778<br> +91-11-26562704</td>
                            <td>deme.ncert@nic.in<br>esdhead@gmail.com<br>esdhead.ncert@gov.in</td>
                        </tr>
                        <tr>
                            <th scope="row">8</th>
                            <td>In-charge, Public Relations</td>
                            <td>-</td>
                            <td>+91-11-26569157</td>
                            <td>ncert.media@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">9</th>
                            <td>School Kits</td>
                            <td>-</td>
                            <td>+91-11-26966257</td>
                            <td>dek.ncert@gmail.com</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@include('home.footer')