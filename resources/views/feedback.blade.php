@include('header')
@include('sidenav')
@include('topbar')
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<style>
  @import url(http://fonts.googleapis.com/css?family=Roboto:500,100,300,700,400);
  
  * {
    margin: 0;
    padding: 0;
    font-family: roboto;
  }
  .cont {
    width: 100%;
    /* max-width: 350px; */
    text-align: center;
  
    /* margin-left:15%; */
    padding: 30px 0;
    background: #FBFBFB;
    color: #EEE;
    border-radius: 5px;
    /* border: thin solid #444; */
    overflow: hidden;
  }
  .cont1 {
    width: 100%;
    /* max-width: 350px; */
    text-align: center;
  
    /* margin-left:15%; */
    padding: 10px 0;
    background: #FBFBFB;
    color: #EEE;
    border-radius: 5px;
    /* border: thin solid #444; */
    overflow: hidden;
  }
  
  div.stars {
    width: 400px;
  }
  
  div.stars1 {
    width: 430px;
  }
  input.star { display: none; }
  
  label.star {
    float: right;
    padding: 10px;
    font-size: 36px;
    color: #444;
    transition: all .2s;
  }
  
  input.star:checked ~ label.star:before {
    content: '\f005';
    color: #FD4;
    transition: all .25s;
  }
  
  input.star-5:checked ~ label.star:before {
    color: #FE7;
    text-shadow: 0 0 20px #952;
  }
  
  input.star-1:checked ~ label.star:before { color: #F62; }
  
  label.star:hover { transform: rotate(-15deg) scale(1.3); }
  
  label.star:before {
    content: '\f006';
    font-family: FontAwesome;
  }
  </style>
  <link href="http://www.cssscript.com/wp-includes/css/sticky.css" rel="stylesheet" type="text/css">
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Feedback </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('feedbackShow')}}"><i class="fa fa-file" aria-hidden="true"></i>
                        <li class="breadcrumb-item"><a href="">Feedback Show</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4 pt-5 ">
        <div class="shadow-lg cont" >
          <span class="text-primary" style="font-size: 32px;">Write Your Feedback </span>
          @if( Session::has('message') )
          <div class="alert alert-success mx-3 mt-3" role="alert">
              {{ Session::get('message') }}
          </div>
          @endif
          <div class="">
          <div class="stars">
              <form action="" method="POST">
                @csrf
              <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
              <label class="star star-5" for="star-5"></label>
              <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
              <label class="star star-4" for="star-4"></label>
              <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
              <label class="star star-3" for="star-3"></label>
              <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
              <label class="star star-2" for="star-2"></label>
              <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
              <label class="star star-1" for="star-1"></label>
          </div>
        </div>
          <div class="px-5"> <textarea rows="7" required name="feedback" class="form-control rounded-0" placeholder="Write Your Feedback Here...!!!"></textarea></div>
          <div class="py-3 px-5"><button type="submit" style="float:right;" name="submit" class="btn btn-outline-primary rounded-0" value="Submit" >Submit</button></div>
        </form>
        </div>
      </div>
      
    </div>
    
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
@include('footer')