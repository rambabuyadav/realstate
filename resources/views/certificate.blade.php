@include('header')
@include('sidenav')
@include('topbar')
<style>
  page[size="A4"] {
    background: #f9f5fe;
    width: 29cm;
    height: 40.7cm;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
  }
  @media print {
    body,
    page[size="A4"] {
      margin: 0;
      box-shadow: 0;
      /* / -webkit-print-color-adjust: exact !important; / */
    }
  }
  .container {
    position: relative;
    text-align: center;
    color: black;
    padding: 25px;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
@php $is_certificate_available = DB::table('application_data')->where('school_id',Auth::user()->school_id)->where('application_status', 6)->exists() @endphp
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-12">
            <div class="page-header-title">
              <h5 class="m-b-10">Certificate </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">Certificate </a></li>
            </ul>
          </div>
          <div class="col-md-12 text-right pr-12">
            @if($is_certificate_available)
            <form method="post" action="{{url('school-certificate')}}" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <button type="submit" name="submit" class="btn btn-sm btn-light btn-offset pull-right" target="_blank" value="pdf" title="Export certificatet in pdf"><a class="text-success" title="Export certificatet in pdf"> <i class="fa fa-file-pdf"> </i> </a></button>
            </form>
            @endif
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
  
<page size="A4" style="background-image: url(assets/images/Certificate-jh.jpg) ; background-repeat: no-repeat;background-size: 100% 100%;">
  <div class="container " style="padding-bottom:0px;">
      <br>
      <br>
      <br>
      <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
      <div class="certifactebox" style="line-height:20px;">
          <span style="margin-left:-20px;font-size:20px;font-weight: bold;">
              Certificate No. - {{$schoolDetail->certificate_number}}
          </span>
          <span style="margin-left:400px;font-size:20px;font-weight: bold;">
              Date : {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}}
          </span>
          
          <h1 class="centerItem" style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-bottom:0px;font-size: 40px;">Department of School Education & Literacy</h1>
          <h1 class="centerItem" style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-bottom:20px;font-size: 35px;"><u>Goverment of Jharkhand</u></h1>
          <!-- <p class="centerItem" style="font-size:15px;line-height:20px;margin-bottom: 0px;">(An Autonomous organisation under the union ministry of Human Resource Development, Jharkhand)</p>
                  <p class="centerItem" style="font-size:17px; ">Sector 3, Dhurwa, Ranchi-843004, Jharkhand</p> -->
      </div>
      <div class="centerHeading centerItem" style="margin-top: 50px;">
          <span style="font-family: Monotype Corsiva;font-size:33px; font-weight: bold;">Right of Children to Free & Compulsory Education Act 2009</span>
      </div>
      <div class="centerHeading centerItem" style="padding-top: 10px;padding-bottom: 10px;">
          <span style="font-family:Monotype Corsiva;color:rgb(146, 3, 3);font-size:45px;font-weight:1000;margin-top: 10px;margin-bottom: 10px;"><b>Certificate of Recognition</b></span>
      </div>
      <div class="centerHeading centerItem">
          <span style="font-family:Times New Roman;font-size:23px;">is being awarded to</span>
      </div>
<br>
      <div class="container main-cert-text" style="font-size:30px;line-height:34px;padding-bottom:0px;">
          <div class="col-md-12" style="font-family:Monotype Corsiva;font-weight: bold;">
              <span> &nbsp;</span><span style="border-bottom:1px solid black;">{{strtoupper($schoolDetail->school_name)}}</span> of District <span style="border-bottom:1px solid black;"> {{ucfirst(strtolower($schoolDetail->district))}} </span>  w.e.f {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}}
          </div>
          <br>
          <br>
          
          <span style="float:left;margin-left:240px;font-family: Tahoma; ">Sig.</span>
          <span style="margin-right: 120px;float:right;font-family: Tahoma; ">Sig.</span>
          <br>
          <span style="float:left;margin-left:40px;text-align:center;font-family: Tahoma; ">District Superintendent of Education</span>
          <span style="margin-right: 30px;float:right;text-align:center;font-family: Tahoma;; ">Deputy Commissioner</span>
          <br>
          
          <span style="float:left;margin-left:220px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span>
          <span style="float:right;margin-right: 90px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span>
          <br>
          <br>
          
      
    
              <center>
                  <span style="font-size: 12px;">This certificate is online generated from https://rte.jharkhand.gov.in and does not require signature.</span>
              </center>
        
      </div>
  </div>
  
  <div style="border:2px solid rgb(146, 3, 3);margin: 0px 80px;"></div>
 
 
  <div class="container ">
      <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
      <div class="certifactebox" style="line-height:20px;">
          <span style="margin-left:-20px;font-size:20px;font-weight: bold;">
              प्रमाण पत्र संख्या - {{$schoolDetail->certificate_number}}
          </span>
          <span style="margin-left:400px;font-size:20px;font-weight: bold;">
              दिनांक: {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}}
          </span>
          <br>
          <br>
         
          <h1 class="centerItem" style="font-family:Times New Roman;font-size:45px;color:rgb(146, 3, 3);margin-bottom:0px;">स्कूली शिक्षा एवं साक्षरता विभाग</h1>
          <h1 class="centerItem" style="font-family:Times New Roman;font-size:35px;color:rgb(146, 3, 3);margin-bottom:20px;"><u>झारखण्ड सरकार</u></h1>
          <!-- <p class="centerItem" style="font-size:15px;line-height:20px;margin-bottom: 0px;">(An Autonomous organisation under the union ministry of Human Resource Development, Jharkhand)</p>
                <p class="centerItem" style="font-size:17px; ">Sector 3, Dhurwa, Ranchi-843004, Jharkhand</p> -->
      </div>
      <div class="centerHeading centerItem" style="margin-top: 50px;">
          <span style="font-family: Monotype Corsiva;font-size:33px; font-weight: bold;">
              <!-- बच्चों को मुफ्त और अनिवार्य शिक्षा का अधिकार अधिनियम 2009 -->
              निःशुल्क और अनिवार्य बाल शिक्षा का अधिकार अधिनियम, 2009
          </span>
      </div>
      <div class="centerHeading centerItem" style="padding-top: 10px;padding-bottom: 10px;">
          <span style="font-family:Times New Roman;font-size:45px;color:rgb(146, 3, 3);font-weight: bold;">मान्यता का प्रमाण पत्र</span>
      </div>
      <div class="centerHeading centerItem">
          <span style="font-family:Times New Roman;font-size:23px;">से सम्मानित किया जा रहा है</span>
      </div>
      <br>
      <br>
      <div class=" main-cert-text" style="font-size:30px;line-height:34px;">
          <div class="col-md-12" style="font-family:Monotype Corsiva;font-weight: bold;">
              <span> &nbsp;</span><span style="border-bottom:1px solid black;">{{strtoupper($schoolDetail->school_name)}}</span> , जिला  <span style="border-bottom:1px solid black;"> {{ucfirst(strtolower($schoolDetail->district))}} </span> प्रभावी तिथि {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}}
          </div>
     
      <br>
      <br>
      
    <div>
        <span style="float:left;margin-left:150px;font-family: Tahoma; ">ह./-</span><span style="margin-right: 90px;float:right;font-family: Tahoma; ">ह./-</span>
        <br>
        
        
        <span style="float:left;margin-left:70px;text-align:center;font-family: Tahoma; ">जिला शिक्षा अधीक्षक</span><span style="float:right;margin-right:80px;text-align:center;font-family: Tahoma;; ">उपायुक्त</span>
        <br>
        <span style="float:left;margin-left:110px;text-align:center;"> {{ucfirst(strtolower($schoolDetail->district))}} </span><span style="float:right;margin-right:80px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span>
        <br>
        <br>
      </div>
          
     
              <center>
                  <span style="font-size: 12px;">यह कंप्यूटर जनित प्रमाण पत्र https://rte.jharkhand.gov.in से निर्गत किया गया है एवं हस्ताक्षर की आवश्यकता नहीं है.</span>
              </center>
        </div>
  </div>
</page>
<page size="A4" style="background-image: url(assets/images/Certificate-jh.jpg) ; background-repeat: no-repeat;background-size: 100% 100%;">
  <div class="container " >
      <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
      <div class="certifactebox" style="line-height:20px;padding: 30px;">
          <br><br> <br><br> <br><br> 
          <h3 class="centerItem" style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-bottom:0px;font-size: 40px;"><u>Condition of Recognition</u></h3>
          <br><br>
          <p  style="font-family:Times New Roman;color:rgb(146, 3, 3);font-size: 25px;">
              Recognition of this School is granted under provision specified in <br><br> 
              section 18 of The Right of Children to Free and compulsory Education Act,  <br><br> 
               2009 & Jharkhand Right of Children to Free and compulsory Education Rules 2011 ,  <br><br>  
                provided that the school will ensure that the provisions given in The Right of <br><br>
                 Children to Free and compulsory Education Act, 2009, Jharkhand Right   <br><br>
               of Children to Free and compulsory Education Rules 2011, Department notification <br><br>
                no. 629 dated 25.04.2019 and other directions issued from time to time by the <br><br>  
                 department are strictly adhered to. <br><br>  
            </p>
      </div>
      <div style="border:2px solid rgb(146, 3, 3);margin: 0px 80px;"></div>
      <div class="container " >
          <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
          <div class="certifactebox" style="line-height:20px;padding: 30px;">
              <h3 class="centerItem" style="font-family:Times New Roman;font-size:29px;color:rgb(146, 3, 3);margin-bottom:0px;font-size: 40px;"><u>मान्यता की शर्त</u></h3>
              <br><br>
              <p  style="font-family:NotoSansRegular;font-size:15px;color:rgb(146, 3, 3);font-size: 25px;">
                 
                  इस स्कूल की मान्यता नि: शुल्क और अनिवार्य शिक्षा का अधिकार अधिनियम, <br><br> 
                  2009 की धारा 18 में निर्दिष्ट प्रावधान के तहत दी गई है और झारखंड नि: शुल्क  <br><br>  
                  और अनिवार्य शिक्षा नियम 2011 के लिए बच्चों का अधिकार है, बशर्ते कि स्कूल यह <br><br> 
               सुनिश्चित करेगा कि प्रावधानों में दिए गए प्रावधान नि: शुल्क और अनिवार्य शिक्षा का अधिकार <br><br> 
                अधिनियम, 2009, झारखंड बच्चों का नि: शुल्क और अनिवार्य शिक्षा नियम 2011, विभाग <br><br>  
                 की अधिसूचना संख्या 629 दिनांक 25.04.2019 और विभाग द्वारा समय-समय पर जारी <br><br>  
                    किए गए अन्य निर्देशों का कड़ाई से पालन किया जाता है।
                </p>
          </div>
      </div>
  </div>
</page>
    <!-- [ Main Content ] end -->
  </div>
</div>
@include('footer')