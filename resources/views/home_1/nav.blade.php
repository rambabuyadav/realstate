<section class="navigation" style="background:#e04848bb;padding:3px;">
    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="navbar-container">
                    <!-- <nav class="navbar navbar-expand-lg  ">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background:white;outline:none;">
                            <span class="navbar-toggler-icon"><i class="fas 'fa-bars"></i></span>
                        </button>
                        <div class="collapse navbar-collapse pull-right" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto  ">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href=" ">Guidelines </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="margin-top:10px;margin-left:6px;background:#e2f2ff;">
                                        @php $GuideLine_List = DB::table('guide_lines')->where('status',1)->get(); @endphp
                                        @foreach($GuideLine_List as $k=>$GuideLine)

                                        <a class="dropdown-item" href="{{url('public/assets/Guideline/')}}/{{$GuideLine->document}}" style="color:black !important;" title="RTE Act" target="_blank">{{$GuideLine->heading}}</a>
                                        @endforeach
                                    </div>
                                </li>
                               
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('faq')}}">FAQ'S</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Contact Us </i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="margin-top:10px;margin-left:6px;background:#e2f2ff;">
                                        <a class="dropdown-item" href="{{url('dse-contact')}}" style="color:black !important;">DSE</a>
                                        <a class="dropdown-item" href="{{url('deo-contact')}}" style="color:black !important;">DEO</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Registration </i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="width:200px;margin-top:10px;background:#e2f2ff;">
                                        <a class="dropdown-item" href="{{route('school_register.index')}}" style="color:black !important;">School Registration</a>
                                        <a class="dropdown-item" href="{{route('student_register.index')}}" style="color:black !important;">Student Registration</a>
                                    </div>
                                </li>
                                <li class="nav-item" style="border-right:none;">
                                    <a class="nav-link" href="{{url('/login')}}">Login</a>
                                </li>
                            </ul>
                        </div>
                    </nav> -->

                </div>
                <nav id="navigation" class="navbar navbar-expand-lg navbar-light">
                    <div class="container-fluid">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse pl-5" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?ln=">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about-us.php?ln=">About Us</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="organisation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Organisation
                                    </a>
                                    <div class="dropdown-menu bg-danger" aria-labelledby="organisation">
                                        <a class="dropdown-item text-white p-2 bg-danger" href="management.php?ln=">Management</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="senior-functionaries.php?ln=">Senior Functionaries</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="disclo_un_rti.php?ln=">Disclosure Under RTI</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="programme-advisory-committee.php?ln=">Programme Advisory Committee</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="constituents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Constituents and Departments
                                    </a>
                                    <div class="dropdown-menu constituents bg-danger" aria-labelledby="constituents">
                                        <a class="dropdown-item text-white p-2 bg-danger" href="national-institute-education.php?ln=">National Institute of Education</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="regional-institutes-education.php?ln=">Regional Institutes of Education</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="https://ciet.nic.in" target="">Central Institute of Educational Technology</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="pss-vocational-education.php?ln=">PSS Central Institute of Vocational Education</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="programmes" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Programmes
                                    </a>
                                    <div class="dropdown-menu programmes bg-danger" aria-labelledby="programmes">
                                        <a class="dropdown-item text-white p-2 bg-danger" href="national-talent-examination.php?ln=">National Talent Search Examination</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="all-india-school-education-survey.php?ln=">All India School Education Survey</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="teacher-innovation-awards.php?ln=">Teacher Innovation Awards</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="jn-national-science-exhibition.php?ln=">Jawaharlal Nehru National Science Exhibition</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="media-programmes.php?ln=">Media Programmes</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="research-grants-(ERIC).php?ln=">Research Grants (ERIC)</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="gallery" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Gallery
                                    </a>
                                    <div class="dropdown-menu gallery bg-danger" aria-labelledby="gallery">
                                        <a class="dropdown-item text-white p-2 bg-danger" href="international-yoga.php?ln=">International Yoga Day</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="kala-utsav.php?ln=">Kala Utsav</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="FoundationDay.php">Foundation Day</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="eRaksha.php">Launch of eRaksha</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="healthandwell.php">Health and Wellness</a>
                                        <! </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="publication" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Publication
                                    </a>
                                    <div class="dropdown-menu publication mega-dropdown-menu bg-danger" aria-labelledby="publication">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <a class="dropdown-item  text-white p-2 bg-danger" href="list-publication.php?ln=">List of Publication</a>
                                                <a class="dropdown-item  text-white p-2 bg-danger" href="https://epathshala.nic.in//pages.php?id=download-app&amp;ln=en" target="_blank">ePub</a>
                                                <a class="dropdown-item  text-white p-2 bg-danger" href="https://epathshala.nic.in//process.php?id=students&amp;type=eTextbooks&amp;ln=en" target="_blank">Flipbook</a>
                                                <a class="dropdown-item  text-white p-2 bg-danger" href="textbook.php">PDF(I-XII)</a>
                                                <a class="dropdown-item  text-white p-2 bg-danger" href="state-uts-eBook.php?ln=">State/ Uts eBook (epub)</a>
                                                <a class="dropdown-item  text-white p-2 bg-danger" href="vocational-education.php?ln=">Vocational Education</a>
                                            </div>

                                        </div>




                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="announcement" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Announcement
                                    </a>
                                    <div class="dropdown-menu announcement bg-danger" aria-labelledby="announcement">
                                        <a class="dropdown-item text-white p-2 bg-danger" href="vacancies.php?ln=">Vacancies</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="tenders.php?ln=">Tenders</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="notices.php?ln=">Notices</a>
                                        <!--<a class="dropdown-item" href="ncert-news.php?ln=">NCERT in News</a>-->
                                        <a class="dropdown-item text-white p-2 bg-danger" href="other-announcements.php?ln=">Other Announcements</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="seminar.php?ln=">Seminar/ Conference/ Workshop</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="contacts" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Contacts
                                    </a>
                                    <div class="dropdown-menu contacts bg-danger" aria-labelledby="contacts">
                                        <a class="dropdown-item text-white p-2 bg-danger" href="persons-contact.php?ln=">Persons to Contact</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="public-information-officers.php?ln=">Public Information Officers</a>
                                        <a class="dropdown-item text-white p-2 bg-danger" href="telephone-directory.php?ln=">Telephone Directory</a>
                                    </div>
                                </li>
                            </ul>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-dark my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>