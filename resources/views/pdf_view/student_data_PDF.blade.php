<div>
  <title>Student Information</title>
  <table border="1" style="border-collapse: collapse;width: 100%;" class="table table-bordered  ">
    <tr>
        <th>SL NO.</th>
        <th>FIELD NAME</th>
        <th>VALUE</th>

    </tr>
    @php
    $tot = 1;
    $district_val = DB::table('fnd_values')->where('fnd_values.id', $students_data->district)->value('display_value');
    $block_val = DB::table('fnd_values')->where('fnd_values.id', $students_data->block)->value('display_value');
    @endphp
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Full Name</td>
        <td class="value_name">{{@$students_data->full_name}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">District</td>
        <td class="value_name">{{@$district_val}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Block</td>
        <td class="value_name">{{@$block_val}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Post Office</td>
        <td class="value_name">{{@$students_data->post_office}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Email</td>
        <td class="value_name">{{@$students_data->email}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Address</td>
        <td class="value_name">{{@$students_data->address}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Village</td>
        <td class="value_name">{{@$students_data->village}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Pin Code</td>
        <td class="value_name">{{@$students_data->pin}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Nearest Police Station</td>
        <td class="value_name">{{@$students_data->nearest_police_station}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Religion</td>
        <td class="value_name">{{@$students_data->religion}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Gender</td>
        <td class="value_name">{{@$students_data->gender}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Caste</td>
        <td class="value_name">{{@$students_data->caste}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">blood Group</td>
        <td class="value_name">{{@$students_data->blood_group}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Disability if Any</td>
        <td class="value_name">{{@$students_data->disability}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Name</td>
        <td class="value_name">{{@$students_data->father_name}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Primary Phone</td>
        <td class="value_name">{{@$students_data->father_primary_phone}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Secondary Phone</td>
        <td class="value_name">{{@$students_data->father_secondary_phone}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Blood Group</td>
        <td class="value_name">{{@$students_data->father_blood_group}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Income</td>
        <td class="value_name">{{@$students_data->father_income}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Mother Name</td>
        <td class="value_name">{{@$students_data->mother_name}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Mother Primary Phone</td>
        <td class="value_name">{{@$students_data->mother_primary_phone}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Mother Secondary Phone</td>
        <td class="value_name">{{@$students_data->mother_secondary_phone}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Mother Blood Group</td>
        <td class="value_name">{{@$students_data->mother_blood_group}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Mother Income</td>
        <td class="value_name">{{@$students_data->mother_income}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Previous School Name</td>
        <td class="value_name">{{@$students_data->previous_school_name}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Last Class Studied</td>
        <td class="value_name">{{@$students_data->last_class_studied}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Class Applied</td>
        <td class="value_name">{{@$students_data->class_applied}}</td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Referred School Name</td>
        <td class="value_name">
            @if(isset($students_data->referred_school_name))
            @foreach($students_data->referred_school_name as $key => $val)
            {{@$val}}<br>
            @endforeach
            @endif
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Address of the School</td>
        <td class="value_name">
            @if(isset($students_data->referred_school_address))
            @foreach($students_data->referred_school_address as $key => $val)
            {{@$val}}<br>
            @endforeach
            @endif
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Distance</td>
        <td class="value_name">
            @if(isset($students_data->distance))
            @foreach($students_data->distance as $key => $val)
            {{@$val}}<br>
            @endforeach
            @endif
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Last Class Marksheet</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->last_class_marksheet}}" target="_blank"> {{@$students_data->last_class_marksheet}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Photo</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->photo}}" target="_blank"> {{@$students_data->photo}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Birth Certificate</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->birth_certificate}}" target="_blank"> {{@$students_data->birth_certificate}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Aadhaar Card</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->aadhaar_card}}" target="_blank"> {{@$students_data->aadhaar_card}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Aadhaar Card</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->father_aadhaar_card}}" target="_blank"> {{@$students_data->father_aadhaar_card}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Ration Card</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->ration_card}}" target="_blank"> {{@$students_data->ration_card}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Father Income Certificate</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->father_income_certificate}}" target="_blank"> {{@$students_data->father_income_certificate}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Mother Income Certificate</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->mother_income_certificate}}" target="_blank"> {{@$students_data->mother_income_certificate}}</a>
        </td>
    </tr>
    <tr>
        <td class="si_no">{{$tot++}}</td>
        <td class="lable_name">Community Certificate</td>
        <td class="value_name">
            <a href="{{url('http://paatham.us/rte-application/public/student/')}}{{@$students_data->community_certificate}}" target="_blank"> {{@$students_data->community_certificate}}</a>
        </td>
    </tr>

</table>
</div>