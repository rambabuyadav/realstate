
<!-- <style>
@font-face {
  font-family: "Dojo Sans Serif";
  font-style: normal;
  font-weight: normal;
  src: url(http://example.com/fonts/dojosans.ttf) format('truetype');
}
* {
  font-family: "Dojo Sans Serif", "DejaVu Sans", sans-serif;
}
</style> -->
<style>
  @font-face {
  font-family: "Dojo Sans Serif";
  font-style: normal;
  font-weight: normal;
  src: url(http://example.com/fonts/dojosans.ttf) format('truetype');
}
* {
  font-family: "Dojo Sans Serif", "DejaVu Sans", sans-serif;
}
 
 

  .container {
    position: relative;
    text-align: center;
    color: black;
    padding: 25px;
  }

  .bottom-left {
    position: absolute;
    bottom: 19%;
    left: 19%;
  }

  .top-left {
    position: absolute;
    top: 16%;
    left: 12%;
  }

  .top-right {
    position: absolute;
    top: 16%;
    right: 11%;
  }

  .bottom-right {
    position: absolute;
    bottom: 19%;
    right: 19%;
  }

  .centered {
    position: absolute;
    top: 45%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .centered1 {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .std_info {
    position: absolute;
    top: 50%;
    right: 24%;
  }

  ::placeholder {
    color: red;
    opacity: 1;
    /* Firefox */
    font-size: 20px;
  }

  input {
    font-size: 20px;
  }

  input:focus {
    outline: none;
  }

  @media print {
    img {
      width: 595px;
      height: 8px;
    }

    .std_info {
      width: 700px;
      right: 21%;
    }

    .container {
      width: 1123px;
      height: 724px;
    }
  }
    
  .centerItem {
    display: flex;
    justify-content: center;
  }
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">

    <!-- [ Main Content ] start -->
    <page size="A4" style="background-image: url(../assets/images/Certificate-jh.jpg) ; background-repeat: no-repeat;background-size: 100% 100%;">

      <div class="row" >
        <div class="col-md-12 pt-5" >
          <!-- <div style="border:2px solid rgb(146, 3, 3);">
            <div style="border:4px solid rgb(146, 3, 3); margin: 2px;">
              <div style="border:2px solid rgb(146, 3, 3);margin: 2px;"> -->
          <div>
            <div>
              <div>
                <div class="container " >
                <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
                <br>
                <div class="certifactebox" style="line-height:20px;">
                 
                      <span style="margin-left:0px;font-size:20px;font-weight: bold;">
                        Certificate No. - {{$schoolDetail->certificate_number}} 
                      </span>   
                      <span  style="margin-left:40px;font-size:20px;font-weight: bold;">
                        Date : {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}} 
                      </span>
                 
                  <h3 class="centerItem" style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-bottom:0px;">Department of School Education & Literacy</h3>
                  <h3 class="centerItem" style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-bottom:-20px;">Goverment of Jharkhand</h3>
                  <!-- <p class="centerItem" style="font-size:15px;line-height:20px;margin-bottom: 0px;">(An Autonomous organisation under the union ministry of Human Resource Development, Jharkhand)</p>
                    <p class="centerItem" style="font-size:17px; ">Sector 3, Dhurwa, Ranchi-843004, Jharkhand</p> -->
                  </div>
                  <br>
                  <div class="centerHeading centerItem" style="margin-top: 50px;">
                    <span style="font-family: Monotype Corsiva;font-size:23px; font-weight: bold;">Under Section 18 of Right to Education Act 2009</span>
                  </div>
                  <div class="centerHeading centerItem">
                    <span style="font-family:Times New Roman;font-size:23px;">Certificate of Recognition</span>
                  </div>
                  <br>
                  <div class="centerHeading centerItem">
                    <span style="font-family:Times New Roman;font-size:23px;">is being awarded to</span>
                  </div>
                  <br>
                  <div class="container main-cert-text" style="font-size:24px;line-height:34px;">
                    <div class="col-md-12"  style="font-family:Monotype Corsiva;font-weight: bold;">
                      <span> &nbsp;</span><span style="border-bottom:1px solid black;font-size:19px;">{{strtoupper($schoolDetail->school_name)}}</span>  of  <span style="border-bottom:1px solid black;font-size:19px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span> District w.e.f {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}}
                    </div>
                  </div>
                 

                <div>
                  <span style="float:left;margin-left:90px;font-family: Tahoma; ">Sig.</span><span style="margin-right: 60px;float:right;font-family: Tahoma; ">Sig.</span><br>
                </div>
                <div>
                  <span style="float:left;margin-left:50px;text-align:center;font-family: Tahoma; ">District Superintendent of Education</span><span style="margin-right: 40px;float:right;text-align:center;font-family: Tahoma;; ">District Committee</span>
                </div>
                <br>
                <div>
                  <span style="float:left;margin-left:90px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span><span style="float:right;margin-right: 50px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span>
                </div>
               <br>
                <div class="row" style="padding-bottom: 10px;">
                  <div class="col-md-12">
                    <center>
                      <span style="font-size: 12px;" >This certificate is online generated from https://rte.jharkhand.gov.in and does not require signature.</span>
                    </center>
                  </div>

                </div>

              </div>
              <div style="border:2px solid rgb(146, 3, 3);margin: 0px 80px;"></div>
             
                <div class="container " >
                <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
                <div class="certifactebox" style="line-height:20px;">
                  
                      <span style="margin-left:0px;font-size:20px;font-weight: bold;">
                        प्रमाण पत्र संख्या  - {{$schoolDetail->certificate_number}} 
                      </span>   
                      <span  style="margin-left:40px;font-size:20px;font-weight: bold;">
                        दिनांक: {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}} 
                      </span>
                 
                     
                      <h3 class="centerItem" style="font-family:Times New Roman,font-size:29px;color:rgb(146, 3, 3);margin-bottom:0px;">स्कूली शिक्षा एवं साक्षरता विभाग</h3>
                  <h3 class="centerItem" style="font-family:Times New Roman,font-size:29px;color:rgb(146, 3, 3);margin-bottom:-20px;">झारखण्ड सरकार</h3>
                  <!-- <p class="centerItem" style="font-size:15px;line-height:20px;margin-bottom: 0px;">(An Autonomous organisation under the union ministry of Human Resource Development, Jharkhand)</p>
                  <p class="centerItem" style="font-size:17px; ">Sector 3, Dhurwa, Ranchi-843004, Jharkhand</p> -->
                </div>
                <div class="centerHeading centerItem" style="margin-top: 0px;">
                  <span style="font-family: Monotype Corsiva;font-size:23px; font-weight: bold;">शिक्षा का अधिकार अधिनियम २००९ के अनुच्छेद १८ के तहत</span>
                </div>
              <br>
                <div class="centerHeading centerItem">
                  <span style="font-family:Times New Roman;font-size:23px;">मान्यता का प्रमाण पत्र</span>
                </div>
              
                <div class="centerHeading centerItem">
                  <span style="font-family:Times New Roman;font-size:23px;">से सम्मानित किया जा रहा है</span>
                </div>
               
                <div class="container main-cert-text" style="font-size:24px;line-height:34px;">
                  <div class="col-md-12"  style="font-family:Monotype Corsiva;font-weight: bold;">
                    <span> &nbsp;</span><span style="border-bottom:1px solid black;font-size:19px;">{{strtoupper($schoolDetail->school_name)}}</span>  ,  <span style="border-bottom:1px solid black;font-size:19px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span> जिला प्रभावी तिथि  {{\Carbon\Carbon::parse($schoolDetail->certified_date)->format('d/m/Y')}}
                  </div>
                </div>
              
                <div>
                  <span style="float:left;margin-left:60px;font-family: Tahoma; ">ह./-</span><span style="margin-right: 60px;float:right;font-family: Tahoma; ">ह./-</span><br>
                </div>
                <br>
             
                <div>
                  <span style="float:left;margin-left:30px;text-align:center;font-family: Tahoma; ">जिला शिक्षा अधीक्षक</span><span style="margin-right: 60px;float:right;text-align:center;font-family: Tahoma;; ">उपायुक्त</span>
                </div>
                <br>
                
                <div>
                  <span style="float:left;margin-left:50px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span><span style="float:right;margin-right: 50px;"> {{ucfirst(strtolower($schoolDetail->district))}} </span>
                </div>
               
                <br>
                <div class="row" style="padding-bottom: 10px;">
                  <div class="col-md-12">
                    <center>
                      <span style="font-size: 12px;" >यह कंप्यूटर जनित प्रमाण पत्र https://rte.jharkhand.gov.in से निकाला गया है एवं हस्ताक्षर की आवश्यकता नहीं है.</span>
                    </center>
                  </div>

                </div>

              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </page>
    <page size="A4" style="background-image: url(../assets/images/Certificate-jh.jpg) ; background-repeat: no-repeat;background-size: 100% 100%;">

      <div class="row" >
        <div class="col-md-12 pt-5" >
          <!-- <div style="border:2px solid rgb(146, 3, 3);">
            <div style="border:4px solid rgb(146, 3, 3); margin: 2px;">
              <div style="border:2px solid rgb(146, 3, 3);margin: 2px;"> -->
          <div>
            <div>
              <div>
                <div class="container " style="" >
                <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
                <div class="certifactebox" style="line-height:20px;">
                 <br>
                 <br>
                  
                  <h3 class="centerItem" style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-top:100px;"><u>Condition of Recognition</u></h3>
                  <p  style="font-family:Times New Roman;color:rgb(146, 3, 3);margin-bottom:100px;">
                    Recognition of this School is granted under provision specified in section 18 of The Right of <br> 
                    Children to Free and compulsory Education Act, 2009 & Jharkhand Right of Children to Free and <br> 
                     compulsory Education Rules 2011 , provided that the school will ensure that the provisions <br>  
                     given in The Right of Children to Free and compulsory Education Act, 2009, Jharkhand Right <br>  
                     of Children to Free and compulsory Education Rules 2011, Department notification no. 629 dated <br>  
                     25.04.2019 and other directions issued from time to time by the department are strictly adhered to. <br>  
                  </p>
                  

              </div>
              <div style="border:2px solid rgb(146, 3, 3);margin: 0px 80px;"></div>
                <div class="container " style="">
                <!-- <img src="assets/Certificate-01.jpg" alt="Snow" style="width:50%; height:150%;" > -->
                <div class="certifactebox" style="line-height:20px;">
                 
                  <br>
                     
                  <h3 class="centerItem" style="font-family:Times New Roman;font-size:29px;color:rgb(146, 3, 3);margin-bottom:0px;"><u>मान्यता की शर्त</u></h3>
                  <p id="hinditxt" style="font-family:NotoSansRegular;font-size:29px;color:rgb(146, 3, 3);margin-top:50px;margin-bottom:50px;">                 
                      इस स्कूल की मान्यता नि: शुल्क और अनिवार्य शिक्षा का अधिकार अधिनियम, 2009 की धारा 18 में निर्दिष्ट प्रावधान के तहत <br> 
                     दी गई है और झारखंड नि: शुल्क और अनिवार्य शिक्षा नियम 2011 के लिए बच्चों का अधिकार है, बशर्ते कि स्कूल यह सुनिश्चित <br> 
                     करेगा कि प्रावधानों में दिए गए प्रावधान नि: शुल्क और अनिवार्य शिक्षा का अधिकार अधिनियम, 2009, झारखंड बच्चों का नि: शुल्क <br> 
                      और अनिवार्य शिक्षा नियम 2011, विभाग की अधिसूचना संख्या 629 दिनांक 25.04.2019 और विभाग द्वारा समय-समय पर जारी <br>  
                      किए गए अन्य निर्देशों का कड़ाई से पालन किया जाता है ।
                  </p>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </page>


    <!-- [ Main Content ] end -->
  </div>
</div>
<script>
  document.getElementById("hinditxt").innerHTML="newtext";

</script>