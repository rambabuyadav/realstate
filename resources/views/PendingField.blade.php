@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div><!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">School Pendding</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="">School All Data Pendding by DSE / DC</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> --> 
          <div class="shadow-lg p-3 mt--6 bg-white rounded">
    
  <!-- general information -->
  <div class="card-body">
      <input type="hidden" name="id" id="school_id" value="">
    <table class="table table-bordered  ">
        <tr>
            <th>SL NO.</th>
            <th>FIELD NAME</th>
            <th>VALUE</th>
            
            <th> Remarks </th>
            <th> Status </th>
          
           
        </tr>
        <tr>
            <td class="si_no">12</td>
            <td class="lable_name">School Opening Date </td>
        <td class="field_name" style="display: none;"> opening_date </td>
                <td class="value_name">2020-12-24</td>
                                      <td>
            XYZ
        </td>
        <td>Pending</td>
                    </tr>
        <tr>
            <td class="si_no">13</td>
            <td class="lable_name">trust/Society/Management Committee Name</td>
        <td class="field_name" style="display: none;"> society_name </td>
                <td class="value_name"> sdfsf Academic Session From Which Recog sdfsf A</td>
                                      <td>
            XYZ
        </td>
        <td>Pending</td>
                    </tr>
        <tr>
            <td class="si_no">14</td>
            <td class="lable_name">is trust/society/management comittee registered?</td>
        <td class="field_name" style="display: none;"> is_society_registered </td>
                <td class="value_name">on</td>
                                      <td>
            XYZ
        </td>
        <td>Pending</td>
                    </tr>
        <tr>
            <td class="si_no">15</td>
            <td class="lable_name">The period until the registration of the Trust / Society / Management Committee is valid</td>
        <td class="field_name" style="display: none;"> society_registration_valid_upto </td>
                <td class="value_name"> 3242-04-23</td>
                                      <td>
            XYZ
        </td>
        <td>Pending</td>
                    </tr>
        <tr>
            <td class="si_no">16</td>
            <td class="lable_name">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of <br> members including their addresses on affidavit ?</td>
        <td class="field_name" style="display: none;"> evidence_of_non_proprietary_nature </td>
                <td class="value_name"> jkhjkhkhjk</td>
                                      <td>
            XYZ
        </td>
        <td>Pending</td>
                    </tr>
       
</table>
        
      </div>
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
</div>
@include('footer')
