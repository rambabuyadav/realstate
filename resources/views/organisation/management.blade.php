@include('home.header')
@php  //echo '<pre>'; print_r($data);//exit;
    $i=1;
    foreach($data as $value){
        
        if($value->page_type == 'Box1'){
            $box1Div = $value;
        }
        if($value->page_type == 'collapse'.$i){
            $collapseDivArr[$i] = $value;
            $i++;
        }
    }
    //echo '======<pre>'; print_r($box1Div);exit;
@endphp
<style>
    .list li{
        display: list-item;
    }
    .card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 20px 30px;
    margin: 10px 0px;
    border-radius: 4px;
    /* box-shadow: inset 1px -1px 10px rgb(109 109 109 / 28%); */
    border: 1px solid #e3e3e3;
    box-shadow: 1px 2px 3px #e7e7e7;
    background: #f3f3f3;
    /* border-top: 0px; */
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-left">{{$data[0]->page_name}}</h1>
        </div>
        <div class="col-md-12">
            <h4>{{$box1Div->heading}}</h4>
            <ul class="list list-unstyled">
                @php
                $box1_text = json_decode($box1Div->list_text);
                $box1_url = json_decode($box1Div->list_url);
                $box1_icon = json_decode($box1Div->list_icon);
                for($i=0;$i<count($box1_text);$i++){
                @endphp
                <li><a href="{{$box1_url[$i]}}" target="_blank"><i class="far {{$box1_icon[$i]}}"></i>&nbsp; {{$box1_text[$i]}}</a> </li>
                @php
                }
                @endphp
            </ul>
            <h4>{{$data[1]->heading}}</h4>
            
            <div class="accordion" id="accordionExample">
                @php 
                if(isset($data[1]->list_text)){ 
                    $list_text = json_decode($data[1]->list_text);//explode(",",$data[1]->list_text);
                    $list_icon = json_decode($data[1]->list_icon);//explode(",",$data[1]->list_icon);//print_r($list_icon);//exit;
                    foreach($list_text as $key => $value){
                @endphp
                    <div>
                        <div class="" id="heading{{$key+1}}">
                            <a href="" data-toggle="collapse" data-target="#collapse{{$key+1}}" aria-expanded="true" aria-controls="collapse{{$key+1}}"><i class="far {{$list_icon[$key]}}"></i>&nbsp; {{$value}}</a>
                        </div>
                    </div>
                    <!-- collapsable div -->
                    <div id="collapse{{$key+1}}" class="collapse" aria-labelledby="heading{{$key+1}}" data-parent="#accordionExample">
                        <div class="card-body">
                            <ul class="list list-unstyled">
                                
                                @isset($collapseDivArr[$key+1])
                                   
                                    @php 
                                    $text = json_decode($collapseDivArr[$key+1]->list_text);//explode(",",$collapseDivArr[$key+1]->list_text);
                                    $url  = json_decode($collapseDivArr[$key+1]->list_url);//explode(",",$collapseDivArr[$key+1]->list_url);
                                    $icon = json_decode($collapseDivArr[$key+1]->list_icon);//explode(",",$collapseDivArr[$key+1]->list_icon);
                                    if(isset($text)){
                                    for($i = 0; $i<count($text); $i++){
                                    @endphp
                                    <li><a href="{{$url[$i]??''}}" target="_blank"><i class="far {{$icon[$i]??''}}"></i>&nbsp; {{$text[$i]??''}}</a></li>
                                    @php }
                                    }
                                    @endphp
                                @endisset
                                
                                

                            </ul>
                        </div>
                    </div>
                    <!-- end collapsable div -->
                @php
                    }
                }
                @endphp
                
            </div>
        </div>
    </div>
</div>

