<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Private School Recognition System | School Registration</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
    <!-- <link rel="stylesheet" href="{{url('assets/css/registration.css')}}"> -->
    <script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
   
    <style>
        body {
            background-image: linear-gradient(to right, #1659c6, white);
        }
        .pay-box-container {
            position: relative;
            width: 100%;
            margin: 90px auto;
            background: #ffffff;
            height: 500px;
        }
        .pay-box-container h1 {
            text-align: center;
            font-size: 65px;
            padding-top: 120px;
        }
        .pay-box-container .check-icon {
            font-size: 50px;
            padding: 30px;
            border-radius: 50%;
            background: #fa6969;
            position: absolute;
            top: -50px;
            left: 45%;
            color:#ffffff;
        }
        .pay-box-container p {
            text-align: center;
            font-size: 20px;
        }
        .pay-box-container .btn-wrapper .arrow_left{
            padding-right:7px;
        }
        .btn-wrapper {
            display: flex;
            justify-content: center;
            border-radius: 0;
            padding: 18px;
            display: flex;
            justify-content: center;
           
        }
        @media (max-width:768px) {
            .pay-box-container .check-icon  {
                left: 33%;
            }
            .pay-box-container  h1{
                font-size:45px;
            }
            .pay-box-container p{
                font-size:15px;
            }
        }
    </style>
</head>
<body>
    <div class="pay-bg-wrapper">
        <div class="container">
            <div class="pay-box-container">
                <h1>Oops!! Payment Failed!</h1>
                <i class="check-icon fa fa-times"></i>
                <p>Please try different payment method</p>
                <div class="btn-wrapper">
                   <a href="https://rte.jharkhand.gov.in/dashboard"><button class="btn btn-danger" style="margin-right:15px;"> <i class="arrow_left fa fa-arrow-left"></i>BACK TO HOME</button></a>  <button class="btn btn-primary">  Pay Now</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>