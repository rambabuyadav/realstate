@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div><!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">School Verify</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="">School All Data Verify by DSE / DC</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> --> 
          <div class="shadow-lg p-3 mt--6 bg-white rounded">
    
  <!-- general information -->
  <div class="card-body">
      <input type="hidden" name="id" id="school_id" value="">
    <table class="table table-bordered  ">
        <tr>
            <th>SL NO.</th>
            <th>FIELD NAME</th>
            <th>VALUE</th>
            
            <th> Remarks </th>
            <th> Status </th>
          
           
        </tr>
        <tr>
            <td class="si_no">23</td>
            <td class="lable_name"> Year</td>
        <td class="field_name" style="display: none;"> session_year </td>
                <td class="value_name"> 2016-17</td>
                                      <td>
            Rejected
        </td><td>Rejected</td>
                    </tr>
        <tr>
            <td class="si_no">24</td>
            <td class="lable_name">Income</td>
        <td class="field_name" style="display: none;"> income </td>
                <td class="value_name">553534000 </td>
                                      <td>
            Rejected
        </td><td>Rejected</td>
                    </tr>
        <tr>
            <td class="si_no">25</td>
            <td class="lable_name">Expenses</td>
        <td class="field_name" style="display: none;"> expenses </td>
                <td class="value_name"> 3535350</td>
                                      <td>
            Rejected
        </td><td>Rejected</td>
                    </tr>
        <tr>
            <td class="si_no">26</td>
            <td class="lable_name">Surplus Money</td>
        <td class="field_name" style="display: none;"> surplus_money </td>
                <td class="value_name"> 5353530</td>
                                      <td>
            Rejected
        </td><td>Rejected</td>
                    </tr>
</table>
        
      </div>
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
</div>
@include('footer')
