@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Block</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('district')}}"><i class="fa fa-file" aria-hidden="true"></i>
                                <li class="breadcrumb-item"><a href="{{url('fndSettings')}}"> Settings</a></li>
                                <li class="breadcrumb-item"><a href="{{url('blockList')}}"> All Block</a></li>
                            <li class="breadcrumb-item"><a href="">Edit Block</a></li>
                        </ul>
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-12">
                    <form action="{{url('updateBlock')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <!-- school details -->
                        <div class="card">
                            <div class="card-header">Edit Block</div>
                            <div class="card-body">
                                <div class="container">
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">District : </label>
                                                <label for="">{{$Block_id->value_set_name}}</label>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Block Name</label>
                                                <input required type="text" name="block" class="form-control rte_input" value="{{@$Block_id->display_value}}">
                                                <input type="hidden" name="block_id" value="{{@$Block_id->value_id}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-4"></div>
                                        <div class="col-md-3 col-4">
                                            <input class="btn btn-primary btn_submit" type="submit" name="submit" value="UPDATE" style="float:right" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="division"]').on('change', function () {
            var stateID = $(this).val();
            var options = '<option value="-1">Select District </option>';
            if (stateID) {
                $.ajax({
                    url: '../DGR/block/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('Block ')
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            var id = data[i]['id'];
                            var value_set_name = data[i]['display_value'];
                            //do something with k or data...
                            options += '<option value="' + id + '">' + value_set_name + '</option>';
                        }
                        $('#district').html(options);
                    }
                });
                $('select[name="district"]').on('change', function () {
                    var stateID = $(this).val();
                    var options = '<option value="-1">Select Block  </option>';
                    if (stateID) {
                        $.ajax({
                            url: '../DGR/village/' + stateID,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                console.log('Village ');
                                console.log(data);
                                for (var i = 0; i < data.length; i++) {
                                    var id = data[i]['id'];
                                    var value_set_name = data[i]['display_value'];
                                    //do something with k or data...
                                    options += '<option value="' + id + '">' + value_set_name + '</option>';
                                }
                                $('#block').html(options);
                            }
                        });
                    } else {
                        $('#block').empty();
                    }
                });
                $('#UserType').on('change', function () {
                    var UserType = $(this).val();
                    console.log(UserType);
                    if (UserType == 'dse' || UserType == 'dc') {
                        $('#DivisionHide').show();
                        $('#DistrictHide').show();
                    }
                    else if (UserType == 'state') {
                        $('#DivisionHide').hide();
                        $('#DistrictHide').hide();
                    }
                    else if (UserType == 'saa' || UserType == 'faa') {
                        $('#DivisionHide').show();
                        $('#DistrictHide').hide();
                    }
                });
            });
</script>