@include('header')
@include('sidenav')
@include('topbar')
<style>
    .table td {
        padding: 4px !important;
    }
</style>
<!-- [ Header ] end -->
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <div class="page-header-title">
                            <h5 class="m-b-10">School Verify</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">Verification Status</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        @if(isset($Message) && count($Message) > 0)
                        <div style="display: none;" class=" message_History">
                            <!-- $Appeal['appeal_messages'][$i] -->
                            <div>
                                @foreach ($Message as $message => $message_list)
                                <label for="message-text" class=""><b>Date:</b> {{$message_list->created_at}}</label><br>
                                <label for="message-text" class=""><b>From:</b> {{$message_list->Sender_name}}</label><br>
                                <label for="message-text" class=""><b>To:</b> {{$message_list->Reciever_name}} </label><br>
                                <label for="message-text" class=""><b>Remarks : </b> {{$message_list->remarks}}</label><br>
                                <label for="message-text" class="">
                                    <!-- {{ $message_list }} -->
                                    @if(isset($message_list->attachments) && $message_list->attachments != null )
                                    @if( count($message_list->attachments) > 0)
                                    <table class="Uploaded_document_link">
                                        <tr class="">
                                            <td>
                                                @foreach ($message_list->attachments as $message_image_key => $message_image_list)
                                                @if($message_image_key > 0)
                                                ,&nbsp;
                                                @endif
                                                @if($message_list->sender_role == 'dc' || $message_list->sender_role == 'dse')
                                                <a href="https://rte.jharkhand.gov.in/public/assets/send_dse_to_school__correction/{{$message_image_list}}"><span>{{$message_image_list}}</span></a>
                                                @elseif($message_list->sender_role == 'school')
                                                <a href="https://rte.jharkhand.gov.in/public/assets/send_school_to_dc_dse_correction/{{$message_image_list}}"><span>{{$message_image_list}}</span></a>
                                                @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    </table>
                                    @endif
                                    @endif
                                </label>
                                <hr>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        @if(Auth::user()->user_role == 'school')
                        <span style="float: right;">
                            <form action="{{url('application-challan')}}" method="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="application_id" value="{{@$user->application_data_id}}">
                                <button style="margin-right: 10px;" type="submit" name="Challan" title="Print Challan " class="btn btn-sm btn-info pull-right"><i class="fa fa-credit-card"></i></button>
                            </form>
                        </span>
                        <button type="button" style="float: right;margin: 3px;" class="btn btn-warning  btn-sm" data-toggle="modal" data-target="#FailedPayment" title="Failed Payment">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button>
                        <button style="float: right;margin: 3px;" class="btn btn-danger message_history_to_school btn-sm" type="button" data-toggle="modal" data-target=".bd-message-history-school-modal-lg" title="Message History"><i class="fa fa-envelope" aria-hidden="true"></i>
                            <sup>
                                @if($Message->application_conversation_unread_count>0)
                                {{$Message->application_conversation_unread_count}}
                                @endif
                            
                            </sup>
                        </button>
                        <button style="float: right;margin: 3px;" class="btn btn-success send_message_dse btn-sm" type="button" data-toggle="modal" data-target=".bd-message-school-modal-lg" title="Message to School"><i class="fa fa-reply" aria-hidden="true"></i>
                        </button>
                        @endif
                        @if(@$user->application_status == 7 && @$user->application_status == 8 && @$user->application_status == 11 && @$user->application_status == 14 && @$user->application_status == 18 && @$user->application_status == 19 && @$user->application_status == 20 && @$user->application_status == 21 && @$user->application_status == 22)
                        <button type="submit" style="float: right;margin: 3px;" class="btn btn-warning re_apply_applictaion btn-sm" data-toggle="modal" data-target=".bd-re-apply-application-modal-lg" title="Re-apply application"><i class="fa fa-reply" aria-hidden="true"></i></button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg   mt--6 bg-white rounded">
                    <!-- general information -->
                    @if(Auth::user()->user_role == 'state')
                    <div class="card-header">
                        <a href="{{url('report')}}"><button style="float: right;" type="button" class="btn btn-danger btn-sm pull-right" title="Return to report module"><i class="fa fa-arrow-left" title="Return to report module"></i></button></a>
                    </div>
                    @endif
                    <input type="text" hidden value="{{@$user->application_data_id}}" name="applicationId" id="applicationId">
                    <input type="text" hidden value="{{@$user->schoolId}}" name="schoolId" id="schoolId">
                    <div class="card-body">
                        <input type="hidden" name="id" id="school_id" value="{{@$user->school_id}}">
                        <table class="table table-bordered table-sm table-responsive ">
                            <thead>
                                <th>SL NO.</th>
                                <th>FIELD NAME</th>
                                <th>VALUE</th>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <th> Remarks </th>
                                <th> File </th>
                                <th> Status </th>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <th style="width: 30px;">ACTION</th>
                                @endif
                            </thead>
                            <tr>
                                @php
                                $tot = 1;
                               
                                @endphp
                                <td class="si_no<td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">School Name</td>
                                <td class="field_name" style="display: none;"> school_name </td>
                                <td class="value_name">{{@$user->school_name}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->school_name_rmk}} </td>
                                <td>
                                    @if(isset($user->school_name_files))
                                    @php $user->school_name_files = json_decode($user->school_name_files, true); @endphp
                                    @foreach($user->school_name_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->school_name_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->school_name_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->school_name_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->school_name_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->school_name_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" disabled title="Already Approved" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->school_name_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" disabled  title="Already Rejected" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Academic Session From Which Recognition Proposed</td>
                                <td class="field_name" style="display: none;"> recognised_by </td>
                                <td class="value_name">{{@$user->recognised_by}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->recognised_by_rmk}} </td>
                                <td>
                                    @if(isset($user->recognised_by_files))
                                    @php $user->recognised_by_files = json_decode($user->recognised_by_files, true); @endphp
                                    @foreach($user->recognised_by_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->recognised_by_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->recognised_by_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->recognised_by_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->recognised_by_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->recognised_by_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->recognised_by_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">District</td>
                                <td class="field_name" style="display: none;"> district </td>
                                <td class="value_name">{{@$user->district}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->district_rmk}} </td>
                                <td>
                                    @if(isset($user->district_files))
                                    @php $user->district_files = json_decode($user->district_files, true); @endphp
                                    @foreach($user->district_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->district_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->district_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->district_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->district_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->district_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->district_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Post Office</td>
                                <td class="field_name" style="display: none;"> post_office </td>
                                <td class="value_name">{{@$user->post_office}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->post_office_rmk}} </td>
                                <td>
                                    @if(isset($user->post_office_files))
                                    @php $user->post_office_files = json_decode($user->post_office_files, true); @endphp
                                    @foreach($user->post_office_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->post_office_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->post_office_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->post_office_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->post_office_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->post_office_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->post_office_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Village / Town</td>
                                <td class="field_name" style="display: none;"> village_city </td>
                                <td class="value_name"> {{@$user->village_city}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->village_city_rmk}} </td>
                                <td>
                                    @if(isset($user->village_city_files))
                                    @php $user->village_city_files = json_decode($user->village_city_files, true); @endphp
                                    @foreach($user->village_city_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->village_city_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->village_city_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->village_city_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->village_city_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->village_city_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->village_city_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Pin Code</td>
                                <td class="field_name" style="display: none;"> pincode </td>
                                <td class="value_name">{{@$user->pincode}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->pincode_rmk}} </td>
                                <td>
                                    @if(isset($user->pincode_files))
                                    @php $user->pincode_files = json_decode($user->pincode_files, true); @endphp
                                    @foreach($user->pincode_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->pincode_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->pincode_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->pincode_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->pincode_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->pincode_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->pincode_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Ph./Mb no. With STD Code</td>
                                <td class="field_name" style="display: none;"> phone_with_std_code </td>
                                <td class="value_name"> {{@$user->phone_with_std_code}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->phone_with_std_code_rmk}} </td>
                                <td>
                                    @if(isset($user->phone_with_std_code_files))
                                    @php $user->phone_with_std_code_files = json_decode($user->phone_with_std_code_files, true); @endphp
                                    @foreach($user->phone_with_std_code_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->phone_with_std_code_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->phone_with_std_code_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->phone_with_std_code_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->phone_with_std_code_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->phone_with_std_code_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->phone_with_std_code_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">FAX No. With STD Code</td>
                                <td class="field_name" style="display: none;"> fax_with_std_code </td>
                                <td class="value_name"> {{@$user->fax_with_std_code}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->fax_with_std_code_rmk}} </td>
                                <td>
                                    @if(isset($user->fax_with_std_code_files))
                                    @php $user->fax_with_std_code_files = json_decode($user->fax_with_std_code_files, true); @endphp
                                    @foreach($user->fax_with_std_code_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->fax_with_std_code_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->fax_with_std_code_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->fax_with_std_code_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->fax_with_std_code_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->fax_with_std_code_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->fax_with_std_code_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Email ID</td>
                                <td class="field_name" style="display: none;"> email </td>
                                <td class="value_name">{{@$user->email}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->email_rmk}} </td>
                                <td>
                                    @if(isset($user->email_files))
                                    @php $user->email_files = json_decode($user->email_files, true); @endphp
                                    @foreach($user->email_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->email_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->email_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->email_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->email_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->email_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->email_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Nearest Police Station</td>
                                <td class="field_name" style="display: none;"> police_station </td>
                                <td class="value_name">{{@$user->police_station}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->police_station_rmk}} </td>
                                <td>
                                    @if(isset($user->police_station_files))
                                    @php $user->police_station_files = json_decode($user->police_station_files, true); @endphp
                                    @foreach($user->police_station_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->police_station_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->police_station_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->police_station_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->police_station_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->police_station_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->police_station_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="card-header pt-0 pb-0">
                                        General Information
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Establishment Year</td>
                                <td class="field_name" style="display: none;"> estd_year </td>
                                <td class="value_name">{{@$user->estd_year}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->estd_year_rmk}} </td>
                                <td>
                                    @if(isset($user->estd_year_files))
                                    @php $user->estd_year_files = json_decode($user->estd_year_files, true); @endphp
                                    @foreach($user->estd_year_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->estd_year_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->estd_year_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->estd_year_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->estd_year_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->estd_year_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->estd_year_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">School Opening Date </td>
                                <td class="field_name" style="display: none;"> opening_date </td>
                                <td class="value_name">{{@$user->opening_date}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->opening_date_rmk}} </td>
                                <td>
                                    @if(isset($user->opening_date_files))
                                    @php $user->opening_date_files = json_decode($user->opening_date_files, true); @endphp
                                    @foreach($user->opening_date_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->opening_date_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->opening_date_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->opening_date_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->opening_date_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->opening_date_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->opening_date_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">trust/Society/Management Committee Name</td>
                                <td class="field_name" style="display: none;"> society_name </td>
                                <td class="value_name"> {{@$user->society_name}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->society_name_rmk}} </td>
                                <td>
                                    @if(isset($user->society_name_files))
                                    @php $user->society_name_files = json_decode($user->society_name_files, true); @endphp
                                    @foreach($user->society_name_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->society_name_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->society_name_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->society_name_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->society_name_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->society_name_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->society_name_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Is trust/society/management comittee registered?</td>
                                <td class="field_name" style="display: none;"> is_society_registered </td>
                                <td class="value_name">{{@$user->is_society_registered}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->is_society_registered_rmk}} </td>
                                <td>
                                    @if(isset($user->is_society_registered_files))
                                    @php $user->is_society_registered_files = json_decode($user->is_society_registered_files, true); @endphp
                                    @foreach($user->is_society_registered_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->is_society_registered_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->is_society_registered_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->is_society_registered_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->is_society_registered_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->is_society_registered_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->is_society_registered_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">The period until the registration of the Trust / Society / Management Committee is valid</td>
                                <td class="field_name" style="display: none;"> society_registration_valid_upto </td>
                                <td class="value_name"> {{@$user->society_registration_valid_upto}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->society_registration_valid_upto_rmk}} </td>
                                <td>
                                    @if(isset($user->society_registration_valid_upto_files))
                                    @php $user->society_registration_valid_upto_files = json_decode($user->society_registration_valid_upto_files, true); @endphp
                                    @foreach($user->society_registration_valid_upto_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->society_registration_valid_upto_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->society_registration_valid_upto_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->society_registration_valid_upto_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->society_registration_valid_upto_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->society_registration_valid_upto_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->society_registration_valid_upto_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Valid Date </td>
                                <td class="value_name"> {{@$user->society_registration_valid_upto}}</td>
                                <td>{{@$user->society_registration_valid_upto_rmk}} </td>
                                <td style="vertical-align : middle;text-align:center;">
                                    <span>Accepted</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of <br> members including their addresses on affidavit ?</td>
                                <td class="field_name" style="display: none;"> evidence_of_non_proprietary_nature </td>
                                <td class="value_name" colspan="5"> {{@$user->evidence_of_non_proprietary_nature1}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Add Relevant Evidence Attachement</td>
                                <td class="field_name" style="display: none;"> evidence_of_non_proprietary_nature_ext </td>
                                <td class="value_name">
                                    @if(isset($user->evidence_of_non_proprietary_nature_ex))
                                    @foreach($user->evidence_of_non_proprietary_nature_ex as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->evidence_of_non_proprietary_nature_rmk}} </td>
                                <td>
                                    @if(isset($user->evidence_of_non_proprietary_nature_files))
                                    @php $user->evidence_of_non_proprietary_nature_file = json_decode($user->evidence_of_non_proprietary_nature_file, true); @endphp
                                    @foreach($user->evidence_of_non_proprietary_nature_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->evidence_of_non_proprietary_nature_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->evidence_of_non_proprietary_nature_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->evidence_of_non_proprietary_nature_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    @if(@$user->evidence_of_non_proprietary_nature_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->evidence_of_non_proprietary_nature_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->evidence_of_non_proprietary_nature_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td colspan="3"><b> Chairman Information</b></td>
                                <!-- @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif -->
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->school_chairman_name_rmk}} </td>
                                <td>
                                    @if(isset($user->school_chairman_name_files))
                                    @php $user->school_chairman_name_files = json_decode($user->school_chairman_name_files, true); @endphp
                                    @foreach($user->school_chairman_name_files as $key => $val)
                                    <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(@$user->school_chairman_name_vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->school_chairman_name_vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->school_chairman_name_vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    <span class="lable_name" style="display: none;">Chairman Information</span>
                                    <span class="field_name" style="display: none;">school_chairman_name</span>
                                    @if(@$user->school_chairman_name_vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->school_chairman_name_vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->school_chairman_name_vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Chairman Name </td>
                                <td class="field_name" style="display: none;"> school_chairman_name </td>
                                <td class="value_name" colspan="5"> {{@$user->school_chairman_name}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Designation</td>
                                <td class="field_name" style="display: none;"> school_chairman_post </td>
                                <td class="value_name" colspan="5">{{@$user->school_chairman_post}} </td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Address</td>
                                <td class="field_name" style="display: none;"> school_chairman_address </td>
                                <td class="value_name" colspan="5">{{@$user->school_chairman_address}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Phone No.</td>
                                <td class="field_name" style="display: none;"> school_chairman_phone_number </td>
                                <td class="value_name" colspan="5">{{@$user->school_chairman_phone_number}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Office</td>
                                <td class="field_name" style="display: none;"> school_chairman_office </td>
                                <td class="value_name" colspan="5"> {{@$user->school_chairman_office}}</td>
                            </tr>
                            <tr>
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name">Email ID</td>
                                <td class="field_name" style="display: none;"> school_chairman_email </td>
                                <td class="value_name" colspan="5">{{@$user->school_chairman_email}} </td>
                            </tr>
                            <tr>
                                <td colspan="7"><b> 3 Years Total Income / Expenses / Surplus / Loss</b></td>
                            </tr>
                            @php
                           
                            $Y = 1;
                            @endphp
                            @for($i=0,$y=1;$i < 3 ; $i++,$y++) <tr>
                             @php 
                             $field_name = 'i_e_s_r_l_year' .$y.'_ext';
                                $vrfy='i_e_s_r_l_year' .$y.'_vrfy'; 
                                $rmk='i_e_s_r_l_year' .$y.'_rmk'; 
                                $files='i_e_s_r_l_year' .$y.'_files'; 
                             @endphp
                                <td class="si_no">{{$tot++}}</td>
                                <td class="lable_name"> Year {{@$y}}</td>
                                <td class="field_name" style="display: none;"> {{$field_name}} </td>
                                <td class="value_name"> {{@$user->session_year[$i]}}
                                </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td>{{@$user->session_year_rmk}} </td>
                                <td>
                                </td>
                                <td>
                                    @if(@$user->$vrfy == 1 )
                                    <span>Pending</span>
                                    @elseif(@$user->$vrfy == 2 )
                                    <span>Approved</span>
                                    @elseif(@$user->$vrfy == 3 )
                                    <span>Rejected</span>
                                    @endif
                                </td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td rowspan="6">
                                    @if(@$user->$vrfy == 1 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->$vrfy == 2 )
                                    <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                    <span>Approved</span>
                                    <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                    @elseif(@$user->$vrfy == 3 )
                                    <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                    <span>Rejected</span>
                                    <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                    @endif
                                </td>
                                @endif
                                </tr>
                                <tr>
                                    <td class="si_no<td class=" si_no">{{$tot++}}</td>
                                    <td class="lable_name">Income</td>
                                    <td class="field_name" style="display: none;"> income </td>
                                    <td class="value_name">{{@$user->income[$i]}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->income_rmk}} </td>
                                    <td>
                                        @if(isset($user->income_files))
                                        @php $user->income_files = json_decode($user->income_files, true); @endphp
                                        @foreach($user->income_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        
                                    </td>
                                    @endif
                                   </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Expenses</td>
                                    <td class="field_name" style="display: none;"> expenses </td>
                                    <td class="value_name"> {{@$user->expenses[$i]}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->expenses_rmk}} </td>
                                    <td>
                                        @if(isset($user->expenses_files))
                                        @php $user->expenses_files = json_decode($user->expenses_files, true); @endphp
                                        @foreach($user->expenses_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        
                                    </td>
                                    @endif
                                   </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Surplus Money</td>
                                    <td class="field_name" style="display: none;"> surplus_money </td>
                                    <td class="value_name"> {{@$user->surplus_money[$i]}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->surplus_money_rmk}} </td>
                                    <td>
                                        @if(isset($user->surplus_money_files))
                                        @php $user->surplus_money_files = json_decode($user->surplus_money_files, true); @endphp
                                        @foreach($user->surplus_money_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        
                                    </td>
                                    @endif
                                   </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Reduce</td>
                                    <td class="field_name" style="display: none;"> reduced_money </td>
                                    <td class="value_name"> {{@$user->reduced_money[$i]}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->reduced_money_rmk}} </td>
                                    <td>
                                        @if(isset($user->reduced_money_files))
                                        @php $user->reduced_money_files = json_decode($user->reduced_money_files, true); @endphp
                                        @foreach($user->reduced_money_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        
                                    </td>
                                    @endif
                                   </tr>
                                <tr>
                                    @php
                                    $File_name = 'last_three_year_tot_income_files_'.$Y;
                                    @endphp
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> last_three_year_tot_income_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->$File_name))
                                        @foreach($user->$File_name as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->last_three_year_tot_income_rmk}} </td>
                                    <td>
                                        @if(isset($user->last_three_year_tot_income_files))
                                        @php $user->last_three_year_tot_income_files = json_decode($user->last_three_year_tot_income_files, true); @endphp
                                        @foreach($user->last_three_year_tot_income_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        
                                    </td>
                                    @endif
                                       @php
                                    $Y++;
                                    @endphp
                                </tr>
                                @endfor
                                <tr>
                                    <th colspan="4">
                                        <div class="card-header pb-0 pt-0">
                                            School Area and Format Details
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Medium Of Education</td>
                                    <td class="field_name" style="display: none;"> medium </td>
                                    <td class="value_name">{{@$user->medium}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->medium_rmk}} </td>
                                    <td>
                                        @if(isset($user->medium_files))
                                        @php $user->medium_files = json_decode($user->medium_files, true); @endphp
                                        @foreach($user->medium_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->medium_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->medium_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->medium_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->medium_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->medium_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->medium_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </td>
                                    <td class="field_name" style="display: none;"> type_of_school </td>
                                    <td class="value_name">{{@$user->type_of_school}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->type_of_school_rmk}} </td>
                                    <td>
                                        @if(isset($user->type_of_school_files))
                                        @php $user->type_of_school_files = json_decode($user->type_of_school_files, true); @endphp
                                        @foreach($user->type_of_school_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->type_of_school_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->type_of_school_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->type_of_school_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->type_of_school_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->type_of_school_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->type_of_school_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Agency Name (if school has support) </td>
                                    <td class="field_name" style="display: none;"> supported_agency_name </td>
                                    <td class="value_name">{{@$user->supported_agency_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->supported_agency_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->supported_agency_name_files))
                                        @php $user->supported_agency_name_files = json_decode($user->supported_agency_name_files, true); @endphp
                                        @foreach($user->supported_agency_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->supported_agency_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->supported_agency_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->supported_agency_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->supported_agency_name_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->supported_agency_name_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->supported_agency_name_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Support Percentage</td>
                                    <td class="field_name" style="display: none;"> agency_supported_percent </td>
                                    <td class="value_name"> {{@$user->agency_supported_percent}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->agency_supported_percent_rmk}} </td>
                                    <td>
                                        @if(isset($user->agency_supported_percent_files))
                                        @php $user->agency_supported_percent_files = json_decode($user->agency_supported_percent_files, true); @endphp
                                        @foreach($user->agency_supported_percent_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->agency_supported_percent_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->agency_supported_percent_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->agency_supported_percent_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->agency_supported_percent_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->agency_supported_percent_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->agency_supported_percent_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">If School Is Recognised Mention Authority Name</td>
                                    <td class="field_name" style="display: none;"> authority_name </td>
                                    <td class="value_name">{{@$user->authority_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->authority_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->authority_name_files))
                                        @php $user->authority_name_files = json_decode($user->authority_name_files, true); @endphp
                                        @foreach($user->authority_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->authority_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->authority_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->authority_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->authority_name_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->authority_name_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->authority_name_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Recognition Number</td>
                                    <td class="field_name" style="display: none;"> recognised_number </td>
                                    <td class="value_name">{{@$user->recognised_number}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->recognised_number_rmk}} </td>
                                    <td>
                                        @if(isset($user->recognised_number_files))
                                        @php $user->recognised_number_files = json_decode($user->recognised_number_files, true); @endphp
                                        @foreach($user->recognised_number_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->recognised_number_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->recognised_number_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->recognised_number_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->recognised_number_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->recognised_number_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->recognised_number_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">The School Has Its Own Building Or Is Working In A Rented Building ? </td>
                                    <td class="field_name" style="display: none;"> is_school_on_rented </td>
                                    <td class="value_name">{{@$user->is_school_on_rented}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->is_school_on_rented_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_on_rented_files))
                                        @php $user->is_school_on_rented_files = json_decode($user->is_school_on_rented_files, true); @endphp
                                        @foreach($user->is_school_on_rented_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->is_school_on_rented_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->is_school_on_rented_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->is_school_on_rented_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->is_school_on_rented_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_on_rented_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_on_rented_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_rent_deatail_ext </td>
                                    <td class="value_name">
                                        @if(@isset($user->school_rent_deatail))
                                        @foreach($user->school_rent_deatail as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_rent_deatail_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_rent_deatail_files))
                                        @php $user->school_rent_deatail_files = json_decode($user->school_rent_deatail_files, true); @endphp
                                        @foreach($user->school_rent_deatail_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_rent_deatail_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_rent_deatail_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_rent_deatail_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_rent_deatail_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_rent_deatail_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_rent_deatail_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development? </td>
                                    <td class="field_name" style="display: none;"> are_school_building_used </td>
                                    <td class="value_name">{{@$user->are_school_building_used}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->are_school_building_used_rmk}} </td>
                                    <td>
                                        @if(isset($user->are_school_building_used_files))
                                        @php $user->are_school_building_used_files = json_decode($user->are_school_building_used_files, true); @endphp
                                        @foreach($user->are_school_building_used_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->are_school_building_used_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->are_school_building_used_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->are_school_building_used_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->are_school_building_used_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->are_school_building_used_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->are_school_building_used_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Total Area Of School</td>
                                    <td class="field_name" style="display: none;"> school_total_area </td>
                                    <td class="value_name">{{@$user->school_total_area}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_total_area_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_total_area_files))
                                        @php $user->school_total_area_files = json_decode($user->school_total_area_files, true); @endphp
                                        @foreach($user->school_total_area_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_total_area_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_total_area_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_total_area_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_total_area_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_total_area_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_total_area_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Plot Number</td>
                                    <td class="field_name" style="display: none;"> plot_number </td>
                                    <td class="value_name">{{@$user->plot_number}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->plot_number_rmk}} </td>
                                    <td>
                                        @if(isset($user->plot_number_files))
                                        @php $user->plot_number_files = json_decode($user->plot_number_files, true); @endphp
                                        @foreach($user->plot_number_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->plot_number_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->plot_number_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->plot_number_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->plot_number_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->plot_number_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->plot_number_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                            
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Khata Number</td>
                                    <td class="field_name" style="display: none;"> khata_number </td>
                                    <td class="value_name">{{@$user->khata_number}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->khata_number_rmk}} </td>
                                    <td>
                                        @if(isset($user->khata_number_files))
                                        @php $user->khata_number_files = json_decode($user->khata_number_files, true); @endphp
                                        @foreach($user->khata_number_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->khata_number_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->khata_number_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->khata_number_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->khata_number_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->khata_number_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->khata_number_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                              
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Area Of School Building Only</td>
                                    <td class="field_name" style="display: none;"> school_building_area </td>
                                    
                                    <td class="value_name">{{@$user->school_building_area}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_building_area_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_building_area_files))
                                        @foreach($user->school_building_area_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_building_area_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_building_area_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_building_area_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_building_area_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_building_area_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_building_area_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                             
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Document For Fixed Deposit Of One Lakh Rupees In The Name Of School</td>
                                    <td class="field_name" style="display: none;"> one_lac_fd_proof_ext </td>
                                    <td class="value_name">
                                        @if($user->one_lac_fd_proof != null )
                                        @foreach($user->one_lac_fd_proof as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->one_lac_fd_proof_rmk}} </td>
                                    <td>
                                        @if(isset($user->one_lac_fd_proof_files))
                                        @foreach($user->one_lac_fd_proof_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->one_lac_fd_proof_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->one_lac_fd_proof_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->one_lac_fd_proof_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->one_lac_fd_proof_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->one_lac_fd_proof_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->one_lac_fd_proof_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_area_deatail_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_area_deatail))
                                        @foreach($user->school_area_deatail as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_area_deatail_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_area_deatail_files))
                                        @php $user->school_area_deatail_files = json_decode($user->school_area_deatail_files, true); @endphp
                                        @foreach($user->school_area_deatail_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_area_deatail_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_area_deatail_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_area_deatail_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_area_deatail_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_area_deatail_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_area_deatail_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <!-- Enrollment -->
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pb-0 pt-0 lable_name">
                                            Financial Details
                                        </div>
                                    </td>
                                    <td class="field_name" style="display: none;"> financial_details_ext </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->financial_details_rmk}} </td>
                                    <td>
                                        @if(isset($user->financial_details_files))
                                        @foreach($user->financial_details_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->financial_details_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->financial_details_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->financial_details_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->financial_details_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->financial_details_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->financial_details_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @if(isset($user->teaching_fee))
                                @foreach($user->teaching_fee as $key => $val)
                                <tr>
                                    <th colspan="7" class="value_name">{{@$user->class_finance[$key]}} </th>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Tuition Fee </td>
                                    <td class="field_name" style="display: none;"> teaching_fee </td>
                                    <td class="value_name" colspan="5">{{@$user->teaching_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Registration Fee </td>
                                    <td class="field_name" style="display: none;"> registration_fee </td>
                                    <td class="value_name" colspan="5">{{@$user->registration_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Admission Fee </td>
                                    <td class="field_name" style="display: none;"> enrollment_fee </td>
                                    <td class="value_name" colspan="5">{{@$user->enrollment_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Reserve Deposite </td>
                                    <td class="field_name" style="display: none;"> security_money </td>
                                    <td class="value_name" colspan="5">{{@$user->security_money[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Annual Charges </td>
                                    <td class="field_name" style="display: none;"> annual_charge </td>
                                    <td class="value_name" colspan="5">{{@$user->annual_charge[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Development Fee </td>
                                    <td class="field_name" style="display: none;"> development_fee </td>
                                    <td class="value_name" colspan="5">{{@$user->development_fee[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Other Charges </td>
                                    <td class="field_name" style="display: none;"> other_charges </td>
                                    <td class="value_name" colspan="5">{{@$user->other_charges[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <th class="lable_name">Total </th>
                                    <td class="field_name" style="display: none;"> total </td>
                                    <th class="value_name" colspan="5">{{@$user->total[$key]}} </th>
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pb-0 pt-0">
                                            Enrollment
                                        </div>
                                    </td>
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->pre_elementary_class_rmk}} </td>
                                    <td>
                                        @if(isset($user->pre_elementary_class_files))
                                        @php $user->pre_elementary_class_files = json_decode($user->pre_elementary_class_files, true); @endphp
                                        @foreach($user->pre_elementary_class_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->pre_elementary_class_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->pre_elementary_class_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->pre_elementary_class_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    <td>
                                        <span class="lable_name" style="display: none;">Enrollment</span>
                                        <span class="field_name" style="display: none;">pre_elementary_class</span>
                                        @if(@$user->pre_elementary_class_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->pre_elementary_class_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->pre_elementary_class_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @if(isset($user->class_names))
                                @foreach($user->class_names as $key => $val)
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">{{@$val}} </td>
                                    <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                    <td class="value_name" colspan="5">{{@$val}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Section </td>
                                    <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                    <td class="value_name" colspan="5">{{@$user->no_of_sections[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Numebr Of Student</td>
                                    <td class="field_name" style="display: none;"> no_of_students </td>
                                    <td class="value_name" colspan="5">{{@$user->no_of_students[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Teacher Student Ratio</td>
                                    <td class="field_name" style="display: none;"> teacher_student_ratio </td>
                                    <td class="value_name" colspan="5">{{@$user->teacher_student_ratio[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Teacher Student Assign</td>
                                    <td class="field_name" style="display: none;"> teacher_student_assign </td>
                                    <td class="value_name" colspan="5">{{@$user->teacher_student_assign[$key]}} </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Teacher</td>
                                    <td class="field_name" style="display: none;"> num_of_teacher </td>
                                    <td class="value_name" colspan="5">{{@$user->num_of_teacher[$key]}} </td>
                                </tr>
                                @endforeach
                                @endif
                                <!-- <tr>
                                <td class="si_no">38</td>
                                <td class="lable_name">Pre-elementary class</td>
                                <td class="field_name" style="display: none;"> pre_elementary_class </td>
                                <td class="value_name">{{@$user->pre_elementary_class}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">39</td>
                                <td class="lable_name">Number Of Section </td>
                                <td class="field_name" style="display: none;"> pre_no_of_section </td>
                                <td class="value_name">{{@$user->pre_no_of_section}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">40</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> pre_no_of_student </td>
                                <td class="value_name">{{@$user->pre_no_of_student}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">41</td>
                                <td class="lable_name">class 1 to 5</td>
                                <td class="field_name" style="display: none;"> class_one_to_five </td>
                                <td class="value_name">{{@$user->class_one_to_five}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">42</td>
                                <td class="lable_name">Number Of Section</td>
                                <td class="field_name" style="display: none;"> onefive_no_of_section </td>
                                <td class="value_name"> {{@$user->onefive_no_of_section}}</td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">43</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> onefive_no_of_student </td>
                                <td class="value_name">{{@$user->onefive_no_of_student}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">44</td>
                                <td class="lable_name">class 6 to 8 </td>
                                <td class="field_name" style="display: none;"> class_six_to_eight </td>
                                <td class="value_name">{{@$user->class_six_to_eight}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                   
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">45</td>
                                <td class="lable_name">Number Of Section</td>
                                <td class="field_name" style="display: none;"> sixeight_no_of_section </td>
                                <td class="value_name">{{@$user->sixeight_no_of_section}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                    
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <td class="si_no">46</td>
                                <td class="lable_name">Number Of Students</td>
                                <td class="field_name" style="display: none;"> sixeight_no_of_student </td>
                                <td class="value_name">{{@$user->sixeight_no_of_student}} </td>
                                @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                <td> </td>
                                <td></td>
                                @endif
                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <td>
                                   
                                </td>
                                @endif
                            </tr> -->
                                <!-- infrastructure details -->
                                <tr>
                                    <td colspan="4">
                                        <div class="card-header pt-0 pb-0">
                                            Infrastructure Details
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Classes </td>
                                    <td class="field_name" style="display: none;"> no_of_class </td>
                                    <td class="value_name">{{@$user->no_of_class}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_class_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_class_files))
                                        @php $user->no_of_class_files = json_decode($user->no_of_class_files, true); @endphp
                                        @foreach($user->no_of_class_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_class_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_class_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_class_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_class_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_class_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_class_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Classroom (in w*h)</td>
                                    <td class="field_name" style="display: none;"> avg_size_cls_room </td>
                                    <td class="value_name">{{@$user->avg_size_cls_room}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->avg_size_cls_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->avg_size_cls_room_files))
                                        @php $user->avg_size_cls_room_files = json_decode($user->avg_size_cls_room_files, true); @endphp
                                        @foreach($user->avg_size_cls_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->avg_size_cls_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->avg_size_cls_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->avg_size_cls_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->avg_size_cls_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_cls_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_cls_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Office Rooms</td>
                                    <td class="field_name" style="display: none;"> no_of_office_room </td>
                                    <td class="value_name">{{@$user->no_of_office_room}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_office_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_office_room_files))
                                        @php $user->no_of_office_room_files = json_decode($user->no_of_office_room_files, true); @endphp
                                        @foreach($user->no_of_office_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_office_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_office_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_office_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_office_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_office_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_office_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Office Rooms (in w*h)</td>
                                    <td class="field_name" style="display: none;"> avg_size_of_office_room </td>
                                    <td class="value_name">{{@$user->avg_size_of_office_room}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->avg_size_of_office_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->avg_size_of_office_room_files))
                                        @php $user->avg_size_of_office_room_files = json_decode($user->avg_size_of_office_room_files, true); @endphp
                                        @foreach($user->avg_size_of_office_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->avg_size_of_office_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->avg_size_of_office_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->avg_size_of_office_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->avg_size_of_office_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_office_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_office_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Store Rooms</td>
                                    <td class="field_name" style="display: none;"> no_of_store_room </td>
                                    <td class="value_name">{{@$user->no_of_store_room}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_store_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_store_room_files))
                                        @php $user->no_of_store_room_files = json_decode($user->no_of_store_room_files, true); @endphp
                                        @foreach($user->no_of_store_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_store_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_store_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_store_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_store_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_store_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_store_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Store Rooms (in w*h)</td>
                                    <td class="field_name" style="display: none;"> avg_size_of_store_room </td>
                                    <td class="value_name">{{@$user->avg_size_of_store_room}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->avg_size_of_store_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->avg_size_of_store_room_files))
                                        @php $user->avg_size_of_store_room_files = json_decode($user->avg_size_of_store_room_files, true); @endphp
                                        @foreach($user->avg_size_of_store_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->avg_size_of_store_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->avg_size_of_store_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->avg_size_of_store_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->avg_size_of_store_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_store_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_store_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Principal Rooms</td>
                                    <td class="field_name" style="display: none;"> no_of_princpal_room </td>
                                    <td class="value_name">{{@$user->no_of_princpal_room}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_princpal_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_princpal_room_files))
                                        @php $user->no_of_princpal_room_files = json_decode($user->no_of_princpal_room_files, true); @endphp
                                        @foreach($user->no_of_princpal_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_princpal_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_princpal_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_princpal_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_princpal_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_princpal_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_princpal_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Principal Rooms (in w*h)</td>
                                    <td class="field_name" style="display: none;"> avg_size_of_principal_room </td>
                                    <td class="value_name">{{@$user->avg_size_of_principal_room}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->avg_size_of_principal_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->avg_size_of_principal_room_files))
                                        @php $user->avg_size_of_principal_room_files = json_decode($user->avg_size_of_principal_room_files, true); @endphp
                                        @foreach($user->avg_size_of_principal_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->avg_size_of_principal_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->avg_size_of_principal_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->avg_size_of_principal_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->avg_size_of_principal_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_principal_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_principal_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Kitchen Rooms</td>
                                    <td class="field_name" style="display: none;"> no_of_kitchen_room </td>
                                    <td class="value_name">{{@$user->no_of_kitchen_room}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_kitchen_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_kitchen_room_files))
                                        @php $user->no_of_kitchen_room_files = json_decode($user->no_of_kitchen_room_files, true); @endphp
                                        @foreach($user->no_of_kitchen_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_kitchen_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_kitchen_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_kitchen_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_kitchen_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_kitchen_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_kitchen_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Average Size Of Kitchen Rooms (in w*h)</td>
                                    <td class="field_name" style="display: none;"> avg_size_of_kitchen_room </td>
                                    <td class="value_name">{{@$user->avg_size_of_kitchen_room}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->avg_size_of_kitchen_room_rmk}} </td>
                                    <td>
                                        @if(isset($user->avg_size_of_kitchen_room_files))
                                        @php $user->avg_size_of_kitchen_room_files = json_decode($user->avg_size_of_kitchen_room_files, true); @endphp
                                        @foreach($user->avg_size_of_kitchen_room_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->avg_size_of_kitchen_room_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->avg_size_of_kitchen_room_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->avg_size_of_kitchen_room_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->avg_size_of_kitchen_room_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_kitchen_room_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->avg_size_of_kitchen_room_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Does The School Has Boundary</td>
                                    <td class="field_name" style="display: none;"> is_school_has_boundary_ext </td>
                                    <td class="value_name">{{@$user->is_school_has_boundary}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->is_school_has_boundary_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_has_boundary_files))
                                        @php $user->is_school_has_boundary_files = json_decode($user->is_school_has_boundary_files, true); @endphp
                                        @foreach($user->is_school_has_boundary_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->is_school_has_boundary_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->is_school_has_boundary_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->is_school_has_boundary_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->is_school_has_boundary_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_has_boundary_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_has_boundary_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Size Of Boundry( W * H ) </td>
                                    <td class="field_name" style="display: none;"> size_of_school_boundary_ext </td>
                                    <td class="value_name">{{@$user->size_of_school_boundary}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->size_of_school_boundary_rmk}} </td>
                                    <td>
                                        @if(isset($user->size_of_school_boundary_files))
                                        @php $user->size_of_school_boundary_files = json_decode($user->size_of_school_boundary_files, true); @endphp
                                        @foreach($user->size_of_school_boundary_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->size_of_school_boundary_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->size_of_school_boundary_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->size_of_school_boundary_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->size_of_school_boundary_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->size_of_school_boundary_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->size_of_school_boundary_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_boundary_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_boundary_files))
                                        @foreach($user->school_boundary_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_boundary_files_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_boundary_files_files))
                                        @php $user->school_boundary_files_files = json_decode($user->school_boundary_files_files, true); @endphp
                                        @foreach($user->school_boundary_files_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_boundary_files_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_boundary_files_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_boundary_files_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_boundary_files_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_boundary_files_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_boundary_files_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Does School Has CCTV ? </td>
                                    <td class="field_name" style="display: none;"> is_school_cctv_ext </td>
                                    <td class="value_name">{{@$user->is_school_cctv}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->is_school_cctv_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_cctv_files))
                                        @php $user->is_school_cctv_files = json_decode($user->is_school_cctv_files, true); @endphp
                                        @foreach($user->is_school_cctv_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->is_school_cctv_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->is_school_cctv_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->is_school_cctv_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->is_school_cctv_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_cctv_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_cctv_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of CCTV </td>
                                    <td class="field_name" style="display: none;"> no_of_school_cctv_ext </td>
                                    <td class="value_name">{{@$user->no_of_school_cctv}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_school_cctv_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_school_cctv_files))
                                        @php $user->no_of_school_cctv_files = json_decode($user->no_of_school_cctv_files, true); @endphp
                                        @foreach($user->no_of_school_cctv_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_school_cctv_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_school_cctv_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_school_cctv_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_school_cctv_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_school_cctv_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_school_cctv_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_cctv_file_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_cctv_file))
                                        @foreach($user->school_cctv_file as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_cctv_file_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_cctv_file_files))
                                        @php $user->school_cctv_file_files = json_decode($user->school_cctv_file_files, true); @endphp
                                        @foreach($user->school_cctv_file_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_cctv_file_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_cctv_file_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_cctv_file_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_cctv_file_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_cctv_file_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_cctv_file_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Sport Ground ?
                                    </td>
                                    <td class="field_name" style="display: none;"> is_school_ground_ext </td>
                                    <td class="value_name">{{@$user->is_school_ground}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->is_school_ground_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_ground_files))
                                        @php $user->is_school_ground_files = json_decode($user->is_school_ground_files, true); @endphp
                                        @foreach($user->is_school_ground_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->is_school_ground_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->is_school_ground_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->is_school_ground_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->is_school_ground_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_ground_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_ground_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Size Of Sport Ground ( W * H )
                                    </td>
                                    <td class="field_name" style="display: none;"> size_of_school_ground_ext </td>
                                    <td class="value_name">{{@$user->size_of_school_ground}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->size_of_school_ground_rmk}} </td>
                                    <td>
                                        @if(isset($user->size_of_school_ground_files))
                                        @php $user->size_of_school_ground_files = json_decode($user->size_of_school_ground_files, true); @endphp
                                        @foreach($user->size_of_school_ground_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->size_of_school_ground_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->size_of_school_ground_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->size_of_school_ground_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->size_of_school_ground_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->size_of_school_ground_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->size_of_school_ground_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> school_ground_file_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_ground_file))
                                        @foreach($user->school_ground_file as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_ground_file_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_ground_file_files))
                                        @php $user->school_ground_file_files = json_decode($user->school_ground_file_files, true); @endphp
                                        @foreach($user->school_ground_file_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_ground_file_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_ground_file_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_ground_file_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_ground_file_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_ground_file_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_ground_file_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Dose The School Has 3 Start Rating In Water And Hygene Facilities ? </td>
                                    <td class="field_name" style="display: none;"> is_school_water_hygene_facilities_ext </td>
                                    <td class="value_name">{{@$user->is_school_water_hygene_facilities}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->is_school_water_hygene_facilities_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_water_hygene_facilities_files))
                                        @php $user->is_school_water_hygene_facilities_files = json_decode($user->is_school_water_hygene_facilities_files, true); @endphp
                                        @foreach($user->is_school_water_hygene_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->is_school_water_hygene_facilities_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->is_school_water_hygene_facilities_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->is_school_water_hygene_facilities_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->is_school_water_hygene_facilities_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_water_hygene_facilities_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_water_hygene_facilities_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> No. Of Water And Hygene Facilitie</td>
                                    <td class="field_name" style="display: none;"> no_of_school_water_hygene_facilities_ext </td>
                                    <td class="value_name">{{@$user->no_of_school_water_hygene_facilities}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_school_water_hygene_facilities_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_water_hygene_facilities_files))
                                        @php $user->is_school_water_hygene_facilities_files = json_decode($user->is_school_water_hygene_facilities_files, true); @endphp
                                        @foreach($user->is_school_water_hygene_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_school_water_hygene_facilities_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_school_water_hygene_facilities_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_school_water_hygene_facilities_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_school_water_hygene_facilities_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_school_water_hygene_facilities_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_school_water_hygene_facilities_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement </td>
                                    <td class="field_name" style="display: none;"> school_water_hygene_facilities_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_water_hygene_facilities_files))
                                        @foreach($user->school_water_hygene_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_water_hygene_facilities_files_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_water_hygene_facilities_files_files))
                                        @php $user->school_water_hygene_facilities_files_files = json_decode($user->school_water_hygene_facilities_files_files, true); @endphp
                                        @foreach($user->school_water_hygene_facilities_files_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_water_hygene_facilities_files_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_water_hygene_facilities_files_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_water_hygene_facilities_files_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_water_hygene_facilities_files_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_water_hygene_facilities_files_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_water_hygene_facilities_files_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Dose The School Has Fire Safty Facilities ? </td>
                                    <td class="field_name" style="display: none;"> is_school_fire_safty_facilities_ext </td>
                                    <td class="value_name">{{@$user->is_school_fire_safty_facilities}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->is_school_fire_safty_facilities_rmk}} </td>
                                    <td>
                                        @if(isset($user->is_school_fire_safty_facilities_files))
                                        @php $user->is_school_fire_safty_facilities_files = json_decode($user->is_school_fire_safty_facilities_files, true); @endphp
                                        @foreach($user->is_school_fire_safty_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->is_school_fire_safty_facilities_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->is_school_fire_safty_facilities_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->is_school_fire_safty_facilities_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->is_school_fire_safty_facilities_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_fire_safty_facilities_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->is_school_fire_safty_facilities_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Number Of fire safty
                                    </td>
                                    <td class="field_name" style="display: none;"> no_of_fire_safty_ext </td>
                                    <td class="value_name">{{@$user->no_of_fire_safty}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_fire_safty_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_fire_safty_files))
                                        @php $user->no_of_fire_safty_fi = json_decode($user->no_of_fire_safty_files, true); @endphp
                                        @foreach($user->no_of_fire_safty_fi as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_fire_safty_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_fire_safty_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_fire_safty_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_fire_safty_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_fire_safty_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_fire_safty_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement </td>
                                    <td class="field_name" style="display: none;"> school_fire_safty_facilities_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->school_fire_safty_facilities_files))
                                        @foreach($user->school_fire_safty_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->school_fire_safty_facilities_files_rmk}} </td>
                                    <td>
                                        @if(isset($user->school_fire_safty_facilities_files_files))
                                        @php $user->school_fire_safty_facilities_files_files = json_decode($user->school_fire_safty_facilities_files_files, true); @endphp
                                        @foreach($user->school_fire_safty_facilities_files_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->school_fire_safty_facilities_files_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->school_fire_safty_facilities_files_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->school_fire_safty_facilities_files_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->school_fire_safty_facilities_files_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_fire_safty_facilities_files_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->school_fire_safty_facilities_files_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>

                                <!-- other convienance -->
                                <tr>
                                    <td colspan="4">
                                        <div class="card-header pb-0 pt-0">
                                            Other Convenience
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Does all facilities have access without interrupted? </td>
                                    <td class="field_name" style="display: none;"> facilities_access_without_interrupted_ext </td>
                                    <td class="value_name"> {{@$user->facilities_access_without_interrupted}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->facilities_access_without_interrupted_rmk}} </td>
                                    <td>
                                        @if(isset($user->facilities_access_without_interrupted_files))
                                        @php $user->facilities_access_without_interrupted_files = json_decode($user->facilities_access_without_interrupted_files, true); @endphp
                                        @foreach($user->facilities_access_without_interrupted_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->facilities_access_without_interrupted_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->facilities_access_without_interrupted_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->facilities_access_without_interrupted_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->facilities_access_without_interrupted_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->facilities_access_without_interrupted_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->facilities_access_without_interrupted_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">List All Teaching Materials</td>
                                    <td class="field_name" style="display: none;"> all_teaching_material_list_ext </td>
                                    <td class="value_name"> {{@$user->all_teaching_material_list}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->all_teaching_material_list_rmk}} </td>
                                    <td>
                                        @if(isset($user->all_teaching_material_list_files))
                                        @php $user->all_teaching_material_list_files = json_decode($user->all_teaching_material_list_files, true); @endphp
                                        @foreach($user->all_teaching_material_list_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->all_teaching_material_list_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->all_teaching_material_list_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->all_teaching_material_list_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->all_teaching_material_list_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_teaching_material_list_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_teaching_material_list_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> all_teaching_material_list_files_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->all_teaching_material_list_files_ex))
                                        @foreach($user->all_teaching_material_list_files_ex as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->all_teaching_material_list_files_rmk}} </td>
                                    <td>
                                        @if(isset($user->all_teaching_material_list_files_files))
                                        @php $user->all_teaching_material_list_files_files = json_decode($user->all_teaching_material_list_files_files, true); @endphp
                                        @foreach($user->all_teaching_material_list_files_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->all_teaching_material_list_files_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->all_teaching_material_list_files_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->all_teaching_material_list_files_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->all_teaching_material_list_files_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_teaching_material_list_files_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_teaching_material_list_files_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">List All Sports Equipments</td>
                                    <td class="field_name" style="display: none;"> all_sports_equipment_list_ext </td>
                                    <td class="value_name"> {{@$user->all_sports_equipment_list}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->all_sports_equipment_list_rmk}} </td>
                                    <td>
                                        @if(isset($user->all_sports_equipment_list_files))
                                        @php $user->all_sports_equipment_list_files = json_decode($user->all_sports_equipment_list_files, true); @endphp
                                        @foreach($user->all_sports_equipment_list_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->all_sports_equipment_list_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->all_sports_equipment_list_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->all_sports_equipment_list_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->all_sports_equipment_list_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_sports_equipment_list_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_sports_equipment_list_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> all_sports_equipment_list_file_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->all_sports_equipment_list_file))
                                        @foreach($user->all_sports_equipment_list_file as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->all_sports_equipment_list_file_rmk}} </td>
                                    <td>
                                        @if(isset($user->all_sports_equipment_list_file_files))
                                        @php $user->all_sports_equipment_list_file_files = json_decode($user->all_sports_equipment_list_file_files, true); @endphp
                                        @foreach($user->all_sports_equipment_list_file_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->all_sports_equipment_list_file_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->all_sports_equipment_list_file_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->all_sports_equipment_list_file_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->all_sports_equipment_list_file_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_sports_equipment_list_file_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->all_sports_equipment_list_file_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th colspan="4">Books Facilities In Library</th>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Books In Library</td>
                                    <td class="field_name" style="display: none;"> books_ext </td>
                                    <td class="value_name"> {{@$user->books}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->books_rmk}} </td>
                                    <td>
                                        @if(isset($user->books_files))
                                        @php $user->books_files = json_decode($user->books_files, true); @endphp
                                        @foreach($user->books_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->books_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->books_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->books_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->books_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->books_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->books_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Newspaper & Magzine</td>
                                    <td class="field_name" style="display: none;"> magazines_ext </td>
                                    <td class="value_name"> {{@$user->magazines}}</td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->magazines_rmk}} </td>
                                    <td>
                                        @if(isset($user->magazines_files))
                                        @php $user->magazines_files = json_decode($user->magazines_files, true); @endphp
                                        @foreach($user->magazines_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->magazines_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->magazines_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->magazines_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->magazines_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->magazines_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->magazines_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevent evidence attachement</td>
                                    <td class="field_name" style="display: none;"> books_in_library_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->books_in_library))
                                        @foreach($user->books_in_library as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->books_in_library_rmk}} </td>
                                    <td>
                                        @if(isset($user->books_in_library_files))
                                        @php $user->books_in_library_files = json_decode($user->books_in_library_files, true); @endphp
                                        @foreach($user->books_in_library_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->books_in_library_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->books_in_library_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->books_in_library_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->books_in_library_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->books_in_library_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->books_in_library_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th colspan="4">Water Facilities</th>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Types Of Water Facilities</td>
                                    <td class="field_name" style="display: none;"> type_of_water_facilities_ext </td>
                                    <td class="value_name">{{@$user->type_of_water_facilities}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->type_of_water_facilities_rmk}} </td>
                                    <td>
                                        @if(isset($user->type_of_water_facilities_files))
                                        @php $user->type_of_water_facilities_files = json_decode($user->type_of_water_facilities_files, true); @endphp
                                        @foreach($user->type_of_water_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->type_of_water_facilities_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->type_of_water_facilities_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->type_of_water_facilities_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->type_of_water_facilities_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->type_of_water_facilities_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->type_of_water_facilities_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Water Supply</td>
                                    <td class="field_name" style="display: none;"> no_of_water_supply_ext </td>
                                    <td class="value_name">{{@$user->no_of_water_supply}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->no_of_water_supply_rmk}} </td>
                                    <td>
                                        @if(isset($user->no_of_water_supply_files))
                                        @php $user->no_of_water_supply_files = json_decode($user->no_of_water_supply_files, true); @endphp
                                        @foreach($user->no_of_water_supply_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->no_of_water_supply_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->no_of_water_supply_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->no_of_water_supply_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->no_of_water_supply_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_water_supply_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->no_of_water_supply_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> water_facilities_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->water_facilities))
                                        @foreach($user->water_facilities as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->water_facilities_rmk}} </td>
                                    <td>
                                        @if(isset($user->water_facilities_files))
                                        @php $user->water_facilities_files = json_decode($user->water_facilities_files, true); @endphp
                                        @foreach($user->water_facilities_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->water_facilities_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->water_facilities_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->water_facilities_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->water_facilities_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->water_facilities_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->water_facilities_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th colspan="4">Cleanliness Related Details</th>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Type Of Toilets</td>
                                    <td class="field_name" style="display: none;"> type_of_toilet_ext </td>
                                    <td class="value_name">{{@$user->type_of_toilet}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->type_of_toilet_rmk}} </td>
                                    <td>
                                        @if(isset($user->type_of_toilet_files))
                                        @php $user->type_of_toilet_files = json_decode($user->type_of_toilet_files, true); @endphp
                                        @foreach($user->type_of_toilet_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->type_of_toilet_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->type_of_toilet_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->type_of_toilet_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->type_of_toilet_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->type_of_toilet_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->type_of_toilet_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Seperate Toilet For Boys</td>
                                    <td class="field_name" style="display: none;"> gents_toilet_ext </td>
                                    <td class="value_name">{{@$user->gents_toilet}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->gents_toilet_rmk}} </td>
                                    <td>
                                        @if(isset($user->gents_toilet_files))
                                        @php $user->gents_toilet_files = json_decode($user->gents_toilet_files, true); @endphp
                                        @foreach($user->gents_toilet_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->gents_toilet_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->gents_toilet_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->gents_toilet_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->gents_toilet_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->gents_toilet_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->gents_toilet_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Number Of Seperate Toilet For Girls</td>
                                    <td class="field_name" style="display: none;"> ladies_toilet_ext </td>
                                    <td class="value_name">{{@$user->ladies_toilet}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->ladies_toilet_rmk}} </td>
                                    <td>
                                        @if(isset($user->ladies_toilet_files))
                                        @php $user->ladies_toilet_files = json_decode($user->ladies_toilet_files, true); @endphp
                                        @foreach($user->ladies_toilet_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->ladies_toilet_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->ladies_toilet_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->ladies_toilet_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->ladies_toilet_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->ladies_toilet_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->ladies_toilet_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Add Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> cleanliness_ext </td>
                                    <td class="value_name">
                                        @if(isset($user->cleanliness))
                                        @foreach($user->cleanliness as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->cleanliness_rmk}} </td>
                                    <td>
                                        @if(isset($user->cleanliness_files))
                                        @php $user->cleanliness_files = json_decode($user->cleanliness_files, true); @endphp
                                        @foreach($user->cleanliness_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->cleanliness_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->cleanliness_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->cleanliness_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->cleanliness_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->cleanliness_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->cleanliness_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pt-0 pb-0">
                                            Principal Specialties
                                        </div>
                                    </td>
                                    <td style="display: none;" class="lable_name"> Name of principal</td>
                                    <td class="field_name" style="display: none;"> principle_name_ext </td>
                                    <td style="display: none;" class="value_name">{{@$user->principle_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @php $user->principle_name_files = json_decode($user->principle_name_files, true); @endphp
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Name of principal</td>
                                    <td class="field_name" style="display: none;"> principle_name_ext </td>
                                    <td class="value_name">{{@$user->principle_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @php $user->principle_name_files = json_decode($user->principle_name_files, true); @endphp
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Father / Husband Or Wife Name</td>
                                    <td class="field_name" style="display: none;"> p_f_h_w_name_ext </td>
                                    <td class="value_name">{{@$user->p_f_h_w_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date Of Birth</td>
                                    <td class="field_name" style="display: none;"> p_date_of_birth_ext </td>
                                    <td class="value_name">{{@$user->p_date_of_birth}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Educational Qualification</td>
                                    <td class="field_name" style="display: none;"> p_education_qualification_ext </td>
                                    <td class="value_name">{{@$user->p_education_qualification}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trainee Qualification</td>
                                    <td class="field_name" style="display: none;"> trainee_qualification_ext </td>
                                    <td class="value_name">{{@$user->pri_trainee_qualification}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teaching Experience</td>
                                    <td class="field_name" style="display: none;"> teaching_experience_ext </td>
                                    <td class="value_name">{{@$user->pri_teaching_experience}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Class Handed Over</td>
                                    <td class="field_name" style="display: none;"> class_handed_over_ext </td>
                                    <td class="value_name">{{@$user->class_handed_over}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date Of Appointment</td>
                                    <td class="field_name" style="display: none;"> date_of_appointment_ext </td>
                                    <td class="value_name">{{@$user->pri_date_of_appointment}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trained Or Untrained</td>
                                    <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                    <td class="value_name">{{@$user->trained_untrained}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name">Relevant Evidence Attachement</td>
                                    <td class="field_name" style="display: none;"> educational_qualification_files </td>
                                    <td class="value_name">
                                        @if(isset($user->educational_qualification_files))
                                        @foreach($user->educational_qualification_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->principle_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->principle_name_files))
                                        @foreach($user->principle_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->principle_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->principle_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->principle_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pt-0 pb-0">
                                            Teachers Information
                                        </div>
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        <span class="lable_name" style="display: none;">Teacher</span>
                                        <span class="field_name" style="display: none;">teacher_name_ext</span>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @if(isset($user->application_teacher_data))
                                @foreach($user->application_teacher_data as $key => $val)
                                <tr>
                                    <th colspan="4">
                                        Tearcher {{$key+1}}
                                    </th>
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Name of Teacher</td>
                                    <td class="field_name" style="display: none;"> teacher_name </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->teacher_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teacher Father/Husband/Wife Name</td>
                                    <td class="field_name" style="display: none;"> teacher_f_h_w_name </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->teacher_f_h_w_name}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date Of Birth</td>
                                    <td class="field_name" style="display: none;"> date_of_birth </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->date_of_birth}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Education Qualification</td>
                                    <td class="field_name" style="display: none;"> education_qualification </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->education_qualification}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trainee Qualification</td>
                                    <td class="field_name" style="display: none;"> trainee_qualification </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->trainee_qualification}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teaching Experience</td>
                                    <td class="field_name" style="display: none;"> teaching_experience </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->teaching_experience}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Class Handed Over</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->cls_handed_over}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Date of Appointment</td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->date_of_appointment}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Trained or Untrained</td>
                                    <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                    <td class="value_name">{{$user->application_teacher_data[$key]->trained_or_untrained}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Teacher Educational Qualification Files</td>
                                    <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                    <td class="value_name">
                                        @if($user->application_teacher_data[$key]->teacher_educational_qualification_files != null)
                                        @php $user->application_teacher_data[$key]->teacher_educational_qualification_files = json_decode($user->application_teacher_data[$key]->teacher_educational_qualification_files, true); @endphp
                                        @foreach($user->application_teacher_data[$key]->teacher_educational_qualification_files as $key_1 => $val_1)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/')}}{{$val_1}}" target="_blank"> File {{$key_1+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->teacher_name_rmk}} </td>
                                    <td>
                                        @if(isset($user->teacher_name_files))
                                        @php $user->teacher_name_files = json_decode($user->teacher_name_files, true); @endphp
                                        @foreach($user->teacher_name_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->teacher_name_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->teacher_name_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->teacher_name_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td colspan="3">
                                        <div class="card-header pt-0 pb-0">
                                            Curriculum and Syllabus
                                        </div>
                                    </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->details_of_curriculum_rmk}} </td>
                                    <td>
                                        @if(isset($user->details_of_curriculum_files))
                                        @php $user->details_of_curriculum_files = json_decode($user->details_of_curriculum_files, true); @endphp
                                        @foreach($user->details_of_curriculum_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->details_of_curriculum_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                        <span class="lable_name" style="display: none;">Curriculum and Syllabus</span>
                                        <span class="field_name" style="display: none;">details_of_curriculum_ext</span>
                                        @if(@$user->details_of_curriculum_vrfy == 1 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->details_of_curriculum_vrfy == 2 )
                                        <!-- <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button> -->
                                        <span>Approved</span>
                                        <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button>
                                        @elseif(@$user->details_of_curriculum_vrfy == 3 )
                                        <button class="btn-success" data-toggle="modal" data-target=".exampleModal1"><i class="fa fa-check"></i></button>
                                        <span>Rejected</span>
                                        <!-- <button class="btn-danger" data-toggle="modal" data-target=".exampleModal2"><i class="fa fa-times"></i></button> -->
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @if(isset($user->details_of_curriculum))
                                @foreach($user->details_of_curriculum as $key => $val)
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Details Of Curriculum And Syllabus Adopted In Every Class(class 1 to 6)</td>
                                    <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                    <td class="value_name">{{@$val}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->details_of_curriculum_rmk}} </td>
                                    <td>
                                        @if(isset($user->details_of_curriculum_files))
                                        @php $user->details_of_curriculum_files = json_decode($user->details_of_curriculum_files, true); @endphp
                                        @foreach($user->details_of_curriculum_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->details_of_curriculum_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Method Of Inspection Of Students</td>
                                    <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                    <td class="value_name">{{@$user->method_of_inspection[$key]}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->details_of_curriculum_rmk}} </td>
                                    <td>
                                        @if(isset($user->details_of_curriculum_files))
                                        @foreach($user->details_of_curriculum_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->details_of_curriculum_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td class="si_no">{{$tot++}}</td>
                                    <td class="lable_name"> Are the students of the school expected to take any board examination till class 6 ?</td>
                                    <td class="field_name" style="display: none;"> trained_untrained_ext </td>
                                    <td class="value_name">{{@$user->school_board_exam_till_cls_eight}} </td>
                                    @if(Auth::user()->user_role=='school' || Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc' || Auth::user()->user_role=='faa' || Auth::user()->user_role=='saa' || Auth::user()->user_role=='state')
                                    <td>{{@$user->details_of_curriculum_rmk}} </td>
                                    <td>
                                        @if(isset($user->details_of_curriculum_files))
                                        @foreach($user->details_of_curriculum_files as $key => $val)
                                        <a href="{{url('https://rte.jharkhand.gov.in/public/assets/applications/dsc_verification/')}}{{$val}}" target="_blank"> File {{$key+1}}</a>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$user->details_of_curriculum_vrfy == 1 )
                                        <span>Pending</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 2 )
                                        <span>Approved</span>
                                        @elseif(@$user->details_of_curriculum_vrfy == 3 )
                                        <span>Rejected</span>
                                        @endif
                                    </td>
                                    @endif
                                    @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                    <td>
                                    </td>
                                    @endif
                                </tr>


                                @if(Auth::user()->user_role=='dse' && (@$user->application_status == 2 || @$user->application_status == 3))
                                <tr>
                                    <td colspan="3" style="text-align: right">
                                        @if( @$user->Accepted == @$user->total_vrfy_field )
                                        <button type="button" class="btn-success" data-toggle="modal" data-target="#AcceptApplication"> Accept</button>
                                        @endif
                                    </td>
                                    <td style="text-align: right">
                                        <button type="button" class="btn-danger" data-toggle="modal" data-target="#RejectApplication">Reject</button>
                                    </td>
                                </tr>
                                @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
</div>
<div class="modal fade exampleModal1" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{url('acceptSchoolFields')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="exampleModalLabel1">Accept </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label" id="acceptLabel">Remarks :</label>
                        <input type="text" hidden class="form-control" name="fieldName" id="acceptfieldName" />
                        <input type="hidden" class="form-control" name="schoolID" id="acceptschoolID" />
                        <input type="hidden" class="form-control" name="status" value="2" />
                        <input type="text" hidden value="" name="approve_applicationId" id="approve_applicationId">
                        <input type="text" hidden value="" name="approve_schoolId" id="approve_schoolId">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_reason[]" type="text" class="form-control" multiple />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade exampleModal2" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel2">Reject </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('rejectSchoolFields')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label" id="rejectLabel">Remarks :</label>
                        <input type="text" hidden class="form-control" name="fieldName" id="rejectfieldName" />
                        <input type="hidden" class="form-control" name="schoolID" id="rejectschoolID" />
                        <input type="hidden" class="form-control" name="status" value="3" />
                        <input type="text" hidden value="" name="reject_applicationId" id="reject_applicationId">
                        <input type="text" hidden value="" name="reject_schoolId" id="reject_schoolId">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_reason[]" multiple type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-message-school-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form method="post" action="send_message_to_dc_dse" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="exampleModalLabel1">Send meaasge to DSE/DC</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <label for="message-text" class="col-form-label">DC/DSE :</label>
                            <select class="form-control" name="send_to" required>
                                <option value="">Select Sending Person</option>
                                <option value="dc">District Committee </option>
                                <option value="dse">District Superintendent of Education </option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="message-text" class="col-form-label"> File :</label>
                            <input type="text" name="send_school_id" hidden id="send_school_id">
                            <input type="text" name="send_application_id" hidden id="send_application_id">
                            <input type="file" accept=".pdf, .jpg, .png, jpeg" multiple name="send_file_upload[]" class="form-control rounded-0" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="message-text" class="col-form-label"> Write Text Here :</label>
                            <textarea name="send_from_dsc_to_school" class="form-control rounded-0" rows="8" placeholder="Write Your Text Here...!!!"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-message-history-school-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel1">Messages History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="Record">
                </table>
            </div>
        </div>
    </div>
</div>
<!-- fro re-apply applictaion at 27-01-2021 anshuman   -->
<div class="modal fade bd-re-apply-application-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel1">Re-apply Application </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{url('re-apply-application')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="school_id" id="re_apply_school_id" />
                    <input type="hidden" name="application_id" id="re_apply_application_id" />
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_re_apply[]" multiple type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Reason :</label>
                        <textarea name="re_apply_reason" type="text" class="form-control"></textarea>
                    </div>
                    <input style="float: right;" type="submit" name="Submit" class="btn btn-primary" value="Submit" />
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="RejectApplication" tabindex="-1" role="dialog" aria-labelledby="RejectApplication" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="RejectApplication">Reject Application</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('application_fields_verified')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="status" value="0" />
                        <input type="text" hidden value="{{@$user->applicationId}}" name="verified_applicationId" id="verified_applicationId">
                        <input type="text" hidden value="{{@$user->schoolId}}" name="verified_schoolId" id="verified_schoolId">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_reason[]" multiple type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<div class="modal fade " id="AcceptApplication" tabindex="-1" role="dialog" aria-labelledby="RejectApplication" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="AcceptApplication">Accept Application</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('application_fields_verified')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="status" value="1" />
                        <input type="text" hidden value="{{@$user->applicationId}}" name="verified_applicationId" id="verified_applicationId">
                        <input type="text" hidden value="{{@$user->schoolId}}" name="verified_schoolId" id="verified_schoolId">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_reason[]" multiple type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="FailedPayment" tabindex="-1" role="dialog" aria-labelledby="FailedPayment" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="FailedPayment">Failed Payment </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm ">
                    <thead>
                        <!-- <th>amount</th> -->
                        <!-- <th>status</th> -->
                        <!-- <th>ifms_office_code</th> -->
                        <!-- <th>security_code</th> -->
                        <!-- <th>payment_status_message</th> -->
                        <th>Tersury Code </th>
                        <th>GRN </th>
                        <th>Transaction Date </th>
                        <th>Transaction Amount </th>
                        <th>Payment Mode </th>
                        <th>Payment </th>
                    </thead>
                    @if(isset($user->failed_payment))
                    @foreach($user->failed_payment as $key => $val)
                    <tr>
                        <input type="hidden" value="{{$user->failed_payment[$key]->id}}" class="id" name="id" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->school_id}}" class="school_id" name="school_id" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->application_id}}" class="application_id" name="application_id" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->amount}}" class="amount" name="amount" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->status}}" class="status" name="status" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->dept_id}}" class="dept_id" name="dept_id" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->receipt_head_code}}" class="receipt_head_code" name="receipt_head_code" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->depositor_name}}" class="depositor_name" name="depositor_name" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->dept_tran_id}}" class="dept_tran_id" name="dept_tran_id" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->depositor_id}}" class="depositor_id" name="depositor_id" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->pan_no}}" class="pan_no" name="pan_no" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->add_info_1}}" class="add_info_1" name="add_info_1" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->add_info_2}}" class="add_info_2" name="add_info_2" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->add_info_3}}" class="add_info_3" name="add_info_3" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->treas_code}}" class="treas_code" name="treas_code" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->ifms_office_code}}" class="ifms_office_code" name="ifms_office_code" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->security_code}}" class="security_code" name="security_code" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->response_url}}" class="response_url" name="response_url" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->response_status}}" class="response_status" name="response_status" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->payment_status_message}}" class="payment_status_message" name="payment_status_message" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->grn}}" class="grn" name="grn" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->cin}}" class="cin" name="cin" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->ref_no}}" class="ref_no" name="ref_no" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->txn_date}}" class="txn_date" name="txn_date" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->txn_amount}}" class="txn_amount" name="txn_amount" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->challan_url}}" class="challan_url" name="challan_url" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->pmode}}" class="pmode" name="pmode" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->add_info_4}}" class="add_info_4" name="add_info_4" />
                        <input type="hidden" value="{{$user->failed_payment[$key]->add_info_5}}" class="add_info_5" name="add_info_5" />

                        <td>{{$user->failed_payment[$key]->treas_code}}</td>
                        <td>{{$user->failed_payment[$key]->grn}}</td>
                        <td>{{$user->failed_payment[$key]->txn_date}}</td>
                        <td>{{$user->failed_payment[$key]->txn_amount}}</td>
                        <td>{{$user->failed_payment[$key]->pmode}}</td>
                        <td><button class="bg-success text-white Failed_Payment"> Pay </button> </td>
                    </tr>
                    @endforeach
                    @endif
                </table>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- <script src="../assets/js/vendor-all.min.js"></script>
    <script src="../assets/js/plugins/bootstrap.min.js"></script>
    <script src="../assets/js/ripple.js"></script>
    <script src="../assets/js/pcoded.min.js"></script> -->
@include('footer')
<!-- Apex Chart -->
<!-- <script src="../assets/js/plugins/apexcharts.min.js"></script> -->
<!-- custom-chart js -->
<!-- <script src="../assets/js/pages/dashboard-main.js"></script> -->
<script>
    $('.Failed_Payment').on('click', function (e) {
        // if(!confirm('Do you want to re-apply application ')){
        //       e.preventDefault();
        // }
        var id = $(this).closest("tr").find('.id').val();
        var school_id = $(this).closest("tr").find('.school_id').val();
        var application_id = $(this).closest("tr").find('.application_id').val();
        var security_code = $(this).closest("tr").find('.security_code').val();
        var response_url = $(this).closest("tr").find('.response_url').val();
        var response_status = $(this).closest("tr").find('.response_status').val();
        var dept_id = $(this).closest("tr").find('.dept_id').val();
        var receipt_head_code = $(this).closest("tr").find('.receipt_head_code').val();
        var depositor_name = $(this).closest("tr").find('.depositor_name').val();
        var dept_tran_id = $(this).closest("tr").find('.dept_tran_id').val();
        var depositor_id = $(this).closest("tr").find('.depositor_id').val();
        var amount = $(this).closest("tr").find('.amount').val();
        var pan_no = $(this).closest("tr").find('.pan_no').val();
        var add_info_1 = $(this).closest("tr").find('.add_info_1').val();
        var add_info_2 = $(this).closest("tr").find('.add_info_2').val();
        var add_info_3 = $(this).closest("tr").find('.add_info_3').val();
        var treas_code = $(this).closest("tr").find('.treas_code').val();
        var ifms_office_code = $(this).closest("tr").find('.ifms_office_code').val();
        var status = $(this).closest("tr").find('.status').val();
        var payment_status_message = $(this).closest("tr").find('.payment_status_message').val();
        var grn = $(this).closest("tr").find('.grn').val();
        var cin = $(this).closest("tr").find('.cin').val();
        var ref_no = $(this).closest("tr").find('.ref_no').val();
        var txn_date = $(this).closest("tr").find('.txn_date').val();
        var txn_amount = $(this).closest("tr").find('.txn_amount').val();
        var challan_url = $(this).closest("tr").find('.challan_url').val();
        var pmode = $(this).closest("tr").find('.pmode').val();
        var add_info_4 = $(this).closest("tr").find('.add_info_4').val();
        var add_info_5 = $(this).closest("tr").find('.add_info_5').val();
        // alert(add_info_5);
     
        $.ajax({
            url: 'failed_payment_paid',
            type: "POST",
            dataType: "json",
            data: {
                _token: '{!! csrf_token() !!}',
                'application_id': application_id,
                'dept_id': dept_id,
                'receipt_head_code': receipt_head_code,
                'depositor_name': depositor_name,
                'dept_tran_id': dept_tran_id,
                'depositor_id': depositor_id,
                'amount': amount,
                'pan_no': pan_no,
                'add_info_1': add_info_1,
                'add_info_2': add_info_2,
                'add_info_3': add_info_3,
                'treas_code': treas_code,
                'ifms_office_code': ifms_office_code,
                'status': status,
                'payment_status_message': payment_status_message,
                'grn': grn,
                'cin': cin,
                'ref_no': ref_no,
                'txn_date': txn_date,
                'txn_amount': txn_amount,
                'challan_url': challan_url,
                'pmode': pmode,
                'add_info_4': add_info_4,
                'add_info_5': add_info_5
            },
            success: function (encryption_code) {
                console.log('treasury ');
                console.log(encryption_code);
                
                $.ajax({
                    url: 'https://jkuberuat.gov.in/JEgras/JeGrasRestful.svc/SBIePayDoubleVerification',
                    type: "POST",
                    dataType: "json",
                    data: {
                        'EncryptTxt':encryption_code['encryption'],
                        'REQDEPTID':dept_id
                    },
                    success: function (data) {
                        console.log('success');
                        console.log(data);
                    }
                });
               
                // $('#encpt').val(data['encryption']);
            }
        });
    });
    $('.ChallanVerify').on('click', function (e) {
        // if(!confirm('Do you want to re-apply application ')){
        //       e.preventDefault();
        // }
        var id = $(this).closest("tr").find('.id').val();
        var school_id = $(this).closest("tr").find('.school_id').val();
        var security_code = $(this).closest("tr").find('.security_code').val();
        var dept_id = 'JEPC';
        var amount = $(this).closest("tr").find('.amount').val();
        var grn = $(this).closest("tr").find('.grn').val();
       
        $.ajax({
            url: 'challan_verify_paid',
            type: "POST",
            dataType: "json",
            data: {
                _token: '{!! csrf_token() !!}',
                'amount': amount,
                'grn': grn,
                'security_code':security_code
            },
            success: function (encryption_code) {
                console.log('treasury ');
                console.log(encryption_code);
                
                $.ajax({
                    url: 'https://finance.jharkhand.gov.in/jegrasDV/JeGrasRestful.svc/GrnEnquiryCommon',
                    type: "POST",
                    dataType: "json",
                    data: {
                        'EncryptTxt':encryption_code['encryption'],
                        'REQDEPTID':dept_id
                    },
                    success: function (data) {
                        console.log('success');
                        console.log(data);
                    }
                });
               
                // $('#encpt').val(data['encryption']);
            }
        });
    });
  
  
  
    $('.re_apply_applictaion').on('click', function (e) {
        // if(!confirm('Do you want to re-apply application ')){
        //       e.preventDefault();
        // }
        var re_apply_school_id = $('#school_id').val();
        $('#re_apply_school_id').val(re_apply_school_id);
        var re_apply_application_id = $('#applicationId').val();
        $('#re_apply_application_id').val(re_apply_application_id);
    });
    $(".btn-success").click(function () {
        var text = $(this).closest("tr").find(".lable_name").text();
        var field_name = $(this).closest("tr").find(".field_name").text();
        var school_id = $('#school_id').val();
        var applicationId = $('#applicationId').val();
        var schoolId = $('#schoolId').val();
        // console.log(text);
        // console.log(field_name);
        // console.log(school_id);
        $('#acceptLabel').text(text);
        $('#acceptfieldName').val(field_name);
        $('#acceptschoolID').val(school_id)
        $('#approve_applicationId').val(applicationId)
        $('#approve_schoolId').val(schoolId)
    });
    $(".btn-danger").click(function () {
        var text = $(this).closest("tr").find(".lable_name").text();
        var field_name = $(this).closest("tr").find(".field_name").text();
        var school_id = $('#school_id').val();
        var applicationId = $('#applicationId').val();
        var schoolId = $('#schoolId').val();
        // console.log(text);
        // console.log(field_name);
        // console.log(school_id);
        $('#rejectLabel').text(text);
        $('#rejectfieldName').val(field_name);
        $('#rejectschoolID').val(school_id)
        $('#reject_applicationId').val(applicationId)
        $('#reject_schoolId').val(schoolId)
    });
    // school massege conversion
    $(".message_history_to_school").click(function () {
        var applicationId = $('#applicationId').val();
        var schoolId = $('#school_id').val();
        $.ajax({
            url: 'update-message-status/' + applicationId,
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log('success ');
            }
        });
        //  console.log('clicked');
        $('#Record').html('Record Not Present');
        var trData = $('.message_History').html();
        //  console.log(trData);
        $('#Record').html(trData);
        //  trData = '';
    });
    $(".send_message_dse").click(function () {
        //  console.log('clicked');
        var applicationId = $('#applicationId').val();
        $('#send_application_id').val(applicationId);
    });
</script>