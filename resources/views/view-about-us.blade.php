@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
       <!-- [ breadcrumb ] start -->
       <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">About Us</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('district')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="fndSettings"> Settings</a></li>
                        <li class="breadcrumb-item"><a href="">About Us</a></li>
                    </ul>
                    @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-12">
                    <form action="{{url('Update-About-Us')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="id" value="{{@$About_us->id}}">
                        <!-- school details -->
                        <div class="card">
                            <div class="card-header">About Us</div>
                            <div class="card-body">
                                @include('message-flash')
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Content</b></label>
                                                <textarea  required type="text" name="text" class="form-control ckeditor  rte_input" cols="7" style="height: 100%;width: 100%;">
                                                    {{@$About_us->text}}
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Image</b></label>
                                                <input   type="file" value="{{@$About_us->image}}" name="image" accept=" .jpg, .png , .jpeg "  class="form-control ckeditor  rte_input" cols="7" style="height: 100%;width: 100%;" value="{{@$About_us->image}}">
                                                    <img src="{{@$About_us->image}}" />
                                              
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Name</b></label>
                                                <input  required type="text" value="{{@$About_us->name}}" name="name" class="form-control ckeditor  rte_input" cols="7" style="height: 100%;width: 100%;" value="{{@$About_us->name}}">
                                                    
                                              
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Designation</b></label>
                                                <input  required type="text" value="{{@$About_us->designation}}" name="designation" class="form-control ckeditor  rte_input" cols="7" style="height: 100%;width: 100%;" value="{{@$About_us->designation}}">
                                                    
                                              
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><b>Department</b></label>
                                                <input  required type="text" value="{{@$About_us->department}}" name="department" class="form-control ckeditor  rte_input" cols="7" style="height: 100%;width: 100%;" value="{{@$About_us->department}}">
                                                    
                                              
                                            </div>
                                        </div>
                                 
                                    <div class="row">
                                        <div class="col-md-9 col-4"></div>
                                        <div class="col-md-3 col-4">
                                            <input class="btn btn-primary btn_submit" type="submit" name="submit" value="Update" style="float:right" />
                                        </div>
                                    </div>
                              
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>