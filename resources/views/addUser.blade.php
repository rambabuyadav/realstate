@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">User</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('userlist')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="">Add User Details</a></li>
                        </ul>
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('addUser')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">User Details</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Full Name </label>
                                            <input required type="text" name="full_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">User Name </label>
                                            <input required type="text" name="user_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">User Role</label>
                                            <select class="form-control rte_input" name="usertype" id="UserType" required>
                                                <option value='-1'>Select User Role</option>
                                                <option value="dc">DC</option>
                                                <option value="dse">DSE</option>
                                                <option value="faa">FAA</option>
                                                <option value="saa">SAA</option>
                                                <option value="state">State</option>
                                                <!-- <option value="school">School</option> -->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Photo</label>
                                            <input required type="file" class="form-control rte_input" name="photo">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">E-Mail ID</label>
                                            <input required type="text" name="email" class="form-control rte_input" placeholder="example@gmail.com">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=""> Password</label>
                                            <input required type="password" name="password" class="form-control rte_input" placeholder="Example Test@1234">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Contact</label>
                                            <input required type="text" class="form-control rte_input" name="phonenumber1">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Address </label>
                                            <textarea type="text" class="form-control rte_input" name="address1" required> </textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">State </label>
                                            <!-- <input required type="text" class="form-control rte_input" name="state"> -->
                                            <select class="form-control rte_input" name="state" id="state" required>
                                                <!-- <option value='-1'>Select State </option> -->
                                                <option value="Jharkhand" selected>Jharkhand</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3" id="DivisionHide">
                                        <div class="form-group">
                                            <label for=""> Division </label>
                                            <select class="form-control rte_input" id="division" name="division" required>
                                                <option value="">--Select--</option>
                                                @foreach($Districts as $District)
                                                <option value="{{$District->id}}">{{$District->display_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3" id="DistrictHide">
                                        <div class="form-group">
                                            <label for=""> District </label>
                                            <select class="form-control rte_input" name="district" required id="district">
                                                <option value="">--Select--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <button class="btn btn-primary btn_submit pull-right">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->
@include('footer')
<script>
    $(document).ready(function () {
        $('select[name="division"]').on('change', function () {
            var stateID = $(this).val();
            var options = '<option value="">---Select---</option>';
            if (stateID) {
                $.ajax({
                    url: 'district/district_list/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('district ')
                        console.log(data);
                        Object.entries(data).forEach(entry => {
                            const [key, value] = entry;
                            options += '<option value="' + key + '">' + value + '</option>';
                            console.log(key, value);
                        });
                        $('#district').html(options);
                    }
                });
            } else {
                $('#district').empty();
            }
        });
        $('#UserType').on('change', function () {
            var UserType = $(this).val();
            console.log(UserType);
            if (UserType == 'dse' || UserType == 'dc') {
                $('#DivisionHide').show();
                $('#DistrictHide').show();
                document.getElementById("division").required = true;
                document.getElementById("district").required = true;
                // $('#BlockHide').hide();
            }
            else if (UserType == 'saa' || UserType == 'state') {
                $('#DivisionHide').hide();
                $('#DistrictHide').hide();
                document.getElementById("division").required = false;
                document.getElementById("district").required = false;
                // $('#BlockHide').hide();
            }
            // else if(UserType=='school') {
            //     $('#DivisionHide').show();
            //     $('#DistrictHide').show();
            //     // $('#BlockHide').show();
            // }
            else if (UserType == 'faa') {
                $('#DivisionHide').show();
                $('#DistrictHide').hide();
                document.getElementById("division").required = true;
                document.getElementById("district").required = false;
                // $('#BlockHide').hide();
            }
        });
    });
</script>