<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Private School Recognition System | School Registration</title>
	<link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
	<link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
	<link rel="stylesheet" href="{{url('assets/css/singlewindowregistration.css')}}">
	<script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
	<!-- CSS -->
	<!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500"> -->
	<!-- <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	<!-- <link rel="stylesheet" href="assets/css/form-elements.css"> -->
	<!-- <link rel="stylesheet" href="assets/school_register_assets/css/style.css"> -->
</head>
<body>
	<!-- Top content -->
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				@if( Session::has('message') )
				<div class="alert alert-success" role="alert">
					{{ Session::get('message') }}
				</div>
				@endif
				<form role="form" method="post" action="{{route('single_window_school_register.store')}}" id="registration-form" enctype="multipart/form-data">
					@csrf
					<fieldset style="opacity: 0.9;">
						<!-- <div class="row" style="opacity: 0.9;">
							
						</div> -->
						<div class="form-top">
							<div class="col-sm-12  text-center">
								<h1 style="color:black;"><strong>School Registration </strong> </h1>
							</div>
						</div>
						<div class="progress rounded-0">
						</div>
						<div class=" form-bottom">
							<div class="row col-md-12">
								<!-- <div class=" form-section"> -->
								<div class="col-12 {{ $errors->has('udise_code') ? 'has-error' : '' }}">
									<label class="col-form-label">Do you have U-DISE code ?</label>
									<input id="yes" type="radio" class="form-input" style="margin-right:10px" name="udise_code" value="Yes"><label class="col-form-label">Yes</label>
									<input id="no" type="radio" name="udise_code" class="form-input" value="No" style="margin-right:10px" checked><label class="col-form-label">No</label>
									@if ($errors->has('udise_code'))
									<span class="text-danger">{{ $errors->first('udise_code') }}</span>
									@endif
								</div>
								<!-- </div> -->
								<!-- <div class=" form-section"> -->
								<style>
									.box {
										display: none;
									}
								</style>
								<div id="U_dice_code" class="col-6 {{ $errors->has('udisecode_name') ? 'has-error' : '' }}  ">
									<label class="col-form-label {{ $errors->has('udisecode_name') ? 'has-error' : '' }} ">U-DISE Code </label>
									<div class="input-group">
										<input class="form-control {{ $errors->has('udisecode_name') ? 'has-error' : '' }}  " type="text" maxlength="11" id="udisecode_name" name="udisecode_name" value="{{ old('udisecode_name') }}" autofocus autocomplete="udisecode_name">
										@if ($errors->has('udisecode_name'))
										<span class="text-danger">{{ $errors->first('udisecode_name') }}</span>
										@endif
										<div class="input-group-append">	
											<span class="input-group-text" style="padding: 0px;line-height:0px;">
												<button type="button" style="padding: 0px;" class="btn" id="fetchUdiseDetails">fetch</button>
											</span>
										</div>
									</div>
								</div>
								<input class="form-control" type="text" hidden  name="single_window_id" value="{{@$data['id']}}">
								<input class="form-control" type="text" hidden  name="user_id" value="{{@$data['user_id']}}">
								<input class="form-control" type="text" hidden  name="school_id" value="{{@$data['school_id']}}">
								<div id="school_managment_option" class=" col-6 {{ $errors->has('school_managment') ? 'has-error' : '' }}">
									<label class="col-form-label">School Managment </label>
									
									<select class="form-control rte_input " name="school_managment" id="">
										<option value="">---Select--- </option>
										<option value="Code-5">Un-Aided Private (Recognised)</option>
										<option value="Code-8">Un-recognised</option>
									</select>
								</div>
								<!-- Code-5
								 
								
								
								 -->
								<!-- </div> -->
								<!-- <div class="row"> -->
								<div class="col-6 {{ $errors->has('school_name') ? 'has-error' : '' }}">
									<label class="col-form-label">School Name</label>
									<input class="form-control" type="text" id="school_name" name="school_name" value="{{ old('school_name') }}" required>
									<input class="form-control" type="text" hidden name="block_name" value="">
									<input class="form-control" type="text" hidden name="dist_name" value="">
									<input class="form-control" type="text" hidden name="intBoysTotalUrinals" value="">
									<input class="form-control" type="text" hidden name="intGirlsTotalUrinals" value="">
									<input class="form-control" type="text" hidden name="vchContact" value="">
									<input class="form-control" type="text" hidden name="vchEmail" value="">
									<input class="form-control" type="text" hidden name="vchHeadofsch" value="">
									<input class="form-control" type="text" hidden name="vchMediumName" value="">
									<input class="form-control" type="text" hidden name="vchPanchayatName" value="">
									<input class="form-control" type="text" hidden name="vchPin" value="">
									<input class="form-control" type="text" hidden name="vchSchoolName" value="">
									<input class="form-control" type="text" hidden name="vchUdiseCode" value="">
									<input class="form-control" type="text" hidden name="vchVillageName" value="">
									<input class="form-control" type="text" hidden name="vchYear" value="">
									<input class="form-control" type="text" hidden name="water_facility" value="">
									@if ($errors->has('school_name'))
									<span class="text-danger">{{ $errors->first('school_name') }}</span>
									@endif
								</div>
								<!-- </div> -->
								<!-- <div class="row"> -->
								@php
								$division_ids = DB::table('fnd_values')->where('fnd_values.value_set_id', 1)->pluck('id');
								$all_districts_list = DB::table('fnd_values')->whereIn('fnd_values.parent_value_id',$division_ids)->pluck('display_value','id');
								@endphp
								<div class="col-6 {{ $errors->has('district') ? 'has-error' : '' }}">
									<label class="col-form-label">District</label>
									<input class="form-control" type="text" id="dist_name_input" name="dist_name" disabled value="">
									<select class="form-control rte_input" name="district" id="dist_name_option" value="{{ old('district') }}">
										<option value="">---Select---</option>
										@foreach($all_districts_list as $key => $value)
										<option value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-6 {{ $errors->has('block') ? 'has-error' : '' }}">
									<label class="col-form-label">Block </label>
									<input class="form-control" type="text" id="block_name_input" name="block_name" disabled value="">
									<select class="form-control rte_input" name="block" id="block_name_option">
										<option value="">---Select--- </option>
									</select>
								</div>
								<!-- </div> -->
								<!-- <div class="row"> -->
								<div class="col-6 {{ $errors->has('user_address') ? 'has-error' : '' }}">
									<label class="col-form-label">Address</label>
									<input class="form-control" type="user_address" name="user_address" id="user_address" value="{{ old('user_address') }}" required>
									@if ($errors->has('user_address'))
									<span class="text-danger">{{ $errors->first('user_address') }}</span>
									@endif
								</div>
								<div class="col-6 {{ $errors->has('username') ? 'has-error' : '' }}">
									<label class="col-form-label">User Name </label>
									<input class="form-control" type="text" id="username" name="username" value="{{ old('username') }}" required>
									@if ($errors->has('username'))
									<span class="text-danger">{{ $errors->first('username') }}</span>
									@endif
								</div>
								<div class="col-6 {{ $errors->has('password') ? 'has-error' : '' }}">
									<label class="col-form-label">Password </label>
									<input class="form-control" type="password" id="password" name="password">
									@if ($errors->has('password'))
									<span class="text-danger">{{ $errors->first('password') }}</span>
									@endif
								</div>
								<div class="col-6 {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
									<label class="col-form-label">Confirm Password </label>
									<input class="form-control" type="password" id="password_confirmation" name="password_confirmation" required>
									@if ($errors->has('password_confirmation'))
									<span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
									@endif
								</div>
							</div>
							<div class=" col-md-12 row">
								<div class="col-6 {{ $errors->has('contact') ? 'has-error' : '' }}">
									<label class="col-form-label">Mobile </label>
									<input class="form-control" type="number" min="6000000000" max="9999999999" maxlength="10" minlength="10" id="contact" name="contact" value="{{ old('contact') }}" required>
									@if ($errors->has('contact'))
									<span class="text-danger">{{ $errors->first('contact') }}</span>
									@endif
								</div>
					
							</div>
							<div class=" col-md-12 row">
								<div class="col-6 {{ $errors->has('email') ? 'has-error' : '' }}">
									<label class="col-form-label">E-mail ID </label>
									<input class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" required>
									@if ($errors->has('email'))
									<span class="text-danger">{{ $errors->first('email') }}</span>
									@endif
								</div>
								<!-- <div class="col-6 row">
									<div class="col-9 {{ $errors->has('email_otp') ? 'has-error' : '' }}">
										<label class="col-form-label">Enter Email OTP to verify your Email ID </label>
										<input class="form-control" type="text" id="" name="email_otp" value="{{ old('email_otp') }}" required>
										@if ($errors->has('email_otp'))
										<span class="text-danger">{{ $errors->first('email_otp') }}</span>
										@endif
									</div>
									<div class="col-3">
										<label class="col-form-label"></label>
										<button type="button" id='' class="btn btn-next btn-warning" style="margin-top:10px;font-size: 12px;">Generate OTP</button>
									</div>
								</div> -->
							</div>
							<div class="row">
								<div class="col-md-2">
									<a href="{{url('/')}}" style="color:white !important;"><button type="button" style="text-align: center;margin-top:20px;" class="btn btn-primary rounded-0 addnew pull-left">Back</button></a>
								</div>
								<div class="col-md-8"></div>
								<div class="col-md-2"><button type="submit" style="text-align: center;margin-top:20px;" class="btn btn-primary rounded-0 addnew pull-right">Submit</button></div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- Javascript -->
	<script src="assets/school_register_assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/school_register_assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/school_register_assets/js/jquery.backstretch.min.js"></script>
	<script>
		jQuery(document).ready(function () {
			/*
				Fullscreen background
			*/
			// $.backstretch("assets/school_register_assets/img/backgrounds/4.jpg");
		});
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$("#registration-form1 .addnew").click(function () {
			if ($("input[name='email']").val() == "") {
				alert("Error: Email is required");
				return false;
			}
			if ($("input[name='school_name']").val() == "") {
				alert("Error: School name is required");
				return false;
			}
			if ($("input[name='first_name']").val() == "") {
				alert("Error: First name is required");
				return false;
			}
			if ($("input[name='last_name']").val() == "") {
				alert("Error: Last name is required");
				return false;
			}
			if ($("input[name='password']").val() == "") {
				alert("Error: password is required");
				return false;
			}
			if ($("input[name='password_confirmation']").val() == "") {
				alert("Error: password_confirmation is required");
				return false;
			}
			$.ajax({
				url: "{{route('school_register.store')}}",
				type: 'POST',
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false,
				success: function (data) {
					window.location.href = "{{url('/')}}";
				},
				error: function (data) {
				}
			});
		});
	</script>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script>
		//funtion to show block district wise
		$('select[name="district"]').on('change', function () {
			console.log('success');
			var stateID = $(this).val();
			var options = '<option value="-1">---select---</option>';
			if (stateID) {
				$.ajax({
					url: 'block_unauth/block_list/' + stateID,
					type: "GET",
					dataType: "json",
					success: function (data) {
						console.log('block ');
						console.log(data);
						Object.entries(data).forEach(entry => {
							const [key, value] = entry;
							options += '<option value="' + key + '">' + value + '</option>';
							console.log(key, value);
						});
						$('#block_name_option').html(options);
					}
				});
			} else {
				$('#block_name_option').empty();
			}
		});
		$('#generate_phone_otp').on('click', function () {
			console.log('success_1');
			var contact = $('#contact').val()+'_school';
			if (contact) {
				$.ajax({
					url: 'generateOTP/mobile/' + contact,
					type: "GET",
					dataType: "json",
					success: function (data) {
						console.log(data);
						alert(data['msg']);
						console.log('success_2');
					}
				});
			} else {
				console.log('failed');
			}
		});
	</script>
	<script>
		$(document).ready(function () {
			$('#U_dice_code').hide();
			$("#block_name_input").hide();
			$("#dist_name_input").hide();
			// $("#school_managment_option").hide();
			$('input[type="radio"]').click(function () {
				var inputValue = $(this).attr("value");
				if ($('#yes').is(":checked")) {
					$('#U_dice_code').show();
					document.getElementById("school_name").readOnly = true;
					$("#block_name_input").hide();
					$("#block_name_option").show();
					$("#dist_name_input").hide();
					$("#dist_name_option").show();
					// $("#school_managment_option").show();
				}
				if ($('#no').is(":checked")) {
					$('#U_dice_code').hide();
					document.getElementById("school_name").readOnly = false;
					
					$("#block_name_input").hide();
					$("#block_name_option").show()
					$("#dist_name_input").hide();
					$("#dist_name_option").show();
			// $("#school_managment_option").hide();
					
				}
			});
			$('#fetchUdiseDetails').on('click', function () {
				var udisecode = $('#udisecode_name').val();
				if (udisecode.match('^(20(0[1-9]|1[0-9]|2[0-4])(0[1-9]|[1-9][0-9])(0[1-9]|[1-9][0-9])(00[1-9]|0[1-9][0-9]|[1-9][0-9][0-9]))$')) {
					console.log('success_1');
					$.ajax({
						url: 'https://rte.jharkhand.gov.in/assets/api/evv-get-auth-token.php',
						type: "GET",
						success: function (data) {
							console.log(data);
							data = JSON.parse(data);
							if (data.status == 200) {
								console.log(data.result.auth_token);
								$.ajax({
									url: 'https://rte.jharkhand.gov.in/assets/api/u-dise-code.php',
									type: "POST",
									data: { 'token': data.result.auth_token, 'udisecode': udisecode },
									// headers: {"api-key": "TEIrVVdpZE5TWENMVVFmclJHTXZXSDJvS1IyckxzSzNTazRveGNGT3RQTT0="},
									success: function (data) {
										console.log('data')
										data = JSON.parse(data);
										if (data.status == 200) {
											console.log(data);
											$("input[name='school_name']").val(data.schoolData.vchSchoolName);
											$("input[name='block_name']").val(data.schoolData.block_name);
											$("input[name='dist_name']").val(data.schoolData.dist_name);
											$("input[name='intBoysTotalUrinals']").val(data.schoolData.intBoysTotalUrinals);
											$("input[name='intGirlsTotalUrinals']").val(data.schoolData.intGirlsTotalUrinals);
											$("input[name='contact']").val(data.schoolData.vchContact);
											$("input[name='email']").val(data.schoolData.vchEmail);
											$("input[name='vchContact']").val(data.schoolData.vchContact);
											$("input[name='vchEmail']").val(data.schoolData.vchEmail);
											$("input[name='vchHeadofsch']").val(data.schoolData.vchHeadofsch);
											$("input[name='vchMediumName']").val(data.schoolData.vchMediumName);
											$("input[name='vchPanchayatName']").val(data.schoolData.vchPanchayatName);
											$("input[name='vchPin']").val(data.schoolData.vchPin);
											$("input[name='vchSchoolName']").val(data.schoolData.vchSchoolName);
											$("input[name='vchUdiseCode']").val(data.schoolData.vchUdiseCode);
											$("input[name='vchVillageName']").val(data.schoolData.vchVillageName);
											$("input[name='vchYear']").val(data.schoolData.vchYear);
											$("input[name='water_facility']").val(data.schoolData.water_facility);
										}
										else {
											alert('No data Found against this U-Dise code.');
										}
									}
								});
							}
							else {
								alert('E-Vidya Vahini Server is not responding. please Try again later.');
							}
						}
					});
				} else {
					alert('You have entered an invalid U-Dise code.');
				}
			});
		});
	</script>
</body>
</html>