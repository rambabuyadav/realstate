@include('header')
@include('sidenav')
@include('topbar')
<style>
    .btnnext {
        margin: 5px 6px;
    }
    .btnPrevious {
        float: right !important;
        margin: 5px 6px;
    }
    .btnsavesubmit {
        border-radius: 30px;
    }
    .parent-title {
        text-align: left;
        font-size: 17px;
        color: #4680ff;
        margin-bottom: 15px;
    }
</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Application</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">Student Application</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 text-right pr-4">
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <div id="tabs" class="card">
                    <form action="{{url('addEdit/studentApplicationData')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <ul class="nav nav-tabs " id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active ui-tabs-panel" id="school-tab" data-toggle="tab" href="#school" role="tab" aria-controls="school" aria-selected="true">
                                    <h5>Basic Personal Information</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ui-tabs-panel ui-tabs-hide" id="parent-tab" data-toggle="tab" href="#parent" role="tab" aria-controls="parent" aria-selected="true">
                                    <h5>Parent Information</h5>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ui-tabs-panel ui-tabs-hide" id="academic-details-tab" data-toggle="tab" href="#academic-details" role="tab" aria-controls="academic-details" aria-selected="true">
                                    <h5>Academic Information</h5>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="school" role="tabpanel" aria-labelledby="school-tab">
                                <div class="card-body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                    <input type="hidden" name="id" value="{{@$students_data->id}}">
                                                    <label for="">Full Name </label>
                                                    <input type="text" maxlength="48" value="{{@$students_data->full_name}}" name="student_name" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Email Id</label>
                                                    <input type="text" name="email" maxlength="38" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Address </label>
                                                    <input type="text" value="{{@$students_data->address}}" name="address" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Post Office </label>
                                                    <input type="text" name="post_office" value="{{@$students_data->post_office}}" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Village / Town </label>
                                                    <input type="text" name="village_town" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Pin Code</label>
                                                    <input type="number" name="pin_code" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Nearest Police Station</label>
                                                    <input type="text" maxlength="20" name="police_station" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Religion</label>
                                                    <input type="text" maxlength="20" name="religion" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Caste</label>
                                                    <select name="caste" class="form-control rte_input">
                                                        <option value="">--Select--</option>
                                                        <option value="OBC">OBC</option>
                                                        <option value="ST">ST</option>
                                                        <option value="SC">SC</option>
                                                        <option value="Minority">Minority</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Gender</label>
                                                    <select name="gender" class="form-control rte_input">
                                                        <option value="">--Select--</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Blood Group</label>
                                                    <input type="text" maxlength="20" name="blood_group" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Disability if any</label>
                                                    <select name="disability" class="form-control rte_input">
                                                        <option value="">--Select--</option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Disability Certificate</label>
                                                    <input type="file" name="disability_certificate" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Birth Certificate</label>
                                                    <input type="file" name="birth_certificate" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Aadhaar Card</label>
                                                    <input type="file" name="aadhaar_card" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Passport Photo</label>
                                                    <input type="file" name="photo" class="form-control rte_input">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="parent" role="tabpanel" aria-labelledby="parent-tab">
                                <div class="card-body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 parent-title">
                                                <span>Father's Details</span>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Father Name </label>
                                                    <input type="text" maxlength="48" name="father_name" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Primary Mobile No. </label>
                                                    <input type="text" name="f_mobile_no_p" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Secondary Mobile No. </label>
                                                    <input type="text" name="f_mobile_no_s" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Blood Group </label>
                                                    <input type="text" name="f_blood_group" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Income </label>
                                                    <input type="text" name="f_income" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Income Certificate</label>
                                                    <input type="file" name="f_income_certificate" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Aadhaar Card</label>
                                                    <input type="file" name="f_aadhaar_card" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Ration Card</label>
                                                    <input type="file" name="ration_card" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Community Certificate</label>
                                                    <input type="file" name="community_certificate" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-12 parent-title">
                                                <span>Mother's Details</span>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Mother Name </label>
                                                    <input type="text" maxlength="48" name="mother_name" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Primary Mobile No. </label>
                                                    <input type="text" name="m_mobile_no_p" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Secondary Mobile No. </label>
                                                    <input type="text" name="m_mobile_no_s" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Blood Group </label>
                                                    <input type="text" name="m_blood_group" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Income </label>
                                                    <input type="text" name="m_income" maxlength="18" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Income Certificate</label>
                                                    <input type="file" name="m_income_certificate" class="form-control rte_input">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="academic-details" role="tabpanel" aria-labelledby="academic-details-tab">
                                <div class="card-body">
                                    <div class="container">
                                        <div class="row">
                                            <!-- <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Previous School Name </label>
                                                    <input type="text" maxlength="48" name="pre_school_name" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Last Class Studied </label>
                                                    <input type="text" maxlength="48" name="last_class_studied" class="form-control rte_input">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Last Class Studied Marksheet</label>
                                                    <input type="file" name="last_class_marksheet" class="form-control rte_input">
                                                </div>
                                            </div> -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Class Applying To </label>
                                                    <!-- <input type="text" maxlength="48" name="class_applying" class="form-control rte_input"> -->
                                                    <select name="class_applying" class="form-control rte_input">
                                                        <option value="">--Select--</option>
                                                        <option value="Pre Primary">Pre Primary</option>
                                                        <option value="Class 1">Class 1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row col-md-12">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Name of the School</label>
                                                        <input type="text" maxlength="100" name="new_school_name[]" class="form-control rte_input">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Address of the School</label>
                                                        <input type="text" maxlength="100" name="new_school_address[]" class="form-control rte_input">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Distance from your home</label>
                                                        <input type="text" maxlength="100" name="school_distance[]" class="form-control rte_input">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="col-md-2 pt-4">
                                                        <a href="javascript:void(0)" class="btn btn-success" onclick="addcurrandsyllabus()" id="addx">Add</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="curr_syllabus_row">
                                        </div>
                                        <div class="add_currandsyllabus_row">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10"></div>
                                            <!-- <button class="btn btn-success btn_submit mx-3 btnsavesubmit" name="save_as_data" value="2" type="submit">Save as Draft</button> -->
                                            <button class="btn btn-primary btn_submit btnsavesubmit" name="save_data" value="1" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-6">
                            <button class=" prevtab btn btn-primary" style="float: left;">Previous</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btnnext nexttab btn btn-primary" style="float: right;">Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')
<script>
    function bootstrapTabControl() {
        var i, items = $('.ui-tabs-panel');
        console.log(items.length);
        pane = $('.tab-pane');
        $('.prevtab').hide();
        $('.nexttab').on('click', function () {
            for (i = 0; i < items.length; i++) {
                if ($(items[i]).hasClass('active') == true) {
                    break;
                }
            }
            if (i < items.length - 1) {
                $(items[i]).removeClass('active');
                $(items[i + 1]).addClass('active');
                // for pane
                $(pane[i]).removeClass('show active');
                $(pane[i + 1]).addClass('show active');
                $('.prevtab').show();
            }
        });
        $('.prevtab').on('click', function () {
            for (i = 0; i < items.length; i++) {
                if ($(items[i]).hasClass('active') == true) {
                    break;
                }
            }
            if (i != 0) {
                // for tab
                $(items[i]).removeClass('active');
                $(items[i - 1]).addClass('active');
                // for pane
                $(pane[i]).removeClass('show active');
                $(pane[i - 1]).addClass('show active');
            }
        });
    }
    bootstrapTabControl();
</script>
<script>
    var i = 2;
    function AddclassIndv() {
        // var inp = $('#clsAdd');
        var html = `<div class="class_rows"><div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Class `+ i + `</label>
                                    <input  type="text" maxlength="11" name="className[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Number Of Section</label>
                                    <input  type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Number Of Students</label>
                                    <input  type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="row">
                            <button type="button" class="btn btn-danger mx-3" onclick="remove_class_row(this)">Remove</button>
                            </div>
                        </div></div>`;
        // $('<div id="clsAdd" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input  type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input  type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input  type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" onclick="remove_class_row(this)" id="remove1">Remove </a></div></div>').appendTo(inp);
        $(".add_class_row").append(html);
        i++;
        if (i > 8) {
            document.getElementById('addClass').style.visibility = 'hidden';
        }
    }
    function remove_class_row(e) {
        console.log(e);
        $(e).parents('.class_rows').remove();
        i--;
        if (i <= 8) {
            document.getElementById('addClass').style.visibility = 'visible';
        }
    }
    // $(document).ready(function () {
    //     // $('#addClass').onclick(function () {
    //     // });
    //     $('body').on('click', '#remove1', function () {
    //         i--;
    //         if (i <= 8) {
    //             // document.getElementById('addClass').style.visibility = 'visible';
    //         }
    //         console.log(i);
    //         $(this).parent('div').remove();
    //         var teacherDataId = i - 1;
    //         document.getElementById("hiddenTeacherId").value = teacherDataId;
    //     });
    // });
</script>
<script>
    var j = 2;
    function teacherRowAdd() {
        // var inp = $('#clsAdd');
        var html = `<div class="removeTeacherRow"><div class="row pt-3 px-3">
                        <div class="col-md-12">
                            <h5> Teacher Information </h5>
                        </div>
                        <hr>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Name Of Teacher</label>
                                <input  type="text" name="teacher_name[]" id="teacher_name" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Father / Husband Or Wife Name</label>
                                <input  type="text" name="teacher_f_h_w_name[]" id="teacher_f_h_w_name" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Date Of Birth</label>
                                <input  type="date" name="teacher_date_of_birth[]" id="teacher_date_of_birth" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Educational Qualification</label>
                                <input  type="text" name="teacher_education_qualification[]" id="teacher_education_qualification" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Trainee Qualification</label>
                                <input  type="text" name="teacher_trainee_qualification[]" id="teacher_trainee_qualification" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Teaching Experience</label>
                                <input  type="text" name="teacher_teaching_experience[]" id="teacher_teaching_experience" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Class Handed Over</label>
                                <input  type="text" name="teacher_class_handed_over[]" id="teacher_class_handed_over" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Date Of Appointment</label>
                                <input  type="date" name="teacher_appointment_date[]" id="teacher_appointment_date" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Trained Or Untrained</label>
                                <input  type="text" name="teacher_trained_or_untrained[]" id="teacher_trained_or_untrained" class="form-control rte_input">
                            </div>
                        </div>
                        <div class="col-md-3"> </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3 text-right pt-4">
                            <div>
                                <a href="javascript:void(0)" class="btn btn-danger" onclick="remove_teacher_row(this)" id="remove">Remove</a>
                            </div>
                        </div>
                    </div>`;
        // $('<div id="clsAdd" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input  type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input  type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input  type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" onclick="remove_class_row(this)" id="remove1">Remove </a></div></div>').appendTo(inp);
        $(".addTeacherRow").append(html);
        i++;
    }
    function remove_teacher_row(e) {
        $(e).parents('.removeTeacherRow').remove();
        i--;
    }
    // $(document).ready(function () {
    //     var i = 2;
    //     $('#add').click(function () {
    //         var inp = $('#dynamic_form_create');
    //         $('<div class="row pt-3 px-3" id="dynamic_form_create' + i + '"><div class="col-md-12"><h5> Teacher Information </h5></div><hr><div class="col-md-3"><div class="form-group"><label for="">Name Of Teacher</label><input  type="text" name="teacher_name' + i + '" id="teacher_name" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Father / Husband Or Wife Name</label><input  type="text" name="teacher_f_h_w_name' + i + '" id="teacher_f_h_w_name" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Date Of Birth</label><input  type="date" name="teacher_date_of_birth' + i + '" id="teacher_date_of_birth" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Educational Qualification</label><input  type="text" name="teacher_education_qualification' + i + '" id="teacher_education_qualification" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Trainee Qualification</label><input  type="text" name="teacher_trainee_qualification' + i + '" id="teacher_trainee_qualification" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Teaching Experience</label><input  type="text" name="teacher_teaching_experience' + i + '" id="teacher_teaching_experience" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Class Handed Over</label><input  type="text" name="teacher_class_handed_over' + i + '" id="teacher_class_handed_over" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Date Of Appointment</label><input  type="date" name="teacher_appointment_date' + i + '" id="teacher_appointment_date" class="form-control rte_input"></div></div><div class="col-md-3"><div class="form-group"><label for="">Trained Or Untrained</label><input  type="text" name="teacher_trained_or_untrained' + i + '" id="teacher_trained_or_untrained" class="form-control rte_input"></div></div><div class="col-md-3"></div><div class="col-md-3"></div><div class="col-md-3 text-right pt-4"></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" id="remove">Remove </a></div></div>').appendTo(inp);
    //         i++;
    //         var teacherDataId = i - 1;
    //         document.getElementById("hiddenTeacherId").value = teacherDataId;
    //     });
    //     $('body').on('click', '#remove', function () {
    //         i--;
    //         $(this).parent('div').remove();
    //         var teacherDataId = i - 1;
    //         document.getElementById("hiddenTeacherId").value = teacherDataId;
    //     });
    // });
</script>
<script>
    var k = 2;
    function addcurrandsyllabus() {
        // var inp = $('#clsAdd');
        var html = `<div class="curr_syllabus_row">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Name of the School</label>
                                    <input type="text" maxlength="100" name="new_school_name[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Address of the School</label>
                                    <input type="text" maxlength="100" name="new_school_address[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Distance from your home</label>
                                    <input type="text" maxlength="100" name="school_distance[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-2 pt-4">
                                    <a href="javascript:void(0)" class="btn btn-danger" onclick="remove_curr_syllabus_row(this)" >Remove</a>
                                </div> 
                            </div>
                        </div></div>`;
        // $('<div id="clsAdd" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input  type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input  type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input  type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" onclick="remove_class_row(this)" id="remove1">Remove </a></div></div>').appendTo(inp);
        $(".add_currandsyllabus_row").append(html);
        k++;
        if (k > 10) {
            document.getElementById('addx').style.visibility = 'hidden';
        }
    }
    function remove_curr_syllabus_row(e) {
        $(e).parents('.curr_syllabus_row').remove();
        k--;
        if (k <= 10) {
            document.getElementById('addClass').style.visibility = 'visible';
        }
    }
</script>
<script type="text/javascript">
    function clsSearch() {
        var fromCls = document.getElementById("from_cls");
        var toCls = document.getElementById("to_cls");
        var selectedText = fromCls.options[fromCls.selectedIndex].innerHTML;
        var fromcls = fromCls.value;
        var tocls = toCls.value;
        if (fromcls == '') {
            alert("Please choose the fromcls");
        }
        else if (tocls == '') {
            alert("Please choose the tocls");
        }
        else {
            $(".add_class_row").html('');
            for (i = fromcls; i <= tocls; i++) {
                var y;
                if (i == 0) {
                    continue;
                }
                if (i == -3) {
                    y = 'Pre Elementery';
                }
                if (i == -2) {
                    y = 'Nurcery';
                }
                if (i == -1) {
                    y = 'Kindergarder';
                }
                if (i == 1) {
                    y = 'Class 1';
                }
                if (i == 2) {
                    y = 'Class 2';
                }
                if (i == 3) {
                    y = 'Class 3';
                }
                if (i == 4) {
                    y = 'Class 4';
                }
                if (i == 5) {
                    y = 'Class 5';
                }
                if (i == 6) {
                    y = 'Class 6';
                }
                if (i == 7) {
                    y = 'Class 7';
                }
                if (i == 8) {
                    y = 'Class 8';
                }
                // if (i !== "") {
                //     document.getElementById('remove1').style.visibility = 'none';
                // }
                html = `<div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">` + y + `</label>
                                    <input type="text" maxlength="11" name="className[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Number Of Section</label>
                                    <input type="text" maxlength="10" name="SectionNo[]" class="form-control rte_input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Number Of Students</label>
                                    <input type="text" maxlength="10" name="studentNo[]" class="form-control rte_input">
                                </div>
                            </div>
                    </div>`;
                $(".add_class_row").append(html);
            }
            // alert("From Class: " + fromcls + " To Class: " + tocls);
            // $('body').on('click', '#remove1', function () {
            //     $(this).parent('div').remove();
            // });
        }
    }
</script>