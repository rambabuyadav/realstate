@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <!-- <h5 class="m-b-10">for School</h5> -->
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row pt-4">
            <div class="col-lg-12">
                <!-- support-section start -->
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card support-bar overflow-hidden">
                            <div class="card-body pb-0" style="height: 150px;">
                                <h2 class="text-c-blue">verification Status</h2>
                                <h2 class="m-0 text-right">47</h2>
                            </div>
                            <!-- <div id="support-chart"></div> -->
                            <div class="card-footer bg-primary text-white">
                                <div class="row text-center">
                                    <div class="col">
                                        <h4 class="m-0 text-white">10</h4>
                                        <span>Open</span>
                                    </div>
                                    <div class="col">
                                        <h4 class="m-0 text-white">5</h4>
                                        <span>Running</span>
                                    </div>
                                    <div class="col">
                                        <h4 class="m-0 text-white">3</h4>
                                        <span>Solved</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card support-bar overflow-hidden">
                            <div class="card-body pb-0" style="height: 150px;">
                                <h2 class="text-c-blue"> Total Accepted Field </h2>
                                <h2 class="m-0 text-right">79</h2>
                            </div>
                            <!-- <div  id="power-card-chart1"></div> -->
                            <div class="card-footer bg-primary text-white">
                                <div class="row text-center">
                                    <div class="col">
                                        <h4 class="m-0 text-white">10</h4>
                                        <span>Open</span>
                                    </div>
                                    <div class="col">
                                        <h4 class="m-0 text-white">5</h4>
                                        <span>Running</span>
                                    </div>
                                    <div class="col">
                                        <h4 class="m-0 text-white">3</h4>
                                        <span>Solved</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card support-bar overflow-hidden">
                            <div class="card-body pb-0" style="height: 150px;">
                                <h2 class="text-c-blue">Total  Rejected Field</h2>
                                <h2 class="m-0 text-right">48</h2>
                            </div>
                            <!-- <div  id="power-card-chart2"></div> -->
                            <div class="card-footer bg-primary text-white">
                                <div class="row text-center">
                                    <div class="col">
                                        <h4 class="m-0 text-white">10</h4>
                                        <span>Open</span>
                                    </div>
                                    <div class="col">
                                        <h4 class="m-0 text-white">5</h4>
                                        <span>Running</span>
                                    </div>
                                    <div class="col">
                                        <h4 class="m-0 text-white">3</h4>
                                        <span>Solved</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- support-section end -->
            </div>
        </div>
    
        <!-- [ Main Content ] end -->
    </div>
</div>
@include('footer')