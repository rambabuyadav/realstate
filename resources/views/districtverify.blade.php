@include('header')
@include('sidenav')
@include('topbar')
<style>
    .table td{
        padding:4px !important;
    }
</style>
<div class="pcoded-main-container">
    <div class="card">
        <div class="card-header">
            School Details
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive table-sm ">
                <tr>
                    <th>SL NO.</th>
                    <th>FIELD NAME</th>
                    <th>VALUE</th>
                    <th>ACTION</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>School Name</td>
                    <td>sanjiv malakar</td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Academic Session From Which Recognition Proposed</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>District</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Post Office</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Village / Town</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Pin Code</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Ph./Mb no. With STD Code</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>FAX No. With STD Code</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Email ID</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Nearest Police Station</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- general information -->
    <div class="card">
        <div class="card-header">
            General Information
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive table-sm">
                <tr>
                    <td>11</td>
                    <td>Establishment Year</td>
                    <td>1987 </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>School Opening Date </td>
                    <td> 02-5-1987</td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>trust/Society/Management Committee Name</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>is trust/society/management comittee registered?</td>
                    <td> No</td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>The period until the registration of the Trust / Society / Management Committee is valid</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of <br> members including their addresses on affidavit ?</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">School Chairman Information</td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>Chairman Name </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>Designation</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>Address</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>Phone No.</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>Office</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>22</td>
                    <td>Email ID</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">Last 3Years Total Income/Expenses/Surplus/Loss</td>
                </tr>
                <tr>
                    <td>23</td>
                    <td> Year</td>
                    <td> 2016-17</td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>24</td>
                    <td>Income</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>25</td>
                    <td>Expenses</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>26</td>
                    <td>Surplus Money</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>27</td>
                    <td>Reduce</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- School Area and Format Details -->
    <div class="card">
        <div class="card-header">
            School Area and Format Details
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive ">
                <tr>
                    <td>28</td>
                    <td>Medium Of Education</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>29</td>
                    <td>Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>30</td>
                    <td>Agency Name (if school has support) </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>31</td>
                    <td>Support Percentage</td>
                    <td> No</td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>32</td>
                    <td>If School Is Recognised Mention Authority Name</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>33</td>
                    <td>Recognition Number</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>34</td>
                    <td>The School Has Its Own Building Or Is Working In A Rented Building ? </td>
                    <td>Own </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>35</td>
                    <td>Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development? </td>
                    <td>Own </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>36</td>
                    <td>Total Area Of School</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>37</td>
                    <td>Area Of School Building Only</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- Enrollment -->
    <div class="card">
        <div class="card-header">
            Enrollment
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive table-sm  ">
                <tr>
                    <td>38</td>
                    <td>Pre-elementary class</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>39</td>
                    <td>Number Of Section </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>40</td>
                    <td>Number Of Students</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>41</td>
                    <td>class 1 to 5</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>42</td>
                    <td>Number Of Section</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>43</td>
                    <td>Number Of Students</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>44</td>
                    <td>class 6 to 8 </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>45</td>
                    <td>Number Of Section</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>46</td>
                    <td>Number Of Students</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- infrastructure details -->
    <div class="card">
        <div class="card-header">
            Infrastructure Details
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive table-sm">
                <tr>
                    <td>47</td>
                    <td>No. Of Classes </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>48</td>
                    <td>Average Size Of Classroom (in w*h)</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>49</td>
                    <td>No. Of Office Rooms</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>50</td>
                    <td>Average Size Of Office Rooms (in w*h)</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>51</td>
                    <td>No. Of Store Rooms</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>52</td>
                    <td>Average Size Of Store Rooms (in w*h)</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>53</td>
                    <td>No. Of Principal Rooms</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>54</td>
                    <td>Average Size Of Principal Rooms (in w*h)</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>55</td>
                    <td>No. Of Kitchen Rooms</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>56</td>
                    <td>Average Size Of Kitchen Rooms (in w*h)</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- other convienance -->
    <div class="card">
        <div class="card-header">
            Other Convenience
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive table-sm">
                <tr>
                    <td>57</td>
                    <td>Does all facilities have access without interrupted? </td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>58</td>
                    <td>List All Teaching Materials</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>59</td>
                    <td>List All Sports Equipments</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <th colspan="4">Books Facilities In Library</th>
                </tr>
                <tr>
                    <td>60</td>
                    <td>Number Of Books In Library</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>61</td>
                    <td>Number Of Newspaper & Magzine</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <th colspan="4">Water Facilities</th>
                </tr>
                <tr>
                    <td>62</td>
                    <td>Types Of Water Facilities</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>63</td>
                    <td>Number Of Water Supply</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <th colspan="4">Cleanliness Related Details</th>
                </tr>
                <tr>
                    <td>64</td>
                    <td>Type Of Toilets</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>65</td>
                    <td>Number Of Seperate Toilet For Boys</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>66</td>
                    <td>Number Of Seperate Toilet For Girls</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- teaching staff specialities -->
    <div class="card">
        <div class="card-header">
            Teaching Staff Specialties
        </div>
        <div class="card-body">
            <table class="table table-bordered  ">
                <tr>
                    <td>57</td>
                    <td>Name Of Principle</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>58</td>
                    <td>Father / Husband Or Wife Name</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>59</td>
                    <td>Date Of Birth</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>59</td>
                    <td>Educational Qualification</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>60</td>
                    <td>Trainee Qualification</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>61</td>
                    <td>Teaching Experience</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>61</td>
                    <td>Class Handed Over</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>62</td>
                    <td>Date Of Appointment</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>63</td>
                    <td>Trained Or Untrained</td>
                    <td> </td>
                    <td>
                        <button class="btn-success"><i class="fa fa-check"></i></button>
                        <button class="btn-danger"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>