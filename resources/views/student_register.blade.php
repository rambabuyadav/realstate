<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Private School Recognition System | Student Registration</title>
	<link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
	<link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
	<link rel="stylesheet" href="{{url('assets/css/registration.css')}}">
	<script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
</head>
<body>
	<!-- Top content -->
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				@if( Session::has('message') )
				<div class="alert alert-success" role="alert">
					{{ Session::get('message') }}
				</div>
				@endif
				<form role="form" method="post" action="{{route('student_register.store')}}" id="registration-form" enctype="multipart/form-data">
					@csrf
					<fieldset style="opacity: 0.9;">
						<div class="form-top">
							<div class="col-sm-12  text-center">
								<h1 style="color:black;"><strong>Student Registration </strong> </h1>
							</div>
						</div>
						<div class="progress rounded-0">
						</div>
						<div class=" form-bottom">
							<div class="row col-md-12">
								<div class="col-6 {{ $errors->has('student_name') ? 'has-error' : '' }}">
									<label class="col-form-label">Name</label>
									<input class="form-control" type="text" id="student_name" name="student_name" value="{{ old('student_name') }}" required>
									@if ($errors->has('student_name'))
									<span class="text-danger">{{ $errors->first('student_name') }}</span>
									@endif
								</div>
								@php
								$division_ids = DB::table('fnd_values')->where('fnd_values.value_set_id', 1)->pluck('id');
								$all_districts_list = DB::table('fnd_values')->whereIn('fnd_values.parent_value_id',$division_ids)->pluck('display_value','id');
								@endphp
								<div class="col-6 {{ $errors->has('district') ? 'has-error' : '' }}">
									<label class="col-form-label">District</label>
									<select class="form-control rte_input" name="district" id="district" value="{{ old('district') }}" required>
										<option value="">---Select---</option>
										@foreach($all_districts_list as $key => $value)
										<option value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-6 {{ $errors->has('block') ? 'has-error' : '' }}">
									<label class="col-form-label">Block </label>
									<select class="form-control rte_input" name="block" id="block" required>
										<option value="">---Select--- </option>
									</select>
								</div>
								
								<div class="col-6 {{ $errors->has('user_address') ? 'has-error' : '' }}">
									<label class="col-form-label">Address</label>
									<input class="form-control" type="text" name="user_address" id="user_address" value="{{ old('user_address') }}" required>
									@if ($errors->has('user_address'))
									<span class="text-danger">{{ $errors->first('user_address') }}</span>
									@endif
								</div>
								<div class="col-6">
									<label class="col-form-label">Post Office</label>
									<input class="form-control" type="text" name="post_office" id="post_office" value="{{ old('post_office') }}">
									
								</div>
								<div class="col-6 {{ $errors->has('username') ? 'has-error' : '' }}">
									<label class="col-form-label">User Name </label>
									<input class="form-control" type="text" id="username" name="username" value="{{ old('username') }}" required>
									@if ($errors->has('username'))
									<span class="text-danger">{{ $errors->first('username') }}</span>
									@endif
								</div>
								<div class="col-6 {{ $errors->has('password') ? 'has-error' : '' }}">
									<label class="col-form-label">Password </label>
									<input class="form-control" type="password" id="password" name="password">
									@if ($errors->has('password'))
									<span class="text-danger">{{ $errors->first('password') }}</span>
									@endif
								</div>
								<div class="col-6 {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
									<label class="col-form-label">Confirm Password </label>
									<input class="form-control" type="password" id="password_confirmation" name="password_confirmation" required>
									@if ($errors->has('password_confirmation'))
									<span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
									@endif
								</div>
							</div>
							<div class=" col-md-12 row">
								<div class="col-6 {{ $errors->has('contact') ? 'has-error' : '' }}">
									<label class="col-form-label">Mobile </label>
									<input class="form-control" type="text" id="contact" name="contact" value="{{ old('contact') }}" required>
									@if ($errors->has('contact'))
									<span class="text-danger">{{ $errors->first('contact') }}</span>
									@endif
								</div>
								<div class="col-6 row">
									<div class="col-9 {{ $errors->has('mobile_otp') ? 'has-error' : '' }}">
										<label class="col-form-label">Enter SMS OTP to verify your mobile no. </label>
										<input class="form-control" type="text" id="mobile_otp" name="mobile_otp" value="{{ old('mobile_otp') }}" required>
										@if ($errors->has('mobile_otp'))
										<span class="text-danger">{{ $errors->first('mobile_otp') }}</span>
										@endif
									</div>
									<div class="col-3">
										<label class="col-form-label"> </label>
										<button type="button" id='generate_phone_otp' class="btn btn-next btn-warning" style="margin-top: 10px;font-size: 12px;">Generate OTP</button>
									</div>
								</div>
							</div>
							<!-- <div class=" col-md-12 row">
								<div class="col-6 {{ $errors->has('email') ? 'has-error' : '' }}">
									<label class="col-form-label">E-mail ID </label>
									<input class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" required>
									@if ($errors->has('email'))
									<span class="text-danger">{{ $errors->first('email') }}</span>
									@endif
								</div>
								<div class="col-6 row">
									<div class="col-9 {{ $errors->has('email_otp') ? 'has-error' : '' }}">
										<label class="col-form-label">Enter Email OTP to verify your Email ID </label>
										<input class="form-control" type="text" id="" name="email_otp" value="{{ old('email_otp') }}" required>
										@if ($errors->has('email_otp'))
										<span class="text-danger">{{ $errors->first('email_otp') }}</span>
										@endif
									</div>
									<div class="col-3">
										<label class="col-form-label"></label>
										<button type="button" id='' class="btn btn-next btn-warning" style="margin-top:10px;font-size: 12px;">Generate OTP</button>
									</div>
								</div>
							</div> -->
							<div class="row">
								<div class="col-md-2">
									<a href="{{url('/')}}" style="color:white !important;"><button type="button" style="text-align: center;margin-top:20px;" class="btn btn-primary rounded-0 addnew pull-left">Back</button></a>
								</div>
								<div class="col-md-8"></div>
								<div class="col-md-2"><button type="submit" style="text-align: center;margin-top:20px;" class="btn btn-primary rounded-0 addnew pull-right">Submit</button></div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- Javascript -->
	<!-- <script src="assets/school_register_assets/js/jquery-1.11.1.min.js"></script> -->
	<script src="assets/school_register_assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/school_register_assets/js/jquery.backstretch.min.js"></script>
	<script>
		jQuery(document).ready(function () {
			/*
				Fullscreen background
			*/
			// $.backstretch("assets/school_register_assets/img/backgrounds/4.jpg");
		});
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$("#registration-form1 .addnew").click(function () {
			if ($("input[name='email']").val() == "") {
				alert("Error: Email is required");
				return false;
			}
			if ($("input[name='student_name']").val() == "") {
				alert("Error: Student name is required");
				return false;
			}
			if ($("input[name='first_name']").val() == "") {
				alert("Error: First name is required");
				return false;
			}
			if ($("input[name='last_name']").val() == "") {
				alert("Error: Last name is required");
				return false;
			}
			if ($("input[name='password']").val() == "") {
				alert("Error: password is required");
				return false;
			}
			if ($("input[name='password_confirmation']").val() == "") {
				alert("Error: password_confirmation is required");
				return false;
			}
			$.ajax({
				url: "{{route('student_register.store')}}",
				type: 'POST',
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false,
				success: function (data) {
					window.location.href = "{{url('/')}}";
				},
				error: function (data) {
				}
			});
		});
		//     $('#password, #password_confirmation').on('keyup', function () {
		//   if ($('#password').val() == $('#password_confirmation').val()) {
		//     $('#messageError').html('Password has  matched ').css('color', 'green');
		//   } else 
		//     $('#messageError').html('Password has not  matched and length should be at least 8 characters').css('color', 'red');
		// });
		// Check Email
		// Check Email
		//              $("#email").on('keyup',function(){ 
		//                       var email=$(this).val();
		//                        if(IsEmail(email)==false){
		//                           $('#invalid_email').css('color','red').html('Email-id is invalid');
		//                           return false;
		//                          }else
		//                          {
		//                          	 $('#invalid_email').hide();
		//                          }
		//                     $.ajax({
		//                         url: "{{url('check_email')}}",
		//                         type:'get',
		//                         data:{'email':email}, 
		//                         success:function(data){  
		//                             if(data.ChKval==1)
		//                             {
		//                             	 $('#Erroremail').css('color','red').text(data.Msg);
		//                             	  $('.addnew').prop('disabled', true);
		//                             	}else{
		//                             		$('#Erroremail').text('');
		//                             		 $('.addnew').prop('disabled', false);
		//                             	}
		//                         },
		//                         error:function (data) {
		//                           $('#Erroremail').text('');
		//                        }
		//                   });
		//       });
		//              function IsEmail(email) {
		// 	           var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		// 	            if(!regex.test(email)) {
		// 	             return false;
		// 	           }else{
		// 	           return true;
		// 	          }
		// }
	</script>
	<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
	<script>
		$(document).ready(function () {
			$('input[type="radio"]').click(function () {
				var inputValue = $(this).attr("value");
				var targetBox = $("." + inputValue);
				$(".box").not(targetBox).hide();
				$("input[name='student_name']").attr('disabled', 'disabled');
				$("input[name='district']").attr('disabled', 'disabled');
				$("input[name='block']").attr('disabled', 'disabled');
				$("input[name='user_address']").attr('disabled', 'disabled');
				$(targetBox).show();
			});
		});
		//funtion to show block district wise
		$('select[name="district"]').on('change', function () {
			console.log('success');
			var stateID = $(this).val();
			var options = '<option value="-1">---select---</option>';
			if (stateID) {
				$.ajax({
					url: 'block_unauth/block_list/' + stateID,
					type: "GET",
					dataType: "json",
					success: function (data) {
						console.log('block ');
						console.log(data);
						Object.entries(data).forEach(entry => {
							const [key, value] = entry;
							options += '<option value="' + key + '">' + value + '</option>';
							console.log(key, value);
						});
						$('#block').html(options);
					}
				});
			} else {
				$('#block').empty();
			}
		});
		$('#generate_phone_otp').on('click', function () {
			console.log('success_1');
			var contact = $('#contact').val()+'_student';
			if (contact) {
				$.ajax({
					url: 'generateOTP/mobile/' + contact,
					type: "GET",
					dataType: "json",
					success: function (data) {
						console.log(data);
						console.log('success_2');
					}
				});
			} else {
				console.log('failed');
			}
		});
	</script>
	<script>
		$(document).ready(function() {
			$('input[type="radio"]').click(function() {
				var inputValue = $(this).attr("value");
				var targetBox = $("." + inputValue);
				$(".box").not(targetBox).show();
				if ($('#yes').is(":checked")) {
					// $(".box").not(targetBox).show();
					$("input[name='udisecode_name']").removeAttr("disabled");
					$("input[name='student_name']").removeAttr("disabled");
					$("input[name='district']").removeAttr("disabled");
					$("input[name='block']").removeAttr("disabled");
					$("input[name='user_address']").removeAttr("disabled");
				} else {
					$("input[name='udisecode_name']").attr("disabled");
					$("input[name='student_name']").attr("disabled", "disabled");
					$("input[name='district']").attr("disabled", "disabled");
					$("input[name='block']").attr("disabled", "disabled");
					$("input[name='user_address']").attr("disabled", "disabled");
				}
			});
		});
	</script>
	<!-- %%%%%%%%%%%%%%%%%% For Form Validation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
	<script>
		$().ready(function() {
	
			$("#registration-form").validate({
	
				errorClass: 'error_help_inline',
				validClass: 'success',
				errorElement: 'span',
				highlight: function(element, errorClass, validClass) {
					$(element).parents("div.control-group").addClass(errorClass).removeClass(validClass);
	
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents(".error").removeClass(errorClass).addClass(validClass);
					$(".error_help_inline").css('color', "red");
				},
				rules: {
					student_name: {
						required: true,
						minlength: 3,
						lettersonly: true
					},
					district: {
						minlength: 3,
						lettersonly: true
					},
					block: {
						required: true,
					},
					user_address: {
						required: true,
						minlength: 20,
						lettersonly: true
					},
					post_office: {
						required: true,
						minlength: 20,
						lettersonly: true
					},
					username: {
						required: true,
						minlength: 3,
						lettersonly: true,
						unameregex: true
					},
					password: {
						required: true,
						minlength: 3,
						pswdregex: true
					},
					password_confirmation: {
						required: true,
						lettersonly: true
					},
					contact: {
						required: true,
						minlength: 10,
						mobileNumber: true
					},
					mobile_otp: {
						required: true,
						minlength: 4,
						maxlength: 8
					}
	
	
				},
				messages: {
					student_name: {
						required: "Please enter first name",
						lettersonly: "It takes character only",
						minlength: "Please enter at least 3 letter"
					},
	
					district: {
						required: "Please select district name",
					},
					block: {
						required: "Please select block name",
	
					},
					user_address: {
						required: "Please enter address",
						lettersonly: "It takes charector only",
						minlength: "Please enter at least 20 letter"
					},
					post_office: {
						required: "Please enter post office",
						minlength: "Please enter atleast 20 character",
						lettersonly: "It takes character only"
					},
					username: {
						required: "Please enter username",
						unameregex: "Username shoud"
	
					},
					password: {
						required: "please enter password",
						minlength: "Please insert atleast 3 character",
						pswdregex: "Password should be atleast 8 characters and must be one capital letter"
					},
					password_confirmation: {
						required: "Please confirm password",
						lettersonly: "Password did not matched"
					},
					contact: {
						required: "Please enter your contact number",
						minlength: "Please enter atleast 10 numbers",
						mobileNumber: "Please insert number only"
					},
					mobile_otp: {
						required: "Please enter mobile otp",
						minlength: "The OTP should be atleast 4 digits number",
						maxlength: "The OTP should be maximum 8 digits number"
					}
	
	
	
	
				}
			});
	
	
		})
	
	
	
	
		jQuery.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z]+$/i.test(value);
		}, "Letters only please");
		jQuery.validator.addMethod("stringsOnly", function(value, element) {
			return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
		}, "Charector only please");
	
		jQuery.validator.addMethod("mobileNumber", function(value, element) {
			return this.optional(element) || /(^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$)|(^[1-9]\d{9}$)/i.test(value);
		}, "Enter currect mobile number please");
	
		jQuery.validator.addMethod("zipcodeNumber", function(value, element) {
			return this.optional(element) || /^[0-9]{5}$/i.test(value);
		}, "Enter currect zipcode please");
	
		jQuery.validator.addMethod("unameregex", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/.test(value);
		}, "Please insert username");
	
		jQuery.validator.addMethod("pswdregex", function(value, element) {
			return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(value);
		}, "Please insert password");
	</script>
</body>
</html>