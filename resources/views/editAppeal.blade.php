@include('header')
@include('sidenav')
@include('topbar')
<style>.text-block {
    display: none;
  }</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5     class="m-b-10">Appellate</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}"><i class="fa fa-file text-white" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{url('appeal')}}">All Appeals Details</a></li>
                        <li class="breadcrumb-item text-white">Edit Appellate </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> -->
          
            <form action="{{url('editAppeal')}}" method="POST" enctype="multipart/form-data">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <input type="hidden" name="id" value="{{$Appeals['id']}}">
          <div class="shadow-lg p-4 mt--6 bg-white rounded">
            <div class="row">
                <div class="col-4">
                    <div class="form-group">  
                        <label for="">Appellate  Authority</label>
                        <select class="form-control" id="my-select" name="appealAutherity" required >
                            <option class="text-center" id="colorselector" value="{{$Appeals['appeal_authority']}}"> 
                            @if ($Appeals['appeal_authority'] === 'faa')
                                First Appllate Authority 
                          @elseif ($Appeals['appeal_authority'] === 'saa')
                                  Second Appllate Authority
                           
                            @endif
                            </option>
                            <option value="FAA"> First Appllate Authority </option>
                            <option value="SAA"> Second Appllate Authority </option>
                        </select>
                    </div>
                </div>
              
            <div id="FAA" class="col-4 HideShowData ">
                <div class="form-group">  
                <label for="">Commissionery</label>
                    <input type="text"  class="form-control " readonly placeholder="Mr. Amit Kumar " value="Kolhan">
                </div>
            </div>
            <div id="FAA" class="col-4 HideShowData ">
            <div class="form-group">    
            <label for="">Officer name</label>
            <input name="FAAOfiicerName" type="text"  class="form-control" readonly placeholder="Mr. Amit Kumar " value="Mr. Amit Kumar ">
            </div>
            </div>
                          
            <div id="SAA" class="col-4 HideShowData ">
            <div class="form-group">
                <label for="">Officer Name</label>
                <input name="SAAOfiicerName" type="text"  class="form-control" readonly placeholder="Mr. Amit Kumar " value="Mr. Rajiv Prasad">
            </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal Date</label>
                <input type="date"  class="form-control" name="appealDate"  value="{{ date('yy-m-d',strtotime($Appeals['appeal_date'])) }}">
            </div>
            </div>
            
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal Number</label>
                <input type="number"  class="form-control" name="appealNumber" value="{{$Appeals['appeal_number']}}">
            </div>
            </div>
            
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal By</label>
                <input type="text"  class="form-control" name="appealBy" value="{{$Appeals['appeal_by']}}">
            </div>
            </div>
            
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal To</label>
                <input type="text"  class="form-control" name="appealTo" value="{{$Appeals['appeal_to']}}">
            </div>
            </div>
            <div class="col-12">
            <div class="form-group">
                <textarea rows="10" class="form-control" name="appealText" placeholder="Write Your Appeal">{{$Appeals['appeal_text']}}</textarea>
                <div class="py-3"><input type="submit" style="float:right;" name="submit" class="btn btn-primary " value="Update Appellate"/></div>
            </div>
            </div>
        </div>
      </div>
    </div>
    </form>
               
    <!-- [ Main Content ] end -->
</div>
</div>
<!-- </script><script src="../assets/js/vendor-all.min.js"></script>
<script src="../assets/js/plugins/bootstrap.min.js"></script>
<script src="../assets/js/ripple.js"></script>
<script src="../assets/js/pcoded.min.js"></script> -->
@include('footer')
<!-- Apex Chart -->
<!-- <script src="../assets/js/plugins/apexcharts.min.js"></script> -->
<!-- custom-chart js -->
<!-- <script src="../assets/js/pages/dashboard-main.js"></script> -->
<script>
    var colorselector = $('#colorselector').val();
    // alert(colorselector);
    if(colorselector=='faa'){
        $('#FAA').show();
        $('#SAA').hide();
    }else if(colorselector=='saa'){
        $('#SAA').show();
        $('#FAA').hide();
    }
    var textBlocks = $('.HideShowData');
$('#my-select').change(function() {  
    textBlocks.hide();
  
  // Show the text block that corresponds to
  // the select value
  textBlocks.filter('#' + this.value).show();
});
</script>
