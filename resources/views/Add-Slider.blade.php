@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Slider List </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="fndSettings"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="Slider-List"> Slider List</a></li>
                            <li class="breadcrumb-item"><a href="">Add Slider List</a></li>
                        </ul>
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('Insert-Slider')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                 
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">Add Slider Line</div>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><b>Title </b></label>
                                    <input required type="text" name="title" class="form-control rte_input"  style="height: 100%;width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><b>Text </b></label>
                                    <input required type="text" name="text" class="form-control rte_input"  style="height: 100%;width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><b>Image</b></label>
                                    <input required type="file" accept=".pdf, .xls, .xlsx, .jpg, .png , .jpeg " name="document" class="form-control rte_input"  >
                                         
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9 col-4"></div>
                                <div class="col-md-3 col-4">
                                    <input class="btn btn-primary btn_submit" type="submit" name="submit" value="Add" style="float:right" />
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')