@include('header') 
@include('sidenav') 
@include('topbar') 
<style>
    td {
        border: 0.5px solid #dad8d8;
        padding: 2px;
    } 
    th {
        border: 0.5px solid #e7e4e4;
        padding: 2px;
        text-align: center;
        background-color: #ecf0f5;
    }
</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div> 
<!-- [ Main Content ] start --> 
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Slider</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('district')}}"><i class="fa fa-file" aria-hidden="true"></i> 
                            <li class="breadcrumb-item"><a href="fndSettings"> Settings</a></li> 
                            <li class="breadcrumb-item"><a href="">Slider </a></li> 
                        </ul> 
                        @if(Session::has('flash_message')) 
                        <div class="alert alert-success"> 
                            {{ Session::get('flash_message') }} 
                        </div> 
                        @endif 
                    </div> 
                </div> 
            </div> 
        </div> 
        <!-- [ breadcrumb ] end --> 
        <!-- [ Main Content ] start --> 
        <div class="row"> 
            <div class="col-md-12"> 
 
 
                <!-- school details --> 
                <div class="card"> 
                    <div class="card-header">Slider
                        <a href="Add-Slider" style="float:right">
                            <button class="btn btn-light text-success btn-sm ">
                                <i class="fa fa-plus " title="Add Slider "></i>
                            </button>
                        </a>
                    </div> 
                    <div class="card-body"> 
                        <table style="  width: 100%;"> 
                            <thead> 
                                <tr> 
                                    <th>Sl. </td> 
                                    <th>Image </th>
                                    <th>Title </th> 
                                    <th>Text</th>
                                    <th>Status</th> 
                                    <th>Action</th> 
                                </tr> 
                            </thead> 
                            <tbody> 
                                @foreach($Sliders as $k=>$Slider) 
                                <tr> 
                                    <td class='ID' style="text-align: center;width: 5%;">{{$k+1}} </td> 
                                    <td class='Name' style="width: 30%;">     
                                     <a href="public/assets/images/{{$Slider->image}}" target="_blank" >{{$Slider->image}}</a>
                                    </td>     
                                    <td class='Name' style="width: 20%;">     
                                        {{$Slider->title}}                           
                                    </td>     
                                    <td class='Name' style="width: 35%;word-wrap: break-all;">
                                        {{$Slider->text}}
                                    </td>
                                    <td class='Name' style="text-align:center;width: 5%;"> 
                                    @if($Slider->status==1)
                                        <span class="text-success">Active</span>
                                    @else
                                        <span class="text-danger">Inactive</span>
                                    @endif
                                    </td> 
                                    <td style="text-align:center; width: 10%;"> 
 
                                        <a href="editSlider/{{$Slider->id}}"><button class="btn btn-light text-primary btn-sm "><i class="fa fa-edit " title="Edit"></i></button></a> 
                                        <a href="delete-Slider/{{$Slider->id}}">
                                            
                                            @if($Slider->status==1)
                                            <button class="btn btn-light text-success btn-sm">
                                                <i class="fas fa-toggle-on"></i>
                                            </button>
                                            @else
                                            <button class="btn btn-light text-danger btn-sm">
                                                <i class="fas fa-toggle-off"></i>
                                            </button>
                                            @endif
                                        </a> 
                                    </td> 
                                </tr> 
                                @endforeach 
                            </tbody>
                        </table> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
</div> 
@include('footer') 
