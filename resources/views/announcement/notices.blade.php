@include('home.header')

<style>
  .notices-list ul li a {
    color: blue !important;

  }

  .notices-list ul li, .news-list ul li {
    display: inline-block;
    padding:10px 0;
  }

  .notices-list,
  .news-list {
    height: 450px;
    overflow: hidden;
    overflow-y: scroll;
  }

  .tab-content .tab-pane .scroll-content-tab .list {
    display: inline-block;
  }

  .subBox {
    border: 1px solid #ccc;
    padding: 10px 0px;
    transition: 0.5s;
    /* background: #f4f4f4; */
    margin: 15px 0px;
  }

  .subBox img {
    width: 55%;
    margin-bottom: 25px;
    border-radius: 100px;
    border: 1px solid #e0effc;
    padding: 5px;
    box-shadow: 6px 6px 8px #fafbfc;
    transition: 0.3s;
  }

  .subBox a {
    color: black;
    font-weight: 500;
  }
  .bg-yellow{
    background:#e6bb38;
  }
  .bg-blue{
    background:#2980de;
  }
</style>

<div class="container mt-5">
    <div class="noticelist-container">
        <div class="row">
        <div class="col-md-6">
            <div class="latestEvent p-2 border mt-4 mb-4">
                <h5 class="p-2 bg-blue text-white"><i class="fas fa-file-signature"></i>&nbsp;&nbsp;Important Notices</h5>
                <div class="notices-list">
                    <ul>
                        <li><a href="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, architecto. <img src="assets/images/new.gif" alt=""> </a></li>
                        <li><a href="">Maiores veritatis nesciunt quis! Earum, repellendus! Quod ea officiis natus!</a></li>
                        <li><a href="">Minus provident quos qui assumenda vitae inventore facere, doloribus sapiente.<img src="assets/images/new.gif" alt=""> </a></li>
                        <li><a href="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, architecto.</a></li>
                        <li><a href="">Maiores veritatis nesciunt quis! Earum, repellendus! Quod ea officiis natus!</a></li>
                        <li><a href="">Minus provident quos qui assumenda vitae inventore facere, doloribus sapiente.</a></li>
                        <li><a href="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, architecto.</a></li>
                        <li><a href="">Maiores veritatis nesciunt quis! Earum, repellendus! Quod ea officiis natus!</a></li>
                        <li><a href="">Minus provident quos qui assumenda vitae inventore facere, doloribus sapiente.</a></li>
                        <li><a href="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, architecto.</a></li>
                        <li><a href="">Maiores veritatis nesciunt quis! Earum, repellendus! Quod ea officiis natus!<img src="assets/images/new.gif" alt=""> </a></li>
                        <li><a href="">Minus provident quos qui assumenda vitae inventore facere, doloribus sapiente.</a></li>
                 

                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="latestnews p-2 border mt-4 mb-4">
                <h5 class="p-2 bg-yellow text-white"><i class="fas fa-pen-square"></i>&nbsp;&nbsp;Latest News</h5>

                <div class="news-list">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="updates-tab" data-toggle="tab" href="#updates" role="tab" aria-controls="updates" aria-selected="false">Notice</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id="cw-tab" data-toggle="tab" href="#cw" role="tab" aria-controls="cw" aria-selected="false">Accommodation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="insurance-tab" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">CGHS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pension-tab" data-toggle="tab" href="#pension" role="tab" aria-controls="pension" aria-selected="false">Pension/Salary</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="updates" role="tabpanel" aria-labelledby="updates-tab">
                            <div class="scroll-content-tab">
                                <ul class="list">
                                    <li><a href="pdf/notice/punctuality.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Observation of punctuality - strict adherence of instrctions<img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/ISLPractitioners.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Need of Indian Sign Languages (ISL) Practitioners to translate the speeches for the benefit of the hearing imapired<img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/FIT21.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;FIT India Quiz 2021 for schools under FIT India Movement <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/akam.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Various activities undertaken by school children as part of Azadi ka Amrit Mahotsav (AKAM) <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/Minutes_of_the meeting_held_under_the_chairmanship_of_Hon'ble_Education_Minister.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Minutes of the Meeting held under the Chairmanship of Hon'ble Education Minister<img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/Celebration_of_International_Literacy_Day.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Celebration of International Literacy Day - 2021 under Shikshak Parv on 8th September, 2021 <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/Azadi_ka_Amrit_Mahotsav-Aryabhata_Ganit_Challenge.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Azadi ka Amrit Mahotsav–Aryabhata Ganit Challenge (AGC) 2021 on DIKSHA and MyGov platforms <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/Swachhata_Pakhwada_2021.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Swachhata Pakhwada 2021 <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/programmes/NTSE/Miscellaneous/Minutes_of_Review_Committee_Meeting_of_NTSE.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Minutes of the Review Committee Meeting of National Talent Search Scheme Held on 5th, August, 2021 <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/Conference_and_Multi_Stakeholder Forum _Addressing_Hate_Speech_through_Education.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Global Education Ministers Conference and Multi - Stakeholder Forum. <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/FIT_India_Quiz_for_schools_under_FIT_India_Movement.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;FIT India Quiz for schools under FIT India Movement. <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/notice/COVID-19-16April21.pdf"><i class="far fa-hand-point-right"></i>&nbsp;Preventive measures to be taken to contain the spread of COVID-19<img src="images/new.gif"></a></li>

                                   
                                </ul>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="cw" role="tabpanel" aria-labelledby="cw-tab">
                            <div class="scroll-content-tab">
                                <ul class="list">
                                    <li><a href="pdf/announcement/notices/campusWelfare/ChangeListJan21toJuly21.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Tentative priority/Change List for the month of January, 2021 to July , 2021 (upto 20.07.2021) of Type-I to Type-V Quarters </a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/type-I-V_July_Dec20.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Tentative List for Change of Type-1 to Type-V Quarters for the month of July 2020 to December, 2020 (upto 20.12.2020)</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/CnW_20July2020.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Revision of flat rates of licence fee for General Pool Residential Accomodation (GPRA) throughout the country w.e.f 01.07.2020</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/type-I to type-II.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Tentative List for Change of Type-I Quarters for the month of Jan. 2020 to July, 2020 (upto 20.07.2020)</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/jan_2020_list.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Final priority list for Type-1 to Type-V Quarters for the month of January,2020(upto 20.01.2020)</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/Notice-1168.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Notice dated 17th December 2019 regarding the farewall of the 04 officials.</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/notice_tender.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Regarding allotment of vacant Shops &amp; Stalls in the NCERT Buildings.</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/Cir_listrie.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Selected/Waiting List of Players for 35th Annual NCERT Staff Tournament 2019</a></li>
                                    <li><a href="pdf/announcement/notices/campusWelfare/CGHS.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Frequenty asked questions with answers about CGHS</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="insurance" role="tabpanel" aria-labelledby="insurance-tab">
                            <div class="scroll-content-tab">
                                <ul class="list">
                                    <li><a href="pdf/announcement/notices/medical/cghs_notification_29.7.21.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Extension of Credit /Cashless facility to the NCERT's retired/serving employees from CGHS empanelled Hospitals/Labs/Diagnostic Centres.</a><img src="images/new.gif"></li>
                                    <li><a href="pdf/announcement/notices/medical/Amendment_in_Maternitybenefit_Act1961.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Amendment in Maternity benefit Act 1961-reg</a></li>
                                    <li><a href="pdf/announcement/notices/medical/cghs-22122020.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Extension of Credit /Cashless facility to the NCERT's retired/serving employees from CGHS empanelled Hospitals/Labs/Diagnostic Centres/OPD in Emergency/Hospital/inpatient treatment on CGHS rates w.e.f 18.12.2020</a></li>
                                    <li><a href="pdf/announcement/notices/medical/c&amp;w_8118-8121.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Extension of Credit /Cashless facility to the NCERT's retired/serving employees from CGHS empanelled Hospitals/Labs/Diagnostic Centres.</a></li>
                                    <li><a href="pdf/announcement/notices/medical/Revision_rates_subscription.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Revision of rates of subscription under Central Government Health Scheme due to revision of pay and allowances of Central Government employees and revision of pension/family pension on account of implementation of recommendations of the Seventh Central Pay Commission</a></li>
                                    <li><a href="pdf/announcement/notices/medical/MedicalReimbursementCircular.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Request for Medical reimbursement claims/Permission-reg.</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pension" role="tabpanel" aria-labelledby="pension-tab">
                            <div class="scroll-content-tab">
                                <ul class="list">
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/DA_Notice.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Revised rates of Dearness Allowance to Central Government employees w.e.f.01.07.2021 <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/Scan 22 Jun 2021.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Appoinment order for Mahavir singh. <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/Letter to all Secretaries GOI reg. prompt disbursement of family pension and related benefits to the eligible family members of the deceased Government employees..pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Letter to all Secretaries GOI reg. prompt disbursement of family pension and related benefits to the Government employees. <img src="images/new.gif"></a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/Pention_Adalat_app_Form.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Pension Adalat </a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/life_certificate.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Life Certificate</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/file_408-443.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Coverage under Central Civil Services (Pensions) Rules, 1972, in place of National Pension System, of those Central Government employees whose selection for appointment was finalized before 01.01.2004 but who joined Government service on or after 01.01.2004</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/1269_1269.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Grant of Dearness Relief to Central Government pensioners/family pensioners Revised rate effective from 01.07.2019-reg</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/second_pension_adalat.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Regarding Second Pension Adalat to address the grievances of Council pensioners on 23rd August 2019.</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/mo_pension.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; M/o Finance notification dt 31.01.2019 of National Pension System for amendments</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/Dearness_relief_1_7_2018.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Grant of Dearness Relief to Central Government pensioners/family pensioners- Revised rate effective from 01.07.2018.</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/retirement_gdg.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Extension of benefits of 'Retirement Gratuity and Death Gratuity to the Central Government employees covered by new Defined Contribution Pension System (National Pension System)</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/da1_7_2016.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Grant of Dearness Relief to Central Government Employees who had drawn lump sum amount on absorption in a PSU/Autonomous body and are in receipt of 1/3rd restored commuted portion of pention- revised rate effective from 1.7.2016</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/pensionpre_200623_5_16.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Revision of pension of pre-2006 pensioners- delinking of revised pension service of 33 years</a></li>
                                    <li><a href="pdf/announcement/notices/gpfpensionsalary/pentionersfac.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Revision of pension of Pre-2006 pensioners of Faculty</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade active show" id="notice-miscellaneous" role="tabpanel" aria-labelledby="notice-miscellaneous-tab">
                            <div class="scroll-content-tab">
                                <ul class="list">
                                    <li><a href="pdf/notice/NSQF.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Regarding several new skills and competencies that are emerging as requirements of 21st century, and that all these need to be systematically identified and developed as NSQF compliant skill subjects for students </a><img src="images/new.gif"></li>
                                    <li><a href="pdf/notice/75anniversery.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;DO regarding celebration of 75th Anniversary of India's Independence </a><img src="images/new.gif"></li>
                                    <li><a href="pdf/notice/nationwide campaign about Tokyo Olympics.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Nationwide campaign about Tokyo Olympics.</a><img src="images/new.gif"></li>
                                    <li><a href="pdf/notice/Disaster Risk reduction activities and programmes - reg.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Disaster Risk Reduction activities and programmes</a><img src="images/new.gif"></li>
                                    <li><a href="pdf/announcement/miscellaneous/LaunchofOnlinePortal_PPP-MIIOrder2017.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Launch of Online Portal for lodging grievances for alleged violation of PPP-MII Order, 2017-reg.</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/Hon_PM_MentoringYUVACampaign.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Hon'ble Prime Minister's Mentoring YUVA Campaign-reg.</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/InteractionHon'ble_PM_Tokyo Olympic_bound_athletes.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Interaction of Hon'ble Prime Minister with the Tokyo Olympic bound athletes on 13th July, 2021 at 5.00 P.M.</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/Orderdated25.03.2021.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Order dated 25.03.2021 for notifying items under Para3(a) of Public Procurement (Preferece to Make In India) Order</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/NightCurfewDelhi.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Night curfew in Delhi with immediate effect from 10:00 p.m. to 5:00 a.m. till 30.04.2021.</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/125thBirth_netaji.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;To celebrate 125th Birth Year of Netaji Subhash Chandra Bose: International Essay Competition.</a></li>

                                    <li><a href="pdf/announcement/miscellaneous/JDPSSCIVE.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Dr Rajesh P. Khambayat Extension order</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/Officials_dealings.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Regarding Official dealings between the Administration and Members of Parliament and State legislatures - Observance of proper procedure</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/CallforProposal.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Call for proposal- Climate Change and Right to Education</a></li>
                                    <li><a href="pdf/announcement/miscellaneous/order_1538.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Regarding hand over the charge of R.K Nayak, CAO/IFA to Sh. T.S Bisht , Sr. Account Officer, Account Branch. </a></li>
                                    <li><a href="pdf/announcement/miscellaneous/COVID_19_Report_MISC.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Covid-19 Report </a></li>
                                    <li><a href="pdf/announcement/notices/miscellaneous/LetterSJMAPMIndiaVPMIVienna.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Letter from Shri Jaideep Mazumdar, Ambassador, Permanent Mission of India, Vienna requesting routing of UNIDO related proposals through PMI, Vienna</a></li>
                                    <li><a href="pdf/announcement/notices/miscellaneous/Covid -19_Attendance.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Preventive measures to contain the spread of Noval Coronavirus (COVID-19) - Attendance of Central Government officials regarding.</a></li>
                                    <li><a href="pdf/announcement/notices/miscellaneous/professor_Info_RTI.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp; Information desired by Hon'ble Mininster of Education on the profile of NCERT professors.</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        </div>
      
    </div>
</div>
<br><br><br><br><br><br>
@include('home.footer')