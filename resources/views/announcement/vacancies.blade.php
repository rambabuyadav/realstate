@include('home.header')

<style>
    .sec-content .subcontent-bx ul li {
        display: inline-block;
    }
    .sec-content .subcontent-bx .sublist li{
        display: inline-block;
    }
    .sec-bx{
        border:1px solid #ddd;
        box-shadow: 3px 5px 7px gray;
        padding:30px;
        margin-bottom:30px;
    }
    .sec-content .subcontent-bx ul{
            background:#ddd;
            padding:30px;
            margin-top:15px;
            border-radius: 8px;
    }
</style>
<div class="container">
    <div class="sec-bx mt-5">
        <div class="sec-content">
            <h2 class="text-warning">Vacancies </h2>
            <i class="far fa-hand-point-right"></i> Academic
            <div class="subcontent-bx">
                <ul>
                    <li><a href="pdf/announcement/vacancies/academicvacancy/AdvtDirectorEnglish.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Advertisement for the post of Director, National Council of Educational Research and Training (NCERT) &nbsp;|&nbsp;English</a>&nbsp;|&nbsp;<a href="pdf/announcement/vacancies/academicvacancy/AdvtDirectorHindi.pdf" target="_blank">Hindi</a><img src="../assets/images/new.gif"></li>
                    <li><a href="pdf/announcement/vacancies/academicvacancy/AdvtDirectorEnglish.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Advertisement for the post of Director, National Council of Educational Research and Training (NCERT) &nbsp;|&nbsp;English</a>&nbsp;|&nbsp;<a href="pdf/announcement/vacancies/academicvacancy/AdvtDirectorHindi.pdf" target="_blank">Hindi</a><img src="../assets/images/new.gif"></li>
                    <li><a href="pdf/announcement/vacancies/academicvacancy/AdvtDirectorEnglish.pdf" target="_blank"><i class="far fa-hand-point-right"></i>&nbsp;Advertisement for the post of Director, National Council of Educational Research and Training (NCERT) &nbsp;|&nbsp;English</a>&nbsp;|&nbsp;<a href="pdf/announcement/vacancies/academicvacancy/AdvtDirectorHindi.pdf" target="_blank">Hindi</a><img src="../assets/images/new.gif"></li>
                </ul>

                <div>
                    <i class="far fa-hand-point-right"></i>&nbsp;<a href="">Non-Academic</a> 
                </div>
                <div>
                    <i class="far fa-hand-point-right"></i>&nbsp;<a href="">Project-staff</a> 
                </div>
                <div>
                    <i class="far fa-hand-point-right"></i>&nbsp;<a href="">Miscellaneous</a> 
                </div>
                <div>
                    <i class="far fa-hand-point-right"></i>&nbsp;<a href="">Results</a> 
                </div>
                
               


            </div>
        </div>
    </div>
</div>
<br><br><br><br>  <br><br><br><br><br><br>
@include('home.footer')