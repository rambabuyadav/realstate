@include('header')
@include('sidenav')
@include('topbar')
<!-- <div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div> -->
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Application </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">Application Form </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="" method="POST">
                    @csrf
                    <div class="card">
                        @if( Session::has('message') )
                        <div class="alert alert-success mx-3 mt-3" role="alert">
                            {{ Session::get('message') }}
                        </div>
                        @endif
                        <div class="card-header">School Details</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">School Name </label>
                                            <input required type="text" maxlength="48" name="school_name" class="form-control rte_input" required>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="">Academic Session From Which Recognition Proposed</label>
                                            <input required type="text" name="recognised_by" maxlength="38" class="form-control rte_input" required>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">District </label>
                                            <input required type="text" name="district" maxlength="18" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Post Office </label>
                                            <input required type="text" name="post_office" maxlength="18" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Village / Town </label>
                                            <input required type="text" name="village_town" maxlength="18" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Pin Code</label>
                                            <input required type="number" name="pin_code" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Ph./Mb no. With STD Code </label>
                                            <input required type="text" name="phone_with_std_code" maxlength="32" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">FAX No. With STD Code </label>
                                            <input required type="text" name="fax_with_std_code" maxlength="32" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Email ID</label>
                                            <input required type="email" name="email" maxlength="48" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nearest Police Station</label>
                                            <input required type="text" maxlength="20" name="police_station" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- general information -->
                    <div class="card">
                        <div class="card-header">
                            General Information
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Establishment Year </label>
                                            <select class="form-control" name="estd_year">
                                                <option value="">Select Year</option>
                                                <option value="2020">2020</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">School Opening Date </label>
                                            <input required type="date" name="opening_date" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">trust/Society/Management Committee Name </label>
                                            <input required type="text" maxlength="48" name="society_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">is trust/society/management comittee registered?</label>
                                            <div>
                                                <input required type="radio" name="is_society_registered" class=""> Yes
                                                <input required type="radio" name="is_society_registered" class="" style="margin-left:20px;"> No
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">The period until the registration of the Trust / Society / Management Committee is valid</label>
                                            <input required type="date" name="society_registration_valid_upto" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Is there any evidence of non-proprietary nature of the Trust / Society / Management Committee, supported by the list of members including their addresses on affidavit ?</label>
                                            <div>
                                                <input required type="radio" class="" name=""> Yes
                                                <input required type="radio" class="" name="" style="margin-left:20px;"> No
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h5 style="margin:20px 0;">School Chairman Information</h5>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Chairman Name </label>
                                            <input required type="text" maxlength="24" name="school_chairman_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=""> Designation</label>
                                            <input required type="text" name="school_chairman_post" maxlength="19" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <input required type="text" name="school_chairman_address" maxlength="78" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Phone No.</label>
                                            <input required type="number" name="school_chairman_phone_number" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Office</label>
                                            <input required type="text" maxlength="15" name="school_chairman_office" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Email ID</label>
                                            <input required type="email" maxlength="30" name="school_chairman_email" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Last 3 Years Total Income/Expenses/Surplus/Loss</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Year</label>
                                            <select name="session_year" class="form-control">
                                                <option>Select Year</option>
                                                <option value="2016-17">2016-17</option>
                                                <option value="2017-18">2017-18</option>
                                                <option value="2018-19">2018-19</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Income</label>
                                            <input required type="number" name="income" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Expenses</label>
                                            <input required type="number" name="expenses" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Surplus Money</label>
                                            <input required type="number" name="surplus_money" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Reduce</label>
                                            <input required type="number" name="reduced_money" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- school format and area -->
                    <div class="card">
                        <div class="card-header">School Area and Format Details</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Medium Of Education </label>
                                            <input required type="text" maxlength="50" name="medium" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="">Type Of School(mention entry and last class of your school)As: Section(2)n Of Act. </label>
                                            <input required type="text" maxlength="20" name="type_of_school" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Agency Name (if school has support) </label>
                                            <input required type="text" maxlength="38" name="supported_agency_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Support Percentage</label>
                                            <input required type="number" maxlength="4" name="agency_supported_percent" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">If School Is Recognised Mention Authority Name</label>
                                            <input required type="text" maxlength="30" name="authority_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Recognition Number</label>
                                            <input required type="number" name="recognised_number" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="">The School Has Its Own Building Or Is Working In A Rented Building ?</label>
                                            <div>
                                                <input required type="radio" name="is_school_on_rented" class=""> Own
                                                <input required type="radio" name="is_school_on_rented" class="" style="margin-left:20px;"> Rented
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And skill development?</label>
                                            <div>
                                                <input required type="radio" class=""> Yes
                                                <input required type="radio" class="" style="margin-left:20px;"> No
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Total Area Of School</label>
                                            <input required type="text" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Area Of School Building Only</label>
                                            <input required type="number" name="school_building_area" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Enrollment
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Pre-elementary class</label>
                                            <input required type="text" maxlength="10" name="pre_elementary_class" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Number Of Section</label>
                                            <input required type="text" maxlength="10" name="pre_no_of_section" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Number Of Students</label>
                                            <input required type="number" name="pre_no_of_student" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Nursery</label>
                                            <input required type="text" maxlength="10" name="class_one_to_five" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Number Of Section</label>
                                            <input required type="number" maxlength="22" name="onefive_no_of_section" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Number Of Students</label>
                                            <input required type="number" maxlength="15" name="onefive_no_of_student" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Kindergarder</label>
                                            <input required type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Number Of Section</label>
                                            <input required type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Number Of Students</label>
                                            <input required type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                                <div id="clsAdd">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Class 1</label>
                                                <input required type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Number Of Section</label>
                                                <input required type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Number Of Students</label>
                                                <input required type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-success ml-3" id="addClass">Add </a>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function () {
                                        var i = 2;
                                        $('#addClass').click(function () {
                                            var inp = $('#clsAdd');
                                            $('<div id="clsAdd' + i + '" ><div class="row"><div class="col-md-4"><div class="form-group"><label for="">Class ' + i + '</label><input required type="text" maxlength="11" name="class_six_to_eight" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Section</label><input required type="text" maxlength="10" name="sixeight_no_of_section" class="form-control rte_input"></div></div><div class="col-md-4"><div class="form-group"><label for="">Number Of Students</label><input required type="text" maxlength="10" name="sixeight_no_of_student" class="form-control rte_input"></div></div><a  href="javascript:void(0)" class="btn btn-danger mx-3" id="remove1">Remove </a></div></div>').appendTo(inp);
                                            i++;
                                            if (i > 8) {
                                                document.getElementById('addClass').style.visibility = 'hidden';
                                            }
                                            console.log(i);
                                            var teacherDataId = i - 1;
                                            document.getElementById("hiddenTeacherId").value = teacherDataId;
                                        });
                                        $('body').on('click', '#remove1', function () {
                                            i--;
                                            if (i <= 8) {
                                                // document.getElementById('addClass').style.visibility = 'visible';
                                            }
                                            console.log(i);
                                            $(this).parent('div').remove();
                                            var teacherDataId = i - 1;
                                            document.getElementById("hiddenTeacherId").value = teacherDataId;
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Infrastructure Details
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Classes </label>
                                            <input required type="text" maxlength="10" name="no_of_class" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Classroom (in w*h)</label>
                                            <input required type="text" maxlength="10" name="avg_size_cls_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Office Rooms</label>
                                            <input required type="text" maxlength="10" name="no_of_office_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Office Rooms (in w*h)</label>
                                            <input required type="text" maxlength="10" name="avg_size_of_office_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Store Rooms</label>
                                            <input required type="text" maxlength="10" name="no_of_store_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Store Rooms (in w*h)</label>
                                            <input required type="text" maxlength="10" name="avg_size_of_store_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Principal Rooms</label>
                                            <input required type="text" maxlength="10" name="no_of_princpal_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Principal Rooms (in w*h)</label>
                                            <input required type="text" maxlength="10" name="avg_size_of_principal_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. Of Kitchen Rooms</label>
                                            <input required type="text" maxlength="10" name="no_of_kitchen_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Average Size Of Kitchen Rooms (in w*h)</label>
                                            <input required type="text" maxlength="10" name="avg_size_of_kitchen_room" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Other Convenience
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Does all facilities have access without interrupted? </label>
                                            <div>
                                                <input required type="radio" class="  " name="facilities_access_without_interrupted"> Yes
                                                <input required type="radio" class="" name="facilities_access_without_interrupted" style="margin-left:20px;"> No
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">List All Teaching Materials</label>
                                            <input required type="text" maxlength="10" name="all_teaching_material_list" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">List All Sports Equipments</label>
                                            <input required type="text" maxlength="10" name="all_sports_equipment_list" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Books Facilities In Library</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Books In Library</label>
                                            <input required type="text" maxlength="200" name="books" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Newspaper & Magzine</label>
                                            <input required type="text" maxlength="200" name="magazines" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Water Facilities</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Types Of Water Facilities</label>
                                            <input required type="text" maxlength="30" name="type_of_water_facilities" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Water Supply</label>
                                            <input required type="text" maxlength="30" name="no_of_water_supply" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 style="margin:20px 0;">Cleanliness Related Details</h5>
                                        <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Type Of Toilets</label>
                                            <input required type="text" maxlength="10" name="type_of_toilet" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Seperate Toilet For Boys</label>
                                            <input required type="text" maxlength="10" name="gents_toilet" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Number Of Seperate Toilet For Girls</label>
                                            <input required type="text" maxlength="10" name="ladies_toilet" class="form-control rte_input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Teaching Staff Specialties
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Name Of Principal</label>
                                            <input required type="text" maxlength="100" name="principle_name" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Father / Husband Or Wife Name</label>
                                            <input required type="text" name="p_f_h_w_name" maxlength="100" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Date Of Birth</label>
                                            <input required type="date" name="p_date_of_birth" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Educational Qualification</label>
                                            <input required type="text" maxlength="100" name="p_education_qualification" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Trainee Qualification</label>
                                            <input required type="text" maxlength="100" name="trainee_qualification" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teaching Experience</label>
                                            <input required type="text" maxlength="100" name="teaching_experience" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Class Handed Over</label>
                                            <input required type="text" maxlength="100" name="class_handed_over" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Date Of Appointment</label>
                                            <input required type="date" name="date_of_appointment" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Trained Or Untrained</label>
                                            <input required type="text" maxlength="100" name="trained_untrained" class="form-control rte_input">
                                        </div>
                                    </div>
                                    <div class="row pt-3 px-3" id="dynamic_form_create">
                                        <div class="col-md-12">
                                            <h5> Teacher Information </h5>
                                        </div>
                                        <hr>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Name Of Teacher</label>
                                                <input required type="text" name="teacher_name1" id="teacher_name" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Father / Husband Or Wife Name</label>
                                                <input required type="text" name="teacher_f_h_w_name1" id="teacher_f_h_w_name" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Date Of Birth</label>
                                                <input required type="date" name="teacher_date_of_birth1" id="teacher_date_of_birth" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Educational Qualification</label>
                                                <input required type="text" name="teacher_education_qualification1" id="teacher_education_qualification" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Trainee Qualification</label>
                                                <input required type="text" name="teacher_trainee_qualification1" id="teacher_trainee_qualification" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Teaching Experience</label>
                                                <input required type="text" name="teacher_teaching_experience1" id="teacher_teaching_experience" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Class Handed Over</label>
                                                <input required type="text" name="teacher_class_handed_over1" id="teacher_class_handed_over" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Date Of Appointment</label>
                                                <input required type="date" name="teacher_appointment_date1" id="teacher_appointment_date" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Trained Or Untrained</label>
                                                <input required type="text" name="teacher_trained_or_untrained1" id="teacher_trained_or_untrained" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-3"> </div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3 text-right pt-4">
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-success" id="add">Add</a>
                                            </div>
                                            <!-- <div class="button-group">
                                            <a href="javascript:void(0)" class="btn btn-primary" id="plus5">Add More</a>
                                            <a href="javascript:void(0)" class="btn btn-danger" id="minus5">Remove</a>
                                        </div>  -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">Curriculum and Syllabus</div>
                        <div class="card-body">
                            <div class="container">
                                <div id="boxs">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="">Details Of Curriculum And Syllabus Adopted Class 1</label>
                                                <input required type="text" maxlength="100" name="details_of_curriculum" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="">Method Of Inspection Of Students</label>
                                                <input required type="text" maxlength="100" name="method_of_inspection" class="form-control rte_input">
                                            </div>
                                        </div>
                                        <div class="col-md-2 pt-4"><a href="javascript:void(0)" class="btn btn-success" id="addx">Add Class</a></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Are the students of the school expected to take any board examination till class 8 ?</label>
                                        <div>
                                            <input required type="radio" name="school_board_exam_till_cls_eight" class=" "> Yes
                                            <input required type="radio" name="school_board_exam_till_cls_eight" class=" " style="margin-left:20px;"> No
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5"></div>
            <button class="btn btn-success btn_submit mx-3" type="submit">Save as Draft</button>
            <button class="btn btn-primary btn_submit " type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->
</div>
</div>
@include('footer')
<!-- <script src="assets/js/vendor-all.min.js"></script>
<script src="assets/js/plugins/bootstrap.min.js"></script>
<script src="assets/js/ripple.js"></script>
<script src="assets/js/pcoded.min.js"> </script> -->
<!-- <script src="assets/js/plugins/apexcharts.min.js"></script> -->
<!-- <script src="assets/js/pages/dashboard-main.js"></script> -->
<!-- <script>
    $(document).ready(function() {
        var dynamic_form_create =  $("#dynamic_form_create").dynamicFormCreate("#dynamic_form_create","#plus5", "#minus5", {
            limit:10,
            formPrefix : "dynamic_form_create",
            normalizeFullForm : false
        });
        // dynamic_form_create.inject([{p_name: 'Hemant',quantity: '123',remarks: 'testing remark'},{p_name: 'Harshal',quantity: '123',remarks: 'testing remark'}]);
        $("#dynamic_form_create #minus5").on('click', function(){
            var initDynamicId = $(this).closest('#dynamic_form_create').parent().find("[id^='dynamic_form_create']").length;
            if (initDynamicId === 2) {
                $(this).closest('#dynamic_form_create').next().find('#minus5').hide();
            }
            $(this).closest('#dynamic_form_create').remove();
        });
        $('form').on('submit', function(event){
            var values = {};
            $.each($('form').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });
            console.log(values)
            event.preventDefault();
        })
    });
</script> -->