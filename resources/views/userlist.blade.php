@include('header')
@include('sidenav')
@include('topbar')
<style>
  .w-5 {
    display: inline;
    height: 30px;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">District </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="">All User Details</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">
            <a href="{{url('addUser')}}" class="text-success" title="add "><button type="button" class="btn btn-sm btn-light btn-offset text-primary" title=" Add User"><i class="fa fa-plus text-success"> </i> </button></a>
            <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="window.location.reload()" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button>
          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <!-- <h1>Here is all Payment Related Details</h1> -->
        <div class="shadow-lg p-3 mt--6 bg-white rounded">
          @include('message-flash')
          <table class="table table-bordered table-sm ">
            <thead>
              <tr>
                <th>ID </td>
                <th>Name</th>
                <th>Designation</th>
                <th>Address</th>
                <th>Phone No</th>
                <th>E-mail ID</th>
                <th>Created At</th>
                <th>Created By</th>
                <th colspan="2" style="text-align:center; width:8%;">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($UserData['User'] as $user)
              <tr>
                <td class='ID'>{{$user->id}} </td>
                <td class='Name'>{{$user->name}} </td>
                <td class="District">{{$user->user_role}}</td>
                <td class='Address'>{{$user->user_address}}</td>
                <td class='Phone'>{{$user->contact}}</td>
                <td class='Email'>{{$user->email}}</td>
                <td class='Date'>{{$user->created_at->format('d/m/Y H:m:i')}}</td>
                @php $created_by_name = DB::table('users')->where('id',$user->created_by)->value('name') @endphp
                <td class='CreatedBy'>{{$created_by_name}}</td>
                <td style="text-align:center;">
                  @php
                  $id = Crypt::encrypt($user->id);
                  @endphp
                  <a href="edit-user/{{$id}}">
                    <button type="submit" class="btn btn-sm btn-ligth text-primary btn-offset" title=""><i class="fa fa-edit " title="Edit"></i></button>
                  </a>
                </td>
                <td>
                  <form method="post" action="{{url('deleteUser')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="user_id" value="{{$user->id}}">
                    <button type="submit" class="btn btn-sm  btn-ligth text-danger btn-offset" title=""><i class="fa fa-trash" title="Delete"></i></i></button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{-- Pagination --}}
          {{ $UserData['User']->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
<!-- [ Main Content ] end -->
<!-- [ Adv Search Model ] -->
<div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form action="{{url('advSearch')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <label for="message-text" class="col-form-label"> Advance Search :</label>
              @isset($UserData['adv_data'])
              <input type="text" name="adv_search" type="text" class="form-control" value="{{$UserData['adv_data']['search_text']}}" placeholder=" ">
              @else
              <input type="text" name="adv_search" id="adv_search" class="form-control rte_input" placeholder=" ">
              @endisset
              <input type="hidden" name="search_status" type="text" class="form-control" value="user_search" />
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <label for="message-text" class="col-form-label"> Division :</label>
              <select class="form-control rte_input" id="division" name="division" required>
                @isset($UserData['adv_data'])
                @if($UserData['adv_data']['divisionName'])
                <option value="{{ $UserData['adv_data']['division_id']}}">{{ $UserData['adv_data']['divisionName']}}</option>
                @endif
                @endisset
                <option value="-1">Select Division </option>
                @foreach($UserData['District'] as $Division)
                <option value="{{$Division->id}}">{{$Division->value_set_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-6">
              <label for="message-text" class="col-form-label"> District :</label>
              <select class="form-control rte_input" id="district" name="district" required>
                @isset($UserData['adv_data'])
                @if($UserData['adv_data']['district_id'] == '-1')
                <option value="-1">Select District </option>
                @elseif($UserData['adv_data']['districtName'])
                <option value="{{ $UserData['adv_data']['district_id']}}">{{ $UserData['adv_data']['districtName']}}</option>
                @endif
                @endisset
                <option value="-1">Select District </option>
              </select>
            </div>
            <div class="col-6">
              <label for="message-text" class="col-form-label"> User Role :</label>
              <select class="form-control rte_input" name="usertype" required>
                @isset($UserData['adv_data'])
                @if($UserData['adv_data']['usertype'] == '-1')
                <option value='-1'>Select User </option>
                @elseif($UserData['adv_data']['usertype'])
                <option value="{{ $UserData['adv_data']['usertype']}}">{{ $UserData['adv_data']['usertype']}}</option>
                @endif
                @endisset
              
                <option value="dc">DC</option>
                <option value="dse">DSE</option>
                <option value="faa">FAA</option>
                <option value="saa">SAA</option>
                <option value="state">State</option>
                <option value="school">School</option>
              </select>
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <input type="submit" name="submit" value="Submit" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- [ Adv Search End ] -->
@include('footer')
<script type="text/javascript">
  $(document).ready(function () {
    $('.deleteGroup').on('click', function (e) {
      if (!confirm('Do you want to delete this item?')) {
        e.preventDefault();
      }
    });
    $('#division').on('change', function () {
      var stateID = $(this).val();
      console.log(stateID);
      var options = '<option value="-1">Select District </option>';
      if (stateID) {
        $.ajax({
          url: 'DGR/block/' + stateID,
          type: "GET",
          dataType: "json",
          success: function (data) {
            console.log('Block ')
            console.log(data);
            for (var i = 0; i < data.length; i++) {
              var id = data[i]['id'];
              var value_set_name = data[i]['display_value'];
              //do something with k or data...
              options += '<option value="' + id + '">' + value_set_name + '</option>';
            }
            $('#district').html(options);
            $('#block').html('<option value="-1">Select Block  </option>');
          }
        });
      } else {
        $('#district').empty();
      }
    });
  });
</script>