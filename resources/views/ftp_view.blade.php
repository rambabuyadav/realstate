@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">File & Folder </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">All File & Folders</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row ftp-cards">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">
                    @include('message-flash')
                    <div class="row" ng-show="views.list">
                        <div class="col-md-12" style="margin-top: 17px;">
                            <h3 class="card-title" style="margin-left: 20px;">
                                <span style="float: right;">
                                    
                                    <button class="btn btn-sm btn-primary add_button" title="Add Document" id="add_button_id" data-toggle="modal" data-target="#add-document" style="float: right;">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-lg-12 col-xl-12 col-xs-12">
                            <div class="card">
                                <div class="card-block box-shadow">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success"><i class="fa fa-folder"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h6 class="text-muted m-b-0">  <u>Controllers Directory</u> </h6>
                                        </div>
                                     
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                            <div class="card">
                                                <div class="card-block box-shadow">
                                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                                        <span class="directory_name" style="display: none;">Controllers</span>
                
                                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                        <div class="m-l-10 align-self-center">
                                                            <h6 class="text-muted m-b-0"> Controllers Files</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                            <div class="card">
                                                <div class="card-block box-shadow">
                                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                                        <span class="directory_name" style="display: none;">Api</span>
                
                                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                        <div class="m-l-10 align-self-center">
                                                            <h6 class="text-muted m-b-0"> API</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                            <div class="card">
                                                <div class="card-block box-shadow">
                                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                                        <span class="directory_name" style="display: none;">Exports</span>
                
                                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                        <div class="m-l-10 align-self-center">
                                                            <h6 class="text-muted m-b-0"> Exports </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                            <div class="card">
                                                <div class="card-block box-shadow">
                                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                                        <span class="directory_name" style="display: none;">Providers</span>
                
                                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                        <div class="m-l-10 align-self-center">
                                                            <h6 class="text-muted m-b-0"> Providers </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                            <div class="card">
                                                <div class="card-block box-shadow">
                                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                                        <span class="directory_name" style="display: none;">Models</span>
                
                                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                        <div class="m-l-10 align-self-center">
                                                            <h6 class="text-muted m-b-0"> Models </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 'routes','Controllers','Api','views','config','Exports','excel_view','auth','home','layouts','pdf_view','profile','reports','school-form-status','config','api','errors' -->
                     
                        
                        <div class="col-md-12 col-sm-12 col-lg-12 col-xl-12 col-xs-12">
                            <div class="card">
                                <div class="card-block box-shadow">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success"><i class="fa fa-folder"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h6 class="text-muted m-b-0"><u>Views Directory</u>  </h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">views</span>
            
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0"> Views Files</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">excel_view</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0">  Excel View  </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">home</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0"> Home </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">layouts</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0">Layout </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">pdf_view</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0">Pdf View </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">profile</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0">Profile </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">reports</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0">Reports </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                                        <div class="card">
                                            <div class="card-block box-shadow">
                                                <div class="d-flex flex-row" style="cursor:pointer;">
                                                    <span class="directory_name" style="display: none;">school-form-status</span>
                                                    <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                                    <div class="m-l-10 align-self-center">
                                                        <h6 class="text-muted m-b-0">School Form Status </h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                      
                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                            <div class="card">
                                <div class="card-block box-shadow">
                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                        <span class="directory_name" style="display: none;">routes</span>
                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h6 class="text-muted m-b-0"> routes </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-4 col-xl-3 col-xs-12 ftp-card">
                            <div class="card">
                                <div class="card-block box-shadow">
                                    <div class="d-flex flex-row" style="cursor:pointer;">
                                       <span class="directory_name" style="display: none;">config</span>
                                        <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h6 class="text-muted m-b-0"> config </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    
                </div>
            </div>
        </div>
        <br>
        <div class="row list-ftp-data">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">FTP Details  
                           <button class="btn btn-sm btn-danger back-to-card" style="float: right;margin-left: 4px;">
                                <i class="fa fa-arrow-left"></i>
                            </button>
                        
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sl. </td>
                                    <th>Name </th>
                                    <th>Location </th>
                                    <th>Directory</th>
                                    <th>File Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="list-data">
                                <tr>
                                    <td>Sl. </td>
                                    <td>Name </td>
                                    <td>Location </td>
                                    <td>Directory</td>
                                    <td>Status</td>
                                    <td>Action</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <div class="row add-edit-ftp-data">
            <div class="col-md-12">
                <form action="{{url('add-edit-ftp')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">FTP Details</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-lg-3 col-xl-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="">Name </label>
                                        <input required type="text" name="full_name" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-lg-3 col-xl-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="">File Location (From root )</label>
                                        <input required type="text" name="user_name" class="form-control rte_input">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-lg-3 col-xl-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="">Directory</label>
                                        <select class="form-control rte_input" name="usertype" id="UserType" required>
                                            <option value='-1'>Select Directory </option>
                                            <option value="route">route</option>
                                            <option value="Controllers">Controllers</option>
                                            <option value="Api">Api</option>
                                            <option value="views">views</option>
                                            <option value="config">config</option>
                                            <option value="Exports">Exports</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-lg-3 col-xl-3 col-xs-12">
                                    <br>
                                    <button class="btn btn-primary btn_submit pull-right" style="float: right;">SUBMIT</button>
                                </div>
                            </div>

                        </div>
                    </div>
            </div>
            </form>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
<div class="modal fade " id="upload-document" tabindex="-1" role="dialog" aria-labelledby="uploadDocument" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Upload Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('upload-ftp-data-list')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                 <div class="modal-body">
                    <div class="form-group">
                         <input type="text" hidden name="upload_file_id" id="upload_file_id">
                        <input type="text" hidden value="update" name="is_upload" id="is_upload">
                         <input type="text" hidden name="upload_file_name" id="upload_file_name">
                         <input type="text" hidden name="upload_folder_name" id="upload_folder_name">
                         <input type="text" hidden name="upload_file_location" id="upload_file_location">
                         <input type="text" hidden name="upload_file_type" id="upload_file_type">
                         <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_upload" accept="php" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade " id="add-document" tabindex="-1" role="dialog" aria-labelledby="addDocument" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('upload-ftp-data-list')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-body">
                    <div class="form-group">
                            <label for="">Directory</label>
                            <select class="form-control rte_input" name="add_folder_name" id="add_folder_name" required>
                                <option value='-1'>--Select--</option>
                                <option value='routes'>Routes</option>
                                <option value='Controllers'>Controllers</option>
                                <option value='views'>Views</option>
                                <option value='config'>Config</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" value="add" name="is_upload" id="is_upload">
                        <label for="message-text" class="col-form-label">File :</label>
                        <input type="file" name="file_upload" accept="php" class="form-control" />
                    </div>
                    <div class="form-group">
                         <label for="message-text" class="col-form-label">File Name</label>
                        <input type="text" name="file_name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Remarks :</label>
                        <textarea name="remarks" type="text" class="form-control" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
@include('footer')
<script type="text/javascript">
    $(document).ready(function () {
        $('.ftp-cards').show();
        $('.list-ftp-data').hide();
        $('.add-edit-ftp-data').hide();


        $('.ftp-card').on('click',function(){
            $('.ftp-cards').hide();
            $('.list-ftp-data').show();
            $('.add-edit-ftp-data').hide();
            var directory_name = $(this).closest('div').find('.directory_name').text();
             $.ajax({
                    url: 'view-ftp-data-list/' + directory_name,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                         console.log(data);
                        $('#list-data').html(data['data']);
                    }
                });
        });
        $('.back-to-card').on('click',function(){
            $('.ftp-cards').show();
            $('.list-ftp-data').hide();
            $('.add-edit-ftp-data').hide();
           
        });
        $('#upload_button_id').on('click',function(){
            console.log($(this).closest('tr'));
            
        });
    });
    function uploadFunction(id){
        var id = id; 
      
        console.log('click');
        console.log('id = '+id);
        // console.log('file_name = '+file_name);
        // console.log('file_location = '+file_location);
        // console.log('directory_name = '+directory_name);
        // console.log('file_type = '+file_type);
        $('#upload_file_id').val( id );
        $.ajax({
                    url: 'view-ftp-data/' + id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log(data['0']['file_name']);
                        // $('#list-data').html(data);
                        $('#upload_file_name').val( data['0']['file_name']);
                        $('#upload_folder_name').val( data['0']['directory_name']);
                        $('#upload_file_location').val( data['0']['file_location']);
                        $('#upload_file_type').val( data['0']['file_type']);
                    }
                });
    }
</script>