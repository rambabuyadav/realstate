@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-10">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Report</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{url('report')}}">Report </a></li>
                        <li class="breadcrumb-item"><a href="">Payments Report </a></li>
                    </ul>
                </div>
                <div class="col-md-2 text-right pr-4">
                    <form method="post" action="{{url('report-payments')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" class='text-block' name="search_text" value="{{@$adv_data['search_text']}}" />
                        <input type="hidden" class='text-block' name="division" value="{{@$adv_data['division_id']}}" />
                        <input type="hidden" class='text-block' name="district" value="{{@$adv_data['district_id']}}" />
                        <input type="hidden" class='text-block' name="block" value="{{@$adv_data['block_id']}}" />
                        <input type="hidden" class='text-block' name="excel" value="excel" />
                        <button type="submit" class="btn btn-sm btn-light btn-offset"  title="Export excel "><a class="text-success" title="Export excel " > <i class="fa fa-file-excel text-success" > </i>  </a></button>
                        <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
                        <a href="{{url('report-payments')}}"> <button type="button" class="btn btn-danger btn-sm pull-right" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <div class="card">
                <div class="card-body">
                    @include('message-flash')
                    <div class="container">
                        <div class="row">
                            <table class="table table-bordered table-responsive  table-sm ">
                                <thead>
                                    <tr style="width: 100%;">
                                    <th style="width: 10%;">Sl No.</th>
                                    <th style="width: 15%;">School Name</th>
                                    <th style="width: 10%;">School No</th>
                                    <th style="width: 15%;">School Address</th>
                                    <th style="width: 15%;">Contact Person</th>
                                    <th style="width: 15%;">Contact Person No</th>
                                    <th style="width: 15%;">Transaction id</th>
                                    <th style="width: 15%;">Payment Type</th>
                                    <th style="width: 10%;">Payment Amount</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i=1;
                                    @endphp
                                    @if(isset($payments))
                                 @foreach($payments as $key => $payment)
                                  <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$payment->school_name}}</td>
                                    <td>{{$payment->phone_no}}</td>
                                    <td title="{{$payment->village_city}}&#10; {{$payment->post_office}} {{$payment->panchayat}} &#10;;{{$payment->block}} &#10;{{$payment->district}} ">
                                      @if($payment->block){{$payment->block}},&nbsp;
                                      @endif
                                      @if($payment->district){{$payment->district}}
                                      @endif
                                    </td>
                                    <td>{{$payment->contact_name}}</td>
                                    <td>{{$payment->mobile_no}}</td>
                                    <td>{{$payment->t_id}}</td>
                                    <td>{{$payment->paid_method}}</td>
                                    <td>{{$payment->payment_amount}}</td>
                                      </tr>
                                       @php
                                    $i++;
                                    @endphp
                                    
                                    @endforeach
                                    @endif
                                    @if($total_amount == 0 || $total_amount == '0')
                                    <tr>
                                      <th colspan="10" style="text-align: center;"><span>No payments Found!!</span></th>
                                    </tr>
                                    @else
                                    <tr>
                                      <th colspan="8"><span style="float: right;"> Total</span></th>
                                      <th>{{$total_amount}}</th>
                                    </tr>
                                    @endif
                                   
                                </tbody>
                            </table>
                            {{-- Pagination --}}
                            {{ $payments->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- [ Main Content ] end -->
</div>
</div>
  <!-- [ Adv Search Model ] -->
  <div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="{{url('report-payments')}}"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                   
                    <div class="form-group">
                        <label for=""> Advance Search </label>
                    @if(isset($adv_data['search_text']))
                    <input type="text" name="search_text" value="{{@$adv_data['search_text']}}" class="form-control rte_input" id="search_text" placeholder="School name/ Application no./ Email/ Phone">
                    @else
                    <input type="text" name="search_text" type="text" class="form-control" placeholder="School name/ Application no./ Email/ Phone">
                    @endif
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for=""> Division </label>
                        <select class="form-control rte_input" name="division" id="division" >
                            @isset($adv_data['division_id'])
                            <option value="{{@$adv_data['division_id']}}">{{@$adv_data['divisionName']}}</option>
                            @endisset
                            <option value="">--Select--</option>
                            @foreach($division_list as $key => $value)
                            <option value="{{$key}}">{{$value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                 
                    <div class="form-group">
                        <label for=""> District</label>
                        <select class="form-control rte_input" name="district"  id="district">
                            @isset($adv_data['district_id'])
                            <option value="{{@$adv_data['district_id']}}">{{@$adv_data['districtName']}}</option>
                            @endisset
                            <option value="">--Select--</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for=""> Block </label>
                        <select class="form-control rte_input" name="block" id="block" >
                            @isset($adv_data['block_id'])
                            <option value="{{@$adv_data['block_id']}}">{{@$adv_data['blockName']}}</option>
                            @endisset
                            <option value="">--Select--</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
    
              </div>
    </form>
      </div>
    </div>
  </div>
  <!-- [ Adv Search End ] -->
@include('footer')
<script>
    $(document).ready(function () {
        $('select[name="division"]').on('change', function () {
            var stateID = $(this).val();
            var options = '<option value="">---Select---</option>';
            if (stateID) {
                $.ajax({
                    url: 'district/district_list/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('district ')
                        console.log(data);
                        Object.entries(data).forEach(entry => {
                            const [key, value] = entry;
                            options += '<option value="' + key + '">' + value + '</option>';
                            console.log(key, value);
                            });
                            $('#district').html(options);
                            $('#block').html('<option value="">---Select---</option>');
                            
                        }
                    });
                } else {
                    $('#district').empty();
                }
            });
            
            $('select[name="district"]').on('change', function () {
                var stateID = $(this).val();
                var options = '<option value="">--Select--</option>';
                if (stateID) {
                    $.ajax({
                        url: 'block/block_list/' + stateID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            
                            console.log('block ');
                            console.log(data);
                            
                            Object.entries(data).forEach(entry => {
                                const [key, value] = entry;
                                options += '<option value="' + key + '">' + value + '</option>';
                                console.log(key, value);
                                });
                            $('#block').html(options);
                    }
                });
            } else {
                $('#block').empty();
            }
        });
    });
</script>