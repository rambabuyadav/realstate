@include('header')
@include('sidenav')
@include('topbar')
<style>
    tr.hiddenRow {
        display: none;
    }
</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Report</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{url('report')}}">Report </a></li>
                            <li class="breadcrumb-item"><a href="">Verified Applications Report </a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 text-right pr-4">
                        <button type="button" class="btn btn-sm btn-light btn-offset"><a class="text-success" title="Export "> <i class="fa fa-file-excel text-success"> </i> </a></button>
                        <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->

        <div class="row">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-header"> Report</div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <table class="table table-bordered table-sm ">
                                    <thead>
                                        <tr>
                                            <th>Apl. no.</td>
                                            <th>School</th>
                                            <th>Block</th>
                                            <th>Division</th>
                                            <th>District</th>
                                            <th>Status</th>
                                            <th>Apl Dt.</th>
                                            <th title="Remaining Date">End Date </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $i=0;
                                        @endphp
                                        @foreach($TotalData['AllDetails'] as $Report)
                                        <tr>
                                            <td>{{$Report->apllication_id}} </td>
                                            @php
                                            $id = Crypt::encrypt(@$Report->id);
                                            @endphp
                                            <td><a href='show-data/{{$id}}'>{{@$Report->school_name}} </a></td>
                                            <td>{{$Report->block}}</td>
                                            <td>{{$Report->Division}}</td>
                                            <td>{{$Report->disrict}}</td>
                                            <td>{{$Report->status_name}}
                                                @if($Report->updated_at==null)

                                                @else
                                                ( {{\Carbon\Carbon::parse ($Report->updated_at)->format('d/m/Y')}} )
                                                @endif
                                            </td>
                                            <td>
                                                @if($Report->created_at==null)
                                                ---
                                                @else
                                                {{\Carbon\Carbon::parse ($Report->created_at)->format('d/m/Y')}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($Report->status_name == 'Certified') 
                                                    ---
                                                @else
                                                    @if($Report->startDate==null) 
                                                        ---
                                                    @else
                                                        {{\Carbon\Carbon::parse ($Report->updated_at)->addDays($Report->endDay)->format('d/m/Y')}} 
                                                        @if( $Report->RemainingDate > 5 )
                                                            <span class="text-success"> ( {{ $Report->RemainingDate }} days ) </span>
                                                        @elseif($Report->RemainingDate < 5 && $Report->RemainingDate > 0 )
                                                            <span class="text-warning"> ( {{ $Report->RemainingDate }} days ) </span>
                                                        @else
                                                            <span class="text-danger"> (Delayed ) </span>
                                                        @endif
                                                    @endif    
                                                @endif
                                            </td>

                                            @php
                                            $i++;
                                            @endphp
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- [ Main Content ] end -->
    </div>
</div>
<!-- [ Adv Search Model ] -->
<div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{url('advSearch')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""> Advance Search Index </label>
                                @isset($TotalData['ReturnRequest'])
                                <input type="text" name="adv_search" id="adv_searchtrue" value="{{$TotalData['ReturnRequest'][0]}}" class="form-control rte_input" placeholder="School/ Application no./ E-mail/ Mobile">
                                @else
                                <input type="text" name="adv_search" id="adv_search" class="form-control rte_input" placeholder="School/ Application no./ E-mail/ Mobile">
                                @endisset
                                <input type="hidden" name="search_status" value="Verifed" id="search_status">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Division </label>
                                <select class="form-control rte_input" name="division" id="division">
                                    @isset($TotalData['ReturnRequest'])
                                    @if($TotalData['ReturnRequest'][1][0])
                                    <option value="{{ $TotalData['ReturnRequest'][1][0]->id }}">{{ $TotalData['ReturnRequest'][1][0]->value_set_name }}</option>
                                    @endif
                                    @endisset
                                    <option value="-1">Select Division </option>
                                    @foreach($TotalData['Districts'] as $District)
                                    <option value="{{$District->id}}">{{$District->value_set_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> District </label>
                                <select class="form-control rte_input" name="district" id="district">
                                    @isset($TotalData['ReturnRequest'])
                                    @if($TotalData['ReturnRequest'][2][0])
                                    <option value="{{ $TotalData['ReturnRequest'][2][0]->id }}">{{ $TotalData['ReturnRequest'][2][0]->display_value }}</option>
                                    @endif
                                    @endisset
                                    <option value="-1">Select District </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""> Block </label>
                                <select class="form-control rte_input" name="block" id="block">
                                    @isset($TotalData['ReturnRequest'])
                                    @if($TotalData['ReturnRequest'][3][0])
                                    <option value="{{ $TotalData['ReturnRequest'][3][0]->id }}">{{ $TotalData['ReturnRequest'][3][0]->display_value }}</option>
                                    @endif
                                    @endisset
                                    <option value="-1">Select Block</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">

                </div>
            </form>
        </div>
    </div>
</div>
<!-- [ Adv Search End ] -->
@include('footer')
<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="division"]').on('change', function () {

            var stateID = $(this).val();
            var options = '<option value="-1">Select District </option>';
            if (stateID) {
                $.ajax({
                    url: 'DGR/block/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('Block ')
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            var id = data[i]['id'];
                            var value_set_name = data[i]['display_value'];
                            //do something with k or data...
                            options += '<option value="' + id + '">' + value_set_name + '</option>';
                        }
                        $('#district').html(options);
                        $('#block').html('<option value="-1">Select Block  </option>');
                    }
                });
            } else {
                $('#district').empty();
            }
        });
        $('select[name="district"]').on('change', function () {
            var stateID = $(this).val();


            var options = '<option value="-1">Select Block  </option>';
            if (stateID) {
                $.ajax({
                    url: 'DGR/village/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('Village ');
                        console.log(data);

                        for (var i = 0; i < data.length; i++) {
                            var id = data[i]['id'];
                            var value_set_name = data[i]['display_value'];
                            //do something with k or data...
                            options += '<option value="' + id + '">' + value_set_name + '</option>';
                        }
                        $('#block').html(options);
                    }
                });
            } else {
                $('#block').empty();
            }
        });

    });
</script>