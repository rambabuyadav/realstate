@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Report </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                            <li class="breadcrumb-item"><a href="">All Reports</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12 ">
                <!-- <h1>Here is all Payment Related Details</h1> -->
                <div class="shadow-lg p-3 mt--6 bg-white rounded">
                    <div class="row" ng-show="views.list">
                        <div class="col-md-12 row" style="margin-top: 17px;">
                            <h3 class="card-title" style="margin-left: 20px;"></h3>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('report-all-applications')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'administrator'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="schoolsList('StudentLoginStatsshow')">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-alt"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">All Applications</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!-- <a href="{{url('reportVerifiedApplications')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="studentExamReportshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-upload"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Verified Applications</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a> -->
                            <a href="{{url('report-delayed-applications')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="StaffLoginReportsshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-import"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Delayed Applications</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{url('report-rdde-appeals')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <!-- <div class="card"> -->
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="usersStats()">
                                            <div class="round align-self-center round-success"><i class="fa fa-praying-hands"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">RDDE Appeals</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </a>
                            <a href="{{url('report-registered-school')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin' || $root.dashboardData.role == 'account'">
                                    <!-- <div class="card"> -->
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="changeView('paymentsReports')">
                                            <div class="round align-self-center round-success"><i class="fa fa-clipboard"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Registered Schools </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('report-pending-applications')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="studentExamReportshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-file-upload"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Pending Applications</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{url('reportRejectedApplications')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="studentExamReportshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-times"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Rejected Applications</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{url('report-dpe-appeals')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin' || $root.dashboardData.role == 'account'">
                                    <!-- <div class="card"> -->
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="changeView('paymentsReports')">
                                            <div class="round align-self-center round-success"><i class="fa fa-praying-hands"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Director of Primary Education Appeals</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="{{url('report-approved-applications')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'admin'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="studentExamReportshow()">
                                            <div class="round align-self-center round-success"><i class="fa fa-check"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Approved Applications</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{url('report-dc-meetings')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'administrator'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="schoolsList('StaffLoginReportsshow')">
                                            <div class="round align-self-center round-success"><i class="fa fa-handshake"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">District Committee Meetings</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{url('report-payments')}}">
                                <div class="card" ng-show="$root.dashboardData.role == 'administrator'">
                                    <div class="card-block box-shadow">
                                        <div class="d-flex flex-row" style="cursor:pointer;" ng-click="schoolsList('StaffLoginReportsshow')">
                                            <div class="round align-self-center round-success"><i class="fa fa-rupee-sign"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h6 class="text-muted m-b-0">Payments</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="gfgmodal" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="gfgmodallabel">Selected row</h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="GFGclass" id="divGFG"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
@include('footer')