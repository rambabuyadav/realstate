@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-10">
                    <div class="page-header-title">
                        <h5 class="m-b-10">
                            <div class="btn-group">
                                <form method="post" action="{{url('report-dpe-appeals')}}" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                  <input type="hidden" class='text-block' name="application_status" value="0">
                                  <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">All</button>
                                </form>
                                <form method="post" action="{{url('report-dpe-appeals')}}" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                  <input type="hidden" class='text-block' name="application_status" value="Pending">
                                  <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Pending </button>
                                </form>
                                <form method="post" action="{{url('report-dpe-appeals')}}" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                  <input type="hidden" class='text-block' name="application_status" value="Accepted">
                                  <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Accepted</button>
                                </form>
                              
                                <form method="post" action="{{url('report-dpe-appeals')}}" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                  <input type="hidden" class='text-block' name="application_status" value="Rejected">
                                  <button type="submit" class="btn btn-sm btn-primary btn-offset" title="">Rejected</button>
                                </form>
                              
                              </div>
              
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{url('report')}}">Report </a></li>
                        <li class="breadcrumb-item"><a href="">Director of Primary Education Appeals Report </a></li>
                    </ul>
                </div>
                <div class="col-md-2 text-right pr-4">
                    <form method="post" action="{{url('report-dpe-appeals')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" class='text-block' name="search_text" value="{{@$adv_data['search_text']}}" />
                        <input type="hidden" class='text-block' name="division" value="{{@$adv_data['division_id']}}" />
                        <input type="hidden" class='text-block' name="district" value="{{@$adv_data['district_id']}}" />
                        <input type="hidden" class='text-block' name="block" value="{{@$adv_data['block_id']}}" />
                        <input type="hidden" class='text-block' name="excel" value="excel" />
                        <button type="submit" class="btn btn-sm btn-light btn-offset"  title="Export excel "><a class="text-success" title="Export excel " > <i class="fa fa-file-excel text-success" > </i>  </a></button>
                        <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
                        <a href="{{url('report-dpe-appeals')}}"> <button type="button" class="btn btn-danger btn-sm pull-right" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <div class="card">
                <div class="card-body">
                    @include('message-flash')
                    <div class="container">
                        <div class="row">
                            <table class="table table-bordered  table-sm ">
                                <thead>
                                    <tr>
                                        <th>Apl. no.</td>
                                        <!-- <th>Appeal no.</th> -->
                                        <th>School</th>
                                        <th>Block</th>
                                        <th>Division</th>
                                        <th>District</th>
                                        <th>Status</th>
                                        <th>Appeal Dt.</th>
                                        <th title="Remaining Date">End Date </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i=0;
                                    @endphp
                                    @foreach($AllDetails as $Report)
                                    <tr>
                                        <td>{{$Report->apllication_id}} </td>
                                        <!-- <td> </td> -->
                                        @php
                                        $id = Crypt::encrypt(@$Report->id);
                                        @endphp
                                        <td><a href = 'show-data/{{$id}}'>{{@$Report->school_name}} </a></td>
                                        <td>{{$Report->block}}</td>
                                        <td>{{$Report->Division}}</td>
                                        <td>{{$Report->disrict}}</td>
                                        <td>{{$Report->status_name}}
                                            @if($Report->updated_at==null)
                                          
                                        @else
                                            ( {{\Carbon\Carbon::parse ($Report->updated_at)->format('d/m/Y')}} ) 
                                        @endif    
                                        </td>
                                        <td>
                                            @if($Report->created_at==null)
                                                ---
                                            @else
                                                {{\Carbon\Carbon::parse ($Report->created_at)->format('d/m/Y')}}
                                            @endif    
                                        </td>
                                        <td>
                                             @if($Report->status_name == 'Certified') 
                                                    ---
                                                @else
                                                    @if($Report->startDate==null) 
                                                        ---
                                                    @else
                                                        {{\Carbon\Carbon::parse ($Report->updated_at)->addDays($Report->endDay)->format('d/m/Y')}} 
                                                        @if( $Report->RemainingDate > 5 )
                                                            <span class="text-success"> ( {{ $Report->RemainingDate }} days ) </span>
                                                        @elseif($Report->RemainingDate < 5 && $Report->RemainingDate > 0 )
                                                            <span class="text-warning"> ( {{ $Report->RemainingDate }} days ) </span>
                                                        @else
                                                            <span class="text-danger"> (Delayed ) </span>
                                                        @endif
                                                    @endif    
                                                @endif    
                                        </td>
                                         
                                        @php
                                        $i++;
                                        @endphp
                                    </tr>
                                    @endforeach
                                     @if($i == 0)
                                    <tr>
                                      <th colspan="8" style="text-align: center;"><span>No Director of Primary Education Appeals Report Found!!</span></th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{-- Pagination --}}
                            {{ $AllDetails->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- [ Main Content ] end -->
</div>
</div>
 <!-- [ Adv Search Model ] -->
 <div class="modal fade bd-adv-search-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="{{url('report-dpe-appeals')}}"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="exampleModalLabel1">Advance Search </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                   
                    <div class="form-group">
                        <label for=""> Advance Search </label>
                    @if(isset($adv_data['search_text']))
                    <input type="text" name="search_text" value="{{@$adv_data['search_text']}}" class="form-control rte_input" id="search_text" placeholder="School name/ Application no./ Email/ Phone">
                    @else
                    <input type="text" name="search_text" type="text" class="form-control" placeholder="School name/ Application no./ Email/ Phone">
                    @endif
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for=""> Division </label>
                        <select class="form-control rte_input" name="division" id="division" >
                            @isset($adv_data['division_id'])
                            <option value="{{@$adv_data['division_id']}}">{{@$adv_data['divisionName']}}</option>
                            @endisset
                            <option value="">--Select--</option>
                            @foreach($division_list as $key => $value)
                            <option value="{{$key}}">{{$value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                 
                    <div class="form-group">
                        <label for=""> District</label>
                        <select class="form-control rte_input" name="district"  id="district">
                            @isset($adv_data['district_id'])
                            <option value="{{@$adv_data['district_id']}}">{{@$adv_data['districtName']}}</option>
                            @endisset
                            <option value="">--Select--</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for=""> Block </label>
                        <select class="form-control rte_input" name="block" id="block" >
                            @isset($adv_data['block_id'])
                            <option value="{{@$adv_data['block_id']}}">{{@$adv_data['blockName']}}</option>
                            @endisset
                            <option value="">--Select--</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
    
              </div>
    </form>
      </div>
    </div>
  </div>
  <!-- [ Adv Search End ] -->
@include('footer')
<script>
    $(document).ready(function () {
        $('select[name="division"]').on('change', function () {
            var stateID = $(this).val();
            var options = '<option value="">---Select---</option>';
            if (stateID) {
                $.ajax({
                    url: 'district/district_list/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('district ')
                        console.log(data);
                        Object.entries(data).forEach(entry => {
                            const [key, value] = entry;
                            options += '<option value="' + key + '">' + value + '</option>';
                            console.log(key, value);
                            });
                            $('#district').html(options);
                            $('#block').html('<option value="">---Select---</option>');
                            
                        }
                    });
                } else {
                    $('#district').empty();
                }
            });
            
            $('select[name="district"]').on('change', function () {
                var stateID = $(this).val();
                var options = '<option value="">--Select--</option>';
                if (stateID) {
                    $.ajax({
                        url: 'block/block_list/' + stateID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            
                            console.log('block ');
                            console.log(data);
                            
                            Object.entries(data).forEach(entry => {
                                const [key, value] = entry;
                                options += '<option value="' + key + '">' + value + '</option>';
                                console.log(key, value);
                                });
                            $('#block').html(options);
                    }
                });
            } else {
                $('#block').empty();
            }
        });
    });
</script>