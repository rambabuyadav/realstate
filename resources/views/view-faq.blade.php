@include('header')
@include('sidenav')
@include('topbar')
<style>
    td {
        border: 0.5px solid #dad8d8;
        padding: 2px;
    }
    th {
        border: 0.5px solid #e7e4e4;
        padding: 2px;
        text-align: center;
        background-color: #ecf0f5;
    }
</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">FAQ</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('district')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="fndSettings"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="">FAQ </a></li>
                        </ul>
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <!-- school details -->
                <div class="card">
                    <div class="card-header">FAQ
                        <a href="Add-FAQ" style="float:right">
                            <button class="btn btn-light text-success btn-sm ">
                                <i class="fa fa-plus " title="Add FAQ "></i>
                            </button>
                        </a>
                    </div>
                    <div class="card-body">
                        <table style="  width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sl. </td>
                                    <th>Heading </th>
                                    <th>Text</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($FAQs as $k=>$FAQ)
                                <tr>
                                    <td class='ID' style="text-align: center;width: 5%;">{{$k+1}} </td>
                                    <td class='Name' style="width: 30%;"> @php echo htmlspecialchars_decode(stripslashes($FAQ->heading)) @endphp </td>
                                    
                                    <td class='Name' style="width: 50%;word-wrap: break-all;">
                                        
                                            @php echo htmlspecialchars_decode(stripslashes($FAQ->text)) @endphp
                                     
                                    </td>
                                    <td class='Name' style="text-align:center;width: 5%;"> 
                                    @if($FAQ->status==1)
                                         <span class="text-success">Active</span>
                                    @else
                                    <span class="text-danger">Inactive</span>
                                    @endif
                                    </td>
                                    <td style="text-align:center; width: 10%;">
                                        <a href="editFAQ/{{$FAQ->id}}"><button class="btn btn-light text-primary btn-sm "><i class="fa fa-edit " title="Edit"></i></button></a>
                                        <a href="delete-FAQ/{{$FAQ->id}}">
                                            
                                            @if($FAQ->status==1)
                                            <button class="btn btn-light text-success btn-sm">
                                                <i class="fas fa-toggle-on"></i>
                                            </button>
                                            @else
                                            <button class="btn btn-light text-danger btn-sm">
                                                <i class="fas fa-toggle-off"></i>
                                            </button>
                                            @endif
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>