<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>paatham.in</title>
<link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
    body {
		font-family: 'Varela Round', sans-serif;
	}
	.modal-confirm {		
		color: #636363;
		width: 70%;
		margin-top:8%;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	
	
</style>
</head>
<body>
<!-- Modal HTML -->
<div id="myModal" class="modal fade in" style="display:block;">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content"><br>
        <div class="text-center">
            <img src="assets/success1.png" style="width:140px; height:140px;">
            <!-- <img src="assets/failed.png" style="width:140px; height:140px;"> -->
        </div>
       <!-- <div class="text-center"> <span style="font-size: 64px;"><b>Try again</b></span></div> -->
       <div class="text-center"> <span style="font-size: 64px;"><b>Thank You!</b></span></div>
       <div class="text-center"> <span style="font-size: 44px;">Your Payment Was Successfully Received </span></div>  <br>
       <!-- <div class="text-center"> <span style="font-size: 44px;">Your Payment Was Failed </span></div>  <br>   -->
       <div class="text-center text-primary"> <span style="font-size: 24px;"> <i class="fas fa-arrow-left"></i> Back To Home</span></div>
       <br><br> 
        <div class="row">
        <div class="col-md-3"></div>
        <div class="col-6">
            <table class="table " style="width:50%;">
                <thead >
                  <tr style="width: 100%;">
                    <th style="width: 50%;">Amount</th>
                    <th style="width: 50%;">4501245</th>
                  </tr>
                  <tr style="width: 100%;">
                    <th style="width: 50%;">Payment Method</th>
                    <th style="width: 50%;">PayPal</th>
                  </tr>
                  <tr style="width: 100%;">
                    <th style="width: 50%;">Charge Id</th>
                    <th style="width: 50%;">3245310245451</th>
                  </tr>
                  <tr style="width: 100%;">
                    <th style="width: 50%;">Transaction Id</th>
                    <th style="width: 50%;">4557577545</th>
                  </tr>
                  <tr style="width: 100%;">
                    <th style="width: 50%;">Date</th>
                    <th style="width: 50%;">20/12/2020 12:24 PM</th>
                  </tr>
                </thead>
              </table>
        </div>
        <div class="col-md-3"></div>
    </div>
    
   
        </div>
        </div>
</div>     
</body>
</html> 