@include('header')
@include('sidenav')
@include('topbar')
<style>.text-block {
    display: none;
  }</style>
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5     class="m-b-10">Appeal</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{url('appeal')}}">Appeal Details</a></li>
                        <li class="breadcrumb-item text-white">Appeal </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <form action = "{{url('addAppeal')}}" method = "post" enctype="multipart/form-data">
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        
    <div class="row">
      <div class="col-md-12 ">
          <!-- <h1>Here is all Payment Related Details</h1> -->
          <div class="shadow-lg p-4 mt--6 bg-white rounded">
<div class="row">
    <div class="col-4">
    <div class="form-group">  
    <label for="">Appeal Authority</label>
    <select class="form-control" id="my-select" name="appealAutherity" required >
            <option class="text-center" id="colorselector"> Select Appeal Authority</option>
            @php
            $appeal_count = DB::table('appeals')->where('school_id', Auth::user()->school_id)->count();
            @endphp
            @if($appeal_count == 0)
            <option value="1"> First Appellate Authority </option>
            @elseif($appeal_count == 1)
            <option value="2"> Second Appellate Authority </option>
            @endif
        </select>
    </div>
</div>
    <div id="1" class="col-4 text-block">
    <div class="form-group">  
    <label for="">Division</label>
        <input type="text"  class="form-control " readonly placeholder="Mr. Amit Kumar " value="Kolhan">
    </div>
</div>
    <div id="1" class="col-4 text-block">
    <div class="form-group">    
    <label for="">Officer name</label>
       <input type="text"  name="FAAOfiicerName"  class="form-control" readonly placeholder="Mr. Amit Kumar " value="Mr. Amit Kumar ">
    </div>
    </div>
    <div id="2" class="col-4 text-block">
    <div class="form-group">
        <label for="">Officer Name</label>
        <input type="text" name="SAAOfiicerName" class="form-control" readonly placeholder="Mr. Amit Kumar " value="Mr. Rajiv Prasad">
    </div>
    </div>
    
</div>
        <div class="row">
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal Date</label>
                <input type="date"  class="form-control" name="appealDate">
            </div>
            </div>
            
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal Number</label>
                <input type="number"  class="form-control" name="appealNumber">
            </div>
            </div>
            
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal By</label>
                <input type="text"  class="form-control" name="appealBy">
            </div>
            </div>
            
            <div class="col-4 text-block">
            <div class="form-group">
                <label for="">Appeal To</label>
                <input type="text"  class="form-control" name="appealTo">
            </div>
            </div>
            
            
            <div class="col-12">
                <div class="form-group">
                    <textarea rows="10" required name="appealText" class="form-control" placeholder="Write Your Appeal"></textarea>
                </div>
            </div>
            <div class="col-4 ">
                <div class="form-group">
                    <label for="">Upload Document</label>
                    <input type="file" multiple  class="form-control" name="school_file[]"  accept=".pdf, .xls, .xlsx">
                </div>
            </div>
            
            <div class="col-8 ">
                <div class="form-group">
                <div class="py-3"><input type="submit" style="float:right;" name="submit" class="btn btn-primary " value="Send Appeal"/></div>
            </div>
            </div>
        </div>
      </div>
    </div>
    </form>
    <!-- [ Main Content ] end -->
</div>
</div>
@include('footer')
<script>
    var textBlocks = $('.text-block');
$('#my-select').change(function() {  
    textBlocks.hide();
  
  // Show the text block that corresponds to
  // the select value
  textBlocks.filter('#' + this.value).show();
});
</script>