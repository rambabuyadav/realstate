@include('header')
@include('sidenav')
@include('topbar')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('district')}}"><i class="fa fa-file" aria-hidden="true"></i>
                            <li class="breadcrumb-item"><a href="{{url('fndSettings')}}"> Settings</a></li>
                            <li class="breadcrumb-item"><a href="">FndSettings</a></li>
                        </ul>
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('saveFndsettings')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" class='text-block' name="id" value="{{@$fnd_settingsget->id}}" />
                    <!-- school details -->
                    <div class="card">
                        <div class="card-header">Response Days Limit</div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days for Verification and sumittal by DSE</label>
                                            <input required type="text" name="verification_by_dse_limit" class="form-control rte_input" value="{{@$fnd_settingsget->verification_by_dse_limit}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days to set & hold meeting by DC</label>
                                            <input required type="text" name="schedule_meeting_by_dc_limit" class="form-control rte_input" value="{{@$fnd_settingsget->schedule_meeting_by_dc_limit}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days to take decision after meeting by DC</label>
                                            <input required type="text" name="take_decision_after_meeting_held_by_dc" class="form-control rte_input" value="{{@$fnd_settingsget->take_decision_after_meeting_held_by_dc}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days to take action by RDDE</label>
                                            <input required type="text" name="faa_decision_limit" class="form-control rte_input" value="{{@$fnd_settingsget->faa_decision_limit}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days to take action by Director of Primary Education</label>
                                            <input required type="text" name="saa_decision_limit" class="form-control rte_input" value="{{@$fnd_settingsget->saa_decision_limit}}">
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days to appeal by school</label>
                                            <input required type="text" name="school_appeal_limit" class="form-control rte_input" value="{{@$fnd_settingsget->school_appeal_limit}}">
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">No. of days for appeal to saa</label>
                                            <input required type="text" name="school_second_appeal_after_rejection_limit" class="form-control rte_input" value="{{@$fnd_settingsget->school_second_appeal_after_rejection_limit}}">
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-4"></div>
                                    <div class="col-md-3 col-4">
                                        <input class="btn btn-primary btn_submit" type="submit" name="submit" value="Update" style="float:right" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')