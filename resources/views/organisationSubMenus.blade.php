@include('header')
@include('sidenav')
@include('topbar')
<style>
  .w-5 {
    display: inline;
    height: 30px;
  }
</style>
<div class="loader-bg">
  <div class="loader-track">
    <div class="loader-fill"></div>
  </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
  <div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
      <div class="page-block">
        <div class="row align-items-center">
          <div class="col-md-10">
            <div class="page-header-title">
              <h5 class="m-b-10">Division </h5>
            </div>
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="dashboard"><i class="fa fa-tachometer-alt" aria-hidden="true"></i></a></li>
              <li class="breadcrumb-item"><a href="fndSettings"> Settings</a></li>
              <li class="breadcrumb-item"><a href="">Organisation / navigation</a></li>
            </ul>
          </div>
          <div class="col-md-2 text-right pr-4">

            <a href="" class="text-success" title="add "><button type="button" class="btn btn-sm btn-light btn-offset text-primary" title=" Add User"><i class="fa fa-plus text-success"> </i> </button></a>
            <button type="button" class="btn btn-sm btn-light btn-offset" data-toggle="modal" data-target=".bd-adv-search-modal-lg" title="Adv. Search"> <i class="fa fa-search text-primary"> </i> </button>
            <button type="button" class="btn btn-danger btn-sm pull-right" onclick="window.location.reload()" title="Click to refresh filter"><i class="fa fa-sync" title="Click to refresh filter"></i></button>

          </div>
        </div>
      </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row">
      <div class="col-md-12 ">
        <!-- <h1>Here is all Payment Related Details</h1> -->
        <div class="shadow-lg p-3 mt--6 bg-white rounded">
          

          
          <table class="table table-bordered table-sm ">
            <thead>
              <tr>
                <th>Sl. </td>
                <th class="text-center">Submenu Pages</th>
                <th>Created At</th>
                <th style="text-align:center;width:8%;">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class='ID'> 1</td>
                <td class='Name'><a href="">Management</a></td>
                <td class='Date'></td>

                <td style="text-align:center;">
                  <a href=""><button class="btn btn-light text-primary btn-sm "><i class="fa fa-edit " title="Edit"></i></button></a>
                  <a href=""><button class="btn btn-light text-danger btn-sm"><i class="fa fa-trash" title="Delete"></i></button></a>
                </td>
              </tr>
              
              <tr>
                <td class='ID'> 2</td>
                <td class='Name'><a href="">Senior Functionaries</a></td>
                <td class='Date'></td>

                <td style="text-align:center;">
                  <a href=""><button class="btn btn-light text-primary btn-sm "><i class="fa fa-edit " title="Edit"></i></button></a>
                  <a href=""><button class="btn btn-light text-danger btn-sm"><i class="fa fa-trash" title="Delete"></i></button></a>
                </td>
              </tr>

              <tr>
                <td class='ID'> 3</td>
                <td class='Name'><a href="">Disclosure Under RTI</a></td>
                <td class='Date'></td>

                <td style="text-align:center;">
                  <a href=""><button class="btn btn-light text-primary btn-sm "><i class="fa fa-edit " title="Edit"></i></button></a>
                  <a href=""><button class="btn btn-light text-danger btn-sm"><i class="fa fa-trash" title="Delete"></i></button></a>
                </td>
              </tr>

              <tr>
                <td class='ID'> 4</td>
                <td class='Name'><a href="">Programme Advisory Committee</a></td>
                <td class='Date'></td>

                <td style="text-align:center;">
                  <a href=""><button class="btn btn-light text-primary btn-sm "><i class="fa fa-edit " title="Edit"></i></button></a>
                  <a href=""><button class="btn btn-light text-danger btn-sm"><i class="fa fa-trash" title="Delete"></i></button></a>
                </td>
              </tr>
            </tbody>
          </table>

        </div>
        <!-- [ Main Content ] end -->
        <!-- [ Adv Search Model ] -->

        <!-- [ Adv Search End ] -->
        @include('footer')

        <script type="text/javascript">
          $(document).ready(function() {
            $('#division').on('change', function() {
              var stateID = $(this).val();
              console.log(stateID);
              var options = '<option value="-1">Select District </option>';
              if (stateID) {
                $.ajax({
                  url: 'DGR/block/' + stateID,
                  type: "GET",
                  dataType: "json",
                  success: function(data) {
                    console.log('Block ')
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                      var id = data[i]['id'];
                      var value_set_name = data[i]['display_value'];
                      //do something with k or data...
                      options += '<option value="' + id + '">' + value_set_name + '</option>';
                    }
                    $('#district').html(options);
                    $('#block').html('<option value="-1">Select Block  </option>');
                  }
                });
              } else {
                $('#district').empty();
              }
            });
          });
        </script>