<div>

    <title>Payment Details</title>

    <table class="table table-bordered">                            

        <thead>

             <tr>

                <th>Sl No.</th>

                <th>School Name</th>

                <th>Appeal To</th>

                <th>Appeal Date</th>

                <th>Pending At</th>

                @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc')

                <th>Remaining Replied Date</th>

                @endif

                <th>Appeal Status</th>

             </tr>

           </thead>

           <tbody>

           @foreach($datalist as $k=>$val)

           <tr>

          

                <td>{{++$k}}</td>

                <td>{{($val->school_name)}}</td>

                <td>

                  @if ( $val->appellate_authority_type =='1')

                  RDDE

                  @elseif($val->appellate_authority_type =='2')

                  Director of Primary Education

                  @endif

                </td>

                <td>{{\Carbon\Carbon::parse ($val->created_at)->format('d/m/Y')}}</td>

                <td>FAA</td>

                @if(Auth::user()->user_role=='dse' || Auth::user()->user_role=='dc')

                <td>{{\Carbon\Carbon::parse ($val->created_at)->addDays(90)->format('d/m/Y')}}</td>

                @endif

                <td>

                  @if ( $val->remarks_status =='1')

                  <span class="text-success"> Accepted </span>

                  @elseif($val->remarks_status =='2')

                  <span class="text-danger"> Rejected </span>

                  @else

                  <span class="text-warning"> Requested </span>

                  @endif

                </td>

                <!-- <td>{{($val->panchayat)}}</td>

                <td>{{($val->post_office)}}</td>

                <td>{{($val->village_city)}}</td>

                <td>{{($val->phone_no)}}</td>

                <td>{{($val->contact_name)}}</td>

                <td>{{($val->mobile_no)}}</td>

                <td>{{($val->t_id)}}</td>

                <td>{{($val->paid_method)}}</td>

                <td>{{($val->payment_amount)}}</td> -->

            </tr> 

             @endforeach

           </tbody>

         </table>



</div>