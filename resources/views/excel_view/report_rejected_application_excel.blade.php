<div>
    <title>Rejected Application Details</title>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Apl. no.</th>
                <th>School</th>
                <th>Block</th>
                <th>Division</th>
                <th>District</th>
                <th>Status</th>
                <th>Apl Dt.</th>
                <th>End Date </th>
            </tr>
        </thead>
        <tbody>
            @php
            $i=0;


            @endphp
            @foreach($datalist as $key=>$Report)

            <tr>

                <td>{{$Report->apllication_id}} </td>
                <td>{{$Report->school_name}} </td>

                <td>{{$Report->Block}}</td>
                <td>{{$Report->Division}}</td>
                <td>{{$Report->Disrict}}</td>
                <td>{{$Report->status_name}}
                    @if($Report->updated_at==null)
                  
                @else
                    ( {{\Carbon\Carbon::parse ($Report->updated_at)->format('d/m/Y')}} ) 
                @endif    
                </td>
  
                <td>
                    @if($Report->created_at==null)
                        ---
                    @else
                        {{\Carbon\Carbon::parse ($Report->created_at)->format('d/m/Y')}}
                    @endif    
                </td>
  
                <td>
                     @if($Report->startDate==null)
                        ---
                    @else
                
                        {{\Carbon\Carbon::parse ($Report->updated_at)->addDays($Report->endDay)->format('d/m/Y')}} 
                        @if( $Report->RemainingDate > 5 )
                            <span class="text-success"> ( {{ $Report->RemainingDate }} days ) </span>
                        @elseif($Report->RemainingDate < 5 && $Report->RemainingDate > 0 )
                            <span class="text-warning"> ( {{ $Report->RemainingDate }} days ) </span>
                        @else
                            <span class="text-danger"> (Delayed ) </span>
                        @endif
                    @endif    
  
                </td>

                @php
                $i++;
                @endphp

            </tr>
            @endforeach


        </tbody>
    </table>






</div>