<div>
    <title>Payment Details</title>
    <table class="table table-bordered">                            
        <thead>
             <tr>
                <th>Sl No.</th>
                <th>School Name</th>
                <th>District</th>
                <th>Block</th>
                <th>Panchayat</th>
                <th>Post Office</th>
                <th>Village/city</th>
                <th>School No</th>
                <th>Contact Person</th>
                <th>Contact Person No</th>
                <th>Payment Date</th>
                <th>Transaction id</th>
                <th>Payment Type</th>
                <th>Payment Amount</th>
             </tr>
           </thead>
           <tbody>
           @foreach($datalist as $k=>$val)
           <tr>
          
                <td>{{$k +1}}</td>
                <td>{{($val->school_name)}}</td>
                <td>{{($val->district)}}</td>
                <td>{{($val->block)}}</td>
                <td>{{($val->panchayat)}}</td>
                <td>{{($val->post_office)}}</td>
                <td>{{($val->village_city)}}</td>
                <td>{{($val->phone_no)}}</td>
                <td>{{($val->contact_name)}}</td>
                <td>{{($val->mobile_no)}}</td>
                <td>{{\Carbon\Carbon::parse ($val->payment_date)->format('d/m/Y')}}</td>
                <td>{{($val->t_id)}}</td>
                <td>{{($val->paid_method)}}</td>
                <td>{{($val->payment_amount)}}</td>
            </tr> 
             @endforeach
             <tr>
              <th colspan="12">Total</th>
              <th>Total</th>
              <th>{{$total}}</th>
            </tr>
           </tbody>
         </table>

</div>