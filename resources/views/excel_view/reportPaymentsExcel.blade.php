<div>
    <title>Report Payments</title>
    <table class="table table-bordered">                            
        <thead>
             <tr>
                <th>Sl No.</th>
                <th>School Name</th>
                <th>School No</th>
                <th>School Address</th>
                <th>Contact Person</th>
                <th>Contact Person No</th>
                <th>Transaction id</th>
                <th>Payment Type</th>
                <th>Payment Amount</th>
                
             </tr>
           </thead>
           <tbody>
             @php
              $i=1;
            @endphp
           @foreach($datalist as $k=>$val)
           <tr>
          
                <td>{{$i}}</td>
                <td>{{($val->school_name)}}</td>
                <td>{{($val->phone_no)}}</td>
                <td title="{{$val->village_city}}&#10; {{$val->post_office}} {{$val->panchayat}} &#10;;{{$val->block}} &#10;{{$val->district}} ">
                  @if($val->block){{$val->block}},&nbsp;
                  @endif
                  @if($val->district){{$val->district}}
                  @endif
                </td>
                <td>{{$val->contact_name}}</td>
                <td>{{$val->mobile_no}}</td>
                <td>{{$val->t_id}}</td>
                <td>{{$val->paid_method}}</td>
                <td>{{$val->payment_amount}}</td>
               @php
                 $i++;
             @endphp
            </tr> 
             
             @endforeach
             
           </tbody>
         </table>

</div>