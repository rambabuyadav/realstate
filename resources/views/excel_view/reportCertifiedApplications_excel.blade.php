<div>
    <title>Report certified Applications</title>
    <table class="table table-bordered table-sm">
        <thead class="text-center">
            <tr>
                <th>Apl. no.</td>
                <th>School</th>
                <th>Block</th>
                <th>Division</th>
                <th>District</th>
               
                <th>Apl Dt.</th>
                
            </tr>
        </thead>
        <tbody>
           
            @foreach($datalist as $key=>$Report)

            <tr>

                <td>{{$Report->apllication_id}} </td>
                <td>{{@$Report->school_name}}</td>
                <td>{{$Report->Block}}</td>
                <td>{{$Report->Division}}</td>
                <td>{{$Report->Disrict}}</td>
                <td>
                    @if($Report->created_at==null)
                    ---
                    @else
                    {{\Carbon\Carbon::parse ($Report->created_at)->format('d/m/Y')}}
                    @endif
                </td>
         @endforeach
            </tr>
          
        </tbody>
    </table>
</div>