<div>
  <title>Rdde reports Details</title>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Apl. no.</th>
        <th>Appeal no.</th>
        <th>School</th>
        <th>Block</th>
        <th>District</th>
        <th>Appeal</th>
        <th>Appeal Dt.</th>
        <th title="Remaining Date">End Date </th>
      </tr>
    </thead>
    <tbody>
      @php
      $i =0
      @endphp
      @foreach($datalist as $k=>$val)
      <tr>

        <td>{{$val->apllication_id}} </td>
        <td><a href='showData/{{@$val->id}}'>{{@$val->school_name}} </a></td>

        <td>{{$val->block}}</td>
        <td>{{$val->Division}}</td>
        <td>{{$val->disrict}}</td>
        <td>{{$val->status_name}}
          @if($val->updated_at==null)
        
      @else
          ( {{\Carbon\Carbon::parse ($val->updated_at)->format('d/m/Y')}} ) 
      @endif    
      </td>

      <td>
          @if($val->created_at==null)
              ---
          @else
              {{\Carbon\Carbon::parse ($val->created_at)->format('d/m/Y')}}
          @endif    
      </td>

      <td>
           @if($val->startDate==null)
              ---
          @else
      
              {{\Carbon\Carbon::parse ($val->updated_at)->addDays($val->endDay)->format('d/m/Y')}} 
              @if( $val->RemainingDate > 5 )
                  <span class="text-success"> ( {{ $val->RemainingDate }} days ) </span>
              @elseif($val->RemainingDate < 5 && $val->RemainingDate > 0 )
                  <span class="text-warning"> ( {{ $val->RemainingDate }} days ) </span>
              @else
                  <span class="text-danger"> (Delayed ) </span>
              @endif
          @endif    

      </td>

        @php
        $i++
        @endphp
      </tr>
      @endforeach
    </tbody>
  </table>

</div>