<div>

    <title>Application Details</title>

    <table class="table table-bordered">                            

        <thead>

             <tr>

                <th>Sl No.</th>

                <th>School Name</th>

                <th>District</th>

                <th>Block</th>

                <th>Police Station</th>

                <th>Panchayat</th>

                <th>Post Office</th>

                <th>Pin Code</th>

                <th>Village/city</th>

                <th>Classes</th>

                <th>School Phone No.</th>

                <th>School Email</th>

                <th>Contact Person</th>

                <th>Contact Person No.</th>

                <th>Contact Person Email</th>

                <th>End Date</th>

                <!-- <th>Application Opning Date</th>

                <th>Vailed Till</th> -->

             </tr>

           </thead>

           <tbody>

           @foreach($datalist as $k=>$val)

           <tr>

          

                <td>{{$k+1}}</td>

                <td>{{($val->school_name)}}</td>

                <td>{{($val->district)}}</td>

                <td>{{($val->block)}}</td>

                <td>{{($val->police_station)}}</td>

                <td>{{($val->panchayat)}}</td>

                <td>{{($val->post_office)}}</td>

                <td>{{($val->pincode)}}</td>

                <td>{{($val->village_city)}}</td>

                <td>{{($val->from_class)}} to {{($val->upto_class)}}</td>

                <td>{{($val->phone_with_std_code)}}</td>

                <td>{{($val->email)}}</td>

                <td>{{($val->school_chairman_name)}}</td>

                <td>{{($val->school_chairman_phone_number)}}</td>

                <td>{{($val->school_chairman_email)}}</td>

               <td>{{\Carbon\Carbon::parse ($val->created_at)->addDays(30)->format('d/m/Y')}}

               </td>

                <!-- <td>{{\Carbon\Carbon::parse($val->opening_date)->format('d/m/Y')}}</td>

                <td>{{\Carbon\Carbon::parse($val->society_registration_valid_upto)->format('d/m/Y')}}</td> -->

            </tr> 

             @endforeach

           </tbody>

         </table>



</div>