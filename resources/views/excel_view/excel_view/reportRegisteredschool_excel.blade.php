<div>
    <title>Registered school Report</title>
    <table class="table table-bordered">                            
        <thead>
             <tr>
                <th>Sl No.</th>
                <th>School</th>
                <th>Block</th>
                <th>Division</th>
                <th>District</th>
                <th>Status</th>
                <th>Apl Dt</th>
                <th>End Date</th>
                
             </tr>
           </thead>
           <tbody>
             @php
              $i=1;
            @endphp
           @foreach($datalist as $k=>$val)
           <tr>
          
                <td>{{$i}}</td>
                <td>{{($val->school_name)}}</td>
                <td>{{($val->block)}}</td>
                <td>{{($val->Division)}}</td>
                <td>{{($val->disrict)}}</td>
                <td>{{($val->status_name)}}</td>
                <td>{{\Carbon\Carbon::parse ($val->created_at)->format('d/m/Y')}}</td>
                 <td>{{\Carbon\Carbon::parse ($val->created_at)->addDays(30)->format('d/m/Y')}}</td>
               @php
                 $i++;
             @endphp
            </tr> 
             
             @endforeach
             
           </tbody>
         </table>

</div>