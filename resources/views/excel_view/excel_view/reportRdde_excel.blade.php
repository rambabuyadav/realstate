<div>
  <title>Rdde reports Details</title>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Apl. no.</th>
        <th>Appeal no.</th>
        <th>School</th>
        <th>Block</th>
        <th>District</th>
        <th>Appeal</th>
        <th>Appeal Dt.</th>
        <th title="Remaining Date">End Date </th>
      </tr>
    </thead>
    <tbody>
      @php
      $i =0
      @endphp
      @foreach($datalist as $k=>$val)
      <tr>

        <td>{{$val->apllication_id}} </td>
        <td><a href='showData/{{@$val->id}}'>{{@$val->school_name}} </a></td>

        <td>{{$val->block}}</td>
        <td>{{$val->Division}}</td>
        <td>{{$val->disrict}}</td>
        <td>{{$val->status_name}}</td>
        <td>{{\Carbon\Carbon::parse ($val->created_at)->format('d/m/Y')}}</td>
        <td>{{\Carbon\Carbon::parse ($val->created_at)->addDays(30)->format('d/m/Y')}}

          @if($total[$i] > 5 )
          <span class="text-success"> ( {{$total[$i] }} days ) </span>
          @elseif($total[$i] == 0)
          <span class="text-success"> End </span>
          @else
          <span class="text-danger"> ( {{$total[$i] }} days ) </span>
          @endif
        </td>

        @php
        $i++
        @endphp
      </tr>
      @endforeach
    </tbody>
  </table>

</div>