<div>
    <title>Rejected Application Details</title>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Apl. no.</th>
                <th>School</th>
                <th>Block</th>
                <th>Division</th>
                <th>District</th>
                <th>Status</th>
                <th>Apl Dt.</th>
                <th>End Date </th>
            </tr>
        </thead>
        <tbody>
            @php
            $i=0;


            @endphp
            @foreach($datalist as $key=>$Report)

            <tr>

                <td>{{$Report->apllication_id}} </td>
                <td>{{$Report->school_name}} </td>

                <td>{{$Report->Block}}</td>
                <td>{{$Report->Division}}</td>
                <td>{{$Report->Disrict}}</td>
                <td>{{$Report->status_name}}</td>
                <td>{{\Carbon\Carbon::parse ($Report->created_at)->format('d/m/Y')}}</td>
                <td>{{\Carbon\Carbon::parse ($Report->created_at)->addDays(30)->format('d/m/Y')}}

                    @if($count[$i] > 5 )
                    <span class="text-success"> ({{ $count[$i] }} days) </span>
                    @else
                    <span class="text-danger"> ({{ $count[$i] }} days) </span>
                    @endif
                </td>

                @php
                $i++;
                @endphp

            </tr>
            @endforeach


        </tbody>
    </table>






</div>