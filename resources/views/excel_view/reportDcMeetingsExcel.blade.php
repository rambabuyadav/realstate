<div>
    <title>Report DC Meetings</title>
    <table class="table table-bordered">                            
        <thead>
             <tr>
                <th>Sl No.</th>
                <th>School</th>
                <th>Block</th>
                <th>Division</th>
                <th>District</th>
                <th>Status</th>
                <th>Apl Dt</th>
                <th>End Date</th>
                
             </tr>
           </thead>
           <tbody>
             @php
              $i=1;
            @endphp
           @foreach($datalist as $k=>$val)
           <tr>
          
                <td>{{$i}}</td>
                <td>{{($val->school_name)}}</td>
                <td>{{($val->block)}}</td>
                <td>{{($val->Division)}}</td>
                <td>{{($val->disrict)}}</td>
                <td>{{$val->status_name}}
                  @if($val->updated_at==null)
                
              @else
                  ( {{\Carbon\Carbon::parse ($val->updated_at)->format('d/m/Y')}} ) 
              @endif    
              </td>

              <td>
                  @if($val->created_at==null)
                      ---
                  @else
                      {{\Carbon\Carbon::parse ($val->created_at)->format('d/m/Y')}}
                  @endif    
              </td>

              <td>
                                   @if($val->status_name == 'Certified')
                    ---
                @else
                   @if($val->startDate==null)
                      ---
                  @else
              
                      {{\Carbon\Carbon::parse ($val->updated_at)->addDays($val->endDay)->format('d/m/Y')}} 
                      @if( $val->RemainingDate > 5 )
                          <span class="text-success"> ( {{ $val->RemainingDate }} days ) </span>
                      @elseif($val->RemainingDate < 5 && $val->RemainingDate > 0 )
                          <span class="text-warning"> ( {{ $val->RemainingDate }} days ) </span>
                      @else
                          <span class="text-danger"> (Delayed ) </span>
                      @endif
                  @endif    
                @endif    
    

              </td>
               @php
                 $i++;
             @endphp
            </tr> 
             
             @endforeach
             
           </tbody>
         </table>

</div>