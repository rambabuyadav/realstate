@include('header')
@include('sidenav')
@include('topbar')
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<style>
  @import url(http://fonts.googleapis.com/css?family=Roboto:500,100,300,700,400);
  
  * {
    margin: 0;
    padding: 0;
    font-family: roboto;
  }
  .cont {
    width: 100%;
    /* max-width: 350px; */
    text-align: center;
  
    /* margin-left:15%; */
    padding: 30px 0;
    background: #FBFBFB;
    color: #EEE;
    border-radius: 5px;
    /* border: thin solid #444; */
    overflow: hidden;
  }
  .cont1 {
    width: 100%;
    text-align: center;
    padding-bottom: 20px;
    padding-top: 10px;
    background: #FBFBFB;
    color: #EEE;
    border-radius: 5px;
    /* border: thin solid #444; */
    overflow: hidden;
  }
  
  div.stars {
    width: 430px;
  }
  div.stars1 {
    width: 430px;
  }
  input.star { display: none; }
  
  label.star {
    float: right;
    padding-left: 10px;
    padding-right: 10px;
    font-size: 22px;
    color: #444;
    transition: all .2s;
  }
  
  input.star:checked ~ label.star:before {
    content: '\f005';
    color: #FD4;
    transition: all .25s;
  }
  
  input.star-5:checked ~ label.star:before {
    color: #FE7;
    text-shadow: 0 0 20px #952;
  }
  
  input.star-1:checked ~ label.star:before { color: #F62; }
  
  label.star:hover { transform: rotate(-15deg) scale(1.3); }
  
  label.star:before {
    content: '\f006';
    font-family: FontAwesome;
  }
  </style>
  <link href="http://www.cssscript.com/wp-includes/css/sticky.css" rel="stylesheet" type="text/css">
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Feedback </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('feedback')}}"><i class="fa fa-file" aria-hidden="true"></i>
                        <li class="breadcrumb-item"><a href="">Feedback </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- [ breadcrumb ] end -->
    <!-- [ Main Content ] start -->
    <div class="row ">
      <div class="col-md-0"></div>
      <div class="col-md-12">
        <div class="shadow-lg cont1" >
          <div class="row pl-5">
            <span class="text-dark" style="font-size: 18px;">Star Rating System  </span>
          </div><hr style="margin-top:10px !important; margin-bottom: 10px !important;">
          <div class="row text-left pl-4">
            <div class="col-md-12 pl-5"><span class="text-dark" style="font-size: 20px;">Average Rating</span></div>
            <div class="col-md-1 pl-5 pt-"><span class="text-dark" style="font-size: 22px;">2.6</span></div>
          <div class="stars col-md-3">
              <input class="star star-5"  type="radio"  value="5"/>
              <label class="star star-5" for=""></label>
              <input class="star star-4"  type="radio"  value="4" checked/>
              <label class="star star-4" for=""></label>
              <input class="star star-3"  type="radio"  value="3"/>
              <label class="star star-3" for=""></label>
              <input class="star star-2"  type="radio"  value="2"/>
              <label class="star star-2" for=""></label>
              <input class="star star-1"  type="radio"  value="1"/>
              <label class="star star-1" for=""></label>
          </div>
        </form>
        </div>
<div class="row">
  <div class="col-md-12 pl-5 text-left"><span class="text-dark" style="font-size: 22px;">Review</span></div>
</div>
@foreach($Alldata as $key => $data)
<div class="row">
  <div class="col-md-1 pl-5 text-left pt-"> <img class="img-radius" src="assets/images/user/avatar.png" alt="User-Profile-Image" style="height:50px; width:50px;" ></div>
  <div class="col-md-2 pl-5 text-left pt-"><span class="text-dark" style="font-size: 14px;">
    
    <?php 
    $date= @$data->created_at;
    $mytime = \Carbon\Carbon::now();
    $current_days = $mytime->toDateTimeString();
    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $current_days);
    $diff_in_days = $to->diffInDays($from);
    ?>
     {{ $diff_in_days }} days ago
    </span><br> <span class="text-dark" style="font-size: 16px;">{{@$data->name}}</span></div>
  <div class="col-md-9 pl-5 text-left">
  <div class="row">
    <div class="">
      <input class="star star-5"  type="radio"  value="5" {{@$data->star_count == '5' ? 'checked' : ''}}/>
      <label class="star star-5" for=""></label>
      <input class="star star-4"  type="radio"  value="4" {{@$data->star_count == '4' ? 'checked' : ''}}/>
      <label class="star star-4" for=""></label>
      <input class="star star-3"  type="radio"  value="3" {{@$data->star_count == '3' ? 'checked' : ''}}/>
      <label class="star star-3" for=""></label>
      <input class="star star-2"  type="radio"  value="2" {{@$data->star_count == '2' ? 'checked' : ''}}/>
      <label class="star star-2" for=""></label>
      <input class="star star-1"  type="radio"  value="1" {{@$data->star_count == '1' ? 'checked' : ''}}/>
      <label class="star star-1" for=""></label>
  </div>
  <br>
</div>
<div class="row pr-5">
  <span class="text-dark" style="font-size: 13px;">{{@$data->feedback}}</span> 
</div>
</div>
</div><hr style="margin-top:10px !important; margin-bottom: 10px !important;">
@endforeach
      </div>
      </div>
      
    </div>
    </div>
    <!-- [ Main Content ] end -->
</div>
</div>
@include('footer')