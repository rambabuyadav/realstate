<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Private School Recognition System | Login</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/login.css')}}">
    <script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
</head>

<body>
    <section class="login-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 login-sec">
                    <div class="logoimg">
                        <a href="{{url('/')}}">
                            <img style="margin-left: 503px;" src="{{url('assets/images/jharkhand-govt-logo.png')}}" alt="Jharkhand Government Logo">
                        </a>
                    </div>
                    <h2 class="text-center">Forgot Password?</h2>
                    <!-- <x-jet-validation-errors class="mb-4 alert-msg-login" /> -->

                    <p style="color: white;">You can reset your password here.</p>
                    @if( Session::has('message') )
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    <form role="form" method="post" action="{{route('forgot_password.store')}}" id="registration-form" enctype="multipart/form-data"> @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label class="labeltxt " for="email">Username / Email</label>
                                    <input id="email" class="block mt-1 w-full inputbox" type="text" name="email" :value="old('email')" required autofocus placeholder="Enter username or email">
                                    @if ($errors->has('email'))

                                    <span class="text-danger">{{ $errors->first('email') }}</span>

                                    @endif
                                </div>
                            </div>
                            <!-- <div class="row"> -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('contact') ? 'has-error' : '' }}">
                                    <label class="labeltxt " for="contact">Mobile No.</label>
                                    <input id="contact" class="block mt-1 w-full inputbox" type="text" name="contact" required autofocus placeholder="Enter mobile no.">
                                    @if ($errors->has('contact'))
                                    <span class="text-danger">{{ $errors->first('contact') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('mobile_otp') ? 'has-error' : '' }}">
                                    <label class="labeltxt " for="mobile_otp">OTP</label>
                                    <input class="block mt-1 w-full inputbox" type="text" id="mobile_otp" name="mobile_otp" value="{{ old('mobile_otp') }}" required autofocus placeholder="Enter mobile OTP">
                                    @if ($errors->has('mobile_otp'))

                                    <span class="text-danger">{{ $errors->first('mobile_otp') }}</span>

                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label"> </label>
                                <button type="button" id='generate_phone_otp' class="btn btn-next btn-warning" style="margin-top: 27px;font-size: 12px;">Generate OTP</button>
                            </div>
                            <!-- </div> -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <label for="password" value="{{ __('Password') }}" class="labeltxt">New Password</label>
                                    <input id="password" class="block mt-1 w-full inputbox" type="password" name="password" required autocomplete="current-password" placeholder="Enter password">
                                    @if ($errors->has('password'))

                                    <span class="text-danger">{{ $errors->first('password') }}</span>

                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                    <label for="password_confirmation" value="{{ __('Password') }}" class="labeltxt">Confirm Password</label>
                                    <input id="password_confirmation" class="block mt-1 w-full   inputbox" type="password" name="password_confirmation" required autocomplete="current-password" placeholder="Enter confirm password">
                                    @if ($errors->has('password_confirmation'))

                                    <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>

                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: center;">
                                <button type="button" style="text-align: center;margin-top:20px;font-size: 17px;" class="btn rounded-0 addnew">
                                    @if (Route::has('password.request'))
                                    <a href="{{route('login')}}" style="font-size: 15px;" class="  pull-right pswd_link">{{ __('Login ') }}</a>
                                    @endif
                                </button>

                                <button type="submit" style="text-align: center;margin-top:20px;font-size: 17px;" class="btn btn-primary rounded-0 addnew">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>
</body>

</html>
<script src="{{url('assets/school_register_assets/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('assets/school_register_assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/school_register_assets/js/jquery.backstretch.min.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>

    // $(document).ready(function () {
        $('#generate_phone_otp').on('click', function () {
            console.log('success_1');
            var contact = $('#contact').val();
            contact = contact+'_forgot';
            if (contact) {
                $.ajax({
                    url: 'generateOTP/mobile/' + contact,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log('success_2');
                    }
                });
            } else {
                console.log('failed');
            }
        });
    // });
</script>