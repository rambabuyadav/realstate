
<section class="login-block">
    <div class="container">
        <div class="row">
            <div class="col-md-4 login-sec">
                <div class="logoimg">
                    
                </div>
                <h2 class="text-center">Login</h2>
                <x-jet-validation-errors class="mb-4 alert-msg-login text-white" />
                <div class="mb-4 font-medium-login text-white" style="color: red;font-size: 15px;"></div>

                <form class="login-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        
                    <div class="form-group">
                        <label class="labeltxt " for="email">Username</label>
                        <input id="email" class="block mt-1 w-full inputbox   " type="text" name="email" value="{{@$data['email']}}" required autofocus placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label for="password" value="{{ __('Password') }}" class="labeltxt">Password</label>
                        <input id="password" class="block mt-1 w-full   inputbox" type="password" name="password" value="{{@$data['password']}}" required autocomplete="current-password" placeholder="Enter password">
                    </div>
                   
                    <div class="col-md-12">
                        <x-jet-button class=" loginbtn   btn-primary" id="clickMe" style="width:100%;">
                            {{ __('Login') }}
                        </x-jet-button>
                    </div>
                </form>
            </div>
            
        </div>
</section>

<script>
    $(document).ready(function(){
        // let button = document.querySelector("#clickMe");
        // button.click();
        document.grtElementById('#clickMe').click();
    });
    
</script>