<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jharkhand Council For Education & Research Training | Login</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/images/favicon-jharkhand.ico')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-4/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/fontawesome-5/css/all.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/login.css')}}">
    <script type="text/javascript" src="{{url('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/css/bootstrap-4/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <?php
    header('X-Content-Type-Options: nosniff');
    ?>
    
    <style>
        input::-webkit-input-placeholder {
            font-size: 15px;
        }

        .container .login-box {
            /* background-image: linear-gradient(to bottom, #2cb42c, #228b22); */
            background: rgb(0, 0, 0, .55);
            position: relative;
            width: 400px;
            margin-top: 20%;
            margin-left: 30%;
            padding: 60px;
            border-top: 4px solid white;
        }

        .login-box img {
            position: absolute;
            width: 80px;
            height: 80px;
            top: -39px;
            right: 53px;
        }

        .login-box label {
            font-size: 15px;
            font-weight: 400;
        }
    </style>
    
</head>

<body>


     <section class="login-block">
        <div class="container">
            <div class="loginbx">
                <div class="row">
                    <div class="col-md-4 login-sec">
                        <div class="logoimg">
                            <a href="{{url('/')}}">
                                <img src="assets/images/download.jfif" alt="image" class="img-circle">
                            </a>
                        </div>
                        <h2 class="text-center">Login</h2>
                        <x-jet-validation-errors class="mb-4 alert-msg-login text-white" />
                        <div class="mb-4 font-medium-login text-white" style="color: red;font-size: 15px;"></div>

                        <form class="login-form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                            <div class="form-group">
                                <label class="labeltxt " for="email">Username</label>
                                <input id="email" class="block mt-1 w-full inputbox   " type="text" name="email" :value="old('email')" required autofocus placeholder="Enter username">
                            </div>
                            <div class="form-group">
                                <label for="password" value="{{ __('Password') }}" class="labeltxt">Password</label>
                                <input id="password" class="block mt-1 w-full   inputbox" type="password" name="password" required autocomplete="current-password" placeholder="Enter password">
                            </div>
                            <div class="form-group">
                                <label for="password" value="{{ __('Password') }}" class="labeltxt">Please Enter the CAPTCHA Below</label>
                                <table width="100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src="{{URL::asset('public/images/captcha.php')}}" style=" height: 33px;padding-right: 10px;">
                                            </td>
                                            <td>
                                                <input class="block mt-1 w-full inputbox" name="captcha" placeholder="Enter CAPTCHA" required="required" type="text" autocomplete="off">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    <small style="margin-left:20px;" class="labeltxt">Remember Me</small>
                                </label>
                                <small>
                                    @if (Route::has('password.request'))
                                    <a href="{{route('forgot_password.index')}}" style="font-size: 15px;" class="pull-right pswd_link"><i class="fa fa-lock" style="margin-right:10px;"></i> {{ __('Forgot your password?') }}</a>
                                    @endif
                                </small>
                            </div>
                            <div class="col-md-12">
                                <x-jet-button class=" loginbtn   btn-primary" style="width:100%;">
                                    {{ __('Login') }}
                                </x-jet-button>
                            </div>
                            .
                            <a href="{{route('school_register.index')}}" style="display:flex;justify-content:center;font-size: 15px; "><span style="color:white;font-size: 15px;">Not registered Yet?</span>&nbsp; Sign up</a>
                        </form>
                    </div>
                    <div class="col-md-8 sliderlogin">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active sliderimg">
                                    <img class="d-block img-fluid" src="assets/images/auth/login/1s.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item sliderimg">
                                    <img class="d-block img-fluid" src="assets/images/auth/login/2s.jpg" alt="Second slide">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

    </section>

    {{-- <section class="login-block">
        <div class="container">

            <div class="login-box">
                <img class="img-circle" src="assets/images/jcert-logo.jpg" alt="">
                <form class="login-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-white text-center mb-5">Login</h1>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="text-white"><i class="fa fa-user mr-2 mb-3"></i>USERNAME</label>
                                <input type="text" class="form-control" name="email" :value="old('email')">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="text-white"><i class="fa fa-lock mr-2 mb-3"></i>PASSWORD</label>
                                <input type="text" class="form-control"name="password">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <span class="mr-2  pull-left show-psd"><input type="checkbox"></span><span style="font-size:15px;" class="text-white">Show Password</span>
                                <span class="pull-right" style="font-size:15px;"><a href="" class="text-white">Forgot Password ?</a></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <span class="mr-2  pull-left"><input type="checkbox"></span><span style="font-size:15px;" class="text-white">Keep me signed in</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="mt-5 btn btn-primary btn-success form-control " style="font-size:15px;">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>


    </section> --}}

</body>

</html>