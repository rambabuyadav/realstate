<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use App\Models\Recent_user;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            $setting_array = fnd_settings::where('status', 1)->first();
            $setting_array->verification_by_dse_limit;
            $setting_array->schedule_meeting_by_dc_limit;
            $setting_array->take_decision_after_meeting_held_by_dc;
            $setting_array->faa_decision_limit;
            $setting_array->saa_decision_limit;
            
            $all_applications_at_dse = application_data::whereIn('application_status', [2, 3])->where('status',1)->get();
           

            foreach($all_applications_at_dse as $key => $value)
            {
               $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
               $lastDate = $createdDate->created_at->addDays($setting_array->verification_by_dse_limit);
               if($lastDate < date('Y-m-d H:i:s'))
               {
                $trackUpdate = new application_tracking();
                $trackUpdate->application_id = $createdDate->application_id;
                $trackUpdate->school_id = $createdDate->school_id;

                $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                $msg = ' Your application is pending and being delayed at District Superintendent of Education as no action has been taken. ';
                $this->TrackingSMS($phone,$msg); 

                $trackUpdate->application_status_id = 18;
                $trackUpdate->created_at = date('Y-m-d H:i:s');
                $trackUpdate->created_by = -1;
                $trackUpdate->save();

                
               }
               else
               {
                   continue;
               }
     
            }

            $all_applications_at_dc_varification = application_data::where('application_status', 4)->where('status',1)->get();
           
            foreach($all_applications_at_dc_varification as $key => $value)
             {
                $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
                $lastDate = $createdDate->created_at->addDays($setting_array->schedule_meeting_by_dc_limit);
                if($lastDate < date('Y-m-d H:i:s'))
                {
                 $trackUpdate = new application_tracking();
                 $trackUpdate->application_id = $createdDate->application_id;
                 $trackUpdate->school_id = $createdDate->school_id;

                 $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                 $msg = ' Your application is pending and being delayed at District Committee as no meeting was held. ';
                 $this->TrackingSMS($phone,$msg); 
 
                 $trackUpdate->application_status_id = 19;
                 $trackUpdate->created_at = date('Y-m-d H:i:s');
                 $trackUpdate->created_by = -1;
                 $trackUpdate->save();
                }
                else
                {
                    continue;
                }
      
             }

             $all_applications_at_dc_meeting = application_data::where('application_status', 5)->where('status',1)->get();
            
            foreach($all_applications_at_dc_meeting as $key => $value)
             {
                $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
                $lastDate = $createdDate->created_at->addDays($setting_array->take_decision_after_meeting_held_by_dc);
                if($lastDate < date('Y-m-d H:i:s'))
                {
                 $trackUpdate = new application_tracking();
                 $trackUpdate->application_id = $createdDate->application_id;
                 $trackUpdate->school_id = $createdDate->school_id;

                 $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                 $msg = ' Your application is pending and being delayed at District Committee as no Verification was held. ';
                 $this->TrackingSMS($phone,$msg); 
 
                 $trackUpdate->application_status_id = 20;  
                 $trackUpdate->created_at = date('Y-m-d H:i:s');
                 $trackUpdate->created_by = -1;
                 $trackUpdate->save();
                }
                else
                {
                    continue;
                }
      
             }

             $all_applications_at_faa = application_data::where('application_status', 9)->where('status',1)->get();
             
             foreach($all_applications_at_faa as $key => $value)
             {
                $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
                $lastDate = $createdDate->created_at->addDays($setting_array->faa_decision_limit);
                if($lastDate < date('Y-m-d H:i:s'))
                {
                 $trackUpdate = new application_tracking();
                 $trackUpdate->application_id = $createdDate->application_id;
                 $trackUpdate->school_id = $createdDate->school_id;

                 $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                 $msg = ' Your application is pending and being delayed at First Appellate Authority as no action has been taken.';
                 $this->TrackingSMS($phone,$msg); 

                 
                 $trackUpdate->application_status_id = 21;
                 $trackUpdate->created_at = date('Y-m-d H:i:s');
                 $trackUpdate->created_by = -1;
                 $trackUpdate->save();
                }
                else
                {
                    continue;
                }
      
             }

             $all_applications_at_saa = application_data::where('application_status', 12)->where('status',1)->get();

             foreach($all_applications_at_saa as $key => $value)
             {
                $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
                $lastDate = $createdDate->created_at->addDays($setting_array->saa_decision_limit);
                if($lastDate < date('Y-m-d H:i:s'))
                {
                 $trackUpdate = new application_tracking();
                 $trackUpdate->application_id = $createdDate->application_id;
                 $trackUpdate->school_id = $createdDate->school_id;

                 $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                 $msg = ' Your application is pending and being delayed at Second Appellate Authority as no action has been taken.';
                 $this->TrackingSMS($phone,$msg); 

                 
                 $trackUpdate->application_status_id = 22;
                 $trackUpdate->created_at = date('Y-m-d H:i:s');
                 $trackUpdate->created_by = -1;
                 $trackUpdate->save();
                }
                else
                {
                    continue;
                }
      
             }

             $all_applications_at_dse_or_dc_compile_by_faa = application_data::where('application_status', 10)->where('status',1)->get();
             foreach($all_applications_at_dse_or_dc_compile_by_faa as $key => $value)
             {
                $appealDeadline = Appeal::where('application_id',$value->id)->where('appeal_status',1)->select('appeal_deadline','appeal_assign_to')->get();
                $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
                if($appealDeadline->appeal_deadline < date('Y-m-d H:i:s'))
                {
                 $trackUpdate = new application_tracking();
                 $trackUpdate->application_id = $createdDate->application_id;
                 $trackUpdate->school_id = $createdDate->school_id;
                 if($appealDeadline->appeal_assign_to == 'dse')
                 {
                    $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                    $msg = 'Your application is delayed at District Superintendent of Education as there was no comply with First Appellate Authority.';
                    $this->TrackingSMS($phone,$msg); 

                    
                    $trackUpdate->application_status_id = 23;
                 }
                elseif($appealDeadline->appeal_assign_to == 'dc')
                 {

                    $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                    $msg = 'Your application is delayed at District Committee as there was no comply with First Appellate Authority.';
                    $this->TrackingSMS($phone,$msg); 

                    
                 $trackUpdate->application_status_id = 25;
                 }
                 $trackUpdate->created_at = date('Y-m-d H:i:s');
                 $trackUpdate->created_by = -1;
                 $trackUpdate->save();
                }
                else
                {
                    continue;
                }
      
             }
             
             $all_applications_at_dse_or_dc_compile_by_saa = application_data::where('application_status', 13)->where('status',1)->get();
             foreach($all_applications_at_dse_or_dc_compile_by_saa as $key => $value)
             {
                $appealDeadline = Appeal::where('application_id',$value->id)->where('appeal_status',1)->select('appeal_deadline','appeal_assign_to')->get();
                $createdDate = application_tracking::where('application_id', $value->id)->orderBy('created_at','DESC')->first();
                if($appealDeadline->appeal_deadline < date('Y-m-d H:i:s'))
                {
                 $trackUpdate = new application_tracking();
                 $trackUpdate->application_id = $createdDate->application_id;
                 $trackUpdate->school_id = $createdDate->school_id;
                 if($appealDeadline->appeal_assign_to == 'dse')
                 {
                    $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                    $msg = 'Your application is delayed at District Superintendent of Education as there was no comply with Second Appellate';
                    $this->TrackingSMS($phone,$msg); 

                    
                    $trackUpdate->application_status_id = 24;
                 }
                elseif($appealDeadline->appeal_assign_to == 'dc')
                 {
                    $phone = Contact::where('school_id', $createdDate->school_id)->value('mobile');
                    $msg = ' Your application is delayed at District Committee as there was no comply with Second Appellate';
                    $this->TrackingSMS($phone,$msg); 

                    
                 $trackUpdate->application_status_id = 26;
                 }
                 $trackUpdate->created_at = date('Y-m-d H:i:s');
                 $trackUpdate->created_by = -1;
                 $trackUpdate->save();
                }
                else
                {
                    continue;
                }
      
             }
           
        })->dailyAt('02:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

     
    public function TrackingSMS($phone,$msg)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        $message = $msg;
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
    }
   
    //function to send single unicode sms
    public function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }

    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }

    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }

    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        echo $result = curl_exec($post); //result from mobile seva server
        curl_close($post);
    }
}
