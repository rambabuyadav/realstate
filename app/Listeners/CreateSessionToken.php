<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\SessionToken;
use Illuminate\Support\Str;

class CreateSessionToken
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $token = new SessionToken;
        $token->user_id = $event->user->id;
        $token->token = Str::random(16);
        $token->save();
        session(['token' => $token->token]);
    }
}
