<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\SessionToken;
use Auth;
use Session;

class ValidateSessionToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = SessionToken::where('user_id', Auth::id())
            ->orderBy('created_at', 'DESC')->first();

        if ($request->session()->get('token') != $token->token) {
            Session::flush();
            Auth::guard('web')->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return redirect('/login');
        }

        return $next($request);
    }
}
