<?php



namespace App\Http\Middleware;



use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;



class VerifyCsrfToken extends Middleware

{

    /**

     * The URIs that should be excluded from CSRF verification.

     *

     * @var array

     */

    protected $except = [

        'school_register*',
        'application-submit/response',
        'application_payment',
        'single-window-login',
        'single_window_school_register'

    ];

}

