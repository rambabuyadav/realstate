<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\application_data;
use App\Models\feedback;
use Illuminate\Database\Eloquent\Model;
use App\Models\application_data_ext_1;
use DB;
use Auth;
class feedbackController extends Controller
{
    function addData(Request $req)
    {
        $feedback_form = new feedback;
        $feedback_form->feedback = $req->feedback;
        $feedback_form->star_count = $req->star;
        $feedback_form->created_by = Auth::user()->name;
        $feedback_form->user_id = Auth::user()->id;
        $feedback_form->save();
        return redirect('feedback')->with('message', 'Thanks for your feedback');
    }
    public function feedbackList()
    {
        $Alldata = DB::table('feedback')
            ->join('users', 'feedback.user_id', '=', 'users.id')
            ->select(
                'feedback.id',
                'feedback.star_count',
                'feedback.feedback',
                'feedback.created_at',
                'feedback.created_by',
                'users.name',
                'users.profile_photo_path',
                'users.email as useremail'
            )->orderBy('created_at', 'desc')->get();
        //    return $Alldata ;
        //    $mytime = \Carbon\Carbon::now();
        //    $current_days = $mytime->toDateTimeString();
        //    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', 'feedback.created_at');
        //    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $current_days);
        //    $diff_in_days = $to->diffInDays($from);
        //    $Alldata = $diff_in_days;
        //    $Alldata = $Alldata->paginate(2);
        //  return $Alldata ;
        return view('feedbackShow', compact('Alldata'));
        //  DB::raw('DATE_FORMAT(feedback.created_at, "%d/%m/%Y %H:%i %p") as created_at'), 
        // $mytime = \Carbon\Carbon::now();
        // $current_days = $mytime->toDateTimeString();
        //     return $diff_in_days;
    }
}
