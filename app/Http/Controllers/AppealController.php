<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Models\Fnd_value;
use App\Models\User;
use App\Models\Fnd_value_set;
use App\Models\Appeal;
use App\Models\application_data;
use App\Models\application_tracking;
use App\Models\Conversations;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExcelExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Crypt;
use App\Models\fnd_settings;
use Carbon\Carbon;
class AppealController extends Controller
{
    // public function __invoke()
    // {
    //     if (Auth::check()) {
    //         if (Auth::user()->user_role = 'state') {
    //             return \Redirect::to('/dashboard');
    //         }
    //     }
    // }
    // //
    // public $data = [];

    // Appea 1 

    public function listAppeals(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'dc' || Auth::user()->user_role == 'dse' || Auth::user()->user_role == 'faa' || Auth::user()->user_role == 'saa' || Auth::user()->user_role == 'school') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                if (Auth::user()->user_role == 'faa') {
                    $division_id = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->value('addresses.division');
                    // return $division_id;
                    $this->data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', '=', $division_id)
                        ->orderBy('fnd_values.display_value', 'ASC')
                        ->pluck(
                            'fnd_values.display_value as district_name',
                            'fnd_values.id as district_id'
                        );
                }
                $this->data['count_appeals'] = 0;
                $search_text = $request->search_text;
                $appeal_status = $request->appeal_status;
                $block = $request->Block;
                $division = $request->division;
                $district = $request->district;
                $fromDate = $request->fromDate;
                $toDate = $request->toDate;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = ['search_text' => $search_text, 'appeal_status' => $appeal_status, 'block_id' => $block, 'blockName' => $blockName, 'division_id' => $division, 'divisionName' => $divisionName, 'district_id' => $district, 'districtName' => $districtName, 'fromDate' => $fromDate, 'toDate' => $toDate];
                $user = Auth::user();
                $this->data['appeals'] = Appeal::leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->leftjoin('fnd_values as fnd_division', 'addresses.division', '=', 'fnd_division.id')
                    ->leftjoin('fnd_values as fnd_district', 'addresses.district', '=', 'fnd_district.id')
                    ->leftjoin('fnd_values as fnd_block', 'addresses.block', '=', 'fnd_block.id')
                    ->where('addresses.school_id', '!=', 1)
                    ->select(
                        'addresses.school_address',
                        'fnd_division.display_value as division',
                        'fnd_district.display_value as district ',
                        'fnd_block.display_value as block',
                        'private_schools.school_name',
                        'private_schools.board',
                        'private_schools.website',
                        'private_schools.school_type',
                        'private_schools.school_email',
                        'private_schools.school_code',
                        'application_data.*',
                        'appeals.*'
                    );
                if ($user->user_role == 'school') {
                    $this->data['appeals'] = $this->data['appeals']
                        ->where('appeals.school_id', '=', $user->school_id);
                    // return $this->data;
                } elseif ($user->user_role == 'faa') {
                    $FAA_Division = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.division')
                        ->get();
                    $FAA_Division = $FAA_Division[0]->division;
                    $this->data['appeals'] = $this->data['appeals']->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', $FAA_Division)
                        ->where('application_data.division', $FAA_Division);
                } elseif ($user->user_role == 'saa') {
                    $this->data['appeals'] = $this->data['appeals']->where('appeals.appellate_authority_type', '2');
                } elseif ($user->user_role == 'dse') {
                    $DSE_District = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.district')
                        ->get();
                    $DSE_District = $DSE_District[0]->district;
                    $this->data['appeals'] = $this->data['appeals']->where('appeals.appeal_assign_to', 'dse')->where('addresses.district', $DSE_District);
                } elseif ($user->user_role == 'dc') {
                    $DC_District = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.district')
                        ->get();
                    $DC_District = $DC_District[0]->district;
                    $this->data['appeals'] = $this->data['appeals']->where('appeals.appeal_assign_to', 'dc')
                        ->where('addresses.district', $DC_District);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['appeals'] =  $this->data['appeals']->where(function ($query) use ($search_text) {
                        $query->where('private_schools.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('private_schools.school_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('appeals.appeal_number', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->appeal_status) && $request->appeal_status != null) {
                    $this->data['appeals'] =  $this->data['appeals']->where('appeals.appeal_status', $appeal_status);
                    if ($request->appeal_status == 0) {
                        // return $request->appeal_status;
                        $this->data['appeals'] =  $this->data['appeals']->whereIn('appeals.appeal_status', [1, 2, 3]);
                    } else {
                        $this->data['appeals'] =  $this->data['appeals']->where('appeals.appeal_status', $appeal_status);
                    }
                    //   $data = ['sa'=>$request->appeal_status,'sasda'=> $this->data['appeals'] ];
                    //   return $data;
                } else {
                    $this->data['appeals'] =  $this->data['appeals']->whereIn('appeals.appeal_status', [1, 2, 3]);
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['appeals'] =  $this->data['appeals']->where('addresses.division', $division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['appeals'] =  $this->data['appeals']->where('addresses.district', $district);
                }
                if (isset($request->Block) && $request->Block != null) {
                    $this->data['appeals'] =  $this->data['appeals']->where('addresses.block', $block);
                }
                if ($request->fromDate != null && $request->toDate != null) {
                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $fromDate)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $toDate)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return redirect()->back()->with('warning', "Form date always lesser than To date");
                    }
                    //return[$start_date,$end_date];  
                    $this->data['appeals'] =  $this->data['appeals']->whereBetween('appeals.appeal_date', [$fromDate, $toDate]);
                }
                $this->data['count_appeals'] = $this->data['appeals']->count();
                $this->data['appeals'] = $this->data['appeals']->paginate(25);
                $appeal_conversations = [];
                foreach ($this->data['appeals'] as $key => $value) {
                    // echo $value->id;
                    $value->appeal_document = json_decode($value->appeal_document);
                    $value->school_file = json_decode($value->school_file);
                    $Conversations = [];
                    $Conversations = Conversations::Join('users as sender', 'conversations.sender_id', '=', 'sender.id')
                        ->Join('users as reciever', 'conversations.reciever_id', '=', 'reciever.id')
                        ->select(
                            'conversations.*',
                            'reciever.user_role as Reciever',
                            'sender.user_role as Sender',
                            'reciever.name as Reciever_name',
                            'sender.name as Sender_name'
                        )
                        ->Where('conversations.parent_id', $value->id)
                        ->where('conversations.conversation_type', 3)
                        ->get();
                    $Conversations->images = [];
                    foreach ($Conversations as  $Conversation) {
                        $Conversation->attachments = json_decode($Conversation->attachments);
                    }
                    // array_push($appeal_conversations,$Conversations);
                    $value->appeal_conversations = $Conversations;
                    if ($user->user_role == 'school') {
                        $value->appeal_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->id)->where('conversations.conversation_type', 3)->where('school_read_status', 0)->count();
                    } elseif ($user->user_role == 'faa') {
                        $value->appeal_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->id)->where('conversations.conversation_type', 3)->where('faa_read_status', 0)->count();
                    } elseif ($user->user_role == 'saa') {
                        $value->appeal_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->id)->where('conversations.conversation_type', 3)->where('saa_read_status', 0)->count();
                    } elseif ($user->user_role == 'dse') {
                        $value->appeal_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->id)->where('conversations.conversation_type', 3)->where('dse_read_status', 0)->count();
                    } elseif ($user->user_role == 'dc') {
                        $value->appeal_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->id)->where('conversations.conversation_type', 3)->where('dc_read_status', 0)->count();
                    }
                }
                // return $appeals;
                // return $val_image[0][0];
                // $Users = User::all();
                // $appeals['image'] = $val_image;
                // echo "<pre>";
                // print_r($appeals);
                // exit();
                // $Appeal = ['appeals' => $appeals, 'count' => $count_appeals];
                // return $Appeal['images'];
                $now = Carbon::now();
                $days_to_varified_by_faa = fnd_settings::value('faa_decision_limit');
                $days_to_varified_by_saa = fnd_settings::value('saa_decision_limit');
                $RemainingDate = [];
                if (Auth::user()->user_role == 'school') {
                    foreach ($this->data['appeals'] as $row) {
                        $startDate = $row->created_at;
                        $cDate = Carbon::parse($startDate);
                        $count = $now->diffInDays($cDate);
                        if ($row->appellate_authority_type == 1) {
                            $date = $days_to_varified_by_faa - (int)$count;
                            $this->data['days_to_varified_by_faa'] = $days_to_varified_by_faa;
                        }
                        if ($row->appellate_authority_type == 2) {
                            $date = $days_to_varified_by_saa - (int)$count;
                            $this->data['days_to_varified_by_saa'] = $days_to_varified_by_saa;
                        }
                        if ($row->appeal_assign_to == 'dc' || $row->appeal_assign_to == 'dse') {
                            $date = $now->diffInDays($row->appeal_deadline, false);
                        }
                        array_push($RemainingDate, $date);
                    }
                    $this->data['count'] = $RemainingDate;
                }
                if (Auth::user()->user_role == 'faa' || Auth::user()->user_role == 'saa') {
                    foreach ($this->data['appeals'] as $row) {
                        $startDate = $row->created_at;
                        $cDate = Carbon::parse($startDate);
                        $count = $now->diffInDays($cDate);
                        if (Auth::user()->user_role == 'faa') {
                            $date = $days_to_varified_by_faa - (int)$count;
                            $this->data['days_to_varified_by_faa'] = $days_to_varified_by_faa;
                        }
                        if (Auth::user()->user_role == 'saa') {
                            $date = $days_to_varified_by_saa - (int)$count;
                            $this->data['days_to_varified_by_saa'] = $days_to_varified_by_saa;
                        }
                        array_push($RemainingDate, $date);
                    }
                    $this->data['count'] = $RemainingDate;
                }
                if (Auth::user()->user_role == 'dse' || Auth::user()->user_role == 'dc') {
                    foreach ($this->data['appeals'] as $key => $value) {
                        $date = $now->diffInDays($value->appeal_deadline, false);
                        array_push($RemainingDate, $date);
                        // return($date);
                    }
                    $this->data['count'] = $RemainingDate;
                }
                if ($request->excel == 'excel') {
                    $params = ['data' => $this->data['appeals'], 'view' => 'excel_view.appeals_excel'];
                    $filename = 'appeals-list.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                } else {
                    // return $this->data;
                    return view('appeal', $this->data);
                }
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function viewadd()
    {
        return view('addAppeal');
    }
    public function insert(Request $request)
    {
        if (Auth::user()->user_role == "student") exit;
        $user = Auth::user();
        $decode_image = '';
        $Appeal = new Appeal;
        $Conversations = new Conversations;
        $application_id = application_data::where('school_id', $user->school_id)->where('status', 1)->value('id');
        $Appeal->school_id = $user->school_id;
        $Appeal->appeal_by = $user->school_id;
        $Appeal->appeal_number = 'apl' . $application_id . date('Ymd');
        $Appeal->appeal_text = $request->appealText;
        $Appeal->application_id = $application_id;
        $Appeal->appellate_authority_type =  $appealAutherity = $request->appealAutherity;
        $FAAOfiicerName = $request->FAAOfiicerName;
        $SAAOfiicerName = $request->SAAOfiicerName;
        if ($request->school_file != null) {
            foreach ($request->school_file as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/school_send_appeal'), $fileName);
                    $image_name[] = $fileName;
                }
            }
        }
        $decode_image = json_encode($image_name, true);
        if ($appealAutherity == '1') {
            $Appeal->appeal_to = $FAAOfiicerName;
        } else {
            $Appeal->appeal_to = $SAAOfiicerName;
        }
        $Appeal->school_file = $decode_image;
        $Appeal->created_ip = $_SERVER['REMOTE_ADDR'];
        $Appeal->created_by = $user->id;
        $Conversations->conversation_type = '3';
        $Conversations->parent_id = Appeal::max('id') + 1;
        $Conversations->sender_id = $user->school_id;
        $Conversations->reciever_id = 6;
        $Conversations->title = 'Title';
        $Conversations->remarks = $request->appealText;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        $Appeal->save();
        if ($appealAutherity == '1') {
            $application_status = 9;

            $phone = Contact::where('school_id', $user->school_id)->value('mobile');
            $msg = 'Your appeal has been sent to First Appellate Authority successfully.';
            $this->TrackingSMS($phone,$msg); 


        } else {
            $application_status = 12;

            $phone = Contact::where('school_id', $user->school_id)->value('mobile');
            $msg = 'Your appeal has been sent to Second Appellate Authority successfully.';
            $this->TrackingSMS($phone,$msg); 


        }
        $Application_ID = application_data::where('application_data.school_id', $user->school_id)->value('id');
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $Application_ID;
        $application_tracking->school_id = $user->school_id;
        $application_tracking->application_status_id = $application_status;
        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        application_data::where('id', $Application_ID)->update([
            'application_status' => $application_status,
            'updated_by' => $user->id
        ]);
        return redirect('appeal-list')->with('success', 'Appeal Send Successfully !!!');
    }
    public function edit($id)
    {
        $Appeals = Appeal::find($id);
        // return $Appeals;
        return view('editAppeal', ['Appeals' => $Appeals]);
    }
    public function deleteData($id)
    {
        $users = Appeal::where('appeals.id', $id)->update([
            'status' => '0'
        ]);
        return redirect('appeal-list')->with('warning', 'User Deleted Successfully !!!');
    }
    public function acceptRemarkAppeal(Request $req)
    {
        $user = Auth::user();
        $decode_image = '';
        $Send_user_district = Address::where('addresses.school_id', $req->AppealSchoolID)
            ->value('district');
        $users_id = User::select('users.id')->join('addresses', 'addresses.user_id', '=', 'users.id')
            ->where('addresses.district', $Send_user_district)
            ->where('users.user_role', $req->appeal_assign_to)
            ->value('users.id');
        $date = Date('Y-m-d H:i:s');
        if ($req->accpet_document != null) {
            foreach ($req->accpet_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/accept_appeal'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
            // echo "<pre>"; print_r($decode_image);exit;
        }
        Appeal::where('appeals.id', $req->id)->update([
            'remarks_status' => '1',
            'appeal_status' => 2,
            'remarks' => $req->appealRemarks,
            'remarks_by' => $user->user_role,
            'remarks_date' => $date,
            'appeal_document' => $decode_image,
            'appeal_deadline' => $req->appeal_deadline,
            'appeal_assign_to' => $req->appeal_assign_to
        ]);
        $Conversations = new Conversations;
        $Conversations->conversation_type = 3;
        $Conversations->parent_id = $req->id;
        $Conversations->sender_id = $user->id;
        $Conversations->reciever_id = $users_id;
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->appealRemarks;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        // return $users_id;
        if ($user->user_role == 'faa') {
            $application_status = 10;

            $phone = Contact::where('school_id', $req->AppealSchoolID)->value('mobile');
            $msg = 'Your appeal has been approved by First Appellate Authority and is in progress with concern authority.';
            $this->TrackingSMS($phone,$msg); 

        } else if ($user->user_role == 'saa') {
            $application_status = 13;

            $phone = Contact::where('school_id', $req->AppealSchoolID)->value('mobile');
            $msg = 'Your appeal has been approved by Second Appellate Authority and is in progress with concern authority.';
            $this->TrackingSMS($phone,$msg); 


        }
        $Application_ID = application_data::where('application_data.school_id', $req->AppealSchoolID)->value('id');
        // return $Application_ID;
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $Application_ID;
        $application_tracking->school_id = $req->AppealSchoolID;
        $application_tracking->application_status_id = $application_status;
        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        application_data::where('id', $Application_ID)->update([
            'application_status' => $application_status,
            'updated_by' => $user->id
        ]);
        return redirect('appeal-list')->with('success', 'Appeal Accepted Successfully !!!');
    }
    public function rejectRemarkAppeal(Request $req)
    {
        $user = Auth::user();
        $decode_image = '';
        $date = Date('Y-m-d H:i:s');
        if ($req->accpet_document != null) {
            foreach ($req->accpet_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/accept_appeal'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
            // echo "<pre>"; print_r($decode_image);exit;
        }
        $users = Appeal::where('appeals.id', $req->id)->update([
            'remarks_status' => '2',
            'appeal_status' => 3,
            'remarks' => $req->appealRejectRemarks,
            'remarks_by' => $user->user_role,
            'remarks_date'   => $date,
            'appeal_document' => $decode_image
        ]);
        $Conversations = new Conversations;
        $Conversations->conversation_type = '3';
        $Conversations->parent_id = $req->id;
        $Conversations->sender_id = $user->id;
        $Conversations->reciever_id = $req->AppealSchoolID;
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->appealRejectRemarks;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        if ($user->user_role == 'faa') {
            
            $application_status = 11;
            $phone = Contact::where('school_id', $req->AppealSchoolID)->value('mobile');
            $msg = 'Your appeal has been declined by First Appellate Authority due to some recognised issue.';
            $this->TrackingSMS($phone,$msg);  

        } else if ($user->user_role == 'saa') {
            $application_status = 14;

            $phone = Contact::where('school_id', $req->AppealSchoolID)->value('mobile');
            $msg = ' Your appeal has been declined by Second Appellate Authority due to some recognised issue.';
            $this->TrackingSMS($phone,$msg); 

        }
        $Application_ID = application_data::where('application_data.school_id', $req->AppealSchoolID)->value('id');
        // return $Application_ID;
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $Application_ID;
        $application_tracking->school_id = $req->AppealSchoolID;
        $application_tracking->application_status_id = $application_status;
        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        application_data::where('id', $Application_ID)->update([
            'application_status' => $application_status,
            'updated_by' => $user->id
        ]);
        return redirect('appeal-list')->with('warning', 'Appeal Rejected Successfully !!!');
    }
    public function replyAppeal(Request $req)
    {
        // return $req;
        $decode_image = '';
        $Send_user_district = Address::where('addresses.school_id', $req->AppealSchoolID)
            ->value('district');
        $Send_user_division = Address::where('addresses.school_id', $req->AppealSchoolID)
            ->value('division');
        $users_id = User::join('addresses', 'addresses.user_id', '=', 'users.id');
        if ($req->appeal_assign_to == 'dc' || $req->appeal_assign_to == 'dse') {
            $users_id = $users_id->where('addresses.district', $Send_user_district);
        }
        if ($req->appeal_assign_to == 'faa') {
            $users_id = $users_id->where('addresses.division', $Send_user_division);
        }
        if ($req->appeal_assign_to == 'saa') {
            $users_id = $users_id->where('addresses.school_state', 'Jharkhand');
        }
        $users_id = $users_id->where('users.user_role', $req->appeal_assign_to)
            ->value('users.id');
        $user = Auth::user();
        $date = Date('Y-m-d H:i:s');
        if ($req->reply_document != null) {
            foreach ($req->reply_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/accept_appeal'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
        }
        $Conversations = new Conversations;
        $Conversations->conversation_type = '3';
        $Conversations->parent_id = $req->id;
        $Conversations->sender_id = $user->id;
        $Conversations->reciever_id = $users_id;
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->replyRemarks;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        // return $users_id;
        return redirect('appeal-list')->with('warning', 'Appeal Replied Successfully !!!');
    }
    public function updateData(Request $req)
    {
        // return $req;
        $user = Auth::user();
        $appealAutherity = $req->appealAutherity;
        $FAAOfiicerName = $req->FAAOfiicerName;
        $SAAOfiicerName = $req->SAAOfiicerName;
        if ($appealAutherity == '1') {
            $appeal_to = $FAAOfiicerName;
        } else {
            $appeal_to = $SAAOfiicerName;
        }
        $Appeal = Appeal::where('id', $req->id)->update([
            'appeal_text' => $req->appealText,
            'appellate_authority_type' => $req->appealAutherity,
            'appeal_to' =>  $appeal_to,
            'updated_ip' => $_SERVER['REMOTE_ADDR'],
            'updated_by' => $user->id
        ]);
        return redirect('appeal-list')->with('success', 'User Update Successfully !!!');
    }
    public function UpdateAppealStatus($request)
    {
        $Conversations = Conversations::Where('conversations.parent_id', $request)
            ->where('conversations.conversation_type', 3);
        if (Auth::user()->user_role == 'school')
            $Conversations->where('school_read_status', 0)->update(['school_read_status' => 1]);
        if (Auth::user()->user_role == 'dc')
            $Conversations->where('dc_read_status', 0)->update(['dc_read_status' => 1]);
        if (Auth::user()->user_role == 'dse')
            $Conversations->where('dse_read_status', 0)->update(['dse_read_status' => 1]);
        if (Auth::user()->user_role == 'faa')
            $Conversations->where('faa_read_status', 0)->update(['faa_read_status' => 1]);
        if (Auth::user()->user_role == 'saa')
            $Conversations->where('saa_read_status', 0)->update(['saa_read_status' => 1]);
        return 1;
    }

    
    public function TrackingSMS($phone,$msg)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        $message = $msg;
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
    }
   
    //function to send single unicode sms
    public function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }

    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }

    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }

    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        echo $result = curl_exec($post); //result from mobile seva server
        curl_close($post);
    }

    
}
