<?php

namespace App\Http\Controllers;

use App\Models\FndPageData;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    public function aboutUs()
    {
        //return  view('home.aboutUs');
        $pageData = FndPageData::where('id',1)
                                ->where('route_id',1)
                                ->where('page_id',1)
                                ->first();
        return  view('home.about-us',['data'=>$pageData]);
    }
     public function index()
     {
         return  view('home.aboutUs');
     }
     ############## Organisation Section ################

     public function managementPage()
     {
        $pageData = FndPageData::LeftJoin('fnd_pages','fnd_pages.id','fnd_page_data.page_id')
                                ->where('fnd_page_data.route_id',2)
                                ->where('fnd_page_data.page_id',2)
                                ->select('fnd_page_data.*','fnd_pages.name as page_name')
                                ->get();
        return  view('organisation.management',['data'=>$pageData]);
     }
     public function seniorFunctionaries()
     {
        return  view('organisation.seniorFunctionaries');
     }
     public function disclosureUnderRti()
     {
        return  view('organisation.disclosureUnderRti');
     }
     public function programmeAdvisoryCommittee()
     {
        return  view('organisation.programmeAdvisoryCommittee');
    }
    
    ############## Constituents and Departments ################
    
    public function nationalInstituteEducation()
    {
        return  view('constituents.nationalInstituteEducation');

    }
    public function regionalInstitutesEducation()
    {
        return  view('constituents.regionalInstitutesEducation');

    }
    public function centralInstituteofEeducationalTechnology()
    {
        return  view('constituents.centralInstituteofEeducationalTechnology');

    }
    public function pssVocationalEducation()
    {
        return  view('constituents.pssVocationalEducation');

    }

    ############## Programmes ################
    public function nationalTalentExamination()
    {
        return  view('programmes.nationalTalentExamination');

    }
    public function allindiaSchoolEducationSurvey()
    {
        return  view('programmes.allindiaSchoolEducationSurvey');

    }
    public function teacherInnovationAwards()
    {
        return  view('programmes.teacherInnovationAwards');

    }
    public function jnNationalScienceExhibition()
    {
        return  view('programmes.jnNationalScienceExhibition');

    }
    public function mediaProgrammes()
    {
        return  view('programmes.mediaProgrammes');

    }
    public function researchGrantsRric()
    {
        return  view('programmes.researchGrantsRric');

    }
    #######################Gallery Menu################

    public function internationalYoga()
    {
        return  view('gallery.internationalYoga');

    }
    public function kalaUtsav()
    {
        return  view('gallery.kalaUtsav');

    }
    public function foundationDay()
    {
        return  view('gallery.foundationDay');

    }
    public function eRaksha()
    {
        return  view('gallery.eRaksha');

    }
    public function healthandWell()
    {
        return  view('gallery.healthandWell');

    }
    
    ####################### Publication ################
    public function listPublication()
    {
        return  view('publication.listPublication');

    }
    public function ePubblication()
    {
        return  view('publication.ePubblication');

    }
    public function Flipbook()
    {
        return  view('publication.Flipbook');

    }
    public function textbook()
    {
        return  view('publication.textbook');

    }
    public function stateUtseBook()
    {
        return  view('publication.stateUtseBook');

    }
    public function vocationalEducation()
    {
        return  view('publication.vocationalEducation');

    }
    #################vacancies ############################
    public function vacancies()
    {
        return  view('announcement.vacancies');

    }
    public function tenders()
    {
        return  view('announcement.tenders');

    }
    public function notices()
    {
        return  view('announcement.notices');

    }
    public function otherAnnouncements()
    {
        return  view('announcement.otherAnnouncements');

    }
    public function seminar()
    {
        return  view('announcement.otherAnnouncements');

    }
    ###############Contacts#################################
    public function personContact()
    {
        return  view('contacts.personContact');

    }
    public function publicInformationOfficers()
    {
        return  view('contacts.publicInformationOfficers');

    }
    public function telephoneDirectory()
    {
        return  view('contacts.telephoneDirectory');

    }
}
