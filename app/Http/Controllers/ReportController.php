<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fnd_value_set;
use App\Models\Fnd_value;
use App\Models\User;
use App\Models\Payment;
use App\Models\Private_school;
use App\Models\application_data;
use App\Models\application_tracking;
use App\Models\application_statuses;
use Carbon\Carbon;
use App\Exports\ExcelExport;
use App\Exports\PaymentExport;
use App\Exports\RejectAppExport;
use App\Models\fnd_settings;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;

class ReportController extends Controller
{
    public $data = [];
    public function report()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('reports/report');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }


    //chat
    public function chat(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('chat');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function index()
    {
        $Districts = Fnd_value_set::select(
            'fnd_value_sets.id',
            'fnd_value_sets.value_set_name'
        )->where('fnd_value_sets.parent_value_set_id', '=', '289')
            ->get();
        // $Users = User::all();
        $Application_Status = application_statuses::select(
            'application_statuses.id',
            'application_statuses.status_name'
        )->get();
        return view('DGR', compact('Districts'));
    }
    public function reportAllApplications(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $this->data['Application_Status'] = application_statuses::orderBy('id')
                    ->pluck(
                        'application_statuses.status_name',
                        'application_statuses.id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $application_id = $request->application_id;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                $applicationName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                if (isset($request->application_id) && $request->application_id != null) {
                    $applicationName = application_statuses::where('application_statuses.id',  $application_id)->value('status_name');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName, 'application_id' => $application_id, 'applicationName' => $applicationName
                ];
                $now = Carbon::now();
                $this->data['AllDetails'] = application_data::leftJoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->leftJoin('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->leftJoin('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    // ->leftJoin('application_trackings', 'application_trackings.application_id', 'application_data.id')
                    ->leftJoin('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.updated_at',
                        'fnd_division.display_value AS Division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name',
                    );


                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.status', 1)->where('application_data.application_status', '>', 1);
                    } else if ($request->application_status == 'Pending') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.status', 1)->whereIn('application_data.application_status', [2, 3, 4, 9, 12]);
                    } else if ($request->application_status == 'Approved') {

                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.status', 1)->whereIn('application_data.application_status', [4, 6, 10, 13]);
                    } else if ($request->application_status == 'Rejected') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.status', 1)->whereIn('application_data.application_status', [7, 8, 11, 14]);
                    } else {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.status', 1)->where('application_data.application_status', $request->application_status);
                    }
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.status', 1)->where('application_data.application_status', '>', 1);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->application_id) && $request->application_id != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status',  $request->application_id);
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->paginate(25);
                }
                // "<pre>";
                // echo
                // print_r($this->data['AllDetails']);
                // exit;
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                // $days_to_varified_by_dse = fnd_settings::value('verification_by_dse_limit');
                // $days_for_final_desicion_by_dc = fnd_settings::value('schedule_meeting_by_dc_limit');
                // $days_to_respond_after_meeting_held_by_dc = fnd_settings::value('take_decision_after_meeting_held_by_dc');
                // $days_to_take_action_by_faa = fnd_settings::value('faa_decision_limit');
                // $days_to_take_action_by_saa = fnd_settings::value('saa_decision_limit');
                // $days_to_varified_by_saa = fnd_settings::value('school_appeal_limit');
                // $days_to_varified_by_saa = fnd_settings::value('school_second_appeal_after_rejection_limit');

                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    // $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportAllApplications_excel', 'total' => $this->data['count']];
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportAllApplications_excel'];
                    $filename = 'all-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportAllApplications', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportPendingApplications(Request $request)
    {
        // return($request);
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $this->data['AllDetails']  = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', [2, 3, 5, 9, 12]);
                    } else if ($request->application_status == 'Pending_DSE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', [2, 3]);
                    } else if ($request->application_status == 'Pending_DC') {

                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 4);
                    } else if ($request->application_status == 'Pending_DPE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 12);
                    } else if ($request->application_status == 'Pending_RDDE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 9);
                    }
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', [2, 3, 5, 9, 12]);
                }

                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportPendingApplications_excel'];
                    $filename = 'pending-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportPendingApplications', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportApprovedApplications(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $this->data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.block',
                        'application_data.district',
                        'application_data.division',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'fnd_division.display_value AS Division',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {

                        $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', [4, 6, 10, 13]);
                    } else if ($request->application_status == 'Approved_DSE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 4);
                    } else if ($request->application_status == 'Approved_DC') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 6);
                    } else if ($request->application_status == 'Approved_RDDE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 10);
                    } else if ($request->application_status == 'Approved_DPE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 13);
                    }
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', [4, 6, 10, 13]);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                $this->data['count'] = $RemainingDate;
                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportApprovedApplications_excel'];
                    $filename = 'approve-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportApprovedApplications', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportDelayedApplications(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $this->data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {

                        $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', ['18', '19', '20', '21', '22']);
                    } else if ($request->application_status == 'Delayed_DSE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 18);
                    } else if ($request->application_status == 'Delayed_DC_Meeting') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 19);
                    } else if ($request->application_status == 'Delayed_DC_verification') {

                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 20);
                    } else if ($request->application_status == 'Delayed_RDDE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 21);
                    } else if ($request->application_status == 'Delayed_DPE') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 22);
                    }
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', ['18', '19', '20', '21', '22']);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                $this->data['count'] = $RemainingDate;
                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportDelayedApplications_excel'];
                    $filename = 'delayed-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportDelayedApplications', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportRejectedApplications(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $division_id = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::user()->id)
                    ->value('addresses.division');
                $this->data['district_list'] = Fnd_value::where('fnd_values.value_set_id', '=', $division_id)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as district_name',
                        'fnd_values.id as district_id'
                    );
                $search_text = $request->search_text;
                $appeal_status = $request->appeal_status;
                $block = $request->Block;
                $division = $request->division;
                $district = $request->district;
                $fromDate = $request->from_date;
                $toDate = $request->to_date;
                $blockName = null;
                $divisionName = null;
                $districtName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = ['search_text' => $search_text, 'block_id' => $block, 'blockName' => $blockName, 'division_id' => $division, 'divisionName' => $divisionName, 'district_id' => $district, 'districtName' => $districtName, 'from_date' => $fromDate, 'to_date' => $toDate];
                $this->data['UserDistrict'] = DB::table('addresses')
                    ->where('addresses.user_id', Auth::user()->id)
                    ->select('addresses.district')
                    ->get();
                $this->data['UserDistrict'] =  $this->data['UserDistrict'][0]->district;
                $now = Carbon::now();
                $this->data['TotalData']  = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {
                        $this->data['TotalData'] = $this->data['TotalData']->whereIn('application_data.application_status', ['7', '8', '11', '14']);
                    } else if ($request->application_status == 'Rejected_DSE') {
                        $this->data['TotalData'] = $this->data['TotalData']->where('application_data.application_status', 7);
                    } else if ($request->application_status == 'Rejected_DC') {
                        $this->data['TotalData'] = $this->data['TotalData']->where('application_data.application_status', 8);
                    } else if ($request->application_status == 'Rejected_RDDE') {

                        $this->data['TotalData'] = $this->data['TotalData']->where('application_data.application_status', 11);
                    } else if ($request->application_status == 'Rejected_DPE') {
                        $this->data['TotalData'] = $this->data['TotalData']->where('application_data.application_status', 14);
                    }
                } else {
                    $this->data['TotalData'] = $this->data['TotalData']->whereIn('application_data.application_status', ['7', '8', '11', '14']);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['TotalData'] =  $this->data['TotalData']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('status.status_name', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['TotalData'] =  $this->data['TotalData']->where('application_data.division', $division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['TotalData'] =  $this->data['TotalData']->where('application_data.district', $district);
                }
                if (isset($request->Block) && $request->Block != null) {
                    $this->data['TotalData'] =  $this->data['TotalData']->where('application_data.block', $block);
                }
                if ($request->from_date != null &&  $request->to_date != null) {
                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $fromDate)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $toDate)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return redirect()->back()->with('warning', "Form date always lesser than To date");
                    }
                    //return[$start_date,$end_date];  
                    $this->data['TotalData'] =  $this->data['TotalData']->whereBetween('application_data.created_at', [$fromDate, $toDate]);
                }
                $this->data['count_TotalData'] = $this->data['TotalData']->count();
                if ($request->submit == 'excel') {
                    $this->data['TotalData'] = $this->data['TotalData']->get();
                } else {
                    $this->data['TotalData'] = $this->data['TotalData']->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['TotalData'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->submit == 'excel') {
                    if (count($this->data['TotalData']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['TotalData'], 'view' => 'excel_view.report_rejected_application_excel'];
                    // return[$params];
                    $filename = 'RejectedApplication.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportRejectedApplications',  $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportDcMeetings(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $this->data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.block',
                        'application_data.district',
                        'application_data.division',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'fnd_division.display_value AS Division',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1')
                    ->where('application_data.application_status', '5');
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportDcMeetings_excel',];
                    $filename = 'Dc-meeting-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportDcMeetings', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportRddeAppeals(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $UserDistrict = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::user()->id)
                    ->value('addresses.district');
                $this->data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as district_name',
                        'fnd_values.id as district_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = ['search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName, 'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName];
                $now = Carbon::now();
                $this->data['AllDetails']  = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');

                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {

                        $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', ['9', '10', '11']);
                    } else if ($request->application_status == 'Pending') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 9);
                    } else if ($request->application_status == 'Accepted') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 10);
                    } else if ($request->application_status == 'Rejected') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 11);
                    }
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', ['9', '10', '11']);
                }

                $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc');
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.id', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->Block) && $request->Block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->Block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->paginate(10);
                }
                // return(count($this->data['AllDetails']));
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportRdde_excel'];
                    $filename = 'report-rdde-list.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportRddeAppeals', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportDpeAppeals(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $this->data['AllDetails']  = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', ['12', '13', '14']);
                    } else if ($request->application_status == 'Pending') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 12);
                    } else if ($request->application_status == 'Accepted') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 13);
                    } else if ($request->application_status == 'Rejected') {
                        $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.application_status', 14);
                    }
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->whereIn('application_data.application_status', ['12', '13', '14']);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportDpeAppeals_excel'];
                    $filename = 'DPE-Appeals-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportDpeAppeals', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportPayments(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                $amount = 0;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $this->data['payments'] = Payment::leftjoin('application_data', 'application_data.school_id', 'payments.school_id')
                    ->leftjoin('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->leftjoin('fnd_values as fnd_district', 'application_data.district', 'fnd_district.id')
                    ->leftjoin('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->select(
                        'payments.*',
                        'application_data.school_name',
                        'application_data.school_name',
                        'application_data.school_name',
                        'application_data.school_name',
                        'application_data.phone_with_std_code as phone_no',
                        'application_data.school_chairman_phone_number as mobile_no',
                        'application_data.school_chairman_name as contact_name',
                        'application_data.village_city',
                        'application_data.post_office',
                        'application_data.panchayat',
                        'fnd_division.display_value as division',
                        'fnd_district.display_value as district',
                        'fnd_block.display_value as block',
                    )
                    ->where('application_data.status', 1)
                    ->where('payments.status', 1);
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['payments'] = $this->data['payments']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['payments'] = $this->data['payments']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['payments'] = $this->data['payments']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['payments'] = $this->data['payments']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['payments'] = $this->data['payments']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['payments'] = $this->data['payments']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                foreach ($this->data['payments'] as $key => $value) {
                    $amount = $value->payment_amount + $amount;
                }
                $this->data['total_amount'] = $amount;
                if ($request->excel == 'excel') {
                    if (count($this->data['payments']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['payments'], 'view' => 'excel_view.reportDpeAppeals_excel'];
                    $filename = 'DPE-Appeals-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportPayments', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportRegisteredSchool(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $this->data['AllDetails'] = application_data::Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_district', 'application_data.district', 'fnd_district.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.block',
                        'application_data.district',
                        'application_data.division',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'fnd_division.display_value AS Division',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1')
                    ->where('application_data.application_status', '2');
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportRegisteredschool_excel'];
                    $filename = 'registered-school-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportRegisteredSchool', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function reportCertifiedApplications(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                $from_date = null;
                $to_date  = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $this->data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName, 'from_date' => $from_date, 'to_date' => $to_date
                ];
                $now = Carbon::now();
                $this->data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.block',
                        'application_data.district',
                        'application_data.division',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'fnd_division.display_value AS Division',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1')
                    ->where('application_data.application_status', '6');
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $this->data['AllDetails'] = $this->data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $this->data['AllDetails'] = $this->data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(25);
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = '';
                $end_day = '';
                foreach ($this->data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                }

                $this->data['count'] = $RemainingDate;
                if ($request->excel == 'excel') {
                    if (count($this->data['AllDetails']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllDetails'], 'view' => 'excel_view.reportCertifiedApplications_excel'];
                    $filename = 'approve-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return view('reports/reportCertifiedApplications', $this->data);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }

    public function  Block($id)
    {
        $Districts = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.parent_value_id', '=', $id)
            ->get();
        return json_encode($Districts);
        // $Users = User::all();
        return view('DGR', compact('Districts'));
    }
    public function  Village($id)
    {
        $Districts = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.parent_value_id', '=', $id)
            ->get();
        return json_encode($Districts);
        return view('DGR', compact('Districts'));
    }
    // myformAjax
    // block list....
    public function  blockList($id)
    {
        $Districts = Fnd_value::where('fnd_values.parent_value_id', '=', $id)
            ->pluck(
                'fnd_values.display_value',
                'fnd_values.id'
            );
        return json_encode($Districts);
    }
    // district list
    public function  districtList($id)
    {
        $Districts = Fnd_value::where('fnd_values.parent_value_id', '=', $id)
            ->pluck(
                'fnd_values.display_value',
                'fnd_values.id'
            );
        // $cities = DB::table("demo_cities")
        //             ->where("state_id",$id)
        //             ->lists("name","id");
        return json_encode($Districts);
        // $Users = User::all();
    }
}
