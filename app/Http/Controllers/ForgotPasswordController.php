<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Session;
class ForgotPasswordController extends Controller
{
    //
    public function index()
    {
        return view('auth.forgot-password');
    }
    function store(Request $request)
    {
        // return ($request);
        $request->validate([
            'email' => 'required',
            'contact' => 'required|numeric|min:10',
            'mobile_otp' => 'required|min:4',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|min:8|max:20|same:password',
        ], [
            'email.required' => 'Email is required',
            'contact.required' => 'Contact is required',
            'contact.min' => 'Mobile number must be 10 digits.',
        ]);
        if ($request->mobile_otp != Session::get('phone_otp')) {
            // return back()->json(['Msg'=>'Mobile OTP did not matched','ChKval'=>1]);
            return back()->with('message', 'Mobile OTP did not matched.');
        }
        $User = User::leftJoin('contacts', 'contacts.user_id', '=', 'users.id')->where('users.status', 1);
        $email = $request->email;
        $User = $User->where(function ($query) use ($email) {
            $query->where('users.username', 'like', $email)
                ->orWhere('users.email', 'like', $email);
        });
        $User = $User->first();
        if (empty($User)) {
            return back()->with('message', 'Please check username or email.');
        } else {
            if ($request->contact != $User->mobile) {
                return back()->with('message', 'Please check mobile number.');
            }
        }
        $User->password = \Hash::make($request->password);
        $User->save();
        return redirect('login')->with('message', 'Your password reset successfully.');
    }
    public function testsms($phone)
    {
        return ($phone);
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        // $message = "Your Normal message here "; //message content
        // $messageUnicode = "मोबाइल सेवा में आपका स्वागत है "; //message content in unicode
        // session_start();
        // $_SESSION['phone_otp'] = $otp = rand(1111,9999);
        $otp = rand(1111, 9999);
        Session::put('phone_otp', $otp);
        $message = "Your OTP to verify your phone number is: " . $otp . ", Use this OTP to register."; //message content in unicode
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $mobileNos = "8797490173,8797490173"; //if bulk sms need to send use mobileNos as keyword and mobile number seperated by commas as value
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        //call method and pass value to send single sms, uncomment next line to use
        // $this->sendSingleSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
        //call method and pass value to send otp sms, uncomment next line to use
        $this->sendOtpSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
        //call this method and pass value to send bulk sms, uncomment next line to use
        //$this->sendBulkSMS($username,$encryp_password,$senderid,$message,$mobileNos,$deptSecureKey);
        //call this method for sending single unicode sms, uncomment next line to use
        //$this->sendSingleUnicode($username,$encryp_password,$senderid,$messageUnicode,$mobileno,$deptSecureKey);
        //call this method for sending single unicode otp sms, uncomment next line to use
        //$this->sendUnicodeOtpSMS($username,$encryp_password,$senderid,$messageUnicode,$mobileno,$deptSecureKey);
        //call this method to send bulk unicode sms, uncomment next line to use
        //$this->sendBulkUnicode($username,$encryp_password,$senderid,$messageUnicode,$mobileNos,$deptSecureKey);
    }
    function testSuccess($phone, $email, $password_1)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        $message = "You have successfully registred for Private School Recognition System. For login please use folowing credentials, Username: " . $email . " Password" . $password_1 . "."; //message content in unicode
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
    }
    //function to send sms using by making http connection
    function post_to_url($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($post); //result from mobile seva server
        echo $result; //output from server displayed
        curl_close($post);
    }
    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        echo $result = curl_exec($post); //result from mobile seva server
        curl_close($post);
    }
    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }
    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }
    //Function to send single sms
    function sendSingleSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "singlemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url to send sms
    }
    //Function to send otp sms
    function sendOtpSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "otpmsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url to send otp sms
    }
    //function to send bulk sms
    function sendBulkSMS($username, $encryp_password, $senderid, $message, $mobileNos, $deptSecureKey)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "bulkmsg",
            "bulkmobno" => trim($mobileNos),
            "key" => trim($key)
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url to send bulk sms
    }
    //function to send single unicode sms
    function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }
    //function to send bulk unicode sms
    function sendBulkUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileNos, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "bulkmobno" => trim($mobileNos),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send bulk unicode sms
    }
    //function to send single unicode otp sms
    function sendUnicodeOtpSMS($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodeotpmsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }
}
