<?php

namespace App\Http\Controllers;

use App\Models\RteFtp;
use App\Models\RteFtpLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;

class RteFtpController extends Controller
{
    //
    public $data = [];
    public function view()
    {
        if (Auth::check()) {
            if ((Auth::user()->user_role == 'superadmin') && (Auth::user()->school_id == '-1')) {
                return view('ftp_view');
            } else {
                return redirect('dashboard')->with('danger', 'You are not Authorised!!');
            }
        }
    }
    public function viewFTPDataList($directory_name)
    {
        if (Auth::check()) {
            if ((Auth::user()->user_role == 'superadmin') && (Auth::user()->school_id == '-1')) {
                $data = RteFtp::where('directory_name', $directory_name)->orderBy('file_name','ASC')->get();
                $tbody = '';
                foreach ($data as $key => $value) {
                    $id = $key + 1;
                    $path = $value->file_location . "/" . $value->file_name . '.' . $value->file_type;
                    // return $path;
                    $url = 'download-ftp-data-list/' . $value->file_location . '/' . $value->file_name . '/' . $value->file_type;
                    $tbody .= '<tr>';
                    $tbody .= '<td class="id">' . $id . '</td>';
                    $tbody .= '<td class="file_name">' . $value->file_name . '</td>';
                    $tbody .= '<td class="file_location">' . $value->file_location . '</td>';
                    $tbody .= '<td class="directory_name">' . $value->directory_name . '</td>';
                    $tbody .= '<td class="file_type">' . $value->file_type . '</td>';
                    $tbody .= '<td><span class="" style="display:inline-block;">';
                    $tbody .= '<form action="download-ftp-data-list" method="post" enctype="multipart/form-data" >';
                    $tbody .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
                    $tbody .= '<input type="hidden" name="file_id" value="' . $value->id . '" />';
                    $tbody .= '<input type="hidden" name="location" value="' . $value->file_location . '" />';
                    $tbody .= '<input type="hidden" name="file_name" value="' . $value->file_name . '" />';
                    $tbody .= '<input type="hidden" name="file_type" value="' . $value->file_type . '" />';
                    $tbody .= '<button type="submit" class="btn btn-sm btn-success" style="margin-right:5px;">';
                    $tbody .= '<i class="fa fa-download" title="Download File " aria-hidden="true"></i>';
                    $tbody .= '</button>';
                    $tbody .= '</form></span>';
                    // $tbody .='<span class="" style="display:inline-block;">';
                    // $tbody .='<a href="'.$path.'" download>';
                    // $tbody .='<button class="btn btn-sm btn-danger"><i class="fa fa-download" aria-hidden="true"></i></button></a></span>';
                    $tbody .= '<span class="" style="display:inline-block;"><button class="btn btn-sm btn-primary upload_button " title="Upload File" id="upload_button_id" ';
                    $tbody .= ' onclick="uploadFunction(' . $value->id . ');" data-toggle="modal" data-target="#upload-document"><i class="fa fa-upload" aria-hidden="true"></i></button></span>';
                    $tbody .= ' </td>';
                    $tbody .= '</tr>';
                }
                // return 'viewFTPDataList';
                return ['data' => $tbody];
            } else {
                return redirect('view-ftp')->with('danger', 'You are not Authorised!!');
            }
        }
    }
    public function download_file(Request $request)
    {
        // return $request;
        if (Auth::check()) {
            if ((Auth::user()->user_role == 'superadmin') && (Auth::user()->school_id == '-1')) {
                // http://localhost/rte-live/view-ftp-data-list/controller
                // http://localhost/rte-live/app/Http/Controllers/application_formController.php
                // http://localhost/rte-live/resources/views/addAppeal.blade.php
                // $file = 'resources/views/addAppeal.blade.php';
                $RteFtp = RteFtp::where('id', $request->file_id)->first();
                // return $RteFtp->id;
                $RteFtpLog = new RteFtpLog();
                $RteFtpLog->file_id =  $RteFtp->id;
                $RteFtpLog->remarks =  $RteFtp->remarks;
                $RteFtpLog->created_by = Auth::user()->id;
                $RteFtpLog->file_created_by =  $RteFtp->created_by;
                $RteFtpLog->file_created_at =  $RteFtp->created_at;
                $RteFtpLog->file_last_update_by =  $RteFtp->updated_by;
                $RteFtpLog->file_last_update_at =  $RteFtp->updated_at;
                $RteFtpLog->ip = $_SERVER['REMOTE_ADDR'];
                $RteFtpLog->file_status = 1;
                $RteFtpLog->save();
                $file = $request->location . '/' . $request->file_name . '.' . $request->file_type;
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit;
            }
        } else {
            return redirect('view-ftp')->with('danger', 'You are not Authorised!!');
        }
    }
    public function upload_file_in_ftp(Request $request)
    {
        // return $request;
        if (Auth::check()) {
            if ((Auth::user()->user_role == 'superadmin') && (Auth::user()->school_id == '-1')) {
                // return $request;
                $path =  $request->upload_file_location . '/' . $request->upload_file_name . '.' . $request->upload_file_type;
                $uploded_file_name = $request->upload_file_name . '.' . $request->upload_file_type;
                if (isset($request->is_upload) && $request->is_upload == 'update') {
                    if (file_exists($path)) {
                        $fileName = $request->file_upload->getClientOriginalName();
                        // return $fileName;
                        if ($fileName == $uploded_file_name) {
                            // return('File matched');
                            $RteFtp = RteFtp::where('id', $request->upload_file_id)->first();
                            $RteFtpLog = new RteFtpLog();
                            $RteFtpLog->file_id =  $RteFtp->id;
                            $RteFtpLog->remarks =  $RteFtp->remarks;
                            $RteFtpLog->created_by = Auth::user()->id;
                            $RteFtpLog->file_created_by =  $RteFtp->created_by;
                            $RteFtpLog->file_created_at =  $RteFtp->created_at;
                            $RteFtpLog->file_last_update_by =  $RteFtp->updated_by;
                            $RteFtpLog->file_last_update_at =  $RteFtp->updated_at;
                            $RteFtpLog->ip = $_SERVER['REMOTE_ADDR'];
                            $RteFtpLog->file_status = 2;
                            $RteFtpLog->save();
                            $decode_image = '';
                            $upload_file_id = $request->upload_file_id;
                            $RteFtp->file_name = $request->upload_file_name;
                            $RteFtp->directory_name = $request->upload_folder_name;
                            $RteFtp->file_location = $request->upload_file_location;
                            $RteFtp->file_type = $request->upload_file_type;
                            $RteFtp->remarks = $request->remarks;
                            $RteFtp->save();
                            $file_upload = $request->file_upload;
                            $image_name = array();
                            if ($request->file_upload != null) {
                                if (isset($request->file_upload) && !empty($request->file_upload)) {
                                    $fileName = $request->file_upload->getClientOriginalName();
                                    $request->file_upload->move(base_path($request->upload_file_location), $fileName);
                                    $image_name[] = $fileName;
                                }
                            }
                        } else {
                            // return('File Not Match.');
                            return redirect('view-ftp')->with('danger', 'Uploded File Not Match.!!');
                        }
                    }
                    // return('File is upload.');
                    return redirect('view-ftp')->with('success', 'File is uploaded.!!');
                } else if (isset($request->is_upload) && $request->is_upload == 'add') {

                    $add_folder_name = $request->add_folder_name;

                    if ($add_folder_name == 'routes') {
                        $path = 'routes/' . $request->file_name . '.php';
                        $upload_file_location = 'routes';
                    }
                    if ($add_folder_name == 'Controllers') {
                        $path = 'app/Http/Controllers/' . $request->file_name . '.php';
                        $upload_file_location = 'app/Http/Controllers';
                    }
                    if ($add_folder_name == 'views') {
                        $path = 'resources/views/' . $request->file_name . '.php';
                        $upload_file_location = 'resources/views';
                    }
                    if ($add_folder_name == 'config') {
                        $path = 'config/' . $request->file_name . '.php';
                        $upload_file_location = 'config';
                    }

                    if (file_exists($path)) {
                        // return('file exist ');
                        return redirect('view-ftp')->with('danger', 'File Already Exist.!!');
                    } else {
                        // return $request;
                        if ($request->file_upload != null) {
                            if (isset($request->file_upload) && !empty($request->file_upload)) {
                                $extension = $request->file_upload->getClientOriginalExtension();
                                // return $extension;
                                if ($extension == 'php') {
                                    $request->file_upload->move(base_path($upload_file_location), $request->file_name . '.' . $extension);
                                    $RteFtp = new RteFtp();
                                    $RteFtp->school_id =  '-1';
                                    $RteFtp->authorised_id = Auth::user()->id;
                                    $RteFtp->file_name =  $request->file_name;
                                    $RteFtp->directory_name = $request->add_folder_name;
                                    $RteFtp->file_location = $upload_file_location;
                                    $RteFtp->file_type = $extension;
                                    $RteFtp->remarks = $request->remarks;
                                    $RteFtp->created_by = Auth::user()->id;
                                    $RteFtp->save();

                                    $RteFtpLog = new RteFtpLog();
                                    $RteFtpLog->file_id =  $RteFtp->id;
                                    $RteFtpLog->remarks =  $RteFtp->remarks;
                                    $RteFtpLog->created_by = Auth::user()->id;
                                    $RteFtpLog->file_created_by =  $RteFtp->created_by;
                                    $RteFtpLog->file_created_at =  $RteFtp->created_at;
                                    $RteFtpLog->file_last_update_by =  $RteFtp->updated_by;
                                    $RteFtpLog->file_last_update_at =  $RteFtp->updated_at;
                                    $RteFtpLog->ip = $_SERVER['REMOTE_ADDR'];
                                    $RteFtpLog->file_status = 2;
                                    $RteFtpLog->save();
                                }
                            }
                        }
                        return redirect('view-ftp')->with('success', 'File is uploaded.!!');
                    }
                }
            }
        } else {
            return redirect('view-ftp')->with('danger', 'You are not Authorised!!');
        }
    }
    public function view_file_in_ftp($id)
    {
        if (Auth::check()) {
            if ((Auth::user()->user_role == 'superadmin') && (Auth::user()->school_id == '-1')) {
                $data = RteFtp::where('id', $id)->get();
                return $data;
            } else {
                return redirect('view-ftp')->with('danger', 'You are not Authorised!!');
            }
        }
    }
}
