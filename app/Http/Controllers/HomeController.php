<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Message;  
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;
use App\Models\Group;
use View;
use App\Events\GroupCreated;
use Hash;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    // show all groups that User is Follow
    public function index()
    {
        $my_id = Auth::id();
        // select channels that User Subscribe
        $users = DB::select("select groups.id, groups.name 
        from groups inner JOIN  group_participants ON groups.id = group_participants.group_id and group_participants.user_id = " . Auth::id() . "
        where group_participants.user_id = " . Auth::id() . "
        group by groups.id, groups.name");
        return view('group.home', compact('users'));
    }
    //  get all Channels are in App
    public function subscribe()
    {
        $groupALL = Group::all();
        $UsersAll=User::where('user_role','user')->get();
        
        return view('group.join', compact('groupALL','UsersAll'));
    }
   // unFollow User a Channel 
    public function remove_user($id)
    {
        $group = Group::find($id);  // find Channel in Group Table
        $my_id = Auth::id();        // current User
        $group->participants()->detach($my_id);  // remove User in group_participants Table
        $messages = message::where(['from' => $my_id])->first(); // find User in Messages Table according his Id
        if (is_array($messages) || is_object($messages))
        {
            foreach($messages as $value) {
                message::where(['from' => $my_id])->delete();  // delete all messages of User in Messages Table
            }
        }
        return redirect()->back();
    }
    
    // get messages of user according find Group     
    public function getMessag($id)
    {
        $my_id = Auth::id();
        $group = Group::find($id);
        // get all messages that User sent & got
        $messages = message::where(['group_id' => $id])->where(['user_id' => $my_id])->get();
        foreach($messages as $value) {
            message::where(['user_id' => $my_id])->update(['is_read' => 1]); // if User start to see messages is_read in Table update to 0
        }
        return view('messages.index', compact(['group', 'messages']));
    }
    // update is_read .... this function update messages is not read and update to read in Navbar
    public function getMessage($id)
    {
        $my_id = Auth::id();
        $group = Group::find($id);
        $messages = message::where(['user_id' => $my_id])->get();
        foreach($messages as $value) {
            message::where(['user_id' => $my_id])->update(['is_read' => 1]);
        }
    }
   
   // send new message to all Followers
    public function sendMessage(Request $request)
    {
       
        $this->validate($request, [
            'message' => 'required',
        ]);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $new_image = time() . "." . $image->getClientOriginalName();
            $image->move(public_path('groupchat/image'), $new_image);
        
        }else{
            $new_image=NULL;	
        }
        $to = $request->id; // this part get Group id
        $from = Auth::id();  // this part get  user id who watnts to send message
        $group = Group::find($request->id);  // find group according id
        $name = Auth::user()->name;
        $group_members = $group->participants()->get();
        // return $group_members;
         // send for all Followers
         foreach($group_members as $value) {
            $message = Message::create(
              $data = array(
               'group_id' => $request->id, 
               'user_id' => $value->id, 
               'message' => $request->message,
               'from' => $from,
               'name' => $name, 
               'image'=>$new_image,
               'is_read' => 0,
               ));
               // Pusher send New message at real time
               $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
                );
            $pusher = new Pusher(
                    env('PUSHER_APP_KEY'),
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'), 
                    $options
                );
            $data = ['from' => $to, 'to' => $value->id]; 
            $notify = '' . $value->id .'';
            $pusher->trigger($notify, 'App\\Events\\Notify', $data);
            
            }

        
        return redirect()->back();
    }
    ###############Add User #############
    //add user 
    public function addUser(){
        $groupALL = Group::all();
        return view('group.groupuser', compact('groupALL'));
    }

    public function createUser(Request $request)
    {
       
        $this->validate($request, [
            'name' => 'required',
            'email'=>'required'
        ]);
    
        if ($request->hasFile('profile_photo_path')) {
            $image = $request->file('profile_photo_path');
            $new_image = time() . "." . $image->getClientOriginalName();
            $image->move(public_path('groupchat/image'), $new_image);
        
        }else{
            $new_image=NULL;	
        }
      
       // insert New user to Table
        $group = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'profile_photo_path' => $new_image,
            'user_role' => 'user',
            'username' => $request->name,
            'school_id' => 0,
            
        ]);
        
    
        return redirect('/users-list')->with('success', 'Your user has been created');
    }

    //user list
    public function usersList(){
        $UserALL = User::all();
        return view('group.userslist', compact('UserALL'));
    }
    
    // Delete User from list
    public function usersDelete($id)
    {
        $user=User::find($id);
        $user->delete();
          //Display an error if the user is deleted
          return redirect()->back()->with('success', 'User has been deleted');
    }

    public function chatDisabled()
    {
       $user= User::where('user_role','user')->select('chat_status')->first();
       if($user)
       {
              if($user->chat_status==1)
              {
                User::where('user_role','user')->update(['chat_status'=>0]);
                return redirect()->back()->with('success', 'Chat has been disabled');
              }
              if($user->chat_status==0)
              {
                User::where('user_role','user')->update(['chat_status'=>1]);
                return redirect()->back()->with('success', 'Chat has been enabled');
              }
       }else{
        return redirect()->back()->with('success', 'Use is not Exist');
       }
       
       
      
    }

   
}