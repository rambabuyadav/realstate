<?php
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\rte_applicant_students;
use App\Models\Fnd_value;
use Illuminate\Http\Request;
use Session;
use Auth;
use PDF;
use DB;
class StudentRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('student_register');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        ############ Form Validation #################
        $request->validate([
            'student_name' => 'required|min:2|max:100',
            'user_address' => 'required|min:2|max:200',
            'username' => 'required|min:2|max:50',
            'contact' => 'required|numeric|min:10',
            'mobile_otp' => 'required|min:4',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|min:8|max:20|same:password',
        ], [
            'username.required' => 'User name is required',
            'student_name.required' => 'Student name is required',
            'student_name.min' => 'Student Name must be at least 2 characters.',
            'student_name.max' => 'Student Name should not be greater than 50 characters.',
            'user_address.min' => 'Address must be at least 2 characters.',
            'user_address.max' => 'Address should not be greater than 200 characters.',
            'contact.min' => 'Mobile number must be 10 digits.',
        ]);
        if ($request->mobile_otp != Session::get('phone_otp')) {
            return response()->json(['Msg' => 'Mobile OTP did not matched', 'ChKval' => 1]);
        }
        ################Get User data##############    
        $userData = $request->only(['contact', 'username', 'password', 'user_address']);
        $userData['password'] =  \Hash::make($userData['password']);
        $userData['user_role'] =  'student';
        // $userData['password'] = bcrypt($userData['password']);
        $userData['school_id'] = 1;
        $userData['name'] = $request->student_name;
        $userData['email'] = $request->username;
        $userData['district'] = $request->district;
        $userData['block'] = $request->block;
        User::create($userData);
        $user_id = User::max('id');
        ################Get School data############## 
        $student_details = new rte_applicant_students;
        $student_details->school_id = -2;
        $student_details->user_id = $user_id;
        $student_details->full_name = $request->student_name;
        $student_details->district = $request->district;
        $student_details->block = $request->block;
        $student_details->address = $request->user_address;
        $student_details->post_office = $request->post_office;
        $student_details->phone = $request->contact;
        $student_details->submit_status = 0;
        $student_details->division = Fnd_value::where('id', $request->district)->value('parent_value_id');
        $student_details->save();
        // email data
        // $email_data = array(
        //     'name' => $request->name,
        //     'email' => $request->email
        // );
        // send email with the template
        // Mail::send('welcome_email', $email_data, function ($message) use ($email_data) {
        //     $message->to($email_data['email'], $email_data['name'])
        //         ->subject('JEPC Rte Application')
        //         ->from('rambabuyadav1994@gmail.com', 'RTE');
        // });
        $mobile = 9792684080;;
        $senderId = "ram yadav";
        $rndno = rand(1000, 9999);
        //$rndno=$this->generateRandomString();
        $request->session()->put('otp', $rndno);
        $message = urlencode("Your otp number:" . $rndno);
        $route = "route=4";
        $authkey = "237996AsRwzrXy7U5b9f5768";
        $postData = array(
            'authkey' => $authkey,
            'mobiles' => $mobile,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));
        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        $this->testSuccess($request->contact, $request->email, $request->password);
        return redirect('login')->with('message', 'You have successfully register.');
    }
    public function getStudentDetails()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'dc' || Auth::user()->user_role == 'dse' || Auth::user()->user_role == 'student') {
                if (Auth::user()->user_role == 'dc' || Auth::user()->user_role == 'dse') {
                    $UserDivision = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.division')
                        ->get();
                    $User_Division = $UserDivision[0]->division;
                    $students_data = rte_applicant_students::where('submit_status', 1)->where('division', $User_Division)->get();
                    foreach ($students_data as $key => $value) {
                        if (!empty($value->referred_school_name) && $value->referred_school_name != null)
                            $value->referred_school_name = json_decode($value->referred_school_name, true);
                        if (!empty($value->distance) && $value->distance != null)
                            $value->distance = json_decode($value->distance, true);
                    }
                    return view('view_student_data', ['students_data' => $students_data]);
                } else {
                    $students_data = rte_applicant_students::where('user_id', Auth::user()->id)->first();
                    if (!empty($students_data->referred_school_name) && $students_data->referred_school_name != null)
                        $students_data->referred_school_name = json_decode($students_data->referred_school_name, true);
                    if (!empty($students_data->distance) && $students_data->distance != null)
                        $students_data->distance = json_decode($students_data->distance, true);
                    if ($students_data->submit_status == 1) {
                        return view('view_student_data', ['students_data' => $students_data]);
                    } else {
                        return view('student_application', ['students_data' => $students_data]);
                    }
                }
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function addEditStudentData(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'dc' || Auth::user()->user_role == 'dse' || Auth::user()->user_role == 'student') {
                $application_data = rte_applicant_students::find($request->id);
                $application_data->user_id = Auth::user()->id;
                $application_data->full_name = $request->student_name;
                // $application_data->district = Auth::user()->district;
                // $application_data->block = Auth::user()->block;
                $application_data->post_office = $request->post_office;
                $application_data->email = $request->email;
                $application_data->address = $request->address;
                $application_data->village = $request->village_town;
                $application_data->pin = $request->pin_code;
                $application_data->nearest_police_station = $request->police_station;
                $application_data->religion = $request->religion;
                $application_data->gender = $request->gender;
                $application_data->caste = $request->caste;
                $application_data->blood_group = $request->blood_group;
                $application_data->disability = $request->disability;
                // $application_data->previous_school_name = $request->pre_school_name;
                // $application_data->last_class_studied = $request->last_class_studied;
                $application_data->class_applied = $request->class_applying;
                // if ($request->has('last_class_marksheet')) {
                //     $fileInstanceMark = $request->last_class_marksheet;
                //     $newFileNameMark = "marksheet_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceMark->getClientOriginalExtension();
                //     $fileInstanceMark->move(public_path('assets/student'), $newFileNameMark);
                //     $application_data->last_class_marksheet = $newFileNameMark;
                // }
                $application_data->referred_school_name = json_encode($request->new_school_name, true);
                $application_data->referred_school_address = json_encode($request->new_school_address, true);
                $application_data->distance = json_encode($request->school_distance, true);
                $application_data->father_name = $request->father_name;
                $application_data->father_primary_phone = $request->f_mobile_no_p;
                $application_data->father_secondary_phone = $request->f_mobile_no_s;
                $application_data->father_blood_group = $request->f_blood_group;
                $application_data->father_income = $request->f_income;
                $application_data->mother_name = $request->mother_name;
                $application_data->mother_primary_phone = $request->m_mobile_no_p;
                $application_data->mother_secondary_phone = $request->m_mobile_no_s;
                $application_data->mother_blood_group = $request->m_blood_group;
                $application_data->mother_income = $request->m_income;
                if ($request->has('photo')) {
                    $fileInstance = $request->photo;
                    $newFileName = "profile_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                    $fileInstance->move(public_path('assets/student'), $newFileName);
                    $application_data->photo = $newFileName;
                }
                if ($request->has('disability_certificate')) {
                    $fileInstanceDC = $request->disability_certificate;
                    $newFileNameDC = "disability_certificate_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceDC->getClientOriginalExtension();
                    $fileInstanceDC->move(public_path('assets/student'), $newFileNameDC);
                    $application_data->disability_certificate = $newFileNameDC;
                }
                if ($request->has('birth_certificate')) {
                    $fileInstanceBc = $request->birth_certificate;
                    $newFileNameBc = "birth_certificate_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceBc->getClientOriginalExtension();
                    $fileInstanceBc->move(public_path('assets/student'), $newFileNameBc);
                    $application_data->birth_certificate = $newFileNameBc;
                }
                if ($request->has('aadhaar_card')) {
                    $fileInstanceAahaar = $request->aadhaar_card;
                    $newFileNameAadhaar = "aadhaar_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceAahaar->getClientOriginalExtension();
                    $fileInstanceAahaar->move(public_path('assets/student'), $newFileNameAadhaar);
                    $application_data->aadhaar_card = $newFileNameAadhaar;
                }
                if ($request->has('f_aadhaar_card')) {
                    $fileInstanceAahaar_f = $request->f_aadhaar_card;
                    $newFileNameAadhaar_f = "aadhaar_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceAahaar_f->getClientOriginalExtension();
                    $fileInstanceAahaar_f->move(public_path('assets/student'), $newFileNameAadhaar_f);
                    $application_data->father_aadhaar_card = $newFileNameAadhaar_f;
                }
                if ($request->has('ration_card')) {
                    $fileInstanceRation = $request->ration_card;
                    $newFileNameRation = "ration_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceRation->getClientOriginalExtension();
                    $fileInstanceRation->move(public_path('assets/student'), $newFileNameRation);
                    $application_data->ration_card = $newFileNameRation;
                }
                if ($request->has('f_income_certificate')) {
                    $fileInstanceIncome = $request->f_income_certificate;
                    $newFileNameIncome = "income_certificate_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceIncome->getClientOriginalExtension();
                    $fileInstanceIncome->move(public_path('assets/student'), $newFileNameIncome);
                    $application_data->father_income_certificate = $newFileNameIncome;
                }
                if ($request->has('m_income_certificate')) {
                    $fileInstanceIncomeM = $request->m_income_certificate;
                    $newFileNameIncomeM = "income_certificate_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceIncomeM->getClientOriginalExtension();
                    $fileInstanceIncomeM->move(public_path('assets/student'), $newFileNameIncomeM);
                    $application_data->mother_income_certificate = $newFileNameIncomeM;
                }
                if ($request->has('community_certificate')) {
                    $fileInstancecommunity = $request->community_certificate;
                    $newFileNamecommunity = "community_certificate_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstancecommunity->getClientOriginalExtension();
                    $fileInstancecommunity->move(public_path('assets/student'), $newFileNamecommunity);
                    $application_data->community_certificate = $newFileNamecommunity;
                }
                $application_data->submit_status = 1;
                $application_data->save();
                return redirect('student-application')->with('message', 'Data Has Been Inserted Successfully.');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function downloadStudentPDF($id)
    {
        // return $id;
        $students_data = rte_applicant_students::where('id', $id)->first();
        $data = ['students_data' => $students_data];
        //$pdf = PDF::loadView('pdf_view.rejected_application_PDF', $data);
        $pdf = PDF::loadView('pdf_view.student_data_PDF', $data)->setPaper('a4', 'landscape');
        return $pdf->download('Student-Data.pdf');
    }
    public function checkEmail(Request $request)
    {
        $userData = User::where('email', $request->email)->first();
        if (!empty($userData)) {
            return response()->json(['Msg' => 'Email already exist', 'ChKval' => 1]);
        } else {
            return response()->json(['ChKval' => 0]);
        }
    }
    public function testsms($phone)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        // $message = "Your Normal message here "; //message content
        // $messageUnicode = "मोबाइल सेवा में आपका स्वागत है "; //message content in unicode
        // session_start();
        // $_SESSION['phone_otp'] = $otp = rand(1111,9999);
        $otp = rand(1111, 9999);
        Session::put('phone_otp', $otp);
        $message = "Your OTP to verify your phone number is: " . $otp . ", Use this OTP to register. " . date('His'); //message content in unicode
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $mobileNos = "8797490173,8797490173"; //if bulk sms need to send use mobileNos as keyword and mobile number seperated by commas as value
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        //call method and pass value to send single sms, uncomment next line to use
        // $this->sendSingleSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
        //call method and pass value to send otp sms, uncomment next line to use
        $this->sendOtpSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
        //call this method and pass value to send bulk sms, uncomment next line to use
        //$this->sendBulkSMS($username,$encryp_password,$senderid,$message,$mobileNos,$deptSecureKey);
        //call this method for sending single unicode sms, uncomment next line to use
        //$this->sendSingleUnicode($username,$encryp_password,$senderid,$messageUnicode,$mobileno,$deptSecureKey);
        //call this method for sending single unicode otp sms, uncomment next line to use
        //$this->sendUnicodeOtpSMS($username,$encryp_password,$senderid,$messageUnicode,$mobileno,$deptSecureKey);
        //call this method to send bulk unicode sms, uncomment next line to use
        //$this->sendBulkUnicode($username,$encryp_password,$senderid,$messageUnicode,$mobileNos,$deptSecureKey);
    }
    function testSuccess($phone, $email, $password_1)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        $message = "You have successfully registred for Private School Recognition System. For login please use folowing credentials, Username: " . $email . " Password" . $password_1 . "."; //message content in unicode
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
    }
    //function to send sms using by making http connection
    function post_to_url($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($post); //result from mobile seva server
        echo $result; //output from server displayed
        curl_close($post);
    }
    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        echo $result = curl_exec($post); //result from mobile seva server
        curl_close($post);
    }
    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }
    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }
    //Function to send single sms
    function sendSingleSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "singlemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url to send sms
    }
    //Function to send otp sms
    function sendOtpSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "otpmsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url to send otp sms
    }
    //function to send bulk sms
    function sendBulkSMS($username, $encryp_password, $senderid, $message, $mobileNos, $deptSecureKey)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "bulkmsg",
            "bulkmobno" => trim($mobileNos),
            "key" => trim($key)
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url to send bulk sms
    }
    //function to send single unicode sms
    function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }
    //function to send bulk unicode sms
    function sendBulkUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileNos, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "bulkmobno" => trim($mobileNos),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send bulk unicode sms
    }
    //function to send single unicode otp sms
    function sendUnicodeOtpSMS($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodeotpmsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }
}
