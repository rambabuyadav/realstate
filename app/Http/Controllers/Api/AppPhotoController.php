<?php
namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Auth;
Use DB;


class AppPhotoController extends Controller
{
    public function image($fileName){
        // if (Auth::guard('api')->check()) {
            // if (Auth::guard('api')->user()->user_role == 'state') {
    // //     if($fileName){
    //     $path = public_path().'/applications/'.$fileName;
    //     return Response::download($path);    
        
    // }
    // //     elseif($fileName){
        if(File::exists(public_path().'/assets/applications/'.$fileName)){
            $path = public_path().'/assets/applications/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/assets/school_send_appeal/'.$fileName)){
            $path = public_path().'/assets/school_send_appeal/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/assets/send_dse_to_school__correction/'.$fileName)){
            $path = public_path().'/assets/send_dse_to_school__correction/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/assets/accept_appeal/'.$fileName)){
            $path = public_path().'/assets/accept_appeal/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/send_dse_to_school_correction/'.$fileName)){
            $path = public_path().'/send_dse_to_school_correction/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/assets/send_school_to_dc_dse_correction/'.$fileName)){
            $path = public_path().'/assets/send_school_to_dc_dse_correction/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/school_send__appeal/'.$fileName)){
            $path = public_path().'/school_send__appeal/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/application_reject/'.$fileName)){
            $path = public_path().'/application_reject/'.$fileName;
            return Response::download($path);
        }elseif(File::exists(public_path().'/assets/applications/dsc_verification/'.$fileName)){
            $path = public_path().'/assets/applications/dsc_verification/'.$fileName;
            return Response::download($path);
        }
        elseif(File::exists(public_path().'/assets/applications/dc_application/accept/'.$fileName)){
            $path = public_path().'/assets/applications/dc_application/accept/'.$fileName;
            return Response::download($path);
        }
        elseif(File::exists(public_path().'/assets/applications/dc_application/reject/'.$fileName)){
            $path = public_path().'/assets/applications/dc_application/reject/'.$fileName;
            return Response::download($path);
        }
        elseif(File::exists(public_path().'/assets/application_accept/'.$fileName)){
            $path = public_path().'/assets/application_accept/'.$fileName;
            return Response::download($path);
        }
        elseif(File::exists(public_path().'/assets/application_reject/'.$fileName)){
            $path = public_path().'/assets/application_reject/'.$fileName;
            return Response::download($path);
        }
        else
        {
            return response()->json(['warning'=>'file not found'],400);
        }
        // }elseif(File::exists(public_path().'/application_reject/'.$fileName)){
        //     $path = public_path().'/application_reject/'.$fileName;
        //     return Response::download($path);
        // }

        
        // // if(file_exists(public_path().'assets/applications/{{ Auth::User()->id }}.jpg'))
        //     $path = public_path().'/school_send_appeal/'.$fileName;
        //     return Response::download($path);  
        // }   
    // } 

    
    // // return response()->json(['message'=> 'Your session has timed out'],401);

    }

    // public function image($fileName){
    //     $path = public_path().'/applications/'.$fileName;
    //     return Response::download($path);        
    // }

}