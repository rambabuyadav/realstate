<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;

use App\Exports\PaymentExport;
use App\Models\Conversations;
use App\Models\Fnd_value;
use App\Models\User;
use App\Models\application_data;
use App\Models\application_data_ext_1;
use App\Models\application_data_ext_file;
use App\Models\application_data_file;
use App\Models\application_data_remarks;
use App\Models\application_data_remarks_ext_1;
use App\Models\application_data_verification;
use App\Models\application_data_verification_ext_1;
use App\Models\application_meetings;
use App\Models\application_statuses;
use App\Models\application_teacher_data;
use App\Models\application_tracking;
use App\Models\certificate;
use App\Models\Fnd_treasury_value;
use App\Models\Fnd_ddo_value;
use App\Models\fnd_settings;
use App\Models\Address;
use App\Models\Transaction;
use App\Models\Re_apply_application;
use App\Models\Payment;
use App\Models\rte_applicant_students;
use App\Exports\ExcelExport;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Response;

use Illuminate\Routing\UrlGenerator;
// use Illuminate\Support\Facades\Input;
use PDF;
use File;
use Input;

class AppApplicationController extends Controller
{


    function addData(Request $req)
    {

        $application_form = new application_data;
        $application_form->school_name = $req->school_name;
        $application_form->recognised_by = $req->recognised_by;
        $application_form->district = $req->district;
        $application_form->post_office = $req->post_office;
        $application_form->village_city = $req->village_town;
        $application_form->pincode = $req->pin_code;
        $application_form->phone_with_std_code = $req->phone_with_std_code;
        $application_form->fax_with_std_code = $req->fax_with_std_code;
        $application_form->email = $req->email;
        $application_form->police_station = $req->police_station;

        $application_form->estd_year = $req->estd_year;
        $application_form->opening_date = $req->opening_date;
        $application_form->society_name = $req->society_name;
        $application_form->is_society_registered = $req->is_society_registered;
        $application_form->society_registration_valid_upto = $req->society_registration_valid_upto;

        $application_form->evidence_of_non_proprietary_nature = $req->evidence_of_non_proprietary_nature;
        $application_form->school_chairman_name = $req->school_chairman_name;
        $application_form->school_chairman_post = $req->school_chairman_post;
        $application_form->school_chairman_address = $req->school_chairman_address;
        $application_form->school_chairman_phone_number = $req->school_chairman_phone_number;
        $application_form->school_chairman_office = $req->school_chairman_office;
        $application_form->school_chairman_email = $req->school_chairman_email;

        $application_form->session_year = $req->session_year;
        $application_form->income = $req->income;
        $application_form->expenses = $req->expenses;
        $application_form->surplus_money = $req->surplus_money;
        $application_form->reduced_money = $req->reduced_money;

        $application_form->medium = $req->medium;
        $application_form->type_of_school = $req->type_of_school;
        $application_form->supported_agency_name = $req->supported_agency_name;
        $application_form->agency_supported_percent = $req->agency_supported_percent;
        $application_form->authority_name = $req->authority_name;
        $application_form->recognised_number = $req->recognised_number;
        $application_form->is_school_on_rented = $req->is_school_on_rented;
        $application_form->are_school_building_used = $req->are_school_building_used;
        $application_form->school_total_area = $req->school_total_area;
        $application_form->school_building_area = $req->school_building_area;
        $application_form->pre_elementary_class = $req->pre_elementary_class;
        $application_form->pre_no_of_section = $req->pre_no_of_section;
        $application_form->pre_no_of_student = $req->pre_no_of_student;
        $application_form->class_one_to_five = $req->class_one_to_five;
        $application_form->onefive_no_of_section = $req->onefive_no_of_section;
        $application_form->onefive_no_of_student = $req->onefive_no_of_student;
        $application_form->class_six_to_eight = $req->class_six_to_eight;
        $application_form->sixeight_no_of_section = $req->sixeight_no_of_section;
        $application_form->sixeight_no_of_student = $req->sixeight_no_of_student;
        $application_form->no_of_class = $req->no_of_class;
        $application_form->no_of_office_room = $req->no_of_office_room;
        $application_form->avg_size_of_office_room = $req->avg_size_of_office_room;
        $application_form->no_of_store_room = $req->no_of_store_room;
        $application_form->avg_size_of_store_room = $req->avg_size_of_store_room;
        $application_form->no_of_princpal_room = $req->no_of_princpal_room;
        $application_form->avg_size_of_principal_room = $req->avg_size_of_principal_room;
        $application_form->no_of_kitchen_room = $req->no_of_kitchen_room;
        $application_form->avg_size_of_kitchen_room = $req->avg_size_of_kitchen_room;
        // $application_form->medium = $req->medium;
        $application_form->save();

        $applicationForm_Id = application_data::max('id');
        $hiddenTeacherId = $req->hiddenTeacherId;
        for ($row = 1; $row <= $hiddenTeacherId; $row++) {


            return  $req->teacher_name . ($row);

            $application_teacher_data = new application_teacher_data;
            $application_teacher_data->application_data_id = $applicationForm_Id;
            $application_teacher_data->teacher_name = $req->teacher_name . $row;
            $application_teacher_data->teacher_f_h_w_name = $req->teacher_f_h_w_name . $row;
            $application_teacher_data->date_of_birth = $req->teacher_date_of_birth . $row;
            $application_teacher_data->education_qualification = $req->teacher_education_qualification . $row;
            $application_teacher_data->trainee_qualification = $req->teacher_trainee_qualification . $row;
            $application_teacher_data->teaching_experience = $req->teacher_teaching_experience . $row;
            $application_teacher_data->cls_handed_over = $req->teacher_class_handed_over . $row;
            $application_teacher_data->date_of_appointment = $req->teacher_appointment_date . $row;
            $application_teacher_data->trained_or_untrained = $req->teacher_trained_or_untrained . $row;
            $application_teacher_data->save();
        }

        $applicationFormId = application_data::max('id');

        $application_form_ext = new application_data_ext_1;
        $application_form_ext->application_form_id = $applicationFormId;
        $application_form_ext->facilities_access_without_interrupted = $req->facilities_access_without_interrupted;
        $application_form_ext->all_teaching_material_list = $req->all_teaching_material_list;
        $application_form_ext->all_sports_equipment_list = $req->all_sports_equipment_list;
        $application_form_ext->books = $req->books;
        $application_form_ext->magazines = $req->magazines;
        $application_form_ext->type_of_water_facilities = $req->type_of_water_facilities;
        $application_form_ext->no_of_water_supply = $req->no_of_water_supply;
        $application_form_ext->type_of_toilet = $req->type_of_toilet;
        $application_form_ext->gents_toilet = $req->gents_toilet;
        $application_form_ext->ladies_toilet = $req->ladies_toilet;
        $application_form_ext->principle_name = $req->principle_name;
        $application_form_ext->p_f_h_w_name = $req->p_f_h_w_name;
        $application_form_ext->p_date_of_birth = $req->p_date_of_birth;
        $application_form_ext->p_education_qualification = $req->p_education_qualification;
        $application_form_ext->trainee_qualification = $req->trainee_qualification;
        $application_form_ext->teaching_experience = $req->teaching_experience;
        $application_form_ext->class_handed_over = $req->class_handed_over;
        $application_form_ext->date_of_appointment = $req->date_of_appointment;
        $application_form_ext->trained_untrained = $req->trained_untrained;
        $application_form_ext->details_of_curriculum = $req->details_of_curriculum;
        $application_form_ext->method_of_inspection = $req->method_of_inspection;
        $application_form_ext->school_board_exam_till_cls_eight = $req->school_board_exam_till_cls_eight;
        // $application_form_ext->save();



        return response()->json(['status' => True, 'message' => "Data Has Been Inserted Successfully"]);
    }

    public function ApplicationList(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'dc' || Auth::guard('api')->user()->user_role == 'dse') {

                $UserDivision = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::guard('api')->user()->id)
                    ->value('addresses.division');
                // return($UserDivision);

                $UserDistrict = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::guard('api')->user()->id)
                    ->value('addresses.district');
                // return($UserDistrict);
                $data['block_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
                    ->pluck('fnd_values.display_value as block_name', 'fnd_values.id as block_id');
                // return( $this->data['block_list']);

                $search_text = $request->search_text;
                $application_status = $request->application_status;
                $block = $request->Block;
                $fromDate = $request->fromDate;
                $toDate = $request->toDate;
                $blockName = null;
                $applicationStatusName = null;

                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                if (isset($request->application_status) && $request->application_status != null) {
                    $applicationStatusName = application_statuses::where('application_statuses.id',  $application_status)->value('status_name');
                }
                $data['adv_data'] = ['search_text' => $search_text, 'application_status' => $application_status, 'applicationStatus' => $applicationStatusName, 'block_id' => $block, 'blockName' => $blockName, 'fromDate' => $fromDate, 'toDate' => $toDate];

                $data['AllData'] = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
                    ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
                    ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->leftjoin('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                    ->where('application_data.district', $UserDistrict)
                    ->where('application_data.status', '1')
                    ->select(
                        'application_data.id as applicationdataid',
                        'application_data.school_id',
                        'application_data.school_name',
                        'application_data.application_status',
                        'application_data.school_id',
                        'application_data.from_class',
                        'application_data.upto_class',
                        'fnd_division.display_value as Division',
                        'fnd_district.display_value as disrict',
                        'application_data.district as district_id',
                        'fnd_block.display_value as Block',
                        'application_data.police_station',
                        'application_data.panchayat',
                        'application_data.post_office',
                        'application_data.village_city',
                        'application_data.pincode',
                        'application_data.phone_with_std_code',
                        'application_data.email',
                        'application_data.society_name',
                        // 'application_data.opening_date',
                        // 'application_data.society_registration_valid_upto',
                        'application_data.school_chairman_name',
                        'application_data.school_chairman_office',
                        'application_data.school_chairman_email',
                        'application_data.school_chairman_phone_number',
                        'application_data.medium',
                        'application_data.type_of_school',
                        'application_data.created_at',
                        'application_data_ext_1.books',
                        'status.status_name AS status_name'

                    );
                // return ['amar'];


                // return($request);
                if (Auth::guard('api')->user()->user_role == 'dse') {
                    $data['AllData'] = $data['AllData']->where('application_data.application_status', '>', 1);
                } else if (Auth::guard('api')->user()->user_role == 'dc') {
                    $data['AllData'] = $data['AllData']->where('application_data.application_status', '>', 1);
                }
                // return ['amar'];
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllData'] = $data['AllData']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.medium', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.id', 'like', '%' .  $search_text . '%');
                    });
                }
                if ($request->application_status == 'verify') {
                    // return $request;
                    $data['AllData'] = $data['AllData']->where('application_data.district', $UserDistrict)->where('application_data.application_status', '!=', 1);
                    application_data::where('id', $request->applicationdataid)->update([
                        'application_status' => 2,
                        'updated_by' => Auth::guard('api')->user()->id
                    ]);
                }
                // return($request->application_status);
                if (isset($request->application_status) && $request->application_status != null && $request->application_status > 0) {
                    if ($request->application_status == 2) {

                        $data['AllData'] = $data['AllData']->where('application_data.district', $UserDistrict)->whereIn('application_data.application_status', [2, 3]);
                    } else {

                        $data['AllData'] = $data['AllData']->where('application_data.district', $UserDistrict)->where('application_data.application_status', $application_status);
                    }
                } elseif (isset($request->application_status) && $request->application_status != null && $request->application_status == 0) {
                    $data['AllData'] = $data['AllData']->where('application_data.district', $UserDistrict)->where('application_data.application_status', '!=', 1);
                } else {
                    if (Auth::guard('api')->user()->user_role == 'dse') {
                        $data['AllData'] = $data['AllData']->where('application_data.district', $UserDistrict)->whereIn('application_data.application_status', [2, 3, 4, 5, 6, 8]);
                    } else if (Auth::guard('api')->user()->user_role == 'dc') {
                        $data['AllData'] = $data['AllData']->where('application_data.district', $UserDistrict)->whereIn('application_data.application_status', [2, 3, 4, 5, 6, 7, 8]);
                    } else if (Auth::guard('api')->user()->user_role == 'faa') {
                        $data['AllData'] = $data['AllData']->whereIn('application_data.division', $UserDivision)->whereIn('application_data.application_status', [4, 5, 6]);
                    }
                }
                //return ['amar'];

                if (isset($request->Block) && $request->Block != null) {
                    $data['AllData'] = $data['AllData']->where('application_data.block',  $request->Block);
                }
                if ($request->fromDate != null && $request->toDate != null) {

                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $fromDate)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $toDate)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return response()->json(['warning' => "From date always lesser than To date"], 400);
                    }
                }
                if ($request->excel == 'excel') {
                    $data['AllData'] = $data['AllData']->get();
                } else {
                    $data['AllData'] = $data['AllData']->get();
                    //     echo '<pre>';
                    //   print_r($this->data['AllData']); exit;
                }

                //   return $this->data['AllData'];
                // return($this->data['AllData'] );

                foreach ($data['AllData'] as $key => $value) {
                    // return $value->applicationdataid;

                    $Message = [];

                    $Message = Conversations::Join('users as sender', 'conversations.sender_id', 'sender.id')
                        ->Join('users as reciever', 'conversations.reciever_id', 'reciever.id')
                        ->select(
                            'conversations.*',
                            'reciever.user_role as reciever_role',
                            'sender.user_role as sender_role',
                            'reciever.name as Reciever_name',
                            // 'conversations.attachments as conversations_attachments'
                        )
                        ->Where('conversations.parent_id', $value->applicationdataid)
                        ->where('conversations.conversation_type', 4)
                        ->get();

                    // if($Message->conversations_attachments== ''){
                    //     $Message->conversations_attachments = null;
                    // }

                    // foreach ($Message as  $Conversation) {
                    //     if($Conversation->attachments ==""||$Conversation->attachments ==''){
                    //         $Conversation->attachments = null;
                    //     }
                    // }

                    $Message->images = [];

                    foreach ($Message as  $Conversation) {
                        $Conversation->attachments = json_decode($Conversation->attachments);
                    }
                    // return $Message;
                    if (Auth::guard('api')->user()->user_role == 'dse') {
                        $value->application_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->applicationdataid)->where('conversations.conversation_type', 4)->where('dse_read_status', 0)->count();
                    } elseif (Auth::guard('api')->user()->user_role == 'dc') {
                        $value->application_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->applicationdataid)->where('conversations.conversation_type', 4)->where('dc_read_status', 0)->count();
                    }
                    // array_push($appeal_conversations,$Message);
                    $value->message_recode = $Message;
                }

                $now = Carbon::now();
                $date = 0;
                $data['last_update'] = "";
                $RemainingDate = [];
                $days_to_varified_by_dse = fnd_settings::value('verification_by_dse_limit');
                $days_to_varified_by_dc = fnd_settings::value('schedule_meeting_by_dc_limit');
                $days_to_varified_by_dc_after_meeting = fnd_settings::value('take_decision_after_meeting_held_by_dc');
                $faa_decision_limit = fnd_settings::value('faa_decision_limit');
                $saa_decision_limit = fnd_settings::value('saa_decision_limit');
                $status_offset = 0;
                foreach ($data['AllData'] as $row) {
                    $id = application_tracking::where('application_id', $row->applicationdataid)->where('school_id', $row->school_id)->max('id');
                    $status_tracking =  application_tracking::where('id', $id)->first();
                    if ($status_tracking) {

                        $startDate = $row->created_at;
                        $data['last_update'] = $startDate;
                        $cDate = Carbon::parse($startDate);
                        $count = $now->diffInDays($cDate);

                        if ($data['AllData'][$status_offset]['application_status'] == 2 || $data['AllData'][$status_offset]['application_status'] == 3) {
                            $date = $days_to_varified_by_dse - (int)$count;
                        } elseif ($data['AllData'][$status_offset]['application_status'] == 4) {
                            $date = $days_to_varified_by_dc - (int)$count;
                        } elseif ($data['AllData'][$status_offset]['application_status'] == 5) {
                            $meeting_date = application_meetings::where('application_id', $row->applicationdataid)->value('meeting_date');
                            $data['last_update'] = $meeting_date;
                            $intDate = Carbon::parse($meeting_date);
                            $count = $now->diffInDays($intDate);

                            $date = $days_to_varified_by_dc_after_meeting - (int)$count;
                        } elseif ($data['AllData'][$status_offset]['application_status'] == 9) {
                            $date = $faa_decision_limit - (int)$count;
                        } elseif ($data['AllData'][$status_offset]['application_status'] == 12) {
                            $date = $saa_decision_limit - (int)$count;
                        }






                        // return $data['last_update'];

                        //if (Auth::guard('api')->user()->user_role == 'dse') {
                        //$date = $days_to_varified_by_dse - (int)$count;
                        $data['days_to_varified_by_dse'] = $days_to_varified_by_dse;
                        //}
                        //if (Auth::guard('api')->user()->user_role == 'dc') {
                        // seen accouding to status
                        //if ($data['AllData'][$status_offset]['application_status'] == 4) {
                        //$date = $days_to_varified_by_dc - (int)$count;
                        $data['days_to_varified_by_dc'] = $days_to_varified_by_dc;
                        //} else {
                        //$date = $days_to_varified_by_dc_after_meeting - (int)$count;
                        $data['days_to_varified_by_dc_after_meeting'] = $days_to_varified_by_dc_after_meeting;
                        $data['faa_decision_limit'] = $faa_decision_limit;
                        $data['saa_decision_limit'] = $saa_decision_limit;
                        //}
                        //}

                    }
                    array_push($RemainingDate, $date);
                    $status_offset++;
                }

                $data['Count'] = $RemainingDate;

                // $data['All_Data'] = ['AllData' => $Alldata, 'Count' => $RemainingDate];
                if ($request->excel == 'excel') {
                    if (count($data['AllData']) == 0) {
                        return response()->json(['warning' => "There are no such data to export!!"], 400);
                    }
                    $params = ['data' => $data['AllData'], 'view' => 'excel_view.application_excel', 'total' => $data['Count']];
                    $filename = 'application-list.xlsx';
                    return response()->Excel::download(new PaymentExport($params), $filename);
                } else {
                    // return $this->data;
                    return response()->json(['application_verify' => $data]);
                }
            } else {
                return response()->json(['message' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'You are not Authorised!!'], 203);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }



    public function showAllData($req)
    {
        if (Auth::guard('api')->check()) {
            // return $req;            
            $id = $req;
            // $id = Crypt::decrypt($req);
            // if (Auth::guard('api')->check()->id == 'saa') {
            $users = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
                ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
                ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_data_id')
                ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.application_data_id')
                ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
                ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.application_data_id')
                ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
                // ->leftjoin('application_teacher_data', 'application_data.id', '=', 'application_teacher_data.application_data_id')
                ->leftjoin('fnd_values', 'application_data.district', '=', 'fnd_values.id')
                ->where('application_data.id', $id)
                ->where('application_data.status', 1)
                ->select(
                    'application_data.id as application_data_Id',
                    'application_data.*',
                    'fnd_values.display_value as District',
                    'application_data.school_id as schoolId',
                    'application_data_ext_1.id as application_data_ext_1_id',
                    'application_data_ext_1.trainee_qualification as pri_trainee_qualification',
                    'application_data_ext_1.teaching_experience as pri_teaching_experience',
                    'application_data_ext_1.date_of_appointment as pri_date_of_appointment',
                    'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                    'application_data_ext_1.class_names as class_names_ex',
                    'application_data_ext_1.no_of_sections as no_of_sections_ex',
                    'application_data_ext_1.no_of_students as no_of_students_ex',
                    'application_data_ext_1.details_of_curriculum as details_of_curriculum_ex',
                    'application_data_ext_1.method_of_inspection as method_of_inspection_ex',
                    'application_data_ext_1.one_lac_fd_proof as one_lac_fd_proof',
                    'application_data_ext_1.all_teaching_material_list_files as all_teaching_material_list_files_ex',


                    'application_data.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature1',
                    'application_data_ext_1.*',

                    // 'application_teacher_data.id as application_teacher_data_id',
                    // 'application_teacher_data.teacher_name as teacher_name',
                    // 'application_teacher_data.trainee_qualification as teacher_trainee_qualification',
                    // 'application_teacher_data.teaching_experience as teacher_teaching_experience',
                    // 'application_teacher_data.date_of_appointment as teacher_date_of_appointment',
                    // 'application_teacher_data.teacher_f_h_w_name as teacher_f_h_w_name',
                    // 'application_teacher_data.date_of_birth as teacher_date_of_birth',
                    // 'application_teacher_data.education_qualification as teacher_education_qualification',
                    // 'application_teacher_data.cls_handed_over as teacher_cls_handed_over',
                    // 'application_teacher_data.trained_or_untrained as teacher_trained_or_untrained',



                    'application_data_ext_files.id as application_data_ext_files_id',
                    'application_data_ext_files.facilities_access_without_interrupted_files as facilities_access_without_interrupted_files',
                    // 'application_data_ext_files.lightning_driver_status_files as lightning_driver_status_files',    
                    // 'application_data_ext_files.all_teaching_material_list_files as all_teaching_material_list_files',
                    'application_data_ext_files.all_sports_equipment_list_files as all_sports_equipment_list_files',
                    'application_data_ext_files.books_files as books_files',
                    'application_data_ext_files.magazines_files as magazines_files',
                    'application_data_ext_files.type_of_water_facilities_files as type_of_water_facilities_files',
                    'application_data_ext_files.no_of_water_supply_files as no_of_water_supply_files',
                    'application_data_ext_files.fire_fighting_files as fire_fighting_files',
                    'application_data_ext_files.lightning_driver_status_files as lightning_driver_status_files',
                    'application_data_ext_files.wc_and_urinal_variety_files as wc_and_urinal_variety_files',

                    'application_data_ext_files.type_of_toilet_files as type_of_toilet_files',
                    'application_data_ext_files.gents_toilet_files as gents_toilet_files',
                    'application_data_ext_files.teacher_name_files as teacher_name_files',
                    'application_data_ext_files.post_name_files as post_name_files',
                    'application_data_ext_files.father_husband_wife_name_files as father_husband_wife_name_files',
                    'application_data_ext_files.dob_2_files as dob_2_files',
                    'application_data_ext_files.educ_qualification_files as educ_qualification_files',
                    'application_data_ext_files.administrative_qualification_files as administrative_qualification_files',
                    'application_data_ext_files.tet_files as tet_files',
                    'application_data_ext_files.experience_files as experience_files',
                    'application_data_ext_files.class_assigned_files as class_assigned_files',
                    'application_data_ext_files.appointment_date_files as appointment_date_files',
                    'application_data_ext_files.syllabus_files as syllabus_files',
                    'application_data_ext_files.inspection_mode_files as inspection_mode_files',
                    'application_data_ext_files.board_in_6_or_before_files as board_in_6_or_before_files',

                    'application_data_ext_files.principle_name_files as principle_name_files',
                    'application_data_ext_files.p_f_h_w_name_files as p_f_h_w_name_files',
                    'application_data_ext_files.p_date_of_birth_files as p_date_of_birth_files',
                    'application_data_ext_files.p_education_qualification_files as p_education_qualification_files',
                    'application_data_ext_files.trainee_qualification_files as trainee_qualification_files',
                    'application_data_ext_files.teaching_experience_files as teaching_experience_files',
                    'application_data_ext_files.class_handed_over_files as class_handed_over_files',
                    'application_data_ext_files.date_of_appointment_files as date_of_appointment_files',
                    'application_data_ext_files.trained_untrained_files as trained_untrained_files',
                    'application_data_ext_files.details_of_curriculum_files as details_of_curriculum_files',
                    'application_data_ext_files.method_of_inspection_files as method_of_inspection_files',
                    'application_data_ext_files.school_board_exam_till_cls_eight_files as school_board_exam_till_cls_eight_files',
                    'application_data_ext_files.evidence_of_non_proprietary_nature_files as evidence_of_non_proprietary_nature_files',
                    'application_data_ext_files.last_three_year_tot_income_files as last_three_year_tot_income_files',
                    'application_data_ext_files.water_facilities_files as water_facilities_files',
                    'application_data_ext_files.cleanliness_files as cleanliness_files',
                    'application_data_ext_files.books_in_library_files as books_in_library_files',
                    'application_data_ext_files.one_lac_fd_proof_files as one_lac_fd_proof_files',
                    'application_data_ext_files.financial_details_files as financial_details_files',
                    'application_data_ext_files.school_rent_deatail_files as school_rent_deatail_files',
                    'application_data_ext_files.school_area_deatail_files as school_area_deatail_files',
                    'application_data_ext_files.is_school_has_boundary_files as is_school_has_boundary_files',
                    'application_data_ext_files.school_boundary_files_files as school_boundary_files_files',
                    'application_data_ext_files.is_school_cctv_files as is_school_cctv_files',
                    'application_data_ext_files.school_cctv_file_files as school_cctv_file_files',
                    'application_data_ext_files.is_school_ground_files as is_school_ground_files',
                    'application_data_ext_files.school_ground_file_files as school_ground_file_files',
                    'application_data_ext_files.is_school_water_hygene_facilities_files as is_school_water_hygene_facilities_files',
                    'application_data_ext_files.school_water_hygene_facilities_files_files as school_water_hygene_facilities_files_files',
                    'application_data_ext_files.all_teaching_material_list_files_files as all_teaching_material_list_files_files',
                    'application_data_ext_files.all_sports_equipment_list_file_files as all_sports_equipment_list_file_files',
                    'application_data_ext_files.is_school_fire_safty_facilities_files as is_school_fire_safty_facilities_files',
                    'application_data_ext_files.school_fire_safty_facilities_files_files as school_fire_safty_facilities_files_files',
                    'application_data_ext_files.educational_qualification_files_files as educational_qualification_files_files',
                    'application_data_ext_files.size_of_school_boundary_files as size_of_school_boundary_files',
                    'application_data_ext_files.no_of_school_cctv_files as no_of_school_cctv_files',
                    'application_data_ext_files.size_of_school_ground_files as size_of_school_ground_files',
                    'application_data_ext_files.no_of_school_water_hygene_facilities_files as no_of_school_water_hygene_facilities_files',
                    'application_data_ext_files.no_of_fire_safty_files as no_of_fire_safty_files',
                    'application_data_ext_files.i_e_s_r_l_files as i_e_s_r_l_files',
                    'application_data_ext_files.i_e_s_r_l_year2_files as i_e_s_r_l_year2_files',
                    'application_data_ext_files.i_e_s_r_l_year3_files as i_e_s_r_l_year3_files',


                    'application_data_files.id as application_data_files_id',
                    'application_data_files.school_name_files as school_name_files',
                    'application_data_files.udise_code_files as udise_code_files',
                    'application_data_files.from_class_files as from_class_files',
                    'application_data_files.upto_class_files as upto_class_files',
                    'application_data_files.recognised_by_files as recognised_by_files',
                    'application_data_files.district_files as district_files',
                    'application_data_files.post_office_files as post_office_files',
                    'application_data_files.village_city_files as village_city_files',
                    'application_data_files.pincode_files as pincode_files',
                    'application_data_files.phone_with_std_code_files as phone_with_std_code_files',
                    'application_data_files.fax_with_std_code_files as fax_with_std_code_files',
                    'application_data_files.email_files as email_files',
                    'application_data_files.police_station_files as police_station_files',
                    'application_data_files.challan_number_1_5_files as challan_number_1_5_files',
                    'application_data_files.challan_number_1_8_files as challan_number_1_8_files',

                    'application_data_files.estd_year_files as estd_year_files',
                    'application_data_files.opening_date_files as opening_date_files',
                    'application_data_files.society_name_files as society_name_files',
                    'application_data_files.is_society_registered_files as is_society_registered_files',
                    'application_data_files.society_registration_valid_upto_files as society_registration_valid_upto_files',
                    'application_data_files.evidence_of_non_proprietary_nature_files as evidence_of_non_proprietary_nature_files',
                    'application_data_files.school_chairman_name_files as school_chairman_name_files',
                    'application_data_files.are_school_building_used_files as are_school_building_used_files',
                    'application_data_files.school_total_area_files as school_total_area_files',

                    'application_data_files.session_year_files as session_year_files',
                    'application_data_files.income_files as income_files',
                    'application_data_files.expenses_files as expenses_files',
                    'application_data_files.surplus_money_files as surplus_money_files',
                    'application_data_files.reduced_money_files as reduced_money_files',
                    'application_data_files.class_files as class_files',
                    'application_data_files.teaching_fee_files as teaching_fee_files',
                    'application_data_files.registration_fee_files as registration_fee_files',
                    'application_data_files.enrollment_fee_files as enrollment_fee_files',
                    'application_data_files.security_money_files as security_money_files',
                    'application_data_files.annual_charge_files as annual_charge_files',
                    'application_data_files.development_fee_files as development_fee_files',
                    'application_data_files.other_charges_files as other_charges_files',
                    'application_data_files.total_files as total_files',


                    'application_data_files.medium_files as medium_files',
                    'application_data_files.type_of_school_files as type_of_school_files',
                    'application_data_files.supported_agency_name_files as supported_agency_name_files',
                    'application_data_files.agency_supported_percent_files as agency_supported_percent_files',
                    'application_data_files.is_school_recognised_files as is_school_recognised_files',
                    'application_data_files.authority_name_files as authority_name_files',
                    'application_data_files.recognised_number_files as recognised_number_files',
                    'application_data_files.is_school_on_rented_files as is_school_on_rented_files',
                    'application_data_files.khata_number_files as khata_number_files',
                    'application_data_files.plot_number_files as plot_number_files',
                    'application_data_files.school_building_area_files as school_building_area_files',
                    'application_data_files.pre_elementary_class_files as pre_elementary_class_files',
                    'application_data_files.no_of_class_files as no_of_class_files',
                    'application_data_files.avg_size_cls_room_files as avg_size_cls_room_files',
                    'application_data_files.no_of_office_room_files as no_of_office_room_files',
                    'application_data_files.avg_size_of_office_room_files as avg_size_of_office_room_files',
                    'application_data_files.no_of_store_room_files as no_of_store_room_files',
                    'application_data_files.avg_size_of_store_room_files as avg_size_of_store_room_files',
                    'application_data_files.no_of_princpal_room_files as no_of_princpal_room_files',
                    'application_data_files.avg_size_of_principal_room_files as avg_size_of_principal_room_files',
                    'application_data_files.no_of_kitchen_room_files as no_of_kitchen_room_files',
                    'application_data_files.avg_size_of_kitchen_room_files as avg_size_of_kitchen_room_files',



                    'application_data_remarks.id as application_data_remarks_id',
                    'application_data_remarks.*',

                    'application_data_remarks_ext_1s.id as application_data_remarks_ext_1s_id',
                    'application_data_remarks_ext_1s.*',

                    'application_data_verifications.id as application_data_verifications_id',
                    'application_data_verifications.*',

                    'application_data_verification_ext_1s.id as application_data_verification_ext_1s_id',
                    'application_data_verification_ext_1s.*'

                )
                ->first();

            $application_teacher_data = application_teacher_data::where('application_data.id', $id)
                ->leftjoin('application_data',  'application_teacher_data.application_data_id', 'application_data.id')
                // ->where('application_teacher_data.application_data_id', 'application_data.id')
                ->select('application_teacher_data.*')
                ->get();

            foreach ($application_teacher_data as $key) {
                // echo $value;
                if ($key->teacher_educational_qualification_files != null)
                    $key->teacher_educational_qualification_files = json_decode($key->teacher_educational_qualification_files, true);
            }
            // return true;
            // return $application_teacher_data;
            $users->application_teacher_data = $application_teacher_data;
            // echo "<pre>"; print_r($userData);exit;
            // return $users;
            // echo "<pre>"; print_r($users);exit;

            // $users = json_decode($users, true);
            //     foreach( $users as $key => $value){
            //         // if ($users!= null)
            //         // $users = json_decode($users, true);
            //         echo "$value <br>";
            //     }
            if ($users->one_lac_fd_proof != null)
                $users->one_lac_fd_proof = json_decode($users->one_lac_fd_proof, true);

            if ($users->session_year != null)
                $users->session_year = json_decode($users->session_year, true);

            if ($users->income != null)
                $users->income = json_decode($users->income, true);

            if ($users->expenses != null)
                $users->expenses = json_decode($users->expenses, true);

            if ($users->surplus_money != null)
                $users->surplus_money = json_decode($users->surplus_money, true);

            if ($users->reduced_money != null)
                $users->reduced_money = json_decode($users->reduced_money, true);

            if ($users->class_finance != null)
            $users->class_finance = json_decode($users->class_finance, true);
            if ($users->teaching_fee != null)
                $users->teaching_fee = json_decode($users->teaching_fee, true);
            if ($users->registration_fee != null)
                $users->registration_fee = json_decode($users->registration_fee, true);
            if ($users->enrollment_fee != null)
                $users->enrollment_fee = json_decode($users->enrollment_fee, true);
            if ($users->security_money != null)
                $users->security_money = json_decode($users->security_money, true);
            if ($users->annual_charge != null)
                $users->annual_charge = json_decode($users->annual_charge, true);
            if ($users->development_fee != null)
                $users->development_fee = json_decode($users->development_fee, true);
            if ($users->other_charges != null)
                $users->other_charges = json_decode($users->other_charges, true);
            if ($users->total != null)
                $users->total = json_decode($users->total, true);

            if ($users->facilities_access_without_interrupted_files != null)
                $users->facilities_access_without_interrupted_files = json_decode($users->facilities_access_without_interrupted_files, true);
            // if ($users->all_teaching_material_list_files != null)
            //     $users->all_teaching_material_list_files = json_decode($users->all_teaching_material_list_files, true);
            if ($users->all_sports_equipment_list_files != null)
                $users->all_sports_equipment_list_files = json_decode($users->all_sports_equipment_list_files, true);
            if ($users->books_files != null)
                $users->books_files = json_decode($users->books_files, true);
            if ($users->magazines_files != null)
                $users->magazines_files = json_decode($users->magazines_files, true);
            if ($users->type_of_water_facilities_files != null)
                $users->type_of_water_facilities_files = json_decode($users->type_of_water_facilities_files, true);
            if ($users->no_of_water_supply_files != null)
                $users->no_of_water_supply_files = json_decode($users->no_of_water_supply_files, true);
            if ($users->type_of_toilet_files != null)
                $users->type_of_toilet_files = json_decode($users->type_of_toilet_files, true);
            if ($users->gents_toilet_files != null)
                $users->gents_toilet_files = json_decode($users->gents_toilet_files, true);
            if ($users->ladies_toilet_files != null)
                $users->ladies_toilet_files = json_decode($users->ladies_toilet_files, true);
            if ($users->teacher_name_files != null)
                $users->teacher_name_files = json_decode($users->teacher_name_files, true);
            if ($users->post_name_files != null)
                $users->post_name_files = json_decode($users->post_name_files, true);
            if ($users->father_husband_wife_name_files != null)
                $users->father_husband_wife_name_files = json_decode($users->father_husband_wife_name_files, true);
            if ($users->educ_qualification_files != null)
                $users->educ_qualification_files = json_decode($users->educ_qualification_files, true);
            if ($users->administrative_qualification_files != null)
                $users->administrative_qualification_files = json_decode($users->administrative_qualification_files, true);
            if ($users->tet_files != null)
                $users->tet_files = json_decode($users->tet_files, true);
            if ($users->experience_files != null)
                $users->experience_files = json_decode($users->experience_files, true);
            if ($users->class_assigned_files != null)
                $users->class_assigned_files = json_decode($users->class_assigned_files, true);
            if ($users->appointment_date_files != null)
                $users->appointment_date_files = json_decode($users->appointment_date_files, true);
            if ($users->syllabus_files != null)
                $users->syllabus_files = json_decode($users->syllabus_files, true);
            if ($users->inspection_mode_files != null)
                $users->inspection_mode_files = json_decode($users->inspection_mode_files, true);
            if ($users->board_in_6_or_before_files != null)
                $users->board_in_6_or_before_files = json_decode($users->board_in_6_or_before_files, true);

            if ($users->principle_name_files != null)
                $users->principle_name_files = json_decode($users->principle_name_files, true);
            if ($users->p_f_h_w_name_files != null)
                $users->p_f_h_w_name_files = json_decode($users->p_f_h_w_name_files, true);
            if ($users->p_date_of_birth_files != null)
                $users->p_date_of_birth_files = json_decode($users->p_date_of_birth_files, true);
            if ($users->p_education_qualification_files != null)
                $users->p_education_qualification_files = json_decode($users->p_education_qualification_files, true);
            if ($users->trainee_qualification_files != null)
                $users->trainee_qualification_files = json_decode($users->trainee_qualification_files, true);
            if ($users->teaching_experience_files != null)
                $users->teaching_experience_files = json_decode($users->teaching_experience_files, true);
            if ($users->class_handed_over_files != null)
                $users->class_handed_over_files = json_decode($users->class_handed_over_files, true);
            if ($users->date_of_appointment_files != null)
                $users->date_of_appointment_files = json_decode($users->date_of_appointment_files, true);
            if ($users->trained_untrained_files != null)
                $users->trained_untrained_files = json_decode($users->trained_untrained_files, true);
            if ($users->details_of_curriculum_files != null)
                $users->details_of_curriculum_files = json_decode($users->details_of_curriculum_files, true);
            if ($users->method_of_inspection_files != null)
                $users->method_of_inspection_files = json_decode($users->method_of_inspection_files, true);
            if ($users->school_board_exam_till_cls_eight_files != null)
                $users->school_board_exam_till_cls_eight_files = json_decode($users->school_board_exam_till_cls_eight_files, true);
            if ($users->evidence_of_non_proprietary_nature_files != null)
                $users->evidence_of_non_proprietary_nature_files = json_decode($users->evidence_of_non_proprietary_nature_files, true);
            if ($users->last_three_year_tot_income_files != null)
                $users->last_three_year_tot_income_files = json_decode($users->last_three_year_tot_income_files, true);
            if ($users->water_facilities_files != null)
                $users->water_facilities_files = json_decode($users->water_facilities_files, true);
            if ($users->cleanliness_files != null)
                $users->cleanliness_files = json_decode($users->cleanliness_files, true);
            if ($users->books_in_library_files != null)
                $users->books_in_library_files = json_decode($users->books_in_library_files, true);
            if ($users->one_lac_fd_proof_files != null)
                $users->one_lac_fd_proof_files = json_decode($users->one_lac_fd_proof_files, true);
            if ($users->financial_details_files != null)
                $users->financial_details_files = json_decode($users->financial_details_files, true);
            if ($users->school_rent_deatail_files != null)
                $users->school_rent_deatail_files = json_decode($users->school_rent_deatail_files, true);
            if ($users->school_area_deatail_files != null)
                $users->school_area_deatail_files = json_decode($users->school_area_deatail_files, true);
            if ($users->is_school_has_boundary_files != null)
                $users->is_school_has_boundary_files = json_decode($users->is_school_has_boundary_files, true);
            if ($users->school_boundary_files_files != null)
                $users->school_boundary_files_files = json_decode($users->school_boundary_files_files, true);
            if ($users->is_school_cctv_files != null)
                $users->is_school_cctv_files = json_decode($users->is_school_cctv_files, true);
            if ($users->school_cctv_file_files != null)
                $users->school_cctv_file_files = json_decode($users->school_cctv_file_files, true);
            if ($users->is_school_ground_files != null)
                $users->is_school_ground_files = json_decode($users->is_school_ground_files, true);
            if ($users->school_ground_file_files != null)
                $users->school_ground_file_files = json_decode($users->school_ground_file_files, true);
            if ($users->is_school_water_hygene_facilities_files != null)
                $users->is_school_water_hygene_facilities_files = json_decode($users->is_school_water_hygene_facilities_files, true);
            if ($users->school_water_hygene_facilities_files_files != null)
                $users->school_water_hygene_facilities_files_files = json_decode($users->school_water_hygene_facilities_files_files, true);
            if ($users->all_teaching_material_list_files_files != null)
                $users->all_teaching_material_list_files_files = json_decode($users->all_teaching_material_list_files_files, true);
            if ($users->all_sports_equipment_list_file_files != null)
                $users->all_sports_equipment_list_file_files = json_decode($users->all_sports_equipment_list_file_files, true);
            if ($users->is_school_fire_safty_facilities_files != null)
                $users->is_school_fire_safty_facilities_files = json_decode($users->is_school_fire_safty_facilities_files, true);
            if ($users->school_fire_safty_facilities_files_files != null)
                $users->school_fire_safty_facilities_files_files = json_decode($users->school_fire_safty_facilities_files_files, true);
            if ($users->educational_qualification_files_files != null)
                $users->educational_qualification_files_files = json_decode($users->educational_qualification_files_files, true);
            if ($users->size_of_school_boundary_files != null)
                $users->size_of_school_boundary_files = json_decode($users->size_of_school_boundary_files, true);
            if ($users->no_of_school_cctv_files != null)
                $users->no_of_school_cctv_files = json_decode($users->no_of_school_cctv_files, true);
            if ($users->size_of_school_ground_files != null)
                $users->size_of_school_ground_files = json_decode($users->size_of_school_ground_files, true);
            if ($users->no_of_school_water_hygene_facilities_files != null)
                $users->no_of_school_water_hygene_facilities_files = json_decode($users->no_of_school_water_hygene_facilities_files, true);
            if ($users->no_of_fire_safty_files != null)
                $users->no_of_fire_safty_files = json_decode($users->no_of_fire_safty_files, true);
            if ($users->i_e_s_r_l_files != null)
                $users->i_e_s_r_l_files = json_decode($users->i_e_s_r_l_files, true);
            if ($users->i_e_s_r_l_year2_files != null)
                $users->i_e_s_r_l_year2_files = json_decode($users->i_e_s_r_l_year2_files, true);
            if ($users->i_e_s_r_l_year3_files != null)
                $users->i_e_s_r_l_year3_files = json_decode($users->i_e_s_r_l_year3_files, true);



            if ($users->school_name_files != null)
                $users->school_name_files = json_decode($users->school_name_files, true);
            if ($users->udise_code_files != null)
                $users->udise_code_files = json_decode($users->udise_code_files, true);
            if ($users->from_class_files != null)
                $users->from_class_files = json_decode($users->from_class_files, true);
            if ($users->upto_class_files != null)
                $users->upto_class_files = json_decode($users->upto_class_files, true);
            if ($users->recognised_by_files != null)
                $users->recognised_by_files = json_decode($users->recognised_by_files, true);
            if ($users->district_files != null)
                $users->district_files = json_decode($users->district_files, true);
            if ($users->post_office_files != null)
                $users->post_office_files = json_decode($users->post_office_files, true);
            if ($users->village_city_files != null)
                $users->village_city_files = json_decode($users->village_city_files, true);
            if ($users->pincode_files != null)
                $users->pincode_files = json_decode($users->pincode_files, true);
            if ($users->phone_with_std_code_files != null)
                $users->phone_with_std_code_files = json_decode($users->phone_with_std_code_files, true);
            if ($users->fax_with_std_code_files != null)
                $users->fax_with_std_code_files = json_decode($users->fax_with_std_code_files, true);
            if ($users->email_files != null)
                $users->email_files = json_decode($users->email_files, true);
            if ($users->police_station_files != null)
                $users->police_station_files = json_decode($users->police_station_files, true);
            if ($users->challan_number_1_5_files != null)
                $users->challan_number_1_5_files = json_decode($users->challan_number_1_5_files, true);
            if ($users->challan_number_1_8_files != null)
                $users->challan_number_1_8_files = json_decode($users->challan_number_1_8_files, true);
            if ($users->estd_year_files != null)
                $users->estd_year_files = json_decode($users->estd_year_files, true);
            if ($users->opening_date_files != null)
                $users->opening_date_files = json_decode($users->opening_date_files, true);
            if ($users->society_name_files != null)
                $users->society_name_files = json_decode($users->society_name_files, true);
            if ($users->is_society_registered_files != null)
                $users->is_society_registered_files = json_decode($users->is_society_registered_files, true);
            if ($users->society_registration_valid_upto_files != null)
                $users->society_registration_valid_upto_files = json_decode($users->society_registration_valid_upto_files, true);
            if ($users->evidence_of_non_proprietary_nature_files != null)
                $users->evidence_of_non_proprietary_nature_files = json_decode($users->evidence_of_non_proprietary_nature_files, true);
            if ($users->school_chairman_name_files != null)
                $users->school_chairman_name_files = json_decode($users->school_chairman_name_files, true);
            if ($users->are_school_building_used_files != null)
                $users->are_school_building_used_files = json_decode($users->are_school_building_used_files, true);
            if ($users->school_total_area_files != null)
                $users->school_total_area_files = json_decode($users->school_total_area_files, true);

            if ($users->session_year_files != null)
                $users->session_year_files = json_decode($users->session_year_files, true);
            if ($users->income_files != null)
                $users->income_files = json_decode($users->income_files, true);
            if ($users->expenses_files != null)
                $users->expenses_files = json_decode($users->expenses_files, true);
            if ($users->surplus_money_files != null)
                $users->surplus_money_files = json_decode($users->surplus_money_files, true);
            if ($users->reduced_money_files != null)
                $users->reduced_money_files = json_decode($users->reduced_money_files, true);
            if ($users->class_files != null)
                $users->class_files = json_decode($users->class_files, true);
            if ($users->teaching_fee_files != null)
                $users->teaching_fee_files = json_decode($users->teaching_fee_files, true);
            if ($users->registration_fee_files != null)
                $users->registration_fee_files = json_decode($users->registration_fee_files, true);
            if ($users->enrollment_fee_files != null)
                $users->enrollment_fee_files = json_decode($users->enrollment_fee_files, true);
            if ($users->security_money_files != null)
                $users->security_money_files = json_decode($users->security_money_files, true);
            if ($users->annual_charge_files != null)
                $users->annual_charge_files = json_decode($users->annual_charge_files, true);
            if ($users->development_fee_files != null)
                $users->development_fee_files = json_decode($users->development_fee_files, true);
            if ($users->other_charges_files != null)
                $users->other_charges_files = json_decode($users->other_charges_files, true);
            if ($users->total_files != null)
                $users->total_files = json_decode($users->total_files, true);
            if ($users->medium_files != null)
                $users->medium_files = json_decode($users->medium_files, true);
            if ($users->type_of_school_files != null)
                $users->type_of_school_files = json_decode($users->type_of_school_files, true);
            if ($users->supported_agency_name_files != null)
                $users->supported_agency_name_files = json_decode($users->supported_agency_name_files, true);
            if ($users->agency_supported_percent_files != null)
                $users->agency_supported_percent_files = json_decode($users->agency_supported_percent_files, true);
            if ($users->authority_name_files != null)
                $users->authority_name_files = json_decode($users->authority_name_files, true);
            if ($users->recognised_number_files != null)
                $users->recognised_number_files = json_decode($users->recognised_number_files, true);
            if ($users->is_school_on_rented_files != null)
                $users->is_school_on_rented_files = json_decode($users->is_school_on_rented_files, true);
            if ($users->khata_number_files != null)
                $users->khata_number_files = json_decode($users->khata_number_files, true);
            if ($users->plot_number_files != null)
                $users->plot_number_files = json_decode($users->plot_number_files, true);

            if ($users->school_building_area_files != null)
                $users->school_building_area_files = json_decode($users->school_building_area_files, true);
            if ($users->pre_elementary_class_files != null)
                $users->pre_elementary_class_files = json_decode($users->pre_elementary_class_files, true);

            if ($users->no_of_class_files != null)
                $users->no_of_class_files = json_decode($users->no_of_class_files, true);
            if ($users->avg_size_cls_room_files != null)
                $users->avg_size_cls_room_files = json_decode($users->avg_size_cls_room_files, true);
            if ($users->no_of_office_room_files != null)
                $users->no_of_office_room_files = json_decode($users->no_of_office_room_files, true);
            if ($users->avg_size_of_office_room_files != null)
                $users->avg_size_of_office_room_files = json_decode($users->avg_size_of_office_room_files, true);

            if ($users->last_three_year_tot_income_files_1 != null)
                $users->last_three_year_tot_income_files_1 = json_decode($users->last_three_year_tot_income_files_1, true);
            if ($users->last_three_year_tot_income_files_2 != null)
                $users->last_three_year_tot_income_files_2 = json_decode($users->last_three_year_tot_income_files_2, true);
            if ($users->last_three_year_tot_income_files_3 != null)
                $users->last_three_year_tot_income_files_3 = json_decode($users->last_three_year_tot_income_files_3, true);

            if ($users->no_of_store_room_files != null)
                $users->no_of_store_room_files = json_decode($users->no_of_store_room_files, true);
            if ($users->avg_size_of_store_room_files != null)
                $users->avg_size_of_store_room_files = json_decode($users->avg_size_of_store_room_files, true);
            if ($users->no_of_princpal_room_files != null)
                $users->no_of_princpal_room_files = json_decode($users->no_of_princpal_room_files, true);
            if ($users->avg_size_of_principal_room_files != null)
                $users->avg_size_of_principal_room_files = json_decode($users->avg_size_of_principal_room_files, true);
            if ($users->no_of_kitchen_room_files != null)
                $users->no_of_kitchen_room_files = json_decode($users->no_of_kitchen_room_files, true);
            if ($users->avg_size_of_kitchen_room_files != null)
                $users->avg_size_of_kitchen_room_files = json_decode($users->avg_size_of_kitchen_room_files, true);

            if ($users->educational_qualification_files != null)
                $users->educational_qualification_files = json_decode($users->educational_qualification_files, true);
            if ($users->teacher_educational_qualification_files != null)
                $users->teacher_educational_qualification_files = json_decode($users->teacher_educational_qualification_files, true);

            if ($users->class_names_ex != null)
                $users->class_names_ex = json_decode($users->class_names_ex, true);

            if ($users->no_of_sections_ex != null)
                $users->no_of_sections_ex = json_decode($users->no_of_sections_ex, true);

            if ($users->no_of_students_ex != null)
                $users->no_of_students_ex = json_decode($users->no_of_students_ex, true);
            if ($users->teacher_name != null)
                $users->teacher_name = json_decode($users->teacher_name, true);
            if ($users->teacher_trainee_qualification != null)
                $users->teacher_trainee_qualification = json_decode($users->teacher_trainee_qualification, true);
            if ($users->teacher_teaching_experience != null)
                $users->teacher_teaching_experience = json_decode($users->teacher_teaching_experience, true);
            if ($users->teacher_date_of_appointment != null)
                $users->teacher_date_of_appointment = json_decode($users->teacher_date_of_appointment, true);
            if ($users->teacher_f_h_w_name != null)
                $users->teacher_f_h_w_name = json_decode($users->teacher_f_h_w_name, true);
            if ($users->teacher_date_of_birth != null)
                $users->teacher_date_of_birth = json_decode($users->teacher_date_of_birth, true);
            if ($users->teacher_education_qualification != null)
                $users->teacher_education_qualification = json_decode($users->teacher_education_qualification, true);
            if ($users->teacher_cls_handed_over != null)
                $users->teacher_cls_handed_over = json_decode($users->teacher_cls_handed_over, true);
            if ($users->teacher_trained_or_untrained != null)
                $users->teacher_trained_or_untrained = json_decode($users->teacher_trained_or_untrained, true);
            if ($users->teacher_student_ratio != null)
                $users->teacher_student_ratio = json_decode($users->teacher_student_ratio, true);

            if ($users->teacher_student_assign != null)
                $users->teacher_student_assign = json_decode($users->teacher_student_assign, true);

            if ($users->num_of_teacher != null)
                $users->num_of_teacher = json_decode($users->num_of_teacher, true);
            if ($users->details_of_curriculum_ex != null)
                $users->details_of_curriculum_ex = json_decode($users->details_of_curriculum_ex, true);

            if ($users->method_of_inspection_ex != null)
                $users->method_of_inspection_ex = json_decode($users->method_of_inspection_ex, true);

            if ($users->evidence_of_non_proprietary_nature_ex != null)
                $users->evidence_of_non_proprietary_nature_ex = json_decode($users->evidence_of_non_proprietary_nature_ex, true);

            if ($users->last_three_year_tot_income != null)
                $users->last_three_year_tot_income = json_decode($users->last_three_year_tot_income, true);

            if ($users->water_facilities != null)
                $users->water_facilities = json_decode($users->water_facilities, true);

            if ($users->school_boundary_files != null)
                $users->school_boundary_files = json_decode($users->school_boundary_files, true);
            if ($users->school_cctv_file != null)
                $users->school_cctv_file = json_decode($users->school_cctv_file, true);

            if ($users->school_ground_file != null)
                $users->school_ground_file = json_decode($users->school_ground_file, true);

            if ($users->school_water_hygene_facilities_files != null)
                $users->school_water_hygene_facilities_files = json_decode($users->school_water_hygene_facilities_files, true);

            if ($users->school_fire_safty_facilities_files != null)
                $users->school_fire_safty_facilities_files = json_decode($users->school_fire_safty_facilities_files, true);

            if ($users->all_teaching_material_list_files_ex != null)
                $users->all_teaching_material_list_files_ex = json_decode($users->all_teaching_material_list_files_ex, true);

            if ($users->all_sports_equipment_list_file != null)
                $users->all_sports_equipment_list_file = json_decode($users->all_sports_equipment_list_file, true);

            if ($users->cleanliness != null)
                $users->cleanliness = json_decode($users->cleanliness, true);

            if ($users->books_in_library != null)
                $users->books_in_library = json_decode($users->books_in_library, true);

            if ($users->school_rent_deatail != null)
                $users->school_rent_deatail = json_decode($users->school_rent_deatail, true);

            if ($users->school_area_deatail != null)
                $users->school_area_deatail = json_decode($users->school_area_deatail, true);

            $users->district = Fnd_value::where('fnd_values.id',  $users->district)->value('display_value');

            $Accepted_Fields = 0;
            $Rejected_Fields = 0;
            $pending_Fields = 0;

            $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
            // echo $alltable_col;
            foreach ($alltable_col as $key => $value) {
                if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at") {

                    if (DB::table('application_data_verifications')->where('application_data_verifications.application_data_id', $id)->where($value, 2)->exists()) {
                        $Accepted_Fields++;
                    }
                    if (DB::table('application_data_verifications')->where('application_data_verifications.application_data_id', $id)->where($value, 3)->exists()) {
                        $Rejected_Fields++;
                    }
                    if (DB::table('application_data_verifications')->where('application_data_verifications.application_data_id', $id)->where($value, 1)->exists()) {
                        $pending_Fields++;
                    }
                }
            }
            $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
            // echo $alltable_col;
            foreach ($alltable_ext_col as $key => $value) {
                if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {

                    if (DB::table('application_data_verification_ext_1s')->where('application_data_verification_ext_1s.application_data_id', $id)->where($value, 2)->exists()) {
                        $Accepted_Fields++;
                    }
                    if (DB::table('application_data_verification_ext_1s')->where('application_data_verification_ext_1s.application_data_id', $id)->where($value, 3)->exists()) {
                        $Rejected_Fields++;
                    }
                    if (DB::table('application_data_verification_ext_1s')->where('application_data_verification_ext_1s.application_data_id', $id)->where($value, 1)->exists()) {
                        $pending_Fields++;
                    }
                }
            }

            $users->Accepted = $Accepted_Fields;
            $key1 = Schema::getColumnListing('application_data');
            $key2 = Schema::getColumnListing('application_data_ext_1');
            $users = ['key1' => $key1, 'key2' => $key2, 'user' => $users];
            return response()->json(['user' => $users]);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }


    //  public function showAllData($id)
    // {

    //     $users = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
    //     ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
    //     ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_id')
    //     ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.apllication_data_id')
    //     ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
    //     ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.apllication_data_id')
    //     ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
    //     ->where('application_data_ext_1.application_data_id', $id)->first();

    //     if($users == ""){
    //         return response()->json(['message'=>"user not found",'users'=>$users]);
    //     }
    //    else{
    //         return response()->json(['message'=>"found", 'users'=>$users]);
    //     } 
    //     //return Response()->json(['users'=>$users]);


    // }


    // public function feedbackDashboard()
    // {
    //     $feedback = DB::table('application_data')
    //     ->select(
    //         'application_data.id',
    //         'application_data.created_by',
    //         'application_data.school_name',
    //         'application_data.status',
    //         'application_data.feedback_file_upload',
    //         'application_data.feedback_from_dsc_to_school'
    //     )
    //     ->where('application_data.created_by', Auth::guard('api')->user()->id)->where('application_data.status', 1)->get();

    // //    return $feedback;

    //     return view('dashboard',['feedback'=>$feedback]);
    // }





    public function schoolList(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'dse') {
                $UserDistrict = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::guard('api')->user()->id)
                    ->value('addresses.district');
                // return($UserDistrict);
                $data['block_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
                    ->pluck('fnd_values.display_value as block_name', 'fnd_values.id as block_id');

                $data['school_list'] = DB::table('private_schools')
                    ->join('users', 'private_schools.id', '=', 'users.school_id')

                    ->select(
                        'private_schools.id',
                        'private_schools.school_code',
                        'private_schools.school_name',
                        'private_schools.school_email',
                    )->where('user_role', 'school')->pluck('school_name', 'id');

                // Private_school::where('id',Auth::guard('api')->user()->school_id)->pluck('school_name','id');
                $search_text = $request->search_text;
                $block = $request->Block;
                //  return['amar'];
                $school = $request->schoolList;
                $from_date = $request->from_date;
                $to_date = $request->to_date;
                $blockName = null;
                $schoolName = null;

                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                if (isset($request->schoolList) && $request->schoolList != null) {
                    $schoolName = Private_school::where('private_schools.id', $school)->value('school_name');
                }
                $data['adv_data'] = ['search_text' => $search_text, 'block_name' => $blockName, 'block_id' => $block, 'from_date' => $from_date, 'to_date' => $to_date, 'school' => $school];

                $data['Alldata'] = DB::table('private_schools')
                    ->leftjoin('application_data', 'application_data.school_id', 'private_schools.id')
                    ->leftjoin('fnd_values', 'application_data.block', '=', 'fnd_values.id')
                    ->join(
                        'users',
                        'private_schools.id',
                        '=',
                        'users.school_id'
                    )

                    ->select(
                        'private_schools.id',
                        'private_schools.school_code',
                        'private_schools.school_name',
                        'application_data.district',
                        'application_data.application_status',
                        'private_schools.school_email',
                        'application_data.block',
                        'fnd_values.display_value as block_name',

                        //    'private_schools.created_at', 
                        DB::raw('DATE_FORMAT(private_schools.created_at, "%d/%m/%Y %H:%i %p") as created_at'),
                        'users.name',
                        'users.user_role',
                        'users.email as useremail'
                    )->where('user_role', 'school')
                    ->where('application_data.district', $UserDistrict);
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['Alldata'] =  $data['Alldata']->where(function ($query) use ($search_text) {
                        $query->where('private_schools.school_code', 'like', '%' .  $search_text . '%')
                            ->orWhere('private_schools.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('private_schools.school_email', 'like', '%' .  $search_text . '%')

                            ->orWhere('users.name', 'like', '%' .  $search_text . '%')
                            ->orWhere('users.email', 'like', '%' .  $search_text . '%');
                    });
                }

                if (isset($request->Block) && $request->Block != null) {
                    $data['Alldata'] =  $data['Alldata']->where('application_data.district', $UserDistrict)->where('application_data.block',  $request->Block);
                }
                if (isset($request->school_status) && $request->school_status != null && $request->school_status > 0) {
                    $data['Alldata'] =  $data['Alldata']->where('application_data.district', $UserDistrict)->where('application_data.application_status',  $request->school_status);
                }

                if ($request->from_date != null && $request->to_date != null) {

                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-",  $from_date)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $to_date)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return response()->json(['warning' => "Form date always lesser than To date"]);
                    }

                    //return[$start_date,$end_date];  
                    $data['Alldata']->whereBetween('private_schools.created_at', [$fromDate, $toDate]);
                }
                if ($request->submit == 'excel') {
                    $data['Alldata'] = $data['Alldata']->get();
                } else {
                    $data['Alldata'] = $data['Alldata']->get();
                    //           echo '<pre>';
                    //   print_r($this->data['Alldata']); exit;
                }

                // return( $this->data);
                if ($request->submit == 'excel') {

                    if (count($data['Alldata']) == 0) {
                        return response()->json(['warning' => "There are no such data to export!!"], 400);
                    }
                    $params = ['data' => $data['Alldata'], 'view' => 'excel_view.schools_excel'];
                    $filename = 'schools.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                    return response()->download($params, $filename);
                } else {
                    return response()->json(['school' => $data]);
                }
            } else {
                return response()->json(['message' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'You are not Authorised!!'], 203);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }





    // public function schoolList()
    // {
    //     $Alldata = DB::table('private_schools')
    //    ->join('users', 'private_schools.id', '=', 'users.school_id')
    //    ->select(
    //        'private_schools.id', 
    //        'private_schools.school_code', 
    //        'private_schools.school_name', 
    //        'private_schools.school_email',
    //     //    'private_schools.created_at', 
    //        DB::raw('DATE_FORMAT(private_schools.created_at, "%d/%m/%Y %H:%i %p") as created_at'), 
    //        'users.name', 
    //        'users.user_role', 
    //        'users.email as useremail')->where('user_role', 'school')
    //    ->get();
    //      return response()->json(compact('Alldata'));
    // }
    public function EditAppForm($id)
    {
        // return $id;

        $userData = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
            ->where('application_data.created_by', $id)->where('application_data.status', 1)
            ->get();

        // return $userData;
        return response()->json('applicationEdit', ['userData' => $userData]);
    }


    // public function acceptSchoolDetail(Request $req)
    // {
    //     // return $req;
    //     $Application_ID = application_data::where('application_data.school_id', $req->approve_schoolId)->value('id');

    //     $user = Auth::guard('api')->user();
    //     $field_name_ext  = substr($req->fieldName, '-4');
    //     $field_name  = substr($req->fieldName, '0', '-4');
    //     //  return $field_name.'  -  '.$field_name_ext;

    //     if (\Input::has('file_reason')) {
    //         $ReasonFileArr = [];
    //         foreach (\Input::file('file_reason') as $file) {
    //             $fileInstance = $file;
    //             $newFileName = "approve_reason_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
    //             $fileInstance->move(public_path('applications/dsc_verification'), $newFileName);

    //             $ReasonFileArr[] = $newFileName;
    //         }
    //         if (count($ReasonFileArr) != 0) { // if have any file, otherwise null saved already
    //             $json_encodeFile_array = json_encode($ReasonFileArr);
    //         } else {
    //             $json_encodeFile_array = null;
    //         }
    //     } else {
    //         $json_encodeFile_array = null;
    //     }

    //     // if ($req->has('file_reason')) {
    //     //     $image = $req->file_reason;
    //     //     $fileName = time() . '.' . $req->file_reason->getClientOriginalName();
    //     //     $req->file_reason->move(public_path('applications/dsc_verification'), $fileName);
    //     //     $path = 'public/applications/dsc_verification/' . $fileName;
    //     // }

    //     if ($field_name_ext == '_ext') {
    //         // return "ext".$req;
    //         // Table::where('field', $value)->where('field', $value)->update(["'field_name1'=> $value1, 'field_name2'=> $value2"]);
    //         $application_data_remarks_ext_1 = application_data_remarks_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
    //         if (!empty($application_data_remarks_ext_1)) {
    //             $users = application_data_remarks_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
    //                 $field_name . '_rmk' => $req->remarks
    //             ]);
    //         }

    //         $application_data_verification_ext_1 = application_data_verification_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
    //         if (!empty($application_data_verification_ext_1)) {

    //             $count_verified_field = application_data_verification::where('school_id', $req->approve_schoolId)->value('count_verified_field');

    //             $selected_filed_value = application_data_verification_ext_1::where('school_id', $req->approve_schoolId)->where('application_data_id',$Application_ID)->value($field_name . '_vrfy');
    //             $count_verified_field = (int)$count_verified_field + 1;
    //             if($selected_filed_value==1){
    //                 application_data_verification::where('application_data_id', $Application_ID)->update([
    //                     'count_verified_field' => $count_verified_field
    //                     ]);
    //                 }
    //             application_data_verification_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
    //                 $field_name . '_vrfy' => $req->status
    //             ]);
    //         }

    //         $application_data_ext_file = application_data_ext_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
    //         if (!empty($application_data_ext_file)) {
    //             $usersStatus = application_data_ext_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
    //                 $field_name . '_files' => $json_encodeFile_array
    //             ]);
    //         }
    //     } else {
    //         // return $req;
    //         $application_data_remarks = application_data_remarks::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
    //         if (!empty($application_data_remarks)) {
    //             $users = application_data_remarks::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
    //                 $req->fieldName . '_rmk' => $req->remarks
    //             ]);
    //         }

    //         $application_data_file = application_data_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
    //         if (!empty($application_data_file)) {
    //             $usersStatus = application_data_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
    //                 $req->fieldName . '_files' => $json_encodeFile_array
    //             ]);
    //         }

    //         $application_data_verification = application_data_verification::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
    //         if (!empty($application_data_verification)) {
    //             $count_verified_field = application_data_verification::where('school_id', $req->approve_schoolId)->value('count_verified_field');

    //             $selected_filed_value = application_data_verification::where('school_id', $req->approve_schoolId)->where('application_data_id',$Application_ID)->value($req->fieldName . '_vrfy');
    //             $count_verified_field = (int)$count_verified_field + 1;
    //             if($selected_filed_value==1){
    //                 application_data_verification::where('application_data_id', $Application_ID)->update([
    //                     'count_verified_field' => $count_verified_field
    //                     ]);
    //                 }
    //             application_data_verification::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
    //                 $req->fieldName . '_vrfy' => $req->status
    //             ]);
    //         }
    //     }
    //     // return $req->approve_schoolId;
    //     $Application_ID = application_data::where('application_data.school_id', $req->approve_schoolId)->value('id');
    //     $Application_Status = application_data::where('application_data.school_id', $req->approve_schoolId)->value('application_status');
    //     // return $Application_Status ;

    //     if ($Application_Status == 2) {

    //         $application_tracking = new application_tracking;
    //         $application_tracking->application_id = $Application_ID;
    //         $application_tracking->school_id = $req->approve_schoolId;
    //         $application_tracking->application_status_id = 3;
    //         $application_tracking->created_by = $user->id;
    //         $application_tracking->save();

    //         application_data::where('id', $Application_ID)->update([
    //             'application_status' => 3,
    //             'updated_by' => $user->id
    //         ]);
    //     }
    //     return response()->json(['success'=> 'Accept Successfully !!!'],200);
    // }


    public function acceptSchoolDetail(Request $req)
    {
        // return $req;
        if (Auth::guard('api')->check()) {

            $user = Auth::guard('api')->user();
            $field_name_ext  = substr($req->fieldName, '-4');
            $field_name  = substr($req->fieldName, '0', '-4');
            //  return $field_name.'  -  '.$field_name_ext;
            // return $field_name;
            if ($req->has('file_reason')) {
                $ReasonFileArr = [];
                foreach ($req->file_reason as $file) {

                    $fileInstance = $file;
                    $newFileName = "approve_reason_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                    $fileInstance->move(public_path('/assets/applications/dsc_verification/'), $newFileName);

                    $ReasonFileArr[] = $newFileName;
                }
                if (count($ReasonFileArr) != 0) { // if have any file, otherwise null saved already
                    $json_encodeFile_array = json_encode($ReasonFileArr);
                } else {
                    $json_encodeFile_array = null;
                }
            } else {
                $json_encodeFile_array = null;
            }
            // return [$json_encodeFile_array];

            // if ($req->has('file_reason')) {
            //     $image = $req->file_reason;
            //     $fileName = time() . '.' . $req->file_reason->getClientOriginalName();
            //     $req->file_reason->move(public_path('applications/dsc_verification'), $fileName);
            //     $path = 'public/applications/dsc_verification/' . $fileName;
            // }

            if ($field_name_ext == '_ext') {
                // return "ext".$req;
                // Table::where('field', $value)->where('field', $value)->update(["'field_name1'=> $value1, 'field_name2'=> $value2"]);
                $application_data_remarks_ext_1 = application_data_remarks_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
                if (!empty($application_data_remarks_ext_1)) {
                    $users = application_data_remarks_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                        $field_name . '_rmk' => $req->remarks
                    ]);
                }

                $application_data_verification_ext_1 = application_data_verification_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
                if (!empty($application_data_verification_ext_1)) {
                    $usersStatus = application_data_verification_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                        $field_name . '_vrfy' => $req->status
                    ]);
                }

                $application_data_ext_file = application_data_ext_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
                if (!empty($application_data_ext_file)) {
                    $usersStatus = application_data_ext_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                        $field_name . '_files' => $json_encodeFile_array
                    ]);
                }
            } else {
                // return $req;
                $application_data_remarks = application_data_remarks::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
                if (!empty($application_data_remarks)) {
                    $users = application_data_remarks::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                        $req->fieldName . '_rmk' => $req->remarks
                    ]);
                }

                $application_data_file = application_data_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
                if (!empty($application_data_file)) {
                    $usersStatus = application_data_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                        $req->fieldName . '_files' => $json_encodeFile_array
                    ]);
                }

                $application_data_verification = application_data_verification::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
                if (!empty($application_data_verification)) {
                    $usersStatus = application_data_verification::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                        $req->fieldName . '_vrfy' => $req->status
                    ]);
                }
            }
            // return $req->approve_schoolId;
            $Application_ID = application_data::where('application_data.school_id', $req->approve_schoolId)->value('id');
            $Application_Status = application_data::where('application_data.school_id', $req->approve_schoolId)->value('application_status');
            // return $Application_Status ;

            // return [$req->file('file_reason')];
            if ($Application_Status == 2) {

                $application_tracking = new application_tracking;
                $application_tracking->application_id = $Application_ID;
                $application_tracking->school_id = $req->approve_schoolId;
                $application_tracking->application_status_id = 3;
                $application_tracking->created_by = $user->id;
                $application_tracking->save();

                application_data::where('id', $Application_ID)->update([
                    'application_status' => 3,
                    'updated_by' => $user->id
                ]);
            }
            return response()->json(['success' => 'Accept Successfully !!!']);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }


    public function rejectSchoolDetail(Request $req)
    {
        if (Auth::guard('api')->check()) {
            // return $req;
            $user = Auth::guard('api')->user();
            $field_name_ext  = substr($req->fieldName, '-4');
            $field_name  = substr($req->fieldName, '0', '-4');
            if ($req->has('file_reason')) {
                $ReasonFileArr = [];
                foreach ($req->file_reason as $file) {
                    $fileInstance = $file;
                    $newFileName = "reject_reason_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                    $fileInstance->move(public_path('/assets/applications/dsc_verification/'), $newFileName);

                    $ReasonFileArr[] = $newFileName;
                }
                if (count($ReasonFileArr) != 0) { // if have any file, otherwise null saved already
                    $json_encodeFile_array = json_encode($ReasonFileArr);
                } else {
                    $json_encodeFile_array = null;
                }
            } else {
                $json_encodeFile_array = null;
            }

            // if ($req->has('file_reason')) {
            //     $image = $req->file_reason;
            //     $fileName = time() . '.' . $req->file_reason->getClientOriginalName();
            //     $req->file_reason->move(public_path('applications/dsc_verification'), $fileName);
            //     $path = 'public/applications/dsc_verification/' . $fileName;
            // }

            //  return $field_name.'  -  '.$field_name_ext;
            if ($field_name_ext == '_ext') {

                $application_data_remarks_ext_1 = application_data_remarks_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_remarks_ext_1)) {
                    $users = application_data_remarks_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $field_name . '_rmk' => $req->remarks
                    ]);
                }

                $application_data_verification_ext_1 = application_data_verification_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_verification_ext_1)) {
                    $count_verified_field = application_data_verification::where('school_id', $req->reject_schoolId)->value('count_verified_field');

                    $selected_filed_value = application_data_verification_ext_1::where('school_id', $req->reject_schoolId)->where('application_data_id', $req->reject_applicationId)->value($field_name . '_vrfy');
                    $count_verified_field = (int)$count_verified_field + 1;
                    if ($selected_filed_value == 1) {
                        application_data_verification::where('application_data_id', $req->reject_applicationId)->update([
                            'count_verified_field' => $count_verified_field
                        ]);
                    }

                    application_data_verification_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $field_name . '_vrfy' => $req->status
                    ]);
                }

                $application_data_ext_file = application_data_ext_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_ext_file)) {
                    $usersStatus = application_data_ext_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $field_name . '_files' => $json_encodeFile_array
                    ]);
                }
            } else {

                $application_data_remarks = application_data_remarks::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_remarks)) {
                    $users = application_data_remarks::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $req->fieldName . '_rmk' => $req->remarks
                    ]);
                }

                $application_data_verification = application_data_verification::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_verification)) {
                    $count_verified_field = application_data_verification::where('school_id', $req->reject_schoolId)->value('count_verified_field');
                    $selected_filed_value = application_data_verification::where('school_id', $req->reject_schoolId)->where('application_data_id', $req->reject_applicationId)->value($req->fieldName . '_vrfy');
                    $count_verified_field = (int)$count_verified_field + 1;
                    // return $selected_filed_value;
                    // return $count_verified_field;
                    if ($selected_filed_value == 1) {
                        application_data_verification::where('application_data_id', $req->reject_applicationId)->update([
                            'count_verified_field' => $count_verified_field
                        ]);
                    }
                    application_data_verification::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $req->fieldName . '_vrfy' => $req->status
                    ]);
                }

                $application_data_file = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_file)) {
                    $usersStatus = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $req->fieldName . '_files' => $json_encodeFile_array
                    ]);
                }

                $application_data_file = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
                if (!empty($application_data_file)) {
                    $usersStatus = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                        $req->fieldName . '_files' => $json_encodeFile_array
                    ]);
                }
            }

            $Application_ID = application_data::where('application_data.school_id', $req->reject_schoolId)->value('id');
            $Application_Status = application_data::where('application_data.school_id', $req->reject_schoolId)->value('application_status');

            if ($Application_Status == 2) {

                $application_tracking = new application_tracking;
                $application_tracking->application_id = $req->reject_applicationId;
                $application_tracking->school_id = $req->reject_schoolId;
                $application_tracking->application_status_id = 3;
                $application_tracking->created_by = $user->id;
                $application_tracking->save();

                application_data::where('id', $Application_ID)->update([
                    'application_status' => 3,
                    'updated_by' => $user->id
                ]);
            }

            return response()->json(['warning' => 'Reject Successfully !!!'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function ApplicationAccept(Request $req)
    {
        // $image_name = '';
        if (Auth::guard('api')->check()) {

            $user = Auth::guard('api')->user();

            $date = Date('Y-m-d H:i:s');
            $image_name = [];
            $decode_image = '';

            if ($req->accpet_document != null) {
                foreach ($req->accpet_document as $img) {
                    if (isset($img) && !empty($img)) {
                        // return $req->accpet_document;
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('/assets/applications/dc_application/accept/'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }

            $application_certificate = new certificate;

            $application_certificate->school_id = $req->SchoolId;
            $application_certificate->certificate_number = $req->certificte_number;
            $application_certificate->certificate_document = $decode_image;
            $application_certificate->certifed_by = $user->id;
            $application_certificate->created_by = $user->id;

            $application_certificate->save();

            $application_tracking = new application_tracking;

            $application_tracking->application_id = $req->ApplicationId;
            $application_tracking->school_id = $req->SchoolId;
            $application_tracking->application_status_id = 6;

            $phone = Contact::where('school_id', $req->SchoolId)->value('mobile');
            $msg = 'Congratulations!! Your school has been recognised to issue certificate under Right to Education System.';
            $this->TrackingSMS($phone, $msg);

            $application_tracking->created_by = $user->id;

            $application_tracking->save();

            $Conversations = new Conversations;

            $Conversations->conversation_type = '1';
            $Conversations->parent_id = $req->ApplicationId;
            $Conversations->sender_id = $user->id;
            $Conversations->reciever_id = User::where('school_id', $req->SchoolId)->where('user_role', 'school')->value('id');
            $Conversations->title = 'Title';
            $Conversations->remarks = $req->accept_remarks;
            $Conversations->attachments = $decode_image;

            $Conversations->save();
            application_data::where('id', $req->ApplicationId)->update([

                'application_status' => 6,
                'updated_by' => $user->id

            ]);

            return response()->json(['success' => 'Application Accepted Successfully !!!'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function ApplicationReject(Request $req)
    {
        if (Auth::guard('api')->check()) {

            // return $req;
            $user = Auth::guard('api')->user();
            $decode_image = '';
            $image_name = [];


            if ($req->reject_document != null) {
                foreach ($req->reject_document as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('/assets/applications/dc_application/reject/'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }
            $application_tracking = new application_tracking;

            $application_tracking->application_id = $req->ApplicationId;
            $application_tracking->school_id = $req->SchoolId;
            $application_tracking->application_status_id = 7;

            $phone = Contact::where('school_id', $req->SchoolId)->value('mobile');
            $msg = 'Your application was rejected by District Committee due to some recognised issues. ';
            $this->TrackingSMS($phone, $msg);

            $application_tracking->created_by = $user->id;

            $application_tracking->save();

            $Conversations = new Conversations;

            $Conversations->conversation_type = '1';
            $Conversations->parent_id = $req->ApplicationId;
            $Conversations->sender_id = $user->id;
            $Conversations->reciever_id = User::where('school_id', $req->SchoolId)->where('user_role', 'school')->value('id');
            $Conversations->title = 'Title';
            $Conversations->remarks = $req->reject_remarks;
            $Conversations->attachments = $decode_image;

            $Conversations->save();

            application_data::where('id', $req->ApplicationId)->update([

                'application_status' => 7,
                'updated_by' => $user->id

            ]);
            return response()->json(['warning' => 'Application Rejected Successfully !!!'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function getApplicationData()
    {
        if (Auth::guard('api')->check()) {
            $userData = array();
            $userData = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
                ->leftjoin('application_teacher_data', 'application_data.id', '=', 'application_teacher_data.application_data_id')
                ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
                ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_data_id')
                ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.application_data_id')
                ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
                ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.application_data_id')
                ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
                ->where('application_data.school_id', Auth::guard('api')->user()->school_id)->where('application_data.status', 1)
                ->select(
                    'application_data.id as id',
                    'application_data.*',
                    'application_data_ext_1.id as application_data_ext_1_id',
                    'application_data_ext_1.trainee_qualification as pri_trainee_qualification',
                    'application_data_ext_1.teaching_experience as pri_teaching_experience',
                    'application_data_ext_1.date_of_appointment as pri_date_of_appointment',
                    'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                    'application_data_ext_1.all_teaching_material_list_files as all_teaching_material_list_files_ex',
                    'application_data.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature1',
                    'application_data_ext_1.*',
                    // 'application_teacher_data.id as application_teacher_data_id',
                    // 'application_teacher_data.teacher_educational_qualification_files as teacher_educational_qualification_files',
                    // 'application_teacher_data.*',

                    'application_data_ext_files.id as application_data_ext_files_id',
                    'application_data_ext_files.*',

                    // 'application_data_files.id as application_data_files_id',
                    // 'application_data_files.*',
                    'application_data_files.id as application_data_files_id',
                    'application_data_files.school_name_files as school_name_files',
                    'application_data_files.udise_code_files as udise_code_files',
                    'application_data_files.from_class_files as from_class_files',
                    'application_data_files.upto_class_files as upto_class_files',
                    'application_data_files.recognised_by_files as recognised_by_files',
                    'application_data_files.district_files as district_files',
                    'application_data_files.post_office_files as post_office_files',
                    'application_data_files.village_city_files as village_city_files',
                    'application_data_files.pincode_files as pincode_files',
                    'application_data_files.phone_with_std_code_files as phone_with_std_code_files',
                    'application_data_files.fax_with_std_code_files as fax_with_std_code_files',
                    'application_data_files.email_files as email_files',
                    'application_data_files.police_station_files as police_station_files',
                    'application_data_files.challan_number_1_5_files as challan_number_1_5_files',
                    'application_data_files.challan_number_1_8_files as challan_number_1_8_files',

                    'application_data_files.estd_year_files as estd_year_files',
                    'application_data_files.opening_date_files as opening_date_files',
                    'application_data_files.society_name_files as society_name_files',
                    'application_data_files.is_society_registered_files as is_society_registered_files',
                    'application_data_files.society_registration_valid_upto_files as society_registration_valid_upto_files',
                    'application_data_files.evidence_of_non_proprietary_nature_files as evidence_of_non_proprietary_nature_files',
                    'application_data_files.school_chairman_name_files as school_chairman_name_files',
                    'application_data_files.are_school_building_used_files as are_school_building_used_files',
                    'application_data_files.school_total_area_files as school_total_area_files',
                    'application_data_files.session_year_files as session_year_files',
                    'application_data_files.income_files as income_files',
                    'application_data_files.expenses_files as expenses_files',
                    'application_data_files.surplus_money_files as surplus_money_files',
                    'application_data_files.reduced_money_files as reduced_money_files',
                    'application_data_files.class_files as class_files',
                    'application_data_files.teaching_fee_files as teaching_fee_files',
                    'application_data_files.registration_fee_files as registration_fee_files',
                    'application_data_files.enrollment_fee_files as enrollment_fee_files',
                    'application_data_files.security_money_files as security_money_files',
                    'application_data_files.annual_charge_files as annual_charge_files',
                    'application_data_files.development_fee_files as development_fee_files',
                    'application_data_files.other_charges_files as other_charges_files',
                    'application_data_files.total_files as total_files',

                    'application_data_files.medium_files as medium_files',
                    'application_data_files.type_of_school_files as type_of_school_files',
                    'application_data_files.supported_agency_name_files as supported_agency_name_files',
                    'application_data_files.agency_supported_percent_files as agency_supported_percent_files',
                    'application_data_files.is_school_recognised_files as is_school_recognised_files',
                    'application_data_files.authority_name_files as authority_name_files',
                    'application_data_files.recognised_number_files as recognised_number_files',
                    'application_data_files.is_school_on_rented_files as is_school_on_rented_files',
                    'application_data_files.khata_number_files as khata_number_files',
                    'application_data_files.plot_number_files as plot_number_files',
                    'application_data_files.school_building_area_files as school_building_area_files',
                    'application_data_files.pre_elementary_class_files as pre_elementary_class_files',

                    'application_data_files.no_of_class_files as no_of_class_files',
                    'application_data_files.avg_size_cls_room_files as avg_size_cls_room_files',
                    'application_data_files.no_of_office_room_files as no_of_office_room_files',
                    'application_data_files.avg_size_of_office_room_files as avg_size_of_office_room_files',
                    'application_data_files.no_of_store_room_files as no_of_store_room_files',
                    'application_data_files.avg_size_of_store_room_files as avg_size_of_store_room_files',
                    'application_data_files.no_of_princpal_room_files as no_of_princpal_room_files',
                    'application_data_files.avg_size_of_principal_room_files as avg_size_of_principal_room_files',
                    'application_data_files.no_of_kitchen_room_files as no_of_kitchen_room_files',
                    'application_data_files.avg_size_of_kitchen_room_files    as avg_size_of_kitchen_room_files   ',

                    'application_data_remarks.id as application_data_remarks_id',
                    'application_data_remarks.*',

                    'application_data_remarks_ext_1s.id as application_data_remarks_ext_1s_id',
                    'application_data_remarks_ext_1s.*',

                    'application_data_verifications.id as application_data_verifications_id',
                    'application_data_verifications.*',

                    'application_data_verification_ext_1s.id as application_data_verification_ext_1s_id',
                    'application_data_verification_ext_1s.*'
                )
                // ->whereNotIn('application_data_ext_1.evidence_of_non_proprietary_nature','application_data_ext_1.last_three_year_tot_income')
                ->first();
            // return [$userData];

            $application_teacher_data = application_teacher_data::where('application_teacher_data.school_id', Auth::guard('api')->user()->school_id)
                ->select('application_teacher_data.*')
                ->get();
            foreach ($application_teacher_data as $key) {
                // echo $value;
                if ($key->teacher_educational_qualification_files != null)
                    $key->teacher_educational_qualification_files = json_decode($key->teacher_educational_qualification_files, true);
            }
            // return true;
            // return $application_teacher_data;
            // echo "<pre>"; print_r($userData);exit;
            $userData->application_teacher_data = $application_teacher_data;
            $allclasses = DB::table('fnd_classes')->where('status', 1)->get();
            if (!empty($userData)) {
                if ($userData->session_year != null)
                    $userData->session_year = json_decode($userData->session_year, true);

                if ($userData->income != null)
                    $userData->income = json_decode($userData->income, true);

                if ($userData->expenses != null)
                    $userData->expenses = json_decode($userData->expenses, true);

                if ($userData->surplus_money != null)
                    $userData->surplus_money = json_decode($userData->surplus_money, true);

                if ($userData->reduced_money != null)
                    $userData->reduced_money = json_decode($userData->reduced_money, true);

                if ($userData->class_finance != null)
                    $userData->class_finance = json_decode($userData->class_finance, true);

                if ($userData->teaching_fee != null)
                    $userData->teaching_fee = json_decode($userData->teaching_fee, true);

                if ($userData->registration_fee != null)
                    $userData->registration_fee = json_decode($userData->registration_fee, true);

                if ($userData->enrollment_fee != null)
                    $userData->enrollment_fee = json_decode($userData->enrollment_fee, true);

                if ($userData->security_money != null)
                    $userData->security_money = json_decode($userData->security_money, true);

                if ($userData->development_fee != null)
                    $userData->development_fee = json_decode($userData->development_fee, true);

                if ($userData->annual_charge != null)
                    $userData->annual_charge = json_decode($userData->annual_charge, true);

                if ($userData->other_charges != null)
                    $userData->other_charges = json_decode($userData->other_charges, true);

                if ($userData->total != null)
                    $userData->total = json_decode($userData->total, true);

                if ($userData->details_of_curriculum != null)
                    $userData->details_of_curriculum = json_decode($userData->details_of_curriculum, true);

                if ($userData->method_of_inspection != null)
                    $userData->method_of_inspection = json_decode($userData->method_of_inspection, true);

                if ($userData->teacher_name != null)
                    $userData->teacher_name = json_decode($userData->teacher_name, true);

                if ($userData->teacher_f_h_w_name != null)
                    $userData->teacher_f_h_w_name = json_decode($userData->teacher_f_h_w_name, true);

                if ($userData->date_of_birth != null)
                    $userData->date_of_birth = json_decode($userData->date_of_birth, true);

                if ($userData->education_qualification != null)
                    $userData->education_qualification = json_decode($userData->education_qualification, true);

                if ($userData->trainee_qualification != null)
                    $userData->trainee_qualification = json_decode($userData->trainee_qualification, true);

                if ($userData->teaching_experience != null)
                    $userData->teaching_experience = json_decode($userData->teaching_experience, true);

                if ($userData->cls_handed_over != null)
                    $userData->cls_handed_over = json_decode($userData->cls_handed_over, true);

                if ($userData->date_of_appointment != null)
                    $userData->date_of_appointment = json_decode($userData->date_of_appointment, true);

                if ($userData->trained_or_untrained != null)
                    $userData->trained_or_untrained = json_decode($userData->trained_or_untrained, true);

                if ($userData->class_names != null)
                    $userData->class_names = json_decode($userData->class_names, true);

                if ($userData->no_of_sections != null)
                    $userData->no_of_sections = json_decode($userData->no_of_sections, true);

                if ($userData->no_of_students != null)
                    $userData->no_of_students = json_decode($userData->no_of_students, true);

                if ($userData->teacher_student_ratio != null)
                    $userData->teacher_student_ratio = json_decode($userData->teacher_student_ratio, true);

                if ($userData->teacher_student_assign != null)
                    $userData->teacher_student_assign = json_decode($userData->teacher_student_assign, true);

                if ($userData->num_of_teacher != null)
                    $userData->num_of_teacher = json_decode($userData->num_of_teacher, true);

                if ($userData->evidence_of_non_proprietary_nature_ex != null)
                    $userData->evidence_of_non_proprietary_nature_ex = json_decode($userData->evidence_of_non_proprietary_nature_ex, true);


                if ($userData->school_name_files != null)
                    $userData->school_name_files = json_decode($userData->school_name_files, true);
                if ($userData->udise_code_files != null)
                    $userData->udise_code_files = json_decode($userData->udise_code_files, true);
                if ($userData->from_class_files != null)
                    $userData->from_class_files = json_decode($userData->from_class_files, true);
                if ($userData->upto_class_files != null)
                    $userData->upto_class_files = json_decode($userData->upto_class_files, true);
                if ($userData->recognised_by_files != null)
                    $userData->recognised_by_files = json_decode($userData->recognised_by_files, true);
                if ($userData->district_files != null)
                    $userData->district_files = json_decode($userData->district_files, true);
                if ($userData->post_office_files != null)
                    $userData->post_office_files = json_decode($userData->post_office_files, true);
                if ($userData->village_city_files != null)
                    $userData->village_city_files = json_decode($userData->village_city_files, true);
                if ($userData->pincode_files != null)
                    $userData->pincode_files = json_decode($userData->pincode_files, true);
                if ($userData->phone_with_std_code_files != null)
                    $userData->phone_with_std_code_files = json_decode($userData->phone_with_std_code_files, true);
                if ($userData->fax_with_std_code_files != null)
                    $userData->fax_with_std_code_files = json_decode($userData->fax_with_std_code_files, true);
                if ($userData->email_files != null)
                    $userData->email_files = json_decode($userData->email_files, true);
                if ($userData->police_station_files != null)
                    $userData->police_station_files = json_decode($userData->police_station_files, true);
                if ($userData->challan_number_1_5_files != null)
                    $userData->challan_number_1_5_files = json_decode($userData->challan_number_1_5_files, true);
                if ($userData->challan_number_1_8_files != null)
                    $userData->challan_number_1_8_files = json_decode($userData->challan_number_1_8_files, true);
                if ($userData->estd_year_files != null)
                    $userData->estd_year_files = json_decode($userData->estd_year_files, true);
                if ($userData->opening_date_files != null)
                    $userData->opening_date_files = json_decode($userData->opening_date_files, true);
                if ($userData->society_name_files != null)
                    $userData->society_name_files = json_decode($userData->society_name_files, true);
                if ($userData->is_society_registered_files != null)
                    $userData->is_society_registered_files = json_decode($userData->is_society_registered_files, true);
                if ($userData->society_registration_valid_upto_files != null)
                    $userData->society_registration_valid_upto_files = json_decode($userData->society_registration_valid_upto_files, true);
                if ($userData->evidence_of_non_proprietary_nature_files != null)
                    $userData->evidence_of_non_proprietary_nature_files = json_decode($userData->evidence_of_non_proprietary_nature_files, true);
                if ($userData->school_chairman_name_files != null)
                    $userData->school_chairman_name_files = json_decode($userData->school_chairman_name_files, true);
                if ($userData->are_school_building_used_files != null)
                    $userData->are_school_building_used_files = json_decode($userData->are_school_building_used_files, true);
                if ($userData->school_total_area_files != null)
                    $userData->school_total_area_files = json_decode($userData->school_total_area_files, true);

                if ($userData->session_year_files != null)
                    $userData->session_year_files = json_decode($userData->session_year_files, true);
                if ($userData->income_files != null)
                    $userData->income_files = json_decode($userData->income_files, true);
                if ($userData->expenses_files != null)
                    $userData->expenses_files = json_decode($userData->expenses_files, true);
                if ($userData->surplus_money_files != null)
                    $userData->surplus_money_files = json_decode($userData->surplus_money_files, true);
                if ($userData->reduced_money_files != null)
                    $userData->reduced_money_files = json_decode($userData->reduced_money_files, true);
                if ($userData->class_files != null)
                    $userData->class_files = json_decode($userData->class_files, true);
                if ($userData->teaching_fee_files != null)
                    $userData->teaching_fee_files = json_decode($userData->teaching_fee_files, true);
                if ($userData->registration_fee_files != null)
                    $userData->registration_fee_files = json_decode($userData->registration_fee_files, true);
                if ($userData->enrollment_fee_files != null)
                    $userData->enrollment_fee_files = json_decode($userData->enrollment_fee_files, true);
                if ($userData->security_money_files != null)
                    $userData->security_money_files = json_decode($userData->security_money_files, true);
                if ($userData->annual_charge_files != null)
                    $userData->annual_charge_files = json_decode($userData->annual_charge_files, true);
                if ($userData->development_fee_files != null)
                    $userData->development_fee_files = json_decode($userData->development_fee_files, true);
                if ($userData->other_charges_files != null)
                    $userData->other_charges_files = json_decode($userData->other_charges_files, true);
                if ($userData->total_files != null)
                    $userData->total_files = json_decode($userData->total_files, true);
                if ($userData->medium_files != null)
                    $userData->medium_files = json_decode($userData->medium_files, true);
                if ($userData->type_of_school_files != null)
                    $userData->type_of_school_files = json_decode($userData->type_of_school_files, true);
                if ($userData->supported_agency_name_files != null)
                    $userData->supported_agency_name_files = json_decode($userData->supported_agency_name_files, true);
                if ($userData->agency_supported_percent_files != null)
                    $userData->agency_supported_percent_files = json_decode($userData->agency_supported_percent_files, true);
                if ($userData->authority_name_files != null)
                    $userData->authority_name_files = json_decode($userData->authority_name_files, true);
                if ($userData->recognised_number_files != null)
                    $userData->recognised_number_files = json_decode($userData->recognised_number_files, true);
                if ($userData->is_school_on_rented_files != null)
                    $userData->is_school_on_rented_files = json_decode($userData->is_school_on_rented_files, true);
                if ($userData->khata_number_files != null)
                    $userData->khata_number_files = json_decode($userData->khata_number_files, true);
                if ($userData->plot_number_files != null)
                    $userData->plot_number_files = json_decode($userData->plot_number_files, true);

                if ($userData->school_building_area_files != null)
                    $userData->school_building_area_files = json_decode($userData->school_building_area_files, true);
                if ($userData->pre_elementary_class_files != null)
                    $userData->pre_elementary_class_files = json_decode($userData->pre_elementary_class_files, true);

                if ($userData->no_of_class_files != null)
                    $userData->no_of_class_files = json_decode($userData->no_of_class_files, true);
                if ($userData->avg_size_cls_room_files != null)
                    $userData->avg_size_cls_room_files = json_decode($userData->avg_size_cls_room_files, true);
                if ($userData->no_of_office_room_files != null)
                    $userData->no_of_office_room_files = json_decode($userData->no_of_office_room_files, true);
                if ($userData->avg_size_of_office_room_files != null)
                    $userData->avg_size_of_office_room_files = json_decode($userData->avg_size_of_office_room_files, true);
                if ($userData->no_of_store_room_files != null)
                    $userData->no_of_store_room_files = json_decode($userData->no_of_store_room_files, true);
                if ($userData->avg_size_of_store_room_files != null)
                    $userData->avg_size_of_store_room_files = json_decode($userData->avg_size_of_store_room_files, true);
                if ($userData->no_of_princpal_room_files != null)
                    $userData->no_of_princpal_room_files = json_decode($userData->no_of_princpal_room_files, true);
                if ($userData->avg_size_of_principal_room_files != null)
                    $userData->avg_size_of_principal_room_files = json_decode($userData->avg_size_of_principal_room_files, true);
                if ($userData->no_of_kitchen_room_files != null)
                    $userData->no_of_kitchen_room_files = json_decode($userData->no_of_kitchen_room_files, true);
                if ($userData->avg_size_of_kitchen_room_files != null)
                    $userData->avg_size_of_kitchen_room_files = json_decode($userData->avg_size_of_kitchen_room_files, true);


                // if ($userData->last_three_year_tot_income != null)
                //     $userData->last_three_year_tot_income = json_decode($userData->last_three_year_tot_income, true);

                if ($userData->last_three_year_tot_income_files_1 != null)
                    $userData->last_three_year_tot_income_files_1 = json_decode($userData->last_three_year_tot_income_files_1, true);
                if ($userData->last_three_year_tot_income_files_2 != null)
                    $userData->last_three_year_tot_income_files_2 = json_decode($userData->last_three_year_tot_income_files_2, true);
                if ($userData->last_three_year_tot_income_files_3 != null)
                    $userData->last_three_year_tot_income_files_3 = json_decode($userData->last_three_year_tot_income_files_3, true);

                if ($userData->water_facilities != null)
                    $userData->water_facilities = json_decode($userData->water_facilities, true);

                if ($userData->school_boundary_files != null)
                    $userData->school_boundary_files = json_decode($userData->school_boundary_files, true);
                if ($userData->school_cctv_file != null)
                    $userData->school_cctv_file = json_decode($userData->school_cctv_file, true);
                if ($userData->school_ground_file != null)
                    $userData->school_ground_file = json_decode($userData->school_ground_file, true);
                if ($userData->school_water_hygene_facilities_files != null)
                    $userData->school_water_hygene_facilities_files = json_decode($userData->school_water_hygene_facilities_files, true);
                if ($userData->school_fire_safty_facilities_files != null)
                    $userData->school_fire_safty_facilities_files = json_decode($userData->school_fire_safty_facilities_files, true);

                if ($userData->all_teaching_material_list_files_ex != null)
                    $userData->all_teaching_material_list_files_ex = json_decode($userData->all_teaching_material_list_files_ex, true);

                if ($userData->all_sports_equipment_list_file != null)
                    $userData->all_sports_equipment_list_file = json_decode($userData->all_sports_equipment_list_file, true);

                if ($userData->educational_qualification_files != null)
                    $userData->educational_qualification_files = json_decode($userData->educational_qualification_files, true);

                // if ($userData->application_teacher_data['teacher_educational_qualification_files_ex'] != null)
                //     $userData->teacher_educational_qualification_files_ex = json_decode($userData->teacher_educational_qualification_files_ex, true);

                if ($userData->cleanliness != null)
                    $userData->cleanliness = json_decode($userData->cleanliness, true);
                if ($userData->books_in_library != null)
                    $userData->books_in_library = json_decode($userData->books_in_library, true);
                if ($userData->school_rent_deatail != null)
                    $userData->school_rent_deatail = json_decode($userData->school_rent_deatail, true);
                if ($userData->school_area_deatail != null)
                    $userData->school_area_deatail = json_decode($userData->school_area_deatail, true);
                if ($userData->one_lac_fd_proof != null)
                    $userData->one_lac_fd_proof = json_decode($userData->one_lac_fd_proof, true);

                $userData->district = Fnd_value::where('fnd_values.id',  $userData->district)->value('display_value');
                $userData->allclasses = $allclasses;
            }
            // $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
            // ->orderBy('fnd_values.display_value', 'ASC')
            // ->pluck(
            // 'fnd_values.display_value as division_name',
            // 'fnd_values.id as division_id'
            // );
            // $UserDistrict = User::leftjoin('addresses','addresses.user_id','users.id')
            // ->where('addresses.user_id', Auth::guard('api')->user()->id)
            // ->value('addresses.district');
            // $this->data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
            // ->orderBy('fnd_values.display_value', 'ASC')
            // ->pluck(
            // 'fnd_values.display_value as district_name',
            // 'fnd_values.id as district_id'
            // );
            // echo "<pre>";
            // print_r($userData);
            // exit;

            $failed_payment = Transaction::where('application_id', $userData->id)->where('response_status', 'FAIL')->get();
            $userData->failed_payment = $failed_payment;

            $Message = [];
            $Application_ID = application_data::where('school_id', Auth::guard('api')->user()->school_id)
                ->value('id');
            $Message = Conversations::Join('users as sender', 'conversations.sender_id', '=', 'sender.id')
                ->Join('users as reciever', 'conversations.reciever_id', '=', 'reciever.id')
                ->select(
                    'conversations.*',
                    'reciever.user_role as reciever_role',
                    'sender.user_role as sender_role',
                    'reciever.name as Reciever_name',
                    'sender.name as Sender_name'
                )
                // ->Where('conversations.reciever_id', Auth::guard('api')->user()->school_id)
                ->Where('conversations.parent_id', $Application_ID)
                ->where('conversations.conversation_type', 4)
                ->get();
            $Message->images = [];
            $Message->application_conversation_unread_count = Conversations::Where('conversations.parent_id', $Application_ID)->where('conversations.conversation_type', 4)->where('school_read_status', 0)->count();


            $Application_ID = application_data::where('school_id', Auth::guard('api')->user()->school_id)->where('status', 1)->value('id');

            $total_vrfy_field = application_data_verification::where('school_id', Auth::guard('api')->user()->school_id)->value('total_vrfy_field');
            $count_verified_field = application_data_verification::where('school_id', Auth::guard('api')->user()->school_id)->value('count_verified_field');

            $userData->total_vrfy_field = $total_vrfy_field;
            $userData->count_verified_field = $count_verified_field;

            foreach ($Message as  $Conversation) {
                $Conversation->attachments = json_decode($Conversation->attachments);
            }
            if (!empty($userData) && ($userData->application_status == 1)) {
                return response()->json(['user' => $userData, 'allclasses' => $allclasses]);
            } elseif (empty($userData)) {
                return response()->json(['user' => $userData, 'allclasses' => $allclasses]);
            } else {
                return response()->json(['user' => $userData, 'Message' => $Message]);
            }
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function AddEditApplicationData(Request $request)
    {
        if (Auth::guard('api')->check()) {

            // echo "<pre>"; 
            // print_r($request);
            // exit;
            //    return $request->teacher_id;
            // $arr = array();
            // if($request->teacher_educational_qualification_files!=null || $request->teacher_educational_qualification_files !=''){

            //     foreach ($request->teacher_educational_qualification_files as $key => $value) {
            //         array_push($arr, $request->teacher_id[$key]);
            //         array_push($arr, $key);
            //         if ($request->has('teacher_educational_qualification_files')) {
            //             $AFileArr = [];

            //             foreach ($request->teacher_educational_qualification_files[$key] as $key1 => $value) {
            //                 $fileInstance = $value;
            //                 $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            //                 $AFileArr[] = $newFileName;
            //             }
            //             array_push($arr, json_encode($AFileArr) );
            //         }
            //     }
            // }
            // return $arr;


            //    return $request;


            // echo "<pre>"; 
            // print_r($request->id);
            // print_r($request->SectionNo);
            // print_r($request->studentNo);
            // print_r($request->method_of_inspection);
            // print_r($request->teacher_name);
            // exit;
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_form = application_data::find($request->id);
                if (!empty($application_form)) {
                    $application_form->updated_by  = Auth::guard('api')->user()->id;
                    $application_form->updated_at  = date('Y-m-d H:i:s');
                } else {
                    $application_form = new application_data;
                    $application_form->created_by  = Auth::guard('api')->user()->id;
                    $application_form->created_at  = date('Y-m-d H:i:s');
                }
            } else {
                $application_form = new application_data;
                $application_form->created_by  = Auth::guard('api')->user()->id;
                $application_form->created_at  = date('Y-m-d H:i:s');
            }
            $application_form->school_id  = Auth::guard('api')->user()->school_id;

            if (isset($request->school_name) && ($request->school_name != '' || $request->school_name != null)) {
                $application_form->school_name = $request->school_name;
            }

            if (isset($request->application_status) && ($request->application_status != '' || $request->application_status != null)) {
                $application_form->application_status = 1;
            }

            if (isset($request->recognised_by) && ($request->recognised_by != '' || $request->recognised_by != null)) {
                $application_form->recognised_by = $request->recognised_by;
            }
            if (isset($request->post_office) && ($request->post_office != '' || $request->post_office != null)) {
                $application_form->post_office = $request->post_office;
            }


            if (isset($request->village_town) && ($request->village_town != '' || $request->village_town != null)) {
                $application_form->village_city = $request->village_town;
            }
            if (isset($request->pin_code) && ($request->pin_code != '' || $request->pin_code != null)) {
                $application_form->pincode = $request->pin_code;
            }
            if (isset($request->phone_with_std_code) && ($request->phone_with_std_code != '' || $request->phone_with_std_code != null)) {
                $application_form->phone_with_std_code = $request->phone_with_std_code;
            }
            if (isset($request->fax_with_std_code) && ($request->fax_with_std_code != '' || $request->fax_with_std_code != null)) {
                $application_form->fax_with_std_code = $request->fax_with_std_code;
            }
            if (isset($request->email) && ($request->email != '' || $request->email != null)) {
                $application_form->email = $request->email;
            }
            if (isset($request->police_station) && ($request->police_station != '' || $request->police_station != null)) {
                $application_form->police_station = $request->police_station;
            }
            if (isset($request->estd_year) && ($request->estd_year != '' || $request->estd_year != null)) {
                $application_form->estd_year = $request->estd_year;
            }
            if (isset($request->opening_date) && ($request->opening_date != '' || $request->opening_date != null)) {
                $application_form->opening_date = $request->opening_date;
            }
            if (isset($request->society_name) && ($request->society_name != '' || $request->society_name != null)) {
                $application_form->society_name = $request->society_name;
            }
            if (isset($request->is_society_registered) && ($request->is_society_registered != '' || $request->is_society_registered != null)) {
                $application_form->is_society_registered = $request->is_society_registered;
            }
            if (isset($request->school_validation_till) && ($request->school_validation_till != '' || $request->school_validation_till != null)) {
                $application_form->school_validation_till = $request->school_validation_till;
            }
            if (isset($request->society_registration_valid_upto) && ($request->society_registration_valid_upto != '' || $request->society_registration_valid_upto != null)) {
                $application_form->society_registration_valid_upto = $request->society_registration_valid_upto;
            }
            if (isset($request->evidence_of_non_proprietary_nature) && ($request->evidence_of_non_proprietary_nature != '' || $request->evidence_of_non_proprietary_nature != null)) {
                $application_form->evidence_of_non_proprietary_nature = $request->evidence_of_non_proprietary_nature;
            }
            if (isset($request->school_chairman_name) && ($request->school_chairman_name != '' || $request->school_chairman_name != null)) {
                $application_form->school_chairman_name = $request->school_chairman_name;
            }
            if (isset($request->school_chairman_post) && ($request->school_chairman_post != '' || $request->school_chairman_post != null)) {
                $application_form->school_chairman_post = $request->school_chairman_post;
            }
            if (isset($request->school_chairman_address) && ($request->school_chairman_address != '' || $request->school_chairman_address != null)) {
                $application_form->school_chairman_address = $request->school_chairman_address;
            }
            if (isset($request->school_chairman_phone_number) && ($request->school_chairman_phone_number != '' || $request->school_chairman_phone_number != null)) {
                $application_form->school_chairman_phone_number = $request->school_chairman_phone_number;
            }
            if (isset($request->school_chairman_office) && ($request->school_chairman_office != '' || $request->school_chairman_office != null)) {
                $application_form->school_chairman_office = $request->school_chairman_office;
            }
            if (isset($request->school_chairman_email) && ($request->school_chairman_email != '' || $request->school_chairman_email != null)) {
                $application_form->school_chairman_email = $request->school_chairman_email;
            }
            if (isset($request->session_year) && ($request->session_year != '' || $request->session_year != null)) {
                $application_form->session_year =  json_encode($request->session_year, true);
            }
            if (isset($request->income) && ($request->income != '' || $request->income != null)) {
                $application_form->income =  json_encode($request->income, true);
            }
            if (isset($request->expenses) && ($request->expenses != '' || $request->expenses != null)) {
                $application_form->expenses =  json_encode($request->expenses, true);
            }
            if (isset($request->surplus_money) && ($request->surplus_money != '' || $request->surplus_money != null)) {
                $application_form->surplus_money =  json_encode($request->surplus_money, true);
            }
            if (isset($request->reduced_money) && ($request->reduced_money != '' || $request->reduced_money != null)) {
                $application_form->reduced_money =  json_encode($request->reduced_money, true);
            }
            if (isset($request->from_class_finance_id) && ($request->from_class_finance_id != '' || $request->from_class_finance_id != null)) {
                $application_form->from_class_finance = $request->from_class_finance_id;
            }
            if (isset($request->to_class_finance_id) && ($request->to_class_finance_id != '' || $request->to_class_finance_id != null)) {
                $application_form->to_class_finance = $request->to_class_finance_id;
            }
            if (isset($request->class_finance) && ($request->class_finance != '' || $request->class_finance != null)) {
                $application_form->class_finance =  json_encode($request->class_finance, true);
            }
            if (isset($request->teaching_fee) && ($request->teaching_fee != '' || $request->teaching_fee != null)) {
                $application_form->teaching_fee =  json_encode($request->teaching_fee, true);
            }
            if (isset($request->registration_fee) && ($request->registration_fee != '' || $request->registration_fee != null)) {
                $application_form->registration_fee =  json_encode($request->registration_fee, true);
            }
            if (isset($request->enrollment_fee) && ($request->enrollment_fee != '' || $request->enrollment_fee != null)) {
                $application_form->enrollment_fee =  json_encode($request->enrollment_fee, true);
            }
            if (isset($request->security_money) && ($request->security_money != '' || $request->security_money != null)) {
                $application_form->security_money =  json_encode($request->security_money, true);
            }
            if (isset($request->development_fee) && ($request->development_fee != '' || $request->development_fee != null)) {
                $application_form->development_fee =  json_encode($request->development_fee, true);
            }
            if (isset($request->annual_charge) && ($request->annual_charge != '' || $request->annual_charge != null)) {
                $application_form->annual_charge =  json_encode($request->annual_charge, true);
            }
            if (isset($request->other_charges) && ($request->other_charges != '' || $request->other_charges != null)) {
                $application_form->other_charges =  json_encode($request->other_charges, true);
            }
            if (isset($request->total) && ($request->total != '' || $request->total != null)) {
                $application_form->total =  json_encode($request->total, true);
            }


            if (isset($request->medium) && ($request->medium != '' || $request->medium != null)) {
                $application_form->medium = $request->medium;
            }
            if (isset($request->type_of_school) && ($request->type_of_school != '' || $request->type_of_school != null)) {
                $application_form->type_of_school = $request->type_of_school;
            }
            if (isset($request->supported_agency_name) && ($request->supported_agency_name != '' || $request->supported_agency_name != null)) {
                $application_form->supported_agency_name = $request->supported_agency_name;
            }
            if (isset($request->agency_supported_percent) && ($request->agency_supported_percent != '' || $request->agency_supported_percent != null)) {
                $application_form->agency_supported_percent = $request->agency_supported_percent;
            }
            if (isset($request->authority_name) && ($request->authority_name != '' || $request->authority_name != null)) {
                $application_form->authority_name = $request->authority_name;
            }
            if (isset($request->recognised_number) && ($request->recognised_number != '' || $request->recognised_number != null)) {
                $application_form->recognised_number = $request->recognised_number;
            }
            if (isset($request->is_school_on_rented) && ($request->is_school_on_rented != '' || $request->is_school_on_rented != null)) {
                $application_form->is_school_on_rented = $request->is_school_on_rented;
            }
            if (isset($request->are_school_building_used) && ($request->are_school_building_used != '' || $request->are_school_building_used != null)) {
                $application_form->are_school_building_used = $request->are_school_building_used;
            }
            if (isset($request->school_total_area) && ($request->school_total_area != '' || $request->school_total_area != null)) {
                $application_form->school_total_area = $request->school_total_area;
            }
            if (isset($request->plot_number) && ($request->plot_number != '' || $request->plot_number != null)) {
                $application_form->plot_number = $request->plot_number;
            }
            if (isset($request->khata_number) && ($request->khata_number != '' || $request->khata_number != null)) {
                $application_form->khata_number = $request->khata_number;
            }
            if (isset($request->school_building_area) && ($request->school_building_area != '' || $request->school_building_area != null)) {
                $application_form->school_building_area = $request->school_building_area;
            }
            if (isset($request->no_of_class) && ($request->no_of_class != '' || $request->no_of_class != null)) {
                $application_form->no_of_class = $request->no_of_class;
            }
            if (isset($request->avg_size_cls_room) && ($request->avg_size_cls_room != '' || $request->avg_size_cls_room != null)) {
                $application_form->avg_size_cls_room = $request->avg_size_cls_room;
            }

            if (isset($request->no_of_office_room) && ($request->no_of_office_room != '' || $request->no_of_office_room != null)) {
                $application_form->no_of_office_room = $request->no_of_office_room;
            }
            if (isset($request->avg_size_of_office_room) && ($request->avg_size_of_office_room != '' || $request->avg_size_of_office_room != null)) {
                $application_form->avg_size_of_office_room = $request->avg_size_of_office_room;
            }
            if (isset($request->no_of_store_room) && ($request->no_of_store_room != '' || $request->no_of_store_room != null)) {
                $application_form->no_of_store_room = $request->no_of_store_room;
            }
            if (isset($request->avg_size_of_store_room) && ($request->avg_size_of_store_room != '' || $request->avg_size_of_store_room != null)) {
                $application_form->avg_size_of_store_room = $request->avg_size_of_store_room;
            }
            if (isset($request->no_of_princpal_room) && ($request->no_of_princpal_room != '' || $request->no_of_princpal_room != null)) {
                $application_form->no_of_princpal_room = $request->no_of_princpal_room;
            }
            if (isset($request->avg_size_of_principal_room) && ($request->avg_size_of_principal_room != '' || $request->avg_size_of_principal_room != null)) {
                $application_form->avg_size_of_principal_room = $request->avg_size_of_principal_room;
            }
            if (isset($request->no_of_kitchen_room) && ($request->no_of_kitchen_room != '' || $request->no_of_kitchen_room != null)) {
                $application_form->no_of_kitchen_room = $request->no_of_kitchen_room;
            }
            if (isset($request->avg_size_of_kitchen_room) && ($request->avg_size_of_kitchen_room != '' || $request->avg_size_of_kitchen_room != null)) {
                $application_form->avg_size_of_kitchen_room = $request->avg_size_of_kitchen_room;
            }

            $application_form->save();


            // $application_form->district = $request->district;
            // $application_form->district = 1;
            // $application_form->state = 'Jharkhand';
            // $application_form->division = 1;
            // $application_form->block = 1;
            // $application_form->panchayat = 1;
            // $application_form->post_office = $request->post_office;
            // $application_form->village_city = $request->village_town;
            // $application_form->pincode = $request->pin_code;
            // $application_form->phone_with_std_code = $request->phone_with_std_code;
            // $application_form->fax_with_std_code = $request->fax_with_std_code;
            // $application_form->email = $request->email;
            // $application_form->police_station = $request->police_station;

            // $application_form->estd_year = $request->estd_year;
            // $application_form->opening_date = $request->opening_date;
            // $application_form->society_name = $request->society_name;
            // $application_form->is_society_registered = $request->is_society_registered;
            // $application_form->society_registration_valid_upto = $request->society_registration_valid_upto;

            // $application_form->evidence_of_non_proprietary_nature = $request->evidence_of_non_proprietary_nature;
            // $application_form->school_chairman_name = $request->school_chairman_name;
            // $application_form->school_chairman_post = $request->school_chairman_post;
            // $application_form->school_chairman_address = $request->school_chairman_address;
            // $application_form->school_chairman_phone_number = $request->school_chairman_phone_number;
            // $application_form->school_chairman_office = $request->school_chairman_office;
            // $application_form->school_chairman_email = $request->school_chairman_email;

            // $application_form->session_year =  json_encode($request->session_year, true);
            // $application_form->income =  json_encode($request->income, true);
            // $application_form->expenses =  json_encode($request->expenses, true);
            // $application_form->surplus_money =  json_encode($request->surplus_money, true);
            // $application_form->reduced_money =  json_encode($request->reduced_money, true);

            // $application_form->from_class_finance = $request->from_class_finance_id;
            // $application_form->to_class_finance = $request->to_class_finance_id;
            // $application_form->class_finance = json_encode($request->class_finance);
            // $application_form->teaching_fee = json_encode($request->teaching_fee);
            // $application_form->registration_fee = json_encode($request->registration_fee);
            // $application_form->enrollment_fee = json_encode($request->enrollment_fee);
            // $application_form->security_money = json_encode($request->security_money);
            // $application_form->development_fee = json_encode($request->development_fee);
            // $application_form->annual_charge = json_encode($request->annual_charge);
            // $application_form->other_charges = json_encode($request->other_charges);
            // $application_form->total = json_encode($request->total);


            // $application_form->medium = $request->medium;
            // $application_form->type_of_school = $request->type_of_school;
            // $application_form->supported_agency_name = $request->supported_agency_name;
            // $application_form->agency_supported_percent = $request->agency_supported_percent;
            // $application_form->authority_name = $request->authority_name;
            // $application_form->recognised_number = $request->recognised_number;
            // $application_form->is_school_on_rented = $request->is_school_on_rented;
            // $application_form->are_school_building_used = $request->are_school_building_used;
            // $application_form->school_total_area = $request->school_total_area;
            // $application_form->plot_number = $request->plot_number;
            // $application_form->khata_number = $request->khata_number;
            // $application_form->school_building_area = $request->school_building_area;
            // // $application_form->pre_elementary_class = $request->pre_elementary_class;
            // // $application_form->pre_no_of_section = $request->pre_no_of_section;
            // // $application_form->pre_no_of_student = $request->pre_no_of_student;
            // // $application_form->class_one_to_five = $request->class_one_to_five;
            // // $application_form->onefive_no_of_section = $request->onefive_no_of_section;
            // // $application_form->onefive_no_of_student = $request->onefive_no_of_student;
            // // $application_form->class_six_to_eight = $request->class_six_to_eight;
            // // $application_form->sixeight_no_of_section = $request->sixeight_no_of_section;
            // // $application_form->sixeight_no_of_student = $request->sixeight_no_of_student;

            // $application_form->no_of_class = $request->no_of_class;
            // $application_form->avg_size_cls_room = $request->avg_size_cls_room;

            // $application_form->no_of_office_room = $request->no_of_office_room;
            // $application_form->avg_size_of_office_room = $request->avg_size_of_office_room;
            // $application_form->no_of_store_room = $request->no_of_store_room;
            // $application_form->avg_size_of_store_room = $request->avg_size_of_store_room;
            // $application_form->no_of_princpal_room = $request->no_of_princpal_room;
            // $application_form->avg_size_of_principal_room = $request->avg_size_of_principal_room;
            // $application_form->no_of_kitchen_room = $request->no_of_kitchen_room;
            // $application_form->avg_size_of_kitchen_room = $request->avg_size_of_kitchen_room;
            // // $application_form->medium = $request->medium;

            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_form_ext = application_data_ext_1::where('application_data_id', $application_form->id)->where('school_id', Auth::guard('api')->user()->school_id)->first();
                if (empty($application_form_ext)) {
                    $application_form_ext = new application_data_ext_1;
                }
            } else {
                $application_form_ext = new application_data_ext_1;
            }
            $application_form_ext->application_data_id  = $application_form->id;
            $application_form_ext->school_id = Auth::guard('api')->user()->school_id;

            if (isset($request->facilities_access_without_interrupted) && ($request->facilities_access_without_interrupted != '' || $request->facilities_access_without_interrupted != null)) {
                $application_form_ext->facilities_access_without_interrupted = $request->facilities_access_without_interrupted;
            }
            if (isset($request->all_teaching_material_list) && ($request->all_teaching_material_list != '' || $request->all_teaching_material_list != null)) {
                $application_form_ext->all_teaching_material_list = $request->all_teaching_material_list;
            }
            if (isset($request->all_sports_equipment_list) && ($request->all_sports_equipment_list != '' || $request->all_sports_equipment_list != null)) {
                $application_form_ext->all_sports_equipment_list = $request->all_sports_equipment_list;
            }
            if (isset($request->books) && ($request->books != '' || $request->books != null)) {
                $application_form_ext->books = $request->books;
            }
            if (isset($request->magazines) && ($request->magazines != '' || $request->magazines != null)) {
                $application_form_ext->magazines = $request->magazines;
            }
            if (isset($request->type_of_water_facilities) && ($request->type_of_water_facilities != '' || $request->type_of_water_facilities != null)) {
                $application_form_ext->type_of_water_facilities = $request->type_of_water_facilities;
            }
            if (isset($request->no_of_water_supply) && ($request->no_of_water_supply != '' || $request->no_of_water_supply != null)) {
                $application_form_ext->no_of_water_supply = $request->no_of_water_supply;
            }
            if (isset($request->type_of_toilet) && ($request->type_of_toilet != '' || $request->type_of_toilet != null)) {
                $application_form_ext->type_of_toilet = $request->type_of_toilet;
            }
            if (isset($request->gents_toilet) && ($request->gents_toilet != '' || $request->gents_toilet != null)) {
                $application_form_ext->gents_toilet = $request->gents_toilet;
            }
            if (isset($request->ladies_toilet) && ($request->ladies_toilet != '' || $request->ladies_toilet != null)) {
                $application_form_ext->ladies_toilet = $request->ladies_toilet;
            }
            if (isset($request->principle_name) && ($request->principle_name != '' || $request->principle_name != null)) {
                $application_form_ext->principle_name = $request->principle_name;
            }
            if (isset($request->p_f_h_w_name) && ($request->p_f_h_w_name != '' || $request->p_f_h_w_name != null)) {
                $application_form_ext->p_f_h_w_name = $request->p_f_h_w_name;
            }
            if (isset($request->p_date_of_birth) && ($request->p_date_of_birth != '' || $request->p_date_of_birth != null)) {
                $application_form_ext->p_date_of_birth = $request->p_date_of_birth;
            }
            if (isset($request->p_education_qualification) && ($request->p_education_qualification != '' || $request->p_education_qualification != null)) {
                $application_form_ext->p_education_qualification = $request->p_education_qualification;
            }
            if (isset($request->trainee_qualification) && ($request->trainee_qualification != '' || $request->trainee_qualification != null)) {
                $application_form_ext->trainee_qualification = $request->trainee_qualification;
            }
            if (isset($request->teaching_experience) && ($request->teaching_experience != '' || $request->teaching_experience != null)) {
                $application_form_ext->teaching_experience = $request->teaching_experience;
            }
            if (isset($request->class_handed_over) && ($request->class_handed_over != '' || $request->class_handed_over != null)) {
                $application_form_ext->class_handed_over = $request->class_handed_over;
            }
            if (isset($request->date_of_appointment) && ($request->date_of_appointment != '' || $request->date_of_appointment != null)) {
                $application_form_ext->date_of_appointment = $request->date_of_appointment;
            }
            if (isset($request->trained_untrained) && ($request->trained_untrained != '' || $request->trained_untrained != null)) {
                $application_form_ext->trained_untrained = $request->trained_untrained;
            }

            if (isset($request->details_of_curriculum) && ($request->details_of_curriculum != '' || $request->details_of_curriculum != null)) {
                $application_form_ext->details_of_curriculum =  json_encode($request->details_of_curriculum, true);
            }

            if (isset($request->method_of_inspection) && ($request->method_of_inspection != '' || $request->method_of_inspection != null)) {
                $application_form_ext->method_of_inspection =  json_encode($request->method_of_inspection, true);
            }

            if (isset($request->school_board_exam_till_cls_eight) && ($request->school_board_exam_till_cls_eight != '' || $request->school_board_exam_till_cls_eight != null)) {
                $application_form_ext->school_board_exam_till_cls_eight = $request->school_board_exam_till_cls_eight;
            }
            if (isset($request->className) && ($request->className != '' || $request->className != null)) {
                $application_form_ext->class_names =  json_encode($request->className, true);
            }
            if (isset($request->SectionNo) && ($request->SectionNo != '' || $request->SectionNo != null)) {
                $application_form_ext->no_of_sections =  json_encode($request->SectionNo, true);
            }
            if (isset($request->studentNo) && ($request->studentNo != '' || $request->studentNo != null)) {
                $application_form_ext->no_of_students =  json_encode($request->studentNo, true);
            }
            if (isset($request->teacher_student_ratio) && ($request->teacher_student_ratio != '' || $request->teacher_student_ratio != null)) {
                $application_form_ext->teacher_student_ratio =  json_encode($request->teacher_student_ratio, true);
            }
            if (isset($request->teacher_student_assign) && ($request->teacher_student_assign != '' || $request->teacher_student_assign != null)) {
                $application_form_ext->teacher_student_assign =  json_encode($request->teacher_student_assign, true);
            }
            if (isset($request->num_of_teacher) && ($request->num_of_teacher != '' || $request->num_of_teacher != null)) {
                $application_form_ext->num_of_teacher =  json_encode($request->num_of_teacher, true);
            }



            if (isset($request->form_class_id) && ($request->form_class_id != '' || $request->form_class_id != null)) {
                $application_form_ext->form_class_id = $request->form_class_id;
            }
            if (isset($request->to_class_id) && ($request->to_class_id != '' || $request->to_class_id != null)) {
                $application_form_ext->to_class_id = $request->to_class_id;
            }
            if (isset($request->is_school_has_boundary) && ($request->is_school_has_boundary != '' || $request->is_school_has_boundary != null)) {
                $application_form_ext->is_school_has_boundary = $request->is_school_has_boundary;
            }
            if (isset($request->is_school_cctv) && ($request->is_school_cctv != '' || $request->is_school_cctv != null)) {
                $application_form_ext->is_school_cctv = $request->is_school_cctv;
            }
            if (isset($request->is_school_ground) && ($request->is_school_ground != '' || $request->is_school_ground != null)) {
                $application_form_ext->is_school_ground = $request->is_school_ground;
            }
            if (isset($request->is_school_water_hygene_facilities) && ($request->is_school_water_hygene_facilities != '' || $request->is_school_water_hygene_facilities != null)) {
                $application_form_ext->is_school_water_hygene_facilities = $request->is_school_water_hygene_facilities;
            }
            if (isset($request->is_school_fire_safty_facilities) && ($request->is_school_fire_safty_facilities != '' || $request->is_school_fire_safty_facilities != null)) {
                $application_form_ext->is_school_fire_safty_facilities = $request->is_school_fire_safty_facilities;
            }
            if (isset($request->size_of_school_boundary) && ($request->size_of_school_boundary != '' || $request->size_of_school_boundary != null)) {
                $application_form_ext->size_of_school_boundary = $request->size_of_school_boundary;
            }
            if (isset($request->no_of_school_cctv) && ($request->no_of_school_cctv != '' || $request->no_of_school_cctv != null)) {
                $application_form_ext->no_of_school_cctv = $request->no_of_school_cctv;
            }
            if (isset($request->size_of_school_ground) && ($request->size_of_school_ground != '' || $request->size_of_school_ground != null)) {
                $application_form_ext->size_of_school_ground = $request->size_of_school_ground;
            }
            if (isset($request->no_of_school_water_hygene_facilities) && ($request->no_of_school_water_hygene_facilities != '' || $request->no_of_school_water_hygene_facilities != null)) {
                $application_form_ext->no_of_school_water_hygene_facilities = $request->no_of_school_water_hygene_facilities;
            }
            if (isset($request->no_of_fire_safty) && ($request->no_of_fire_safty != '' || $request->no_of_fire_safty != null)) {
                $application_form_ext->no_of_fire_safty = $request->no_of_fire_safty;
            }
            // $application_form_ext_ext = new application_data_ext_1;

            // $application_form_ext_ext->facilities_access_without_interrupted = $request->facilities_access_without_interrupted;
            // $application_form_ext_ext->all_teaching_material_list = $request->all_teaching_material_list;
            // $application_form_ext_ext->all_sports_equipment_list = $request->all_sports_equipment_list;
            // $application_form_ext_ext->books = $request->books;
            // $application_form_ext_ext->magazines = $request->magazines;
            // $application_form_ext_ext->type_of_water_facilities = $request->type_of_water_facilities;
            // $application_form_ext_ext->no_of_water_supply = $request->no_of_water_supply;
            // $application_form_ext_ext->type_of_toilet = $request->type_of_toilet;
            // $application_form_ext_ext->gents_toilet = $request->gents_toilet;
            // $application_form_ext_ext->ladies_toilet = $request->ladies_toilet;
            // $application_form_ext_ext->principle_name = $request->principle_name;
            // $application_form_ext_ext->p_f_h_w_name = $request->p_f_h_w_name;
            // $application_form_ext_ext->p_date_of_birth = $request->p_date_of_birth;
            // $application_form_ext_ext->p_education_qualification = $request->p_education_qualification;
            // $application_form_ext_ext->trainee_qualification = $request->trainee_qualification;
            // $application_form_ext_ext->teaching_experience = $request->teaching_experience;
            // $application_form_ext_ext->class_handed_over = $request->class_handed_over;
            // $application_form_ext_ext->date_of_appointment = $request->date_of_appointment;
            // $application_form_ext_ext->trained_untrained = $request->trained_untrained;
            // $application_form_ext_ext->details_of_curriculum = json_encode($request->details_of_curriculum);
            // $application_form_ext_ext->method_of_inspection = json_encode($request->method_of_inspection);
            // $application_form_ext_ext->school_board_exam_till_cls_eight = $request->school_board_exam_till_cls_eight;
            // $application_form_ext_ext->class_names = json_encode($request->className);
            // $application_form_ext_ext->no_of_sections = json_encode($request->SectionNo);
            // $application_form_ext_ext->no_of_students = json_encode($request->studentNo);
            // $application_form_ext_ext->teacher_student_ratio = json_encode($request->teacher_student_ratio);
            // $application_form_ext_ext->teacher_student_assign = json_encode($request->teacher_student_assign);
            // $application_form_ext_ext->num_of_teacher = json_encode($request->num_of_teacher);
            // $application_form_ext_ext->form_class_id = $request->form_class_id;
            // $application_form_ext_ext->to_class_id = $request->to_class_id;
            // $application_form_ext_ext->is_school_has_boundary = $request->is_school_has_boundary;
            // $application_form_ext_ext->is_school_cctv = $request->is_school_cctv;
            // $application_form_ext_ext->is_school_ground = $request->is_school_ground;
            // $application_form_ext_ext->is_school_water_hygene_facilities = $request->is_school_water_hygene_facilities;
            // $application_form_ext_ext->is_school_fire_safty_facilities = $request->is_school_fire_safty_facilities;
            // $application_form_ext_ext->size_of_school_boundary = $request->size_of_school_boundary;
            // $application_form_ext_ext->no_of_school_cctv = $request->no_of_school_cctv;
            // $application_form_ext_ext->size_of_school_ground = $request->size_of_school_ground;
            // $application_form_ext_ext->no_of_school_water_hygene_facilities = $request->no_of_school_water_hygene_facilities;
            // $application_form_ext_ext->no_of_fire_safty = $request->no_of_fire_safty;
            if (isset($request->evidence_of_non_proprietary_nature_files) && ($request->evidence_of_non_proprietary_nature_files != '' || $request->evidence_of_non_proprietary_nature_files != null)) {
                if ($request->has('evidence_of_non_proprietary_nature_files')) {
                    $AFileArr = [];
                    foreach ($request->evidence_of_non_proprietary_nature_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->evidence_of_non_proprietary_nature = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->educational_qualification_files) && ($request->educational_qualification_files != '' || $request->educational_qualification_files != null)) {
                if ($request->has('educational_qualification_files')) {
                    $AFileArr = [];
                    foreach ($request->educational_qualification_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->educational_qualification_files = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->school_boundary_files) && ($request->school_boundary_files != '' || $request->school_boundary_files != null)) {

                if ($request->has('school_boundary_files')) {
                    $AFileArr = [];
                    foreach ($request->school_boundary_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_boundary_files = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->all_teaching_material_list_files) && ($request->all_teaching_material_list_files != '' || $request->all_teaching_material_list_files != null)) {

                if ($request->has('all_teaching_material_list_files')) {
                    $AFileArr = [];
                    foreach ($request->all_teaching_material_list_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->all_teaching_material_list_files = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->all_sports_equipment_list_file) && ($request->all_sports_equipment_list_file != '' || $request->all_sports_equipment_list_file != null)) {

                if ($request->has('all_sports_equipment_list_file')) {
                    $AFileArr = [];
                    foreach ($request->all_sports_equipment_list_file as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->all_sports_equipment_list_file = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->school_cctv_file) && ($request->school_cctv_file != '' || $request->school_cctv_file != null)) {

                if ($request->has('school_cctv_file')) {
                    $AFileArr = [];
                    foreach ($request->school_cctv_file as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_cctv_file = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->school_ground_file) && ($request->school_ground_file != '' || $request->school_ground_file != null)) {

                if ($request->has('school_ground_file')) {
                    $AFileArr = [];
                    foreach ($request->school_ground_file as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_ground_file = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->school_water_hygene_facilities_files) && ($request->school_water_hygene_facilities_files != '' || $request->school_water_hygene_facilities_files != null)) {

                if ($request->has('school_water_hygene_facilities_files')) {
                    $AFileArr = [];
                    foreach ($request->school_water_hygene_facilities_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $AFileArr[] = $newFileName;
                    }
                    if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_water_hygene_facilities_files = json_encode($AFileArr);
                    }
                }
            }
            if (isset($request->last_three_year_tot_income_files_1) && ($request->last_three_year_tot_income_files_1 != '' || $request->last_three_year_tot_income_files_1 != null)) {

                if ($request->has('last_three_year_tot_income_files_1')) {
                    $BFileArr = [];
                    foreach ($request->last_three_year_tot_income_files_1 as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_income_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $BFileArr[] = $newFileName;
                    }
                    if (count($BFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->last_three_year_tot_income_files_1 = json_encode($BFileArr);
                    }
                }
            }
            if (isset($request->last_three_year_tot_income_files_2) && ($request->last_three_year_tot_income_files_2 != '' || $request->last_three_year_tot_income_files_2 != null)) {

                if ($request->has('last_three_year_tot_income_files_2')) {
                    $BFileArr = [];
                    foreach ($request->last_three_year_tot_income_files_2 as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_income_2_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $BFileArr[] = $newFileName;
                    }
                    if (count($BFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->last_three_year_tot_income_files_2 = json_encode($BFileArr);
                    }
                }
            }
            if (isset($request->last_three_year_tot_income_files_3) && ($request->last_three_year_tot_income_files_3 != '' || $request->last_three_year_tot_income_files_3 != null)) {

                if ($request->has('last_three_year_tot_income_files_3')) {
                    $BFileArr = [];
                    foreach ($request->last_three_year_tot_income_files_3 as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_income_3_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $BFileArr[] = $newFileName;
                    }
                    if (count($BFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->last_three_year_tot_income_files_3 = json_encode($BFileArr);
                    }
                }
            }
            if (isset($request->water_facilities_files) && ($request->water_facilities_files != '' || $request->water_facilities_files != null)) {

                if ($request->has('water_facilities_files')) {
                    $CFileArr = [];
                    foreach ($request->water_facilities_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_3_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $CFileArr[] = $newFileName;
                    }
                    if (count($CFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->water_facilities = json_encode($CFileArr);
                    }
                }
            }
            if (isset($request->cleanliness_files) && ($request->cleanliness_files != '' || $request->cleanliness_files != null)) {

                if ($request->has('cleanliness_files')) {
                    $DFileArr = [];
                    foreach ($request->cleanliness_files as $file) {
                        $fileInstance = $file;
                        $newFileName = "application_4_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                        $fileInstance->move(public_path('applications'), $newFileName);

                        $DFileArr[] = $newFileName;
                    }
                    if (count($DFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->cleanliness = json_encode($DFileArr);
                    }
                }
            }
            if (isset($request->books_in_library_files) && ($request->books_in_library_files != '' || $request->books_in_library_files != null)) {

                if ($request->has('books_in_library_files')) {
                    $BlFileArr = [];
                    foreach ($request->books_in_library_files as $file) {
                        $fileInstanceBL = $file;
                        $newFileNameBL = "application_5_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceBL->getClientOriginalExtension();
                        $fileInstanceBL->move(public_path('applications'), $newFileNameBL);

                        $BlFileArr[] = $newFileNameBL;
                    }
                    if (count($BlFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->books_in_library = json_encode($BlFileArr);
                    }
                }
            }
            if (isset($request->school_rent_deatail) && ($request->school_rent_deatail != '' || $request->school_rent_deatail != null)) {

                if ($request->has('school_rent_deatail')) {
                    $SRFileArr = [];
                    foreach ($request->school_rent_deatail as $file) {
                        $fileInstanceSR = $file;
                        $newFileNameSR = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSR->getClientOriginalExtension();
                        $fileInstanceSR->move(public_path('applications'), $newFileNameSR);

                        $SRFileArr[] = $newFileNameSR;
                    }
                    if (count($SRFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_rent_deatail = json_encode($SRFileArr);
                    }
                }
            }
            if (isset($request->school_area_deatail) && ($request->school_area_deatail != '' || $request->school_area_deatail != null)) {

                if ($request->has('school_area_deatail')) {
                    $SAFileArr = [];
                    foreach ($request->school_area_deatail as $file) {
                        $fileInstanceSA = $file;
                        $newFileNameSA = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                        $fileInstanceSA->move(public_path('applications'), $newFileNameSA);

                        $SAFileArr[] = $newFileNameSA;
                    }
                    if (count($SAFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_area_deatail = json_encode($SAFileArr);
                    }
                }
            }
            if (isset($request->school_fire_safty_facilities_files) && ($request->school_fire_safty_facilities_files != '' || $request->school_fire_safty_facilities_files != null)) {

                if ($request->has('school_fire_safty_facilities_files')) {
                    $SAFileArr = [];
                    foreach ($request->school_fire_safty_facilities_files as $file) {
                        $fileInstanceSA = $file;
                        $newFileNameSA = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                        $fileInstanceSA->move(public_path('applications'), $newFileNameSA);

                        $SAFileArr[] = $newFileNameSA;
                    }
                    if (count($SAFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->school_fire_safty_facilities_files = json_encode($SAFileArr);
                    }
                }
            }
            if (isset($request->one_lac_fd_proof) && ($request->one_lac_fd_proof != '' || $request->one_lac_fd_proof != null)) {
                if ($request->has('one_lac_fd_proof')) {
                    $SAFileArr = [];
                    foreach ($request->one_lac_fd_proof as $file) {
                        $fileInstanceSA = $file;
                        $newFileNameSA = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                        $fileInstanceSA->move(public_path('applications'), $newFileNameSA);

                        $SAFileArr[] = $newFileNameSA;
                    }
                    if (count($SAFileArr) != 0) { // if have any file, otherwise null saved already
                        $application_form_ext->one_lac_fd_proof = json_encode($SAFileArr);
                    }
                }
            }
            $application_form_ext->save();

            // return $request->teacher_educational_qualification_files; 

            $arr = array();
            if (isset($request->teacher_id) && ($request->teacher_id != '' || $request->teacher_id != null) && is_array($request->teacher_id) && count($request->teacher_id) > 0) {
                // return [$request->teacher_id];
                // return ('amar');
                foreach ($request->teacher_id as $key => $val) {
                    array_push($arr, ['if 1' => $key]);
                    if (isset($request->teacher_id[$key]) && ($request->teacher_id[$key] != '' || $request->teacher_id[$key] != null)) {
                        $application_teacher_data = application_teacher_data::where('id', $request->teacher_id[$key])->first();

                        if (empty($application_teacher_data)) {
                            $application_teacher_data = new application_teacher_data;
                            array_push($arr, ['if ' => $key]);
                        }
                    } else if ($request->teacher_id[$key] == null) {
                        $application_teacher_data = new application_teacher_data;
                        array_push($arr, ['else if ' => $key]);
                    }
                    // return ['else '=>$arr];
                    // $application_teacher_data = new application_teacher_data;
                    if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                        $application_form->application_data_id = $request->id;
                    }
                    $application_teacher_data->school_id  = Auth::guard('api')->user()->school_id;

                    if (isset($request->teacher_name[$key]) && ($request->teacher_name[$key] != '' || $request->teacher_name[$key] != null)) {
                        $application_teacher_data->teacher_name = $request->teacher_name[$key];
                    }
                    if (isset($request->teacher_f_h_w_name[$key]) && ($request->teacher_f_h_w_name[$key] != '' || $request->teacher_f_h_w_name[$key] != null)) {
                        $application_teacher_data->teacher_f_h_w_name = $request->teacher_f_h_w_name[$key];
                    }
                    if (isset($request->teacher_date_of_birth[$key]) && ($request->teacher_date_of_birth[$key] != '' || $request->teacher_date_of_birth[$key] != null)) {
                        $application_teacher_data->date_of_birth = $request->teacher_date_of_birth[$key];
                    }
                    if (isset($request->teacher_education_qualification[$key]) && ($request->teacher_education_qualification[$key] != '' || $request->teacher_education_qualification[$key] != null)) {
                        $application_teacher_data->education_qualification = $request->teacher_education_qualification[$key];
                    }
                    if (isset($request->teacher_trainee_qualification[$key]) && ($request->teacher_trainee_qualification[$key] != '' || $request->teacher_trainee_qualification[$key] != null)) {
                        $application_teacher_data->trainee_qualification = $request->teacher_trainee_qualification[$key];
                    }
                    if (isset($request->teacher_teaching_experience[$key]) && ($request->teacher_teaching_experience[$key] != '' || $request->teacher_teaching_experience[$key] != null)) {
                        $application_teacher_data->teaching_experience = $request->teacher_teaching_experience[$key];
                    }
                    if (isset($request->teacher_class_handed_over[$key]) && ($request->teacher_class_handed_over[$key] != '' || $request->teacher_class_handed_over[$key] != null)) {
                        $application_teacher_data->cls_handed_over = $request->teacher_class_handed_over[$key];
                    }
                    if (isset($request->teacher_appointment_date[$key]) && ($request->teacher_appointment_date[$key] != '' || $request->teacher_appointment_date[$key] != null)) {
                        $application_teacher_data->date_of_appointment = $request->teacher_appointment_date[$key];
                    }
                    if (isset($request->teacher_trained_or_untrained[$key]) && ($request->teacher_trained_or_untrained[$key] != '' || $request->teacher_trained_or_untrained[$key] != null)) {
                        $application_teacher_data->trained_or_untrained = $request->teacher_trained_or_untrained[$key];
                    }
                    // if (isset($request->no_of_fire_safty) && ($request->no_of_fire_safty != '' || $request->no_of_fire_safty != null)) {
                    //     $application_teacher_data->no_of_fire_safty = $request->no_of_fire_safty;
                    // }
                    // if (isset($request->no_of_fire_safty) && ($request->no_of_fire_safty != '' || $request->no_of_fire_safty != null)) {
                    //     $application_teacher_data->no_of_fire_safty = $request->no_of_fire_safty;
                    // }
                    // $application_teacher_data->application_data_id = $request->id;
                    // $application_teacher_data->school_id  = Auth::guard('api')->user()->school_id;
                    // $application_teacher_data->teacher_name = $request->teacher_name[$key];
                    // $application_teacher_data->teacher_f_h_w_name = $request->teacher_f_h_w_name[$key];
                    // $application_teacher_data->date_of_birth = $request->teacher_date_of_birth[$key];
                    // $application_teacher_data->education_qualification = $request->teacher_education_qualification[$key];
                    // $application_teacher_data->trainee_qualification = $request->teacher_trainee_qualification[$key];
                    // $application_teacher_data->teaching_experience = $request->teacher_teaching_experience[$key];
                    // $application_teacher_data->cls_handed_over = $request->teacher_class_handed_over[$key];
                    // $application_teacher_data->date_of_appointment = $request->teacher_appointment_date[$key];
                    // $application_teacher_data->trained_or_untrained = $request->teacher_trained_or_untrained[$key];

                    if ($request->teacher_educational_qualification_files != null || $request->teacher_educational_qualification_files != '') {

                        foreach ($request->teacher_educational_qualification_files as $key_file => $value_file) {
                            if ($key_file == $key) {
                                // array_push($arr, $request->teacher_id[$key]);
                                array_push($arr, ['file ' => $key]);
                                if ($request->has('teacher_educational_qualification_files')) {
                                    $AFileArr = [];

                                    foreach ($request->teacher_educational_qualification_files[$key_file] as $key1 => $value_file) {
                                        $fileInstance = $value_file;
                                        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                                        $fileInstanceSA->move(public_path('applications'), $newFileNameSA);
                                        $AFileArr[] = $newFileName;
                                    }
                                    // array_push($arr, json_encode($AFileArr) );
                                    $application_teacher_data->teacher_educational_qualification_files = json_encode($AFileArr);
                                }
                            }
                        }
                    }



                    $application_teacher_data->save();
                    // return $arr;
                    // return $request->teacher_educational_qualification_files[$key] ;

                    //application_data_ext_files
                    if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                        $application_data_ext_files = application_data_ext_file::where('application_data_id', $application_form->id)->first();
                        if (empty($application_data_ext_files)) {
                            $application_data_ext_files = new application_data_ext_file;
                        }
                    } else {
                        $application_data_ext_files = new application_data_ext_file;
                    }
                    $application_data_ext_files->application_data_id = $application_form->id;
                    $application_data_ext_files->school_id = Auth::guard('api')->user()->school_id;
                    $application_data_ext_files->save();
                }
            }
            //application_data_files
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_data_files = application_data_file::where('application_data_id', $application_form->id)->first();
                if (empty($application_data_files)) {
                    $application_data_files = new application_data_file;
                    $application_data_files->created_by  = Auth::guard('api')->user()->id;
                } else {
                    $application_data_files->updated_at  = Auth::guard('api')->user()->id;
                }
            } else {
                $application_data_files = new application_data_file;
                $application_data_files->created_by  = Auth::guard('api')->user()->id;
            }
            $application_data_files->application_data_id = $application_form->id;
            $application_data_files->school_id = Auth::guard('api')->user()->school_id;
            $application_data_files->save();

            //application_data_remarks
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_data_remarks = application_data_remarks::where('application_data_id', $application_form->id)->first();
                if (empty($application_data_remarks)) {
                    $application_data_remarks = new application_data_remarks;
                    $application_data_remarks->created_by_rmk  = Auth::guard('api')->user()->id;
                } else {
                    $application_data_remarks->updated_by_rmk  = Auth::guard('api')->user()->id;
                }
            } else {
                $application_data_remarks = new application_data_remarks;
                $application_data_remarks->created_by_rmk  = Auth::guard('api')->user()->id;
            }
            $application_data_remarks->application_data_id = $application_form->id;
            $application_data_remarks->school_id = Auth::guard('api')->user()->school_id;
            $application_data_remarks->save();

            //application_data_remarks_ext_1s
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_data_remarks_ext_1s = application_data_remarks_ext_1::where('application_data_id', $application_form->id)->first();
                if (empty($application_data_remarks_ext_1s)) {
                    $application_data_remarks_ext_1s = new application_data_remarks_ext_1;
                }
            } else {
                $application_data_remarks_ext_1s = new application_data_remarks_ext_1;
            }
            $application_data_remarks_ext_1s->application_data_id = $application_form->id;
            $application_data_remarks_ext_1s->school_id = Auth::guard('api')->user()->school_id;
            $application_data_remarks_ext_1s->save();

            //application_data_verifications
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_data_verifications = application_data_verification::where('application_data_id', $application_form->id)->first();
                if (empty($application_data_verifications)) {
                    $application_data_verifications = new application_data_verification;
                    $application_data_verifications->created_by_vrfy  = Auth::guard('api')->user()->id;
                } else {
                    $application_data_verifications->updated_by_vrfy  = Auth::guard('api')->user()->id;
                }
            } else {
                $application_data_verifications = new application_data_verification;
                $application_data_verifications->created_by_vrfy  = Auth::guard('api')->user()->id;
            }
            $application_data_verifications->application_data_id = $application_form->id;
            $application_data_verifications->school_id = Auth::guard('api')->user()->school_id;
            $application_data_verifications->save();

            // /application_data_verification_ext_1s
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_data_verification_ext_1s = application_data_verification_ext_1::where('application_data_id', $application_form->id)->first();
                if (empty($application_data_verification_ext_1s)) {
                    $application_data_verification_ext_1s = new application_data_verification_ext_1;
                }
            } else {
                $application_data_verification_ext_1s = new application_data_verification_ext_1;
            }
            $application_data_verification_ext_1s->application_data_id   = $application_form->id;
            $application_data_verification_ext_1s->school_id = Auth::guard('api')->user()->school_id;
            $application_data_verification_ext_1s->save();
            // if (isset($request->save_data) && $request->save_data == '1') {

            //     $application_tracking = new application_tracking;
            //     $application_tracking->application_id = $application_form->id;
            //     $application_tracking->school_id = Auth::guard('api')->user()->school_id;
            //     $application_tracking->application_status_id = 2;
            //     $application_tracking->created_by = Auth::guard('api')->user()->id;
            //     $application_tracking->save();

            // application_data::where('id', $Application_ID)->update([
            //     'application_status' => 3,
            //     'updated_by' => Auth::guard('api')->user()->id
            //     ]);
            // }
            //if (isset($request->Payment) && $request->Payment == '3') {
            // return $request;
            //$Treasurys = Fnd_treasury_value::select(
            //'fnd_treasury_values.id',
            //'fnd_treasury_values.treasury_title'

            //)->get();

            //return view('payment_application', ['School_Data' => $request, 'Treasurys' => $Treasurys]);
            //}
            // return $request;
            if (isset($request->Payment) && $request->Payment == '3') {
                // return $request;
                return response()->json(['user' => $request]);
            }

            return response()->json(['message' => 'Data Has Been Inserted Successfully'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function applicationFieldsVarified(Request $request)
    {
        // return $request;
        $user = Auth::guard('api')->user();
        if (isset($request->verified_applicationId) && ($request->verified_applicationId != '' || $request->verified_applicationId != null)) {
            $application_form = application_data::find($request->verified_applicationId);
            if (!empty($application_form)) {
                $application_form->updated_by  = Auth::guard('api')->user()->id;
                $application_form->updated_at  = date('Y-m-d H:i:s');
            } else {
                return response()->json(['message' => 'Application not found']);
            }
        } else {
            return response()->json(['message' => 'Application not found']);
        }
        if ($request->status == 1) {
            $decode_image = '';
            if ($request->file_reason != null) {
                foreach ($request->file_reason as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('/assets/application_accept/'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }
            $application_form->application_status = 4;
            $Conversations = new Conversations;
            $Conversations->conversation_type = '5';
            $Conversations->parent_id = $request->verified_applicationId;
            $Conversations->sender_id = Auth::guard('api')->user()->id;
            $Conversations->reciever_id = $request->verified_schoolId;
            $Conversations->title = 'Title';
            $Conversations->remarks = $request->remarks;
            $Conversations->attachments = $decode_image;
            $Conversations->save();
        } elseif ($request->status == 0) {
            $decode_image = '';
            if ($request->file_reason != null) {
                foreach ($request->file_reason as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('/assets/application_reject/'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }
            $application_form->application_status = 8;
            $Conversations = new Conversations;
            $Conversations->conversation_type = '6';
            $Conversations->parent_id = $request->verified_applicationId;
            $Conversations->sender_id = Auth::guard('api')->user()->id;
            $Conversations->reciever_id = $request->verified_schoolId;
            $Conversations->title = 'Title';
            $Conversations->remarks = $request->remarks;
            $Conversations->attachments = $decode_image;
            $Conversations->save();
        }
        $application_form->save();
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $request->verified_applicationId;
        $application_tracking->school_id = $request->verified_schoolId;
        if ($request->status == 1) {
            $application_tracking->application_status_id  = 4;

            $phone = Contact::where('school_id', $request->verified_schoolId)->value('mobile');
            $msg = 'Your application has been verified by District Superintendent of Education and is currently undergoing the next process.';
            $this->TrackingSMS($phone, $msg);
        } elseif ($request->status == 0) {
            $application_tracking->application_status_id  = 8;

            $phone = Contact::where('school_id', $request->verified_schoolId)->value('mobile');
            $msg = 'Your application was rejected by District Superintendent of Education due to some recognised issues. ';
            $this->TrackingSMS($phone, $msg);
        }
        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        return response()->json(['message' => 'Fields Has Been Verified Successfully']);
    }

    public function getApplicationfeildData($status)
    {
        $userData = array();
        $userData = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
            ->leftjoin('application_teacher_data', 'application_data.id', '=', 'application_teacher_data.application_data_id')
            ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
            ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_id')
            ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.application_data_id')
            ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
            ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.application_data_id')
            ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
            ->where('application_data.school_id', Auth::guard('api')->user()->school_id)->where('application_data.status', 1)
            ->select(
                'application_data.id as id',
                'application_data.*',
                'application_data_ext_1.id as application_data_ext_1_id',
                'application_data_ext_1.trainee_qualification as pri_trainee_qualification',
                'application_data_ext_1.teaching_experience as pri_teaching_experience',
                'application_data_ext_1.date_of_appointment as pri_date_of_appointment',
                'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                'application_data.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature1',
                'application_data_ext_1.*',
                'application_teacher_data.id as application_teacher_data_id',
                'application_teacher_data.*',

                'application_data_ext_files.id as application_data_ext_files_id',
                'application_data_ext_files.*',

                'application_data_files.id as application_data_files_id',
                'application_data_files.*',

                'application_data_remarks.id as application_data_remarks_id',
                'application_data_remarks.*',

                'application_data_remarks_ext_1s.id as application_data_remarks_ext_1s_id',
                'application_data_remarks_ext_1s.*',

                'application_data_verifications.id as application_data_verifications_id',
                'application_data_verifications.*',

                'application_data_verification_ext_1s.id as application_data_verification_ext_1s_id',
                'application_data_verification_ext_1s.*'
            )
            ->first();
        // echo "<pre>"; print_r($userData);exit;
        $allclasses = DB::table('fnd_classes')->where('status', 1)->get();
        if (!empty($userData)) {
            if ($userData->details_of_curriculum != null)
                $userData->details_of_curriculum = json_decode($userData->details_of_curriculum, true);

            if ($userData->method_of_inspection != null)
                $userData->method_of_inspection = json_decode($userData->method_of_inspection, true);

            if ($userData->teacher_name != null)
                $userData->teacher_name = json_decode($userData->teacher_name, true);

            if ($userData->teacher_f_h_w_name != null)
                $userData->teacher_f_h_w_name = json_decode($userData->teacher_f_h_w_name, true);

            if ($userData->date_of_birth != null)
                $userData->date_of_birth = json_decode($userData->date_of_birth, true);

            if ($userData->education_qualification != null)
                $userData->education_qualification = json_decode($userData->education_qualification, true);

            if ($userData->trainee_qualification != null)
                $userData->trainee_qualification = json_decode($userData->trainee_qualification, true);

            if ($userData->teaching_experience != null)
                $userData->teaching_experience = json_decode($userData->teaching_experience, true);

            if ($userData->cls_handed_over != null)
                $userData->cls_handed_over = json_decode($userData->cls_handed_over, true);

            if ($userData->date_of_appointment != null)
                $userData->date_of_appointment = json_decode($userData->date_of_appointment, true);

            if ($userData->trained_or_untrained != null)
                $userData->trained_or_untrained = json_decode($userData->trained_or_untrained, true);

            if ($userData->class_names != null)
                $userData->class_names = json_decode($userData->class_names, true);

            if ($userData->no_of_sections != null)
                $userData->no_of_sections = json_decode($userData->no_of_sections, true);

            if ($userData->no_of_students != null)
                $userData->no_of_students = json_decode($userData->no_of_students, true);

            if ($userData->evidence_of_non_proprietary_nature_ex != null)
                $userData->evidence_of_non_proprietary_nature_ex = json_decode($userData->evidence_of_non_proprietary_nature_ex, true);

            if ($userData->last_three_year_tot_income != null)
                $userData->last_three_year_tot_income = json_decode($userData->last_three_year_tot_income, true);

            if ($userData->water_facilities != null)
                $userData->water_facilities = json_decode($userData->water_facilities, true);

            if ($userData->cleanliness != null)
                $userData->cleanliness = json_decode($userData->cleanliness, true);

            $userData->district = Fnd_value::where('fnd_values.id',  $userData->district)->value('display_value');
            $userData->allclasses = $allclasses;
        }
        // $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
        // ->orderBy('fnd_values.display_value', 'ASC')
        // ->pluck(
        // 'fnd_values.display_value as division_name',
        // 'fnd_values.id as division_id'
        // );
        // $UserDistrict = User::leftjoin('addresses','addresses.user_id','users.id')
        // ->where('addresses.user_id', Auth::guard('api')->user()->id)
        // ->value('addresses.district');
        // $this->data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
        // ->orderBy('fnd_values.display_value', 'ASC')
        // ->pluck(
        // 'fnd_values.display_value as district_name',
        // 'fnd_values.id as district_id'
        // );
        // echo "<pre>";
        // print_r($userData);
        // exit;

        if ($status == "accepted") {
            return response()->json(['user' => $userData]);
        }

        if (!empty($userData) && $userData->application_status == 1) {
            return response()->json(['user' => $userData, 'allclasses' => $allclasses]);
        } elseif (empty($userData)) {
            return response()->json(['user' => $userData, 'allclasses' => $allclasses]);
        } else {
            return response()->json(['user' => $userData]);
        }
    }

    public function ApplicationMeeting(Request $req)
    {
        if (Auth::guard('api')->check()) {

            // return $req;
            $user = Auth::guard('api')->user();

            $date = Date('Y-m-d H:i:s');
            $decode_image = '';
            // $image_name=[];
            $image_name = array();
            if ($req->meeting_document != null) {
                foreach ($req->meeting_document as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('applications\dc_application\meeting'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }

            $application_meetings = new application_meetings;

            $application_meetings->application_id = $req->ApplicationId;
            $application_meetings->school_id = $req->SchoolId;
            $application_meetings->meeting_date = $req->meeting_deadline;
            $application_meetings->meeting_remarks = $req->meeting_remarks;
            $application_meetings->meeting_documents = $decode_image;
            $application_meetings->meeting_status = 1;
            $application_meetings->created_by = $user->id;

            $application_meetings->save();

            $application_tracking = new application_tracking;

            $application_tracking->application_id = $req->ApplicationId;
            $application_tracking->school_id = $req->SchoolId;
            $application_tracking->application_status_id = 5;
            $application_tracking->created_by = $user->id;

            $application_tracking->save();

            $Conversations = new Conversations;

            $Conversations->conversation_type = '1';
            $Conversations->parent_id = $req->ApplicationId;
            $Conversations->sender_id = $user->id;
            $Conversations->reciever_id = $req->SchoolId;
            $Conversations->title = 'Title';
            $Conversations->remarks = $req->meeting_remarks;
            $Conversations->attachments = $decode_image;

            $Conversations->save();

            application_data::where('id', $req->ApplicationId)->update([

                'application_status' => 5,
                'updated_by' => $user->id

            ]);

            return response()->json(['success' => 'Meeting Scheduled Successfully !!!'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function downloadStudentPDF($id)
    {
        $students_data = rte_applicant_students::where('id', $id)->first();
        if (!empty($students_data->referred_school_name) && $students_data->referred_school_name != null)
            $students_data->referred_school_name = json_decode($students_data->referred_school_name, true);

        if (!empty($students_data->referred_school_address) && $students_data->referred_school_address != null)
            $students_data->referred_school_address = json_decode($students_data->referred_school_address, true);

        if (!empty($students_data->distance) && $students_data->distance != null)
            $students_data->distance = json_decode($students_data->distance, true);
        $data = ['students_data' => $students_data];
        // return $data;
        //$pdf = PDF::loadView('pdf_view.rejected_application_PDF', $data);
        // $pdf = PDF::loadView('pdf_view.student_data_PDF', $students_data);
        // $pdf =  base64_encode(file_get_contents('path/to/my.pdf'));
        $pdf = PDF::loadView('pdf_view.student_data_PDF', $data)->setPaper('a4', 'landscape');
        return $pdf->download('Student-Data.pdf');
        return response()->download($pdf);
    }

    function SendMessageToDCDSE(Request $req)
    {
        if (Auth::guard('api')->check()) {

            // $image_name='';
            $image_name = [];
            $decode_image = null;

            $Send_user_district = Address::where('addresses.school_id', Auth::guard('api')->user()->school_id)
                ->value('district');

            $users_id = User::join('addresses', 'addresses.user_id', '=', 'users.id');
            if ($req->send_to == 'dc' || $req->send_to == 'dse') {
                $users_id = $users_id->where('addresses.district', $Send_user_district);
            }
            // return [$users_id];

            $users_id = $users_id->where('users.user_role', $req->send_to)
                ->value('users.id');
            // return [$users_id];

            if ($users_id == null) {
                return response()->json(['warning' => 'reciever id  cannot be empty'], 400);
            } else {

                if ($req->send_file_upload != null) {
                    foreach ($req->send_file_upload as $img) {
                        if (isset($img) && !empty($img)) {
                            $fileName = time() . '.' . $img->getClientOriginalName();
                            $img->move(public_path('/assets/send_school_to_dc_dse_correction/'), $fileName);
                            $image_name[] = $fileName;
                        }
                    }
                    $decode_image = json_encode($image_name, true);
                }

                $Conversations = new Conversations;

                $Conversations->conversation_type = 4;
                $Conversations->parent_id = $req->send_application_id;
                $Conversations->sender_id = Auth::guard('api')->user()->id;
                $Conversations->reciever_id = $users_id;
                $Conversations->title = 'Title';
                $Conversations->remarks = $req->send_from_dsc_to_school;
                $Conversations->attachments = $decode_image;
                $Conversations->save();



                return response()->json(['feedback' => 'Your Message send Successfully'], 200);
            }
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    function SendMessageToSchool(Request $req)
    {
        if (Auth::guard('api')->check()) {
            //     $image_name = [];
            //    $decode_image = '';
            $decode_image = $image_name = [];
            if ($req->send_file_upload != null) {
                // $image_name=[];
                foreach ($req->send_file_upload as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('/assets/send_dse_to_school__correction/'), $fileName);
                        // return $a;
                        $image_name[] = $fileName;
                    }
                }
            }

            $decode_image = json_encode($image_name, true);
            $users_id = User::where('school_id', $req->send_school_id)->value('id');

            $Conversations = new Conversations;

            $Conversations->conversation_type = 4;
            $Conversations->parent_id = $req->send_application_id;
            $Conversations->sender_id = Auth::guard('api')->user()['id'];
            $Conversations->reciever_id = $users_id;
            $Conversations->conversation_date = $req->send_date;
            $Conversations->title = 'Title';
            $Conversations->remarks = $req->send_from_dsc_to_school;
            $Conversations->attachments = $decode_image;
            $Conversations->save();

            return response()->json(['feedback' => 'Message send to School Successfully'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function Application_Payment(Request $req)
    {
        // return $req;

        $application_status = application_data::where('id', $req->applicationId)
            ->select(
                'application_status',
                'id',
                'school_id',
                'school_name',
                'state',
                'division',
                'district',
                'block',
                'pincode',
                'post_office',
                'type_of_school'
            )
            ->first();

        //   return $application_status;                      
        $DEPTID = 'JEPC'; // provided by department fixed
        $DEPRECIEPTHEADCODETID = '020201101010101'; // provided by department fixed
        $DEPOSITERNAME = $application_status->school_name; // dynamic school name
        // $DEPTTRANID = substr($req->application_forn_number, 2) . 'r' . date('YmdHis'); //19 characters only = 010001r210208120203
        $application_id = $req->applicationId;
        $DEPTTRANID = strlen($application_id) == 1 ? '00000' . $application_id : (strlen($application_id) == 2 ? '0000' . $application_id : (strlen($application_id) == 3 ? '000' . $application_id : (strlen($application_id) == 4 ? '00' . $application_id : (strlen($application_id) == 5 ? '0' . $application_id : $application_id))));
        if (strlen($application_id) > 5) {
            return "Department transaction ID limit reached.";
        }
        $DEPTTRANID .= 'r' . date('ymdHis');
        // $DEPTTRANID = substr($req->applicationId, 2) . 'r' . date('YmdHis'); //19 characters only = 000001r210208120203
        // return $DEPTTRANID;
        if ($application_status->type_of_school == '1-5') {
            $AMOUNT = 12500; //it will be either 12500 | 25000

        } elseif ($application_status->type_of_school == '1-8') {

            $AMOUNT = 25000;
        }

        //$AMOUNT = 2;
        $DEPOSITERID = $req->applicationId;
        $PANNO_optional = 'NA'; //NA
        $ADDINFO1_optional = ($application_status->school_id != '') ? $application_status->school_id : 'NA';  //application details in character
        $ADDINFO2_optional = ($application_status->type_of_school != '') ?  $application_status->type_of_school : 'NA';
        $ADDINFO3_optional = ($application_status->school_name != '') ? $application_status->school_name : 'NA';
        $ADDINFO3_optional .= ',' . ($application_status->post_office != '') ? $application_status->post_office : 'NA';
        $ADDINFO3_optional .= ',' . ($application_status->pincode != '') ? $application_status->pincode : 'NA'; //school details in character 2
        $ADDINFO3_optional .= ',' . $req->applicationId . ',' . $application_status->division . ',' . $application_status->district . ',' . $application_status->block; //school details in character 1
        $TREASCODE = $req->Treasury_Code; // will be dynamic
        $IFMSOFFICECODE = $req->ddo; //will be dynamic
        $SECURITYCODE = 'sec1234'; //provided by jeGras fiexd
        $RESPONSE_URL_optional = 'NA'; // NA

        $simple_string = "";
        $simple_string = $DEPTID . "|" . $DEPRECIEPTHEADCODETID . "|" . $DEPOSITERNAME . "|" . $DEPTTRANID . "|" . $AMOUNT . "|" . $DEPOSITERID . "|" . $PANNO_optional . "|" . $ADDINFO1_optional . "|" . $ADDINFO2_optional . "|" . $ADDINFO3_optional . "|" . $TREASCODE . "|" . $IFMSOFFICECODE . "|" . $SECURITYCODE . "|NA"; //$RESPONSE_URL_optional

        // Display the original string
        // echo "Original String: " . $simple_string;

        // Store the cipher method
        $ciphering = "AES-128-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = "key1234";

        // Store the encryption key
        $encryption_key = "key1234";

        $transactions = new Transaction();
        $transactions->dept_id = $DEPTID;
        $transactions->application_id = $DEPOSITERID;
        $transactions->school_id = $application_status->school_id;
        $transactions->receipt_head_code = $DEPRECIEPTHEADCODETID;
        $transactions->dept_tran_id = $DEPTTRANID;
        $transactions->amount = $AMOUNT;
        $transactions->depositor_name = $application_status->school_name;
        $transactions->depositor_id = $DEPOSITERID;
        $transactions->pan_no =  $PANNO_optional;
        $transactions->add_info_1 = $ADDINFO1_optional;
        $transactions->add_info_2 = $ADDINFO2_optional;
        $transactions->add_info_3 = $ADDINFO3_optional;
        $transactions->treas_code = $TREASCODE;
        $transactions->ifms_office_code = $IFMSOFFICECODE;
        $transactions->security_code = $SECURITYCODE;
        $transactions->save();

        // Use openssl_encrypt() function to encrypt the data
        $encryption = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);

        // Display the encrypted string
        // return $application_status;
        return ["enc" => $encryption];
    }

    public function DDO_List($req)
    {

        $DDO = Fnd_ddo_value::select(
            'fnd_ddo_values.id',
            'fnd_ddo_values.ddo_name',
            'fnd_ddo_values.ddo_office',
            'fnd_ddo_values.ddo_code'
        )->where('treasury_id', $req)->get();

        $Treasurys = Fnd_treasury_value::where('id', $req)->value('treasury_code');
        $data = ['DDO' => $DDO, 'Treasurys' => $Treasurys];
        return $data;
    }

    public function applicationSubmitPayment(Request $request)
    {
        $transactions = new Transaction();
        $transactions->dept_id = 'JEPC';
        $transactions->receipt_head_code = '020201101010101';
        $transactions->depositor_name = 'rte application test school'; //alpha numeric
        $transactions->dept_tran_id = '2021020401012369860'; //19 characters only
        $transactions->amount = '10';
        $transactions->depositor_id = '20210101001';
        $transactions->pan_no = 'GLTPK4310R';
        $transactions->add_info_1 = 'school-detail-1';
        $transactions->add_info_2 = 'school-detail-2';
        $transactions->add_info_3 = 'application-detail';
        $transactions->treas_code = 'JSR';
        $transactions->ifms_office_code = 'JSRDAD036';
        $transactions->security_code = 'sec1234';
        $transactions->response_url = 'https://paatham.us/rte-application/';

        $transactions->created_at = date('Y-m-d H:i:s');
        $transactions->save();
        $applicationId = "coming soon";
    }

    public function challanPreview(Request $req)
    {
        // return $req;
        $transaction = Transaction::leftjoin('application_data', 'application_data.id', 'transactions.application_id')->where('application_id', $req->application_id)->first();
        // return $transaction;
        return response()->json(['challan' => $transaction]);
    }
    public function ApplicationPaymentForm(Request $request)
    {
        // return $request;
        $application_id = $request->application_id;
        $application_data = application_data::where('application_data.id', $application_id)->where('application_data.status', 1)
            ->first();
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'

        )->get();


        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return response()->json(['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }

    public function UpdateApplicationFile(Request $request)
    {
        if (Auth::guard('api')->check()) {
            // return $request;
            $field_name = $request->field_name;
            $file_name = $request->file_name;
            $fileInstance = $request->file;
            $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move(public_path('assets/applications/'), $newFileName);

            $image_path = public_path("assets/applications/" . $file_name);

            // return $image_path;
            // if(!File::exists($image_path)){
            //     // abort(404);
            //     return response()->json(['warning'=>'file not found'],400);
            // }else{
            // unlink($image_path);
            // }
            if (file_exists($image_path)) {
                unlink($image_path);
                // return 'yes';
                // dd('File is exists.');
            }

            if ($field_name == 'teacher_educational_qualification_files') {

                $file_arr = application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->value($request->field_name);
            } else {

                $file_arr = application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->value($request->field_name);
            }
            if ($file_arr != null)
                $file_arr = json_decode($file_arr, true);

            foreach ($file_arr as $key => $item) {
                if ($item == $file_name)
                    $file_arr[$key] = $newFileName;
            }
            $file_arr = json_encode($file_arr);
            if ($field_name == 'teacher_educational_qualification_files') {
                application_teacher_data::where('application_teacher_data.application_data_id', $request['teacher_id'])->update([$field_name => $file_arr]);
            } else {
                application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->update([$field_name => $file_arr]);
            }
            return response()->json(['message' => 'File updated successfully', 'File' => $newFileName], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }
    public function RemoveApplicationFile(Request $request)
    {
        // return $request;
        if (Auth::guard('api')->check()) {
            $field_name = $request->field_name;
            $file_name = $request->file_name;
            $image_path = public_path("assets/applications/" . $file_name);
            // return $image_path;
            // if(!File::exists($image_path)){
            //     // abort(404);
            //     return response()->json(['warning'=>'file not found'],400);
            // }    
            // else{

            // unlink($image_path);
            // }

            if (file_exists($image_path)) {
                unlink($image_path);
                // dd('File is exists.');
            } else {
                return response()->json(['warning' => 'file not found'], 400);
            }

            if ($field_name == 'teacher_educational_qualification_files') {

                $file_arr = application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->value($request->field_name);
            } else {

                $file_arr = application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->value($request->field_name);
            }
            if ($file_arr != null)
                $file_arr = json_decode($file_arr, true);

            if (($key = array_search($file_name, $file_arr)) !== false) {
                unset($file_arr[$key]);
            }
            $file_arr = json_encode(array_values($file_arr));

            if ($field_name == 'teacher_educational_qualification_files') {
                application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->update([$field_name => $file_arr]);
            } else {
                application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->update([$field_name => $file_arr]);
            }
            return response()->json(['message' => 'Application file removed Successfully'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function InsertApplicationFile(Request $request)
    {
        if (Auth::guard('api')->check()) {
            $file_arr = array();
            $newFileName = '';
            $field_name = $request->field_name;

            $fileInstance = $request->file;
            foreach ($fileInstance as $img) {
                if (isset($img) && !empty($img)) {
                    $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $img->getClientOriginalExtension();
                    // return $newFileName;
                    $img->move(public_path('assets/applications'), $newFileName);

                    if ($field_name == 'teacher_educational_qualification_files') {

                        $file_arr = application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->value($request->field_name);
                    } else {

                        $file_arr = application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->value($request->field_name);
                    }
                    if ($file_arr != null) {
                        // return $file_arr;
                        $file_arr = json_decode($file_arr, true);
                        array_push($file_arr, $newFileName);
                        $file_arr = json_encode($file_arr);
                    } else {
                        $file_arr_1[] = $newFileName;
                        $file_arr = json_encode($file_arr_1);
                    }
                    if ($field_name == 'teacher_educational_qualification_files') {
                        application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->update([$field_name => $file_arr]);
                    } else {
                        application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->update([$field_name => $file_arr]);
                    }
                    // return [$app];
                }
            }
            return response()->json(['message' => 'application file added successfully', 'File' => $newFileName], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }
    public function UpdateMessageStatus($request)
    {
        if (Auth::guard('api')->check()) {
            $Conversations = Conversations::Where('conversations.parent_id', $request)
                ->where('conversations.conversation_type', 4);

            if (Auth::guard('api')->user()->user_role == 'school')
                $Conversations->where('school_read_status', 0)->update(['school_read_status' => 1]);
            if (Auth::guard('api')->user()->user_role == 'dc')
                $Conversations->where('dc_read_status', 0)->update(['dc_read_status' => 1]);
            if (Auth::guard('api')->user()->user_role == 'dse')
                $Conversations->where('dse_read_status', 0)->update(['dse_read_status' => 1]);
            if (Auth::guard('api')->user()->user_role == 'faa')
                $Conversations->where('faa_read_status', 0)->update(['faa_read_status' => 1]);
            if (Auth::guard('api')->user()->user_role == 'saa')
                $Conversations->where('saa_read_status', 0)->update(['saa_read_status' => 1]);

            return response()->json(['message' => '1']);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    public function DeleteTeacher($request)
    {
        $file_arr = application_teacher_data::where('application_teacher_data.id', $request)->value('teacher_educational_qualification_files');
        $file_arr_1 = json_encode($file_arr);
        if ($file_arr_1 != 'null') {
            $file_arr = json_decode($file_arr, true);
            foreach ($file_arr as $key => $value) {
                $image_path = public_path("applications/" . $value);
                if (file_exists($image_path)) {
                    unlink($image_path);
                }
            }
        }
        if (DB::table('application_teacher_data')->where('id', $request)->exists()) {
            DB::table('application_teacher_data')->where('id', $request)->delete();
            return response()->json(['message' => 'teacher deleted successfully'], 200);
        } else
            return response()->json(['warning' => "teacher doesn't exist"], 400);
    }
    public function Pending_Payment(Request $req)
    {

        $DEPTID = 'JEPC'; // provided by department fixed
        $GRN = $req->grn; // will be dynamic
        $DEPTTRANID  = $req->dept_tran_id; //will be dynamic
        $SECURITYCODE = 'sec1234'; //provided by jeGras fiexd
        $simple_string = "";
        $simple_string = $GRN . "|" . $DEPTID . "|" . $DEPTTRANID . "|" . $SECURITYCODE; //$RESPONSE_URL_optional

        // Display the original string
        // echo "Original String: " . $simple_string;
        // Store the cipher method
        $ciphering = "AES-128-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = "key1234";

        // Store the encryption key
        $encryption_key = "key1234";

        // Use openssl_encrypt() function to encrypt the data
        $encryption = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);
        // Display the encrypted string
        // return $application_status;
        return ["enc" => $encryption];
    }
    public function Challan_Verify_Paid(Request $req)
    {

        $GRN = $req->grn; // will be dynamic
        $AMOUNT  = $req->amount; //will be dynamic
        $SECURITYCODE = 'sec1234'; //provided by jeGras fiexd
        $simple_string = "";
        $simple_string = $GRN . "|" . $AMOUNT . "|" . $SECURITYCODE; //$RESPONSE_URL_optional

        // Display the original string
        // echo "Original String: " . $simple_string;
        // Store the cipher method
        $ciphering = "AES-128-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = "key1234";

        // Store the encryption key
        $encryption_key = "key1234";

        // Use openssl_encrypt() function to encrypt the data
        $encryption = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);
        // Display the encrypted string
        // return $application_status;
        return ["enc" => $encryption];
    }
    public function Challan_Paid_Verify(Request $req)   // for manual entry by DSE accept challan 
    {

        $applicationdataid = $req->applicationdataid; // will be dynamic
        $GRN = $req->grn; // will be dynamic

        application_data::where('id', $applicationdataid)->update([
            'application_status' => 2,
            'updated_by' => '-1'
        ]);

        $school_id = $req->school_id; // will be dynamic

        $application_tracking = new application_tracking;
        $application_tracking->application_id = $applicationdataid;
        $application_tracking->school_id = $school_id;
        $application_tracking->application_status_id = 2;
        $application_tracking->created_by = -1;
        $application_tracking->save();

        Transaction::where('application_id', $applicationdataid)->where('grn', $GRN)->update([
                'status' => 2,
                'pmode' => 'Challan'
            ]);

        return redirect()->back();
    }



    public function AppChallanForm($id)
    {

        $application_data = application_data::where('application_data.id', $id)->where('application_data.status', 1)
            ->first();
        // return $application_data;
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'
        )->get();
        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return view('challan_form', ['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }

    public function AppPaymentForm($id)
    {

        $application_data = application_data::where('application_data.id', $id)->where('application_data.status', 1)
            ->first();
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'
        )->get();
        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return view('payment_application', ['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }

    public function TrackingSMS($phone, $msg)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        $message = $msg;
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $templeteId = 1307161787218600805; // template id
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey, $templeteId);
    }

    //function to send single unicode sms
    public function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey, $templeteId)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key),
            "templateid" => $templeteId
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url_unicode to send single unicode sms
    }

    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }

    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }

    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        echo $result = curl_exec($post); //result from mobile seva server
        curl_close($post);
    }
}
