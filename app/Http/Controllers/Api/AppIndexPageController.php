<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\About_us;

use App\Models\Faqs;
use App\Models\District_email;
use App\Models\Deo_email;
use App\Models\Contact_us;
use App\Models\GuideLine;
use App\Models\Slider;
use App\Models\HonblePerson;
use Auth;
use Carbon\Carbon;
use DB;

class AppIndexPageController extends Controller
{

    function footer()
    {
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        return view('home.footer', $this->data);
        return "Comming Soon";
    }
    function View_Home_Page()
    {
        $this->data['Sliders'] = Slider::where('status', 1)->get();
        $this->data['HonblePersons'] = HonblePerson::where('status', 1)->get();
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        // return $this->data['Slider'];
        // return $this->data['HonblePerson'];
        // return $this->data;
        // footer();
        return view('home.index', $this->data);
        // return  View('footer') ;
        // return "Comming Soon";
    }

    function View_ContactUs(){
        $Contact_us = Contact_us::where('status',1)->first();
        return response()->json(['Contact_us'=>$Contact_us]);
}


    function View_About_Us()
    {
        $data['About_us'] = About_us::select(
            'about_us.id',
            'about_us.text',
            'about_us.image',
            'about_us.name',
            'about_us.designation',
            'about_us.department',
            'about_us.created_by',
        )->first();
        $data['Contact_us'] = Contact_us::where('status', 1)->first();

        return response()->json(['About_us' => $data]);
    }
    function View_FAQs()
    {
        $FAQs = Faqs::where('status',1)->get();
        return response()->json(['FAQs'=>$FAQs]);
    }
    function View_DSE()
    {
   
        $DSE_contact = District_email::where('status',1)->get();
        return response()->json(['DSE_contact'=>$DSE_contact]);
    }
    function View_DEO()
    {
        $DEO_contact = Deo_email::where('status',1)->get();
        return response()->json(['DEO_contact'=>$DEO_contact]);
    }
  

    function ViewAboutUs()
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $About_us = About_us::select(
                    'about_us.id',
                    'about_us.text',
                    'about_us.image',
                    'about_us.name',
                    'about_us.designation',
                    'about_us.department',
                    'about_us.created_by',
                    'about_us.created_at'
                )->first();
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['About_us'=>$About_us]);
    }
    function UpdateAboutUs(Request $req)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $About_us = About_us::find($req->id);

                $About_us->text = $req->text;
                $About_us->name = $req->name;
                $About_us->designation = $req->designation;
                $About_us->department = $req->department;
                $About_us->updated_by = Auth::guard('api')->user()->user_id;
                if (isset($req->image) && ($req->image != '' || $req->image != null)) {
                if ($req->has('image')) {
                    $image = $req->image;
                    $fileName = time() . '.' . $req->image->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jpeg' && $file_extension != 'JPEG') {
                        return response()->json(['warning' => 'This file type is not supported for profile picture, you can only use jpg, jpeg and png files.'], 400);
                    }
                    $req->image->move(public_path('../assets/images'), $fileName);
                    $path = 'assets/images/' . $fileName;
                    $About_us->image = $path;
                    // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
                }
                $About_us->save();
            }
                return response()->json(['About_us' => 'updated successfully'], 200);
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
            // return redirect(['message'=>'inserted successfully'],200)->with('success', 'Update Successfully !!');
        }
        return response()->json(['warning' => 'Your session has timed out'], 401);

    }
    function ViewFAQs()
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                
                $FAQs = Faqs::get();
                
                // return  $FAQs;
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['FAQs'=>$FAQs]);
    }
    function EditFAQs($id)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                
                $FAQs = Faqs::where('id',$id)->first();
                
                // return  $FAQs;
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['FAQs'=>$FAQs]);
    }
    
    
    function UpdateFAQ(Request $req)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                
                $FAQs = Faqs::find($req->id);
                
                $FAQs->heading = $req->heading;
                $FAQs->text = $req->text;
                $FAQs->updated_by = Auth::guard('api')->user()->user_id ;
                
                $FAQs->save();
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=>'FAQs updated successfully'],200);
    }
    
    function AddFAQ(){
        if (Auth::check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

        return view('Add-FAQ');
          } 
    else {
        return redirect('dashboard')->with('warning', 'You are not Authorised!!');
    }
    }
}
    function InsertFAQ(Request $req)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                
                $FAQs = new Faqs;
                
                $FAQs->heading = $req->heading;
                $FAQs->text = $req->text;
                $FAQs->created_by = Auth::guard('api')->user()->user_id ;
                
                $FAQs->save();
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=>'FAQs inserted successfully'],200);
    }
    
    
    function DeleteFAQ($id)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                
                $FAQs = FAQs::find($id);
                             
                if($FAQs->status ==0){

                    $FAQs->status = 1;
                }else{
                    $FAQs->status = 0;

                }
                $FAQs->updated_by = Auth::guard('api')->user()->user_id ;
                
                $FAQs->save();
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=>'FAQs deleted successfully'],200);
    }

    function Add_DSE(){
        if (Auth::check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

        return view('Add-DSE');
          } 
    else {
        return response()->json(['warning'=> 'You are not Authorised!!'],203);
    }
    }
    }
    
    function Add_DEO(){
        if (Auth::check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

        return view('Add-DEO');
          } 
    else {
        return response()->json(['warning'=> 'You are not Authorised!!'],203);
    }
    }
}
    function DSEList()
    {
               if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
            
                $DSE_contact = District_email::get();
             }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);

        }  
        return response()->json(['DSE_contact'=>$DSE_contact],200);
    }
    function Edit_DSE($id)
    {
               if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
            
                $DSE_contact = District_email::where('id',$id)->first();
             }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
          
        return response()->json(['DSE_contact'=>$DSE_contact],200);
    }
    function Update_DSE(Request $req)
    {
               if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                if (District_email::where('email', '=', $req->email)->where('id', '!=', $req->id)->exists()) {
                    return response()->json(['warning'=> 'E-mail Id Already Exist !!!'],400);
                 }else{

               
                $District_email = District_email::find($req->id);
                             
                $District_email->district_id =$req->disrtict;
                $District_email->email = $req->email;
                $District_email->updated_by = Auth::guard('api')->user()->user_id ;
                
                $District_email->save();
             }
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
          
        return response()->json(['message'=>'updated successfully'],200);
    }
    function Insert_DSE(Request $req)
    {
               if (Auth::guard('api')->check()) {
         
            
                if (Auth::guard('api')->user()->user_role == 'state') {
                    if (District_email::where('email', '=', $req->email)->exists()) {
                        return response()->json(['warning'=> 'E-mail Id Already Exist !!!'],400);
                     }else{
                $District_email = new District_email;
                             
                $District_email->district_id =$req->disrtict;
                $District_email->email = $req->email;
                $District_email->created_by = Auth::guard('api')->user()->user_id ;
                
                $District_email->save();
             }
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
          
        return response()->json(['message'=>'Inserted successfully'],200);
    }

    function DeleteDSE($id)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                
                $District_email = District_email::find($id);
                if($District_email->status ==0){

                    $District_email->status = 1;
                }else{
                    $District_email->status = 0;

                }
                $District_email->updated_by = Auth::guard('api')->user()->user_id ;
                
                $District_email->save();
            }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=>'Deleted successfully'],200);
    }

    function DEOList()
    {
               if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
             
                $DEO_contact = Deo_email::get();
             
         
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
          
        return response()->json(['DEO_contact'=>$DEO_contact]);
    }
    return response()->json(['message'=> 'Your session has timed out'],401);
    }

    function Edit_DEO($id)
    {
            if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
            
                $DSE_contact = Deo_email::where('id',$id)->first();
            
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
        return response()->json(['DSE_contact'=> $DSE_contact]);
    }
    return response()->json(['message'=> 'Your session has timed out'],401);
    }
    function Update_DEO(Request $req)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                if (Deo_email::where('email', '=', $req->email)->where('id', '!=', $req->id)->exists()) {
                    return response()->json(['warning' => 'E-mail Id Already Exist !!!'], 400);
                } else {

                    $District_email = Deo_email::find($req->id);

                    $District_email->district_id = $req->disrtict;
                    $District_email->email = $req->email;
                    $District_email->updated_by = Auth::guard('api')->user()->user_id;

                    $District_email->save();
                }
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'updated successfully'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }
    function Insert_DEO(Request $req)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {


                if (Deo_email::where('email', '=', $req->email)->exists()) {
                    // user found
                    return response()->json(['warning' => 'E-mail Id Already Exist !!!'], 400);
                } else {


                    $District_email = new Deo_email;

                    $District_email->district_id = $req->disrtict;
                    $District_email->email = $req->email;
                    $District_email->created_by = Auth::guard('api')->user()->user_id;

                    $District_email->save();
                }
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'Inserted successfully'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    function DeleteDEO($id)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $Deo_email = Deo_email::find($id);
                if ($Deo_email->status == 0) {

                    $Deo_email->status = 1;
                } else {
                    $Deo_email->status = 0;
                }
                $Deo_email->updated_by = Auth::guard('api')->user()->user_id;

                $Deo_email->save();
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'Deleted successfully'], 200);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    function ContactUs(){
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
               $Contact_us = Contact_us::first();
           }
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
        // return $Contact_us;
        return response()->json(['Contact_us'=> $Contact_us]);
    }
    function UpdateContactUs(Request $req)
    {
               if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
               

                $Contact_us = Contact_us::find($req->id);
                             
                $Contact_us->heading =$req->heading;
                $Contact_us->phone =$req->phone;
                $Contact_us->email =$req->email;
                $Contact_us->facebook_link =$req->facebook_link;
                $Contact_us->instagram_link =$req->instagram_link;
                $Contact_us->twitter_link = $req->twitter_link;
                $Contact_us->updated_by = Auth::guard('api')->user()->user_id ;
                
                $Contact_us->save();
                 }
             
        }else{
            return response()->json(['warning'=> 'You are not Authorised!!'],203);
        }  
          
        return response()->json(['success'=> 'Update Successfully !!'],200);
    }

    function GuideLineList()
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $GuideLine_List = GuideLine::get();
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['GuideLineList' => $GuideLine_List]);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }
    function EditGuideLine($id)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $GuideLine = GuideLine::where('id', $id)->first();
                return response()->json(['editGuideLine'=> $GuideLine]);
            } 
            else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    function AddGuideLine()
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

        return response()->json(['Add-GuideLine'=>'added guideline']);
    } 
    else {
        return response()->json(['warning'=> 'You are not Authorised!!'],203);
    }
    }
    return response()->json(['message'=> 'Your session has timed out'],401);
}
    function InsertGuideLine(Request $req)
    {
        // return $req;
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $GuideLine = new GuideLine;

                $GuideLine->heading = $req->heading;
                if ($req->has('document')) {
                    $fileInstanceSA = $req->document;
                    $newFileNameSA = date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                    $fileInstanceSA->move(public_path('assets/Guideline/'), $newFileNameSA);
                    $GuideLine->document = $newFileNameSA;
                }
                $GuideLine->created_by =  Auth::guard('api')->user()->user_id;
                $GuideLine->save();

                return response()->json(['message'=>'GuideLine inserted successfully'],200);
            }
             else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    function UpdateGuideLine(Request $req)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $GuideLine =  GuideLine::find($req->id);

                $GuideLine->heading = $req->heading;
                if (isset($req->document) && ($req->document != '' || $req->document != null)) {
                if ($req->has('document')) {
                    $fileInstanceSA = $req->document;
                    $newFileNameSA = date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                    $fileInstanceSA->move(public_path('assets/Guideline/'), $newFileNameSA);
                    $GuideLine->document = $newFileNameSA;
                }
                $GuideLine->created_by =  Auth::guard('api')->user()->user_id;
                $GuideLine->save();
            }
            }
             else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=>'GuideLine updated successfully'],200);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    function DeleteGuideLine($id)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $GuideLine = GuideLine::find($id);
                if ($GuideLine->status == 0) {

                    $GuideLine->status = 1;
                } else {
                    $GuideLine->status = 0;
                }
                $GuideLine->updated_by = Auth::guard('api')->user()->user_id;

                $GuideLine->save();
            } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=>'GuideLine deleted successfully'],200);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    
    function SliderList(){

        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $Slider = Slider::get();
                return $Slider;
                return response()->json(['Sliders' => $Slider]);

                // return "Comming Soon";
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'Your session has timed out'], 401);
        }
    }
    function EditSlider($id){

        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $Slider = Slider::where('id',$id)->first();
                // return $Slider;
                return response()->json(['editSlider'=> $Slider],200);

        return "Comming Soon";
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function AddSlider(){

        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                return response()->json('Add-Slider');
            } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function InsertSlider(Request $req){

        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
// return $req;
        $Slider = new Slider;

        $Slider->title =  $req->title ;
        // $Slider->title =  $req->title ;
        $Slider->text =  $req->text ;
       
        if ($req->has('document')) {
            $document = $req->document;
            $fileName = time() . '.' . $req->document->getClientOriginalName();
            $file_extension = \File::extension($fileName);
            if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jpeg' && $file_extension != 'JPEG') {
                return response()->json(['warning'=> 'This file type is not supported for profile picture, you can only use jpg, jpeg and png files.'],400);
            }
            $req->document->move(public_path('../assets/images'), $fileName);
            $path = $fileName;
            $Slider->image = $path;
            // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
        }
        $Slider->created_by = Auth::guard('api')->user()->id;
        $Slider->save();
        // return $Slider;
        return response()->json(['message'=>'inserted successfully'],200);

        // return "Comming Soon";
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function UpdateSlider(Request $request){

        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
// return $request;
        $Slider = Slider::find($request->id);
        $Slider->title =  $request->title ;
        // $Slider->title =  $request->title ;
        $Slider->text =  $request->text ;
       
        if ($request->has('document')) {
            $document = $request->document;
            $fileName = time() . '.' . $request->document->getClientOriginalName();
            $file_extension = \File::extension($fileName);
            if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jpeg' && $file_extension != 'JPEG') {
                return response()->json(['warning'=>'this file type is not supported for profile picture, you can only use jpg, jpeg and png files.'],400);
            }
            $request->document->move(public_path('../assets/images'), $fileName);
            $path = $fileName;
            $Slider->image = $path;
            // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
        }
        $Slider->updated_by = Auth::guard('api')->user()->id;
        $Slider->save();
        return response()->json(['message'=>'updated successfully'],200);

        // return "Comming Soon";
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function DeleteSlider($id){
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

                $Slider = Slider::find($id);
                if ($Slider->status == 0) {

                    $Slider->status = 1;
                } else {
                    $Slider->status = 0;
                }
                $Slider->updated_by = Auth::guard('api')->user()->user_id;

                $Slider->save();
            } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
        return response()->json(['message'=>'deleted successfully'],200);
    }



    function HonblePersonList(){
          if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {           
                $HonblePerson = HonblePerson::get();
           return response()->json(["Honble-Person"=>$HonblePerson]);
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }    
    }
    function EditHonblePerson($id){
          if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $HonblePerson = HonblePerson::where('id', $id)->first();
               return response()->json(['HonblePerson'=> $HonblePerson]);
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function AddHonblePerson(){
          if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {

               return "Comming Soon";
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function InsertHonblePerson(Request $request){
          if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $HonblePerson = new HonblePerson;
                // $request->validate([
                //     'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:100',
                // ]);
                if ($request->has('image')) {
                    $fileInstanceSA = $request->image;
                    $newFileNameSA = date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                    $fileInstanceSA->move(public_path('../assets/'), $newFileNameSA);
                    $HonblePerson->image = $newFileNameSA;
                }
                $HonblePerson->name = $request->name;
                $HonblePerson->designation = $request->designation;
                $HonblePerson->created_by =  Auth::guard('api')->user()->user_id;
                $HonblePerson->save();
               return response()->json(['message'=>'Inserted successfully'],200);
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function UpdateHonblePerson(Request $request){
          if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $request->validate([
                    'image' => 'mimes:jpeg,png,jpg,gif,svg|max:100',
                ]);
                $HonblePerson = HonblePerson::find($request->id);
                $HonblePerson->name =  $request->name;
                // $HonblePerson->title =  $request->title ;
                $HonblePerson->designation =  $request->designation;
                if ($request->has('image')) {
                    $image = $request->image;
                    $fileName = time() . '.' . $request->image->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $request->image->move(public_path('../assets/'), $fileName);
                    $path = $fileName;
                    $HonblePerson->image = $path;
                    // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
                }
                $HonblePerson->updated_by = Auth::guard('api')->user()->id;
                $HonblePerson->save();
               return response()->json(['message'=>'Updated successfully']);
         } else {
                return response()->json(['warning'=> 'You are not Authorised!!'],203);
            }
        }
    }
    function DeleteHonblePerson($id)
    {
        if (Auth::guard('api')->check()) {

            if (Auth::guard('api')->user()->user_role == 'state') {
                // return 'amar';

                $HonblePerson = HonblePerson::find($id);
                if ($HonblePerson->status == 0) {
                    $HonblePerson->status = 1;
                } else {
                    $HonblePerson->status = 0;
                }
                $HonblePerson->updated_by = Auth::guard('api')->user()->user_id;
                $HonblePerson->save();

                return response()->json(['message'=>'Deleted successfully'],200);
            } else {
                return response()->json(['warning' => 'You are not Authorised!!'], 203);
            }
        }
        return response()->json(['warning' => 'You session is timed out!!'], 401);
    }
}
