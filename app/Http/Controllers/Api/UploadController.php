<?php
 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Validator,Redirect,Response,File;
use App\Document;
// use Validator;
 
 
class UploadController extends Controller
 
{
 
    /**
 
     * Create a new controller instance.
 
     *
 
     * @return void
 
     */
 
    public function imagesUpload()
 
    {
 
        return view('image');
 
    }
 
 
 
    /**
 
     * Create a new controller instance.
 
     *
 
     * @return void
 
     */
 
    // public function imagesUploadPost(Request $request)
 
    // {
        // return['amar'];
        // request()->validate([
 
        //     'uploadFile' => 'required',
 
        // ]);
        
 
 
    //     foreach ($request->file('uploadFile') as $key => $value) {
    //        // return['amar'];
    //         $imageName = time(). $key . '.' . $value->getClientOriginalExtension();
 
    //         $value->move(public_path('assets/images'), $imageName);
 
    //     }
 
 
 
    //     return response()->json(['success'=>'Images Uploaded Successfully.']);
 
    // }

    public function store(Request $request)
{
    if(!$request->hasFile('fileName')) {
        return response()->json(['message'=>'upload_file_not_found'], 400);
    }
 
    $allowedfileExtension=['pdf','jpg','png'];
    $files = $request->file('fileName'); 
    $errors = [];
 
    foreach ($files as $file) {      
 
        $extension = $file->getClientOriginalExtension();
 
        $check = in_array($extension,$allowedfileExtension);
 
        if($check) {
            foreach($request->fileName as $mediaFiles) {
 
                $path = $mediaFiles->store('public/images');
                $name = $mediaFiles->getClientOriginalName();
      
                //store image file into directory and db
                $save = new Image();
                $save->title = $name;
                $save->path = $path;
                $save->save();
            }
        } else {
            return response()->json(['message'=>'invalid_file_format'], 422);
        }
 
        return response()->json(['message'=>'file_uploaded'], 200);
 
    }
}
 
}