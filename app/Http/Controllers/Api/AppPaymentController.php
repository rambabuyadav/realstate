<?php





namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Fnd_value_set;

use App\Models\Fnd_value;

use App\Models\application_data;

use App\Models\application_data_ext_1;

use App\Models\application_statuses;

use App\Models\Payment;

use App\Models\Private_school;

use App\Models\User;

use App\Exports\PaymentExport;

use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;



use DB;

use Auth;








class AppPaymentController extends Controller

{

    // myformAjax



    public $data = [];

    public function listPayment(Request $request)

    {
        if (Auth::guard('api')->check())
         {
            if (Auth::guard('api')->user()->user_role == 'dse') {

                // return($request);

                $data['block_list'] = User::leftjoin('addresses', 'addresses.user_id', 'users.id')

                    ->leftjoin('fnd_values', 'fnd_values.parent_value_id', 'addresses.district')

                    ->where('addresses.user_id', Auth::guard('api')->user()->id)

                    ->orderBy('fnd_values.display_value', 'ASC')

                    ->pluck(

                        'fnd_values.display_value as block_name',

                        'fnd_values.id as block_id'

                    );





                // return( $block_list);

                $data['school_list'] = Private_school::where('status', 1)

                    ->orderBy('school_name', 'ASC')

                    ->pluck('school_name', 'id as school_id');

                // return( $schoolList);

                $search_text = $request->search_text;

                $block = $request->Block;

                $school = $request->schoolList;

                $paymentType = $request->paymentType;

                $contactNumber = $request->contact_number;

                $blockName = null;

                $schoolName = null;

                if (isset($request->Block) && $request->Block != null) {

                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }

                if (isset($request->schoolList) && $request->schoolList != null) {

                    $schoolName = Private_school::where('private_schools.id', $school)->value('school_name');
                }

                $data['adv_data'] = ['search_text' => $search_text, 'block_id' => $block, 'blockName' => $blockName, 'schoolName' => $schoolName, 'school' => $school, 'paymentType' => $paymentType, 'contactNumber' => $contactNumber];

                // "<pre>";

                // print_r(Auth::guard('api')->user()->user_role);

                // exit;

                $amount = 0;

                $data['payments'] = Payment::leftjoin('application_data', 'application_data.school_id', '=', 'payments.school_id')

                    ->leftjoin('users', 'users.id', 'payments.created_by')

                    ->leftjoin('private_schools', 'private_schools.id', 'payments.school_id')

                    ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')

                    ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')

                    ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')

                    ->select(

                        'payments.*',

                        'users.name as created_by_name',

                        'private_schools.board as board_type',

                        'private_schools.website as school_website',

                        'application_data.type_of_school',

                        'application_data.email as private_school_email',

                        'application_data.school_name',

                        'application_data.phone_with_std_code as phone_no',

                        'application_data.school_chairman_phone_number as mobile_no',

                        'application_data.school_chairman_name as contact_name',

                        'application_data.village_city',

                        'application_data.post_office',

                        'application_data.panchayat',

                        'fnd_division.display_value as division',

                        'fnd_district.display_value as district',

                        'fnd_block.display_value as block',

                    )

                    ->where('application_data.status', 1)

                    ->where('payments.status', 1);

                // return $search_text;

                if (isset($request->search_text) && $request->search_text != null) {

                    $data['payments'] =  $data['payments']->where(function ($query) use ($search_text) {

                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')

                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')

                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')

                            ->orWhere('payments.t_id', 'like', '%' .  $search_text . '%')

                            ->orWhere('application_data.school_chairman_name', 'like', '%' .  $search_text . '%')

                            ->orWhere('application_data.post_office', 'like', '%' .  $search_text . '%')

                            ->orWhere('application_data.panchayat', 'like', '%' .  $search_text . '%')

                            ->orWhere('application_data.village_city', 'like', '%' .  $search_text . '%');
                    });
                }

                // $data['payments'] = $data['payments']->get();

                // return( $data['payments']); 

                if (isset($request->Block) && $request->Block != null) {

                    $data['payments'] =  $data['payments']->where('application_data.block',  $request->Block);
                }

                if (isset($request->payment_amount) && $request->payment_amount != null) {

                    $data['payments'] =  $data['payments']->where('payments.payment_amount',  $request->payment_amount);
                }

                // $data['payments'] = $data['payments']->get();

                // return( $data['payments']); 

                if (isset($request->schoolList) && $request->schoolList != null) {

                    $data['payments'] =  $data['payments']->where('application_data.school_name', $request->schoolList);
                }

                if (isset($request->paymentType) && $request->paymentType != null) {

                    $data['payments'] =  $data['payments']->where('payments.paid_method', $request->paymentType);
                }

                // $data['payments'] = $data['payments']->get();

                // return( $data['payments']);  

                if (isset($request->contact_number) && $request->contact_number != null) {

                    $data['payments'] =  $data['payments']->where('application_data.phone_with_std_code', $request->contact_number)

                        ->orWhere('application_data.school_chairman_phone_number', $request->contact_number);
                }

                $data['payments'] = $data['payments']->get();

                foreach ($data['payments'] as $key => $value) {

                    $amount = $value->payment_amount + $amount;
                }

                $data['total_amount'] = $amount;

                // return( $data);

                if ($request->excel == 'excel') {

                    $params = ['data' => $data['payments'], 'total' => $data['total_amount'], 'view' => 'excel_view.payments_excel'];

                    $filename = 'payments.xlsx';

                    return Excel::download(new PaymentExport($params), $filename);
                } else {

                    return response()->json(['payment'=> $data],200);
                }
            } 
            else
            {
                return response()->json(['message'=> 'You are not Authorised!!'],401);
            }
        }
        return response()->json(['message'=> 'You are not Authorised!!'],401);

    }
}

