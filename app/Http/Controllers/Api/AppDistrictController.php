<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

// use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Contact;
use App\Models\Address;
use App\Models\Person_detail;
use App\Models\Fnd_value;
use App\Models\Fnd_value_set;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;



class AppDistrictController extends Controller
{
    public function show()
    {

        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $Users = User::paginate(25);
                $Users = User::join('addresses', 'users.id', '=', 'addresses.user_id')
                    ->join('contacts', 'contacts.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.name',
                        'users.email',
                        'users.user_role',
                        'users.created_at',
                        'addresses.school_address as user_address',
                        'addresses.created_by',
                        'contacts.mobile as contact'
                    )->where('users.status', '=', '1')->paginate(25);

                $Districts = Fnd_value::select(
                    'fnd_values.id',
                    'fnd_values.display_value as value_set_name'
                )->where('fnd_values.value_set_id', 1)
                    ->get();
                // $Users = User::all();
                // $Users = User::all();
                $UserData = ['User' => $Users, 'District' => $Districts];
                return response()->json(['district', compact('UserData')]);
            } else {
                return response()->json(['message', 'You are not Authorised!!']);
            }
        }
        // return ['amar'];
        return response()->json(['message', 'You are not Authorised!!']);
    }
    public function insert(Request $request)
    {

        $path = null;
        $users = new User;
        $contact = new Contact;
        $address = new Address;
        $person_detail = new Person_detail;


        $max = User::max('id');

        $max = $max + 1;

        if ($request->has('photo')) {
            $image = $request->photo;
            $fileName = time() . '.' . $request->photo->getClientOriginalName();
            $request->photo->move(public_path('assets/images'), $fileName);
            $path = 'public/images/' . $fileName;
        }
        $pass =  \Hash::make($request->password);
        $users->name = $request->user_name;
        $users->email = $request->email;
        $users->password =  $pass;
        $users->profile_photo_path = $path;
        $users->user_role = $person_detail->user_type = $request->usertype;


        // Get the currently authenticated user...
        $user = Auth::guard('api')->user();
        $sName = $contact->created_by = $person_detail->created_by = $address->created_by = $user->id;

        $contact->user_id = $max;
        $person_detail->user_id = $max;
        $address->user_id = $max;
        $person_detail->full_name = $request->full_name;
        $person_detail->user_name = $request->user_name;
        $person_detail->user_address = $request->address1;
        $contact->mobile = $request->phonenumber1;
        $contact->school_email = $request->email;
        $contact->created_ip = $address->created_ip = $person_detail->created_ip  = $ip = $_SERVER['REMOTE_ADDR'];
        $address->school_address = $request->address1;
        $address->school_id  = $users->school_id = $contact->school_id =  '1';
        $address->division = $request->division;
        $address->district = $request->district;
        $address->school_state = 'Jharkhand';



        // $contact->phone_2 = $request->phonenumber2;
        //  $contact->whatsapp = $request->whatsupnumber;
        // $person_detail->middle_name = $request->middle_name;
        // $person_detail->last_name = $request->last_name;
        // $person_detail->gender = $request->gender;
        // $person_detail->DOB =  $request->dob;
        // $address->address2 = $request->address2;
        // $address->village = $request->village;
        // $address->block = $request->block;
        // $address->po = $request->postoffice;
        // $address->ps = $request->policestation;
        // $address->cluster = $request->cluster;
        // $address->school_city = $request->city;
        // $address->pincode = $request->pincode;


        // if ($request->has('photo')) {
        //     $image=$request->photo;
        //     $fileName = time().'.'.$request->photo;
        //     $request->photo->store('public/images');
        //     $users->profile_photo_path=$fileName;
        //     }

        $users->save();

        $contact->save();

        $address->save();
        $person_detail->save();

        return response()->json(['success', 'User Inserted Successfully !!!'],200);
    }


    public function edit(Request $req)
    {
        //$id = Crypt::decrypt($req);

        $DistrictName = $BolckName = ['0' => ''];

        $userData = User::leftjoin('addresses', 'users.id', 'addresses.user_id')
            ->leftjoin('contacts', 'contacts.user_id', 'users.id')
            ->leftjoin('person_details', 'person_details.user_id', 'users.id')
            ->where('users.id', $req->id)
            ->first();

        // return $userData;
        $Block =  $userData['block'];
        $District =  $userData['district'];


        $BolckName = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.id', '=', $Block)
            ->get();

        $DistrictName = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.id', '=', $District)
            ->get();


        $Districts = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value as value_set_name'
        )->where('fnd_values.value_set_id', 1)
            ->get();
        // $Users = User::all();
        //    return view('addDSC',compact('Districts'));
        // $ArrUserData = ['UserData'=>$userData,'BlockName'=>$BolckName,'DistrictName'=>$DistrictName];
        // return $ArrUserData;
        // return (['UserData'=>$userData,'BlockName'=>$BolckName,'DistrictName'=>$DistrictName,'Districts'=>$Districts]);

        return response()->json(['user' => $userData, 'BlockName' => $BolckName, 'DistrictName' => $DistrictName, 'Districts' => $Districts]);
    }

    public function updateData($id,Request $req)
    {

        // return [$id];
        if (Auth::guard('api')->check()) {

            if (Auth::guard('api')->user()->user_role == 'state') {
                // return $req;
                // Get the currently authenticated user...
                $user = Auth::guard('api')->user();

                if ($req->has('photo')) {
                    $image = $req->photo;
                    $fileName = time() . '.' . $req->photo->getClientOriginalName();
                    $req->photo->move(public_path('assets/images'), $fileName);
                    $path = 'public/images/' . $fileName;
                } else {
                    $path = $req->photo_path;
                }
                //
                

                $users = User::where('users.id', $req->id)->first();
                $users->name = $req->user_name;
                $user->email = $req->email;
                $user->profile_photo_path = $path;
                $user->save();

                $address = Address::where('addresses.user_id', $req->id)->first();
                $address->school_address = $req->address1;
                $address->division = $req->division;
                $address->district = $req->district;
                $address->school_state = 'Jharkhand';
                $address->updated_ip = $_SERVER['REMOTE_ADDR'];
                $address->updated_by = $user->id;
                $address->save();


                $contact = Contact::where('contacts.user_id', $req->id)->first();
                $contact->mobile = $req->phonenumber1;
                $contact->school_email = $req->email;
                $contact->updated_ip = $_SERVER['REMOTE_ADDR'];
                $contact->updated_by = $req->phonenumber1;
                $contact->mobile = $user->id;
                $contact->save();

                $person_detail = Person_detail::where('person_details.user_id', $req->id)->first();
                $person_detail->full_name = $req->full_name;
                $person_detail->user_name = $req->user_name;
                $person_detail->user_address = $req->address1;
                $person_detail->updated_ip = $_SERVER['REMOTE_ADDR'];
                $person_detail->updated_by = $user->id;
                $person_detail->save();

                // $users = User::where('users.id', $req->id)->update([
                //     'name' => $req->user_name,
                //     'email' => $req->email,
                //     'profile_photo_path' => $path
                //     // 'user_role' => $req->usertype

                // ]);

                // $address = Address::where('addresses.user_id', $req->id)->update([
                //     'school_address' => $req->address1,
                //     // 'address2' => $req->address2,
                //     // 'village' => $req->village,
                //     // 'block' => $req->block,
                //     // 'po' => $req->postoffice,
                //     // 'ps' => $req->policestation,
                //     // 'cluster' => $req->cluster,
                //     // 'school_city' => $req->city,
                //     'division' => $req->division,
                //     'district' => $req->district,
                //     'school_state' => 'Jharkhand',
                //     'updated_ip' => $_SERVER['REMOTE_ADDR'],
                //     'updated_by' => $user->id

                // ]);

                // $contact = Contact::where('contacts.user_id', $req->id)->update([
                //     'mobile' => $req->phonenumber1,
                //     'school_email' => $req->email,
                //     // 'phone_2' => $req->phonenumber2,
                //     // 'whatsapp' => $req->whatsupnumber,
                //     'updated_ip' => $_SERVER['REMOTE_ADDR'],
                //     'updated_by' => $user->id


                // ]);

                // $person_detail = Person_detail::where('person_details.user_id', $req->id)->update([
                //     'full_name' => $req->full_name,
                //     'user_name' => $req->user_name,
                //     'user_address' => $req->address1,
                //     // 'gender' => $req->gender,
                //     // 'DOB' =>  $req->dob,
                //     'updated_ip' => $_SERVER['REMOTE_ADDR'],
                //     'updated_by' => $user->id
                // ]);
                //return ['amar'];
                //'success', 'User Update Successfully !!!'
                return response()->json([$users, $address, $contact, $person_detail]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],203);
            }
        }
        return response()->json(['message'=> 'You are not Authorised!!'],203);
    }


    public function deleteData(Request $req)
    {
        // return [$req->id];
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $id = $req['user_id'];
                //return [$req->id];

                //$users = User::where('users.id', $id)->first();
              

                //$users = Contact::where('contacts.user_id', $id)->first();




                User::where('users.id', $id)->update([
                    'status' => '0'
                ]);
            //    return [$req->id];
                $users = Contact::where('contacts.user_id', $id)->update([
                    'status' => '0'
                ]);
                $users = Address::where('addresses.user_id', $id)->update([
                    'status' => '0'
                ]);
                $users = Person_detail::where('person_details.user_id', $id)->update([
                    'status' => '0'
                ]);

                return response()->json(['warning', 'User Deleted Successfully !!!']);
            } else {
                return response()->json(['message', 'You are not Authorised!!']);
            }
        }
        return response()->json(['message', 'You are not Authorised!!']);
    }
}
