<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\User;
use App\Models\Contact;
use App\Models\application_data;
use App\Models\application_teacher_data;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Str;



class AppLoginController extends Controller
{
    public function login(Request $request) 
    {
        //$responseArray = [];
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $responseArray['Role'] = $user->user_role;
            $responseArray['Id'] = $user->id;
            $responseArray['School_id'] = $user->school_id;
            $responseArray['Name'] = $user->name;
            $responseArray['Email'] = $user->email;
            $responseArray['District'] = user::where('id', $user->id)
                ->select('users.school_id')
                ->first();
            $responseArray['Application_data_id'] = user::where('id', $user->id)
                ->select('users.school_id')
                ->first();
            // return  $responseArray['Application_data_id'];
            $responseArray['District'] = application_data::where('school_id',  $responseArray['District']->school_id)
                ->leftjoin('fnd_values', 'application_data.district', '=', 'fnd_values.id')
                ->pluck('fnd_values.display_value as District')
                ->first();
            $responseArray['Teacher_id'] = application_teacher_data::where('school_id', $responseArray['Application_data_id']->school_id )->pluck('id')
                ->first();
            $responseArray['Application_data_id'] = application_data::where('school_id',  $responseArray['Application_data_id']->school_id)
                // ->leftjoin('fnd_values', 'application_data.district', '=', 'fnd_values.id')
                ->pluck('id')
                ->first();
            // return  $responseArray['District'];
            
            $responseArray['Contact'] = Contact::where('user_id', $user->id)->pluck('mobile')->first();
            

            $responseArray['token'] = $user->createToken('MyApp')->accessToken;

            // $responseArray['refresh'] = Http::asForm()->post('https://paatham.us/rte-application/api/applogin/oauth/token', [
            //     'grant_type' => 'refresh_token',
            //     'refresh_token' => 'the-refresh-token',
            //     'client_id' => 'client-id',
            //     'client_secret' => 'client-secret',
            //     'scope' => '',
            // ]);
            
            // return $response->json();


            return response()->json(['status' => True, 'message' => "token generated successfully", 'success' => $responseArray], 200);
        }

        //elseif(Auth::attempt(['username'=>$request->username,'password'=>$request->password])){
        //     $user = Auth::user();

        //     $responseArray['token'] = $user->createToken('MyApp')->accessToken;
        //     $responseArray['name'] = $user->name;

        //     return response()->json(['success'=> $responseArray],200);

        // }

        else {
            return response()->json(['error' => 'Invalid User'], 203);
        }
    }
}
