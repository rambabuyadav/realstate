<?php



namespace App\Http\Controllers\Api;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB;

use Auth;

use App\Models\Fnd_value;

use App\Models\Fnd_value_set;

use App\Models\fnd_settings;



class AppDivisionDistrictBlockController extends Controller

{





    function index()

    {



        return view('fndSettings');

    }



    // Division add,edit,delete start

    function divisionList()

    {

        

        $Division = Fnd_value::select(

            'fnd_values.id',

            'fnd_values.display_value',

            'fnd_values.created_at'

        )->where('fnd_values.value_set_id', '=', 1)

            ->where('fnd_values.status', '=', 1)

            ->orderBy('fnd_values.display_value', 'ASC')

            ->get();

        return response()->json(['divisionList'=> $Division]);

    }



    public function addDivision()

    {

        return view('addDivision');

    }

  



    public function saveDivisionData(Request $request)

    {     

        $Fnd_value_division = new Fnd_value;

        $Fnd_value_division->value_set_id = 1;

        $Fnd_value_division->display_value = $request->division;

        $Fnd_value_division->enabled_flag = 1;

        $Fnd_value_division->status = 1;

        $Fnd_value_division->created_by = Auth::user()->id;

        $Fnd_value_division->created_at = date('Y-m-d H:i:s');        

        $Fnd_value_division->save();

        

        $Fnd_value_set_division =new Fnd_value_set;

        $Fnd_value_set_division->value_set_name = $request->division;

        $Fnd_value_set_division->parent_value_set_id = 1;

        $Fnd_value_set_division->parent_value_id = $Fnd_value_division->id;

        $Fnd_value_set_division->status = 1;

        $Fnd_value_set_division->created_by = Auth::user()->id;

        $Fnd_value_set_division->created_at = date('Y-m-d H:i:s');

        $Fnd_value_set_division->save();

        

        return redirect('divisionList')->with('success', 'Division Add Successfully !!!');

    }



    public function editDivision($id)

    {



        $Division = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.parent_value_id', '=', 'fnd_values.id')

        ->select(

            'fnd_value_sets.id as value_sets_id',

            'fnd_value_sets.value_set_name',

            'fnd_values.id as value_id',

            'fnd_values.display_value'

        )->where('fnd_values.id', $id)->first();



        return view('editDivision', ['Division' => $Division]);

    }





    public function updateDivisionData(Request $req)

    {      

         

        $Fnd_value = Fnd_value::find($req->division_id);

        $Fnd_value->display_value = $req->division;

        $Fnd_value->save(); 



        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id',$Fnd_value->value_set_id)->where('parent_value_id',$Fnd_value->id)->first();

        $Fnd_value_set->value_set_name = $req->division;

        $Fnd_value_set->save();



        return redirect('divisionList')->with('success', 'Division Update Successfully !!!');

    }



    public function deleteDivisionData($id)

    {

        

        $Fnd_value = Fnd_value::find($id);

        $Fnd_value->status = 0;

        $Fnd_value->save(); 



        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id',$Fnd_value->value_set_id)->where('parent_value_id',$Fnd_value->id)->first();

        $Fnd_value_set->status = 0;

        $Fnd_value_set->save();



        return redirect('divisionList')->with('warning', 'Division Deleted Successfully !!!');

    }

    // Division add,edit,delete end

    // District add,edit,delete start



    

    function districtList()

    {

        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');



        $District = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')

            ->select(

                'fnd_values.id as value_id',

                'fnd_values.display_value',

                'fnd_value_sets.id as value_sets_id',

                'fnd_value_sets.value_set_name',

            )->whereIn('fnd_values.parent_value_id', $division_ids)

            ->where('fnd_values.status', 1)

            ->orderBy('fnd_value_sets.value_set_name', 'ASC')

            ->orderBy('fnd_values.display_value', 'ASC')

            ->get();

                // echo"<pre>";print_r($District);exit;

        return response()->json(['districtList'=> $District]);

    }



    public function deleteDistrictData($id)

    {

        $Fnd_value = Fnd_value::find($id);

        $Fnd_value->status = 0;

        $Fnd_value->save(); 



        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id',$Fnd_value->value_set_id)->where('parent_value_id',$Fnd_value->id)->first();

        $Fnd_value_set->status = 0;

        $Fnd_value_set->save();



        return redirect('districtList')->with('warning', 'District Deleted Successfully !!!');

    }



    public function editDistrict($id)

    {



        $District_id = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')

        ->select(

            'fnd_value_sets.id as value_sets_id',

            'fnd_value_sets.value_set_name',

            'fnd_values.id as value_id',

            'fnd_values.display_value'

        )->where('fnd_values.id', '=', $id)->first();



        return view('editDistrict', ['District_id' => $District_id]);

    }



    public function updateDistrictData(Request $req)

    {

       



        $Fnd_value = Fnd_value::find($req->district_id);

        $Fnd_value->display_value = $req->district;

        $Fnd_value->save(); 



        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id',$Fnd_value->value_set_id)->where('parent_value_id',$Fnd_value->id)->first();

        $Fnd_value_set->value_set_name = $req->district;

        $Fnd_value_set->save();



        return redirect('districtList')->with('success', 'District Update Successfully !!!');

    }



    public function addDistrict()

    {

       

        $Divisions = Fnd_value::select(

            'fnd_values.id',

            'fnd_values.display_value',

        )->where('fnd_values.value_set_id', 1)

            ->where('fnd_values.status', 1)

            ->orderBy('fnd_values.display_value', 'ASC')

            ->get();



        return view('addDistrict', ['Divisions' => $Divisions]);

    }



    public function saveDistrictData(Request $request)

    {



        $Fnd_value_district = new Fnd_value;

        $Fnd_value_district->value_set_id = Fnd_value_set::where('parent_value_id', $request->division)->value('id');

        $Fnd_value_district->parent_value_id = $request->division; // fnd_values primary key

        $Fnd_value_district->display_value = $request->district;

        $Fnd_value_district->enabled_flag = 1;

        $Fnd_value_district->status = 1;

        $Fnd_value_district->created_by = Auth::user()->id;

        $Fnd_value_district->created_at = date('Y-m-d H:i:s');

        $Fnd_value_district->save();



        $Fnd_value_set_district =new Fnd_value_set;

        $Fnd_value_set_district->value_set_name = $request->district;

        $Fnd_value_set_district->parent_value_id = $Fnd_value_district->id;

        $Fnd_value_set_district->parent_value_set_id = $Fnd_value_district->value_set_id;

        $Fnd_value_set_district->status = 1;

        $Fnd_value_set_district->created_by = Auth::user()->id;

        $Fnd_value_set_district->created_at = date('Y-m-d H:i:s');

        $Fnd_value_set_district->save();











        return redirect('districtList')->with('success', 'District Add Successfully !!!');

    }

    // District add,edit,delete end

    // Block add,edit,delete start



    function blockList()

    {

        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');



        $AllListData = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')

        ->leftjoin('fnd_values as block_list_fnd', 'block_list_fnd.parent_value_id', '=', 'fnd_values.id')

           

            ->select(

                'fnd_values.id as value_id',

                'fnd_values.display_value',

                'fnd_value_sets.id as value_sets_id',

                'fnd_value_sets.value_set_name',

                'block_list_fnd.display_value as block_name',

                'block_list_fnd.id as block_id',

                 'block_list_fnd.created_at as block_created_at'

                 

            )->whereIn('fnd_values.parent_value_id', $division_ids)



           // ->where('block_list_fnd.parent_value_id', '=', 'fnd_values.id')

            ->where('block_list_fnd.status', 1)

           ->orderBy('fnd_value_sets.value_set_name', 'ASC')

           ->orderBy('fnd_values.display_value', 'ASC')

            ->get();

        return response()->json(['blockList'=> $AllListData]);

    }



    public function deleteBlockData($id)

    {

        

        $block_Fnd_value = Fnd_value::find($id);

        $block_Fnd_value->status = 0;

        $block_Fnd_value->save(); 



        $block_Fnd_value_set =  Fnd_value_set::where('parent_value_set_id',$block_Fnd_value->value_set_id)->where('parent_value_id',$block_Fnd_value->parent_value_id)->first();

        $block_Fnd_value_set->status = 0;

        $block_Fnd_value_set->save();



        return redirect('blockList')->with('warning', 'Block Deleted Successfully !!!');

    }



    public function editBlock($id)

    {



        $Block_id = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')

           

            ->select(

                'fnd_values.id as value_id',

                'fnd_values.display_value',

                'fnd_value_sets.id as value_sets_id',

                'fnd_value_sets.value_set_name'

                 

            )

            ->where('fnd_values.id', $id)->first();

           

        return view('editBlock', ['Block_id' => $Block_id]);

    }



    public function updateBlockData(Request $req)

    {

        $Fnd_value = Fnd_value::find($req->block_id);

        $Fnd_value->display_value = $req->block;

        $Fnd_value->save(); 



        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id',$Fnd_value->value_set_id)->where('parent_value_id',$Fnd_value->parent_value_id)->first();

        $Fnd_value_set->value_set_name = $req->block;

        $Fnd_value_set->save();



        return redirect('blockList')->with('success', 'Update Block Successfully !!!');

    }



    public function  getdistrict($id)

    {



        $Districts = Fnd_value::select(

            'fnd_values.id',

            'fnd_values.display_value'

        )->where('fnd_values.parent_value_id', '=', $id)

            ->get();

        return json_encode($Districts);





        // $Users = User::all();

        return view('addBlock', compact('Districts'));

    }



    public function addBlock()

    {

       

        $Divisions = Fnd_value::select(

            'fnd_values.id',

            'fnd_values.display_value',

        )->where('fnd_values.value_set_id', 1)

            ->where('fnd_values.status', 1)

            ->orderBy('fnd_values.display_value', 'ASC')

            ->get();



        return view('addBlock', ['Divisions' => $Divisions]);

    }



    public function saveBlockData(Request $request)

    {



        $Fnd_value_block = new Fnd_value;

        $Fnd_value_block->value_set_id = Fnd_value_set::where('parent_value_set_id', Fnd_value::where('id', $request->district)->value('value_set_id'))->where('parent_value_id', $request->district)->value('id');

        $Fnd_value_block->parent_value_id = $request->district; // fnd_values primary key

        $Fnd_value_block->display_value = $request->block;

        $Fnd_value_block->enabled_flag = 1;

        $Fnd_value_block->status = 1;

        $Fnd_value_block->created_by = Auth::user()->id;

        $Fnd_value_block->created_at = date('Y-m-d H:i:s');

        $Fnd_value_block->save();      



        return redirect('blockList')->with('success', 'Block Add Successfully !!!');

    }

    // Block add,edit,delete end





    public function addEditFndsettings()

    {

       

        $fnd_settingsget = fnd_settings::select(

            'fnd_settings.maximum_number_of_day_for_varification_by_dse',

            'fnd_settings.maximum_number_of_day_for_final_desicion_by_dc',

            'fnd_settings.maximum_number_of_day_to_respond_after_meeting_held_by_dc',

            'fnd_settings.maximum_number_of_day_to_take_action_by_faa',

            'fnd_settings.maximum_number_of_day_to_take_action_by_saa',

            'fnd_settings.maximum_number_of_day_to_appeal_by_school',

            'fnd_settings.max_no_day_for_appeal_to_saa_after_faa_appeal_rejections_school',

        )->first();





            return view('addEditFndsettings', ['fnd_settingsget' => $fnd_settingsget]);

       

    }

    public function saveFndsettings(Request $request)

    {

        

        $Fndsetting = new fnd_settings;

        $Fndsetting->maximum_number_of_day_for_varification_by_dse = $request->maximum_number_of_day_for_varification_by_dse; // fnd_values primary key

        $Fndsetting->maximum_number_of_day_for_final_desicion_by_dc = $request->maximum_number_of_day_for_final_desicion_by_dc;

        $Fndsetting->maximum_number_of_day_to_respond_after_meeting_held_by_dc = $request->maximum_number_of_day_to_respond_after_meeting_held_by_dc;

        $Fndsetting->maximum_number_of_day_to_take_action_by_faa = $request->maximum_number_of_day_to_take_action_by_faa;

        $Fndsetting->maximum_number_of_day_to_take_action_by_saa = $request->maximum_number_of_day_to_take_action_by_saa;

        $Fndsetting->maximum_number_of_day_to_appeal_by_school = $request->maximum_number_of_day_to_appeal_by_school;

        $Fndsetting->max_no_day_for_appeal_to_saa_after_faa_appeal_rejections_school = $request->max_no_day_for_appeal_to_saa_after_faa_appeal_rejections_school;

        $Fndsetting->created_by = Auth::user()->id;

        $Fndsetting->created_at = date('Y-m-d H:i:s');

        $Fndsetting->save();      



        return redirect('addEditFndsettings')->with('success', 'Add Successfully !!!');

        

    }

    

}

