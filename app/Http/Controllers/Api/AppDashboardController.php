<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Private_school;
use App\Models\Person_detail;
use App\Models\Contact;
use App\Models\Address;
use App\Models\application_data;
use App\Models\application_data_ext_1;
use App\Models\Fnd_value;
use App\Models\Appeal;

use Illuminate\Support\Facades\Crypt;

use App\Models\application_meetings;
use App\Models\application_tracking;
use Carbon\Carbon;

use DB;
use Auth;
use Hash;


class AppDashboardController extends Controller
{

    public function index()
    {
        $wordCount = $wordCountAll = $wordCountAcceptedByAllUsers = $wordCountA = $wordCountByDSC = $wordCountR =  $wordCountP = $wordCountAcpt = $AllDistrict = $Arr_Block_Filter = '';

        if (Auth::guard('api')->user()->user_role == 'saa') {


            $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
                ->select(
                    'application_data.school_name',
                    'addresses.block',
                    'addresses.district',
                    'appeals.created_at',
                    'appeals.appeal_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block_name',
                    'appeals.appeal_status AS status_name'

                )
                ->where('appeals.appellate_authority_type', '2')
                ->where('appeals.school_id', '!=', '1')
                ->whereIn('appeals.appeal_status', [1, 2, 3])
                ->orderBy('appeals.created_at', 'DESC')->get();
            // return $AllDistrict;
            $CountAllDistrict = $AllDistrict->count();

            $all_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->where('appeals.appellate_authority_type', '2')
                ->where('appeals.school_id', '!=', '1')
                ->whereIn('appeals.appeal_status', [1, 2, 3])->count();


            $pending_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->where('appeals.appellate_authority_type', '2')
                ->where('appeals.school_id', '!=', '1')
                ->where('appeals.appeal_status', '1')->count();


            $accepted_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->where('appeals.appellate_authority_type', '2')
                ->where('appeals.school_id', '!=', '1')
                ->where('appeals.appeal_status', '2')->count();


            $rejected_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->where('appeals.appellate_authority_type', '2')
                ->where('appeals.school_id', '!=', '1')
                ->where('appeals.appeal_status', '3')->count();

            $District = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.value_set_id', '=', 1)->get();
            // return $District;
            $DistrictCount = $District->count();

            $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();

            for ($i = 0; $i < $DistrictCount; $i++) {
                $AllAppeal = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->where('application_data.division', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '2')
                    ->where('appeals.school_id', '!=', '1')
                    ->where('addresses.division', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                $CountAllApplication = $AllAppeal->count();
                array_push($Total_Appeal, $CountAllApplication);

                $AllPending = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->where('application_data.division', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '2')
                    ->where('appeals.school_id', '!=', '1')
                    ->where('addresses.division', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->where('appeals.appeal_status', 1)->get();
                $CountAllPending = $AllPending->count();
                array_push($Total_Pending, $CountAllPending);

                $AllAppealVerified = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->where('application_data.division', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '2')
                    ->where('appeals.school_id', '!=', '1')
                    ->where('addresses.division', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->where('appeals.appeal_status', 2)->get();
                $CountAllVerified = $AllAppealVerified->count();
                array_push($Total_Verification, $CountAllVerified);


                $AllAppealReject = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->where('application_data.division', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '2')
                    ->where('appeals.school_id', '!=', '1')
                    ->where('addresses.division', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->where('appeals.appeal_status', 3)->get();
                $CountAllReject = $AllAppealReject->count();
                array_push($Total_Rejection, $CountAllReject);
            }

            $Arr_District_Filter = [
                'DistrictCount' => $DistrictCount,
                'District' => $District,
                'Total_Appeal' => $Total_Appeal,
                'Total_Verification' => $Total_Verification,
                'Total_Rejection' => $Total_Rejection,
                'Total_Pending' => $Total_Pending
            ];


            $wordCount = [
                'AllDistrict' => $AllDistrict,
                'allappeals' => $all_appeals,
                'pendingappeals' => $pending_appeals,
                'acceptedappeals' => $accepted_appeals,
                'rejectedAppeal' => $rejected_appeals,
                'Arr_District_Filter' => $Arr_District_Filter
            ];
            return response()->json(['role' => 'saa', 'data' => $wordCount]);
        }

        if (Auth::guard('api')->user()->user_role == 'faa') {

            $UserDivision = DB::table('addresses')
                ->where('addresses.user_id', Auth::guard('api')->user()->id)
                ->select('addresses.division')
                ->get();

            $User_Division = $UserDivision[0]->division;

            $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                ->where('application_data.division', $User_Division)
                ->where('appeals.appellate_authority_type', '1')
                ->where('addresses.division', '=', $User_Division)
                ->select(
                    'application_data.school_name',
                    'addresses.block',
                    'addresses.district',
                    'appeals.created_at',
                    'appeals.appeal_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block_name',
                    'appeals.appeal_status AS status_name'

                )
                ->whereIn('appeals.appeal_status', [1, 2, 3])
                ->orderBy('appeals.created_at', 'DESC')->get();
            // return $AllDistrict;
            $CountAllDistrict = $AllDistrict->count();

            $all_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->where('application_data.division', $User_Division)
                ->where('appeals.appellate_authority_type', '1')
                ->where('addresses.division', '=', $User_Division)
                ->whereIn('appeals.appeal_status', [1, 2, 3])->count();


            $pending_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->where('application_data.division', $User_Division)
                ->where('appeals.appellate_authority_type', '1')
                ->where('addresses.division', '=', $User_Division)
                ->where('appeals.appeal_status', '1')->count();


            $accepted_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->where('application_data.division', $User_Division)
                ->where('appeals.appellate_authority_type', '1')
                ->where('addresses.division', '=', $User_Division)
                ->where('appeals.appeal_status', '2')->count();


            $rejected_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                ->where('application_data.division', $User_Division)
                ->where('appeals.appellate_authority_type', '1')
                ->where('addresses.division', '=', $User_Division)
                ->where('appeals.appeal_status', '3')->count();


            $District = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.parent_value_id', '=', $User_Division)->get();
            // return $District;
            $DistrictCount = $District->count();

            $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();

            for ($i = 0; $i < $DistrictCount; $i++) {
                $AllAppeal = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->where('application_data.district', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '1')
                    ->where('addresses.district', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                $CountAllApplication = $AllAppeal->count();
                array_push($Total_Appeal, $CountAllApplication);

                $AllPending = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->where('application_data.district', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '1')
                    ->where('addresses.district', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->where('appeals.appeal_status', 1)->get();
                $CountAllPending = $AllPending->count();
                array_push($Total_Pending, $CountAllPending);

                $AllAppealVerified = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->where('application_data.district', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '1')
                    ->where('addresses.district', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->where('appeals.appeal_status', 2)->get();
                $CountAllVerified = $AllAppealVerified->count();
                array_push($Total_Verification, $CountAllVerified);


                $AllAppealReject = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                    ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                    ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                    ->where('application_data.district', $District[$i]->id)
                    ->where('appeals.appellate_authority_type', '1')
                    ->where('addresses.district', '=', $District[$i]->id)
                    ->where('appeals.status', '1')
                    ->where('appeals.appeal_status', 3)->get();
                $CountAllReject = $AllAppealReject->count();
                array_push($Total_Rejection, $CountAllReject);
            }

            $Arr_District_Filter = [
                'DistrictCount' => $DistrictCount,
                'District' => $District,
                'Total_Appeal' => $Total_Appeal,
                'Total_Verification' => $Total_Verification,
                'Total_Rejection' => $Total_Rejection,
                'Total_Pending' => $Total_Pending
            ];

            $wordCount = [
                'AllDistrict' => $AllDistrict,
                'allappeals' => $all_appeals,
                'pendingappeals' => $pending_appeals,
                'acceptedappeals' => $accepted_appeals,
                'rejectedAppeal' => $rejected_appeals,
                'Arr_District_Filter' => $Arr_District_Filter
            ];
            return response()->json(['role' => 'faa', 'data' => $wordCount]);
        }

        if (Auth::guard('api')->user()->user_role == 'dse') {

            $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending =  array();
            $UserDistrict = DB::table('addresses')
                ->where('addresses.user_id', Auth::guard('api')->user()->id)
                ->select('addresses.district')
                ->get();
            $User_District = $UserDistrict[0]->district;
            $Block = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.parent_value_id', '=', $User_District)->get();
            $BlockCount = $Block->count();

            $AllDistrict = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->limit($BlockCount - 1)->get();

            $Alldata = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->where('application_status', '>', '1')->get();
            $wordCountAll = $Alldata->count();

            $AlldataByDSC = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->whereIn('application_status', [2, 3])->get();
            $wordCountByDSC = $AlldataByDSC->count();

            $AlldataAccept = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->where('application_status', 4)->get();
            $wordCountAcpt = $AlldataAccept->count();

            $AlldataReject = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->where('application_status', 8)->get();
            $wordCountR = $AlldataReject->count();

            for ($i = 0; $i < $BlockCount; $i++) {
                $AllApplication = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', '>', '1')->get();
                $CountAllApplication = $AllApplication->count();
                array_push($Total_Application, $CountAllApplication);

                $AllReject = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', 8)->get();

                $CountAllReject = $AllReject->count();
                array_push($Total_Rejection, $CountAllReject);

                $AllVerified = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', 4)->get();
                $CountAllVerified = $AllVerified->count();
                array_push($Total_Verification, $CountAllVerified);


                $AllPending = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->whereIn('application_status', [2, 3])->get();
                $CountAllPending = $AllPending->count();
                array_push($Total_Pending, $CountAllPending);
            }
            $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Approved' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];

            // return $Arr_Block_Filter;
            $All_Meeting_Data = DB::table('application_data')
                ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                ->select(
                    'application_data.school_name',
                    'application_data.created_at',
                    'application_data.block',
                    'application_data.district',
                    'application_data.application_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block',
                    'status.status_name AS status_name',
                    'application_meetings.meeting_date'
                )
                ->where('application_data.application_status', '5')
                ->where('application_data.district', $User_District)
                ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                ->where('application_data.status', '1')->limit($BlockCount - 1)
                ->get();


            $wordCount = [
                'All'                 => $wordCountAll,
                'Active'              => $wordCountA,
                'Pending'       => $wordCountByDSC,
                'Reject'              => $wordCountR,
                // 'Pending'              => $wordCountP,
                'Accept'              => $wordCountAcpt,
                'AllDistrict'         => $AllDistrict,
                'Arr_Block_Filter'    => $Arr_Block_Filter,
                'All_Meeting_Data' => $All_Meeting_Data
            ];
            return response()->json(['role' => 'dse', 'data' => $wordCount]);
        }

        if (Auth::guard('api')->user()->user_role == 'dc') {

            $UserDistrict = DB::table('addresses')
                ->where('addresses.user_id', Auth::guard('api')->user()->id)
                ->select('addresses.district')
                ->get();

            $User_District = $UserDistrict[0]->district;
            $Block = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.parent_value_id', '=', $User_District)
                ->get();
            $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending = array();

            $BlockCount = $Block->count();
            // return $BlockCount;
            $Alldata = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->limit($BlockCount - 1)
                ->where('application_status', '>', '1')
                ->orderBy('created_at', 'ASC')->get();
            $wordCountAll = $Alldata->count();

            $AllVerified = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                // ->where('application_status', '4')->get();
                ->where('application_status', 6)->get();
            $AllVerifiedCount = $AllVerified->count();

            $AllPending = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                // ->where('application_status', '4')->get();
                ->where('application_status', 4)->get();
            $AllPendingCount = $AllPending->count();

            $Meeting = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->where('application_status', '5')->get();
            $MeetingCount = $Meeting->count();

            $Certified = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                ->where('application_status', '6')
                ->limit($BlockCount - 1)
                ->orderBy('created_at', 'DESC')->get();
            $CertifiedCount = $Certified->count();

            $Reject = DB::table('application_data')
                ->where('district', $User_District)
                ->where('status', '1')
                // ->where('application_status', '7')->get();
                ->where('application_status', 7)->get();
            $RejectCount = $Reject->count();

            $All_Meeting_Data = DB::table('application_data')
                ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                ->select(
                    'application_data.school_name',
                    'application_data.created_at',
                    'application_data.block',
                    'application_data.district',
                    'application_data.application_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block',
                    'status.status_name AS status_name',
                    'application_meetings.meeting_date'
                )
                ->where('application_data.application_status', '5')
                ->where('application_data.district', $User_District)
                ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                ->where('application_data.status', '1')->limit($BlockCount - 1)
                ->get();

            for ($i = 0; $i < $BlockCount; $i++) {

                $AllApplication = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', '>', '1')->get();
                $CountAllApplication = $AllApplication->count();
                array_push($Total_Application, $CountAllApplication);

                $AllReject = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', 7)->get();
                // ->where('application_status', '7')->get();
                $CountAllReject = $AllReject->count();
                array_push($Total_Rejection, $CountAllReject);

                $AllVerified = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', 6)->get();
                // ->where('application_status', '6')->get();
                $CountAllVerified = $AllVerified->count();
                array_push($Total_Verification, $CountAllVerified);

                $AllPending = DB::table('application_data')
                    ->where('district', $User_District)
                    ->where('status', '1')
                    ->where('block', $Block[$i]->id)
                    ->where('application_status', '4')->get();
                // ->whereIn('application_status', [2, 3, 4, 9, 12])->get();
                $CountAllPending = $AllPending->count();
                array_push($Total_Pending, $CountAllPending);
            }
            $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];

            // return($Arr_Block_Filter);


            $wordCount = [
                'All'           => $wordCountAll,
                'AllDistrict' => $Alldata,
                'AllVerifiedCount' => $AllVerifiedCount,
                'AllPendingCount' => $AllPendingCount,
                'MeetingCount' => $MeetingCount,
                'CertifiedCount' => $CertifiedCount,
                'RejectCount' => $RejectCount,
                'AllCertified' => $Certified,
                'Arr_Block_Filter' => $Arr_Block_Filter,
                'All_Meeting_Data' => $All_Meeting_Data

            ];
            return response()->json(['role' => 'dc', 'data' => $wordCount]);
        }

        if (Auth::guard('api')->user()->user_role == 'state') {
            $Total_Pending  = $Total_Approved = $Total_Application = $Total_Verification = $Total_Rejection = array();
            $UserDivision = DB::table('addresses')
                ->where('addresses.user_id', Auth::guard('api')->user()->id)
                ->select('addresses.division')
                ->get();
            $User_Division = $UserDivision[0]->division;

            $Division = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.value_set_id', '1')
            ->where('fnd_values.status',1)
                ->get();

            $DivisionCount = $Division->count();

            $Alldata = DB::table('application_data')->where('status', '1')->where('application_data.application_status', '>', 1)->get();
            $wordCountAll = $Alldata->count();

            $AcceptedByAllUsers = DB::table('application_data')->where('status', '1')->where('application_status', [4, 6, 10, 13])->get();
            $wordCountAcceptedByAllUsers = $AcceptedByAllUsers->count();

            $AlldataAccept = DB::table('application_data')->where('status', '1')->where('application_status', '6')->get();
            $wordCountAcpt = $AlldataAccept->count();

            $AlldataReject = DB::table('application_data')->where('status', '1')->whereIn('application_status', [7, 8, 11, 14])->get();
            $wordCountR = $AlldataReject->count();

            $AllDetails = DB::table('application_data')
                ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                ->select(
                    'application_data.school_name',
                    'application_data.created_at',
                    'application_data.block',
                    'application_data.district',
                    'application_data.application_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block',
                    'status.status_name AS status_name'
                )->where('application_data.status', '1')
                ->where('application_data.application_status', '>', 1)
                ->limit($DivisionCount)
                ->orderBy('created_at', 'desc')->get();

            $AllCertified = DB::table('application_data')->where('status', '1')->where('division', $User_Division)->where('application_status', '6')->get();
            $CountAllCertified = $AllCertified->count();

            for ($i = 0; $i < $DivisionCount; $i++) {

                $AllApplication = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_data.application_status', '>', 1)->get();
                $CountAllApplication = $AllApplication->count();
                array_push($Total_Application, $CountAllApplication);

                $AllReject = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->whereIn('application_status', [7, 8, 11, 14])->get();
                $CountAllReject = $AllReject->count();
                array_push($Total_Rejection, $CountAllReject);

                $AllPending = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->wherein('application_status', [2, 3, 4, 9, 12])->get();
                $CountAllPending = $AllPending->count();
                array_push($Total_Pending, $CountAllPending);

                $AllApproved = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_status', 6)->get();
                $CountAllApproved = $AllApproved->count();
                array_push($Total_Approved, $CountAllApproved);
            }
            $Arr_Block_Filter = ['BlockCount' => $DivisionCount, 'Block' => $Division, 'Total_Application' => $Total_Application, 'Total_Pending' => $Total_Pending, 'Total_Approved' => $Total_Approved, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection];

            $wordCount = [
                'All' => $wordCountAll,
                'Active' => $wordCountA,
                'VerifiedByAllUser' => $wordCountAcceptedByAllUsers,
                'Reject' => $wordCountR,
                'Pending' => $wordCountP,
                'Accept' => $wordCountAcpt,
                'AllCertified' => $AllCertified,
                'AllDetails' => $AllDetails,
                'Arr_Block_Filter' => $Arr_Block_Filter
            ];
            return response()->json(['role' => 'state', 'data' => $wordCount]);
        }

        if (Auth::guard('api')->user()->user_role == 'school') {
            // return 'true';
            $Accepted_Fields = 0;
            $Rejected_Fields = 0;
            $pending_Fields = 0;
            $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
            foreach ($alltable_col as $key => $value) {
                //    echo $value;
                if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at") {
                    if (DB::table('application_data_verifications')->where('school_id', Auth::guard('api')->user()->school_id)->where($value, 2)->exists()) {
                        $Accepted_Fields++;
                    }
                    if (DB::table('application_data_verifications')->where('school_id', Auth::guard('api')->user()->school_id)->where($value, 3)->exists()) {
                        $Rejected_Fields++;
                    }
                    if (DB::table('application_data_verifications')->where('school_id', Auth::guard('api')->user()->school_id)->where($value, 1)->exists()) {
                        $pending_Fields++;
                    }
                }
            }
            $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
            // echo $alltable_col;
            foreach ($alltable_ext_col as $key => $value) {
                if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {

                    if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::guard('api')->user()->school_id)->where($value, 2)->exists()) {
                        $Accepted_Fields++;
                    }
                    if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::guard('api')->user()->school_id)->where($value, 3)->exists()) {
                        $Rejected_Fields++;
                    }
                    if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::guard('api')->user()->school_id)->where($value, 1)->exists()) {
                        $pending_Fields++;
                    }
                }
            }

            $UserDistrict = DB::table('private_schools')
                ->where('id', Auth::guard('api')->user()->school_id)
                ->value('district');

            $User_District = $UserDistrict;
            $Block = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.parent_value_id', '=', $User_District)
                ->get();


            $Application_Status = application_data::where('application_data.school_id', Auth::guard('api')->user()->school_id)
                ->value('application_data.application_status');


            // $acceptedted_feild = DB::table('application_data_verifications')
            // ->select( DB::raw('count(school_name_vrfy,udise_code_vrfy) as accepted_count, school_name_vrfy') )
            // ->where('application_data_verifications.school_id', Auth::user()->school_id)
            // ->groupBy('school_name_vrfy')
            // ->get();
            // return $acceptedted_feild;

            $Application_Tracking = DB::table('application_trackings')
                ->Join('application_statuses', 'application_trackings.application_status_id', '=', 'application_statuses.id')
                ->select(
                    'application_trackings.application_id',
                    'application_trackings.school_id',
                    'application_trackings.application_status_id',
                    'application_trackings.created_at',
                    'application_trackings.created_by',
                    'application_statuses.status_name AS status_name'
                )
                ->where('application_trackings.school_id', Auth::guard('api')->user()->school_id)
                ->get();

            $CountApplication_Tracking = $Application_Tracking->count();


            $feedback = DB::table('application_data')
                ->select(
                    'application_data.id',
                    'application_data.created_by',
                    'application_data.school_name',
                    'application_data.status',
                    'application_data.feedback_file_upload',
                    'application_data.feedback_from_dsc_to_school',
                    'application_data.verification_date'
                )
                ->where('application_data.school_id', Auth::guard('api')->user()->school_id)->where('application_data.status', 1)->get();

            $All_Meeting_Data_school = DB::table('application_meetings')
                ->Join('application_data', 'application_data.id', '=', 'application_meetings.application_id')
                ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                ->Join('users', 'users.id', '=', 'application_meetings.created_by')
                // ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                ->select(
                    'application_meetings.meeting_date',
                    'application_meetings.meeting_remarks',
                    'application_data.school_name',
                    'users.name',
                    'application_meetings.created_at',
                    'application_meetings.meeting_text',
                    'application_meetings.meeting_remarks',
                    'application_meetings.meeting_documents',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block',
                    // 'status.status_name AS status_name'
                )
                // ->where('application_data.application_status', '5')
                ->where('application_meetings.school_id', Auth::guard('api')->user()->school_id)
                ->get();

            foreach ($All_Meeting_Data_school as  $Meeting_Data_school) {
                $Meeting_Data_school->meeting_documents = json_decode($Meeting_Data_school->meeting_documents);
            }

            // if ($All_Meeting_Data_school->meeting_documents != null)
            // $All_Meeting_Data_school->meeting_documents = json_decode($All_Meeting_Data_school->meeting_documents, true);

            if ($Application_Status == 1) {
                $Accepted_Fields = 0;
                $Rejected_Fields = 0;
                $pending_Fields = 0;
            }

            $wordCount = [
                'feedback' => $feedback,
                'Application_Tracking' => $Application_Tracking,
                'Accepted_Fields' => $Accepted_Fields,
                'Rejected_Fields' => $Rejected_Fields,
                'pending_Fields' => $pending_Fields,
                'All_Meeting_Data_school' => $All_Meeting_Data_school,
                'Application_Status' => $Application_Status

            ];
            return response()->json(['role' => 'school', 'data' => $wordCount]);
        }

        return response()->json(['message' => 'Your session has timed out'], 401);

        // return response()->json($wordCount);
    }
    public function SchoolList()
    {
        $Alldata = DB::table('private_schools')
            ->join('addresses', 'private_schools.id', '=', 'addresses.school_id')
            ->join('contacts', 'private_schools.id', '=', 'contacts.school_id')
            ->select('private_schools.school_name', 'addresses.*', 'contacts.mobile')
            ->where('addresses.user_id', Auth::guard('api')->user()->id)
            ->get();
        // dd($Alldata);
        return response()->json(['school' => $Alldata]);
    }
    public function EditDataShow(Request $req)
    {
        //return $req->id;
        // $id = $req['user_id'];
        if (Auth::guard('api')->check()) {

            $userData = DB::table('users')
                ->where('users.id', $req->id)
                ->get();
            // return $userData;

            if ($userData == "") {
                return response()->json(['message' => "not found", 'userData' => $userData]);
            } else {
                return response()->json(['userData' => $userData]);
            }
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    function editData(Request $req)
    {
        // $pass=bcrypt('12345678');
        // return $req;
                if (Auth::guard('api')->check()) {

        if ($req->submit == 'Remove') {
            User::where('users.id', $req->id)->update(['profile_photo_path' => '']);
        } elseif ($req->upload == 'upload') {

            if ($req->has('profile_pic_upload')) {
                $image = $req->profile_pic_upload;
                $fileName = time() . '.' . $req->profile_pic_upload->getClientOriginalName();
                $file_extension = \File::extension($fileName);
                if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jpeg' && $file_extension != 'JPEG') {
                    return response()->json(['warning'=> 'This file type is not supported for profile picture, you can only use jpg, jpeg and png files.'],400);
                }
                $req->profile_pic_upload->move(public_path('assets/images/user_image'), $fileName);
                $path = 'public/assets/images/user_image/'. $fileName;
                User::where('users.id', Auth::guard('api')->user()->id)->update(['profile_photo_path' => $path]);
            }
        // } else {
        //     $req->validate([
        //         'password' => [
        //             'required', 'string', 'min:8', 'regex:/[a-z]/',
        //             'regex:/[A-Z]/',
        //             'regex:/[a-z]/',
        //             'regex:/[0-9]/',
        //             'regex:/[@$!%*#?&]/'
        //         ],
        //     ]);
        //     if (Hash::check($req->password_confirmation, User::where('id', $req->id)->value('password'))) {
        //         if (isset($req->password) && $req->password != '') {
        //             $hashed_pass = \Hash::make($req->password);
        //             User::where('id', $req->id)->update(['password' => $hashed_pass]);
        //         }
        //     }
            // return ($req->profile_pic_upload);
            // if ($req->has('profile_pic_upload')) {
            //     $image = $req->profile_pic_upload;
            //     $fileName = time() . '.' . $req->profile_pic_upload->getClientOriginalName();
            //     $file_extension = \File::extension($fileName);
            //     if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
            //         return response()->json(['warning'=> 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.'],400);
            //     }
            //     $req->profile_pic_upload->move(public_path('assets/images/user_image'), $fileName);
            //     $path = 'public/assets/images/user_image/' . $fileName;
            //     User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
            // }
        }

        return response()->json(['success'=> 'Profile Has Been Updated Successfully'],200);
    }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }

    // function editData(Request $req)
    // {
    //     // $pass=bcrypt('12345678');
    //     //return $req->id;
    //     if (Auth::guard('api')->check()) {

    //         if ($req->submit == 'Remove') {
    //             User::where('id', $req->id)->update(['profile_photo_path' => '']);
    //         } elseif ($req->upload == 'upload') {

    //             if ($req->has('profile_pic_upload')) {
    //                 $image = $req->profile_pic_upload;
    //                 $fileName = time() . '.' . $req->profile_pic_upload->getClientOriginalName();
    //                 $req->profile_pic_upload->move(public_path('images/user_image'), $fileName);
    //                 $path = 'public/images/user_image/' . $fileName;
    //                 User::where('id', Auth::guard('api')->user()->id)->update(['profile_photo_path' => $path]);
    //             }
    //         } else {
    //             // $req->validate([
    //             //     'password' => ['required','string','min:8','regex:/[a-z]/',
    //             //     'regex:/[A-Z]/',
    //             //     'regex:/[a-z]/',
    //             //     'regex:/[0-9]/',
    //             //     'regex:/[@$!%*#?&]/'],
    //             //]);

    //             //return $req->id;

    //             if (Hash::check($req->password_confirmation, User::where('id', $req->id)->value('password'))) {
    //                 if (isset($req->password) && $req->password != '') {
    //                     $hashed_pass = \Hash::make($req->password);
    //                     User::where('id', $req->id)->update(['password' => $hashed_pass]);
    //                 }
    //             }
    //             // return ($req->profile_pic_upload);
    //             if ($req->has('profile_pic_upload')) {
    //                 $image = $req->profile_pic_upload;
    //                 $fileName = time() . '.' . $req->profile_pic_upload->getClientOriginalName();
    //                 $req->profile_pic_upload->move(public_path('images/user_image'), $fileName);
    //                 $path = 'public/images/user_image/' . $fileName;
    //                 User::where('id', $req->id)->update(['profile_photo_path' => $path]);
    //             }
    //         }

    //         return response()->json(['success' => 'Profile Has Been Updated Successfully']);
    //     }
    //     return response()->json(['message' => 'Your session has timed out'], 401);
    // }

    function divisionList()
    {

        $Division = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value',
            'fnd_values.created_at'
        )->where('fnd_values.value_set_id', '=', 1)
            ->where('fnd_values.status', '=', 1)
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        return response()->json(['divisionList', compact('Division')]);
    }

    function districtList()
    {
        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');

        $District = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')
            ->select(
                'fnd_values.id as value_id',
                'fnd_values.display_value',
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name',
            )->whereIn('fnd_values.parent_value_id', $division_ids)
            ->where('fnd_values.status', 1)
            ->orderBy('fnd_value_sets.value_set_name', 'ASC')
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        // echo"<pre>";print_r($District);exit;
        return response()->json(['districtList', compact('District')]);
    }
    function blockList()
    {
        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');

        $AllListData = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')
            ->leftjoin('fnd_values as block_list_fnd', 'block_list_fnd.parent_value_id', '=', 'fnd_values.id')

            ->select(
                'fnd_values.id as value_id',
                'fnd_values.display_value',
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name',
                'block_list_fnd.display_value as block_name',
                'block_list_fnd.id as block_id',
                'block_list_fnd.created_at as block_created_at'

            )->whereIn('fnd_values.parent_value_id', $division_ids)

            // ->where('block_list_fnd.parent_value_id', '=', 'fnd_values.id')
            ->where('block_list_fnd.status', 1)
            ->orderBy('fnd_value_sets.value_set_name', 'ASC')
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        return [$AllListData];
        return response()->json(['blockList', compact('AllListData')]);
    }

    function ChangePassword(Request $req)
    {
        if (Auth::guard('api')->check()) {

            $hashed_pass = \Hash::make($req->new_password);
            $old_password_enter = $req->old_password;

            $old_password = User::where('id', Auth::guard('api')->user()->id)->value('password');

            if (Hash::check($old_password_enter, $old_password)) {

                User::where('id', Auth::guard('api')->user()->id)->update(['password' => $hashed_pass]);
                return response()->json(['status' => True, 'Message' => 'Password Has Been Updated Successfully'], 200);
            } else {

                return response()->json(['warning' => 'Old Password Not Matched'], 400);
            }
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }
}
