<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Fnd_value_set;
use App\Models\Fnd_value;
use App\Models\User;
use App\Models\Payment;
use App\Models\Private_school;
use App\Models\application_data;
use App\Models\application_tracking;
use App\Models\application_data_ext_1;
use App\Models\application_statuses;
use Carbon\Carbon;
use App\Exports\ExcelExport;
use App\Exports\PaymentExport;
use App\Exports\RejectAppExport;
use App\Models\fnd_settings;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;
class AppReportController extends Controller
{
    public $data = [];
    public function report()
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                return response()->json('report');
            } else {
                return response()->json(['message', 'You are not Authorised!!'],401);
            }
        }
    }
    public function index()
    {
        $Districts = Fnd_value_set::select(
            'fnd_value_sets.id',
            'fnd_value_sets.value_set_name'
        )->where('fnd_value_sets.parent_value_set_id', '=', '289')
            ->get();
        // $Users = User::all();
        $Application_Status = application_statuses::select(
            'application_statuses.id',
            'application_statuses.status_name'
        )->get();
        return response()->json(['DGR'=> $Districts]);
    }
    public function reportAllApplications(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $data['Application_Status'] = application_statuses::orderBy('id')
                    ->pluck(
                        'application_statuses.status_name',
                        'application_statuses.id'
                    );
                // $UserDistrict = DB::table('addresses')
                //     ->where('addresses.user_id', Auth::user()->id)
                //     ->select('addresses.district')
                //     ->get();
                // $User_District = $UserDistrict[0]->district;
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $application_id = $request->application_id;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                $applicationName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                if (isset($request->application_id) && $request->application_id != null) {
                    $applicationName = application_statuses::where('application_statuses.id',  $application_id)->value('status_name');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName, 'application_id' => $application_id, 'applicationName' => $applicationName
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::leftJoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->leftJoin('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                ->leftJoin('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                // ->leftJoin('application_trackings', 'application_trackings.application_id', 'application_data.id')
                ->leftJoin('application_statuses as status', 'status.id', 'application_data.application_status')
                ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.updated_at',
                        'fnd_division.display_value AS Division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                );
                    // ->where('application_data.status', 1);
                    if (isset($request->application_status) && $request->application_status != null) {
                        if( $request->application_status=='0'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.status',1);
                        }else if( $request->application_status=='Pending'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.status',1)->whereIn('application_data.application_status',[2, 3, 4, 9, 12]);
                        }else if( $request->application_status=='Approved'){
                   
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.status',1)->whereIn('application_data.application_status',[4, 6, 10, 13]);
                        }else if( $request->application_status=='Rejected'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.status',1)->whereIn('application_data.application_status', [7, 8, 11, 14]);
                        }else{
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.status',1)->where('application_data.application_status',$request->application_status);
                        }
                    }else{
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.status',1);
                    }
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->application_id) && $request->application_id != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',  $request->application_id);
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                ->where('status',1)
                ->first();
                // $days_to_varified_by_dse = fnd_settings::value('verification_by_dse_limit');
                // $days_for_final_desicion_by_dc = fnd_settings::value('schedule_meeting_by_dc_limit');
                // $days_to_respond_after_meeting_held_by_dc = fnd_settings::value('take_decision_after_meeting_held_by_dc');
                // $days_to_take_action_by_faa = fnd_settings::value('faa_decision_limit');
                // $days_to_take_action_by_saa = fnd_settings::value('saa_decision_limit');
                // $days_to_varified_by_saa = fnd_settings::value('school_appeal_limit');
                // $days_to_varified_by_saa = fnd_settings::value('school_second_appeal_after_rejection_limit');
               
                $date = 0;
                $end_day =0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date =application_tracking::where('application_id', $value->id)
                    ->where('application_status_id',2)
                    ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', $value->application_status)
                    ->value('created_at');
                    
                    $cDate = Carbon::parse( $value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if($value->application_status == 2 || $value->application_status == 3)
                    {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    }
                    elseif($value->application_status == 4)
                    {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    }
                    elseif($value->application_status == 5)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 6)
                    {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 7)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }             
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportAllApplications_excel', 'total' => $data['count']];
                    $filename = 'all-application.xlsx';
                    return Excel::download(new PaymentExport($params), $filename);
                }
                return response()->json(['reportAllApplications'=> $data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportCertifiedApplications(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                ->orderBy('fnd_values.display_value', 'ASC')
                ->pluck(
                    'fnd_values.display_value as division_name',
                    'fnd_values.id as division_id'
                );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                $from_date = null;
                $to_date = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id', $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id', $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id', $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName, 'from_date' => $from_date, 'to_date' => $to_date
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                ->select(
                    'application_data.id',
                    'application_data.apllication_id',
                    'application_data.school_name',
                    'application_data.created_at',
                    'application_data.block',
                    'application_data.district',
                    'application_data.division',
                    'application_data.application_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block',
                    'fnd_division.display_value AS Division',
                    'status.status_name AS status_name'
                )
                ->where('application_data.status', '1')
                ->where('application_data.application_status', '6');
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' . $search_text . '%')
                        ->orWhere('application_data.email', 'like', '%' . $search_text . '%')
                        ->orWhere('application_data.school_chairman_email', 'like', '%' . $search_text . '%')
                        ->orWhere('application_data.school_chairman_phone_number', 'like', '%' . $search_text . '%')
                        ->orWhere('application_data.phone_with_std_code', 'like', '%' . $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division', $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district', $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block', $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                        'verification_by_dse_limit',
                        'schedule_meeting_by_dc_limit',
                        'take_decision_after_meeting_held_by_dc',
                        'faa_decision_limit',
                        'saa_decision_limit',
                        'school_appeal_limit',
                        'school_second_appeal_after_rejection_limit',
                    )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = 0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', 2)
                    ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', $value->application_status)
                    ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
                $data['count'] = $RemainingDate;
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportCertifiedApplications_excel'];
                    $filename = 'approve-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                
                return response()->json(['reportCertifiedApplications'=> $data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportPendingApplications(Request $request)
    {
        // return($request);
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->where('fnd_values.status',1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $data['AllDetails']  = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                    // ->whereIn('application_data.application_status', ['3', '5', '9', '12']);
                    if (isset($request->application_status) && $request->application_status != null) {
                        if( $request->application_status=='0'){
                            $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', [2,3, 5, 9, 12]);
                        }else if( $request->application_status=='Pending_DSE'){
                            $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status',[2,3]);
                        }else if( $request->application_status=='Pending_DC'){
                   
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',4);
                        }else if( $request->application_status=='Pending_DPE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',12);
                        }
                        else if( $request->application_status=='Pending_RDDE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',9);
                        }
                    }else{
                        $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', [2,3, 5, 9, 12]);
                    }
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                ->where('status',1)
                ->first();
                $RemainingDate = [];
                    $date =0;
                    $end_day = 0;
                    foreach ($data['AllDetails'] as $key => $value) {
                        $value->appl_created_date =application_tracking::where('application_id', $value->id)
                        ->where('application_status_id',2)
                        ->value('created_at');
                        $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                        $cDate = Carbon::parse( $value->startDate);
                        $count = $now->diffInDays($cDate);
                        // return $value->startDate;
                        if($value->application_status == 2 || $value->application_status == 3)
                        {
                            $date = $days->verification_by_dse_limit - (int)$count;
                            $end_day = $days->verification_by_dse_limit;
                        }
                        elseif($value->application_status == 4)
                        {
                            $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                            $end_day = $days->schedule_meeting_by_dc_limit;
                        }
                        elseif($value->application_status == 5)
                        {
                            $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                            $end_day = $days->take_decision_after_meeting_held_by_dc;
                        }
                        elseif($value->application_status == 6)
                        {
                            // certified date have to shown
                            $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                            $end_day = $days->take_decision_after_meeting_held_by_dc;
                        }
                        elseif($value->application_status == 7)
                        {
                            $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                            $end_day = $days->take_decision_after_meeting_held_by_dc;
                        }             
                        $value->RemainingDate = $date;
                        $value->endDay = $end_day;
                        $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                        $value->enddate =  $enddate;
                    }
                  
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportPendingApplications_excel'];
                    $filename = 'pending-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                $data['AllDetails'] = ['data'=>$data['AllDetails']];
                return response()->json(['reportPendingApplications'=>$data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
   
    
    public function reportApprovedApplications(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.block',
                        'application_data.district',
                        'application_data.division',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'fnd_division.display_value AS Division',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                    // ->whereIn('application_data.application_status', ['4', '6', '10', '13']);
                    if (isset($request->application_status) && $request->application_status != null) {
                        if( $request->application_status=='0'){
                
                            $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', [4, 6, 10, 13]);
                        }else if( $request->application_status=='Approved_DSE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',4);
                        }else if( $request->application_status=='Approved_DC'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',6);
                        }else if( $request->application_status=='Approved_RDDE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',10);
                        }
                        else if( $request->application_status=='Approved_DPE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',13);
                        }
                    }else{
                        $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', [4, 6, 10, 13]);
                    }
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                ->where('status',1)
                ->first();
                $RemainingDate = [];
                $date =0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date =application_tracking::where('application_id', $value->id)
                    ->where('application_status_id',2)
                    ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', $value->application_status)
                    ->value('created_at');
                    $cDate = Carbon::parse( $value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if($value->application_status == 2 || $value->application_status == 3)
                    {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    }
                    elseif($value->application_status == 4)
                    {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    }
                    elseif($value->application_status == 5)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 6)
                    {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 7)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }             
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
              
                $data['count'] = count($data['AllDetails']);
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportApprovedApplications_excel'];
                    $filename = 'approve-application.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                $data['AllDetails'] = ['data'=>$data['AllDetails']];
                return response()->json(['reportApprovedApplications'=> $data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportVerifiedApplications()
    {
        $Districts = Fnd_value_set::select(
            'fnd_value_sets.id',
            'fnd_value_sets.value_set_name'
        )->where('fnd_value_sets.parent_value_set_id', '=', '289')
            ->get();
        // $Users = User::all();
        $Application_Status = application_statuses::select(
            'application_statuses.id',
            'application_statuses.status_name'
        )->get();
        $UserDistrict = DB::table('addresses')
            ->where('addresses.user_id', Auth::guard('api')->user()->id)
            ->select('addresses.district')
            ->get();
        $User_District = $UserDistrict[0]->district;
        $now = Carbon::now();
        $AllDetails = DB::table('application_data')
            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
            ->Join('fnd_value_sets as fnd_division', 'application_data.division', '=', 'fnd_division.id')
            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
            ->select(
                'application_data.id',
                'application_data.apllication_id',
                'application_data.school_name',
                'application_data.created_at',
                'application_data.block',
                'application_data.district',
                'application_data.division',
                'application_data.application_status',
                'fnd_district.display_value AS disrict',
                'fnd_block.display_value AS block',
                'fnd_division.value_set_name AS Division',
                'status.status_name AS status_name'
            )
            ->where('application_data.status', '1')
            ->where('application_data.application_status', '4')
            ->orderBy('created_at', 'desc')->get();
        $RemainingDate = [];
        foreach ($AllDetails as $row) {
            $startDate = $row->created_at;
            $cDate = Carbon::parse($startDate);
            $count = $now->diffInDays($cDate);
            $date = 90 - (int)$count;
            array_push($RemainingDate, $date);
        }
        //   return $RemainingDate; 
        /*$days=date_diff($end,$start);*/
        //    echo $days;
        // $Users = User::all();
        $TotalData = ['Districts' => $Districts, 'AllDetails' => $AllDetails, 'count' => $RemainingDate, 'Application_Status' => $Application_Status];
        return response()->json(['reportVerifiedApplications'=> $TotalData]);
    }
    public function reportRejectedApplications(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $division_id = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::guard('api')->user()->id)
                    ->value('addresses.division');
                $data['district_list'] = Fnd_value::where('fnd_values.value_set_id', '=', $division_id)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as district_name',
                        'fnd_values.id as district_id'
                    );
                $search_text = $request->search_text;
                $appeal_status = $request->appeal_status;
                $block = $request->Block;
                $division = $request->division;
                $district = $request->district;
                $fromDate = $request->from_date;
                $toDate = $request->to_date;
                $blockName = null;
                $divisionName = null;
                $districtName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = ['search_text' => $search_text, 'block_id' => $block, 'blockName' => $blockName, 'division_id' => $division, 'divisionName' => $divisionName, 'district_id' => $district, 'districtName' => $districtName, 'from_date' => $fromDate, 'to_date' => $toDate];
                $data['UserDistrict'] = DB::table('addresses')
                    ->where('addresses.user_id', Auth::guard('api')->user()->id)
                    ->select('addresses.district')
                    ->get();
                $data['UserDistrict'] =  $data['UserDistrict'][0]->district;
                $now = Carbon::now();
                $data['TotalData'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                
                ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                
                
                ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                
                ->select(
                
                'application_data.id',
                
                'application_data.apllication_id',
                
                'application_data.school_name',
                
                'application_data.created_at',
                
                'application_data.division',
                
                
                
                'application_data.block',
                
                'application_data.district',
                
                'application_data.application_status',
                
                'fnd_district.display_value AS disrict',
                
                'fnd_division.display_value AS Division',
                
                'fnd_block.display_value AS block',
                
                'status.status_name AS status_name'
                
                )
                
                ->where('application_data.status', '1');
                    // ->whereIn('application_data.application_status', ['7', '8', '11', '14']);
                    if (isset($request->application_status) && $request->application_status != null) {
                        if( $request->application_status=='0'){
                            $data['TotalData'] = $data['TotalData'] ->whereIn('application_data.application_status', ['7', '8', '11', '14']);
                        }else if( $request->application_status=='Rejected_DSE'){
                            $data['TotalData'] = $data['TotalData']->where('application_data.application_status',7);
                        }else if( $request->application_status=='Rejected_DC'){
                            $data['TotalData'] = $data['TotalData']->where('application_data.application_status',8);
                        }
                        else if( $request->application_status=='Rejected_RDDE'){
    
                                $data['TotalData'] = $data['TotalData']->where('application_data.application_status',11);
                        }
                        else if( $request->application_status=='Rejected_DPE'){
                            $data['TotalData'] = $data['TotalData']->where('application_data.application_status',14);
                        }
                       
                    }else{
                        $data['TotalData'] = $data['TotalData'] ->whereIn('application_data.application_status', ['7', '8', '11', '14']);
                    }
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['TotalData'] =  $data['TotalData']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('status.status_name', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['TotalData'] =  $data['TotalData']->where('application_data.division', $division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['TotalData'] =  $data['TotalData']->where('application_data.district', $district);
                }
                if (isset($request->Block) && $request->Block != null) {
                    $data['TotalData'] =  $data['TotalData']->where('application_data.block', $block);
                }
                if ($request->from_date != null &&  $request->to_date != null) {
                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $fromDate)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $toDate)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return response()->json(['warning'=> "Form date always lesser than To date"],400);
                    }
                    //return[$start_date,$end_date];  
                    $data['TotalData'] =  $data['TotalData']->whereBetween('application_data.created_at', [$fromDate, $toDate]);
                }
                $data['count_TotalData'] = $data['TotalData']->count();
                if ($request->submit == 'excel') {
                    $data['TotalData'] = $data['TotalData']->get();
                } else {
                    $data['TotalData'] = $data['TotalData']->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                ->where('status',1)
                ->first();
                $RemainingDate = [];
                $date =0;
                $end_day = 0;
                foreach ($data['TotalData'] as $key => $value) {
                    $value->appl_created_date =application_tracking::where('application_id', $value->id)
                    ->where('application_status_id',2)
                    ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', $value->application_status)
                    ->value('created_at');
                    $cDate = Carbon::parse( $value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if($value->application_status == 2 || $value->application_status == 3)
                    {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    }
                    elseif($value->application_status == 4)
                    {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    }
                    elseif($value->application_status == 5)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 6)
                    {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 7)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }             
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
              
                if ($request->submit == 'excel') {
                    if (count($data['TotalData']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['TotalData'], 'view' => 'excel_view.report_rejected_application_excel'];
                    // return[$params];
                    $filename = 'RejectedApplication.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return response()->json(['reportRejectedApplications'=>  $data]);
            } else {
                return response(['message'=> 'You are not Authorised!!'],203);
            }
            return response(['message'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportDelayedApplications(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.division',
                        'application_data.block',
                        'application_data.district',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_division.display_value AS Division',
                        'fnd_block.display_value AS block',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1');
                    // ->whereIn('application_data.application_status', ['18','19','20','21','22']);
                    if (isset($request->application_status) && $request->application_status != null) {
                        if( $request->application_status=='0'){
                
                            $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', ['18','19','20','21','22']);
                        }else if( $request->application_status=='Delayed_DSE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',18);
                        }else if( $request->application_status=='Delayed_DC_Meeting'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',19);
                        }
                        else if( $request->application_status=='Delayed_DC_verification'){
    
                                $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',20);
                        }
                        else if( $request->application_status=='Delayed_RDDE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',21);
                        }
                        else if( $request->application_status=='Delayed_DPE'){
                            $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status',22);
                        }
                    }else{
                        $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', ['18','19','20','21','22']);
                    }
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                ->where('status',1)
                ->first();
                $RemainingDate = [];
                $date =0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date =application_tracking::where('application_id', $value->id)
                    ->where('application_status_id',2)
                    ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', $value->application_status)
                    ->value('created_at');
                    $cDate = Carbon::parse( $value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if($value->application_status == 2 || $value->application_status == 3)
                    {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    }
                    elseif($value->application_status == 4)
                    {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    }
                    elseif($value->application_status == 5)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 6)
                    {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 7)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }             
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
                $data['count'] = $RemainingDate;
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportDelayedApplications_excel', 'total' => $data['count']];
                    $filename = 'delayed-application.xlsx';
                    return Excel::download(new PaymentExport($params), $filename);
                }
                return response()->json(['reportDelayedApplications'=> $data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],401);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],401);
        }
    }
    public function reportDcMeetings(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                    ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                    ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                    ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                    ->select(
                        'application_data.id',
                        'application_data.apllication_id',
                        'application_data.school_name',
                        'application_data.created_at',
                        'application_data.block',
                        'application_data.district',
                        'application_data.division',
                        'application_data.application_status',
                        'fnd_district.display_value AS disrict',
                        'fnd_block.display_value AS block',
                        'fnd_division.display_value AS Division',
                        'status.status_name AS status_name'
                    )
                    ->where('application_data.status', '1')
                    ->where('application_data.application_status', '5');
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                ->where('status',1)
                ->first();
                $RemainingDate = [];
                $date =0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date =application_tracking::where('application_id', $value->id)
                    ->where('application_status_id',2)
                    ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                    ->where('application_status_id', $value->application_status)
                    ->value('created_at');
                    $cDate = Carbon::parse( $value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if($value->application_status == 2 || $value->application_status == 3)
                    {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    }
                    elseif($value->application_status == 4)
                    {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    }
                    elseif($value->application_status == 5)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 6)
                    {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    elseif($value->application_status == 7)
                    {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }             
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate= Carbon::parse( $value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
              
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportDcMeetings_excel',];
                    $filename = 'Dc-meeting-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return response()->json(['reportDcMeetings'=> $data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],203);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportPayments(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                $amount = 0;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $data['payments'] = Payment::leftjoin('application_data', 'application_data.school_id', 'payments.school_id')
                ->leftjoin('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                ->leftjoin('fnd_values as fnd_district', 'application_data.district', 'fnd_district.id')
                ->leftjoin('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                ->select(
                    'payments.*',
                    'application_data.school_name',
                    'application_data.school_name',
                    'application_data.school_name',
                    'application_data.school_name',
                    'application_data.phone_with_std_code as phone_no',
                    'application_data.school_chairman_phone_number as mobile_no',
                    'application_data.school_chairman_name as contact_name',
                    'application_data.village_city',
                    'application_data.post_office',
                    'application_data.panchayat',
                    'fnd_division.display_value as division',
                    'fnd_district.display_value as district',
                    'fnd_block.display_value as block',
                )
                    ->where('application_data.status', 1)
                    ->where('payments.status', 1);
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['payments'] = $data['payments']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['payments'] = $data['payments']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['payments'] = $data['payments']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['payments'] = $data['payments']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['payments'] = $data['payments']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['payments'] = $data['payments']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                foreach ($data['payments'] as $key => $value) {
                    $amount = $value->payment_amount + $amount;
                }
                $data['total_amount'] = $amount;
                if ($request->excel == 'excel') {
                    if (count($data['payments']) == 0) {
                        return response()->json(['warning'=> "There are no such data to export!!"],400);
                    }
                    $params = ['data' => $data['payments'], 'view' => 'excel_view.reportDpeAppeals_excel'];
                    $filename = 'DPE-Appeals-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                $data['payments'] = ['data'=>$data['payments']];
                return response()->json(['reportPayments'=> $data]);
            } else {
                return response()->json(['message'=> 'You are not Authorised!!'],401);
            }
            return response()->json(['message'=> 'You are not Authorised!!'],401);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportRddeAppeals(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $UserDistrict = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                ->where('addresses.user_id', Auth::guard('api')->user()->id)
                    ->value('addresses.district');
                $data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
                    ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as district_name',
                        'fnd_values.id as district_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = ['search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName, 'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                ->select(
                    'application_data.id',
                    'application_data.apllication_id',
                    'application_data.school_name',
                    'application_data.created_at',
                    'application_data.division',
                    'application_data.block',
                    'application_data.district',
                    'application_data.application_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_division.display_value AS Division',
                    'fnd_block.display_value AS block',
                    'status.status_name AS status_name'
                )
                ->where('application_data.status', '1');
                // ->whereIn('application_data.application_status', ['9', '10', '11'])
                // ->orderBy('created_at', 'desc');
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {
                        $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', ['9', '10', '11']);
                    } else if ($request->application_status == 'Pending') {
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status', 9);
                    } else if ($request->application_status == 'Accepted') {
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status', 10);
                    } else if ($request->application_status == 'Rejected') {
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status', 11);
                    }
                } else {
                    $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', ['9', '10', '11']);
                }
                $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc');
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.id', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->Block) && $request->Block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->Block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->get();
                }
                // return(count($data['AllDetails']));
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = 0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate = Carbon::parse($value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning' => "There are no such data to export!!"], 400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportRdde_excel'];
                    $filename = 'report-rdde-list.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return response()->json(['reportRddeAppeals' => $data]);
            } else {
                return response()->json(['message' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'You are not Authorised!!'], 203);
        }
        return response()->json(['message' => 'Your session has timed out'], 401);
    }
    public function reportDpeAppeals(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                ->Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                
                ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                
                
                ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                
                ->select(
                
                'application_data.id',
                
                'application_data.apllication_id',
                
                'application_data.school_name',
                
                'application_data.created_at',
                
                'application_data.division',
                
                
                
                'application_data.block',
                
                'application_data.district',
                
                'application_data.application_status',
                
                'fnd_district.display_value AS disrict',
                
                'fnd_division.display_value AS Division',
                
                'fnd_block.display_value AS block',
                
                'status.status_name AS status_name'
                
                )
                
                ->where('application_data.status', '1');
                // ->whereIn('application_data.application_status', ['12', '13', '14']);
                if (isset($request->application_status) && $request->application_status != null) {
                    if ($request->application_status == '0') {
                        $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', ['12', '13', '14']);
                    } else if ($request->application_status == 'Pending') {
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status', 12);
                    } else if ($request->application_status == 'Accepted') {
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status', 13);
                    } else if ($request->application_status == 'Rejected') {
                        $data['AllDetails'] = $data['AllDetails']->where('application_data.application_status', 14);
                    }
                } else {
                    $data['AllDetails'] = $data['AllDetails']->whereIn('application_data.application_status', ['12', '13', '14']);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = 0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate = Carbon::parse($value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning' => "There are no such data to export!!"], 400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportDpeAppeals_excel'];
                    $filename = 'DPE-Appeals-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                return response()->json(['reportDpeAppeals' => $data]);
            } else {
                return response()->json(['message' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'You are not Authorised!!'],203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
    public function reportRegisteredSchool(Request $request)
    {
        if (Auth::guard('api')->check()) {
            if (Auth::guard('api')->user()->user_role == 'state') {
                $data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
                ->orderBy('fnd_values.display_value', 'ASC')
                    ->pluck(
                        'fnd_values.display_value as division_name',
                        'fnd_values.id as division_id'
                    );
                $search_text = $request->search_text;
                $division = $request->division;
                $district = $request->district;
                $block = $request->block;
                $divisionName = null;
                $districtName = null;
                $blockName = null;
                if (isset($request->division) && $request->division != null) {
                    $divisionName = Fnd_value::where('fnd_values.id',  $division)->value('display_value');
                }
                if (isset($request->district) && $request->district != null) {
                    $districtName = Fnd_value::where('fnd_values.id',  $district)->value('display_value');
                }
                if (isset($request->block) && $request->block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                $data['adv_data'] = [
                    'search_text' => $search_text, 'division_id' => $division, 'district_id' => $district, 'divisionName' => $divisionName,
                    'districtName' => $districtName, 'block_id' => $block, 'blockName' => $blockName
                ];
                $now = Carbon::now();
                $data['AllDetails'] = application_data::Join('fnd_values as fnd_division', 'application_data.division', 'fnd_division.id')
                ->Join('fnd_values as fnd_district', 'application_data.district', 'fnd_district.id')
                ->Join('fnd_values as fnd_block', 'application_data.block', 'fnd_block.id')
                ->Join('application_statuses as status', 'status.id', 'application_data.application_status')
                ->select(
                    'application_data.id',
                    'application_data.apllication_id',
                    'application_data.school_name',
                    'application_data.created_at',
                    'application_data.block',
                    'application_data.district',
                    'application_data.division',
                    'application_data.application_status',
                    'fnd_district.display_value AS disrict',
                    'fnd_block.display_value AS block',
                    'fnd_division.display_value AS Division',
                    'status.status_name AS status_name'
                )
                    ->where('application_data.status', '1')
                    ->where('application_data.application_status', '2');
                if (isset($request->search_text) && $request->search_text != null) {
                    $data['AllDetails'] = $data['AllDetails']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->division) && $request->division != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.division',  $request->division);
                }
                if (isset($request->district) && $request->district != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.district',  $request->district);
                }
                if (isset($request->block) && $request->block != null) {
                    $data['AllDetails'] = $data['AllDetails']->where('application_data.block',  $request->block);
                }
                if ($request->excel == 'excel') {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                } else {
                    $data['AllDetails'] = $data['AllDetails']->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->get();
                }
                $days = fnd_settings::select(
                    'verification_by_dse_limit',
                    'schedule_meeting_by_dc_limit',
                    'take_decision_after_meeting_held_by_dc',
                    'faa_decision_limit',
                    'saa_decision_limit',
                    'school_appeal_limit',
                    'school_second_appeal_after_rejection_limit',
                )
                    ->where('status', 1)
                    ->first();
                $RemainingDate = [];
                $date = 0;
                $end_day = 0;
                foreach ($data['AllDetails'] as $key => $value) {
                    $value->appl_created_date = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', 2)
                        ->value('created_at');
                    $value->startDate = application_tracking::where('application_id', $value->id)
                        ->where('application_status_id', $value->application_status)
                        ->value('created_at');
                    $cDate = Carbon::parse($value->startDate);
                    $count = $now->diffInDays($cDate);
                    // return $value->startDate;
                    if ($value->application_status == 2 || $value->application_status == 3) {
                        $date = $days->verification_by_dse_limit - (int)$count;
                        $end_day = $days->verification_by_dse_limit;
                    } elseif ($value->application_status == 4) {
                        $date = $days->schedule_meeting_by_dc_limit - (int)$count;
                        $end_day = $days->schedule_meeting_by_dc_limit;
                    } elseif ($value->application_status == 5) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 6) {
                        // certified date have to shown
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    } elseif ($value->application_status == 7) {
                        $date = $days->take_decision_after_meeting_held_by_dc - (int)$count;
                        $end_day = $days->take_decision_after_meeting_held_by_dc;
                    }
                    $value->RemainingDate = $date;
                    $value->endDay = $end_day;
                    $enddate = Carbon::parse($value->startDate)->addDays($end_day)->format('d/m/Y');
                    $value->enddate =  $enddate;
                }
                if ($request->excel == 'excel') {
                    if (count($data['AllDetails']) == 0) {
                        return response()->json(['warning' => "There are no such data to export!!"], 400);
                    }
                    $params = ['data' => $data['AllDetails'], 'view' => 'excel_view.reportRegisteredschool_excel'];
                    $filename = 'registered-school-report.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                }
                $data['AllDetails'] = ['data'=>$data['AllDetails']];
                return response()->json(['reportRegisteredSchool' => $data]);
            } else {
                return response()->json(['message' => 'You are not Authorised!!'], 203);
            }
            return response()->json(['message' => 'You are not Authorised!!'], 203);
        }
        return response()->json(['message'=> 'Your session has timed out'],401);
    }
   
    // myformAjax
    public function  Block($id)
    {
        $Districts = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.parent_value_id', '=', $id)
            ->get();
        return json_encode($Districts);
        // $Users = User::all();
        return response()->json('DGR', compact('Districts'));
    }
    public function  Village($id)
    {
        $Districts = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.parent_value_id', '=', $id)
            ->get();
        // $cities = DB::table("demo_cities")
        //             ->where("state_id",$id)
        //             ->lists("name","id");
        return json_encode($Districts);
        // $Users = User::all();
        return response()->json('DGR', compact('Districts'));
    }
    // public function advSearch(Request $request)
    // {
    //     // return $request;
    //     $Division_Name = $District_Name = $Block_Name = $Application_Status_Name = ['0' => ''];
    //     if ($request->division != '-1') {
    //         $Division_Name =  Fnd_value_set::select(
    //             'fnd_value_sets.id',
    //             'fnd_value_sets.value_set_name'
    //         )->where('fnd_value_sets.id', '=', $request->division)
    //             ->get();
    //     }
    //     if ($request->district != '-1') {
    //         $District_Name = Fnd_value::select(
    //             'fnd_values.id',
    //             'fnd_values.display_value'
    //         )->where('fnd_values.id', '=', $request->district)
    //             ->get();
    //     }
    //     if ($request->block != '-1') {
    //         $Block_Name = Fnd_value::select(
    //             'fnd_values.id',
    //             'fnd_values.display_value'
    //         )->where('fnd_values.id', '=', $request->block)
    //             ->get();
    //     }
    //     if ($request->application_status != '-1') {
    //         $Application_Status_Name = application_statuses::select(
    //             'application_statuses.id',
    //             'application_statuses.status_name'
    //         )->where('application_statuses.id', '=', $request->application_status)
    //             ->get();
    //     }
    //     $Req = [$request->adv_search, $Division_Name, $District_Name, $Block_Name, $Application_Status_Name, $request->usertype];
    //     // return $Req;
    //     $Districts = Fnd_value_set::select(
    //         'fnd_value_sets.id',
    //         'fnd_value_sets.value_set_name'
    //     )->where('fnd_value_sets.parent_value_set_id', '=', '289')
    //         ->get();
    //     $Application_Status = application_statuses::select(
    //         'application_statuses.id',
    //         'application_statuses.status_name'
    //     )->get();
    //     $Adv_Search = $request->adv_search;
    //     // return $Adv_Search;
    //     $now = Carbon::now();
    //     $UserDistrict = DB::table('addresses')
    //         ->where('addresses.user_id', Auth::guard('api')->user()->id)
    //         ->select('addresses.district')
    //         ->get();
    //     $User_District = $UserDistrict[0]->district;
    //     switch ($request->search_status) {
    //         case 'user_search':
    //             $Districts = Fnd_value_set::select(
    //                 'fnd_value_sets.id',
    //                 'fnd_value_sets.value_set_name'
    //             )->where('fnd_value_sets.parent_value_set_id', '=', '289')->get();
    //             $Users = User::join('addresses', 'users.id', '=', 'addresses.user_id')
    //                 ->join('contacts', 'contacts.user_id', '=', 'users.id')
    //                 ->select(
    //                     'users.id',
    //                     'users.name',
    //                     'users.email',
    //                     'users.user_role',
    //                     'users.created_at',
    //                     'addresses.school_address as user_address',
    //                     'addresses.created_by',
    //                     'contacts.mobile as contact'
    //                 )->where('users.status', '=', '1');
    //             if ($request->adv_search) {
    //                 $Users = $Users->Where('users.name', 'LIKE', '%' . $request->adv_search . '%')
    //                     ->orWhere('users.email', 'LIKE', '%' . $request->adv_search . '%')
    //                     ->orWhere('contacts.mobile', 'LIKE', '%' . $request->adv_search . '%')
    //                     ->orWhere('contacts.phone_2', 'LIKE', '%' . $request->adv_search . '%')
    //                     ->orWhere('contacts.whatsapp', 'LIKE', '%' . $request->adv_search . '%');
    //             }
    //             if ($request->division != '-1') {
    //                 $Users = $Users->where('addresses.division', $request->division);
    //             }
    //             if ($request->district != '-1') {
    //                 $Users = $Users->where('addresses.district', $request->district);
    //             }
    //             if ($request->usertype != '-1') {
    //                 $Users = $Users->where('users.user_role', $request->usertype);
    //             }
    //             // $Users = $Users->get();
    //             $Users = $Users->orderBy('users.created_at', 'asc')->get(;
    //             $UserData = ['User' => $Users, 'District' => $Districts, 'ReturnRequest' => $Req];
    //             // return $UserData;
    //             return response()->json( compact('UserData'));
    //             break;
    //         case '':
    //             break;
    //         default:
    //             $AllDetails = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
    //                 ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
    //                 ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
    //                 ->Join('fnd_value_sets as fnd_division', 'application_data.division', '=', 'fnd_division.id')
    //                 ->select(
    //                     'application_data.id',
    //                     'application_data.apllication_id',
    //                     'application_data.school_name',
    //                     'application_data.created_at',
    //                     'application_data.block',
    //                     'application_data.district',
    //                     'application_data.application_status',
    //                     'fnd_district.display_value AS disrict',
    //                     'fnd_division.value_set_name AS Division',
    //                     'fnd_block.display_value AS block',
    //                     'status.status_name AS status_name'
    //                 )->where('application_data.status', '=', '1');
    //             if ($request->adv_search) {
    //                 $AllDetails = $AllDetails->Where('application_data.school_name', 'LIKE', '%' . $Adv_Search . '%');
    //             }
    //             if ($request->district != '-1') {
    //                 $AllDetails = $AllDetails->where('application_data.district', $request->district);
    //             }
    //             if ($request->search_status == 'Delayed') {
    //                 $AllDetails = $AllDetails->where('application_data.application_status', '15');
    //             }
    //             if ($request->search_status == 'MeetingSheduled') {
    //                 $AllDetails = $AllDetails->where('application_data.application_status', '5');
    //             }
    //             if ($request->search_status == 'Verifed') {
    //                 $AllDetails = $AllDetails->where('application_data.application_status', '4');
    //             }
    //             if ($request->search_status == 'DPE') {
    //                 $AllDetails = $AllDetails->whereIn('application_data.application_status', ['12', '13', '14']);
    //             }
    //             if ($request->search_status == 'Registered') {
    //                 $AllDetails = $AllDetails->where('application_data.application_status', '1');
    //             }
    //             if ($request->search_status == 'RejectedApplication') {
    //                 $AllDetails = $AllDetails->whereIn('application_data.application_status', ['7', '8', '11', '14']);
    //             }
    //             if ($request->search_status == 'Approved') {
    //                 $AllDetails = $AllDetails->where('application_data.application_status', '6');
    //             }
    //             if ($request->block != '-1') {
    //                 $AllDetails = $AllDetails->where('application_data.block', $request->block);
    //             }
    //             if ($request->division != '-1') {
    //                 $AllDetails = $AllDetails->where('application_data.division', $request->division);
    //             }
    //             $AllDetails = $AllDetails->orderBy('created_at', 'desc')->get();
    //             // return $AllDetails; 
    //             $RemainingDate = [];
    //             foreach ($AllDetails as $row) {
    //                 $startDate = $row->created_at;
    //                 $cDate = Carbon::parse($startDate);
    //                 $count = $now->diffInDays($cDate);
    //                 $date = 90 - (int)$count;
    //                 array_push($RemainingDate, $date);
    //             }
    //             $TotalData = ['Districts' => $Districts, 'AllDetails' => $AllDetails, 'count' => $RemainingDate, 'Application_Status' => $Application_Status, 'ReturnRequest' => $Req];
    //             // return $TotalData;
    //             if ($request->search_status == 'Delayed') {
    //                 return response()->json( $TotalData);
    //             }
    //             if ($request->search_status == 'Verifed') {
    //                 return response()->json( $TotalData);
    //             }
    //             if ($request->search_status == 'MeetingScheduled') {
    //                 return response()->json( $TotalData);
    //             }
    //             if ($request->search_status == 'DPE') {
    //               return response()->json($TotalData);
    //             }
    //             if ($request->search_status == 'RejectedApplication') {
    //                 // return $TotalData;
    //                 return response()->json( $TotalData);
    //             }
    //             if ($request->search_status == 'Registered') {
    //                 // return $TotalData;
    //                 return response()->json($TotalData);
    //             }
    //             if ($request->search_status == 'Approved') {
    //                 // return $TotalData;
    //                 return response()->json($TotalData);
    //             }
    //             break;
    //     }
    //     return response()->json(['data'=>$TotalData]);
    // }
    public function advSearchRDDE(Request $request)
    {
        $Division_Name = $District_Name = $Block_Name = ['0' => ''];
        if ($request->division != '-1') {
            $Division_Name =  Fnd_value_set::select(
                'fnd_value_sets.id',
                'fnd_value_sets.value_set_name'
            )->where('fnd_value_sets.id', '=', $request->division)
                ->get();
        }
        if ($request->district != '-1') {
            $District_Name = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.id', '=', $request->district)
                ->get();
        }
        if ($request->block != '-1') {
            $Block_Name = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.id', '=', $request->block)
                ->get();
        }
        $Req = [$request->adv_search, $Division_Name, $District_Name, $Block_Name];
        $Districts = Fnd_value_set::select(
            'fnd_value_sets.id',
            'fnd_value_sets.value_set_name'
        )->where('fnd_value_sets.parent_value_set_id', '=', '289')
            ->get();
        $Application_Status = application_statuses::select(
            'application_statuses.id',
            'application_statuses.status_name'
        )->get();
        $Adv_Search = $request->adv_search;
        // return $Adv_Search;
        $now = Carbon::now();
        $UserDistrict = DB::table('addresses')
            ->where('addresses.user_id', Auth::guard('api')->user()->id)
            ->select('addresses.district')
            ->get();
        $User_District = $UserDistrict[0]->district;
        $AllDetails = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
            ->Join('fnd_value_sets as fnd_division', 'application_data.division', '=', 'fnd_division.id')
            ->select(
                'application_data.id',
                'application_data.apllication_id',
                'application_data.school_name',
                'application_data.created_at',
                'application_data.block',
                'application_data.district',
                'application_data.application_status',
                'fnd_district.display_value AS disrict',
                'fnd_division.value_set_name AS Division',
                'fnd_block.display_value AS block',
                'status.status_name AS status_name'
            )->where('application_data.status', '=', '1')
            ->whereIn('application_data.application_status', ['9', '10', '11']);
        if ($request->adv_search) {
            $AllDetails = $AllDetails->Where('application_data.school_name', 'LIKE', '%' . $Adv_Search . '%');
        }
        if ($request->district != '-1') {
            $AllDetails = $AllDetails->where('application_data.district', $request->district);
        }
        if ($request->block != '-1') {
            $AllDetails = $AllDetails->where('application_data.block', $request->block);
        }
        if ($request->division != '-1') {
            $AllDetails = $AllDetails->where('application_data.division', $request->division);
        }
        $AllDetails = $AllDetails->orderBy('created_at', 'desc')->get();
        // return $AllDetails; 
        $RemainingDate = [];
        foreach ($AllDetails as $row) {
            $startDate = $row->created_at;
            $cDate = Carbon::parse($startDate);
            $count = $now->diffInDays($cDate);
            $date = 90 - (int)$count;
            array_push($RemainingDate, $date);
        }
        $TotalData = ['Districts' => $Districts, 'AllDetails' => $AllDetails, 'count' => $RemainingDate, 'ReturnRequest' => $Req];
        // return $TotalData;
        return response()->json(compact('TotalData'));
    }
    public function advSearchAllReport(Request $request)
    {
        // return $request;
        $Division_Name = $District_Name = $Block_Name = $Application_Status_Name = ['0' => ''];
        if ($request->division != '-1') {
            $Division_Name =  Fnd_value_set::select(
                'fnd_value_sets.id',
                'fnd_value_sets.value_set_name'
            )->where('fnd_value_sets.id', '=', $request->division)
                ->get();
        }
        if ($request->district != '-1') {
            $District_Name = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.id', '=', $request->district)
                ->get();
        }
        if ($request->block != '-1') {
            $Block_Name = Fnd_value::select(
                'fnd_values.id',
                'fnd_values.display_value'
            )->where('fnd_values.id', '=', $request->block)
                ->get();
        }
        if ($request->application_status != '-1') {
            $Application_Status_Name = application_statuses::select(
                'application_statuses.id',
                'application_statuses.status_name'
            )->where('application_statuses.id', '=', $request->application_status)
                ->get();
        }
        $Req = [$request->adv_search, $Division_Name, $District_Name, $Block_Name, $Application_Status_Name];
        // return $Req;
        $Districts = Fnd_value_set::select(
            'fnd_value_sets.id',
            'fnd_value_sets.value_set_name'
        )->where('fnd_value_sets.parent_value_set_id', '=', '289')
            ->get();
        $Application_Status = application_statuses::select(
            'application_statuses.id',
            'application_statuses.status_name'
        )->get();
        $Adv_Search = $request->adv_search;
        // return $Adv_Search;
        $now = Carbon::now();
        $UserDistrict = DB::table('addresses')
            ->where('addresses.user_id', Auth::guard('api')->user()->id)
            ->select('addresses.district')
            ->get();
        $User_District = $UserDistrict[0]->district;
        $AllDetails = application_data::Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
            ->Join('fnd_value_sets as fnd_division', 'application_data.division', '=', 'fnd_division.id')
            ->select(
                'application_data.id',
                'application_data.apllication_id',
                'application_data.school_name',
                'application_data.created_at',
                'application_data.block',
                'application_data.district',
                'application_data.application_status',
                'fnd_district.display_value AS disrict',
                'fnd_division.value_set_name AS Division',
                'fnd_block.display_value AS block',
                'status.status_name AS status_name'
            )->where('application_data.status', '=', '1');
        if ($request->adv_search) {
            $AllDetails = $AllDetails->Where('application_data.school_name', 'like', '%' . $request->adv_search . '%')
                ->orWhere('application_data.recognised_by', 'like', '%' . $request->adv_search . '%')
                ->orWhere('application_data.police_station', 'like', '%' . $request->adv_search . '%');
        }
        if ($request->district != '-1') {
            $AllDetails = $AllDetails->where('application_data.district', $request->district);
        }
        if ($request->block != '-1') {
            $AllDetails = $AllDetails->where('application_data.block', $request->block);
        }
        if ($request->division != '-1') {
            $AllDetails = $AllDetails->where('application_data.division', $request->division);
        }
        if ($request->application_status != '-1') {
            $AllDetails = $AllDetails->where('application_data.application_status', $request->application_status);
        }
        $AllDetails = $AllDetails->orderBy('created_at', 'desc')->get();
        // return $AllDetails; 
        $RemainingDate = [];
        foreach ($AllDetails as $row) {
            $startDate = $row->created_at;
            $cDate = Carbon::parse($startDate);
            $count = $now->diffInDays($cDate);
            $date = 90 - (int)$count;
            array_push($RemainingDate, $date);
        }
        $TotalData = ['Districts' => $Districts, 'AllDetails' => $AllDetails, 'count' => $RemainingDate, 'Application_Status' => $Application_Status, 'ReturnRequest' => $Req];
        // return $TotalData;
        return response()->json(compact('TotalData'));
    }
    public function  blockList($id) {
        $Districts = Fnd_value::where('fnd_values.parent_value_id', '=', $id)
            ->pluck(
                'fnd_values.display_value',
                'fnd_values.id'
           );
        return json_encode($Districts);
    }
    public function  districtList($id)
    {
        $Districts = Fnd_value::where('fnd_values.parent_value_id', '=', $id)
            ->pluck(
                'fnd_values.display_value',
                'fnd_values.id'
            );
        // $cities = DB::table("demo_cities")
        //             ->where("state_id",$id)
        //             ->lists("name","id");
        return json_encode($Districts);
        // $Users = User::all();
    }
}
