<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

// use App\Http\Controllers\Controller\Api\testsms;
use App\Models\User;
use App\Models\Private_school;
use App\Models\Person_detail;
use App\Models\Contact;
use App\Models\Address;
use App\Models\application_data;
use App\Models\application_data_remarks;
use App\Models\application_data_verification;
use App\Models\application_data_remarks_ext_1;
use App\Models\application_data_verification_ext_1;
use App\Models\application_data_ext_file;
use App\Models\application_data_file;
use App\Models\application_teacher_data;
use App\Models\application_data_ext_1;
use App\Models\Fnd_value;
use App\Models\Evv_udise_data;
use Illuminate\Http\Request;
use Mail;
use Session;
use Hash;

class AppSchoolRegisterController  extends Controller
{
    //     public function __construct(AcmeInterface $sqlRepo, AcmeInterface $redisRepo)
    // {
    //     $this->sqlRepo = $sqlRepo;
    //     $this->redisRepo = $redisRepo;

    // }
    // global $obj ;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('school_register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request->all();
        ############ Form Validation #################
        // $request->validate([

        //     // 'udise_code' => 'required|min:2|max:50',
        //     'school_name' => 'required|min:2|max:100',
        //     'user_address' => 'required|min:2|max:200',
        //     'username' => 'required|min:2|max:50|unique:users',
        //     'contact' => 'required|numeric|min:10',
        //     'email' => 'required|email|unique:users',
        //     'mobile_otp' => 'required|min:4',
        //     // 'email_otp' => 'required|min:4',
        //     'password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/',
        //     'password_confirmation' => 'required|min:8|max:20|same:password',

        // ], [

        //     // 'udise_code.required' => 'Name is required',
        //     'username.required' => 'User name is required',
        //     'school_name.required' => 'School name is required',
        //     'school_name.min' => 'School Name must be at least 2 characters.',
        //     'school_name.max' => 'School Name should not be greater than 50 characters.',
        //     'user_address.min' => 'Address must be at least 2 characters.',
        //     'user_address.max' => 'School Name should not be greater than 200 characters.',
        //     'contact.min' => 'Mobile number must be 10 digits.',
        //     'password.regex' => 'Password must be of Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.',

        // ]);

        if ($request->mobile_otp != Session::get('phone_otp')) {
            Session::put('status', 'Mobile OTP did not matched.');
            return response()->json(['Msg' => 'Mobile OTP did not matched', 'ChKval' => 1]);
        }
        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');
        $all_districts_list = Fnd_value::whereIn('fnd_values.value_set_id', $division_ids)->pluck('id');
        ################Get School data############## 
        $schoolData = $request->only(['school_name', 'udise_code', 'udisecode_name', 'mobile_otp', 'email_otp']);
        $getLast_id = Private_school::create($schoolData);


        ################Get User data##############    
        $userData = $request->only(['contact', 'username', 'email', 'password', 'user_address']);
        //  $userData['password'] =  \Hash::make($userData['password']);
        $userData['user_role'] =  'school';
        // $userData['username'] =  'school';
        $userData['password'] = bcrypt($userData['password']);
        $userData['school_id'] = $getLast_id->id;
        $userData['name'] = $request->school_name;
        if (isset($request->dist_name) && $request->dist_name != null) {
            $userData['district'] = Fnd_value::whereIn('id', $all_districts_list)->where('display_value', strtoupper($request->dist_name))->value('id');
        } else {
            $userData['district'] = $request->district;
        }
        if (isset($request->block_name) && $request->block_name != null) {
            $userData['block'] = Fnd_value::where('parent_value_id', $userData['district'])->where('display_value', strtoupper($request->block_name))->value('id');
        } else {
            $userData['block'] = $request->block;
        }
        $user_id =  User::create($userData);

        // EVV API data
        $Evv_udise_data = new Evv_udise_data;
        $Evv_udise_data->school_id = $getLast_id->id;
        $Evv_udise_data->created_at = date('Y-m-d H:i:s');
        $Evv_udise_data->block_name = $request->block_name;
        $Evv_udise_data->dist_name = $request->dist_name;
        $Evv_udise_data->vchSchoolName = $request->vchSchoolName;
        $Evv_udise_data->vchContact = $request->vchContact;
        $Evv_udise_data->vchEmail = $request->vchEmail;
        $Evv_udise_data->vchUdiseCode = $request->vchUdiseCode;
        $Evv_udise_data->intBoysTotalUrinals = $request->intBoysTotalUrinals;
        $Evv_udise_data->intGirlsTotalUrinals = $request->intGirlsTotalUrinals;
        $Evv_udise_data->vchHeadofsch = $request->vchHeadofsch;
        $Evv_udise_data->vchMediumName = $request->vchMediumName;
        $Evv_udise_data->vchPanchayatName = $request->vchPanchayatName;
        $Evv_udise_data->vchPin = $request->vchPin;
        $Evv_udise_data->vchVillageName = $request->vchVillageName;
        $Evv_udise_data->vchYear = $request->vchYear;
        $Evv_udise_data->water_facility = $request->water_facility;
        $Evv_udise_data->save();

        $application_form = new application_data;
        $application_form->created_by  = -1;
        $application_form->created_at  = date('Y-m-d H:i:s');
        $application_form->school_id  = $getLast_id->id;

        if (
            isset($request->vchSchoolName) && $request->vchSchoolName != null
        ) {
            $application_form->school_name = $request->vchSchoolName;
        } else {
            $application_form->school_name = $request->school_name;
        }
        if (isset($request->vchUdiseCode) && $request->vchUdiseCode != null) {
            $application_form->udise_code = $request->vchUdiseCode;
        }
        // if (isset($request->vchSchoolName) && $request->vchSchoolName != null) { 
        //     $application_form->school_name = $request->vchSchoolName;
        // }
        $application_form->application_status = 1;
        $application_form->state = 'Jharkhand';
        $application_form->division = Fnd_value::where('id', $request->district)->value('parent_value_id');
        $application_form->district = $userData['district'];
        $application_form->block = $userData['block'];
        $application_form->save();

        $address = new Address;
        $address->school_id = $getLast_id->id;
        $address->user_id = $user_id->id;
        $address->school_address = $request->user_address;
        $address->division = $application_form->division;
        $address->district = $application_form->district;
        $address->block = $application_form->block;
        $address->school_state = $application_form->state;
        $address->created_by = -1;
        $address->save();

        $contact = new Contact;
        $contact->school_id = $getLast_id->id;
        $contact->user_id = $user_id->id;
        $contact->school_email = $request->email;
        $contact->mobile = $request->contact;
        $contact->created_by = -1;
        $contact->save();



        $application_form_ext = new application_data_ext_1;
        $application_form_ext->application_data_id  = $application_form->id;
        $application_form_ext->school_id = $getLast_id->id;
        $application_form_ext->save();

        $application_teacher_data = new application_teacher_data;
        $application_teacher_data->application_data_id = $application_form->id;
        $application_teacher_data->school_id  = $getLast_id->id;
        $application_teacher_data->save();

        $application_data_ext_files = new application_data_ext_file;
        $application_data_ext_files->application_data_id = $application_form->id;
        $application_data_ext_files->school_id = $getLast_id->id;
        $application_data_ext_files->save();

        $application_data_files = new application_data_file;
        $application_data_files->created_by  = -1;
        $application_data_files->application_data_id = $application_form->id;
        $application_data_files->school_id = $getLast_id->id;
        $application_data_files->save();

        $application_data_remarks = new application_data_remarks;
        $application_data_remarks->created_by_rmk  = -1;
        $application_data_remarks->application_data_id = $application_form->id;
        $application_data_remarks->school_id = $getLast_id->id;
        $application_data_remarks->save();

        $application_data_remarks_ext_1s = new application_data_remarks_ext_1;
        $application_data_remarks_ext_1s->application_data_id = $application_form->id;
        $application_data_remarks_ext_1s->school_id = $getLast_id->id;
        $application_data_remarks_ext_1s->save();

        $application_data_verifications = new application_data_verification;
        $application_data_verifications->created_by_vrfy  = -1;
        $application_data_verifications->application_data_id = $application_form->id;
        $application_data_verifications->school_id = $getLast_id->id;
        $application_data_verifications->save();

        $application_data_verification_ext_1s = new application_data_verification_ext_1;
        $application_data_verification_ext_1s->application_data_id   = $application_form->id;
        $application_data_verification_ext_1s->school_id = $getLast_id->id;
        $application_data_verification_ext_1s->save();



        // email data
        // $email_data = array(
        //     'name' => $request->name,
        //     'email' => $request->email
        // );

        // send email with the template
        // Mail::send('welcome_email', $email_data, function ($message) use ($email_data) {
        //     $message->to($email_data['email'], $email_data['name'])
        //         ->subject('JEPC Rte Application')
        //         ->from('rambabuyadav1994@gmail.com', 'RTE');
        // });
        // $mobile = 8624937510;;
        // $senderId = "ram yadav";
        // $rndno = rand(1000, 9999);
        // //$rndno=$this->generateRandomString();
        // $request->session()->put('otp', $rndno);
        // $message = urlencode("Your otp number:" . $rndno);
        // $route = "route=4";
        // $authkey = "237996AsRwzrXy7U5b9f5768";
        // $postData = array(
        //     'authkey' => $authkey,
        //     'mobiles' => $mobile,
        //     'message' => $message,
        //     'sender' => $senderId,
        //     'route' => $route
        // );
        // $url = "https://control.msg91.com/api/sendhttp.php";

        // $ch = curl_init();
        // curl_setopt_array($ch, array(
        //     CURLOPT_URL => $url,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_POST => true,
        //     CURLOPT_POSTFIELDS => $postData
        // ));

        // $output = curl_exec($ch);
        // if (curl_errno($ch)) {
        //     echo 'error:' . curl_error($ch);
        // }
        // curl_close($ch);
        $this->testSuccess($request->contact, $request->email, $request->password);


        return response()->json(['message' => 'You have successfully register.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
    public function checkEmail(Request $request)
    {
        $userData = User::where('email', $request->email)->first();

        if (!empty($userData)) {
            return response()->json(['Msg' => 'Email already exist', 'ChKval' => 1]);
        } else {
            return response()->json(['ChKval' => 0]);
        }
    }
    public function checkUsername(Request $request)
    {
        $userData = User::where('username', $request->username)->first();

        if (!empty($userData)) {
            return response()->json(['Msg' => 'Username already exist', 'ChKval' => 1]);
        } else {
            return response()->json(['ChKval' => 0]);
        }
    }


    public function testsms($phone)
    {
        // return ($phone);
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        // $message = "Your Normal message here "; //message content
        // $messageUnicode = "मोबाइल सेवा में आपका स्वागत है "; //message content in unicode
        // session_start();
        // $_SESSION['phone_otp'] = $otp = rand(1111,9999);
        $otp = rand(1111, 9999);
        Session::put('phone_otp', $otp);
        $message = "Your OTP to verify your phone number is: " . $otp . ", Use this OTP to register."; //message content in unicode
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $mobileNos = "8797490173,8797490173"; //if bulk sms need to send use mobileNos as keyword and mobile number seperated by commas as value
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $templeteId = 1307161787218600805; // template id
        //call method and pass value to send single sms, uncomment next line to use
        // $this->sendSingleSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
        //$obj= $otp;
        //call method and pass value to send otp sms, uncomment next line to use
        $valx = $this->sendOtpSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey, $templeteId);

        //call this method and pass value to send bulk sms, uncomment next line to use
        //$this->sendBulkSMS($username,$encryp_password,$senderid,$message,$mobileNos,$deptSecureKey);

        //call this method for sending single unicode sms, uncomment next line to use
        //$this->sendSingleUnicode($username,$encryp_password,$senderid,$messageUnicode,$mobileno,$deptSecureKey);

        //call this method for sending single unicode otp sms, uncomment next line to use
        //$this->sendUnicodeOtpSMS($username,$encryp_password,$senderid,$messageUnicode,$mobileno,$deptSecureKey);

        //call this method to send bulk unicode sms, uncomment next line to use
        //$this->sendBulkUnicode($username,$encryp_password,$senderid,$messageUnicode,$mobileNos,$deptSecureKey);
        return response()->json(['OTP is' => $otp, "data" => $valx]);
    }

    function testSuccess($phone, $email, $password_1)
    {

        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment

        $message = "You have successfully registered for Private School Recognition System. For login please use folowing credentials, Username: " . $email . " Password: " . $password_1 . "."; //message content in unicode
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $templeteId = 1307161787218600805; // template id
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey, $templeteId,);
    }

    //function to send sms using by making http connection
    function post_to_url($url, $data)
    {
        // return($url);
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }

        rtrim($fields, '&');
        $post = curl_init();

        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($post); //result from mobile seva server
        json_encode($result);
        //  echo $result; //output from server displayed
        //    return response()->json('message' );
        curl_close($post);

        return $result;
    }

    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');

        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        return response()->json($result = curl_exec($post)); //result from mobile seva server
        curl_close($post);
    }

    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }

    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }

    //Function to send single sms
    function sendSingleSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey, $templeteId)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "singlemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key),
            "templateid" => $templeteId,

        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url to send sms
    }

    //Function to send otp sms
    function sendOtpSMS($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey, $templeteId)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "otpmsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key),
            "templateid" => $templeteId,
            //"otp"=>$otp
        );
        $return = $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url to send otp sms
        return $return;
    }


    //function to send bulk sms
    function sendBulkSMS($username, $encryp_password, $senderid, $message, $mobileNos, $deptSecureKey, $templeteId)
    {
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "bulkmsg",
            "bulkmobno" => trim($mobileNos),
            "key" => trim($key),
            "templateid" => $templeteId,
        );
        $this->post_to_url("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url to send bulk sms

    }

    //function to send single unicode sms
    function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey, $templeteId)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key),
            "templateid" => $templeteId,
        );

        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url_unicode to send single unicode sms
    }

    //function to send bulk unicode sms
    function sendBulkUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileNos, $deptSecureKey, $templeteId)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "bulkmobno" => trim($mobileNos),
            "key" => trim($key),
            "templateid" => $templeteId,
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url_unicode to send bulk unicode sms
    }

    //function to send single unicode otp sms
    function sendUnicodeOtpSMS($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey, $templeteId)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodeotpmsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key),
            "templateid" => $templeteId,
        );

        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT", $data); //calling post_to_url_unicode to send single unicode sms
    }

    // block list....
    public function  blockList($id)
    {

        $Districts = Fnd_value::where('fnd_values.parent_value_id', '=', $id)
            ->pluck(
                'fnd_values.display_value',
                'fnd_values.id'
            );

        return json_encode($Districts);
    }
    // district list
    public function  districtList($id)
    {

        $Districts = Fnd_value::where('fnd_values.parent_value_id', '=', $id)
            ->pluck(
                'fnd_values.display_value',
                'fnd_values.id'
            );
        // $cities = DB::table("demo_cities")
        //             ->where("state_id",$id)
        //             ->lists("name","id");
        return json_encode($Districts);


        // $Users = User::all();
    }

    public function syncOrders()
    {
        if (auth()->check()) {
            Order::truncate();

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-evidyavahini.jharkhand.gov.in/api/getToken",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 600,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                //echo "cURL Error #:" . $err;
            } else {
                echo $response;
            }
        } else {
            return response('/');
        }
    }
}
