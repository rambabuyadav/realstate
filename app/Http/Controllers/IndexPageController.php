<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\About_us;
use App\Models\Faqs;
use App\Models\District_email;
use App\Models\Deo_email;
use App\Models\Contact_us;
use App\Models\GuideLine;
use App\Models\Slider;
use App\Models\HonblePerson;
use Auth;
use Carbon\Carbon;
use DB;
class IndexPageController extends Controller
{
    function footer()
    {
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        return view('home.footer', $this->data);
        return "Comming Soon";
    }
    function View_Home_Page()
    {
        
        return view('home.home_property');
       
    }
    function View_About_Us()
    {
        $this->data['About_us'] = About_us::select(
            'about_us.id',
            'about_us.text',
            'about_us.image',
            'about_us.name',
            'about_us.designation',
            'about_us.department',
            'about_us.created_by',
        )->first();
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        return view('home.about-us', $this->data);
    }
    function View_FAQs()
    {
        $this->data['FAQs'] = Faqs::where('status', 1)->get();
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        return view('home.faq', $this->data);
    }
    function View_DSE()
    {
        $this->data['DSE_contact'] = District_email::where('status', 1)->get();
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        return view('home.dse-contact', $this->data);
    }
    function View_DEO()
    {
        $this->data['DEO_contact'] = Deo_email::where('status', 1)->get();
        $this->data['Contact_us'] = Contact_us::where('status', 1)->first();
        return view('home.deo-contact', $this->data);
    }
    function ViewAboutUs()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $About_us = About_us::select(
                    'about_us.id',
                    'about_us.text',
                    'about_us.image',
                    'about_us.name',
                    'about_us.designation',
                    'about_us.department',
                    'about_us.created_by',
                    'about_us.created_at'
                )->first();
                return view('view-about-us', compact('About_us'));
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function UpdateAboutUs(Request $req)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $About_us = About_us::find($req->id);
                $About_us->text = $req->text;
                $About_us->name = $req->name;
                $About_us->designation = $req->designation;
                $About_us->department = $req->department;
                $About_us->updated_by = Auth::user()->user_id;
                if ($req->has('image')) {
                    $image = $req->image;
                    $fileName = time() . '.' . $req->image->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $req->image->move(public_path('assets/images'), $fileName);
                    $path = 'public/assets/images/' . $fileName;
                    $About_us->image = $path;
                    // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
                }
                $About_us->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('view-about-us', compact('About_us'));
        // return redirect()->back()->with('success', 'Update Successfully !!');
    }
    function ViewFAQs()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $FAQs = Faqs::get();
                // return  $FAQs;
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('view-faq', compact('FAQs'));
    }
    function EditFAQs($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $FAQs = Faqs::where('id', $id)->first();
                // return  $FAQs;
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('editFAQ', compact('FAQs'));
    }
    function UpdateFAQ(Request $req)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $FAQs = Faqs::find($req->id);
                $FAQs->heading = $req->heading;
                $FAQs->text = $req->text;
                $FAQs->updated_by = Auth::user()->user_id;
                $FAQs->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function AddFAQ()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('Add-FAQ');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function InsertFAQ(Request $req)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $FAQs = new Faqs;
                $FAQs->heading = $req->heading;
                $FAQs->text = $req->text;
                $FAQs->created_by = Auth::user()->user_id;
                $FAQs->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function DeleteFAQ($id)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $FAQs = FAQs::find($id);
                if ($FAQs->status == 0) {
                    $FAQs->status = 1;
                } else {
                    $FAQs->status = 0;
                }
                $FAQs->updated_by = Auth::user()->user_id;
                $FAQs->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function Add_DSE()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('Add-DSE');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function Add_DEO()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('Add-DEO');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function DSEList()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $DSE_contact = District_email::get();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('DSEList', compact('DSE_contact'));
    }
    function Edit_DSE($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $DSE_contact = District_email::where('id', $id)->first();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('editDSE', compact('DSE_contact'));
    }
    function Update_DSE(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                if (District_email::where('email', '=', $req->email)->where('id', '!=', $req->id)->exists()) {
                    return redirect('editDSE/' . $req->id)->with('warning', 'E-mail Id Already Exist !!!');
                } else {
                    $District_email = District_email::find($req->id);
                    $District_email->district_id = $req->disrtict;
                    $District_email->email = $req->email;
                    $District_email->updated_by = Auth::user()->user_id;
                    $District_email->save();
                }
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function Insert_DSE(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                if (District_email::where('email', '=', $req->email)->exists()) {
                    return redirect('DSE-List')->with('warning', 'E-mail Id Already Exist !!!');
                } else {
                    $District_email = new District_email;
                    $District_email->district_id = $req->disrtict;
                    $District_email->email = $req->email;
                    $District_email->created_by = Auth::user()->user_id;
                    $District_email->save();
                }
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function DeleteDSE($id)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $District_email = District_email::find($id);
                if ($District_email->status == 0) {
                    $District_email->status = 1;
                } else {
                    $District_email->status = 0;
                }
                $District_email->updated_by = Auth::user()->user_id;
                $District_email->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function DEOList()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $DEO_contact = Deo_email::get();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('DEOList', compact('DEO_contact'));
    }
    function Edit_DEO($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $DSE_contact = Deo_email::where('id', $id)->first();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('editDEO', compact('DSE_contact'));
    }
    function Update_DEO(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                if (Deo_email::where('email', '=', $req->email)->where('id', '!=', $req->id)->exists()) {
                    return redirect('editDEO/' . $req->id)->with('warning', 'E-mail Id Already Exist !!!');
                } else {
                    $District_email = Deo_email::find($req->id);
                    $District_email->district_id = $req->disrtict;
                    $District_email->email = $req->email;
                    $District_email->updated_by = Auth::user()->user_id;
                    $District_email->save();
                }
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function Insert_DEO(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                if (Deo_email::where('email', '=', $req->email)->exists()) {
                    // user found
                    return redirect('DEO')->with('warning', 'E-mail Id Already Exist !!!');
                } else {
                    $District_email = new Deo_email;
                    $District_email->district_id = $req->disrtict;
                    $District_email->email = $req->email;
                    $District_email->created_by = Auth::user()->user_id;
                    $District_email->save();
                }
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function DeleteDEO($id)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Deo_email = Deo_email::find($id);
                if ($Deo_email->status == 0) {
                    $Deo_email->status = 1;
                } else {
                    $Deo_email->status = 0;
                }
                $Deo_email->updated_by = Auth::user()->user_id;
                $Deo_email->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function ContactUs()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Contact_us = Contact_us::where('status', 1)->first();
            }
        } else {
            return redirect('dashboard')->with('warning', 'You are not Authorised!!');
        }
        // return $Contact_us;
        return view('view-contact-us', compact('Contact_us'));
    }
    function UpdateContactUs(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Contact_us = Contact_us::find($req->id);
                $Contact_us->heading = $req->heading;
                $Contact_us->phone = $req->phone;
                $Contact_us->email = $req->email;
                $Contact_us->facebook_link = $req->facebook_link;
                $Contact_us->instagram_link = $req->instagram_link;
                $Contact_us->twitter_link = $req->twitter_link;
                $Contact_us->updated_by = Auth::user()->user_id;
                $Contact_us->save();
            }
        } else {
            return redirect('dashboard')->with('warning', 'You are not Authorised!!');
        }
        return redirect()->back()->with('success', 'Update Successfully !!');
    }
    function GuideLineList()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $GuideLine_List = GuideLine::get();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return view('GuideLineList', compact('GuideLine_List'));
    }
    function EditGuideLine($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $GuideLine = GuideLine::where('id', $id)->first();
                return view('editGuideLine', compact('GuideLine'));
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function AddGuideLine()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('Add-GuideLine');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function InsertGuideLine(Request $req)
    {
        // return $req;
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $GuideLine = new GuideLine;
                $GuideLine->heading = $req->heading;
                if ($req->has('document')) {
                    $fileInstanceSA = $req->document;
                    $newFileNameSA = date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                    $fileInstanceSA->move(public_path('assets/Guideline/'), $newFileNameSA);
                    $GuideLine->document = $newFileNameSA;
                }
                $GuideLine->created_by =  Auth::user()->user_id;
                $GuideLine->save();
                return redirect()->back();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function UpdateGuideLine(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $GuideLine =  GuideLine::find($req->id);
                $GuideLine->heading = $req->heading;
                if ($req->has('document')) {
                    $fileInstanceSA = $req->document;
                    $newFileNameSA = date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                    $fileInstanceSA->move(public_path('assets/Guideline/'), $newFileNameSA);
                    $GuideLine->document = $newFileNameSA;
                }
                $GuideLine->created_by =  Auth::user()->user_id;
                $GuideLine->save();
                return redirect()->back();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function DeleteGuideLine($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $GuideLine = GuideLine::find($id);
                if ($GuideLine->status == 0) {
                    $GuideLine->status = 1;
                } else {
                    $GuideLine->status = 0;
                }
                $GuideLine->updated_by = Auth::user()->user_id;
                $GuideLine->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function SliderList()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Slider = Slider::get();
                // return $Slider;
                return view('Slider-List', ['Sliders' => $Slider]);
                // return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function EditSlider($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Slider = Slider::where('id', $id)->first();
                // return $Slider;
                return view('editSlider', compact('Slider'));
                return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function AddSlider()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('Add-Slider');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function InsertSlider(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return $req;
                $Slider = new Slider;
                $Slider->title =  $req->title;
                // $Slider->title =  $req->title ;
                $Slider->text =  $req->text;
                if ($req->has('document')) {
                    $document = $req->document;
                    $fileName = time() . '.' . $req->document->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $req->document->move(public_path('../assets/images'), $fileName);
                    $path = $fileName;
                    $Slider->image = $path;
                    // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
                }
                $Slider->created_by = Auth::user()->id;
                $Slider->save();
                // return $Slider;
                return redirect()->back();
                return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function UpdateSlider(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return $request;
                $Slider = Slider::find($request->id);
                $Slider->title =  $request->title;
                // $Slider->title =  $request->title ;
                $Slider->text =  $request->text;
                if ($request->has('document')) {
                    $document = $request->document;
                    $fileName = time() . '.' . $request->document->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $request->document->move(public_path('../assets/images'), $fileName);
                    $path = $fileName;
                    $Slider->image = $path;
                    // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
                }
                $Slider->updated_by = Auth::user()->id;
                $Slider->save();
                return redirect()->back();
                return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function DeleteSlider($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Slider = Slider::find($id);
                if ($Slider->status == 0) {
                    $Slider->status = 1;
                } else {
                    $Slider->status = 0;
                }
                $Slider->updated_by = Auth::user()->user_id;
                $Slider->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }
    function HonblePersonList()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $HonblePerson = HonblePerson::get();
                // return $HonblePerson;
                return view('Honble-Person', ['HonblePersons' => $HonblePerson]);
                return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function EditHonblePerson($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $HonblePerson = HonblePerson::where('id', $id)->first();
                // return $HonblePerson;
                return view('editHonblePerson', compact('HonblePerson'));
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function AddHonblePerson()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                return view('Add-Honble-Person');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function InsertHonblePerson(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $HonblePerson = new HonblePerson;
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:100',
                ]);
                if ($request->has('image')) {
                    $fileInstanceSA = $request->image;
                    $newFileNameSA = date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                    $fileInstanceSA->move(public_path('../assets/'), $newFileNameSA);
                    $HonblePerson->image = $newFileNameSA;
                }
                $HonblePerson->name = $request->name;
                $HonblePerson->designation = $request->designation;
                $HonblePerson->created_by =  Auth::user()->user_id;
                $HonblePerson->save();
                return redirect()->back();
                //    return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function UpdateHonblePerson(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $request->validate([
                    'image' => 'mimes:jpeg,png,jpg,gif,svg|max:100',
                ]);
                $HonblePerson = HonblePerson::find($request->id);
                $HonblePerson->name =  $request->name;
                // $HonblePerson->title =  $request->title ;
                $HonblePerson->designation =  $request->designation;
                if ($request->has('image')) {
                    $image = $request->image;
                    $fileName = time() . '.' . $request->image->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $request->image->move(public_path('../assets/'), $fileName);
                    $path = $fileName;
                    $HonblePerson->image = $path;
                    // User::where('users.id', $req->id)->update(['profile_photo_path' => $path]);
                }
                $HonblePerson->updated_by = Auth::user()->id;
                $HonblePerson->save();
                return redirect()->back();
                return "Comming Soon";
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    function DeleteHonblePerson($id)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $HonblePerson = HonblePerson::find($id);
                if ($HonblePerson->status == 0) {
                    $HonblePerson->status = 1;
                } else {
                    $HonblePerson->status = 0;
                }
                $HonblePerson->updated_by = Auth::user()->user_id;
                $HonblePerson->save();
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
        return redirect()->back();
    }

   
}
