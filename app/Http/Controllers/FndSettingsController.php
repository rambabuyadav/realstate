<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Fnd_value;
use App\Models\Fnd_value_set;
use App\Models\fnd_settings;
use App\Models\FndPage;
use App\Models\FndPageData;
use App\Models\FndUrl;
use App\Models\MenuSetting;

class FndSettingsController extends Controller
{
    function index()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state' || Auth::user()->user_role == 'superadmin') {
                // return view('fndSettings');
                //return view('userSetting');
                // $menuSetting = MenuSetting::where('status',1)
                //                             ->where('is_deleted',0)
                //                             ->get();
                // return view('menuSetting', ['data'=> $menuSetting]);
                return view('fndSettings.index');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    function menuSettings()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $menuSetting = MenuSetting::where('parent_menu',0)
                                            ->where('status',1)
                                            ->where('is_deleted',0)
                                            ->get();
                    //echo '<pre>'; print_r($menuSetting); exit;        
                return view('fndSettings.menus',['menuList'=>$menuSetting]);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    function submenuSettings()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $menuList = MenuSetting::where('parent_menu',0)
                                            ->where('status',1)
                                            ->where('is_deleted',0)
                                            ->get();
                $data['menu']  =  $menuList;                         
                $submenuList = MenuSetting::join('menu_settings as menu', 'menu.id', '=', 'menu_settings.parent_menu')
                                            ->where('menu_settings.parent_menu','<>',0)
                                            ->where('menu_settings.status',1)
                                            ->where('menu_settings.is_deleted',0)
                                            ->select('menu_settings.*','menu.menu_title as parent')
                                            ->get();                            
                $data['submenu']  =  $submenuList;                       
                return view('fndSettings.submenus',['data'=>$data]);
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    function storeMenu(Request $request){ 
        for ($i = 0; $i < count($request->menu_title); $i++) {
            //return($request); 
            if(isset($request) && isset($request->menu_id) && $request->menu_id[$i]!= ''){
                $menuSetting = MenuSetting::findorfail($request->menu_id[$i]);
                $menuSetting->updated_by = Auth::user()->id;
            }
            else{
            $menuSetting = new MenuSetting;
            }

            $menuSetting->menu_title = $request->menu_title[$i];
            $menuSetting->menu_url = $request->menu_url[$i];
            //$menuSetting->parent_menu = 0;// $request->parent_menu;
            //$menuSetting->nested_menu = 0;// $request->nested_menu;
            $menuSetting->status = $request->status[$i];
            $menuSetting->created_by = Auth::user()->id;

            $menuSetting->save();
            //$data = $menuSetting;//echo '<pre>'; print_r($data); exit;
        }
        // $menuSetting = MenuSetting::where('parent_menu',0)
        //                                     ->where('status',1)
        //                                     ->where('is_deleted',0)
        //                                     ->get();
        //return view('fndSettings.menus', ['menuList'=> $menuSetting]);
        return redirect('/fndSettings/menus/');

    }

    function storeSubMenu(Request $request){ 
        for ($i = 0; $i < count($request->menu_title); $i++) {
            //return($request); 
            if(isset($request) && isset($request->menu_id) && $request->menu_id[$i]!= ''){
                $menuSetting = MenuSetting::findorfail($request->menu_id[$i]);
                $menuSetting->updated_by = Auth::user()->id;
            }
            else{
            $menuSetting = new MenuSetting;
            }

            $menuSetting->menu_title = $request->menu_title[$i];
            $menuSetting->menu_url = $request->menu_url[$i];
            $menuSetting->parent_menu = $request->parent_menu;
            //$menuSetting->nested_menu = 0;// $request->nested_menu;
            $menuSetting->status = $request->status[$i];
            $menuSetting->created_by = Auth::user()->id;

            $menuSetting->save();
            MenuSetting::whereId($request->parent_menu)->update(['nested_menu'=> 1 , 'updated_by' =>  Auth::user()->id ,]);

            //$data = $menuSetting;//echo '<pre>'; print_r($data); exit;
        }
        return redirect('/fndSettings/submenus/');

    }

    function editMenu($id){ //return $id;
        $data = MenuSetting::whereId($id)->first();//echo '<pre>'; print_r($data); exit;  
        $menuSetting = MenuSetting::where('parent_menu',0)
                                            ->where('status',1)
                                            ->where('is_deleted',0)
                                            ->get();//echo '<pre>'; print_r($menuSetting); exit;  
                              
        return view('fndSettings.menus',['data'=>$data,'menuList'=>$menuSetting]);
    }
    function deleteMenu($id){ 
        MenuSetting::whereId($id)->update(['status'=> 0 ,'is_deleted'=>1,'deleted_at' =>  now(), 'deleted_by' =>  Auth::user()->id ,]);
        return redirect('/fndSettings/menus/');

    }



    //about us edit
    function aboutUsEdit(){
        return view('aboutUsEdit');

    }
    ########################## Manage Pages settings S####################
    //************  for Superadmin *************//
    function pageIndexDev(){
        $pages = FndPage::where('status',1)
                        ->where('is_deleted',0)
                        ->get();
        return view('fndSettings.pages.indexDev', ['data'=> $pages]); 
        //return view('fndSettings.pages.addPageUrl'); 
    }
    function addPageUrl(){
        return view('fndSettings.pages.addPageUrl'); 
    }
    function editPageUrl($id){
        $data['page'] = FndPage::whereId($id)->first();//echo '<pre>'; print_r($data['page']); exit;  
        $data['route'] = FndUrl::where('id',$data['page']->route_id)->first();//echo '<pre>'; print_r($data['route']); exit;  
        
        return view('fndSettings.pages.addPageUrl',['data'=>$data]); 
    }
    
    function insertPageUrl(Request $request){
        if(isset($request->id) && $request->id!=''){
            $fndPage  = FndPage::whereId($request->id)->first();
            $fndUrl = FndUrl::where('id',$fndPage->route_id)->first();//echo '<pre>'; print_r($fndUrl); exit;  
            $fndPage->updated_by = Auth::user()->id;
            $fndUrl->updated_by  = Auth::user()->id;

        }
        else{
            $fndPage = new FndPage();
            $fndUrl  = new FndUrl();
        }
        
        $fndUrl->route_name = $request->route_name;
        $fndUrl->unique_code = $request->route_unique_code;
        $fndUrl->url = $request->route_url;
        $fndUrl->status = $request->status;
        $fndUrl->created_by = Auth::user()->id;
        $fndUrl->save();

        $fndPage->route_id = $fndUrl->id;
        $fndPage->unique_code = $request->page_unique_code;
        $fndPage->name = $request->page_name;
        $fndPage->page_url = $request->route_url;
        $fndPage->description = $request->description;
        $fndPage->status = $request->status;
        $fndPage->created_by = Auth::user()->id;
        $fndPage->save();
        return redirect('fndSettings/pages/'); 

    }
    function listPageData($id){
        $list = FndPageData::leftJoin('fnd_pages','fnd_pages.id','fnd_page_data.page_id')
                            ->where('page_id',$id)
                            ->select('fnd_page_data.*','fnd_pages.name as page_name')
                            ->get();
        return view('fndSettings.pages.listPageData',['data'=>$list]); 

    }
    function addPageData($id){
        $data = FndPage::whereId($id)
        ->select('fnd_pages.*','fnd_pages.id as page_id','fnd_pages.name as page_name')
        ->first();
        return view('fndSettings.pages.addPageData',['data'=> $data]); 
    }
    function editPageData($id){
        $pageData = FndPageData::leftJoin('fnd_pages','fnd_pages.id','fnd_page_data.page_id')
                                ->where('fnd_page_data.id',$id)
                                ->select('fnd_page_data.*','fnd_page_data.id as page_data_id','fnd_pages.name as page_name')
                                ->first();
        if (Auth::user()->user_role == 'superadmin') {                       
            return view('fndSettings.pages.addPageData',['data'=> $pageData]); 
        }
        else {
            return view('fndSettings.pages.editPageContent',['data'=> $pageData]); 
        }
    }
    function insertPageData(Request $request){//print_r
        if(isset($request) && $request->page_data_id != ''){//return ('update'.Auth::user()->id);
            $pageData = FndPageData::whereId($request->page_data_id)->first();
            $pageData->updated_by = Auth::user()->id;
        }
        else{
            $pageData = new FndPageData();
        }
        $pageData->route_id  = $request->route_id;
        $pageData->page_id   = $request->page_id;
        $pageData->page_type = $request->page_type;
        $pageData->fa_icon   = $request->fa_icon;
        $pageData->fa_class  = $request->fa_class;
        $pageData->icon_name = $request->icon_name;
        
        $image_dir = preg_replace('/\s+/', '', $request->page_name);
        
        if($request->image){
            $image_name  = array();
            $image_type  = array();
            $image_title = array();
            if(!empty(array_filter($_FILES['image']['name']))) {
                // Loop through each file in files[] array
                foreach ($_FILES['image']['tmp_name'] as $key => $value) {
                    //echo $key;
                    $file_tmpname = $_FILES['image']['tmp_name'][$key];
                    $file_name = $_FILES['image']['name'][$key];
                    //$file_size = $_FILES['image']['size'][$key];
                    $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    //$maxsize = 2 * 1024 * 1024;
                    //if ($file_size > $maxsize){       
                    //return("Error: File size is larger than the allowed limit.");
                    //}
                    
                    $file_name_new = $image_dir.'_'.$request->page_type.'_'.time().$file_name;
                    $filepath = public_path('assets/images/'.$image_dir.'/').$file_name_new;
                    move_uploaded_file($file_tmpname, $filepath);
                    
                    array_push($image_name,   $file_name_new);   
                    array_push($image_type,   $file_ext);   
                    array_push($image_title,   $file_name_new);   
                    // if($key!= 0) {
                    //     $pageData->image_name  .= ','.$file_name_new ;
                    //     $pageData->image_type  .= ','.$file_ext ;
                    //     $pageData->image_title .= ','.$file_name_new ;
                    // }
                    // else {
                    //     $pageData->image_name  = $file_name_new ;
                    //     $pageData->image_type  = $file_ext ;
                    //     $pageData->image_title = $file_name_new ;
                    // }
                }
                $pageData->image_name     = json_encode($image_name );
                $pageData->image_type     = json_encode($image_type );
                $pageData->image_title    = json_encode($image_title);
            } 
        }//echo '<pre>';print_r($image_name);exit;
        
        $pageData->image_location = $image_dir;
        $pageData->link           = $request->link;
        $pageData->text           = $request->text;
        $pageData->heading        = $request->heading;

        //print_r($request->list_text);exit;
        
        if(isset($request->list_text) && $request->list_text[0] !=''){
            $list_type   = array();
            $list_format = array();
            $list_icon   = array();
            $list_text   = array();
            $list_url    = array();
            $list_title  = array();    
            for ($i = 0; $i < count($request->list_text); $i++) {//echo '<br>$i'.$request->list_text[$i];
                if($request->list_text[$i] !=''){ //if row is blank while updating , then no entry was updated against that index (if we want to delete that index)
                array_push($list_type,   $request->list_type[$i]);   
                array_push($list_format, $request->list_format[$i]); 
                array_push($list_icon,   $request->list_icon[$i]);   
                array_push($list_text,   $request->list_text[$i]);   
                array_push($list_url,    $request->list_url[$i]);    
                array_push($list_title,  $request->list_title[$i]);
                }
                
            }//print_r($list_text);exit;
            $pageData->list_type      = json_encode($list_type  );
            $pageData->list_format    = json_encode($list_format);
            $pageData->list_icon      = json_encode($list_icon  );
            $pageData->list_text      = json_encode($list_text  );
            $pageData->list_url       = json_encode($list_url   );
            $pageData->list_title     = json_encode($list_title );
        }
        $pageData->status         = $request->status;
        $pageData->created_by     = Auth::user()->id;

        $pageData->save();
        return redirect('fndSettings/pages/listPageData/'.$request->page_id); 
    }
    function getImage($id,$index) {
        $pageData = FndPageData::findOrFail($id);//echo ($leave->attachment);
        $image = json_decode($pageData->image_name);//print_r($pageData);exit;
         foreach($image as $key =>$value){//echo '<br>'.public_path('/leave_documents/' . $att);
            if($key == $index){
                return response()->download(public_path('/assets/images/' .$pageData->image_location.'/'.$value));
            }
         }
    }
    //************ *************//
    function pageIndex(){
        $pages = FndPage::leftJoin('fnd_urls','fnd_urls.id','fnd_pages.route_id')
                        ->where('fnd_pages.status',1)
                        ->where('fnd_pages.is_deleted',0)
                        ->select('fnd_pages.*','fnd_urls.route_name')
                        ->get();//echo '<pre>'; print_r($pages); exit;  
        return view('fndSettings.pages.index', ['data'=> $pages]);
    }
    function PageContentList($id){
        $list = FndPageData::leftJoin('fnd_pages','fnd_pages.id','fnd_page_data.page_id')
                            ->where('page_id',$id)
                            ->select('fnd_page_data.*','fnd_pages.name as page_name')
                            ->get();
        return view('fndSettings.pages.listPageData',['data'=>$list]); 
    }
    function updatePageContent(Request $request){
        $pageData = FndPageData::where('id',$request->id)->first();
        $pageData->route_id  = $request->route_id;
        $pageData->page_id   = $request->page_id;
        $pageData->page_type = $request->page_type;
        $pageData->fa_icon   = $request->fa_icon;
        $pageData->fa_class  = $request->fa_class;
        $pageData->icon_name = $request->icon_name;
        $image_dir = preg_replace('/\s+/', '', $request->page_name);
        if($request->image){
            $image_name  = array();
            $image_type  = array();
            $image_title = array();
            if(!empty(array_filter($_FILES['image']['name']))) {
                // Loop through each file in files[] array
                foreach ($_FILES['image']['tmp_name'] as $key => $value) {
                    //echo $key;
                    $file_tmpname = $_FILES['image']['tmp_name'][$key];
                    $file_name = $_FILES['image']['name'][$key];
                    //$file_size = $_FILES['image']['size'][$key];
                    $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    //$maxsize = 2 * 1024 * 1024;
                    //if ($file_size > $maxsize){       
                    //return("Error: File size is larger than the allowed limit.");
                    //}
                    
                    $file_name_new = $image_dir.'_'.$request->page_type.'_'.time().$file_name;
                    $filepath = public_path('assets/images/'.$image_dir.'/').$file_name_new;
                    move_uploaded_file($file_tmpname, $filepath);

                     
                    
                    // if($key!= 0) {
                    //     $pageData->image_name  .= ','.$file_name_new ;
                    //     $pageData->image_type  .= ','.$file_ext ;
                    //     $pageData->image_title .= ','.$file_name_new ;
                    // }
                    // else {
                    //     $pageData->image_name  = $file_name_new ;
                    //     $pageData->image_type  = $file_ext ;
                    //     $pageData->image_title = $file_name_new ;
                    // }
                }
                array_push($image_name,   $file_name_new);   
                array_push($image_type,   $file_ext);   
                array_push($image_title,   $file_name_new);  
            }
            $pageData->image_name     = json_encode($image_name );
            $pageData->image_type     = json_encode($image_type );
            $pageData->image_title    = json_encode($image_title); 
        }
        $pageData->image_location = $image_dir;
        $pageData->link           = $request->link;
        $pageData->text           = $request->text;
        $pageData->heading        = $request->heading;
        // $pageData->list_type      = $request->list_type;
        // $pageData->list_format    = $request->list_format;
        // $pageData->list_icon      = $request->list_icon;
        // $pageData->list_text      = $request->list_text;
        // $pageData->list_url       = $request->list_url;
        // $pageData->list_title     = $request->list_title;
        if(isset($request->list_text) && $request->list_text[0] !=''){
            $list_text   = array();
            $list_url    = array();
            $list_title  = array();    
            for ($i = 0; $i < count($request->list_text); $i++) {
                if($request->list_text[$i] !=''){ //if row is blank while updating , then no entry was updated against that index (if we want to delete that index)
                array_push($list_text,   $request->list_text[$i]);   
                array_push($list_url,    $request->list_url[$i]);    
                array_push($list_title,  $request->list_title[$i]);
                }
                
            }
            $pageData->list_text      = json_encode($list_text  );
            $pageData->list_url       = json_encode($list_url   );
            $pageData->list_title     = json_encode($list_title );
        }
        $pageData->status         = $request->status;
        $pageData->created_by     = Auth::user()->id;
        $pageData->updated_by = Auth::user()->id;

        // foreach($request as $key =>$value){
        //     if()
        // }

        $pageData->save();
        return redirect('fndSettings/pages/listPageData/'.$request->page_id); 
    }
    function addPage(){
        return view('fndSettings.pages.addPage');
    }


    ########################## End S####################

    // function organisationSubMenu(){
    //     return view('organisationSubMenus');
    // }

    // function constituentSubMenu(){
    //     return view('constituentSubMenus');
    // }
    
    // function programmeSubMenu(){
    //     return view('programmeSubMenus');
    // }

    
    //Organisation edit
    function managementEdit(){
        return view('managementEdit');
    }
    function seniorFunEdit(){
        return view('seniorFunEdit');
    }
    function disclosureRtiEdit(){
        return view('disclosureRtiEdit');
    }
    function progAdvEdit(){
        return view('progAdvEdit');
    }
    //constituent edit
    function nieEdit(){
        return view('nieEdit');
    }
    function rieEdit(){
        return view('rieEdit');
    }
    function cietEdit(){
        return view('cietEdit');
    }
    function pciveEdit(){
        return view('pciveEdit');
    }
     //programmes edit
     function ntseEdit(){
        return view('ntseEdit');
    }
    function aisesEdit(){
        return view('aisesEdit');
    }
    function tiaEdit(){
        return view('tiaEdit');
    }
    function jnnseEdit(){
        return view('jnnseEdit');
    }
    function mediaEdit(){
        return view('mediaEdit');
    }
    function researchGrantsEdit(){
        return view('researchGrantsEdit');
    }
    //Gallery edit
    function yogaDayEdit(){
        return view('yogaDayEdit');
    }
    function KalaUtsavEdit(){
        return view('KalaUtsavEdit');
    }
    function foundationDayEdit(){
        return view('foundationDayEdit');
    }
    function erakshaEdit(){
        return view('erakshaEdit');
    }
    function healthWellnessEdit(){
        return view('healthWellnessEdit');
    }
    //Publication edit
    function publicationEdit(){
        return view('publicationEdit');
    }
    function epubEdit(){
        return view('epubEdit');
    }
    function flipbookEdit(){
        return view('flipbookEdit');
    }
    function pdfEdit(){
        return view('pdfEdit');
    }
    function stateEbookEdit(){
        return view('stateEbookEdit');
    }
    function vocationalEdit(){
        return view('vocationalEdit');
    }
    //Announcement edit
    function vacancyEdit(){
        return view('vacancyEdit');
    }
    function tenderEdit(){
        return view('tenderEdit');
    }
    function noticeEdit(){
        return view('noticeEdit');
    }
    function otherAnnouncementEdit(){
        return view('otherAnnouncementEdit');
    }
    function seminarWorkshopEdit(){
        return view('seminarWorkshopEdit');
    }
    //Contact edit
    function personContactEdit(){
        return view('personContactEdit');
    }
    function publicInfoEdit(){
        return view('publicInfoEdit');
    }
    function telDirectoryEdit(){
        return view('telDirectoryEdit');
    }
    

    // Division add,edit,delete start
    function divisionList()
    {
        $Division = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value',
            'fnd_values.created_at'
        )->where('fnd_values.value_set_id', '=', 1)
            ->where('fnd_values.status', '=', 1)
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        return view('pdivisionList', compact('Division'));
    }
    public function addDivision()
    {
        return view('addDivision');
    }
    public function saveDivisionData(Request $request)
    {
        $Fnd_value_division = new Fnd_value;
        $Fnd_value_division->value_set_id = 1;
        $Fnd_value_division->display_value = $request->division;
        $Fnd_value_division->enabled_flag = 1;
        $Fnd_value_division->status = 1;
        $Fnd_value_division->created_by = Auth::user()->id;
        $Fnd_value_division->created_at = date('Y-m-d H:i:s');
        $Fnd_value_division->save();
        $Fnd_value_set_division = new Fnd_value_set;
        $Fnd_value_set_division->value_set_name = $request->division;
        $Fnd_value_set_division->parent_value_set_id = 1;
        $Fnd_value_set_division->parent_value_id = $Fnd_value_division->id;
        $Fnd_value_set_division->status = 1;
        $Fnd_value_set_division->created_by = Auth::user()->id;
        $Fnd_value_set_division->created_at = date('Y-m-d H:i:s');
        $Fnd_value_set_division->save();
        return redirect('divisionList')->with('success', 'Division Add Successfully !!!');
    }
    public function editDivision($id)
    {
        $Division = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.parent_value_id', '=', 'fnd_values.id')
            ->select(
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name',
                'fnd_values.id as value_id',
                'fnd_values.display_value'
            )->where('fnd_values.id', $id)->first();
        return view('editDivision', ['Division' => $Division]);
    }
    public function updateDivisionData(Request $req)
    {
        $Fnd_value = Fnd_value::find($req->division_id);
        $Fnd_value->display_value = $req->division;
        $Fnd_value->save();
        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id', $Fnd_value->value_set_id)->where('parent_value_id', $Fnd_value->id)->first();
        $Fnd_value_set->value_set_name = $req->division;
        $Fnd_value_set->save();
        return redirect('divisionList')->with('success', 'Division Update Successfully !!!');
    }
    public function deleteDivisionData($id)
    {
        $Fnd_value = Fnd_value::find($id);
        $Fnd_value->status = 0;
        $Fnd_value->save();
        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id', $Fnd_value->value_set_id)->where('parent_value_id', $Fnd_value->id)->first();
        $Fnd_value_set->status = 0;
        $Fnd_value_set->save();
        return redirect('divisionList')->with('warning', 'Division Deleted Successfully !!!');
    }
    // Division add,edit,delete end
    // District add,edit,delete start
    function districtList()
    {
        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');
        $District = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')
            ->select(
                'fnd_values.id as value_id',
                'fnd_values.display_value',
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name',
            )->whereIn('fnd_values.parent_value_id', $division_ids)
            ->where('fnd_values.status', 1)
            ->orderBy('fnd_value_sets.value_set_name', 'ASC')
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        // echo"<pre>";print_r($District);exit;
        return view('districtList', compact('District'));
    }
    public function deleteDistrictData($id)
    {
        $Fnd_value = Fnd_value::find($id);
        $Fnd_value->status = 0;
        $Fnd_value->save();
        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id', $Fnd_value->value_set_id)->where('parent_value_id', $Fnd_value->id)->first();
        $Fnd_value_set->status = 0;
        $Fnd_value_set->save();
        return redirect('districtList')->with('warning', 'District Deleted Successfully !!!');
    }
    public function editDistrict($id)
    {
        $District_id = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')
            ->select(
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name',
                'fnd_values.id as value_id',
                'fnd_values.display_value'
            )->where('fnd_values.id', '=', $id)->first();
        return view('editDistrict', ['District_id' => $District_id]);
    }
    public function updateDistrictData(Request $req)
    {
        $Fnd_value = Fnd_value::find($req->district_id);
        $Fnd_value->display_value = $req->district;
        $Fnd_value->save();
        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id', $Fnd_value->value_set_id)->where('parent_value_id', $Fnd_value->id)->first();
        $Fnd_value_set->value_set_name = $req->district;
        $Fnd_value_set->save();
        return redirect('districtList')->with('success', 'District Update Successfully !!!');
    }
    public function addDistrict()
    {
        $Divisions = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value',
        )->where('fnd_values.value_set_id', 1)
            ->where('fnd_values.status', 1)
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        return view('addDistrict', ['Divisions' => $Divisions]);
    }
    public function saveDistrictData(Request $request)
    {
        $Fnd_value_district = new Fnd_value;
        $Fnd_value_district->value_set_id = Fnd_value_set::where('parent_value_id', $request->division)->value('id');
        $Fnd_value_district->parent_value_id = $request->division; // fnd_values primary key
        $Fnd_value_district->display_value = $request->district;
        $Fnd_value_district->enabled_flag = 1;
        $Fnd_value_district->status = 1;
        $Fnd_value_district->created_by = Auth::user()->id;
        $Fnd_value_district->created_at = date('Y-m-d H:i:s');
        $Fnd_value_district->save();
        $Fnd_value_set_district = new Fnd_value_set;
        $Fnd_value_set_district->value_set_name = $request->district;
        $Fnd_value_set_district->parent_value_id = $Fnd_value_district->id;
        $Fnd_value_set_district->parent_value_set_id = $Fnd_value_district->value_set_id;
        $Fnd_value_set_district->status = 1;
        $Fnd_value_set_district->created_by = Auth::user()->id;
        $Fnd_value_set_district->created_at = date('Y-m-d H:i:s');
        $Fnd_value_set_district->save();
        return redirect('districtList')->with('success', 'District Add Successfully !!!');
    }
    // District add,edit,delete end
    // Block add,edit,delete start
    function blockList()
    {
        $division_ids = Fnd_value::where('fnd_values.value_set_id', 1)->pluck('id');
        $AllListData = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')
            ->leftjoin('fnd_values as block_list_fnd', 'block_list_fnd.parent_value_id', '=', 'fnd_values.id')
            ->select(
                'fnd_values.id as value_id',
                'fnd_values.display_value',
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name',
                'block_list_fnd.display_value as block_name',
                'block_list_fnd.id as block_id',
                'block_list_fnd.created_at as block_created_at'
            )->whereIn('fnd_values.parent_value_id', $division_ids)
            // ->where('block_list_fnd.parent_value_id', '=', 'fnd_values.id')
            ->where('block_list_fnd.status', 1)
            ->orderBy('fnd_value_sets.value_set_name', 'ASC')
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        return view('blockList', compact('AllListData'));
    }
    public function deleteBlockData($id)
    {
        $block_Fnd_value = Fnd_value::find($id);
        $block_Fnd_value->status = 0;
        $block_Fnd_value->save();
        $block_Fnd_value_set =  Fnd_value_set::where('parent_value_set_id', $block_Fnd_value->value_set_id)->where('parent_value_id', $block_Fnd_value->parent_value_id)->first();
        $block_Fnd_value_set->status = 0;
        $block_Fnd_value_set->save();
        return redirect('blockList')->with('warning', 'Block Deleted Successfully !!!');
    }
    public function editBlock($id)
    {
        $Block_id = Fnd_value::leftjoin('fnd_value_sets', 'fnd_value_sets.id', '=', 'fnd_values.value_set_id')
            ->select(
                'fnd_values.id as value_id',
                'fnd_values.display_value',
                'fnd_value_sets.id as value_sets_id',
                'fnd_value_sets.value_set_name'
            )
            ->where('fnd_values.id', $id)->first();
        return view('editBlock', ['Block_id' => $Block_id]);
    }
    public function updateBlockData(Request $req)
    {
        $Fnd_value = Fnd_value::find($req->block_id);
        $Fnd_value->display_value = $req->block;
        $Fnd_value->save();
        $Fnd_value_set =  Fnd_value_set::where('parent_value_set_id', $Fnd_value->value_set_id)->where('parent_value_id', $Fnd_value->parent_value_id)->first();
        $Fnd_value_set->value_set_name = $req->block;
        $Fnd_value_set->save();
        return redirect('blockList')->with('success', 'Update Block Successfully !!!');
    }
    public function  getdistrict($id)
    {
        $Districts = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value'
        )->where('fnd_values.parent_value_id', '=', $id)
            ->get();
        return json_encode($Districts);
        // $Users = User::all();
        return view('addBlock', compact('Districts'));
    }
    public function addBlock()
    {
        $Divisions = Fnd_value::select(
            'fnd_values.id',
            'fnd_values.display_value',
        )->where('fnd_values.value_set_id', 1)
            ->where('fnd_values.status', 1)
            ->orderBy('fnd_values.display_value', 'ASC')
            ->get();
        return view('addBlock', ['Divisions' => $Divisions]);
    }
    public function saveBlockData(Request $request)
    {
        $Fnd_value_block = new Fnd_value;
        $Fnd_value_block->value_set_id = Fnd_value_set::where('parent_value_set_id', Fnd_value::where('id', $request->district)->value('value_set_id'))->where('parent_value_id', $request->district)->value('id');
        $Fnd_value_block->parent_value_id = $request->district; // fnd_values primary key
        $Fnd_value_block->display_value = $request->block;
        $Fnd_value_block->enabled_flag = 1;
        $Fnd_value_block->status = 1;
        $Fnd_value_block->created_by = Auth::user()->id;
        $Fnd_value_block->created_at = date('Y-m-d H:i:s');
        $Fnd_value_block->save();
        return redirect('blockList')->with('success', 'Block Add Successfully !!!');
    }
    // Block add,edit,delete end
    //   %%%%%%%%%%%%%%%%%%%%%%%% Below function is use foer view only %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    public function addEditFndsettings()
    {
        $fnd_settingsget = fnd_settings::select(
            'id',
            'fnd_settings.verification_by_dse_limit',
            'fnd_settings.schedule_meeting_by_dc_limit',
            'fnd_settings.take_decision_after_meeting_held_by_dc',
            'fnd_settings.faa_decision_limit',
            'fnd_settings.saa_decision_limit',
            'fnd_settings.school_appeal_limit',
            'fnd_settings.school_second_appeal_after_rejection_limit',
        )
            ->where('status', 1)
            ->first();
        // return($fnd_settingsget);
        return view('addEditFndsettings', ['fnd_settingsget' => $fnd_settingsget]);
    }
    // %%%%%%%%%%%%%%%%%%%%%% below funtion is use for add and edit fnd setting %%%%%%%%%%%%%%%%%%%%%%%%%
    public function saveFndsettings(Request $request)
    {
        // return($request);
        if (isset($request->id) && $request->id != null) {
            fnd_settings::where('id', $request->id)->where('status', 1)->update(['status' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
        }
        $Fndsetting = new fnd_settings;
        $Fndsetting->verification_by_dse_limit = $request->verification_by_dse_limit; // fnd_values primary key
        $Fndsetting->schedule_meeting_by_dc_limit = $request->schedule_meeting_by_dc_limit;
        $Fndsetting->take_decision_after_meeting_held_by_dc = $request->take_decision_after_meeting_held_by_dc;
        $Fndsetting->faa_decision_limit = $request->faa_decision_limit;
        $Fndsetting->saa_decision_limit = $request->saa_decision_limit;
        $Fndsetting->school_appeal_limit = $request->school_appeal_limit;
        $Fndsetting->school_second_appeal_after_rejection_limit = $request->school_second_appeal_after_rejection_limit;
        $Fndsetting->created_by = Auth::user()->id;
        $Fndsetting->created_at = date('Y-m-d H:i:s');
        $Fndsetting->save();
        return redirect()->back()->with('success', 'Setting Add Successfully!!!');
    }

    // %%%%%%%%%%%%%%%%%%%%%% submenu pages %%%%%%%%%%%%%%%%%%%%%%%%%
    public function organisationSubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('organisationSetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function constituentSubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('constituentSetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function programmeSubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('programmeSetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function gallerySubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('gallerySetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function publicationSubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('publicationSetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function announcementSubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('announcementSetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function contactSubmenu(){
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return view('fndSettings');
                return view('contactSetting');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }


}
