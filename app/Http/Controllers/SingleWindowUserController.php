<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use App\Models\User;
use App\Models\Private_school;
use App\Models\single_window_user;
use Hash;
use Illuminate\Support\Facades\DB;
use App\Providers\JetstreamServiceProvider;
use Auth;
class SingleWindowUserController extends Controller
{
    
    public function single_window_login(Request $request)
    {

        $custId = $request->custId;  // Customer registration reference no at Single window system
        // $pwd = $request->password;  // password of the user
        $first_name = $request->first_name; // First name of the user
        $last_name = $request->last_name; // Last name of the user
        $email = $request->email;  // Email id of the user
        $mobile = $request->mobile; // Mobile no of the user
        $department_id = $request->department_id; // Unique id mapped for department at Single Window system (Provided separately)
        $session_id = $request->session_id; // Unique id generated for each user at Single window system on login

        if ($user = single_window_user::where('custId', $custId)->where('status', 1)->where('is_registered', 1)->first()) {
            // return $user;
            $credentials = ([
                'email' => $user->email,
                'password' =>  $user->password
            ]);
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();

                $wordCount = $wordCountAll = $wordCountAcceptedByAllUsers = $wordCountA = $wordCountByDSC = $wordCountR =  $wordCountP = $wordCountAcpt = $AllDistrict = $Arr_Block_Filter = '';
                if (Auth::user()->user_role == 'saa') {
                    $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
                        ->select(
                            'application_data.school_name',
                            'addresses.block',
                            'addresses.district',
                            'appeals.created_at',
                            'appeals.appeal_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block_name',
                            'appeals.appeal_status AS status_name'
                        )
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->whereIn('appeals.appeal_status', [1, 2, 3])
                        ->orderBy('appeals.created_at', 'DESC')->get();
                    // return $AllDistrict;
                    $CountAllDistrict = $AllDistrict->count();
                    $all_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                    $pending_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->where('appeals.appeal_status', '1')->count();
                    $accepted_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->where('appeals.appeal_status', '2')->count();
                    $rejected_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->where('appeals.appeal_status', '3')->count();
                    $District = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.value_set_id', '=', 1)->get();
                    // return $District;
                    $DistrictCount = $District->count();
                    $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                    for ($i = 0; $i < $DistrictCount; $i++) {
                        $AllAppeal = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                        $CountAllApplication = $AllAppeal->count();
                        array_push($Total_Appeal, $CountAllApplication);
                        $AllPending = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 1)->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                        $AllAppealVerified = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 2)->get();
                        $CountAllVerified = $AllAppealVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllAppealReject = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 3)->get();
                        $CountAllReject = $AllAppealReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                    }
                    $Arr_District_Filter = [
                        'DistrictCount' => $DistrictCount,
                        'District' => $District,
                        'Total_Appeal' => $Total_Appeal,
                        'Total_Verification' => $Total_Verification,
                        'Total_Rejection' => $Total_Rejection,
                        'Total_Pending' => $Total_Pending
                    ];
                    $wordCount = [
                        'AllDistrict' => $AllDistrict,
                        'allappeals' => $all_appeals,
                        'pendingappeals' => $pending_appeals,
                        'acceptedappeals' => $accepted_appeals,
                        'rejectedAppeal' => $rejected_appeals,
                        'Arr_District_Filter' => $Arr_District_Filter
                    ];
                }
                if (Auth::user()->user_role == 'faa') {
                    $UserDivision = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.division')
                        ->get();
                    $User_Division = $UserDivision[0]->division;
                    $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->select(
                            'application_data.school_name',
                            'addresses.block',
                            'addresses.district',
                            'appeals.created_at',
                            'appeals.appeal_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block_name',
                            'appeals.appeal_status AS status_name'
                        )
                        ->whereIn('appeals.appeal_status', [1, 2, 3])
                        ->orderBy('appeals.created_at', 'DESC')->get();
                    // return $AllDistrict;
                    $CountAllDistrict = $AllDistrict->count();
                    $all_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                    $pending_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->where('appeals.appeal_status', '1')->count();
                    $accepted_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->where('appeals.appeal_status', '2')->count();
                    $rejected_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->where('appeals.appeal_status', '3')->count();
                    $District = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_Division)->get();
                    // return $District;
                    $DistrictCount = $District->count();
                    $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                    for ($i = 0; $i < $DistrictCount; $i++) {
                        $AllAppeal = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                        $CountAllApplication = $AllAppeal->count();
                        array_push($Total_Appeal, $CountAllApplication);
                        $AllPending = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 1)->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                        $AllAppealVerified = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 2)->get();
                        $CountAllVerified = $AllAppealVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllAppealReject = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 3)->get();
                        $CountAllReject = $AllAppealReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                    }
                    $Arr_District_Filter = [
                        'DistrictCount' => $DistrictCount,
                        'District' => $District,
                        'Total_Appeal' => $Total_Appeal,
                        'Total_Verification' => $Total_Verification,
                        'Total_Rejection' => $Total_Rejection,
                        'Total_Pending' => $Total_Pending
                    ];
                    $wordCount = [
                        'AllDistrict' => $AllDistrict,
                        'allappeals' => $all_appeals,
                        'pendingappeals' => $pending_appeals,
                        'acceptedappeals' => $accepted_appeals,
                        'rejectedAppeal' => $rejected_appeals,
                        'Arr_District_Filter' => $Arr_District_Filter
                    ];
                }
                if (Auth::user()->user_role == 'dse') {
                    $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending =  array();
                    $UserDistrict = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.district')
                        ->get();
                    $User_District = $UserDistrict[0]->district;
                    $Block = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_District)->get();
                    $BlockCount = $Block->count();
                    $AllDistrict = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->limit($BlockCount - 1)->get();
                    $Alldata = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', '>', '1')->get();
                    $wordCountAll = $Alldata->count();
                    $AlldataByDSC = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->whereIn('application_status', [2, 3])->get();
                    $wordCountByDSC = $AlldataByDSC->count();
                    $AlldataAccept = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', 4)->get();
                    $wordCountAcpt = $AlldataAccept->count();
                    $AlldataReject = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', 8)->get();
                    $wordCountR = $AlldataReject->count();
                    for ($i = 0; $i < $BlockCount; $i++) {
                        $AllApplication = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', '>', '1')->get();
                        $CountAllApplication = $AllApplication->count();
                        array_push($Total_Application, $CountAllApplication);
                        $AllReject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 8)->get();
                        $CountAllReject = $AllReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                        $AllVerified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 4)->get();
                        $CountAllVerified = $AllVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllPending = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->whereIn('application_status', [2, 3])->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                    }
                    $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                    // return $Arr_Block_Filter;
                    $All_Meeting_Data = DB::table('application_data')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                        ->select(
                            'application_data.school_name',
                            'application_data.created_at',
                            'application_data.block',
                            'application_data.district',
                            'application_data.application_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            'status.status_name AS status_name',
                            'application_meetings.meeting_date'
                        )
                        ->where('application_data.application_status', '5')
                        ->where('application_data.district', $User_District)
                        ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                        ->where('application_data.status', '1')->limit($BlockCount - 1)
                        ->get();
                    $wordCount = [
                        'All'                 => $wordCountAll,
                        'Active'              => $wordCountA,
                        'VerifiedByDSC'       => $wordCountByDSC,
                        'Reject'              => $wordCountR,
                        'Pending'              => $wordCountP,
                        'Accept'              => $wordCountAcpt,
                        'AllDistrict'         => $AllDistrict,
                        'Arr_Block_Filter'    => $Arr_Block_Filter,
                        'All_Meeting_Data' => $All_Meeting_Data
                    ];
                }
                if (Auth::user()->user_role == 'dc') {
                    $UserDistrict = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.district')
                        ->get();
                    $User_District = $UserDistrict[0]->district;
                    $Block = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_District)
                        ->get();
                    $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                    $BlockCount = $Block->count();
                    // return $BlockCount;
                    $Alldata = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->limit($BlockCount - 1)
                        ->where('application_status', '>', '1')
                        ->orderBy('created_at', 'ASC')->get();
                    $wordCountAll = $Alldata->count();
                    $AllVerified = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        // ->where('application_status', '4')->get();
                        ->where('application_status', 6)->get();
                    $AllVerifiedCount = $AllVerified->count();
                    $AllPending = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        // ->where('application_status', '4')->get();
                        ->where('application_status', 4)->get();
                    $AllPendingCount = $AllPending->count();
                    $Meeting = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', '5')->get();
                    $MeetingCount = $Meeting->count();
                    $Certified = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', '6')
                        ->limit($BlockCount - 1)
                        ->orderBy('created_at', 'DESC')->get();
                    $CertifiedCount = $Certified->count();
                    $Reject = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        // ->where('application_status', '7')->get();
                        ->where('application_status', 7)->get();
                    $RejectCount = $Reject->count();
                    $All_Meeting_Data = DB::table('application_data')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                        ->select(
                            'application_data.school_name',
                            'application_data.created_at',
                            'application_data.block',
                            'application_data.district',
                            'application_data.application_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            'status.status_name AS status_name',
                            'application_meetings.meeting_date'
                        )
                        ->where('application_data.application_status', '5')
                        ->where('application_data.district', $User_District)
                        ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                        ->where('application_data.status', '1')->limit($BlockCount - 1)
                        ->get();
                    for ($i = 0; $i < $BlockCount; $i++) {
                        $AllApplication = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', '>', '1')->get();
                        $CountAllApplication = $AllApplication->count();
                        array_push($Total_Application, $CountAllApplication);
                        $AllReject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 7)->get();
                        // ->where('application_status', '7')->get();
                        $CountAllReject = $AllReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                        $AllVerified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 6)->get();
                        // ->where('application_status', '6')->get();
                        $CountAllVerified = $AllVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllPending = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', '4')->get();
                        // ->whereIn('application_status', [2, 3, 4, 9, 12])->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                    }
                    $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                    // return($Arr_Block_Filter);
                    $wordCount = [
                        'All'           => $wordCountAll,
                        'AllDistrict' => $Alldata,
                        'AllVerifiedCount' => $AllVerifiedCount,
                        'AllPendingCount' => $AllPendingCount,
                        'MeetingCount' => $MeetingCount,
                        'CertifiedCount' => $CertifiedCount,
                        'RejectCount' => $RejectCount,
                        'AllCertified' => $Certified,
                        'Arr_Block_Filter' => $Arr_Block_Filter,
                        'All_Meeting_Data' => $All_Meeting_Data
                    ];
                }
                if (Auth::user()->user_role == 'state') {
                    $Total_Pending  = $Total_Approved = $Total_Application = $Total_Verification = $Total_Rejection = array();
                    $UserDivision = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.division')
                        ->get();
                    $User_Division = $UserDivision[0]->division;
                    $Division = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.value_set_id', '1')
                        ->get();
                    $DivisionCount = $Division->count();
                    $Alldata = DB::table('application_data')->where('status', '1')->where('application_data.application_status', '>', 1)->get();
                    $wordCountAll = $Alldata->count();
                    $AcceptedByAllUsers = DB::table('application_data')->where('status', '1')->whereIn('application_status', [4, 6, 10, 13])->get();
                    $wordCountAcceptedByAllUsers = $AcceptedByAllUsers->count();
                    $AlldataAccept = DB::table('application_data')->where('status', '1')->where('application_status', '6')->get();
                    $wordCountAcpt = $AlldataAccept->count();
                    $AlldataReject = DB::table('application_data')->where('status', '1')->whereIn('application_status', [7, 8, 11, 14])->get();
                    $wordCountR = $AlldataReject->count();
                    $AllDetails = DB::table('application_data')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->select(
                            'application_data.school_name',
                            'application_data.created_at',
                            'application_data.block',
                            'application_data.district',
                            'application_data.application_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            'status.status_name AS status_name'
                        )
                        ->where('application_data.status', '1')
                        ->where('application_data.application_status', '>', 1)
                        ->limit($DivisionCount)
                        ->orderBy('created_at', 'desc')->get();
                    $AllCertified = DB::table('application_data')->where('status', '1')->where('division', $User_Division)->where('application_status', '6')->get();
                    $CountAllCertified = $AllCertified->count();
                    for ($i = 0; $i < $DivisionCount; $i++) {
                        $AllApplication = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_data.application_status', '>', 1)->get();
                        $CountAllApplication = $AllApplication->count();
                        array_push($Total_Application, $CountAllApplication);
                        $AllReject = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->whereIn('application_status', [7, 8, 11, 14])->get();
                        $CountAllReject = $AllReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                        $AllPending = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->wherein('application_status', [2, 3, 4, 9, 12])->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                        $AllApproved = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_status', 6)->get();
                        $CountAllApproved = $AllApproved->count();
                        array_push($Total_Approved, $CountAllApproved);
                    }
                    $Arr_Block_Filter = ['BlockCount' => $DivisionCount, 'Block' => $Division, 'Total_Application' => $Total_Application, 'Total_Pending' => $Total_Pending, 'Total_Approved' => $Total_Approved, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection];
                    $wordCount = [
                        'All' => $wordCountAll,
                        'Active' => $wordCountA,
                        'VerifiedByAllUser' => $wordCountAcceptedByAllUsers,
                        'Reject' => $wordCountR,
                        'Pending' => $wordCountP,
                        'Accept' => $wordCountAcpt,
                        'AllCertified' => $AllCertified,
                        'AllDetails' => $AllDetails,
                        'Arr_Block_Filter' => $Arr_Block_Filter
                    ];
                }
                if (Auth::user()->user_role == 'school') {
                    // return 'true';
                    $Accepted_Fields = 0;
                    $Rejected_Fields = 0;
                    $pending_Fields = 0;
                    $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
                    foreach ($alltable_col as $key => $value) {
                        if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at" && $value != "created_by_vrfy" && $value != "updated_by_vrfy") {
                            if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                $Accepted_Fields++;
                            }
                            if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                $Rejected_Fields++;
                            }
                            if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                $pending_Fields++;
                            }
                        }
                    }
                    $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
                    // echo $alltable_col;
                    foreach ($alltable_ext_col as $key => $value) {
                        if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {
                            if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                $Accepted_Fields++;
                            }
                            if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                $Rejected_Fields++;
                            }
                            if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                $pending_Fields++;
                            }
                        }
                    }
                    $UserDistrict = DB::table('private_schools')
                        ->where('id', Auth::user()->school_id)
                        ->value('district');
                    $User_District = $UserDistrict;
                    $Block = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_District)
                        ->get();
                    $Application_Status = application_data::where('application_data.school_id', Auth::user()->school_id)
                        ->value('application_data.application_status');
                    // $acceptedted_feild = DB::table('application_data_verifications')
                    // ->select( DB::raw('count(school_name_vrfy,udise_code_vrfy) as accepted_count, school_name_vrfy') )
                    // ->where('application_data_verifications.school_id', Auth::user()->school_id)
                    // ->groupBy('school_name_vrfy')
                    // ->get();
                    // return $acceptedted_feild;
                    $Application_Tracking = DB::table('application_trackings')
                        ->Join('application_statuses', 'application_trackings.application_status_id', '=', 'application_statuses.id')
                        ->select(
                            'application_trackings.application_id',
                            'application_trackings.school_id',
                            'application_trackings.application_status_id',
                            'application_trackings.created_at',
                            'application_trackings.created_by',
                            'application_statuses.status_name AS status_name'
                        )
                        ->where('application_trackings.school_id', Auth::user()->school_id)
                        ->get();
                    $CountApplication_Tracking = $Application_Tracking->count();
                    $feedback = DB::table('application_data')
                        ->select(
                            'application_data.id',
                            'application_data.created_by',
                            'application_data.school_name',
                            'application_data.status',
                            'application_data.feedback_file_upload',
                            'application_data.feedback_from_dsc_to_school',
                            'application_data.verification_date'
                        )
                        ->where('application_data.school_id', Auth::user()->school_id)->where('application_data.status', 1)->get();
                    $All_Meeting_Data_school = DB::table('application_meetings')
                        ->Join('application_data', 'application_data.id', '=', 'application_meetings.application_id')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('users', 'users.id', '=', 'application_meetings.created_by')
                        // ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->select(
                            'application_meetings.meeting_date',
                            'application_meetings.meeting_remarks',
                            'application_data.school_name',
                            'users.name',
                            'application_meetings.created_at',
                            'application_meetings.meeting_text',
                            'application_meetings.meeting_remarks',
                            'application_meetings.meeting_documents',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            // 'status.status_name AS status_name'
                        )
                        // ->where('application_data.application_status', '5')
                        ->where('application_meetings.school_id', Auth::user()->school_id)
                        ->get();
                    foreach ($All_Meeting_Data_school as  $Meeting_Data_school) {
                        $Meeting_Data_school->meeting_documents = json_decode($Meeting_Data_school->meeting_documents);
                    }
                    // if ($All_Meeting_Data_school->meeting_documents != null)
                    // $All_Meeting_Data_school->meeting_documents = json_decode($All_Meeting_Data_school->meeting_documents, true);
                    if ($Application_Status == 1) {
                        $Accepted_Fields = 0;
                        $Rejected_Fields = 0;
                        $pending_Fields = 0;
                    }
                    $wordCount = [
                        'feedback' => $feedback,
                        'Application_Tracking' => $Application_Tracking,
                        'Accepted_Fields' => $Accepted_Fields,
                        'Rejected_Fields' => $Rejected_Fields,
                        'pending_Fields' => $pending_Fields,
                        'All_Meeting_Data_school' => $All_Meeting_Data_school,
                        'Application_Status' => $Application_Status
                    ];
                }
                // return [Auth::user()];
                return view('dashboard', compact('wordCount'));
            }
        } elseif ($user = single_window_user::where('custId', $custId)->where('status', 1)->where('is_registered', 0)->first()) {
            // return $user;
            $credentials = ([
                'email' => $user->email,
                'password' =>  $user->password
            ]);
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();

                // return [Auth::user()];
                return view('single_window_school_register')->with('data', $user);
            }
        } else {
            // return 0;
            $school = new Private_school;
            $school->school_name = $request->school_name;
            if(Private_school::where('school_email',$email)->count() > 0){
                return response()->json(['message'=> 'Mail Already Exist']);
            }
            $school->school_email = $email;
            // $school->udise_code = $request->udise_code;
            $school->contact_person = $first_name . ' ' . $last_name;
            $school->created_by = -1;
            $school->save();

            $user = new User;
            $user->school_id = $school->id;
            $user->name = $first_name . ' ' . $last_name;
            $user->username = $custId;
            $user->password = Hash::make($custId);
            if(User::where('email',$email)->count() > 0){
                return response()->json(['message'=> 'Mail Already Exist']);
            }
            $user->email = $email;
            $user->user_role = 'school';
            $user->save();

            $single_user = new single_window_user;
            $single_user->custId = $custId;
            $single_user->user_id = $user->id;
            $single_user->school_id = $school->id;
            $single_user->username = $custId;
            $single_user->password = $custId;
            $single_user->first_name = $first_name;
            $single_user->last_name = $last_name;
            $single_user->email = $email;
            $single_user->mobile = $mobile;
            $single_user->department_id = $department_id;
            $single_user->session_id = $session_id;
            $single_user->created_by = -1;
            $single_user->save();

            if ($user = single_window_user::where('custId', $custId)->where('status', 1)->where('is_registered', 0)->first()) {
                // return $user;
                $credentials = ([
                    'email' => $user->email,
                    'password' =>  $user->password
                ]);
                if (Auth::attempt($credentials)) {
                    $request->session()->regenerate();

                    $wordCount = $wordCountAll = $wordCountAcceptedByAllUsers = $wordCountA = $wordCountByDSC = $wordCountR =  $wordCountP = $wordCountAcpt = $AllDistrict = $Arr_Block_Filter = '';
                    if (Auth::user()->user_role == 'saa') {
                        $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
                            ->select(
                                'application_data.school_name',
                                'addresses.block',
                                'addresses.district',
                                'appeals.created_at',
                                'appeals.appeal_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block_name',
                                'appeals.appeal_status AS status_name'
                            )
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])
                            ->orderBy('appeals.created_at', 'DESC')->get();
                        // return $AllDistrict;
                        $CountAllDistrict = $AllDistrict->count();
                        $all_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                        $pending_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('appeals.appeal_status', '1')->count();
                        $accepted_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('appeals.appeal_status', '2')->count();
                        $rejected_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('appeals.appeal_status', '3')->count();
                        $District = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.value_set_id', '=', 1)->get();
                        // return $District;
                        $DistrictCount = $District->count();
                        $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                        for ($i = 0; $i < $DistrictCount; $i++) {
                            $AllAppeal = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                            $CountAllApplication = $AllAppeal->count();
                            array_push($Total_Appeal, $CountAllApplication);
                            $AllPending = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 1)->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                            $AllAppealVerified = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 2)->get();
                            $CountAllVerified = $AllAppealVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllAppealReject = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 3)->get();
                            $CountAllReject = $AllAppealReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                        }
                        $Arr_District_Filter = [
                            'DistrictCount' => $DistrictCount,
                            'District' => $District,
                            'Total_Appeal' => $Total_Appeal,
                            'Total_Verification' => $Total_Verification,
                            'Total_Rejection' => $Total_Rejection,
                            'Total_Pending' => $Total_Pending
                        ];
                        $wordCount = [
                            'AllDistrict' => $AllDistrict,
                            'allappeals' => $all_appeals,
                            'pendingappeals' => $pending_appeals,
                            'acceptedappeals' => $accepted_appeals,
                            'rejectedAppeal' => $rejected_appeals,
                            'Arr_District_Filter' => $Arr_District_Filter
                        ];
                    }
                    if (Auth::user()->user_role == 'faa') {
                        $UserDivision = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.division')
                            ->get();
                        $User_Division = $UserDivision[0]->division;
                        $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->select(
                                'application_data.school_name',
                                'addresses.block',
                                'addresses.district',
                                'appeals.created_at',
                                'appeals.appeal_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block_name',
                                'appeals.appeal_status AS status_name'
                            )
                            ->whereIn('appeals.appeal_status', [1, 2, 3])
                            ->orderBy('appeals.created_at', 'DESC')->get();
                        // return $AllDistrict;
                        $CountAllDistrict = $AllDistrict->count();
                        $all_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                        $pending_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->where('appeals.appeal_status', '1')->count();
                        $accepted_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->where('appeals.appeal_status', '2')->count();
                        $rejected_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->where('appeals.appeal_status', '3')->count();
                        $District = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_Division)->get();
                        // return $District;
                        $DistrictCount = $District->count();
                        $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                        for ($i = 0; $i < $DistrictCount; $i++) {
                            $AllAppeal = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                            $CountAllApplication = $AllAppeal->count();
                            array_push($Total_Appeal, $CountAllApplication);
                            $AllPending = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 1)->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                            $AllAppealVerified = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 2)->get();
                            $CountAllVerified = $AllAppealVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllAppealReject = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 3)->get();
                            $CountAllReject = $AllAppealReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                        }
                        $Arr_District_Filter = [
                            'DistrictCount' => $DistrictCount,
                            'District' => $District,
                            'Total_Appeal' => $Total_Appeal,
                            'Total_Verification' => $Total_Verification,
                            'Total_Rejection' => $Total_Rejection,
                            'Total_Pending' => $Total_Pending
                        ];
                        $wordCount = [
                            'AllDistrict' => $AllDistrict,
                            'allappeals' => $all_appeals,
                            'pendingappeals' => $pending_appeals,
                            'acceptedappeals' => $accepted_appeals,
                            'rejectedAppeal' => $rejected_appeals,
                            'Arr_District_Filter' => $Arr_District_Filter
                        ];
                    }
                    if (Auth::user()->user_role == 'dse') {
                        $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending =  array();
                        $UserDistrict = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.district')
                            ->get();
                        $User_District = $UserDistrict[0]->district;
                        $Block = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_District)->get();
                        $BlockCount = $Block->count();
                        $AllDistrict = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->limit($BlockCount - 1)->get();
                        $Alldata = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', '>', '1')->get();
                        $wordCountAll = $Alldata->count();
                        $AlldataByDSC = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->whereIn('application_status', [2, 3])->get();
                        $wordCountByDSC = $AlldataByDSC->count();
                        $AlldataAccept = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', 4)->get();
                        $wordCountAcpt = $AlldataAccept->count();
                        $AlldataReject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', 8)->get();
                        $wordCountR = $AlldataReject->count();
                        for ($i = 0; $i < $BlockCount; $i++) {
                            $AllApplication = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', '>', '1')->get();
                            $CountAllApplication = $AllApplication->count();
                            array_push($Total_Application, $CountAllApplication);
                            $AllReject = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 8)->get();
                            $CountAllReject = $AllReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                            $AllVerified = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 4)->get();
                            $CountAllVerified = $AllVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllPending = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->whereIn('application_status', [2, 3])->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                        }
                        $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                        // return $Arr_Block_Filter;
                        $All_Meeting_Data = DB::table('application_data')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                            ->select(
                                'application_data.school_name',
                                'application_data.created_at',
                                'application_data.block',
                                'application_data.district',
                                'application_data.application_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                'status.status_name AS status_name',
                                'application_meetings.meeting_date'
                            )
                            ->where('application_data.application_status', '5')
                            ->where('application_data.district', $User_District)
                            ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                            ->where('application_data.status', '1')->limit($BlockCount - 1)
                            ->get();
                        $wordCount = [
                            'All'                 => $wordCountAll,
                            'Active'              => $wordCountA,
                            'VerifiedByDSC'       => $wordCountByDSC,
                            'Reject'              => $wordCountR,
                            'Pending'              => $wordCountP,
                            'Accept'              => $wordCountAcpt,
                            'AllDistrict'         => $AllDistrict,
                            'Arr_Block_Filter'    => $Arr_Block_Filter,
                            'All_Meeting_Data' => $All_Meeting_Data
                        ];
                    }
                    if (Auth::user()->user_role == 'dc') {
                        $UserDistrict = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.district')
                            ->get();
                        $User_District = $UserDistrict[0]->district;
                        $Block = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_District)
                            ->get();
                        $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                        $BlockCount = $Block->count();
                        // return $BlockCount;
                        $Alldata = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->limit($BlockCount - 1)
                            ->where('application_status', '>', '1')
                            ->orderBy('created_at', 'ASC')->get();
                        $wordCountAll = $Alldata->count();
                        $AllVerified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            // ->where('application_status', '4')->get();
                            ->where('application_status', 6)->get();
                        $AllVerifiedCount = $AllVerified->count();
                        $AllPending = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            // ->where('application_status', '4')->get();
                            ->where('application_status', 4)->get();
                        $AllPendingCount = $AllPending->count();
                        $Meeting = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', '5')->get();
                        $MeetingCount = $Meeting->count();
                        $Certified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', '6')
                            ->limit($BlockCount - 1)
                            ->orderBy('created_at', 'DESC')->get();
                        $CertifiedCount = $Certified->count();
                        $Reject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            // ->where('application_status', '7')->get();
                            ->where('application_status', 7)->get();
                        $RejectCount = $Reject->count();
                        $All_Meeting_Data = DB::table('application_data')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                            ->select(
                                'application_data.school_name',
                                'application_data.created_at',
                                'application_data.block',
                                'application_data.district',
                                'application_data.application_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                'status.status_name AS status_name',
                                'application_meetings.meeting_date'
                            )
                            ->where('application_data.application_status', '5')
                            ->where('application_data.district', $User_District)
                            ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                            ->where('application_data.status', '1')->limit($BlockCount - 1)
                            ->get();
                        for ($i = 0; $i < $BlockCount; $i++) {
                            $AllApplication = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', '>', '1')->get();
                            $CountAllApplication = $AllApplication->count();
                            array_push($Total_Application, $CountAllApplication);
                            $AllReject = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 7)->get();
                            // ->where('application_status', '7')->get();
                            $CountAllReject = $AllReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                            $AllVerified = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 6)->get();
                            // ->where('application_status', '6')->get();
                            $CountAllVerified = $AllVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllPending = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', '4')->get();
                            // ->whereIn('application_status', [2, 3, 4, 9, 12])->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                        }
                        $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                        // return($Arr_Block_Filter);
                        $wordCount = [
                            'All'           => $wordCountAll,
                            'AllDistrict' => $Alldata,
                            'AllVerifiedCount' => $AllVerifiedCount,
                            'AllPendingCount' => $AllPendingCount,
                            'MeetingCount' => $MeetingCount,
                            'CertifiedCount' => $CertifiedCount,
                            'RejectCount' => $RejectCount,
                            'AllCertified' => $Certified,
                            'Arr_Block_Filter' => $Arr_Block_Filter,
                            'All_Meeting_Data' => $All_Meeting_Data
                        ];
                    }
                    if (Auth::user()->user_role == 'state') {
                        $Total_Pending  = $Total_Approved = $Total_Application = $Total_Verification = $Total_Rejection = array();
                        $UserDivision = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.division')
                            ->get();
                        $User_Division = $UserDivision[0]->division;
                        $Division = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.value_set_id', '1')
                            ->get();
                        $DivisionCount = $Division->count();
                        $Alldata = DB::table('application_data')->where('status', '1')->where('application_data.application_status', '>', 1)->get();
                        $wordCountAll = $Alldata->count();
                        $AcceptedByAllUsers = DB::table('application_data')->where('status', '1')->whereIn('application_status', [4, 6, 10, 13])->get();
                        $wordCountAcceptedByAllUsers = $AcceptedByAllUsers->count();
                        $AlldataAccept = DB::table('application_data')->where('status', '1')->where('application_status', '6')->get();
                        $wordCountAcpt = $AlldataAccept->count();
                        $AlldataReject = DB::table('application_data')->where('status', '1')->whereIn('application_status', [7, 8, 11, 14])->get();
                        $wordCountR = $AlldataReject->count();
                        $AllDetails = DB::table('application_data')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->select(
                                'application_data.school_name',
                                'application_data.created_at',
                                'application_data.block',
                                'application_data.district',
                                'application_data.application_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                'status.status_name AS status_name'
                            )
                            ->where('application_data.status', '1')
                            ->where('application_data.application_status', '>', 1)
                            ->limit($DivisionCount)
                            ->orderBy('created_at', 'desc')->get();
                        $AllCertified = DB::table('application_data')->where('status', '1')->where('division', $User_Division)->where('application_status', '6')->get();
                        $CountAllCertified = $AllCertified->count();
                        for ($i = 0; $i < $DivisionCount; $i++) {
                            $AllApplication = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_data.application_status', '>', 1)->get();
                            $CountAllApplication = $AllApplication->count();
                            array_push($Total_Application, $CountAllApplication);
                            $AllReject = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->whereIn('application_status', [7, 8, 11, 14])->get();
                            $CountAllReject = $AllReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                            $AllPending = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->wherein('application_status', [2, 3, 4, 9, 12])->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                            $AllApproved = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_status', 6)->get();
                            $CountAllApproved = $AllApproved->count();
                            array_push($Total_Approved, $CountAllApproved);
                        }
                        $Arr_Block_Filter = ['BlockCount' => $DivisionCount, 'Block' => $Division, 'Total_Application' => $Total_Application, 'Total_Pending' => $Total_Pending, 'Total_Approved' => $Total_Approved, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection];
                        $wordCount = [
                            'All' => $wordCountAll,
                            'Active' => $wordCountA,
                            'VerifiedByAllUser' => $wordCountAcceptedByAllUsers,
                            'Reject' => $wordCountR,
                            'Pending' => $wordCountP,
                            'Accept' => $wordCountAcpt,
                            'AllCertified' => $AllCertified,
                            'AllDetails' => $AllDetails,
                            'Arr_Block_Filter' => $Arr_Block_Filter
                        ];
                    }
                    if (Auth::user()->user_role == 'school') {
                        // return 'true';
                        $Accepted_Fields = 0;
                        $Rejected_Fields = 0;
                        $pending_Fields = 0;
                        $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
                        foreach ($alltable_col as $key => $value) {
                            if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at" && $value != "created_by_vrfy" && $value != "updated_by_vrfy") {
                                if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                    $Accepted_Fields++;
                                }
                                if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                    $Rejected_Fields++;
                                }
                                if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                    $pending_Fields++;
                                }
                            }
                        }
                        $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
                        // echo $alltable_col;
                        foreach ($alltable_ext_col as $key => $value) {
                            if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {
                                if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                    $Accepted_Fields++;
                                }
                                if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                    $Rejected_Fields++;
                                }
                                if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                    $pending_Fields++;
                                }
                            }
                        }
                        $UserDistrict = DB::table('private_schools')
                            ->where('id', Auth::user()->school_id)
                            ->value('district');
                        $User_District = $UserDistrict;
                        $Block = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_District)
                            ->get();
                        $Application_Status = application_data::where('application_data.school_id', Auth::user()->school_id)
                            ->value('application_data.application_status');
                        // $acceptedted_feild = DB::table('application_data_verifications')
                        // ->select( DB::raw('count(school_name_vrfy,udise_code_vrfy) as accepted_count, school_name_vrfy') )
                        // ->where('application_data_verifications.school_id', Auth::user()->school_id)
                        // ->groupBy('school_name_vrfy')
                        // ->get();
                        // return $acceptedted_feild;
                        $Application_Tracking = DB::table('application_trackings')
                            ->Join('application_statuses', 'application_trackings.application_status_id', '=', 'application_statuses.id')
                            ->select(
                                'application_trackings.application_id',
                                'application_trackings.school_id',
                                'application_trackings.application_status_id',
                                'application_trackings.created_at',
                                'application_trackings.created_by',
                                'application_statuses.status_name AS status_name'
                            )
                            ->where('application_trackings.school_id', Auth::user()->school_id)
                            ->get();
                        $CountApplication_Tracking = $Application_Tracking->count();
                        $feedback = DB::table('application_data')
                            ->select(
                                'application_data.id',
                                'application_data.created_by',
                                'application_data.school_name',
                                'application_data.status',
                                'application_data.feedback_file_upload',
                                'application_data.feedback_from_dsc_to_school',
                                'application_data.verification_date'
                            )
                            ->where('application_data.school_id', Auth::user()->school_id)->where('application_data.status', 1)->get();
                        $All_Meeting_Data_school = DB::table('application_meetings')
                            ->Join('application_data', 'application_data.id', '=', 'application_meetings.application_id')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('users', 'users.id', '=', 'application_meetings.created_by')
                            // ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->select(
                                'application_meetings.meeting_date',
                                'application_meetings.meeting_remarks',
                                'application_data.school_name',
                                'users.name',
                                'application_meetings.created_at',
                                'application_meetings.meeting_text',
                                'application_meetings.meeting_remarks',
                                'application_meetings.meeting_documents',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                // 'status.status_name AS status_name'
                            )
                            // ->where('application_data.application_status', '5')
                            ->where('application_meetings.school_id', Auth::user()->school_id)
                            ->get();
                        foreach ($All_Meeting_Data_school as  $Meeting_Data_school) {
                            $Meeting_Data_school->meeting_documents = json_decode($Meeting_Data_school->meeting_documents);
                        }
                        // if ($All_Meeting_Data_school->meeting_documents != null)
                        // $All_Meeting_Data_school->meeting_documents = json_decode($All_Meeting_Data_school->meeting_documents, true);
                        if ($Application_Status == 1) {
                            $Accepted_Fields = 0;
                            $Rejected_Fields = 0;
                            $pending_Fields = 0;
                        }
                        $wordCount = [
                            'feedback' => $feedback,
                            'Application_Tracking' => $Application_Tracking,
                            'Accepted_Fields' => $Accepted_Fields,
                            'Rejected_Fields' => $Rejected_Fields,
                            'pending_Fields' => $pending_Fields,
                            'All_Meeting_Data_school' => $All_Meeting_Data_school,
                            'Application_Status' => $Application_Status
                        ];
                    }
                    // return [Auth::user()];
                    return view('single_window_school_register')->with('data', $user);
                }
            }
        }
    }
   

    public function storePaymentResponse(Request $request)
    {
        $error = "Payment Failed";
        $payments = new payments;
        $payments->school_id = Auth::user()->school_id;
        $payments->payment_title = $request->payment_title;
        $payments->payment_description = $request->payment_description;
        $payments->payment_amount = $request->amount;
        $payments->amount = $request->school_id;
        $payments->payment_date = date('Y-m-d');

        if ($request->payment_status == 'successful') {
            $success = true;
        }

        if ($success === true) {

            // return $subscription_invoices;   
            $payments->payment_status = 'SUCCESS';
            $payments->save();
        } else {

            $payments->payment_status = 'FAILED';

            $payments->payment_success_details = $request->error_code  ?? $error;
        }
        $payments->save();

        return response()->json(['message' => 'Response stored successfully']);
    }

    public function forPayment(Request $request)
    {
        $datas_to_send = [];

        // $fields = array(
        //     'data'          => array(
        //         'title' => "From API",
        //         'body' => "This is a notification from API",
        //         'active_notifications' => 99,
        //         'sound' => 'default',
        //         'icon' => 'icon'
        //     ),
        //     'priority' => 'high'
        // );
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://advantage.jharkhand.gov.in/SingleWindow/DepartmentForms/status',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"application_stage":10,"status":"Application submitted successfully. Payment
            Awaited","acknowledgment_no":"LBRXX11773","service_type_id":1,"caf_unique_
            no":"9802dfb08","department_id":"JEPC","swsregid":9858,"payable_amount":400,"
            payment_validity":"06-07-2017
            00:00:00","payment_other_details":null,"certificate_url":null,"approval_date":"06
            -07-2017 00:00:00" }',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiM2YxYTkzNjIwMTM0ZDhiYzg3NDgzMWU5NDMxZjdlMzc1OGU5OWUzMjI1ZWQwNDRhMWRjNGU4NDk1ZTViOWI5YTE1NTQxY2UzOWI2MzQ3MjgiLCJpYXQiOiIxNjI3MzY4NDg1LjkzMjE1OCIsIm5iZiI6IjE2MjczNjg0ODUuOTMyMTYzIiwiZXhwIjoiMTY1ODkwNDQ4NS45Mjg2MDEiLCJzdWIiOiIzNjgiLCJzY29wZXMiOltdfQ.MJLtaRqPf1efktwfeQbghMP6Ywx84mKE_x5VJOi4qpaDEaYFFX2MIVkTecXHTDjGmTTZKAAiTxrdaHEfdSKeOAuhe2BfaE3nelZSwauQ-5qewnCWTDa5fyHmPAavfJpX3FlJjhQzu3Aq85yQkNTq4jgm2e7C7l2GU2eCDdaVXlqlXlOQ28JwK1D1EMaLe9lHU0-UFluqcysJfLqv69hUa6cKXYoyr2CA1Uyo_xQkoZktpNPs2CWTeHHEvHzWFWcB7myIBFRnhE8c8BnSz6AOEA3r85eYrMNVij7VJDVQjo1WWH62SVdPB1DKBBh-dursBjoQM535OolxtFdXo2lzjL8PGOvJT4K8KlE6MJ9_nMgobPxsKArQiXFR2S-FSmlpYWr19xZUFqCyZe3M6F8EjiTrfj7vOKHw7Jw5ABoMvK5S-MRRZZP40fPBDwmRrnkNiVwPVeJigz081KnQrWraFGkGxe0mTS0LdIawmC5asWNK3FRq5ECugjADYUhfA3INxu7vC_XGuVvK5BMoX7UKwMLkw5gxSR1wgo_5Q_jeDVkwX19S92UeLhTrU-EgBCVrT3lPI3yyPrR2XU_dBa1OgZC7NBWRwEht2bMBXyKbc7CXZmVQ5VKosWIErgjpcC23ARFNNBJXySXefa8l3HlBVh-qQGpoGtFuVfQwvFBE_xg'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
    }

}
