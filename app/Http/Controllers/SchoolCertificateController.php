<?php
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Private_school;
use App\Models\Person_detail;
use App\Models\certificate;
use App\Models\application_data;
use App\Models\Contact;
use App\Models\Address;
use Illuminate\Http\Request;
use Mail;
use Auth;
use PDF;
class SchoolCertificateController extends Controller
{
  /* School Certificate Data 
      Using  $this->data = [] for all function*/
  public $data = [];
  public function schoolCertificate(Request $request)
  {
    if (Auth::check()) {
      if (Auth::user()->user_role == 'school') {
        $this->data['schoolDetail'] = application_data::leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
          ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
          ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
          ->leftjoin('certificates', 'certificates.school_id', 'application_data.school_id')
          ->select(
            'application_data.id as application_id',
            'application_data.school_name',
            'application_data.from_class',
            'application_data.upto_class',
            'fnd_division.display_value as division',
            'fnd_district.display_value as district',
            'fnd_block.display_value as block',
            'application_data.police_station',
            'application_data.panchayat',
            'application_data.post_office',
            'certificates.certified_date',
            'certificates.certificate_number',
          )
          ->where('application_data.school_id', Auth::user()->school_id)
          ->where('application_data.status', '1')
          ->first();
        if ($request->submit == 'pdf') {
          $data = ['schoolDetail' => $this->data['schoolDetail']];
          return view('school-certificate', $this->data);
          //$pdf = PDF::loadView('pdf_view.rejected_application_PDF', $data);
          $pdf = PDF::loadView('pdf_view.certificate_PDF', $data)->setPaper('a4', 'portrait');
          return $pdf->download('certificate.pdf');
        }
        return view('certificate', $this->data);
      } else {
        return redirect('dashboard')->with('message', 'You are not Authorised!!');
      }
    }
  }
}
