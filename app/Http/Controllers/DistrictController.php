<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
// use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Contact;
use App\Models\Address;
use App\Models\Person_detail;
use App\Models\Fnd_value;
use App\Models\Fnd_value_set;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
class DistrictController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $Districts = Fnd_value::select(
                    'fnd_values.id',
                    'fnd_values.display_value'
                )->where('fnd_values.value_set_id', 1)
                    ->get();
                // $Users = User::all();
                return view('addDSC', compact('Districts'));
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function show()
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $division = Fnd_value::select(
                    'fnd_values.id',
                    'fnd_values.display_value as value_set_name'
                )->where('fnd_values.value_set_id', 1)
                    ->get();
                $Users = User::join('addresses', 'users.id', '=', 'addresses.user_id')
                    ->join('contacts', 'contacts.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.name',
                        'users.email',
                        'users.user_role',
                        'users.created_at',
                        'addresses.school_address as user_address',
                        'addresses.created_by',
                        'contacts.mobile as contact'
                    )
                    ->where('users.status', 1)
                    ->paginate(25);
                // $Users = User::all();
                // $Users = User::all();
                $UserData = ['User' => $Users, 'District' => $division];
                return view('district', compact('UserData'));
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function insert(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                if ($request->usertype == 'dse' && User::leftJoin('addresses', 'addresses.user_id', '=', 'users.id')->where(['users.user_role' => 'dse', 'addresses.district' => $request->district])->exists()) {
                    return redirect()->back()->with('warning', 'District Superintendent of Education User for this District, already exists.');
                } elseif ($request->usertype == 'dc' && User::leftJoin('addresses', 'addresses.user_id', '=', 'users.id')->where(['users.user_role' => 'dc', 'addresses.district' => $request->district])->exists()) {
                    return redirect()->back()->with('warning', 'District Committee user for this District, already exists.');
                } elseif ($request->usertype == 'faa' && User::leftJoin('addresses', 'addresses.user_id', '=', 'users.id')->where(['users.user_role' => 'faa', 'addresses.division' => $request->division])->exists()) {
                    return redirect()->back()->with('warning', 'First Appellate Authority user for this Division, already exists.');
                } elseif ($request->usertype == 'saa' && User::leftJoin('addresses', 'addresses.user_id', '=', 'users.id')->where(['users.user_role' => 'saa'])->exists()) {
                    return redirect()->back()->with('warning', 'Second Appellate Authority user, already exists.');
                }
                if (User::where('username', $request->user_name)->exists()) {
                    return redirect()->back()->with('warning', 'This username already exists.');
                } elseif (User::where('email', $request->email)->exists()) {
                    return redirect()->back()->with('warning', 'This Email ID already exists.');
                }
                $users = new User;
                $pass =  \Hash::make($request->password);
                $users->email = $request->email;
                $users->school_id = 1;
                $users->name = $request->full_name;
                $users->username = $request->user_name;
                $users->password =  $pass;
                $users->user_role = $request->usertype;
                if ($request->has('photo')) {
                    $image = $request->photo;
                    $fileName = time() . '.' . $request->photo->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $request->photo->move(public_path('assets/images'), $fileName);
                    $path = 'public/images/' . $fileName;
                    $users->profile_photo_path = $path;
                }
                $users->save();
                $contact = new Contact;
                $contact->created_by =  Auth::user()->id;
                $contact->created_ip =  $_SERVER['REMOTE_ADDR'];
                $contact->mobile = $request->phonenumber1;
                $contact->school_email = $request->email;
                $contact->user_id = $users->id;
                $contact->save();
                $address = new Address;
                $address->created_by = Auth::user()->id;
                $address->created_ip =  $_SERVER['REMOTE_ADDR'];
                $address->district = $request->district;
                $address->division = $request->division;
                $address->school_address = $request->address1;
                $address->school_id  = $users->school_id = $contact->school_id =  '1';
                $address->school_state = 'Jharkhand';
                $address->user_id = $users->id;
                $address->save();
                $person_detail = new Person_detail;
                $person_detail->created_by =  Auth::user()->id;
                $person_detail->created_ip = $_SERVER['REMOTE_ADDR'];
                $person_detail->full_name = $request->full_name;
                $person_detail->user_address = $request->address1;
                $person_detail->user_id = $users->id;
                $person_detail->user_name = $request->user_name;
                $person_detail->user_type = $request->usertype;
                $person_detail->save();
                return redirect('district')->with('success', 'User Inserted Successfully !!!');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function edit($req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $id = Crypt::decrypt($req);
                $DistrictName = ['0' => ''];
                $userData = User::leftjoin('addresses', 'users.id', 'addresses.user_id')
                    ->leftjoin('contacts', 'contacts.user_id', 'users.id')
                    ->leftjoin('person_details', 'person_details.user_id', 'users.id')
                    ->where('users.id', $id)
                    ->first();
                // return $userData;
                // $Block =  $userData['block'];
                $District =  $userData['district'];
                $DistrictName = Fnd_value::select(
                    'fnd_values.id',
                    'fnd_values.display_value'
                )->where('fnd_values.id', '=', $District)
                    ->get();
                $Districts = Fnd_value::select(
                    'fnd_values.id',
                    'fnd_values.display_value as value_set_name'
                )->where('fnd_values.value_set_id', 1)
                    ->get();
                return view('editDSC', ['user' => $userData, 'DistrictName' => $DistrictName, 'Districts' => $Districts]);
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    // below function is user to delete user
    public function deleteData(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                $id = $req['user_id'];
                $users = User::where('users.id', $id)->update([
                    'status' => '0'
                ]);
                $users = Contact::where('contacts.user_id', $id)->update([
                    'status' => '0'
                ]);
                $users = Address::where('addresses.user_id', $id)->update([
                    'status' => '0'
                ]);
                $users = Person_detail::where('person_details.user_id', $id)->update([
                    'status' => '0'
                ]);
                return redirect('district')->with('warning', 'User Deleted Successfully !!!');
            } else {
                return redirect('dashboard')->with('warning', 'You are not Authorised!!');
            }
        }
    }
    // below function is user to Update user Profile
    public function updateData(Request $req)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'state') {
                // return $req;
                // Get the currently authenticated user...
                $user = Auth::user();
                if (User::where('username', $req->user_name)->where('id', '!=', $req->id)->exists()) {
                    return redirect()->back()->with('warning', 'This username already exists.');
                } elseif (User::where('email', $req->email)->where('id', '!=', $req->id)->exists()) {
                    return redirect()->back()->with('warning', 'This Email ID already exists.');
                }
                if ($req->has('photo')) {
                    $image = $req->photo;
                    $fileName = time() . '.' . $req->photo->getClientOriginalName();
                    $file_extension = \File::extension($fileName);
                    if ($file_extension != 'jpg' && $file_extension != 'JPG' && $file_extension != 'png' && $file_extension != 'PNG' && $file_extension != 'jepg' && $file_extension != 'JEPG') {
                        return redirect()->back()->with('warning', 'This file type is not supported for profile picture, you can only use jpg, jepg and png files.');
                    }
                    $req->photo->move(public_path('assets/images'), $fileName);
                    $path = 'public/images/' . $fileName;
                } else {
                    $path = $req->photo_path;
                }
                $users = User::where('users.id', $req->id)->update([
                    'name' => $req->user_name,
                    'email' => $req->email,
                    'profile_photo_path' => $path
                    // 'user_role' => $req->usertype
                ]);
                $address = Address::where('addresses.user_id', $req->id)->update([
                    'school_address' => $req->address1,
                    // 'address2' => $req->address2,
                    // 'village' => $req->village,
                    // 'block' => $req->block,
                    // 'po' => $req->postoffice,
                    // 'ps' => $req->policestation,
                    // 'cluster' => $req->cluster,
                    // 'school_city' => $req->city,
                    'division' => $req->division,
                    'district' => $req->district,
                    'school_state' => 'Jharkhand',
                    'updated_ip' => $_SERVER['REMOTE_ADDR'],
                    'updated_by' => $user->id
                ]);
                $contact = Contact::where('contacts.user_id', $req->id)->update([
                    'mobile' => $req->phonenumber1,
                    'school_email' => $req->email,
                    // 'phone_2' => $req->phonenumber2,
                    // 'whatsapp' => $req->whatsupnumber,
                    'updated_ip' => $_SERVER['REMOTE_ADDR'],
                    'updated_by' => $user->id
                ]);
                $person_detail = Person_detail::where('person_details.user_id', $req->id)->update([
                    'full_name' => $req->full_name,
                    'user_name' => $req->user_name,
                    'user_address' => $req->address1,
                    // 'gender' => $req->gender,
                    // 'DOB' =>  $req->dob,
                    'updated_ip' => $_SERVER['REMOTE_ADDR'],
                    'updated_by' => $user->id
                ]);
                return redirect('district')->with('success', 'User Update Successfully !!!');
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
}
