<?php

namespace App\Http\Controllers;

use App\Exports\PaymentExport;
use App\Models\Conversations;
use App\Models\Fnd_value;
use App\Models\User;
use App\Models\Contact;

use App\Models\application_data;
use App\Models\application_data_ext_1;
use App\Models\application_data_ext_file;
use App\Models\application_data_file;
use App\Models\application_data_remarks;
use App\Models\application_data_remarks_ext_1;
use App\Models\application_data_verification;
use App\Models\application_data_verification_ext_1;
use App\Models\application_meetings;
use App\Models\application_statuses;
use App\Models\application_teacher_data;
use App\Models\application_tracking;
use App\Models\certificate;
use App\Models\Fnd_treasury_value;
use App\Models\Fnd_ddo_value;
use App\Models\rte_applicant_students;
use App\Models\fnd_settings;
use App\Models\Address;
use App\Models\Re_apply_application;
use App\Models\Payment;
use App\Models\Transaction;
use App\Exports\ExcelExport;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Crypt;
use PDF;
use PHPUnit\Framework\Constraint\Count;

use Mail;
use Session;
use Hash;
use PhpParser\Node\Expr\AssignOp\Concat;
use App\Models\Private_school;
use App\Models\single_window_user;

class application_formController extends Controller
{



    function editData(Request $req)
    {
        $user = Auth::user();
        $createdby = $req->id;
        $application_data = application_data::where('application_data.created_by', $createdby)->where('application_data.status', 1)->update([
            // $application_form = new application_data;
            'school_name' => $req->school_name,
            'recognised_by' => $req->recognised_by,
            'district' => $req->district,
            'post_office' => $req->post_office,
            'village_city' => $req->village_town,
            'pincode' => $req->pin_code,
            'phone_with_std_code' => $req->phone_with_std_code,
            'fax_with_std_code' => $req->fax_with_std_code,
            'email' => $req->email,
            'police_station' => $req->police_station,
            'estd_year' => $req->estd_year,
            'opening_date' => $req->opening_date,
            'society_name' => $req->society_name,
            'is_society_registered' => $req->is_society_registered,
            'society_registration_valid_upto' => $req->society_registration_valid_upto,
            'evidence_of_non_proprietary_nature' => $req->evidence_of_non_proprietary_nature,
            'school_chairman_name' => $req->school_chairman_name,
            'school_chairman_post' => $req->school_chairman_post,
            'school_chairman_address' => $req->school_chairman_address,
            'school_chairman_phone_number' => $req->school_chairman_phone_number,
            'school_chairman_office' => $req->school_chairman_office,
            'school_chairman_email' => $req->school_chairman_email,
            'session_year' => $req->session_year,
            'income' => $req->income,
            'expenses' => $req->expenses,
            'surplus_money' => $req->surplus_money,
            'reduced_money' => $req->reduced_money,
            'medium' => $req->medium,
            'type_of_school' => $req->type_of_school,
            'supported_agency_name' => $req->supported_agency_name,
            'agency_supported_percent' => $req->agency_supported_percent,
            'authority_name' => $req->authority_name,
            'recognised_number' => $req->recognised_number,
            'is_school_on_rented' => $req->is_school_on_rented,
            'are_school_building_used' => $req->are_school_building_used,
            'school_total_area' => $req->school_total_area,
            'school_building_area' => $req->school_building_area,
            'pre_elementary_class' => $req->pre_elementary_class,
            'pre_no_of_section' => $req->pre_no_of_section,
            'pre_no_of_student' => $req->pre_no_of_student,
            'class_one_to_five' => $req->class_one_to_five,
            'onefive_no_of_section' => $req->onefive_no_of_section,
            'onefive_no_of_student' => $req->onefive_no_of_student,
            'class_six_to_eight' => $req->class_six_to_eight,
            'sixeight_no_of_section' => $req->sixeight_no_of_section,
            'sixeight_no_of_student' => $req->sixeight_no_of_student,
            'no_of_class' => $req->no_of_class,
            'avg_size_cls_room' => $req->avg_size_cls_room,
            'no_of_office_room' => $req->no_of_office_room,
            'avg_size_of_office_room' => $req->avg_size_of_office_room,
            'no_of_store_room' => $req->no_of_store_room,
            'avg_size_of_store_room' => $req->avg_size_of_store_room,
            'no_of_princpal_room' => $req->no_of_princpal_room,
            'avg_size_of_principal_room' => $req->avg_size_of_principal_room,
            'no_of_kitchen_room' => $req->no_of_kitchen_room,
            'avg_size_of_kitchen_room' => $req->avg_size_of_kitchen_room,
            'created_by' => 1
        ]);
        $application_data_ext_1 = application_data_ext_1::where('application_data_ext_1.created_by', $createdby)->where('application_data_ext_1.status', 1)->update([
            'facilities_access_without_interrupted' => $req->facilities_access_without_interrupted,
            'all_teaching_material_list' => $req->all_teaching_material_list,
            'all_sports_equipment_list' => $req->all_sports_equipment_list,
            'books' => $req->books,
            'magazines' => $req->magazines,
            'type_of_water_facilities' => $req->type_of_water_facilities,
            'no_of_water_supply' => $req->no_of_water_supply,
            'type_of_toilet' => $req->type_of_toilet,
            'gents_toilet' => $req->gents_toilet,
            'ladies_toilet' => $req->ladies_toilet,
            'principle_name' => $req->principle_name,
            'p_f_h_w_name' => $req->p_f_h_w_name,
            'p_date_of_birth' => $req->p_date_of_birth,
            'p_education_qualification' => $req->p_education_qualification,
            'trainee_qualification' => $req->trainee_qualification,
            'teaching_experience' => $req->teaching_experience,
            'class_handed_over' => $req->class_handed_over,
            'date_of_appointment' => $req->date_of_appointment,
            'trained_untrained' => $req->trained_untrained,
            'details_of_curriculum' => $req->details_of_curriculum,
            'method_of_inspection' => $req->method_of_inspection,
            'school_board_exam_till_cls_eight' => $req->school_board_exam_till_cls_eight
            // $application_form_ext->save();
        ]);
        return redirect('district')->with('success', 'Data Has Been Updated Successfully');
    }
    function editDscFeedback(Request $req)
    {
        $id = $req->id;
        $path = '';
        if ($req->has('feedback_file_upload')) {
            $image = $req->feedback_file_upload;
            $fileName = time() . '.' . $req->feedback_file_upload->getClientOriginalName();
            $req->feedback_file_upload->move(public_path('assets/images/dsc_to_school'), $fileName);
            $path = 'public/images/dsc_to_school/' . $fileName;
        }
        application_data::where('application_data.id', $id)->update([
            'feedback_from_dsc_to_school' => $req->feedback_from_dsc_to_school,
            'verification_date' => $req->feedback_date,
            'feedback_file_upload' => $path
        ]);
        return redirect('application-verify')->with('feedback', 'Your Feedback send to School Successfully');
    }
    function SendMessageToSchool(Request $req)
    {
        $decode_image = $image_name = [];
        if ($req->send_file_upload != null) {
            foreach ($req->send_file_upload as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/send_dse_to_school__correction'), $fileName);
                    $image_name[] = $fileName;
                }
            }
        }
        $decode_image = json_encode($image_name, true);
        $users_id = User::where('school_id', $req->send_school_id)->value('id');
        $Conversations = new Conversations;
        $Conversations->conversation_type = 4;
        $Conversations->parent_id = $req->send_application_id;
        $Conversations->sender_id = Auth::user()->id;
        $Conversations->reciever_id = $users_id;
        $Conversations->conversation_date = $req->send_date;
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->send_from_dsc_to_school;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        return redirect('application-verify')->with('feedback', 'Your Message send to School Successfully');
    }
    function SendMessageToDCDSE(Request $req)
    {
        $Send_user_district = Address::where('addresses.school_id', Auth::user()->school_id)
            ->value('district');
        // return $Send_user_district;
        $users_id = User::join('addresses', 'addresses.user_id', '=', 'users.id');
        if ($req->send_to == 'dc' || $req->send_to == 'dse') {
            $users_id = $users_id->where('addresses.district', $Send_user_district);
        }
        // return $users_id;
        $users_id = $users_id->where('users.user_role', $req->send_to)
            ->value('users.id');
        $decode_image = $image_name = '';
        if ($req->send_file_upload != null) {
            foreach ($req->send_file_upload as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/send_school_to_dc_dse_correction'), $fileName);
                    $image_name[] = $fileName;
                }
            }
        }
        $decode_image = json_encode($image_name, true);
        $Conversations = new Conversations;
        $Conversations->conversation_type = 4;
        $Conversations->parent_id = $req->send_application_id;
        $Conversations->sender_id = Auth::user()->id;
        $Conversations->reciever_id = $users_id;
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->send_from_dsc_to_school;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        return redirect('application')->with('feedback', 'Your Message send Successfully');
    }
    function addData(Request $req)
    {
        // if (Auth::check()) {
        //     if (Auth::user()->user_role == 'school') {
        //         $application_form = application_data::where('school_id', Auth::user()->school_id);
        //         $division = Address::where('user_id', Auth::user()->id)->value('division');
        //         $district = Address::where('user_id', Auth::user()->id)->value('district');
        //         $block = Address::where('user_id', Auth::user()->id)->value('block');
        //     }
        // }
        $application_form = new application_data;
        $application_form->school_name = $req->school_name;
        $application_form->recognised_by = $req->recognised_by;
        $application_form->district = $req->district;
        $application_form->post_office = $req->post_office;
        $application_form->village_city = $req->village_town;
        $application_form->pincode = $req->pin_code;
        $application_form->phone_with_std_code = $req->phone_with_std_code;
        $application_form->fax_with_std_code = $req->fax_with_std_code;
        $application_form->email = $req->email;
        $application_form->police_station = $req->police_station;
        $application_form->estd_year = $req->estd_year;
        $application_form->opening_date = $req->opening_date;
        $application_form->society_name = $req->society_name;
        $application_form->is_society_registered = $req->is_society_registered;
        $application_form->society_registration_valid_upto = $req->society_registration_valid_upto;
        $application_form->evidence_of_non_proprietary_nature = $req->evidence_of_non_proprietary_nature;
        $application_form->school_chairman_name = $req->school_chairman_name;
        $application_form->school_chairman_post = $req->school_chairman_post;
        $application_form->school_chairman_address = $req->school_chairman_address;
        $application_form->school_chairman_phone_number = $req->school_chairman_phone_number;
        $application_form->school_chairman_office = $req->school_chairman_office;
        $application_form->school_chairman_email = $req->school_chairman_email;
        $application_form->session_year = $req->session_year;
        $application_form->income = $req->income;
        $application_form->expenses = $req->expenses;
        $application_form->surplus_money = $req->surplus_money;
        $application_form->reduced_money = $req->reduced_money;
        $application_form->medium = $req->medium;
        $application_form->type_of_school = $req->type_of_school;
        $application_form->supported_agency_name = $req->supported_agency_name;
        $application_form->agency_supported_percent = $req->agency_supported_percent;
        $application_form->authority_name = $req->authority_name;
        $application_form->recognised_number = $req->recognised_number;
        $application_form->is_school_on_rented = $req->is_school_on_rented;
        $application_form->are_school_building_used = $req->are_school_building_used;
        $application_form->school_total_area = $req->school_total_area;
        $application_form->school_building_area = $req->school_building_area;
        $application_form->pre_elementary_class = $req->pre_elementary_class;
        $application_form->pre_no_of_section = $req->pre_no_of_section;
        $application_form->pre_no_of_student = $req->pre_no_of_student;
        $application_form->class_one_to_five = $req->class_one_to_five;
        $application_form->onefive_no_of_section = $req->onefive_no_of_section;
        $application_form->onefive_no_of_student = $req->onefive_no_of_student;
        $application_form->class_six_to_eight = $req->class_six_to_eight;
        $application_form->sixeight_no_of_section = $req->sixeight_no_of_section;
        $application_form->sixeight_no_of_student = $req->sixeight_no_of_student;
        $application_form->no_of_class = $req->no_of_class;
        $application_form->no_of_office_room = $req->no_of_office_room;
        $application_form->avg_size_of_office_room = $req->avg_size_of_office_room;
        $application_form->no_of_store_room = $req->no_of_store_room;
        $application_form->avg_size_of_store_room = $req->avg_size_of_store_room;
        $application_form->no_of_princpal_room = $req->no_of_princpal_room;
        $application_form->avg_size_of_principal_room = $req->avg_size_of_principal_room;
        $application_form->no_of_kitchen_room = $req->no_of_kitchen_room;
        $application_form->avg_size_of_kitchen_room = $req->avg_size_of_kitchen_room;
        // $application_form->medium = $req->medium;
        $application_form->save();
        $applicationForm_Id = application_data::max('id');
        $hiddenTeacherId = $req->hiddenTeacherId;
        for ($row = 1; $row <= $hiddenTeacherId; $row++) {
            return  $req->teacher_name . ($row);
            $application_teacher_data = new application_teacher_data;
            $application_teacher_data->application_data_id = $applicationForm_Id;
            $application_teacher_data->teacher_name = $req->teacher_name . $row;
            $application_teacher_data->teacher_f_h_w_name = $req->teacher_f_h_w_name . $row;
            $application_teacher_data->date_of_birth = $req->teacher_date_of_birth . $row;
            $application_teacher_data->education_qualification = $req->teacher_education_qualification . $row;
            $application_teacher_data->trainee_qualification = $req->teacher_trainee_qualification . $row;
            $application_teacher_data->teaching_experience = $req->teacher_teaching_experience . $row;
            $application_teacher_data->cls_handed_over = $req->teacher_class_handed_over . $row;
            $application_teacher_data->date_of_appointment = $req->teacher_appointment_date . $row;
            $application_teacher_data->trained_or_untrained = $req->teacher_trained_or_untrained . $row;
            $application_teacher_data->save();
        }
        $applicationFormId = application_data::max('id');
        $application_form_ext = new application_data_ext_1;
        $application_form_ext->application_form_id = $applicationFormId;
        $application_form_ext->facilities_access_without_interrupted = $req->facilities_access_without_interrupted;
        $application_form_ext->all_teaching_material_list = $req->all_teaching_material_list;
        $application_form_ext->all_sports_equipment_list = $req->all_sports_equipment_list;
        $application_form_ext->books = $req->books;
        $application_form_ext->magazines = $req->magazines;
        $application_form_ext->type_of_water_facilities = $req->type_of_water_facilities;
        $application_form_ext->no_of_water_supply = $req->no_of_water_supply;
        $application_form_ext->type_of_toilet = $req->type_of_toilet;
        $application_form_ext->gents_toilet = $req->gents_toilet;
        $application_form_ext->ladies_toilet = $req->ladies_toilet;
        $application_form_ext->principle_name = $req->principle_name;
        $application_form_ext->p_f_h_w_name = $req->p_f_h_w_name;
        $application_form_ext->p_date_of_birth = $req->p_date_of_birth;
        $application_form_ext->p_education_qualification = $req->p_education_qualification;
        $application_form_ext->trainee_qualification = $req->trainee_qualification;
        $application_form_ext->teaching_experience = $req->teaching_experience;
        $application_form_ext->class_handed_over = $req->class_handed_over;
        $application_form_ext->date_of_appointment = $req->date_of_appointment;
        $application_form_ext->trained_untrained = $req->trained_untrained;
        $application_form_ext->details_of_curriculum = $req->details_of_curriculum;
        $application_form_ext->method_of_inspection = $req->method_of_inspection;
        $application_form_ext->school_board_exam_till_cls_eight = $req->school_board_exam_till_cls_eight;
        $application_form_ext->save();
        return redirect('application')->with('message', 'Data Has Been Inserted Successfully');
    }
    public function ApplicationList(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'dc' || Auth::user()->user_role == 'dse') {
                $UserDivision = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::user()->id)
                    ->value('addresses.division');
                // return($UserDivision);
                $UserDistrict = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::user()->id)
                    ->value('addresses.district');
                // return($UserDistrict);
                $this->data['block_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
                    ->pluck('fnd_values.display_value as block_name', 'fnd_values.id as block_id');
                // return( $this->data['block_list']);
                $search_text = $request->search_text;
                $application_status = $request->application_status;
                $block = $request->Block;
                $fromDate = $request->fromDate;
                $toDate = $request->toDate;
                $blockName = null;
                $applicationStatusName = null;
                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                if (isset($request->application_status) && $request->application_status != null) {
                    $applicationStatusName = application_statuses::where('application_statuses.id',  $application_status)->value('status_name');
                }
                $this->data['adv_data'] = ['search_text' => $search_text, 'application_status' => $application_status, 'applicationStatus' => $applicationStatusName, 'block_id' => $block, 'blockName' => $blockName, 'fromDate' => $fromDate, 'toDate' => $toDate];
                $this->data['AllData'] = application_data::leftjoin('application_data_ext_1', 'application_data.id',  'application_data_ext_1.application_data_id')
                    ->leftjoin('fnd_values as fnd_division', 'application_data.division',  'fnd_division.id')
                    ->leftjoin('fnd_values as fnd_district', 'application_data.district',  'fnd_district.id')
                    ->leftjoin('application_statuses as status', 'status.id',  'application_data.application_status')
                    ->leftjoin('fnd_values as fnd_block', 'application_data.block',  'fnd_block.id')
                    ->where('application_data.district', $UserDistrict)
                    ->where('application_data.status', '1')
                    ->select(
                        'application_data.id as applicationdataid',
                        'application_data.school_id',
                        'application_data.school_name',
                        'application_data.application_status',
                        'application_data.school_id',
                        'application_data.from_class',
                        'application_data.upto_class',
                        'fnd_division.display_value as Division',
                        'fnd_district.display_value as disrict',
                        'application_data.district as district_id',
                        'fnd_block.display_value as Block',
                        'application_data.police_station',
                        'application_data.panchayat',
                        'application_data.post_office',
                        'application_data.village_city',
                        'application_data.pincode',
                        'application_data.phone_with_std_code',
                        'application_data.email',
                        'application_data.society_name',
                        // 'application_data.opening_date',
                        // 'application_data.society_registration_valid_upto',
                        'application_data.school_chairman_name',
                        'application_data.school_chairman_office',
                        'application_data.school_chairman_email',
                        'application_data.school_chairman_phone_number',
                        'application_data.medium',
                        'application_data.created_at',
                        'application_data_ext_1.books',
                        'status.status_name AS status_name'
                    );
                // return($request);
                if (Auth::user()->user_role == 'dse') {
                    $this->data['AllData'] = $this->data['AllData']->where('application_data.application_status', '>', 1);
                } else if (Auth::user()->user_role == 'dc') {
                    $this->data['AllData'] = $this->data['AllData']->where('application_data.application_status', '>', 1);
                }
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['AllData'] = $this->data['AllData']->where(function ($query) use ($search_text) {
                        $query->where('application_data.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.medium', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.school_chairman_phone_number', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.phone_with_std_code', 'like', '%' .  $search_text . '%')
                            ->orWhere('application_data.id', 'like', '%' .  $search_text . '%');
                    });
                }
                // return($request->application_status);

                if ($request->application_status == 'verify') {
                    // return $request;
                    $this->data['AllData'] = $this->data['AllData']->where('application_data.district', $UserDistrict)->where('application_data.application_status', '!=', 1);
                    application_data::where('id', $request->applicationdataid)->update([
                        'application_status' => 2,
                        'updated_by' => Auth::user()->id
                    ]);
                }
                if (isset($request->application_status) && $request->application_status != null && $request->application_status > 0) {
                    if ($request->application_status == 2) {
                        $this->data['AllData'] = $this->data['AllData']->where('application_data.district', $UserDistrict)->whereIn('application_data.application_status', [2, 3]);
                    } else {

                        $this->data['AllData'] = $this->data['AllData']->where('application_data.district', $UserDistrict)->where('application_data.application_status', $application_status);
                    }
                } elseif (isset($request->application_status) && $request->application_status != null && $request->application_status == 0) {
                    $this->data['AllData'] = $this->data['AllData']->where('application_data.district', $UserDistrict)->where('application_data.application_status', '!=', 1);
                } else {
                    if (Auth::user()->user_role == 'dse') {
                        $this->data['AllData'] = $this->data['AllData']->where('application_data.district', $UserDistrict)->whereIn('application_data.application_status', [2, 3, 4, 8]);
                    } else if (Auth::user()->user_role == 'dc') {
                        $this->data['AllData'] = $this->data['AllData']->where('application_data.district', $UserDistrict)->whereIn('application_data.application_status', [4, 5, 6, 7]);
                    } else if (Auth::user()->user_role == 'faa') {
                        $this->data['AllData'] = $this->data['AllData']->whereIn('application_data.division', $UserDivision)->whereIn('application_data.application_status', [4, 5, 6]);
                    }
                }
                if (isset($request->Block) && $request->Block != null) {
                    $this->data['AllData'] = $this->data['AllData']->where('application_data.block',  $request->Block);
                }
                if ($request->fromDate != null && $request->toDate != null) {
                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $fromDate)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $toDate)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return redirect()->back()->with('warning', "Form date always lesser than To date");
                    }
                }
                if ($request->excel == 'excel') {
                    $this->data['AllData'] = $this->data['AllData']->get();
                } else {
                    $this->data['AllData'] = $this->data['AllData']->paginate(10);
                    //     echo '<pre>';
                    //   print_r($this->data['AllData']); exit;
                }
                //   return $this->data['AllData'];
                // return($this->data['AllData'] );
                foreach ($this->data['AllData'] as $key => $value) {
                    // return $value->applicationdataid;
                    $Message = [];
                    $Message = Conversations::Join('users as sender', 'conversations.sender_id', 'sender.id')
                        ->Join('users as reciever', 'conversations.reciever_id', 'reciever.id')
                        ->select(
                            'conversations.*',
                            'reciever.user_role as reciever_role',
                            'sender.user_role as sender_role',
                            'reciever.name as Reciever_name',
                            'sender.name as Sender_name'
                        )
                        ->Where('conversations.parent_id', $value->applicationdataid)
                        ->where('conversations.conversation_type', 4)
                        ->get();
                    $Message->images = [];
                    foreach ($Message as  $Conversation) {
                        $Conversation->attachments = json_decode($Conversation->attachments);
                    }
                    // array_push($appeal_conversations,$Message);
                    if (Auth::user()->user_role == 'dse') {
                        $value->application_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->applicationdataid)->where('conversations.conversation_type', 4)->where('dse_read_status', 0)->count();
                    } elseif (Auth::user()->user_role == 'dc') {
                        $value->application_conversation_unread_count = Conversations::Where('conversations.parent_id', $value->applicationdataid)->where('conversations.conversation_type', 4)->where('dc_read_status', 0)->count();
                    }
                    $value->message_recode = $Message;
                }
                $now = Carbon::now();
                $date = 0;
                $this->data['last_update'] = "";
                $RemainingDate = [];
                $days_to_varified_by_dse = fnd_settings::value('verification_by_dse_limit');
                $days_to_varified_by_dc = fnd_settings::value('schedule_meeting_by_dc_limit');
                $days_to_varified_by_dc_after_meeting = fnd_settings::value('take_decision_after_meeting_held_by_dc');
                $faa_decision_limit = fnd_settings::value('faa_decision_limit');
                $saa_decision_limit = fnd_settings::value('saa_decision_limit');
                $status_offset = 0;
                foreach ($this->data['AllData'] as $row) {
                    $id = application_tracking::where('application_id', $row->applicationdataid)->where('school_id', $row->school_id)->max('id');
                    $status_tracking =  application_tracking::where('id', $id)->first();
                    if ($status_tracking) {
                        $startDate = $status_tracking->created_at;
                        $this->data['last_update'] = $startDate;
                        $cDate = Carbon::parse($startDate);
                        $count = $now->diffInDays($cDate);
                        if ($this->data['AllData'][$status_offset]['application_status'] == 2 || $this->data['AllData'][$status_offset]['application_status'] == 3) {
                            $date = $days_to_varified_by_dse - (int)$count;
                        } elseif ($this->data['AllData'][$status_offset]['application_status'] == 4) {
                            $date = $days_to_varified_by_dc - (int)$count;
                        } elseif ($this->data['AllData'][$status_offset]['application_status'] == 5) {
                            $meeting_date = application_meetings::where('application_id', $row->applicationdataid)->value('meeting_date');
                            $this->data['last_update'] = $meeting_date;
                            $intDate = Carbon::parse($meeting_date);
                            $count = $now->diffInDays($intDate);
                            $date = $days_to_varified_by_dc_after_meeting - (int)$count;
                        } elseif ($this->data['AllData'][$status_offset]['application_status'] == 9) {
                            $date = $faa_decision_limit - (int)$count;
                        } elseif ($this->data['AllData'][$status_offset]['application_status'] == 12) {
                            $date = $saa_decision_limit - (int)$count;
                        }
                        // return $this->data['last_update'];
                        //if (Auth::user()->user_role == 'dse') {
                        //$date = $days_to_varified_by_dse - (int)$count;
                        $this->data['days_to_varified_by_dse'] = $days_to_varified_by_dse;
                        //}
                        //if (Auth::user()->user_role == 'dc') {
                        // seen accouding to status
                        //if ($this->data['AllData'][$status_offset]['application_status'] == 4) {
                        //$date = $days_to_varified_by_dc - (int)$count;
                        $this->data['days_to_varified_by_dc'] = $days_to_varified_by_dc;
                        //} else {
                        //$date = $days_to_varified_by_dc_after_meeting - (int)$count;
                        $this->data['days_to_varified_by_dc_after_meeting'] = $days_to_varified_by_dc_after_meeting;
                        $this->data['faa_decision_limit'] = $faa_decision_limit;
                        $this->data['saa_decision_limit'] = $saa_decision_limit;
                        //}
                        //}
                    }
                    array_push($RemainingDate, $date);
                    $status_offset++;
                }
                $this->data['Count'] = $RemainingDate;
                // $this->data['All_Data'] = ['AllData' => $Alldata, 'Count' => $RemainingDate];
                if ($request->excel == 'excel') {
                    if (count($this->data['AllData']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['AllData'], 'view' => 'excel_view.application_excel', 'total' => $this->data['Count']];
                    $filename = 'application-list.xlsx';
                    return Excel::download(new PaymentExport($params), $filename);
                } else {
                    // return $this->data;
                    return view('application_verify', $this->data);
                }
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function showAllData($req)
    {
        $id = Crypt::decrypt($req);
        $users = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
            ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
            ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_data_id')
            ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.application_data_id')
            ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
            ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.application_data_id')
            ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
            ->leftjoin('application_teacher_data', 'application_data.id', '=', 'application_teacher_data.application_data_id')
            ->where('application_data.id', $id)
            ->where('application_data.status', 1)
            ->select(
                'application_data.id as applicationId',
                'application_data.*',
                'application_data.school_id as schoolId',
                'application_data_ext_1.id as application_data_ext_1_id',
                'application_data_ext_1.trainee_qualification as pri_trainee_qualification',
                'application_data_ext_1.teaching_experience as pri_teaching_experience',
                'application_data_ext_1.date_of_appointment as pri_date_of_appointment',
                'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                'application_data_ext_1.all_teaching_material_list_files as all_teaching_material_list_files_ex',
                'application_data.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature1',
                'application_data_ext_1.*',
                'application_teacher_data.id as application_teacher_data_id',
                'application_teacher_data.id as application_teacher_data_id',
                'application_teacher_data.*',
                'application_data_ext_files.id as application_data_ext_files_id',
                'application_data_ext_files.*',
                'application_data_files.id as application_data_files_id',
                'application_data_files.*',
                'application_data_remarks.id as application_data_remarks_id',
                'application_data_remarks.*',
                'application_data_remarks_ext_1s.id as application_data_remarks_ext_1s_id',
                'application_data_remarks_ext_1s.*',
                'application_data_verifications.id as application_data_verifications_id',
                'application_data_verifications.*',
                'application_data_verification_ext_1s.id as application_data_verification_ext_1s_id',
                'application_data_verification_ext_1s.*'
            )
            ->first();
        // echo "<pre>"; print_r($users);exit;
        $application_teacher_data = application_teacher_data::where('application_teacher_data.application_data_id', $id)->get();
        // return $application_teacher_data;
        // echo "<pre>"; print_r($application_teacher_data);exit;
        $users->application_teacher_data = $application_teacher_data;
        if ($users->session_year != null)
            $users->session_year = json_decode($users->session_year, true);
        if ($users->income != null)
            $users->income = json_decode($users->income, true);
        if ($users->expenses != null)
            $users->expenses = json_decode($users->expenses, true);
        if ($users->surplus_money != null)
            $users->surplus_money = json_decode($users->surplus_money, true);
        if ($users->reduced_money != null)
            $users->reduced_money = json_decode($users->reduced_money, true);
        if ($users->class_finance != null)
            $users->class_finance = json_decode($users->class_finance, true);
        if ($users->teaching_fee != null)
            $users->teaching_fee = json_decode($users->teaching_fee, true);
        if ($users->registration_fee != null)
            $users->registration_fee = json_decode($users->registration_fee, true);
        if ($users->enrollment_fee != null)
            $users->enrollment_fee = json_decode($users->enrollment_fee, true);
        if ($users->security_money != null)
            $users->security_money = json_decode($users->security_money, true);
        if ($users->development_fee != null)
            $users->development_fee = json_decode($users->development_fee, true);
        if ($users->annual_charge != null)
            $users->annual_charge = json_decode($users->annual_charge, true);
        if ($users->other_charges != null)
            $users->other_charges = json_decode($users->other_charges, true);
        if ($users->total != null)
            $users->total = json_decode($users->total, true);
        if ($users->class_names != null)
            $users->class_names = json_decode($users->class_names, true);
        if ($users->no_of_sections != null)
            $users->no_of_sections = json_decode($users->no_of_sections, true);
        if ($users->no_of_students != null)
            $users->no_of_students = json_decode($users->no_of_students, true);
        // if ($users->teacher_name != null)
        //     $users->teacher_name = json_decode($users->teacher_name, true);
        // if ($users->teacher_f_h_w_name != null)
        //     $users->teacher_f_h_w_name = json_decode($users->teacher_f_h_w_name, true);
        // if ($users->date_of_birth != null)
        //     $users->date_of_birth = json_decode($users->date_of_birth, true);
        // if ($users->education_qualification != null)
        //     $users->education_qualification = json_decode($users->education_qualification, true);
        // if ($users->trainee_qualification != null)
        //     $users->trainee_qualification = json_decode($users->trainee_qualification, true);
        // if ($users->teaching_experience != null)
        //     $users->teaching_experience = json_decode($users->teaching_experience, true);
        // if ($users->cls_handed_over != null)
        //     $users->cls_handed_over = json_decode($users->cls_handed_over, true);
        // if ($users->date_of_appointment != null)
        //     $users->date_of_appointment = json_decode($users->date_of_appointment, true);
        // if ($users->trained_or_untrained != null)
        //     $users->trained_or_untrained = json_decode($users->trained_or_untrained, true);
        if ($users->teacher_student_ratio != null)
            $users->teacher_student_ratio = json_decode($users->teacher_student_ratio, true);
        if ($users->teacher_student_assign != null)
            $users->teacher_student_assign = json_decode($users->teacher_student_assign, true);
        if ($users->num_of_teacher != null)
            $users->num_of_teacher = json_decode($users->num_of_teacher, true);
        if ($users->details_of_curriculum != null)
            $users->details_of_curriculum = json_decode($users->details_of_curriculum, true);
        if ($users->method_of_inspection != null)
            $users->method_of_inspection = json_decode($users->method_of_inspection, true);
        if ($users->evidence_of_non_proprietary_nature_ex != null)
            $users->evidence_of_non_proprietary_nature_ex = json_decode($users->evidence_of_non_proprietary_nature_ex, true);
        if ($users->last_three_year_tot_income_files_1 != null)
            $users->last_three_year_tot_income_files_1 = json_decode($users->last_three_year_tot_income_files_1, true);
        if ($users->last_three_year_tot_income_files_2 != null)
            $users->last_three_year_tot_income_files_2 = json_decode($users->last_three_year_tot_income_files_2, true);
        if ($users->last_three_year_tot_income_files_3 != null)
            $users->last_three_year_tot_income_files_3 = json_decode($users->last_three_year_tot_income_files_3, true);
        if ($users->water_facilities != null)
            $users->water_facilities = json_decode($users->water_facilities, true);
        if ($users->school_boundary_files != null)
            $users->school_boundary_files = json_decode($users->school_boundary_files, true);
        if ($users->school_cctv_file != null)
            $users->school_cctv_file = json_decode($users->school_cctv_file, true);
        if ($users->school_ground_file != null)
            $users->school_ground_file = json_decode($users->school_ground_file, true);
        if ($users->school_water_hygene_facilities_files != null)
            $users->school_water_hygene_facilities_files = json_decode($users->school_water_hygene_facilities_files, true);
        if ($users->school_fire_safty_facilities_files != null)
            $users->school_fire_safty_facilities_files = json_decode($users->school_fire_safty_facilities_files, true);
        if ($users->all_teaching_material_list_files_ex != null)
            $users->all_teaching_material_list_files_ex = json_decode($users->all_teaching_material_list_files_ex, true);
        if ($users->all_sports_equipment_list_file != null)
            $users->all_sports_equipment_list_file = json_decode($users->all_sports_equipment_list_file, true);
        if ($users->educational_qualification_files != null)
            $users->educational_qualification_files = json_decode($users->educational_qualification_files, true);
        if ($users->teacher_educational_qualification_files != null)
            $users->teacher_educational_qualification_files = json_decode($users->teacher_educational_qualification_files, true);
        if ($users->cleanliness != null)
            $users->cleanliness = json_decode($users->cleanliness, true);
        if ($users->books_in_library != null)
            $users->books_in_library = json_decode($users->books_in_library, true);
        if ($users->school_rent_deatail != null)
            $users->school_rent_deatail = json_decode($users->school_rent_deatail, true);
        if ($users->school_area_deatail != null)
            $users->school_area_deatail = json_decode($users->school_area_deatail, true);
        if ($users->one_lac_fd_proof != null)
            $users->one_lac_fd_proof = json_decode($users->one_lac_fd_proof, true);
        $users->district = Fnd_value::where('fnd_values.id',  $users->district)->value('display_value');
        $Accepted_Fields = 0;
        $Rejected_Fields = 0;
        $pending_Fields = 0;
        $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
        // echo $alltable_col;
        foreach ($alltable_col as $key => $value) {
            if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at") {
                if (DB::table('application_data_verifications')->where('application_data_verifications.application_data_id', $id)->where($value, 2)->exists()) {
                    $Accepted_Fields++;
                }
                if (DB::table('application_data_verifications')->where('application_data_verifications.application_data_id', $id)->where($value, 3)->exists()) {
                    $Rejected_Fields++;
                }
                if (DB::table('application_data_verifications')->where('application_data_verifications.application_data_id', $id)->where($value, 1)->exists()) {
                    $pending_Fields++;
                }
            }
        }
        $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
        // echo $alltable_col;
        foreach ($alltable_ext_col as $key => $value) {
            if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {
                if (DB::table('application_data_verification_ext_1s')->where('application_data_verification_ext_1s.application_data_id', $id)->where($value, 2)->exists()) {
                    $Accepted_Fields++;
                }
                if (DB::table('application_data_verification_ext_1s')->where('application_data_verification_ext_1s.application_data_id', $id)->where($value, 3)->exists()) {
                    $Rejected_Fields++;
                }
                if (DB::table('application_data_verification_ext_1s')->where('application_data_verification_ext_1s.application_data_id', $id)->where($value, 1)->exists()) {
                    $pending_Fields++;
                }
            }
        }
        $users->Accepted = $Accepted_Fields;
        // echo "<pre>";
        // print_r($users->application_teacher_data[2]->application_data_id);
        // exit();
        return view('districttest', ['user' => $users]);
    }
    public function schoolList(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_role == 'dse') {
                $UserDistrict = User::leftjoin('addresses', 'addresses.user_id', 'users.id')
                    ->where('addresses.user_id', Auth::user()->id)
                    ->value('addresses.district');
                // return($UserDistrict);
                $this->data['block_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
                    ->pluck('fnd_values.display_value as block_name', 'fnd_values.id as block_id');
                $this->data['school_list'] = DB::table('private_schools')
                    ->join('users', 'private_schools.id', '=', 'users.school_id')
                    ->select(
                        'private_schools.id',
                        'private_schools.school_code',
                        'private_schools.school_name',
                        'private_schools.school_email',
                    )->where('user_role', 'school')->pluck('school_name', 'id');
                // Private_school::where('id',Auth::user()->school_id)->pluck('school_name','id');
                $search_text = $request->search_text;
                $block = $request->Block;
                // return[$block];
                $school = $request->schoolList;
                $from_date = $request->from_date;
                $to_date = $request->to_date;
                $blockName = null;
                $schoolName = null;
                if (isset($request->Block) && $request->Block != null) {
                    $blockName = Fnd_value::where('fnd_values.id',  $block)->value('display_value');
                }
                if (isset($request->schoolList) && $request->schoolList != null) {
                    $schoolName = Private_school::where('private_schools.id', $school)->value('school_name');
                }
                $this->data['adv_data'] = ['search_text' => $search_text, 'block_name' => $blockName, 'block_id' => $block, 'from_date' => $from_date, 'to_date' => $to_date, 'school' => $school];
                $this->data['Alldata'] = DB::table('private_schools')
                    ->leftjoin('application_data', 'application_data.school_id', 'private_schools.id')
                    ->leftjoin('fnd_values', 'application_data.block', '=', 'fnd_values.id')
                    ->join('users', 'private_schools.id', '=', 'users.school_id')
                    ->select(
                        'private_schools.id',
                        'private_schools.school_code',
                        'private_schools.school_name',
                        'application_data.district',
                        'private_schools.school_email',
                        'application_data.block',
                        'fnd_values.display_value as block_name',
                        //    'private_schools.created_at', 
                        DB::raw('DATE_FORMAT(private_schools.created_at, "%d/%m/%Y %H:%i %p") as created_at'),
                        'users.name',
                        'users.user_role',
                        'users.email as useremail'
                    )->where('user_role', 'school')
                    ->where('application_data.district', $UserDistrict);
                if (isset($request->search_text) && $request->search_text != null) {
                    $this->data['Alldata'] =  $this->data['Alldata']->where(function ($query) use ($search_text) {
                        $query->where('private_schools.school_code', 'like', '%' .  $search_text . '%')
                            ->orWhere('private_schools.school_name', 'like', '%' .  $search_text . '%')
                            ->orWhere('private_schools.school_email', 'like', '%' .  $search_text . '%')
                            ->orWhere('users.name', 'like', '%' .  $search_text . '%')
                            ->orWhere('users.email', 'like', '%' .  $search_text . '%');
                    });
                }
                if (isset($request->Block) && $request->Block != null) {
                    $this->data['Alldata'] =  $this->data['Alldata']->where('application_data.district', $UserDistrict)->where('application_data.block',  $request->Block);
                }
                if (isset($request->school_status) && $request->school_status != null && $request->school_status > 0) {
                    $this->data['Alldata'] =  $this->data['Alldata']->where('application_data.district', $UserDistrict)->where('application_data.application_status',  $request->school_status);
                }
                if ($request->from_date != null && $request->to_date != null) {
                    $fromDate =  date('Y-m-d 00:00:00', strtotime(str_replace("/", "-",  $from_date)));
                    $toDate =  date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $to_date)));
                    //  return[$fromDate,$toDate ];
                    if ($fromDate >=  $toDate) {
                        return redirect()->back()->with('warning', "Form date always lesser than To date");
                    }
                    //return[$start_date,$end_date];  
                    $this->data['Alldata']->whereBetween('private_schools.created_at', [$fromDate, $toDate]);
                }
                if ($request->submit == 'excel') {
                    $this->data['Alldata'] = $this->data['Alldata']->get();
                } else {
                    $this->data['Alldata'] = $this->data['Alldata']->paginate(15);
                    //           echo '<pre>';
                    //   print_r($this->data['Alldata']); exit;
                }
                // return( $this->data);
                if ($request->submit == 'excel') {
                    if (count($this->data['Alldata']) == 0) {
                        return redirect()->back()->with('warning', "There are no such data to export!!");
                    }
                    $params = ['data' => $this->data['Alldata'], 'view' => 'excel_view.schools_excel'];
                    $filename = 'schools.xlsx';
                    return Excel::download(new ExcelExport($params), $filename);
                } else {
                    return view('school', $this->data);
                }
            } else {
                return redirect('dashboard')->with('message', 'You are not Authorised!!');
            }
        }
    }
    public function EditAppForm($id)
    {
        // return $id;
        $userData = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
            ->where('application_data.created_by', $id)->where('application_data.status', 1)
            ->get();
        // return $userData;
        return view('applicationEdit', ['userData' => $userData]);
    }
    public function acceptSchoolDetail(Request $req)
    {
        // return $req;
        $Application_ID = application_data::where('application_data.school_id', $req->approve_schoolId)->value('id');
        $user = Auth::user();
        $field_name_ext  = substr($req->fieldName, '-4');
        $field_name  = substr($req->fieldName, '0', '-4');
        //  return $field_name.'  -  '.$field_name_ext;
        if ($req->has('file_reason')) {
            $ReasonFileArr = [];
            foreach ($req->file('file_reason') as $file) {
                $fileInstance = $file;
                $newFileName = "approve_reason_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications/dsc_verification'), $newFileName);
                $ReasonFileArr[] = $newFileName;
            }
            if (count($ReasonFileArr) != 0) { // if have any file, otherwise null saved already
                $json_encodeFile_array = json_encode($ReasonFileArr);
            } else {
                $json_encodeFile_array = null;
            }
        } else {
            $json_encodeFile_array = null;
        }
        // if ($req->has('file_reason')) {
        //     $image = $req->file_reason;
        //     $fileName = time() . '.' . $req->file_reason->getClientOriginalName();
        //     $req->file_reason->move(public_path('assets/applications/dsc_verification'), $fileName);
        //     $path = 'public/applications/dsc_verification/' . $fileName;
        // }
        if ($field_name_ext == '_ext') {
            // return "ext".$req;
            // Table::where('field', $value)->where('field', $value)->update(["'field_name1'=> $value1, 'field_name2'=> $value2"]);
            $application_data_remarks_ext_1 = application_data_remarks_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
            if (!empty($application_data_remarks_ext_1)) {
                $users = application_data_remarks_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                    $field_name . '_rmk' => $req->remarks
                ]);
            }
            $application_data_verification_ext_1 = application_data_verification_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
            if (!empty($application_data_verification_ext_1)) {
                $count_verified_field = application_data_verification::where('school_id', $req->approve_schoolId)->value('count_verified_field');
                $selected_filed_value = application_data_verification_ext_1::where('school_id', $req->approve_schoolId)->where('application_data_id', $Application_ID)->value($field_name . '_vrfy');
                $count_verified_field = (int)$count_verified_field + 1;
                if ($selected_filed_value == 1) {
                    application_data_verification::where('application_data_id', $Application_ID)->update([
                        'count_verified_field' => $count_verified_field
                    ]);
                }
                application_data_verification_ext_1::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                    $field_name . '_vrfy' => $req->status
                ]);
            }
            $application_data_ext_file = application_data_ext_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
            if (!empty($application_data_ext_file)) {
                $usersStatus = application_data_ext_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                    $field_name . '_files' => $json_encodeFile_array
                ]);
            }
        } else {
            // return $req;
            $application_data_remarks = application_data_remarks::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
            if (!empty($application_data_remarks)) {
                $users = application_data_remarks::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                    $req->fieldName . '_rmk' => $req->remarks
                ]);
            }
            $application_data_file = application_data_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
            if (!empty($application_data_file)) {
                $usersStatus = application_data_file::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                    $req->fieldName . '_files' => $json_encodeFile_array
                ]);
            }
            $application_data_verification = application_data_verification::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->first();
            if (!empty($application_data_verification)) {
                $count_verified_field = application_data_verification::where('school_id', $req->approve_schoolId)->value('count_verified_field');
                $selected_filed_value = application_data_verification::where('school_id', $req->approve_schoolId)->where('application_data_id', $Application_ID)->value($req->fieldName . '_vrfy');
                $count_verified_field = (int)$count_verified_field + 1;
                if ($selected_filed_value == 1) {
                    application_data_verification::where('application_data_id', $Application_ID)->update([
                        'count_verified_field' => $count_verified_field
                    ]);
                }
                application_data_verification::where('application_data_id', $req->approve_applicationId)->where('school_id', $req->approve_schoolId)->update([
                    $req->fieldName . '_vrfy' => $req->status
                ]);
            }
        }
        // return $req->approve_schoolId;
        $Application_ID = application_data::where('application_data.school_id', $req->approve_schoolId)->value('id');
        $Application_Status = application_data::where('application_data.school_id', $req->approve_schoolId)->value('application_status');
        // return $Application_Status ;
        if ($Application_Status == 2) {
            $application_tracking = new application_tracking;
            $application_tracking->application_id = $Application_ID;
            $application_tracking->school_id = $req->approve_schoolId;
            $application_tracking->application_status_id = 3;

            $phone = Contact::where('school_id', $req->approve_schoolId)->value('mobile');
            $msg = 'Your application is processed successfully and is under verification.';
            $this->TrackingSMS($phone, $msg);

            $application_tracking->created_by = $user->id;
            $application_tracking->save();
            application_data::where('id', $Application_ID)->update([
                'application_status' => 3,
                'updated_by' => $user->id
            ]);
        }
        return redirect()->back()->with('success', 'Accept Successfully !!!');
    }
    public function rejectSchoolDetail(Request $req)
    {
        // return $req;
        $user = Auth::user();
        $field_name_ext  = substr($req->fieldName, '-4');
        $field_name  = substr($req->fieldName, '0', '-4');
        if ($req->has('file_reason')) {
            $ReasonFileArr = [];
            foreach ($req->file('file_reason') as $file) {
                $fileInstance = $file;
                $newFileName = "reject_reason_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications/dsc_verification'), $newFileName);
                $ReasonFileArr[] = $newFileName;
            }
            if (count($ReasonFileArr) != 0) { // if have any file, otherwise null saved already
                $json_encodeFile_array = json_encode($ReasonFileArr);
            } else {
                $json_encodeFile_array = null;
            }
        } else {
            $json_encodeFile_array = null;
        }
        // if ($req->has('file_reason')) {
        //     $image = $req->file_reason;
        //     $fileName = time() . '.' . $req->file_reason->getClientOriginalName();
        //     $req->file_reason->move(public_path('assets/applications/dsc_verification'), $fileName);
        //     $path = 'public/applications/dsc_verification/' . $fileName;
        // }
        //  return $field_name.'  -  '.$field_name_ext;
        if ($field_name_ext == '_ext') {
            $application_data_remarks_ext_1 = application_data_remarks_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_remarks_ext_1)) {
                $users = application_data_remarks_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $field_name . '_rmk' => $req->remarks
                ]);
            }
            $application_data_verification_ext_1 = application_data_verification_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_verification_ext_1)) {
                $count_verified_field = application_data_verification::where('school_id', $req->reject_schoolId)->value('count_verified_field');
                $selected_filed_value = application_data_verification_ext_1::where('school_id', $req->reject_schoolId)->where('application_data_id', $req->reject_applicationId)->value($field_name . '_vrfy');
                $count_verified_field = (int)$count_verified_field + 1;
                if ($selected_filed_value == 1) {
                    application_data_verification::where('application_data_id', $req->reject_applicationId)->update([
                        'count_verified_field' => $count_verified_field
                    ]);
                }
                application_data_verification_ext_1::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $field_name . '_vrfy' => $req->status
                ]);
            }
            $application_data_ext_file = application_data_ext_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_ext_file)) {
                $usersStatus = application_data_ext_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $field_name . '_files' => $json_encodeFile_array
                ]);
            }
        } else {
            $application_data_remarks = application_data_remarks::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_remarks)) {
                $users = application_data_remarks::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $req->fieldName . '_rmk' => $req->remarks
                ]);
            }
            $application_data_verification = application_data_verification::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_verification)) {
                $count_verified_field = application_data_verification::where('school_id', $req->reject_schoolId)->value('count_verified_field');
                $selected_filed_value = application_data_verification::where('school_id', $req->reject_schoolId)->where('application_data_id', $req->reject_applicationId)->value($req->fieldName . '_vrfy');
                $count_verified_field = (int)$count_verified_field + 1;
                // return $selected_filed_value;
                // return $count_verified_field;
                if ($selected_filed_value == 1) {
                    application_data_verification::where('application_data_id', $req->reject_applicationId)->update([
                        'count_verified_field' => $count_verified_field
                    ]);
                }
                application_data_verification::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $req->fieldName . '_vrfy' => $req->status
                ]);
            }
            $application_data_file = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_file)) {
                $usersStatus = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $req->fieldName . '_files' => $json_encodeFile_array
                ]);
            }
            $application_data_file = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->first();
            if (!empty($application_data_file)) {
                $usersStatus = application_data_file::where('application_data_id', $req->reject_applicationId)->where('school_id', $req->reject_schoolId)->update([
                    $req->fieldName . '_files' => $json_encodeFile_array
                ]);
            }
        }
        $Application_ID = application_data::where('application_data.school_id', $req->reject_schoolId)->value('id');
        $Application_Status = application_data::where('application_data.school_id', $req->reject_schoolId)->value('application_status');
        if ($Application_Status == 2) {

            $application_tracking = new application_tracking;
            $application_tracking->application_id = $req->reject_applicationId;
            $application_tracking->school_id = $req->reject_schoolId;
            $application_tracking->application_status_id = 3;

            $phone = Contact::where('school_id', $req->reject_schoolId)->value('mobile');
            $msg = 'Your application is processed successfully and is under verification.';
            $this->TrackingSMS($phone, $msg);

            $application_tracking->created_by = $user->id;
            $application_tracking->save();
            application_data::where('id', $Application_ID)->update([
                'application_status' => 3,
                'updated_by' => $user->id
            ]);
        }
        return redirect()->back()->with('warning', 'Reject Successfully !!!');
    }
    public function ApplicationMeeting(Request $req)
    {
        // return $req;
        $user = Auth::user();
        $date = Date('Y-m-d H:i:s');
        $decode_image = '';
        // $image_name = array();
        if ($req->meeting_document != null) {
            foreach ($req->meeting_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/applications/dc_application/meeting'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
        }
        $application_meetings = application_meetings::where('application_id', $req->ApplicationId)->where('school_id', $req->SchoolId)->exists();
        if (!empty($application_meetings)) {
            application_meetings::where('application_id', $req->ApplicationId)->where('school_id', $req->SchoolId)->update([
                'application_id' => $req->ApplicationId,
                'school_id' => $req->SchoolId,
                'meeting_date' => $req->meeting_deadline,
                'meeting_remarks' => $req->meeting_remarks,
                'meeting_documents' => $decode_image,
                'meeting_status' => 1,
                'updated_by' => $user->id
            ]);
        } else {
            $application_meetings = new application_meetings;
            $application_meetings->application_id = $req->ApplicationId;
            $application_meetings->school_id = $req->SchoolId;
            $application_meetings->meeting_date = $req->meeting_deadline;
            $application_meetings->meeting_remarks = $req->meeting_remarks;
            $application_meetings->meeting_documents = $decode_image;
            $application_meetings->meeting_status = 1;
            $application_meetings->created_by = $user->id;
            $application_meetings->save();
        }
        //$application_meetings = new application_meetings;
        //$application_meetings->application_id = $req->ApplicationId;
        //$application_meetings->school_id = $req->SchoolId;
        //$application_meetings->meeting_date = $req->meeting_deadline;
        //$application_meetings->meeting_remarks = $req->meeting_remarks;
        //$application_meetings->meeting_documents = $decode_image;
        //$application_meetings->meeting_status = 1;
        //$application_meetings->created_by = $user->id;
        //$application_meetings->save();
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $req->ApplicationId;
        $application_tracking->school_id = $req->SchoolId;
        $application_tracking->application_status_id = 5;

        $phone = Contact::where('school_id', $req->SchoolId)->value('mobile');
        $msg = 'Your application is in progress and a meeting is scheduled for review by District Committee on ' . $req->meeting_deadline . ' . ';
        $this->TrackingSMS($phone, $msg);

        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        $Conversations = new Conversations;
        $Conversations->conversation_type = '1';
        $Conversations->parent_id = $req->ApplicationId;
        $Conversations->sender_id = $user->id;
        $Conversations->reciever_id = User::where('school_id', $req->SchoolId)->where('user_role', 'school')->value('id');
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->meeting_remarks;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        application_data::where('id', $req->ApplicationId)->update([
            'application_status' => 5,
            'updated_by' => $user->id
        ]);
        return redirect('application-verify')->with('success', 'Meeting Scheduled Successfully !!!');
    }
    public function ApplicationAccept(Request $req)
    {
        // return $req;
        $user = Auth::user();
        $date = Date('Y-m-d H:i:s');
        $decode_image = '';
        if ($req->accpet_document != null) {
            foreach ($req->accpet_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/applications/dc_application/accept'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
        }
        $application_certificate = new certificate;
        $application_certificate->school_id = $req->SchoolId;
        application_data::where('school_id', $req->SchoolId)->where('status', 1)->value('apllication_id');
        $application_form = application_data::where('school_id', $req->SchoolId)->value('district');
        $display_short_value = Fnd_value::where('id',  $application_form)->value('display_short_value');
        $district = application_data::where('application_status', 6)->where('district', $application_form)->count();
        $district++;
        $district = strlen($district) == 1 ? '000' . $district : (strlen($district) == 2 ? '00' . $district : (strlen($district) == 3 ? '0' . $district : $district));
        $certificte_number = "";
        $certificte_number .= "" . date('Y') . '/20' . $display_short_value . "/" . $district;
        $application_certificate->certificate_number = $certificte_number ?? $req->certificte_number;
        $application_certificate->certificate_document = $decode_image;
        $application_certificate->certifed_by = $user->id;
        $application_certificate->created_by = $user->id;
        $application_certificate->save();
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $req->ApplicationId;
        $application_tracking->school_id = $req->SchoolId;
        $application_tracking->application_status_id = 6;

        $phone = Contact::where('school_id', $req->SchoolId)->value('mobile');
        $msg = 'Congratulations!! Your school has been recognised to issue certificate under Right to Education System.';
        $this->TrackingSMS($phone, $msg);

        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        $Conversations = new Conversations;
        $Conversations->conversation_type = '1';
        $Conversations->parent_id = $req->ApplicationId;
        $Conversations->sender_id = $user->id;
        $Conversations->reciever_id = User::where('school_id', $req->SchoolId)->where('user_role', 'school')->value('id');
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->accept_remarks;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        application_data::where('id', $req->ApplicationId)->update([
            'application_status' => 6,
            'updated_by' => $user->id
        ]);
        return redirect('application-verify')->with('success', 'Application Accepted Successfully !!!');
    }
    public function ApplicationReject(Request $req)
    {
        // return $req;
        $user = Auth::user();
        $decode_image = '';
        if ($req->reject_document != null) {
            foreach ($req->reject_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/applications/dc_application/reject'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
        }
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $req->ApplicationId;
        $application_tracking->school_id = $req->SchoolId;
        $application_tracking->application_status_id = 7;

        $phone = Contact::where('school_id', $req->SchoolId)->value('mobile');
        $msg = 'Your application was rejected by District Committee due to some recognised issues. ';
        $this->TrackingSMS($phone, $msg);

        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        $Conversations = new Conversations;
        $Conversations->conversation_type = '1';
        $Conversations->parent_id = $req->ApplicationId;
        $Conversations->sender_id = $user->id;
        $Conversations->reciever_id = User::where('school_id', $req->SchoolId)->where('user_role', 'school')->value('id');
        $Conversations->title = 'Title';
        $Conversations->remarks = $req->reject_remarks;
        $Conversations->attachments = $decode_image;
        $Conversations->save();
        application_data::where('id', $req->ApplicationId)->update([
            'application_status' => 7,
            'updated_by' => $user->id
        ]);
        return redirect('application-verify')->with('warning', 'Application Rejected Successfully !!!');
    }
    //Fetch Data school application if already exist
    public function getApplicationData()
    {
        $userData = array();
        $userData = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
            ->leftjoin('application_teacher_data', 'application_data.id', '=', 'application_teacher_data.application_data_id')
            ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
            ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_data_id')
            ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.application_data_id')
            ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
            ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.application_data_id')
            ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
            ->where('application_data.school_id', Auth::user()->school_id)->where('application_data.status', 1)
            ->select(
                'application_data.id as id',
                'application_data.*',
                'application_data_ext_1.id as application_data_ext_1_id',
                'application_data_ext_1.trainee_qualification as pri_trainee_qualification',
                'application_data_ext_1.teaching_experience as pri_teaching_experience',
                'application_data_ext_1.date_of_appointment as pri_date_of_appointment',
                'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                'application_data_ext_1.all_teaching_material_list_files as all_teaching_material_list_files_ex',
                'application_data.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature1',
                'application_data_ext_1.*',
                'application_data_ext_files.id as application_data_ext_files_id',
                'application_data_ext_files.*',
                'application_data_files.id as application_data_files_id',
                'application_data_files.*',
                'application_data_remarks.id as application_data_remarks_id',
                'application_data_remarks.*',
                'application_data_remarks_ext_1s.id as application_data_remarks_ext_1s_id',
                'application_data_remarks_ext_1s.*',
                'application_data_verifications.id as application_data_verifications_id',
                'application_data_verifications.*',
                'application_data_verification_ext_1s.id as application_data_verification_ext_1s_id',
                'application_data_verification_ext_1s.*'
            )
            ->first();
        // echo "<pre>"; print_r($userData);exit;
        $application_teacher_data = application_teacher_data::where('application_teacher_data.school_id', Auth::user()->school_id)->get();
        $userData->application_teacher_data = $application_teacher_data;
        $allclasses = DB::table('fnd_classes')->where('status', 1)->get();
        if (!empty($userData)) {
            if ($userData->session_year != null)
                $userData->session_year = json_decode($userData->session_year, true);
            if ($userData->income != null)
                $userData->income = json_decode($userData->income, true);
            if ($userData->expenses != null)
                $userData->expenses = json_decode($userData->expenses, true);
            if ($userData->surplus_money != null)
                $userData->surplus_money = json_decode($userData->surplus_money, true);
            if ($userData->reduced_money != null)
                $userData->reduced_money = json_decode($userData->reduced_money, true);
            if ($userData->class_finance != null)
                $userData->class_finance = json_decode($userData->class_finance, true);
            if ($userData->teaching_fee != null)
                $userData->teaching_fee = json_decode($userData->teaching_fee, true);
            if ($userData->registration_fee != null)
                $userData->registration_fee = json_decode($userData->registration_fee, true);
            if ($userData->enrollment_fee != null)
                $userData->enrollment_fee = json_decode($userData->enrollment_fee, true);
            if ($userData->security_money != null)
                $userData->security_money = json_decode($userData->security_money, true);
            if ($userData->development_fee != null)
                $userData->development_fee = json_decode($userData->development_fee, true);
            if ($userData->annual_charge != null)
                $userData->annual_charge = json_decode($userData->annual_charge, true);
            if ($userData->other_charges != null)
                $userData->other_charges = json_decode($userData->other_charges, true);
            if ($userData->total != null)
                $userData->total = json_decode($userData->total, true);
            if ($userData->details_of_curriculum != null)
                $userData->details_of_curriculum = json_decode($userData->details_of_curriculum, true);
            if ($userData->method_of_inspection != null)
                $userData->method_of_inspection = json_decode($userData->method_of_inspection, true);
            // if ($userData->teacher_name != null)
            //     $userData->teacher_name = json_decode($userData->teacher_name, true);
            // if ($userData->teacher_f_h_w_name != null)
            //     $userData->teacher_f_h_w_name = json_decode($userData->teacher_f_h_w_name, true);
            // if ($userData->date_of_birth != null)
            //     $userData->date_of_birth = json_decode($userData->date_of_birth, true);
            // if ($userData->education_qualification != null)
            //     $userData->education_qualification = json_decode($userData->education_qualification, true);
            // if ($userData->trainee_qualification != null)
            //     $userData->trainee_qualification = json_decode($userData->trainee_qualification, true);
            // if ($userData->teaching_experience != null)
            //     $userData->teaching_experience = json_decode($userData->teaching_experience, true);
            // if ($userData->cls_handed_over != null)
            //     $userData->cls_handed_over = json_decode($userData->cls_handed_over, true);
            // if ($userData->date_of_appointment != null)
            //     $userData->date_of_appointment = json_decode($userData->date_of_appointment, true);
            // if ($userData->trained_or_untrained != null)
            //     $userData->trained_or_untrained = json_decode($userData->trained_or_untrained, true);
            if ($userData->class_names != null)
                $userData->class_names = json_decode($userData->class_names, true);
            if ($userData->no_of_sections != null)
                $userData->no_of_sections = json_decode($userData->no_of_sections, true);
            if ($userData->no_of_students != null)
                $userData->no_of_students = json_decode($userData->no_of_students, true);
            if ($userData->teacher_student_ratio != null)
                $userData->teacher_student_ratio = json_decode($userData->teacher_student_ratio, true);
            if ($userData->teacher_student_assign != null)
                $userData->teacher_student_assign = json_decode($userData->teacher_student_assign, true);
            if ($userData->num_of_teacher != null)
                $userData->num_of_teacher = json_decode($userData->num_of_teacher, true);
            if ($userData->evidence_of_non_proprietary_nature_ex != null)
                $userData->evidence_of_non_proprietary_nature_ex = json_decode($userData->evidence_of_non_proprietary_nature_ex, true);
            if ($userData->last_three_year_tot_income_files_1 != null)
                $userData->last_three_year_tot_income_files_1 = json_decode($userData->last_three_year_tot_income_files_1, true);
            if ($userData->last_three_year_tot_income_files_2 != null)
                $userData->last_three_year_tot_income_files_2 = json_decode($userData->last_three_year_tot_income_files_2, true);
            if ($userData->last_three_year_tot_income_files_3 != null)
                $userData->last_three_year_tot_income_files_3 = json_decode($userData->last_three_year_tot_income_files_3, true);
            if ($userData->water_facilities != null)
                $userData->water_facilities = json_decode($userData->water_facilities, true);
            if ($userData->school_boundary_files != null)
                $userData->school_boundary_files = json_decode($userData->school_boundary_files, true);
            if ($userData->school_cctv_file != null)
                $userData->school_cctv_file = json_decode($userData->school_cctv_file, true);
            if ($userData->school_ground_file != null)
                $userData->school_ground_file = json_decode($userData->school_ground_file, true);
            if ($userData->school_water_hygene_facilities_files != null)
                $userData->school_water_hygene_facilities_files = json_decode($userData->school_water_hygene_facilities_files, true);
            if ($userData->school_fire_safty_facilities_files != null)
                $userData->school_fire_safty_facilities_files = json_decode($userData->school_fire_safty_facilities_files, true);
            if ($userData->all_teaching_material_list_files_ex != null)
                $userData->all_teaching_material_list_files_ex = json_decode($userData->all_teaching_material_list_files_ex, true);
            if ($userData->all_sports_equipment_list_file != null)
                $userData->all_sports_equipment_list_file = json_decode($userData->all_sports_equipment_list_file, true);
            if ($userData->educational_qualification_files != null)
                $userData->educational_qualification_files = json_decode($userData->educational_qualification_files, true);
            if ($userData->teacher_educational_qualification_files != null)
                $userData->teacher_educational_qualification_files = json_decode($userData->teacher_educational_qualification_files, true);
            if ($userData->cleanliness != null)
                $userData->cleanliness = json_decode($userData->cleanliness, true);
            if ($userData->books_in_library != null)
                $userData->books_in_library = json_decode($userData->books_in_library, true);
            if ($userData->school_rent_deatail != null)
                $userData->school_rent_deatail = json_decode($userData->school_rent_deatail, true);
            if ($userData->school_area_deatail != null)
                $userData->school_area_deatail = json_decode($userData->school_area_deatail, true);
            if ($userData->one_lac_fd_proof != null)
                $userData->one_lac_fd_proof = json_decode($userData->one_lac_fd_proof, true);
            $userData->district = Fnd_value::where('fnd_values.id',  $userData->district)->value('display_value');
            $userData->allclasses = $allclasses;
        }
        // return $userData;
        // $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
        // ->orderBy('fnd_values.display_value', 'ASC')
        // ->pluck(
        // 'fnd_values.display_value as division_name',
        // 'fnd_values.id as division_id'
        // );
        // $UserDistrict = User::leftjoin('addresses','addresses.user_id','users.id')
        // ->where('addresses.user_id', Auth::user()->id)
        // ->value('addresses.district');
        // $this->data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
        // ->orderBy('fnd_values.display_value', 'ASC')
        // ->pluck(
        // 'fnd_values.display_value as district_name',
        // 'fnd_values.id as district_id'
        // );
        $failed_payment = Transaction::where('application_id', $userData->id)->where('response_status', 'FAIL')->get();
        $userData->failed_payment = $failed_payment;
        // echo "<pre>"; print_r($userData->failed_payment);exit;
        // return $userData->id;
        // echo "<pre>"; print_r($userData->failed_payment);exit;
        $Message = [];
        $Application_ID = application_data::where('school_id', Auth::user()->school_id)
            ->value('id');
        $Message = Conversations::Join('users as sender', 'conversations.sender_id', '=', 'sender.id')
            ->Join('users as reciever', 'conversations.reciever_id', '=', 'reciever.id')
            ->select(
                'conversations.*',
                'reciever.user_role as reciever_role',
                'sender.user_role as sender_role',
                'reciever.name as Reciever_name',
                'sender.name as Sender_name'
            )
            // ->Where('conversations.reciever_id', Auth::user()->id)
            ->Where('conversations.parent_id', $Application_ID)
            ->where('conversations.conversation_type', 4)
            ->get();
        $Message->images = [];
        $Message->application_conversation_unread_count = Conversations::Where('conversations.parent_id', $Application_ID)->where('conversations.conversation_type', 4)->where('school_read_status', 0)->count();
        $Application_ID = application_data::where('school_id', Auth::user()->school_id)->where('status', 1)->value('id');
        $total_vrfy_field = application_data_verification::where('school_id', Auth::user()->school_id)->value('total_vrfy_field');
        $count_verified_field = application_data_verification::where('school_id', Auth::user()->school_id)->value('count_verified_field');
        $userData->total_vrfy_field = $total_vrfy_field;
        $userData->count_verified_field = $count_verified_field;
        foreach ($Message as  $Conversation) {
            $Conversation->attachments = json_decode($Conversation->attachments);
        }
        // return $Message;
        if (!empty($userData) && ($userData->application_status == 1)) {
            return view('applicaton_form', ['user' => $userData, 'allclasses' => $allclasses]);
        } elseif (empty($userData)) {
            return view('applicaton_form', ['user' => $userData, 'allclasses' => $allclasses]);
        } else {
            return view('districttest', ['user' => $userData, 'Message' => $Message]);
        }
    }
    public function AddEditApplicationData(Request $request)
    {

        // $arr = [];

        // return $request->evidence_of_non_proprietary_nature_ex_check;


        // foreach ($request->evidence_of_non_proprietary_nature_files as $file) {
        //     $fileInstance = $file;
        // return $request;
        //     $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
        //     $fileInstance->move(public_path('assets/applications'), $newFileName);
        //     $AFileArr[] = $newFileName;
        // }
        // $application_teacher_data = application_teacher_data::get();
        // echo "<pre>"; 
        // print_r($request->id);
        // print_r($request->SectionNo);
        // print_r($request->studentNo);
        // print_r($request->method_of_inspection);
        // print_r($request->teacher_name);
        // exit;
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_form = application_data::find($request->id);
            if (!empty($application_form)) {
                $application_form->updated_by  = Auth::user()->id;
                $application_form->updated_at  = date('Y-m-d H:i:s');
            } else {
                $application_form = new application_data;
                $application_form->created_by  = Auth::user()->id;
                $application_form->created_at  = date('Y-m-d H:i:s');
            }
        } else {
            $application_form = new application_data;
            $application_form->created_by  = Auth::user()->id;
            $application_form->created_at  = date('Y-m-d H:i:s');
        }
        $application_form->school_id  = Auth::user()->school_id;
        $application_form->school_name = $request->school_name;
        $application_form->application_status = 1;
        $application_form->recognised_by = $request->recognised_by;
        // $application_form->district = $request->district;
        // $application_form->district = 1;
        // $application_form->state = 'Jharkhand';
        // $application_form->division = 1;
        // $application_form->block = 1;
        // $application_form->panchayat = 1;
        $application_form->post_office = $request->post_office;
        $application_form->village_city = $request->village_town;
        $application_form->pincode = $request->pin_code;
        $application_form->phone_with_std_code = $request->phone_with_std_code;
        $application_form->fax_with_std_code = $request->fax_with_std_code;
        $application_form->email = $request->email;
        $application_form->police_station = $request->police_station;
        $application_form->estd_year = $request->estd_year;
        $application_form->opening_date = $request->opening_date;
        $application_form->society_name = $request->society_name;
        $application_form->is_society_registered = $request->is_society_registered;
        $application_form->school_validation_till = $request->school_validation_till;
        $application_form->society_registration_valid_upto = $request->society_registration_valid_upto;
        $application_form->evidence_of_non_proprietary_nature = $request->evidence_of_non_proprietary_nature;
        $application_form->school_chairman_name = $request->school_chairman_name;
        $application_form->school_chairman_post = $request->school_chairman_post;
        $application_form->school_chairman_address = $request->school_chairman_address;
        $application_form->school_chairman_phone_number = $request->school_chairman_phone_number;
        $application_form->school_chairman_office = $request->school_chairman_office;
        $application_form->school_chairman_email = $request->school_chairman_email;
        $application_form->session_year =  json_encode($request->session_year, true);
        $application_form->income =  json_encode($request->income, true);
        $application_form->expenses =  json_encode($request->expenses, true);
        $application_form->surplus_money =  json_encode($request->surplus_money, true);
        $application_form->reduced_money =  json_encode($request->reduced_money, true);
        // $application_form->teaching_fee = $request->teaching_fee;
        // $application_form->registration_fee = $request->registration_fee;
        // $application_form->enrollment_fee = $request->enrollment_fee;
        // $application_form->security_money = $request->security_money;
        // $application_form->development_fee = $request->development_fee;
        // $application_form->annual_charge = $request->annual_charge;
        // $application_form->other_charges = $request->other_charges;
        // $application_form->total = $request->total;
        $application_form->from_class_finance = $request->from_class_finance_id;
        $application_form->to_class_finance = $request->to_class_finance_id;
        $application_form->class_finance = json_encode($request->class_finance);
        $application_form->teaching_fee = json_encode($request->teaching_fee);
        $application_form->registration_fee = json_encode($request->registration_fee);
        $application_form->enrollment_fee = json_encode($request->enrollment_fee);
        $application_form->security_money = json_encode($request->security_money);
        $application_form->development_fee = json_encode($request->development_fee);
        $application_form->annual_charge = json_encode($request->annual_charge);
        $application_form->other_charges = json_encode($request->other_charges);
        $application_form->total = json_encode($request->total);
        $application_form->medium = $request->medium;
        $application_form->type_of_school = $request->type_of_school;
        $application_form->supported_agency_name = $request->supported_agency_name;
        $application_form->agency_supported_percent = $request->agency_supported_percent;
        $application_form->authority_name = $request->authority_name;
        $application_form->recognised_number = $request->recognised_number;
        $application_form->is_school_on_rented = $request->is_school_on_rented;
        $application_form->are_school_building_used = $request->are_school_building_used;
        $application_form->school_total_area = $request->school_total_area;
        $application_form->plot_number = $request->plot_number;
        $application_form->khata_number = $request->khata_number;
        $application_form->school_building_area = $request->school_building_area;
        // $application_form->pre_elementary_class = $request->pre_elementary_class;
        // $application_form->pre_no_of_section = $request->pre_no_of_section;
        // $application_form->pre_no_of_student = $request->pre_no_of_student;
        // $application_form->class_one_to_five = $request->class_one_to_five;
        // $application_form->onefive_no_of_section = $request->onefive_no_of_section;
        // $application_form->onefive_no_of_student = $request->onefive_no_of_student;
        // $application_form->class_six_to_eight = $request->class_six_to_eight;
        // $application_form->sixeight_no_of_section = $request->sixeight_no_of_section;
        // $application_form->sixeight_no_of_student = $request->sixeight_no_of_student;
        $application_form->no_of_class = $request->no_of_class;
        $application_form->avg_size_cls_room = $request->avg_size_cls_room;
        $application_form->no_of_office_room = $request->no_of_office_room;
        $application_form->avg_size_of_office_room = $request->avg_size_of_office_room;
        $application_form->no_of_store_room = $request->no_of_store_room;
        $application_form->avg_size_of_store_room = $request->avg_size_of_store_room;
        $application_form->no_of_princpal_room = $request->no_of_princpal_room;
        $application_form->avg_size_of_principal_room = $request->avg_size_of_principal_room;
        $application_form->no_of_kitchen_room = $request->no_of_kitchen_room;
        $application_form->avg_size_of_kitchen_room = $request->avg_size_of_kitchen_room;
        // $application_form->medium = $request->medium;
        $application_form->save();
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_form_ext = application_data_ext_1::where('application_data_id', $application_form->id)->where('school_id', Auth::user()->school_id)->first();
            if (empty($application_form_ext)) {
                $application_form_ext = new application_data_ext_1;
            }
        } else {
            $application_form_ext = new application_data_ext_1;
        }
        // $application_form_ext = new application_data_ext_1;
        $application_form_ext->application_data_id  = $application_form->id;
        $application_form_ext->school_id = Auth::user()->school_id;
        $application_form_ext->facilities_access_without_interrupted = $request->facilities_access_without_interrupted;
        $application_form_ext->all_teaching_material_list = $request->all_teaching_material_list;
        $application_form_ext->all_sports_equipment_list = $request->all_sports_equipment_list;
        $application_form_ext->books = $request->books;
        $application_form_ext->magazines = $request->magazines;
        $application_form_ext->type_of_water_facilities = $request->type_of_water_facilities;
        $application_form_ext->no_of_water_supply = $request->no_of_water_supply;
        $application_form_ext->type_of_toilet = $request->type_of_toilet;
        $application_form_ext->gents_toilet = $request->gents_toilet;
        $application_form_ext->ladies_toilet = $request->ladies_toilet;
        $application_form_ext->principle_name = $request->principle_name;
        $application_form_ext->p_f_h_w_name = $request->p_f_h_w_name;
        $application_form_ext->p_date_of_birth = $request->p_date_of_birth;
        $application_form_ext->p_education_qualification = $request->p_education_qualification;
        $application_form_ext->trainee_qualification = $request->trainee_qualification;
        $application_form_ext->teaching_experience = $request->teaching_experience;
        $application_form_ext->class_handed_over = $request->class_handed_over;
        $application_form_ext->date_of_appointment = $request->date_of_appointment;
        $application_form_ext->trained_untrained = $request->trained_untrained;
        $application_form_ext->details_of_curriculum = json_encode($request->details_of_curriculum);
        $application_form_ext->method_of_inspection = json_encode($request->method_of_inspection);
        $application_form_ext->school_board_exam_till_cls_eight = $request->school_board_exam_till_cls_eight;
        $application_form_ext->class_names = json_encode($request->className);
        $application_form_ext->no_of_sections = json_encode($request->SectionNo);
        $application_form_ext->no_of_students = json_encode($request->studentNo);
        $application_form_ext->teacher_student_ratio = json_encode($request->teacher_student_ratio);
        $application_form_ext->teacher_student_assign = json_encode($request->teacher_student_assign);
        $application_form_ext->num_of_teacher = json_encode($request->num_of_teacher);
        $application_form_ext->form_class_id = $request->form_class_id;
        $application_form_ext->to_class_id = $request->to_class_id;
        $application_form_ext->is_school_has_boundary = $request->is_school_has_boundary;
        $application_form_ext->is_school_cctv = $request->is_school_cctv;
        $application_form_ext->is_school_ground = $request->is_school_ground;
        $application_form_ext->is_school_water_hygene_facilities = $request->is_school_water_hygene_facilities;
        $application_form_ext->is_school_fire_safty_facilities = $request->is_school_fire_safty_facilities;
        $application_form_ext->size_of_school_boundary = $request->size_of_school_boundary;
        $application_form_ext->no_of_school_cctv = $request->no_of_school_cctv;
        $application_form_ext->size_of_school_ground = $request->size_of_school_ground;
        $application_form_ext->no_of_school_water_hygene_facilities = $request->no_of_school_water_hygene_facilities;
        $application_form_ext->no_of_fire_safty = $request->no_of_fire_safty;
        if ($request->has('evidence_of_non_proprietary_nature_files')) {
            $AFileArr = [];
            foreach ($request->evidence_of_non_proprietary_nature_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->evidence_of_non_proprietary_nature = json_encode($AFileArr);
            }
        }
        if ($request->has('educational_qualification_files')) {
            $AFileArr = [];
            foreach ($request->educational_qualification_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->educational_qualification_files = json_encode($AFileArr);
            }
        }
        if ($request->has('school_boundary_files')) {
            $AFileArr = [];
            foreach ($request->school_boundary_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_boundary_files = json_encode($AFileArr);
            }
        }
        if ($request->has('all_teaching_material_list_files')) {
            $AFileArr = [];
            foreach ($request->all_teaching_material_list_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->all_teaching_material_list_files = json_encode($AFileArr);
            }
        }
        if ($request->has('all_sports_equipment_list_file')) {
            $AFileArr = [];
            foreach ($request->all_sports_equipment_list_file as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->all_sports_equipment_list_file = json_encode($AFileArr);
            }
        }
        if ($request->has('school_cctv_file')) {
            $AFileArr = [];
            foreach ($request->school_cctv_file as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_cctv_file = json_encode($AFileArr);
            }
        }
        if ($request->has('school_ground_file')) {
            $AFileArr = [];
            foreach ($request->school_ground_file as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_ground_file = json_encode($AFileArr);
            }
        }
        if ($request->has('school_water_hygene_facilities_files')) {
            $AFileArr = [];
            foreach ($request->school_water_hygene_facilities_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $AFileArr[] = $newFileName;
            }
            if (count($AFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_water_hygene_facilities_files = json_encode($AFileArr);
            }
        }
        if ($request->has('last_three_year_tot_income_files_1')) {
            $BFileArr = [];
            foreach ($request->last_three_year_tot_income_files_1 as $file) {
                $fileInstance = $file;
                $newFileName = "application_income_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $BFileArr[] = $newFileName;
            }
            if (count($BFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->last_three_year_tot_income_files_1 = json_encode($BFileArr);
            }
        }
        if ($request->has('last_three_year_tot_income_files_2')) {
            $BFileArr = [];
            foreach ($request->last_three_year_tot_income_files_2 as $file) {
                $fileInstance = $file;
                $newFileName = "application_income_2_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $BFileArr[] = $newFileName;
            }
            if (count($BFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->last_three_year_tot_income_files_2 = json_encode($BFileArr);
            }
        }
        if ($request->has('last_three_year_tot_income_files_3')) {
            $BFileArr = [];
            foreach ($request->last_three_year_tot_income_files_3 as $file) {
                $fileInstance = $file;
                $newFileName = "application_income_3_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $BFileArr[] = $newFileName;
            }
            if (count($BFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->last_three_year_tot_income_files_3 = json_encode($BFileArr);
            }
        }
        if ($request->has('water_facilities_files')) {
            $CFileArr = [];
            foreach ($request->water_facilities_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_3_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $CFileArr[] = $newFileName;
            }
            if (count($CFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->water_facilities = json_encode($CFileArr);
            }
        }
        if ($request->has('cleanliness_files')) {
            $DFileArr = [];
            foreach ($request->cleanliness_files as $file) {
                $fileInstance = $file;
                $newFileName = "application_4_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                $fileInstance->move(public_path('assets/applications'), $newFileName);
                $DFileArr[] = $newFileName;
            }
            if (count($DFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->cleanliness = json_encode($DFileArr);
            }
        }
        if ($request->has('books_in_library_files')) {
            $BlFileArr = [];
            foreach ($request->books_in_library_files as $file) {
                $fileInstanceBL = $file;
                $newFileNameBL = "application_5_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceBL->getClientOriginalExtension();
                $fileInstanceBL->move(public_path('assets/applications'), $newFileNameBL);
                $BlFileArr[] = $newFileNameBL;
            }
            if (count($BlFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->books_in_library = json_encode($BlFileArr);
            }
        }
        if ($request->has('school_rent_deatail')) {
            $SRFileArr = [];
            foreach ($request->school_rent_deatail as $file) {
                $fileInstanceSR = $file;
                $newFileNameSR = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSR->getClientOriginalExtension();
                $fileInstanceSR->move(public_path('assets/applications'), $newFileNameSR);
                $SRFileArr[] = $newFileNameSR;
            }
            if (count($SRFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_rent_deatail = json_encode($SRFileArr);
            }
        }
        if ($request->has('school_area_deatail')) {
            $SAFileArr = [];
            foreach ($request->school_area_deatail as $file) {
                $fileInstanceSA = $file;
                $newFileNameSA = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                $fileInstanceSA->move(public_path('assets/applications'), $newFileNameSA);
                $SAFileArr[] = $newFileNameSA;
            }
            if (count($SAFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_area_deatail = json_encode($SAFileArr);
            }
        }
        if ($request->has('school_fire_safty_facilities_files')) {
            $SAFileArr = [];
            foreach ($request->school_fire_safty_facilities_files as $file) {
                $fileInstanceSA = $file;
                $newFileNameSA = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                $fileInstanceSA->move(public_path('assets/applications'), $newFileNameSA);
                $SAFileArr[] = $newFileNameSA;
            }
            if (count($SAFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->school_fire_safty_facilities_files = json_encode($SAFileArr);
            }
        }
        if ($request->has('one_lac_fd_proof')) {
            $SAFileArr = [];
            foreach ($request->one_lac_fd_proof as $file) {
                $fileInstanceSA = $file;
                $newFileNameSA = "application_6_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstanceSA->getClientOriginalExtension();
                $fileInstanceSA->move(public_path('assets/applications'), $newFileNameSA);
                $SAFileArr[] = $newFileNameSA;
            }
            if (count($SAFileArr) != 0) { // if have any file, otherwise null saved already
                $application_form_ext->one_lac_fd_proof = json_encode($SAFileArr);
            }
        }
        $application_form_ext->save();
        //  return $request->teacher_name ;
        $arr = [];
        foreach ($request->teacher_name as $key => $val) {
            array_push($arr, ['if 1' => $key]);
            if (isset($request->teacher_id[$key]) && ($request->teacher_id[$key] != '' || $request->teacher_id[$key] != null)) {
                $application_teacher_data = application_teacher_data::where('id', $request->teacher_id[$key])->first();
                if (empty($application_teacher_data)) {
                    $application_teacher_data = new application_teacher_data;
                    array_push($arr, ['if ' => $key]);
                }
            } else if ($request->teacher_id[$key] == null) {
                $application_teacher_data = new application_teacher_data;
                array_push($arr, ['else if ' => $key]);
            }
            // return ['else '=>$arr];
            // $application_teacher_data = new application_teacher_data;
            $application_teacher_data->application_data_id = $request->id;
            $application_teacher_data->school_id  = Auth::user()->school_id;
            $application_teacher_data->teacher_name = $request->teacher_name[$key];
            $application_teacher_data->teacher_f_h_w_name = $request->teacher_f_h_w_name[$key];
            $application_teacher_data->date_of_birth = $request->teacher_date_of_birth[$key];
            $application_teacher_data->education_qualification = $request->teacher_education_qualification[$key];
            $application_teacher_data->trainee_qualification = $request->teacher_trainee_qualification[$key];
            $application_teacher_data->teaching_experience = $request->teacher_teaching_experience[$key];
            $application_teacher_data->cls_handed_over = $request->teacher_class_handed_over[$key];
            $application_teacher_data->date_of_appointment = $request->teacher_appointment_date[$key];
            $application_teacher_data->trained_or_untrained = $request->teacher_trained_or_untrained[$key];
            if ($request->teacher_educational_qualification_files != null || $request->teacher_educational_qualification_files != '') {
                foreach ($request->teacher_educational_qualification_files as $key_file => $value_file) {
                    if ($key_file == $key) {
                        // array_push($arr, $request->teacher_id[$key]);
                        array_push($arr, ['file ' => $key]);
                        if ($request->has('teacher_educational_qualification_files')) {
                            $AFileArr = [];
                            foreach ($request->teacher_educational_qualification_files[$key_file] as $key1 => $value_file) {
                                $fileInstance = $value_file;
                                $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
                                $fileInstance->move(public_path('assets/applications'), $newFileName);
                                $AFileArr[] = $newFileName;
                            }
                            // array_push($arr, json_encode($AFileArr) );
                            $application_teacher_data->teacher_educational_qualification_files = json_encode($AFileArr);
                        }
                    }
                }
            }
            $application_teacher_data->save();
            // return $arr;
            // return $request->teacher_educational_qualification_files[$key] ;
            //application_data_ext_files
            if (isset($request->id) && ($request->id != '' || $request->id != null)) {
                $application_data_ext_files = application_data_ext_file::where('application_data_id', $application_form->id)->first();
                if (empty($application_data_ext_files)) {
                    $application_data_ext_files = new application_data_ext_file;
                }
            } else {
                $application_data_ext_files = new application_data_ext_file;
            }
            $application_data_ext_files->application_data_id = $application_form->id;
            $application_data_ext_files->school_id = Auth::user()->school_id;
            $application_data_ext_files->save();
        }
        //application_data_ext_files
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_data_ext_files = application_data_ext_file::where('application_data_id', $application_form->id)->first();
            if (empty($application_data_ext_files)) {
                $application_data_ext_files = new application_data_ext_file;
            }
        } else {
            $application_data_ext_files = new application_data_ext_file;
        }
        $application_data_ext_files->application_data_id = $application_form->id;
        $application_data_ext_files->school_id = Auth::user()->school_id;
        $application_data_ext_files->save();
        //application_data_files
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_data_files = application_data_file::where('application_data_id', $application_form->id)->first();
            if (empty($application_data_files)) {
                $application_data_files = new application_data_file;
                $application_data_files->created_by  = Auth::user()->id;
            } else {
                $application_data_files->updated_at  = Auth::user()->id;
            }
        } else {
            $application_data_files = new application_data_file;
            $application_data_files->created_by  = Auth::user()->id;
        }
        $application_data_files->application_data_id = $application_form->id;
        $application_data_files->school_id = Auth::user()->school_id;
        $application_data_files->save();
        //application_data_remarks
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_data_remarks = application_data_remarks::where('application_data_id', $application_form->id)->first();
            if (empty($application_data_remarks)) {
                $application_data_remarks = new application_data_remarks;
                $application_data_remarks->created_by_rmk  = Auth::user()->id;
            } else {
                $application_data_remarks->updated_by_rmk  = Auth::user()->id;
            }
        } else {
            $application_data_remarks = new application_data_remarks;
            $application_data_remarks->created_by_rmk  = Auth::user()->id;
        }
        $application_data_remarks->application_data_id = $application_form->id;
        $application_data_remarks->school_id = Auth::user()->school_id;
        $application_data_remarks->save();
        //application_data_remarks_ext_1s
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_data_remarks_ext_1s = application_data_remarks_ext_1::where('application_data_id', $application_form->id)->first();
            if (empty($application_data_remarks_ext_1s)) {
                $application_data_remarks_ext_1s = new application_data_remarks_ext_1;
            }
        } else {
            $application_data_remarks_ext_1s = new application_data_remarks_ext_1;
        }
        $application_data_remarks_ext_1s->application_data_id = $application_form->id;
        $application_data_remarks_ext_1s->school_id = Auth::user()->school_id;
        $application_data_remarks_ext_1s->save();
        //application_data_verifications
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_data_verifications = application_data_verification::where('application_data_id', $application_form->id)->first();
            if (empty($application_data_verifications)) {
                $application_data_verifications = new application_data_verification;
                $application_data_verifications->created_by_vrfy  = Auth::user()->id;
            } else {
                $application_data_verifications->updated_by_vrfy  = Auth::user()->id;
            }
        } else {
            $application_data_verifications = new application_data_verification;
            $application_data_verifications->created_by_vrfy  = Auth::user()->id;
        }
        $application_data_verifications->application_data_id = $application_form->id;
        $application_data_verifications->school_id = Auth::user()->school_id;
        $application_data_verifications->save();
        // /application_data_verification_ext_1s
        if (isset($request->id) && ($request->id != '' || $request->id != null)) {
            $application_data_verification_ext_1s = application_data_verification_ext_1::where('application_data_id', $application_form->id)->first();
            if (empty($application_data_verification_ext_1s)) {
                $application_data_verification_ext_1s = new application_data_verification_ext_1;
            }
        } else {
            $application_data_verification_ext_1s = new application_data_verification_ext_1;
        }
        $application_data_verification_ext_1s->application_data_id   = $application_form->id;
        $application_data_verification_ext_1s->school_id = Auth::user()->school_id;
        $application_data_verification_ext_1s->save();
        // if (isset($request->save_data) && $request->save_data == '1') {
        //     $application_tracking = new application_tracking;
        //     $application_tracking->application_id = $application_form->id;
        //     $application_tracking->school_id = Auth::user()->school_id;
        //     $application_tracking->application_status_id = 2;

        // $phone = Contact::where('school_id', $school_id)->value('mobile');
        // $msg = 'Application Tracking ';
        // $this->TrackingSMS($phone,$msg);

        //     $application_tracking->created_by = Auth::user()->id;
        //     $application_tracking->save();
        // application_data::where('id', $Application_ID)->update([
        //     'application_status' => 3,
        //     'updated_by' => Auth::user()->id
        //     ]);
        // }
        //if (isset($request->Payment) && $request->Payment == '3') {
        // return $request;
        //$Treasurys = Fnd_treasury_value::select(
        //'fnd_treasury_values.id',
        //'fnd_treasury_values.treasury_title'
        //)->get();
        //return view('payment_application', ['School_Data' => $request, 'Treasurys' => $Treasurys]);
        //}
        // return $request;
        if (isset($request->Payment) && $request->Payment == '3') {
            // $request->data = '98342423423';
            // return [$request->teacher_educational_qualification_files];

            $request->validate([

                'school_name' => 'required',
                'recognised_by' => 'required',
                'post_office' => 'required',
                'village_town' => 'required',
                'pin_code' => 'required',
                'phone_with_std_code' => 'required',
                'fax_with_std_code' => 'required',
                'email' => 'required',
                'police_station' => 'required',
                'estd_year' => 'required',
                'opening_date' => 'required',
                'society_name' => 'required',
                'is_society_registered' => 'required',
                'school_validation_till' => 'required',
                'society_registration_valid_upto' => 'required_if:school_validation_till,2',
                'evidence_of_non_proprietary_nature' => 'required',

                'evidence_of_non_proprietary_nature_ex_check' => 'required',
                'evidence_of_non_proprietary_nature_files[]' => 'required_if:evidence_of_non_proprietary_nature_ex_check,0',

                'last_three_year_tot_income_files_check_1' => 'required',
                'last_three_year_tot_income_files_1[]' => 'required_if:last_three_year_tot_income_files_check_1,0',

                'last_three_year_tot_income_files_check_2' => 'required',
                'last_three_year_tot_income_files_2[]' => 'required_if:last_three_year_tot_income_files_check_2,0',

                'last_three_year_tot_income_files_check_3' => 'required',
                'last_three_year_tot_income_files_3[]' => 'required_if:last_three_year_tot_income_files_check_3,0',

                'school_rent_deatail_check' => 'required',
                'school_rent_deatail[]' => 'required_if:school_rent_deatail_check,0',

                'school_area_deatail_check' => 'required',
                'school_area_deatail[]' => 'required_if:school_area_deatail_check,0',

                'one_lac_fd_proof_check' => 'required',
                'one_lac_fd_proof[]' => 'required_if:one_lac_fd_proof_check,0',

                'school_boundary_files_check' => 'required',
                'school_boundary_files[]' => 'required_if:school_boundary_files_check,0 & required_if:is_school_has_boundary,Yes',

                'school_cctv_file_check' => 'required',
                'school_cctv_file[]' => 'required_if:school_cctv_file_check,0 & required_if:is_school_cctv,Yes',

                'school_ground_file_check' => 'required',
                'school_ground_file[]' => 'required_if:school_ground_file_check,0 & required_if:is_school_ground,Yes',

                'school_water_hygene_facilities_files_check' => 'required',
                'school_water_hygene_facilities_files[]' => 'required_if:school_water_hygene_facilities_files_check,0 & required_if:is_school_water_hygene_facilities,Yes',

                'school_fire_safty_facilities_files_check' => 'required',
                'school_fire_safty_facilities_files[]' => 'required_if:school_fire_safty_facilities_files_check,0 & required_if:is_school_fire_safty_facilities,Yes',

                'all_teaching_material_list_files_check' => 'required',
                'all_teaching_material_list_files[]' => 'required_if:all_teaching_material_list_files_check,0',

                'all_sports_equipment_list_file_check' => 'required',
                'all_sports_equipment_list_file[]' => 'required_if:all_sports_equipment_list_file_check,0',

                'books_in_library_files_check' => 'required',
                'books_in_library_files[]' => 'required_if:books_in_library_files_check,0',

                'water_facilities_files_check' => 'required',
                'water_facilities_files[]' => 'required_if:water_facilities_files_check,0',

                'cleanliness_files_check' => 'required',
                'cleanliness_files[]' => 'required_if:cleanliness_files_check,0',

                'educational_qualification_files_check' => 'required',
                'educational_qualification_files[]' => 'required_if:educational_qualification_files_check,0',

                'teacher_educational_qualification_files_check' => 'required',
                'teacher_educational_qualification_files[][]' => 'required_if:teacher_educational_qualification_files_check,0',


                'income.*'  => "required",
                'expenses.*' => 'required',
                'surplus_money.*' => 'required',
                'reduced_money.*' => 'required',
                'medium' => 'required',
                'type_of_school' => 'required',
                'supported_agency_name' => 'required',
                'agency_supported_percent' => 'required',
                'authority_name' => 'required',
                'recognised_number' => 'required',
                'is_school_on_rented' => 'required',
                'are_school_building_used' => 'required',
                'school_total_area' => 'required',
                'plot_number' => 'required',
                'khata_number' => 'required',
                'school_building_area' => 'required',
                'from_class_finance_id' => 'required',
                'to_class_finance_id' => 'required',
                'class_finance' => 'required',
                'teaching_fee.*' => 'required',
                'registration_fee.*' => 'required',
                'enrollment_fee.*' => 'required',
                'security_money.*' => 'required',
                'development_fee.*' => 'required',
                'annual_charge.*' => 'required',
                'other_charges.*' => 'required',
                'total.*' => 'required',
                'form_class_id' => 'required',
                'to_class_id' => 'required',
                'className.*' => 'required',
                'SectionNo.*' => 'required',
                'studentNo.*' => 'required',
                'teacher_student_ratio.*' => 'required',
                'teacher_student_assign.*' => 'required',
                'num_of_teacher.*' => 'required',
                'no_of_class' => 'required',
                'avg_size_cls_room' => 'required',
                'no_of_office_room' => 'required',
                'avg_size_of_office_room' => 'required',
                'no_of_store_room' => 'required',
                'avg_size_of_store_room' => 'required',
                'no_of_princpal_room' => 'required',
                'avg_size_of_principal_room' => 'required',
                'no_of_kitchen_room' => 'required',
                'avg_size_of_kitchen_room' => 'required',
                'is_school_has_boundary' => 'required',
                'size_of_school_boundary' => 'required_if:is_school_has_boundary,Yes',
                'is_school_cctv' => 'required',
                'no_of_school_cctv' => 'required_if:is_school_cctv,Yes',
                'is_school_ground' => 'required',
                'size_of_school_ground' => 'required_if:is_school_ground,Yes',
                'is_school_water_hygene_facilities' => 'required',
                'no_of_school_water_hygene_facilities' => ' required_if:is_school_water_hygene_facilities,Yes',
                'is_school_fire_safty_facilities' => 'required',
                'no_of_fire_safty' => ' required_if:is_school_fire_safty_facilities,Yes',
                'facilities_access_without_interrupted' => 'required',
                'all_teaching_material_list' => 'required',
                'all_sports_equipment_list' => 'required',
                'books' => 'required',
                'magazines' => 'required',
                'type_of_water_facilities' => 'required',
                'no_of_water_supply' => 'required',
                'type_of_toilet' => 'required',
                'gents_toilet' => 'required',
                'ladies_toilet' => 'required',
                'principle_name' => 'required',
                'p_f_h_w_name' => 'required',
                'p_date_of_birth' => 'required',
                'p_education_qualification' => 'required',
                'trainee_qualification' => 'required',
                'teaching_experience' => 'required',
                'class_handed_over' => 'required',
                'date_of_appointment' => 'required',
                'trained_untrained' => 'required',
                'teacher_name.*' => 'required',
                'teacher_f_h_w_name.*' => 'required',
                'teacher_date_of_birth.*' => 'required',
                'teacher_education_qualification.*' => 'required',
                'teacher_trainee_qualification.*' => 'required',
                'teacher_teaching_experience.*' => 'required',
                'teacher_class_handed_over.*' => 'required',
                'teacher_appointment_date.*' => 'required',
                'teacher_trained_or_untrained.*' => 'required',
                'details_of_curriculum.*' => 'required',
                'method_of_inspection.*' => 'required',
                'school_board_exam_till_cls_eight' => 'required',
                'school_chairman_name' => 'required',
                'school_chairman_post' => 'required',
                'school_chairman_address' => 'required',
                'school_chairman_phone_number' => 'required',
                'school_chairman_office' => 'required',
                'school_chairman_email' => 'required',

            ], [
                'school_name.required' => 'School name is required ',
                'recognised_by.required' => 'Academic session from which recognition proposed is required ',
                'post_office.required' => 'Post office is required ',
                'village_town.required' => 'Village / town is required ',
                'pin_code.required' => 'Pin code is required ',
                'phone_with_std_code.required' => 'Ph./Mb number with STD code is required ',
                'fax_with_std_code.required' => 'FAX number with STD code is required ',
                'email.required' => 'E-mail is required ',
                'police_station.required' => 'Nearest police station is required ',
                'estd_year.required' => 'Establishment year is required ',
                'opening_date.required' => 'School opening date is required ',
                'society_name.required' => 'Trust / society / management committee name is required ',
                'is_society_registered.required' => 'Is trust / society / management comittee registered? is required ',
                'school_validation_till.required' => 'The period until the registration of the trust / society / management committee is valid is required ',
                'society_registration_valid_upto.required' => 'society_registration_valid_upto is required ',
                'evidence_of_non_proprietary_nature.required' => 'Is there any evidence of Non-proprietary nature of the trust / society / management committee, supported by the list of members including their addresses on affidavit ? is required ',

                'school_chairman_email.required' => 'School chairman e-mail is required .',

                'principle_name.required' => ' Principle name is required . ',
                'p_f_h_w_name.required' => ' Principle father / husband or wife name  is required . ',
                'p_date_of_birth.required' => ' Principle date of birth is required . ',
                'p_education_qualification.required' => ' Principle education qualification is required . ',
                'trainee_qualification.required' => 'Principle trainee qualification is required . ',
                'teaching_experience.required' => 'Principle teaching experience is required . ',
                'class_handed_over.required' => 'Principle class handed_over is required . ',
                'date_of_appointment.required' => 'Principle date of appointment is required . ',
                'trained_untrained.required' => 'Principle trained untrained is required . ',

                'no_of_class.required' => ' Number of class is required .',
                'avg_size_cls_room.required' => ' Average Size Of Classroom (In W*H) is required .',
                'no_of_office_room.required' => ' No. Of Office Rooms is required .',
                'avg_size_of_office_room.required' => ' Average Size Of Office Rooms (In W*H) is required .',
                'no_of_store_room.required' => ' No. Of Store Rooms is required .',
                'avg_size_of_store_room.required' => ' Average Size Of Store Rooms (In W*H) is required .',
                'no_of_princpal_room.required' => ' No. Of Principal Rooms is required .',
                'avg_size_of_principal_room.required' => ' Average Size Of Principal Rooms (In W*H) is required .',
                'no_of_kitchen_room.required' => ' No. Of Kitchen Rooms is required .',
                'avg_size_of_kitchen_room.required' => ' Average Size Of Kitchen Rooms (In W*H) is required .',
                'is_school_has_boundary.required' => ' Does The School Has Boundary is required .',
                'size_of_school_boundary.required' => ' Size Of Boundry( W * H ) is required .',
                'is_school_cctv.required' => ' Does School Has CCTV ? is required .',
                'no_of_school_cctv.required' => ' No. Of CCTV is required .',
                'is_school_ground.required' => ' Sport Ground ? is required .',
                'size_of_school_ground.required' => ' Size Of Sport Ground ( W * H ) is required .',
                'is_school_water_hygene_facilities.required' => ' Dose The School Has 3 Start Rating In Water And Hygene Facilities ? is required .',
                'no_of_school_water_hygene_facilities.required' => ' No. Of Water And Hygene Facilitie is required .',
                'is_school_fire_safty_facilities.required' => ' Dose The School Has Fire Safty Facilities ? is required .',
                'no_of_fire_safty.required' => ' No. Of Fire Safty Facilities is required .',
                'facilities_access_without_interrupted.required' => ' Does All Facilities Have Access Without Interruption? is required .',
                'all_teaching_material_list.required' => ' List All Teaching Materials is required .',
                'all_sports_equipment_list.required' => ' List All Sports Equipments is required .',
                'books.required' => ' Number Of Books In Library is required .',
                'magazines.required' => ' Number Of Newspaper & Magazine is required .',
                'type_of_water_facilities.required' => ' Types Of Water Facilities is required .',
                'no_of_water_supply.required' => ' Number Of Water Supply is required .',
                'type_of_toilet.required' => ' Type Of Toilets is required .',
                'gents_toilet.required' => ' Number Of Separate Toilet For Boys is required .',
                'ladies_toilet.required' => ' Number Of Separate Toilet For Girls is required .',

                'medium.required' => 'Medium is required . ',
                'type_of_school.required' => 'Type of school is required . ',
                'supported_agency_name.required' => 'Supported agency name is required . ',
                'agency_supported_percent.required' => 'Agency supported percent is required . ',
                'authority_name.required' => 'Authority name is required . ',
                'recognised_number.required' => 'Recognised number is required . ',
                'is_school_on_rented.required' => 'The School Has Its Own Building Or Is Working In A Rented Building ? is required . ',
                'are_school_building_used.required' => 'Are School Buildings Or Other Structures Or Sports Sites Being Used Only For The Purpose Of Education And Skill Development? is required . ',
                'school_total_area.required' => 'Total Area Of School ( Rakba In Acers) is required . ',
                'plot_number.required' => 'Plot Number is required . ',
                'khata_number.required' => 'Khata Number is required . ',
                'school_building_area.required' => 'Area Of School Building Only is required . ',
                'from_class_finance_id.required' => ' Class From is required . ',
                'to_class_finance_id.required' => 'Class To is required . ',


                'income.*.required'  => ' Income is required . ',
                'expenses.*.required' => ' Expense is required . ',
                'surplus_money.*.required' => ' Surplus money is required . ',
                'reduced_money.*.required' => ' Reduced money is required . ',
                'teaching_fee.*.required' => ' Teaching fee is required . ',
                'registration_fee.*.required' => ' Registration fee is required . ',
                'enrollment_fee.*.required' => ' Enrollment fee is required . ',
                'security_money.*.required' => ' Security money is required . ',
                'development_fee.*.required' => ' Development fee is required . ',
                'annual_charge.*.required' => ' Annual charge is required . ',
                'other_charges.*.required' => ' Other charges is required . ',
                'total.*.required' => ' Total is required . ',
                'className.*.required' => ' Class name is required . ',
                'SectionNo.*.required' => 'Number of section is required . ',
                'studentNo.*.required' => 'Number of student is required . ',
                'teacher_student_ratio.*.required' => ' Teacher student ratio is required . ',
                'teacher_student_assign.*.required' => ' Teacher student assign is required . ',
                'num_of_teacher.*.required' => ' Number of teacher is required . ',
                'teacher_name.*.required' => ' Teacher name is required . ',
                'teacher_f_h_w_name.*.required' => ' Teacher father / husband or wife name is required . ',
                'teacher_date_of_birth.*.required' => ' Teacher date of birth is required . ',
                'teacher_education_qualification.*.required' => ' Teacher education qualification is required . ',
                'teacher_trainee_qualification.*.required' => ' Teacher trainee qualification is required . ',
                'teacher_teaching_experience.*.required' => ' Teacher teaching experience is required . ',
                'teacher_class_handed_over.*.required' => ' Teacher class handed over is required . ',
                'teacher_appointment_date.*.required' => ' Teacher appointment date is required . ',
                'teacher_trained_or_untrained.*.required' => ' Teacher trained or untrained is required . ',
                'details_of_curriculum.*.required' => ' Details of curriculum is required . ',
                'method_of_inspection.*.required' => ' Method of inspection is required . ',
                'evidence_of_non_proprietary_nature_files[].required_if' => 'Add Relevant Evidence Attachementis required . ',
                'last_three_year_tot_income_files_1[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'last_three_year_tot_income_files_2[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'last_three_year_tot_income_files_3[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_rent_deatail[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_area_deatail[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'one_lac_fd_proof[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_boundary_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_cctv_file[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_ground_file[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_water_hygene_facilities_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'school_fire_safty_facilities_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'all_teaching_material_list_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'all_sports_equipment_list_file[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'books_in_library_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'water_facilities_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'cleanliness_files[].required_if' => ' Add Relevant Evidence Attachement is required .  ',
                'educational_qualification_files[].required_if' => ' Educational Qualification file is required .  ',
                'teacher_educational_qualification_files[][].required_if' => 'Teacher educational qualification files is required . ',
            ]);




            // return ['data'];
            $Transaction = Transaction::where('application_id', $request->id)->where('response_status', 'PENDING')->get();
            $Count = Transaction::where('application_id', $request->id)->where('response_status', 'PENDING')->count();
            return view('school_application_preview', ['user' => $request, 'Transaction' => $Transaction, 'count' => $Count]);
        }
        return redirect('application')->with('message', 'Data Has Been Inserted Successfully');
    }
    public function applicationFieldsVarified(Request $request)
    {
        // return $request;
        $user = Auth::user();
        if (isset($request->verified_applicationId) && ($request->verified_applicationId != '' || $request->verified_applicationId != null)) {
            $application_form = application_data::find($request->verified_applicationId);
            if (!empty($application_form)) {
                $application_form->updated_by  = Auth::user()->id;
                $application_form->updated_at  = date('Y-m-d H:i:s');
            } else {
                return redirect('application-verify')->with('message', 'Application not found');
            }
        } else {
            return redirect('application-verify')->with('message', 'Application not found');
        }
        if ($request->status == 1) {
            $decode_image = '';
            if ($request->file_reason != null) {
                foreach ($request->file_reason as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('assets/application_accept'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }
            $application_form->application_status = 4;
            $Conversations = new Conversations;
            $Conversations->conversation_type = '5';
            $Conversations->parent_id = $request->verified_applicationId;
            $Conversations->sender_id = Auth::user()->id;
            $Conversations->reciever_id = $request->verified_schoolId;
            $Conversations->title = 'Title';
            $Conversations->remarks = $request->remarks;
            $Conversations->attachments = $decode_image;
            $Conversations->save();
        } elseif ($request->status == 0) {
            $decode_image = '';
            if ($request->file_reason != null) {
                foreach ($request->file_reason as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('assets/application_reject'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }
            $application_form->application_status = 8;
            $Conversations = new Conversations;
            $Conversations->conversation_type = '6';
            $Conversations->parent_id = $request->verified_applicationId;
            $Conversations->sender_id = Auth::user()->id;
            $Conversations->reciever_id = $request->verified_schoolId;
            $Conversations->title = 'Title';
            $Conversations->remarks = $request->remarks;
            $Conversations->attachments = $decode_image;
            $Conversations->save();
        }
        $application_form->save();
        $application_tracking = new application_tracking;
        $application_tracking->application_id = $request->verified_applicationId;
        $application_tracking->school_id = $request->verified_schoolId;
        if ($request->status == 1) {
            $application_tracking->application_status_id  = 4;

            $phone = Contact::where('school_id', $request->verified_schoolId)->value('mobile');
            $msg = 'Your application has been verified by District Superintendent of Education and is currently undergoing the next process.';
            $this->TrackingSMS($phone, $msg);
        } elseif ($request->status == 0) {
            $application_tracking->application_status_id  = 8;

            $phone = Contact::where('school_id', $request->verified_schoolId)->value('mobile');
            $msg = 'Your application was rejected by District Superintendent of Education due to some recognised issues. ';
            $this->TrackingSMS($phone, $msg);
        }
        $application_tracking->created_by = $user->id;
        $application_tracking->save();
        return redirect('application-verify')->with('message', 'Fields Has Been Verified Successfully');
    }
    //new functions nisha
    public function getApplicationfeildData($status)
    {
        $userData = array();
        $userData = application_data::leftjoin('application_data_ext_1', 'application_data.id', '=', 'application_data_ext_1.application_data_id')
            // ->leftjoin('application_teacher_data', 'application_data.id', '=', 'application_teacher_data.application_data_id')
            ->leftjoin('application_data_ext_files', 'application_data.id', '=', 'application_data_ext_files.application_data_id')
            ->leftjoin('application_data_files', 'application_data.id', '=', 'application_data_files.application_data_id')
            ->leftjoin('application_data_remarks', 'application_data.id', '=', 'application_data_remarks.application_data_id')
            ->leftjoin('application_data_remarks_ext_1s', 'application_data.id', '=', 'application_data_remarks_ext_1s.application_data_id')
            ->leftjoin('application_data_verifications', 'application_data.id', '=', 'application_data_verifications.application_data_id')
            ->leftjoin('application_data_verification_ext_1s', 'application_data.id', '=', 'application_data_verification_ext_1s.application_data_id')
            ->where('application_data.school_id', Auth::user()->school_id)->where('application_data.status', 1)
            ->select(
                'application_data.id as id',
                'application_data.*',
                'application_data_ext_1.id as application_data_ext_1_id',
                'application_data_ext_1.trainee_qualification as pri_trainee_qualification',
                'application_data_ext_1.teaching_experience as pri_teaching_experience',
                'application_data_ext_1.date_of_appointment as pri_date_of_appointment',
                'application_data_ext_1.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature_ex',
                'application_data_ext_1.all_teaching_material_list_files as all_teaching_material_list_files_ex',
                'application_data.evidence_of_non_proprietary_nature as evidence_of_non_proprietary_nature1',
                'application_data_ext_1.*',
                // 'application_teacher_data.id as application_teacher_data_id',
                // 'application_teacher_data.*',
                'application_data_ext_files.id as application_data_ext_files_id',
                'application_data_ext_files.*',
                'application_data_files.id as application_data_files_id',
                'application_data_files.*',
                'application_data_remarks.id as application_data_remarks_id',
                'application_data_remarks.*',
                'application_data_remarks_ext_1s.id as application_data_remarks_ext_1s_id',
                'application_data_remarks_ext_1s.*',
                'application_data_verifications.id as application_data_verifications_id',
                'application_data_verifications.*',
                'application_data_verification_ext_1s.id as application_data_verification_ext_1s_id',
                'application_data_verification_ext_1s.*'
            )
            ->first();
        // echo "<pre>"; print_r($userData);exit;
        $application_teacher_data = application_teacher_data::where('application_teacher_data.application_data_id', Auth::user()->school_id)->get();
        $userData->application_teacher_data = $application_teacher_data;
        $allclasses = DB::table('fnd_classes')->where('status', 1)->get();
        if (!empty($userData)) {
            if ($userData->session_year != null)
                $userData->session_year = json_decode($userData->session_year, true);
            if ($userData->income != null)
                $userData->income = json_decode($userData->income, true);
            if ($userData->expenses != null)
                $userData->expenses = json_decode($userData->expenses, true);
            if ($userData->surplus_money != null)
                $userData->surplus_money = json_decode($userData->surplus_money, true);
            if ($userData->reduced_money != null)
                $userData->reduced_money = json_decode($userData->reduced_money, true);
            if ($userData->class_finance != null)
                $userData->class_finance = json_decode($userData->class_finance, true);
            if ($userData->teaching_fee != null)
                $userData->teaching_fee = json_decode($userData->teaching_fee, true);
            if ($userData->registration_fee != null)
                $userData->registration_fee = json_decode($userData->registration_fee, true);
            if ($userData->enrollment_fee != null)
                $userData->enrollment_fee = json_decode($userData->enrollment_fee, true);
            if ($userData->security_money != null)
                $userData->security_money = json_decode($userData->security_money, true);
            if ($userData->development_fee != null)
                $userData->development_fee = json_decode($userData->development_fee, true);
            if ($userData->annual_charge != null)
                $userData->annual_charge = json_decode($userData->annual_charge, true);
            if ($userData->other_charges != null)
                $userData->other_charges = json_decode($userData->other_charges, true);
            if ($userData->total != null)
                $userData->total = json_decode($userData->total, true);
            if ($userData->details_of_curriculum != null)
                $userData->details_of_curriculum = json_decode($userData->details_of_curriculum, true);
            if ($userData->method_of_inspection != null)
                $userData->method_of_inspection = json_decode($userData->method_of_inspection, true);
            if ($userData->teacher_name != null)
                $userData->teacher_name = json_decode($userData->teacher_name, true);
            if ($userData->teacher_f_h_w_name != null)
                $userData->teacher_f_h_w_name = json_decode($userData->teacher_f_h_w_name, true);
            if ($userData->date_of_birth != null)
                $userData->date_of_birth = json_decode($userData->date_of_birth, true);
            if ($userData->education_qualification != null)
                $userData->education_qualification = json_decode($userData->education_qualification, true);
            if ($userData->teacher_student_assign != null)
                $userData->teacher_student_assign = json_decode($userData->teacher_student_assign, true);
            if ($userData->num_of_teacher != null)
                $userData->num_of_teacher = json_decode($userData->num_of_teacher, true);
            if ($userData->trainee_qualification != null)
                $userData->trainee_qualification = json_decode($userData->trainee_qualification, true);
            if ($userData->teaching_experience != null)
                $userData->teaching_experience = json_decode($userData->teaching_experience, true);
            if ($userData->cls_handed_over != null)
                $userData->cls_handed_over = json_decode($userData->cls_handed_over, true);
            if ($userData->date_of_appointment != null)
                $userData->date_of_appointment = json_decode($userData->date_of_appointment, true);
            if ($userData->trained_or_untrained != null)
                $userData->trained_or_untrained = json_decode($userData->trained_or_untrained, true);
            if ($userData->class_names != null)
                $userData->class_names = json_decode($userData->class_names, true);
            if ($userData->no_of_sections != null)
                $userData->no_of_sections = json_decode($userData->no_of_sections, true);
            if ($userData->no_of_students != null)
                $userData->no_of_students = json_decode($userData->no_of_students, true);
            if ($userData->teacher_student_ratio != null)
                $userData->teacher_student_ratio = json_decode($userData->teacher_student_ratio, true);
            if ($userData->evidence_of_non_proprietary_nature_ex != null)
                $userData->evidence_of_non_proprietary_nature_ex = json_decode($userData->evidence_of_non_proprietary_nature_ex, true);
            if ($userData->last_three_year_tot_income_files_1 != null)
                $userData->last_three_year_tot_income_files_1 = json_decode($userData->last_three_year_tot_income_files_1, true);
            if ($userData->last_three_year_tot_income_files_2 != null)
                $userData->last_three_year_tot_income_files_2 = json_decode($userData->last_three_year_tot_income_files_2, true);
            if ($userData->last_three_year_tot_income_files_3 != null)
                $userData->last_three_year_tot_income_files_3 = json_decode($userData->last_three_year_tot_income_files_3, true);
            if ($userData->water_facilities != null)
                $userData->water_facilities = json_decode($userData->water_facilities, true);
            if ($userData->school_boundary_files != null)
                $userData->school_boundary_files = json_decode($userData->school_boundary_files, true);
            if ($userData->school_cctv_file != null)
                $userData->school_cctv_file = json_decode($userData->school_cctv_file, true);
            if ($userData->school_ground_file != null)
                $userData->school_ground_file = json_decode($userData->school_ground_file, true);
            if ($userData->school_water_hygene_facilities_files != null)
                $userData->school_water_hygene_facilities_files = json_decode($userData->school_water_hygene_facilities_files, true);
            if ($userData->school_fire_safty_facilities_files != null)
                $userData->school_fire_safty_facilities_files = json_decode($userData->school_fire_safty_facilities_files, true);
            if ($userData->all_teaching_material_list_files_ex != null)
                $userData->all_teaching_material_list_files_ex = json_decode($userData->all_teaching_material_list_files_ex, true);
            if ($userData->all_sports_equipment_list_file != null)
                $userData->all_sports_equipment_list_file = json_decode($userData->all_sports_equipment_list_file, true);
            if ($userData->educational_qualification_files != null)
                $userData->educational_qualification_files = json_decode($userData->educational_qualification_files, true);
            if ($userData->teacher_educational_qualification_files != null)
                $userData->teacher_educational_qualification_files = json_decode($userData->teacher_educational_qualification_files, true);
            if ($userData->cleanliness != null)
                $userData->cleanliness = json_decode($userData->cleanliness, true);
            if ($userData->books_in_library != null)
                $userData->books_in_library = json_decode($userData->books_in_library, true);
            if ($userData->school_rent_deatail != null)
                $userData->school_rent_deatail = json_decode($userData->school_rent_deatail, true);
            if ($userData->school_area_deatail != null)
                $userData->school_area_deatail = json_decode($userData->school_area_deatail, true);
            if ($userData->one_lac_fd_proof != null)
                $userData->one_lac_fd_proof = json_decode($userData->one_lac_fd_proof, true);
            $userData->district = Fnd_value::where('fnd_values.id',  $userData->district)->value('display_value');
            $userData->allclasses = $allclasses;
        }
        // $this->data['division_list'] = Fnd_value::where('fnd_values.value_set_id', 1)
        // ->orderBy('fnd_values.display_value', 'ASC')
        // ->pluck(
        // 'fnd_values.display_value as division_name',
        // 'fnd_values.id as division_id'
        // );
        // $UserDistrict = User::leftjoin('addresses','addresses.user_id','users.id')
        // ->where('addresses.user_id', Auth::user()->id)
        // ->value('addresses.district');
        // $this->data['district_list'] = Fnd_value::where('fnd_values.parent_value_id', $UserDistrict)
        // ->orderBy('fnd_values.display_value', 'ASC')
        // ->pluck(
        // 'fnd_values.display_value as district_name',
        // 'fnd_values.id as district_id'
        // );
        // echo "<pre>";
        // print_r($userData);
        // exit;
        if ($status == "accepted") {
            // return $userData;
            return view('acceptedfeildschool', ['user' => $userData]);
        }
        if ($status == "Pending") {
            return view('PendingApplicationField', ['user' => $userData]);
        }
        if ($status == "Rejected") {
            return view('RejectedApplicationField', ['user' => $userData]);
        }
        if (!empty($userData) && $userData->application_status == 1) {
            return view('applicaton_form', ['user' => $userData, 'allclasses' => $allclasses]);
        } elseif (empty($userData)) {
            return view('applicaton_form', ['user' => $userData, 'allclasses' => $allclasses]);
        } else {
            return view('districttest', ['user' => $userData]);
        }
    }
    public function downloadStudentPDF($id)
    {
        $students_data = rte_applicant_students::where('id', $id)->first();
        if (!empty($students_data->referred_school_name) && $students_data->referred_school_name != null)
            $students_data->referred_school_name = json_decode($students_data->referred_school_name, true);
        if (!empty($students_data->referred_school_address) && $students_data->referred_school_address != null)
            $students_data->referred_school_address = json_decode($students_data->referred_school_address, true);
        if (!empty($students_data->distance) && $students_data->distance != null)
            $students_data->distance = json_decode($students_data->distance, true);
        $data = ['students_data' => $students_data];
        // return $data;
        //$pdf = PDF::loadView('pdf_view.rejected_application_PDF', $data);
        // $pdf = PDF::loadView('pdf_view.student_data_PDF', $students_data);
        $pdf = PDF::loadView('pdf_view.student_data_PDF', $data)->setPaper('a4', 'landscape');
        return $pdf->download('Student-Data.pdf');
    }
    public function reApplyApplication(Request $req)
    {
        $decode_image = '';
        if ($req->meeting_document != null) {
            foreach ($req->meeting_document as $img) {
                if (isset($img) && !empty($img)) {
                    $fileName = time() . '.' . $img->getClientOriginalName();
                    $img->move(public_path('assets/application_re-apply'), $fileName);
                    $image_name[] = $fileName;
                }
            }
            $decode_image = json_encode($image_name, true);
        }
        $application_data = application_data::find($req->application_id);
        $newRow = $application_data->replicate()->setTable('application_data');
        $newRow->save();
        application_data::where('application_data.id', $req->application_id)->update(['status' => '0']);
        $application_status = application_data::where('id', $req->application_id)->value('application_status');
        $application_status = application_data::where('id', $req->application_id)->value('application_status');
        $Re_apply_application = new Re_apply_application;
        $Re_apply_application->school_id = $req->school_id;
        $Re_apply_application->prev_app_id = $req->application_id;
        $Re_apply_application->prev_app_status = $application_status;
        $Re_apply_application->re_apply_reason = $req->re_apply_reason;
        $Re_apply_application->new_app_id = $newRow->id;
        $Re_apply_application->re_apply_document = $decode_image;
        $Re_apply_application->created_by = Auth::user()->id;
        $Re_apply_application->save();
        $application_data_ext_1_id = application_data_ext_1::where('school_id', $req->school_id)->where('application_data_id', $req->application_id)->value('id');
        $application_data_ext_1 = application_data_ext_1::find($application_data_ext_1_id);
        $newapplication_data_ext_1 = $application_data_ext_1->replicate()->setTable('application_data_ext_1');
        $newapplication_data_ext_1->application_data_id = $newRow->id;
        $newapplication_data_ext_1->save();
        $application_data_ext_file = new application_data_ext_file;
        $application_data_ext_file->created_by =  Auth::user()->id;
        $application_data_ext_file->application_data_id =  $req->application_id;
        $application_data_ext_file->application_data_id = $newRow->id;
        $application_data_ext_file->school_id =  $req->school_id;
        $application_data_ext_file->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_data_ext_file->save();
        $application_data_file = new application_data_file;
        $application_data_file->created_by =  Auth::user()->id;
        $application_data_file->application_data_id =  $req->application_id;
        $application_data_file->application_data_id = $newRow->id;
        $application_data_file->school_id =  $req->school_id;
        $application_data_file->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_data_file->save();
        $application_data_remarks = new application_data_remarks;
        $application_data_remarks->created_by =  Auth::user()->id;
        $application_data_remarks->application_data_id =  $req->application_id;
        $application_data_remarks->application_data_id = $newRow->id;
        $application_data_remarks->school_id =  $req->school_id;
        $application_data_remarks->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_data_remarks->save();
        $application_data_remarks_ext_1 = new application_data_remarks_ext_1;
        $application_data_remarks_ext_1->created_by =  Auth::user()->id;
        $application_data_remarks_ext_1->application_data_id =  $req->application_id;
        $application_data_remarks_ext_1->application_data_id = $newRow->id;
        $application_data_remarks_ext_1->school_id =  $req->school_id;
        $application_data_remarks_ext_1->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_data_remarks_ext_1->save();
        $application_data_verification = new application_data_verification;
        $application_data_verification->created_by =  Auth::user()->id;
        $application_data_verification->application_data_id =  $req->application_id;
        $application_data_verification->application_data_id = $newRow->id;
        $application_data_verification->school_id =  $req->school_id;
        $application_data_verification->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_data_verification->save();
        $application_data_verification_ext_1 = new application_data_verification_ext_1;
        $application_data_verification_ext_1->created_by =  Auth::user()->id;
        $application_data_verification_ext_1->application_data_id =  $req->application_id;
        $application_data_verification_ext_1->application_data_id = $newRow->id;
        $application_data_verification_ext_1->school_id =  $req->school_id;
        $application_data_verification_ext_1->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_data_verification_ext_1->save();
        $application_teacher_data = new application_teacher_data;
        $application_teacher_data->created_by =  Auth::user()->id;
        $application_teacher_data->application_data_id =  $req->application_id;
        $application_teacher_data->application_data_id = $newRow->id;
        $application_teacher_data->school_id =  $req->school_id;
        $application_teacher_data->created_ip =  $_SERVER['REMOTE_ADDR'];
        $application_teacher_data->save();
        return redirect()->back();
    }
    public function applicationSubmitResponse(Request $request)
    {
        return ["dfsvgbfs"];
        ini_set('memory_limit', '-1');
        $ciphering = "AES-128-CBC";
        $options = 0;
        $decryption_iv = 'key1234';
        $decryption_key = "key1234";
        $decryption = @openssl_decrypt($request->responseparam, $ciphering, $decryption_key, $options, $decryption_iv);
        $decryption = explode('|', $decryption);
        // return [$decryption];
        // echo "<pre>";
        // print_r($decryption);
        // print_r($request);
        // exit;
        // echo $request;
        if (isset($decryption[17]) && $decryption[17] != '') {
            $decryption[17] = date('Y-m-d H:i:s', strtotime($decryption[17]));
        } else {
            $decryption[17] = date('Y-m-d H:i:s');
        }
        Transaction::where('dept_tran_id', $decryption[3])
            ->update([
                '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ' => $decryption[12],
                'payment_status_message' => $decryption[13],
                'grn' => $decryption[14],
                'cin' => $decryption[15],
                'ref_no' => $decryption[16],
                'txn_date' => $decryption[17],
                'txn_amount' => $decryption[18],
                'challan_url' => $decryption[19],
                'pmode' => $decryption[20],
                'add_info_4' => $decryption[21],
                'add_info_5' => $decryption[22],
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        $status = $decryption[12];
        $applicationId = $decryption[5];
        $school_id = $decryption[7];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        }
        //  whether ip is from proxy 
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        //  whether ip is from remote address 
        else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        if (isset($status) && $status == 'SUCCESS') {
            $payments = new Payment();
            $payments->t_id = $decryption[3];
            $payments->payment_title = $decryption[2];
            $payments->payment_description = $decryption[9];
            $payments->payment_user = $applicationId;
            $payments->payment_rows = null;
            $payments->payment_amount = $decryption[18];
            $payments->payment_status = $decryption[12];
            $payments->payment_date = $decryption[17];
            $payments->payment_success_details = $decryption[13];
            $payments->paid_method = $decryption[20];
            $payments->school_id =  $school_id;
            $payments->status = 1;
            $payments->created_by = -1;
            $payments->created_ip = $ip_address;
            $payments->created_at = date('Y-m-d H:i:s');
            $payments->save();
            application_data::where('id', $applicationId)->where('status', 1)->update(['application_status' => 2]);
            $application_form = application_data::where('id', $applicationId)->value('district');
            $display_short_value = Fnd_value::where('id',  $application_form)->value('display_short_value');
            $district = application_data::where('application_status', '>=', 2)->where('district', $application_form)->count();
            $district++;
            $district = strlen($district) == 1 ? '000' . $district : (strlen($district) == 2 ? '00' . $district : (strlen($district) == 3 ? '0' . $district : $district));
            $application_id_for_certificate = '20' . $display_short_value . $district;
            application_data::where('id', $applicationId)->where('status', 1)->update(['apllication_id' => $application_id_for_certificate, 'updated_at' => date('Y-m-d H:i:s')]);
            $application_tracking = new application_tracking;
            $application_tracking->application_id = $applicationId;
            $application_tracking->school_id = $school_id;
            $application_tracking->application_status_id = 2;


            $phone = Contact::where('school_id', $school_id)->value('mobile');
            $msg = 'Your application has been submitted successfully under Private School Recognition System.';
            $this->TrackingSMS($phone, $msg);

            $application_tracking->created_by = -1;
            $application_tracking->save();
            return view('paymentSuccessfull');
        } else {
            return view('paymentFailed');
        }
    }
    public function Application_Payment(Request $req)
    {
        // return $req;
        $application_status = application_data::where('id', $req->applicationId)
            ->select(
                'application_status',
                'id',
                'school_id',
                'school_name',
                'state',
                'division',
                'district',
                'block',
                'pincode',
                'post_office',
                'type_of_school'
            )
            ->first();
        //   return $application_status;                      
        $DEPTID = 'JEPC'; // provided by department fixed
        $DEPRECIEPTHEADCODETID = '020201101010101'; // provided by department fixed
        $DEPOSITERNAME = $application_status->school_name; // dynamic school name
        // $DEPTTRANID = substr($req->application_forn_number, 2) . 'r' . date('YmdHis'); //19 characters only = 010001r210208120203
        $application_id = $req->applicationId;
        $DEPTTRANID = strlen($application_id) == 1 ? '00000' . $application_id : (strlen($application_id) == 2 ? '0000' . $application_id : (strlen($application_id) == 3 ? '000' . $application_id : (strlen($application_id) == 4 ? '00' . $application_id : (strlen($application_id) == 5 ? '0' . $application_id : $application_id))));
        if (strlen($application_id) > 5) {
            return "Department transaction ID limit reached.";
        }
        $DEPTTRANID .= 'r' . date('ymdHis');
        // $DEPTTRANID = substr($req->applicationId, 2) . 'r' . date('YmdHis'); //19 characters only = 000001r210208120203
        // return $DEPTTRANID;
        if ($application_status->type_of_school == '1-5') {
            $AMOUNT = 12500; //it will be either 12500 | 25000
        } elseif ($application_status->type_of_school == '1-8') {
            $AMOUNT = 25000;
        }
        // $AMOUNT = 2;
        $DEPOSITERID = $req->applicationId;
        $PANNO_optional = 'NA'; //NA
        $ADDINFO1_optional = ($application_status->school_id != '') ? $application_status->school_id : 'NA';  //application details in character
        $ADDINFO2_optional = ($application_status->type_of_school != '') ?  $application_status->type_of_school : 'NA';
        $ADDINFO3_optional = ($application_status->school_name != '') ? $application_status->school_name : 'NA';
        $ADDINFO3_optional .= ',' . ($application_status->post_office != '') ? $application_status->post_office : 'NA';
        $ADDINFO3_optional .= ',' . ($application_status->pincode != '') ? $application_status->pincode : 'NA'; //school details in character 2
        $ADDINFO3_optional .= ',' . $req->applicationId . ',' . $application_status->division . ',' . $application_status->district . ',' . $application_status->block; //school details in character 1
        $TREASCODE = $req->Treasury_Code; // will be dynamic
        $IFMSOFFICECODE = $req->ddo; //will be dynamic
        $SECURITYCODE = 'sec1234'; //provided by jeGras fiexd
        $RESPONSE_URL_optional = 'NA'; // NA
        $simple_string = "";
        $simple_string = $DEPTID . "|" . $DEPRECIEPTHEADCODETID . "|" . $DEPOSITERNAME . "|" . $DEPTTRANID . "|" . $AMOUNT . "|" . $DEPOSITERID . "|" . $PANNO_optional . "|" . $ADDINFO1_optional . "|" . $ADDINFO2_optional . "|" . $ADDINFO3_optional . "|" . $TREASCODE . "|" . $IFMSOFFICECODE . "|" . $SECURITYCODE . "|NA"; //$RESPONSE_URL_optional
        // Display the original string
        // echo "Original String: " . $simple_string;
        // Store the cipher method
        $ciphering = "AES-128-CBC";
        // Use OpenSSl Encryption method
        $options = 0;
        // Non-NULL Initialization Vector for encryption
        $encryption_iv = "key1234";
        // Store the encryption key
        $encryption_key = "key1234";
        $transactions = new Transaction();
        $transactions->dept_id = $DEPTID;
        $transactions->application_id = $DEPOSITERID;
        $transactions->school_id = $application_status->school_id;
        $transactions->receipt_head_code = $DEPRECIEPTHEADCODETID;
        $transactions->dept_tran_id = $DEPTTRANID;
        $transactions->amount = $AMOUNT;
        $transactions->depositor_name = $application_status->school_name;
        $transactions->depositor_id = $DEPOSITERID;
        $transactions->pan_no =  $PANNO_optional;
        $transactions->add_info_1 = $ADDINFO1_optional;
        $transactions->add_info_2 = $ADDINFO2_optional;
        $transactions->add_info_3 = $ADDINFO3_optional;
        $transactions->treas_code = $TREASCODE;
        $transactions->ifms_office_code = $IFMSOFFICECODE;
        $transactions->security_code = $SECURITYCODE;
        $transactions->response_status = 'PENDING';
        $transactions->save();
        // Use openssl_encrypt() function to encrypt the data
        $encryption = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);
        // Display the encrypted string
        // return $application_status;
        return ["enc" => $encryption];
    }
    public function DDO_List($req)
    {
        $DDO = Fnd_ddo_value::select(
            'fnd_ddo_values.id',
            'fnd_ddo_values.ddo_name',
            'fnd_ddo_values.ddo_office',
            'fnd_ddo_values.ddo_code'
        )->where('treasury_id', $req)->get();
        $Treasurys = Fnd_treasury_value::where('id', $req)->value('treasury_code');
        $data = ['DDO' => $DDO, 'Treasurys' => $Treasurys];
        return $data;
    }
    public function applicationSubmitPayment(Request $request)
    {
        $transactions = new Transaction();
        $transactions->dept_id = 'JEPC';
        $transactions->receipt_head_code = '020201101010101';
        $transactions->depositor_name = 'rte application test school'; //alpha numeric
        $transactions->dept_tran_id = '2021020401012369860'; //19 characters only
        $transactions->amount = '10';
        $transactions->depositor_id = '20210101001';
        $transactions->pan_no = 'GLTPK4310R';
        $transactions->add_info_1 = 'school-detail-1';
        $transactions->add_info_2 = 'school-detail-2';
        $transactions->add_info_3 = 'application-detail';
        $transactions->treas_code = 'JSR';
        $transactions->ifms_office_code = 'JSRDAD036';
        $transactions->security_code = 'sec1234';
        $transactions->response_url = 'https://paatham.us/rte-application/';
        $transactions->created_at = date('Y-m-d H:i:s');
        $transactions->save();
        $applicationId = "coming soon";
    }
    public function challanPreview(Request $req)
    {
        // return $req;
        $transaction = Transaction::leftjoin('application_data', 'application_data.id', 'transactions.application_id')->where('application_id', $req->application_id)->first();
        // return $transaction;
        return view('challan', ['challan' => $transaction]);
    }
    public function ApplicationPaymentForm(Request $request)
    {
        // return $request;
        $application_id = $request->application_id;
        $application_data = application_data::where('application_data.id', $application_id)->where('application_data.status', 1)
            ->first();
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'
        )->get();
        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return view('payment_application', ['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }
    public function ApplicationChallanForm(Request $request)
    {
        // return $request;
        $application_id = $request->application_id;
        $application_data = application_data::where('application_data.id', $application_id)->where('application_data.status', 1)
            ->first();
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'
        )->get();
        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return view('challan_form', ['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }

    public function Application_Challan(Request $req)
    {
        // return $req;
        // $data = Transaction::where('grn',$req->GRN)->exists();
        // return $data;

        if (!Transaction::where('grn', $req->GRN)->exists()) {

            // return ['if '=>$req];
            application_data::where('id', $req->applicationId)->update([
                'application_status' => 27,
                'updated_by' => '-1'
            ]);


            $transactions = new Transaction();
            $transactions->dept_id = 'JEPC';
            $transactions->school_id = $req->school_id;
            $transactions->application_id = $req->applicationId;
            // $transactions->receipt_head_code = '020201101010101';
            $transactions->depositor_name = $req->school_name; //alpha numeric
            // $transactions->dept_tran_id = '2021020401012369860'; //19 characters only
            $transactions->amount = $req->amount;
            // $transactions->depositor_id = '20210101001';
            // $transactions->pan_no = 'GLTPK4310R';
            $transactions->add_info_1 = $req->applicationId . ' | ' . $req->school_name;
            $transactions->add_info_2 = $req->post_office;
            $transactions->add_info_3 = $req->phone_with_std_code;
            // $transactions->add_info_4 = $req->transaction_id;
            // $transactions->treas_code = 'JSR';
            $transactions->grn = $req->GRN;
            // $transactions->ifms_office_code = 'JSRDAD036';
            $transactions->security_code = 'sec1234';
            $transactions->response_url = 'https://paatham.us/rte-application/';


            if ($req->document != null) {
                foreach ($req->document as $img) {
                    if (isset($img) && !empty($img)) {
                        $fileName = time() . '.' . $img->getClientOriginalName();
                        $img->move(public_path('assets/'), $fileName);
                        $image_name[] = $fileName;
                    }
                }
                $decode_image = json_encode($image_name, true);
            }

            $transactions->document = $decode_image;
            $transactions->created_at = date('Y-m-d H:i:s');
            $transactions->save();
            return redirect('dashboard')->with('message', 'challan submitted !!');
        } else {
            // return ['else '=>$req];

            return redirect('dashboard')->with('message', 'GRN Number Already Exist !!');
        }
    }

    public function UpdateApplicationFile(Request $request)
    {
        // return $request;
        $field_name = $request->field_name;
        $file_name = $request->file_name;
        $fileInstance = $request->file;
        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
        $fileInstance->move(public_path('assets/applications'), $newFileName);
        $image_path = public_path("assets/applications/" . $file_name);
        @unlink($image_path);
        if ($field_name == 'teacher_educational_qualification_files') {
            $file_arr = application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->value($request->field_name);
        } else {
            $file_arr = application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->value($request->field_name);
        }
        if ($file_arr != null)
            $file_arr = json_decode($file_arr, true);
        foreach ($file_arr as $key => $item) {
            if ($item == $file_name)
                $file_arr[$key] = $newFileName;
        }
        $file_arr = json_encode($file_arr);
        if ($field_name == 'teacher_educational_qualification_files') {
            application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->update([$field_name => $file_arr]);
        } else {
            application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->update([$field_name => $file_arr]);
        }
        return redirect()->back();
    }
    public function RemoveApplicationFile(Request $request)
    {
        // return $request;
        $field_name = $request->field_name;
        $file_name = $request->file_name;
        $image_path = public_path("assets/applications/" . $file_name);
        // return $image_path;
        @unlink($image_path);
        if ($field_name == 'teacher_educational_qualification_files') {
            $file_arr = application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->value($request->field_name);
        } else {
            $file_arr = application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->value($request->field_name);
        }
        if ($file_arr != null)
            $file_arr = json_decode($file_arr, true);
        if (($key = array_search($file_name, $file_arr)) !== false) {
            unset($file_arr[$key]);
        }
        $file_arr = json_encode(array_values($file_arr));
        if ($field_name == 'teacher_educational_qualification_files') {
            application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->update([$field_name => $file_arr]);
        } else {
            application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->update([$field_name => $file_arr]);
        }
        return redirect()->back();
    }
    public function InsertApplicationFile(Request $request)
    {
        // return $request;
        $newFileName = '';
        $field_name = $request->field_name;
        $fileInstance = $request->file;
        $newFileName = "application_1_" . date('YmdHis') . "_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
        $fileInstance->move(public_path('assets/applications'), $newFileName);
        if ($field_name == 'teacher_educational_qualification_files') {
            $file_arr = application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->value($request->field_name);
        } else {
            $file_arr = application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->value($request->field_name);
        }
        if ($file_arr != null)
            $file_arr = json_decode($file_arr, true);
        array_push($file_arr, $newFileName);
        $file_arr = json_encode($file_arr);
        if ($field_name == 'teacher_educational_qualification_files') {
            application_teacher_data::where('application_teacher_data.id', $request['teacher_id'])->update([$field_name => $file_arr]);
        } else {
            application_data_ext_1::where('application_data_ext_1.application_data_id', $request['application_id'])->update([$field_name => $file_arr]);
        }
        return redirect()->back();
    }
    public function UpdateMessageStatus($request)
    {
        $Conversations = Conversations::Where('conversations.parent_id', $request)
            ->where('conversations.conversation_type', 4);
        if (Auth::user()->user_role == 'school')
            $Conversations->where('school_read_status', 0)->update(['school_read_status' => 1]);
        if (Auth::user()->user_role == 'dc')
            $Conversations->where('dc_read_status', 0)->update(['dc_read_status' => 1]);
        if (Auth::user()->user_role == 'dse')
            $Conversations->where('dse_read_status', 0)->update(['dse_read_status' => 1]);
        if (Auth::user()->user_role == 'faa')
            $Conversations->where('faa_read_status', 0)->update(['faa_read_status' => 1]);
        if (Auth::user()->user_role == 'saa')
            $Conversations->where('saa_read_status', 0)->update(['saa_read_status' => 1]);
        return 1;
    }
    public function DeleteTeacher($request)
    {
        $file_arr = application_teacher_data::where('application_teacher_data.id', $request)->value('teacher_educational_qualification_files');
        $file_arr_1 = json_encode($file_arr);
        if ($file_arr_1 != 'null') {
            $file_arr = json_decode($file_arr, true);
            foreach ($file_arr as $key => $value) {
                $image_path = public_path("assets/applications/" . $value);
                if (file_exists($image_path)) {
                    unlink($image_path);
                }
            }
        }
        DB::table('application_teacher_data')->where('id', $request)->delete();
        return 1;
    }
    public function Pending_Payment(Request $req)
    {

        $DEPTID = 'JEPC'; // provided by department fixed
        $GRN = $req->grn; // will be dynamic
        $DEPTTRANID  = $req->dept_tran_id; //will be dynamic
        $SECURITYCODE = 'sec1234'; //provided by jeGras fiexd
        $simple_string = "";
        $simple_string = $GRN . "|" . $DEPTID . "|" . $DEPTTRANID . "|" . $SECURITYCODE; //$RESPONSE_URL_optional

        // Display the original string
        // echo "Original String: " . $simple_string;
        // Store the cipher method
        $ciphering = "AES-128-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = "key1234";

        // Store the encryption key
        $encryption_key = "key1234";

        // Use openssl_encrypt() function to encrypt the data
        $encryption = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);
        // Display the encrypted string
        // return $application_status;
        return ["enc" => $encryption];
    }
    public function Challan_Verify_Paid(Request $req)
    {

        $GRN = $req->grn; // will be dynamic
        $AMOUNT  = $req->amount; //will be dynamic
        $SECURITYCODE = 'sec1234'; //provided by jeGras fiexd
        $simple_string = "";
        $simple_string = $GRN . "|" . $AMOUNT . "|" . $SECURITYCODE; //$RESPONSE_URL_optional

        // Display the original string
        // echo "Original String: " . $simple_string;
        // Store the cipher method
        $ciphering = "AES-128-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = "key1234";

        // Store the encryption key
        $encryption_key = "key1234";

        // Use openssl_encrypt() function to encrypt the data
        $encryption = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);
        // Display the encrypted string
        // return $application_status;
        return ["enc" => $encryption];
    }
    public function Challan_Paid_Verify(Request $req)   // for manual entry by DSE accept challan 
    {

        $applicationdataid = $req->applicationdataid; // will be dynamic
        $GRN = $req->grn; // will be dynamic

        application_data::where('id', $applicationdataid)->update([
            'application_status' => 2,
            'updated_by' => '-1'
        ]);

        $school_id = $req->school_id; // will be dynamic

        $application_tracking = new application_tracking;
        $application_tracking->application_id = $applicationdataid;
        $application_tracking->school_id = $school_id;
        $application_tracking->application_status_id = 2;
        $application_tracking->created_by = -1;
        $application_tracking->save();

        Transaction::where('application_id', $applicationdataid)->where('grn', $GRN)->update([
                'status' => 2,
                'pmode' => 'Challan'
            ]);

        return redirect()->back();
    }



    public function AppChallanForm($id)
    {

        $application_data = application_data::where('application_data.id', $id)->where('application_data.status', 1)
            ->first();
        // return $application_data;
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'
        )->get();
        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return view('challan_form', ['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }

    public function AppPaymentForm($id)
    {

        $application_data = application_data::where('application_data.id', $id)->where('application_data.status', 1)
            ->first();
        $Treasurys = Fnd_treasury_value::select(
            'fnd_treasury_values.id',
            'fnd_treasury_values.treasury_title'
        )->get();
        // return (['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
        return view('payment_application', ['School_Data' => $application_data, 'Treasurys' => $Treasurys]);
    }
    public function TrackingSMS($phone, $msg)
    {
        header('Content-Type: text/html;');
        $username = "uidjharsms-DOSENL"; //username of the department
        $password = "Jepcsms@123"; //password of the department
        $senderid = "JHGOVT"; //senderid of the deparment
        $message = $msg;
        $mobileno = $phone; //if single sms need to be send use mobileno keyword
        $deptSecureKey = "37e4833a-1360-4ba3-b0ea-ce019987c175"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $this->sendSingleUnicode($username, $encryp_password, $senderid, $message, $mobileno, $deptSecureKey);
    }

    //function to send single unicode sms
    public function sendSingleUnicode($username, $encryp_password, $senderid, $messageUnicode, $mobileno, $deptSecureKey)
    {
        $finalmessage = $this->string_to_finalmessage(trim($messageUnicode));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($finalmessage) . trim($deptSecureKey));
        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($finalmessage),
            "smsservicetype" => "unicodemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $this->post_to_url_unicode("https://msdgweb.mgov.gov.in/esms/sendsmsrequest", $data); //calling post_to_url_unicode to send single unicode sms
    }

    //function to convert unicode text in UTF-8 format
    function string_to_finalmessage($message)
    {
        $finalmessage = "";
        $sss = "";
        for ($i = 0; $i < mb_strlen($message, "UTF-8"); $i++) {
            $sss = mb_substr($message, $i, 1, "utf-8");
            $a = 0;
            $abc = "&#" . $this->ordutf8($sss, $a) . ";";
            $finalmessage .= $abc;
        }
        return $finalmessage;
    }

    //function to convet utf8 to html entity
    function ordutf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) -
                ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        return $code;
    }

    //function to send unicode sms by making http connection
    function post_to_url_unicode($url, $data)
    {
        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("Content-length:"
            . strlen($fields)));
        curl_setopt($post, CURLOPT_HTTPHEADER, array("User-Agent:Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)"));
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        echo $result = curl_exec($post); //result from mobile seva server
        curl_close($post);
    }

    public function single_window_login(Request $request)
    {

        $custId = $request->custId;  // Customer registration reference no at Single window system
        // $pwd = $request->password;  // password of the user
        $first_name = $request->first_name; // First name of the user
        $last_name = $request->last_name; // Last name of the user
        $email = $request->email;  // Email id of the user
        $mobile = $request->mobile; // Mobile no of the user
        $department_id = $request->department_id; // Unique id mapped for department at Single Window system (Provided separately)
        $session_id = $request->session_id; // Unique id generated for each user at Single window system on login

        if ($user = single_window_user::where('custId', $custId)->where('status', 1)->where('is_registered', 1)->first()) {
            // return $user;
            $credentials = ([
                'email' => $user->email,
                'password' =>  $user->password
            ]);
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();

                $wordCount = $wordCountAll = $wordCountAcceptedByAllUsers = $wordCountA = $wordCountByDSC = $wordCountR =  $wordCountP = $wordCountAcpt = $AllDistrict = $Arr_Block_Filter = '';
                if (Auth::user()->user_role == 'saa') {
                    $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
                        ->select(
                            'application_data.school_name',
                            'addresses.block',
                            'addresses.district',
                            'appeals.created_at',
                            'appeals.appeal_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block_name',
                            'appeals.appeal_status AS status_name'
                        )
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->whereIn('appeals.appeal_status', [1, 2, 3])
                        ->orderBy('appeals.created_at', 'DESC')->get();
                    // return $AllDistrict;
                    $CountAllDistrict = $AllDistrict->count();
                    $all_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                    $pending_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->where('appeals.appeal_status', '1')->count();
                    $accepted_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->where('appeals.appeal_status', '2')->count();
                    $rejected_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->where('appeals.appellate_authority_type', '2')
                        ->where('appeals.school_id', '!=', '1')
                        ->where('appeals.appeal_status', '3')->count();
                    $District = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.value_set_id', '=', 1)->get();
                    // return $District;
                    $DistrictCount = $District->count();
                    $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                    for ($i = 0; $i < $DistrictCount; $i++) {
                        $AllAppeal = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                        $CountAllApplication = $AllAppeal->count();
                        array_push($Total_Appeal, $CountAllApplication);
                        $AllPending = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 1)->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                        $AllAppealVerified = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 2)->get();
                        $CountAllVerified = $AllAppealVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllAppealReject = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('application_data.division', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('addresses.division', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 3)->get();
                        $CountAllReject = $AllAppealReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                    }
                    $Arr_District_Filter = [
                        'DistrictCount' => $DistrictCount,
                        'District' => $District,
                        'Total_Appeal' => $Total_Appeal,
                        'Total_Verification' => $Total_Verification,
                        'Total_Rejection' => $Total_Rejection,
                        'Total_Pending' => $Total_Pending
                    ];
                    $wordCount = [
                        'AllDistrict' => $AllDistrict,
                        'allappeals' => $all_appeals,
                        'pendingappeals' => $pending_appeals,
                        'acceptedappeals' => $accepted_appeals,
                        'rejectedAppeal' => $rejected_appeals,
                        'Arr_District_Filter' => $Arr_District_Filter
                    ];
                }
                if (Auth::user()->user_role == 'faa') {
                    $UserDivision = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.division')
                        ->get();
                    $User_Division = $UserDivision[0]->division;
                    $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->select(
                            'application_data.school_name',
                            'addresses.block',
                            'addresses.district',
                            'appeals.created_at',
                            'appeals.appeal_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block_name',
                            'appeals.appeal_status AS status_name'
                        )
                        ->whereIn('appeals.appeal_status', [1, 2, 3])
                        ->orderBy('appeals.created_at', 'DESC')->get();
                    // return $AllDistrict;
                    $CountAllDistrict = $AllDistrict->count();
                    $all_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                    $pending_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->where('appeals.appeal_status', '1')->count();
                    $accepted_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->where('appeals.appeal_status', '2')->count();
                    $rejected_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                        ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                        ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                        ->where('application_data.division', $User_Division)
                        ->where('appeals.appellate_authority_type', '1')
                        ->where('addresses.division', '=', $User_Division)
                        ->where('appeals.appeal_status', '3')->count();
                    $District = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_Division)->get();
                    // return $District;
                    $DistrictCount = $District->count();
                    $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                    for ($i = 0; $i < $DistrictCount; $i++) {
                        $AllAppeal = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                        $CountAllApplication = $AllAppeal->count();
                        array_push($Total_Appeal, $CountAllApplication);
                        $AllPending = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 1)->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                        $AllAppealVerified = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 2)->get();
                        $CountAllVerified = $AllAppealVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllAppealReject = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.district', $District[$i]->id)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.district', '=', $District[$i]->id)
                            ->where('appeals.status', '1')
                            ->where('appeals.appeal_status', 3)->get();
                        $CountAllReject = $AllAppealReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                    }
                    $Arr_District_Filter = [
                        'DistrictCount' => $DistrictCount,
                        'District' => $District,
                        'Total_Appeal' => $Total_Appeal,
                        'Total_Verification' => $Total_Verification,
                        'Total_Rejection' => $Total_Rejection,
                        'Total_Pending' => $Total_Pending
                    ];
                    $wordCount = [
                        'AllDistrict' => $AllDistrict,
                        'allappeals' => $all_appeals,
                        'pendingappeals' => $pending_appeals,
                        'acceptedappeals' => $accepted_appeals,
                        'rejectedAppeal' => $rejected_appeals,
                        'Arr_District_Filter' => $Arr_District_Filter
                    ];
                }
                if (Auth::user()->user_role == 'dse') {
                    $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending =  array();
                    $UserDistrict = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.district')
                        ->get();
                    $User_District = $UserDistrict[0]->district;
                    $Block = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_District)->get();
                    $BlockCount = $Block->count();
                    $AllDistrict = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->limit($BlockCount - 1)->get();
                    $Alldata = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', '>', '1')->get();
                    $wordCountAll = $Alldata->count();
                    $AlldataByDSC = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->whereIn('application_status', [2, 3])->get();
                    $wordCountByDSC = $AlldataByDSC->count();
                    $AlldataAccept = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', 4)->get();
                    $wordCountAcpt = $AlldataAccept->count();
                    $AlldataReject = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', 8)->get();
                    $wordCountR = $AlldataReject->count();
                    for ($i = 0; $i < $BlockCount; $i++) {
                        $AllApplication = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', '>', '1')->get();
                        $CountAllApplication = $AllApplication->count();
                        array_push($Total_Application, $CountAllApplication);
                        $AllReject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 8)->get();
                        $CountAllReject = $AllReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                        $AllVerified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 4)->get();
                        $CountAllVerified = $AllVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllPending = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->whereIn('application_status', [2, 3])->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                    }
                    $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                    // return $Arr_Block_Filter;
                    $All_Meeting_Data = DB::table('application_data')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                        ->select(
                            'application_data.school_name',
                            'application_data.created_at',
                            'application_data.block',
                            'application_data.district',
                            'application_data.application_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            'status.status_name AS status_name',
                            'application_meetings.meeting_date'
                        )
                        ->where('application_data.application_status', '5')
                        ->where('application_data.district', $User_District)
                        ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                        ->where('application_data.status', '1')->limit($BlockCount - 1)
                        ->get();
                    $wordCount = [
                        'All'                 => $wordCountAll,
                        'Active'              => $wordCountA,
                        'VerifiedByDSC'       => $wordCountByDSC,
                        'Reject'              => $wordCountR,
                        'Pending'              => $wordCountP,
                        'Accept'              => $wordCountAcpt,
                        'AllDistrict'         => $AllDistrict,
                        'Arr_Block_Filter'    => $Arr_Block_Filter,
                        'All_Meeting_Data' => $All_Meeting_Data
                    ];
                }
                if (Auth::user()->user_role == 'dc') {
                    $UserDistrict = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.district')
                        ->get();
                    $User_District = $UserDistrict[0]->district;
                    $Block = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_District)
                        ->get();
                    $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                    $BlockCount = $Block->count();
                    // return $BlockCount;
                    $Alldata = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->limit($BlockCount - 1)
                        ->where('application_status', '>', '1')
                        ->orderBy('created_at', 'ASC')->get();
                    $wordCountAll = $Alldata->count();
                    $AllVerified = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        // ->where('application_status', '4')->get();
                        ->where('application_status', 6)->get();
                    $AllVerifiedCount = $AllVerified->count();
                    $AllPending = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        // ->where('application_status', '4')->get();
                        ->where('application_status', 4)->get();
                    $AllPendingCount = $AllPending->count();
                    $Meeting = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', '5')->get();
                    $MeetingCount = $Meeting->count();
                    $Certified = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        ->where('application_status', '6')
                        ->limit($BlockCount - 1)
                        ->orderBy('created_at', 'DESC')->get();
                    $CertifiedCount = $Certified->count();
                    $Reject = DB::table('application_data')
                        ->where('district', $User_District)
                        ->where('status', '1')
                        // ->where('application_status', '7')->get();
                        ->where('application_status', 7)->get();
                    $RejectCount = $Reject->count();
                    $All_Meeting_Data = DB::table('application_data')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                        ->select(
                            'application_data.school_name',
                            'application_data.created_at',
                            'application_data.block',
                            'application_data.district',
                            'application_data.application_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            'status.status_name AS status_name',
                            'application_meetings.meeting_date'
                        )
                        ->where('application_data.application_status', '5')
                        ->where('application_data.district', $User_District)
                        ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                        ->where('application_data.status', '1')->limit($BlockCount - 1)
                        ->get();
                    for ($i = 0; $i < $BlockCount; $i++) {
                        $AllApplication = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', '>', '1')->get();
                        $CountAllApplication = $AllApplication->count();
                        array_push($Total_Application, $CountAllApplication);
                        $AllReject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 7)->get();
                        // ->where('application_status', '7')->get();
                        $CountAllReject = $AllReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                        $AllVerified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', 6)->get();
                        // ->where('application_status', '6')->get();
                        $CountAllVerified = $AllVerified->count();
                        array_push($Total_Verification, $CountAllVerified);
                        $AllPending = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('block', $Block[$i]->id)
                            ->where('application_status', '4')->get();
                        // ->whereIn('application_status', [2, 3, 4, 9, 12])->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                    }
                    $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                    // return($Arr_Block_Filter);
                    $wordCount = [
                        'All'           => $wordCountAll,
                        'AllDistrict' => $Alldata,
                        'AllVerifiedCount' => $AllVerifiedCount,
                        'AllPendingCount' => $AllPendingCount,
                        'MeetingCount' => $MeetingCount,
                        'CertifiedCount' => $CertifiedCount,
                        'RejectCount' => $RejectCount,
                        'AllCertified' => $Certified,
                        'Arr_Block_Filter' => $Arr_Block_Filter,
                        'All_Meeting_Data' => $All_Meeting_Data
                    ];
                }
                if (Auth::user()->user_role == 'state') {
                    $Total_Pending  = $Total_Approved = $Total_Application = $Total_Verification = $Total_Rejection = array();
                    $UserDivision = DB::table('addresses')
                        ->where('addresses.user_id', Auth::user()->id)
                        ->select('addresses.division')
                        ->get();
                    $User_Division = $UserDivision[0]->division;
                    $Division = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.value_set_id', '1')
                        ->get();
                    $DivisionCount = $Division->count();
                    $Alldata = DB::table('application_data')->where('status', '1')->where('application_data.application_status', '>', 1)->get();
                    $wordCountAll = $Alldata->count();
                    $AcceptedByAllUsers = DB::table('application_data')->where('status', '1')->whereIn('application_status', [4, 6, 10, 13])->get();
                    $wordCountAcceptedByAllUsers = $AcceptedByAllUsers->count();
                    $AlldataAccept = DB::table('application_data')->where('status', '1')->where('application_status', '6')->get();
                    $wordCountAcpt = $AlldataAccept->count();
                    $AlldataReject = DB::table('application_data')->where('status', '1')->whereIn('application_status', [7, 8, 11, 14])->get();
                    $wordCountR = $AlldataReject->count();
                    $AllDetails = DB::table('application_data')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->select(
                            'application_data.school_name',
                            'application_data.created_at',
                            'application_data.block',
                            'application_data.district',
                            'application_data.application_status',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            'status.status_name AS status_name'
                        )
                        ->where('application_data.status', '1')
                        ->where('application_data.application_status', '>', 1)
                        ->limit($DivisionCount)
                        ->orderBy('created_at', 'desc')->get();
                    $AllCertified = DB::table('application_data')->where('status', '1')->where('division', $User_Division)->where('application_status', '6')->get();
                    $CountAllCertified = $AllCertified->count();
                    for ($i = 0; $i < $DivisionCount; $i++) {
                        $AllApplication = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_data.application_status', '>', 1)->get();
                        $CountAllApplication = $AllApplication->count();
                        array_push($Total_Application, $CountAllApplication);
                        $AllReject = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->whereIn('application_status', [7, 8, 11, 14])->get();
                        $CountAllReject = $AllReject->count();
                        array_push($Total_Rejection, $CountAllReject);
                        $AllPending = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->wherein('application_status', [2, 3, 4, 9, 12])->get();
                        $CountAllPending = $AllPending->count();
                        array_push($Total_Pending, $CountAllPending);
                        $AllApproved = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_status', 6)->get();
                        $CountAllApproved = $AllApproved->count();
                        array_push($Total_Approved, $CountAllApproved);
                    }
                    $Arr_Block_Filter = ['BlockCount' => $DivisionCount, 'Block' => $Division, 'Total_Application' => $Total_Application, 'Total_Pending' => $Total_Pending, 'Total_Approved' => $Total_Approved, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection];
                    $wordCount = [
                        'All' => $wordCountAll,
                        'Active' => $wordCountA,
                        'VerifiedByAllUser' => $wordCountAcceptedByAllUsers,
                        'Reject' => $wordCountR,
                        'Pending' => $wordCountP,
                        'Accept' => $wordCountAcpt,
                        'AllCertified' => $AllCertified,
                        'AllDetails' => $AllDetails,
                        'Arr_Block_Filter' => $Arr_Block_Filter
                    ];
                }
                if (Auth::user()->user_role == 'school') {
                    // return 'true';
                    $Accepted_Fields = 0;
                    $Rejected_Fields = 0;
                    $pending_Fields = 0;
                    $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
                    foreach ($alltable_col as $key => $value) {
                        if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at" && $value != "created_by_vrfy" && $value != "updated_by_vrfy") {
                            if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                $Accepted_Fields++;
                            }
                            if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                $Rejected_Fields++;
                            }
                            if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                $pending_Fields++;
                            }
                        }
                    }
                    $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
                    // echo $alltable_col;
                    foreach ($alltable_ext_col as $key => $value) {
                        if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {
                            if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                $Accepted_Fields++;
                            }
                            if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                $Rejected_Fields++;
                            }
                            if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                $pending_Fields++;
                            }
                        }
                    }
                    $UserDistrict = DB::table('private_schools')
                        ->where('id', Auth::user()->school_id)
                        ->value('district');
                    $User_District = $UserDistrict;
                    $Block = Fnd_value::select(
                        'fnd_values.id',
                        'fnd_values.display_value'
                    )->where('fnd_values.parent_value_id', '=', $User_District)
                        ->get();
                    $Application_Status = application_data::where('application_data.school_id', Auth::user()->school_id)
                        ->value('application_data.application_status');
                    // $acceptedted_feild = DB::table('application_data_verifications')
                    // ->select( DB::raw('count(school_name_vrfy,udise_code_vrfy) as accepted_count, school_name_vrfy') )
                    // ->where('application_data_verifications.school_id', Auth::user()->school_id)
                    // ->groupBy('school_name_vrfy')
                    // ->get();
                    // return $acceptedted_feild;
                    $Application_Tracking = DB::table('application_trackings')
                        ->Join('application_statuses', 'application_trackings.application_status_id', '=', 'application_statuses.id')
                        ->select(
                            'application_trackings.application_id',
                            'application_trackings.school_id',
                            'application_trackings.application_status_id',
                            'application_trackings.created_at',
                            'application_trackings.created_by',
                            'application_statuses.status_name AS status_name'
                        )
                        ->where('application_trackings.school_id', Auth::user()->school_id)
                        ->get();
                    $CountApplication_Tracking = $Application_Tracking->count();
                    $feedback = DB::table('application_data')
                        ->select(
                            'application_data.id',
                            'application_data.created_by',
                            'application_data.school_name',
                            'application_data.status',
                            'application_data.feedback_file_upload',
                            'application_data.feedback_from_dsc_to_school',
                            'application_data.verification_date'
                        )
                        ->where('application_data.school_id', Auth::user()->school_id)->where('application_data.status', 1)->get();
                    $All_Meeting_Data_school = DB::table('application_meetings')
                        ->Join('application_data', 'application_data.id', '=', 'application_meetings.application_id')
                        ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                        ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                        ->Join('users', 'users.id', '=', 'application_meetings.created_by')
                        // ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                        ->select(
                            'application_meetings.meeting_date',
                            'application_meetings.meeting_remarks',
                            'application_data.school_name',
                            'users.name',
                            'application_meetings.created_at',
                            'application_meetings.meeting_text',
                            'application_meetings.meeting_remarks',
                            'application_meetings.meeting_documents',
                            'fnd_district.display_value AS disrict',
                            'fnd_block.display_value AS block',
                            // 'status.status_name AS status_name'
                        )
                        // ->where('application_data.application_status', '5')
                        ->where('application_meetings.school_id', Auth::user()->school_id)
                        ->get();
                    foreach ($All_Meeting_Data_school as  $Meeting_Data_school) {
                        $Meeting_Data_school->meeting_documents = json_decode($Meeting_Data_school->meeting_documents);
                    }
                    // if ($All_Meeting_Data_school->meeting_documents != null)
                    // $All_Meeting_Data_school->meeting_documents = json_decode($All_Meeting_Data_school->meeting_documents, true);
                    if ($Application_Status == 1) {
                        $Accepted_Fields = 0;
                        $Rejected_Fields = 0;
                        $pending_Fields = 0;
                    }
                    $wordCount = [
                        'feedback' => $feedback,
                        'Application_Tracking' => $Application_Tracking,
                        'Accepted_Fields' => $Accepted_Fields,
                        'Rejected_Fields' => $Rejected_Fields,
                        'pending_Fields' => $pending_Fields,
                        'All_Meeting_Data_school' => $All_Meeting_Data_school,
                        'Application_Status' => $Application_Status
                    ];
                }
                // return [Auth::user()];
                return view('dashboard', compact('wordCount'));
            }
        } elseif ($user = single_window_user::where('custId', $custId)->where('status', 1)->where('is_registered', 0)->first()) {
            // return $user;
            $credentials = ([
                'email' => $user->email,
                'password' =>  $user->password
            ]);
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();

                // return [Auth::user()];
                return view('single_window_school_register')->with('data', $user);
            }
        } else {
            // return 0;
            $school = new Private_school;
            $school->school_name = $request->school_name;
            if(Private_school::where('school_email',$email)->count() > 0){
                return response()->json(['message'=> 'Mail Already Exist']);
            }
            $school->school_email = $email;
            // $school->udise_code = $request->udise_code;
            $school->contact_person = $first_name . ' ' . $last_name;
            $school->created_by = -1;
            $school->save();

            $user = new User;
            $user->school_id = $school->id;
            $user->name = $first_name . ' ' . $last_name;
            $user->username = $custId;
            $user->password = Hash::make($custId);
            if(User::where('email',$email)->count() > 0){
                return response()->json(['message'=> 'Mail Already Exist']);
            }
            $user->email = $email;
            $user->user_role = 'school';
            $user->save();

            $single_user = new single_window_user;
            $single_user->custId = $custId;
            $single_user->user_id = $user->id;
            $single_user->school_id = $school->id;
            $single_user->username = $custId;
            $single_user->password = $custId;
            $single_user->first_name = $first_name;
            $single_user->last_name = $last_name;
            $single_user->email = $email;
            $single_user->mobile = $mobile;
            $single_user->department_id = $department_id;
            $single_user->session_id = $session_id;
            $single_user->created_by = -1;
            $single_user->save();

            if ($user = single_window_user::where('custId', $custId)->where('status', 1)->where('is_registered', 0)->first()) {
                // return $user;
                $credentials = ([
                    'email' => $user->email,
                    'password' =>  $user->password
                ]);
                if (Auth::attempt($credentials)) {
                    $request->session()->regenerate();

                    $wordCount = $wordCountAll = $wordCountAcceptedByAllUsers = $wordCountA = $wordCountByDSC = $wordCountR =  $wordCountP = $wordCountAcpt = $AllDistrict = $Arr_Block_Filter = '';
                    if (Auth::user()->user_role == 'saa') {
                        $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->leftjoin('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->leftjoin('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->leftjoin('fnd_values as fnd_division', 'application_data.division', '=', 'fnd_division.id')
                            ->select(
                                'application_data.school_name',
                                'addresses.block',
                                'addresses.district',
                                'appeals.created_at',
                                'appeals.appeal_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block_name',
                                'appeals.appeal_status AS status_name'
                            )
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])
                            ->orderBy('appeals.created_at', 'DESC')->get();
                        // return $AllDistrict;
                        $CountAllDistrict = $AllDistrict->count();
                        $all_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                        $pending_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('appeals.appeal_status', '1')->count();
                        $accepted_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('appeals.appeal_status', '2')->count();
                        $rejected_appeals = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->where('appeals.appellate_authority_type', '2')
                            ->where('appeals.school_id', '!=', '1')
                            ->where('appeals.appeal_status', '3')->count();
                        $District = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.value_set_id', '=', 1)->get();
                        // return $District;
                        $DistrictCount = $District->count();
                        $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                        for ($i = 0; $i < $DistrictCount; $i++) {
                            $AllAppeal = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                            $CountAllApplication = $AllAppeal->count();
                            array_push($Total_Appeal, $CountAllApplication);
                            $AllPending = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 1)->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                            $AllAppealVerified = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 2)->get();
                            $CountAllVerified = $AllAppealVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllAppealReject = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->where('application_data.division', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '2')
                                ->where('appeals.school_id', '!=', '1')
                                ->where('addresses.division', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 3)->get();
                            $CountAllReject = $AllAppealReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                        }
                        $Arr_District_Filter = [
                            'DistrictCount' => $DistrictCount,
                            'District' => $District,
                            'Total_Appeal' => $Total_Appeal,
                            'Total_Verification' => $Total_Verification,
                            'Total_Rejection' => $Total_Rejection,
                            'Total_Pending' => $Total_Pending
                        ];
                        $wordCount = [
                            'AllDistrict' => $AllDistrict,
                            'allappeals' => $all_appeals,
                            'pendingappeals' => $pending_appeals,
                            'acceptedappeals' => $accepted_appeals,
                            'rejectedAppeal' => $rejected_appeals,
                            'Arr_District_Filter' => $Arr_District_Filter
                        ];
                    }
                    if (Auth::user()->user_role == 'faa') {
                        $UserDivision = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.division')
                            ->get();
                        $User_Division = $UserDivision[0]->division;
                        $AllDistrict = DB::table('appeals')->leftjoin('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->leftjoin('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->select(
                                'application_data.school_name',
                                'addresses.block',
                                'addresses.district',
                                'appeals.created_at',
                                'appeals.appeal_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block_name',
                                'appeals.appeal_status AS status_name'
                            )
                            ->whereIn('appeals.appeal_status', [1, 2, 3])
                            ->orderBy('appeals.created_at', 'DESC')->get();
                        // return $AllDistrict;
                        $CountAllDistrict = $AllDistrict->count();
                        $all_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->whereIn('appeals.appeal_status', [1, 2, 3])->count();
                        $pending_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->where('appeals.appeal_status', '1')->count();
                        $accepted_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->where('appeals.appeal_status', '2')->count();
                        $rejected_appeals = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                            ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                            ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                            ->where('application_data.division', $User_Division)
                            ->where('appeals.appellate_authority_type', '1')
                            ->where('addresses.division', '=', $User_Division)
                            ->where('appeals.appeal_status', '3')->count();
                        $District = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_Division)->get();
                        // return $District;
                        $DistrictCount = $District->count();
                        $Total_Appeal = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                        for ($i = 0; $i < $DistrictCount; $i++) {
                            $AllAppeal = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->whereIn('appeals.appeal_status', [1, 2, 3])->get();
                            $CountAllApplication = $AllAppeal->count();
                            array_push($Total_Appeal, $CountAllApplication);
                            $AllPending = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 1)->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                            $AllAppealVerified = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 2)->get();
                            $CountAllVerified = $AllAppealVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllAppealReject = DB::table('appeals')->leftjoin('private_schools', 'private_schools.id', '=', 'appeals.school_id')
                                ->join('addresses', 'addresses.school_id', '=', 'appeals.school_id')
                                ->join('application_data', 'application_data.school_id', '=', 'appeals.school_id')
                                ->where('application_data.district', $District[$i]->id)
                                ->where('appeals.appellate_authority_type', '1')
                                ->where('addresses.district', '=', $District[$i]->id)
                                ->where('appeals.status', '1')
                                ->where('appeals.appeal_status', 3)->get();
                            $CountAllReject = $AllAppealReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                        }
                        $Arr_District_Filter = [
                            'DistrictCount' => $DistrictCount,
                            'District' => $District,
                            'Total_Appeal' => $Total_Appeal,
                            'Total_Verification' => $Total_Verification,
                            'Total_Rejection' => $Total_Rejection,
                            'Total_Pending' => $Total_Pending
                        ];
                        $wordCount = [
                            'AllDistrict' => $AllDistrict,
                            'allappeals' => $all_appeals,
                            'pendingappeals' => $pending_appeals,
                            'acceptedappeals' => $accepted_appeals,
                            'rejectedAppeal' => $rejected_appeals,
                            'Arr_District_Filter' => $Arr_District_Filter
                        ];
                    }
                    if (Auth::user()->user_role == 'dse') {
                        $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending =  array();
                        $UserDistrict = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.district')
                            ->get();
                        $User_District = $UserDistrict[0]->district;
                        $Block = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_District)->get();
                        $BlockCount = $Block->count();
                        $AllDistrict = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->limit($BlockCount - 1)->get();
                        $Alldata = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', '>', '1')->get();
                        $wordCountAll = $Alldata->count();
                        $AlldataByDSC = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->whereIn('application_status', [2, 3])->get();
                        $wordCountByDSC = $AlldataByDSC->count();
                        $AlldataAccept = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', 4)->get();
                        $wordCountAcpt = $AlldataAccept->count();
                        $AlldataReject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', 8)->get();
                        $wordCountR = $AlldataReject->count();
                        for ($i = 0; $i < $BlockCount; $i++) {
                            $AllApplication = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', '>', '1')->get();
                            $CountAllApplication = $AllApplication->count();
                            array_push($Total_Application, $CountAllApplication);
                            $AllReject = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 8)->get();
                            $CountAllReject = $AllReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                            $AllVerified = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 4)->get();
                            $CountAllVerified = $AllVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllPending = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->whereIn('application_status', [2, 3])->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                        }
                        $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                        // return $Arr_Block_Filter;
                        $All_Meeting_Data = DB::table('application_data')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                            ->select(
                                'application_data.school_name',
                                'application_data.created_at',
                                'application_data.block',
                                'application_data.district',
                                'application_data.application_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                'status.status_name AS status_name',
                                'application_meetings.meeting_date'
                            )
                            ->where('application_data.application_status', '5')
                            ->where('application_data.district', $User_District)
                            ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                            ->where('application_data.status', '1')->limit($BlockCount - 1)
                            ->get();
                        $wordCount = [
                            'All'                 => $wordCountAll,
                            'Active'              => $wordCountA,
                            'VerifiedByDSC'       => $wordCountByDSC,
                            'Reject'              => $wordCountR,
                            'Pending'              => $wordCountP,
                            'Accept'              => $wordCountAcpt,
                            'AllDistrict'         => $AllDistrict,
                            'Arr_Block_Filter'    => $Arr_Block_Filter,
                            'All_Meeting_Data' => $All_Meeting_Data
                        ];
                    }
                    if (Auth::user()->user_role == 'dc') {
                        $UserDistrict = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.district')
                            ->get();
                        $User_District = $UserDistrict[0]->district;
                        $Block = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_District)
                            ->get();
                        $Total_Application = $Total_Verification = $Total_Rejection = $Total_Pending = array();
                        $BlockCount = $Block->count();
                        // return $BlockCount;
                        $Alldata = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->limit($BlockCount - 1)
                            ->where('application_status', '>', '1')
                            ->orderBy('created_at', 'ASC')->get();
                        $wordCountAll = $Alldata->count();
                        $AllVerified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            // ->where('application_status', '4')->get();
                            ->where('application_status', 6)->get();
                        $AllVerifiedCount = $AllVerified->count();
                        $AllPending = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            // ->where('application_status', '4')->get();
                            ->where('application_status', 4)->get();
                        $AllPendingCount = $AllPending->count();
                        $Meeting = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', '5')->get();
                        $MeetingCount = $Meeting->count();
                        $Certified = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            ->where('application_status', '6')
                            ->limit($BlockCount - 1)
                            ->orderBy('created_at', 'DESC')->get();
                        $CertifiedCount = $Certified->count();
                        $Reject = DB::table('application_data')
                            ->where('district', $User_District)
                            ->where('status', '1')
                            // ->where('application_status', '7')->get();
                            ->where('application_status', 7)->get();
                        $RejectCount = $Reject->count();
                        $All_Meeting_Data = DB::table('application_data')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->leftjoin('application_meetings', 'application_meetings.application_id', '=', 'application_data.id')
                            ->select(
                                'application_data.school_name',
                                'application_data.created_at',
                                'application_data.block',
                                'application_data.district',
                                'application_data.application_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                'status.status_name AS status_name',
                                'application_meetings.meeting_date'
                            )
                            ->where('application_data.application_status', '5')
                            ->where('application_data.district', $User_District)
                            ->whereDate('application_meetings.meeting_date', '>', Carbon::now())
                            ->where('application_data.status', '1')->limit($BlockCount - 1)
                            ->get();
                        for ($i = 0; $i < $BlockCount; $i++) {
                            $AllApplication = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', '>', '1')->get();
                            $CountAllApplication = $AllApplication->count();
                            array_push($Total_Application, $CountAllApplication);
                            $AllReject = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 7)->get();
                            // ->where('application_status', '7')->get();
                            $CountAllReject = $AllReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                            $AllVerified = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', 6)->get();
                            // ->where('application_status', '6')->get();
                            $CountAllVerified = $AllVerified->count();
                            array_push($Total_Verification, $CountAllVerified);
                            $AllPending = DB::table('application_data')
                                ->where('district', $User_District)
                                ->where('status', '1')
                                ->where('block', $Block[$i]->id)
                                ->where('application_status', '4')->get();
                            // ->whereIn('application_status', [2, 3, 4, 9, 12])->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                        }
                        $Arr_Block_Filter = ['BlockCount' => $BlockCount, 'Block' => $Block, 'Total_Application' => $Total_Application, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection, 'Total_Pending' => $Total_Pending];
                        // return($Arr_Block_Filter);
                        $wordCount = [
                            'All'           => $wordCountAll,
                            'AllDistrict' => $Alldata,
                            'AllVerifiedCount' => $AllVerifiedCount,
                            'AllPendingCount' => $AllPendingCount,
                            'MeetingCount' => $MeetingCount,
                            'CertifiedCount' => $CertifiedCount,
                            'RejectCount' => $RejectCount,
                            'AllCertified' => $Certified,
                            'Arr_Block_Filter' => $Arr_Block_Filter,
                            'All_Meeting_Data' => $All_Meeting_Data
                        ];
                    }
                    if (Auth::user()->user_role == 'state') {
                        $Total_Pending  = $Total_Approved = $Total_Application = $Total_Verification = $Total_Rejection = array();
                        $UserDivision = DB::table('addresses')
                            ->where('addresses.user_id', Auth::user()->id)
                            ->select('addresses.division')
                            ->get();
                        $User_Division = $UserDivision[0]->division;
                        $Division = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.value_set_id', '1')
                            ->get();
                        $DivisionCount = $Division->count();
                        $Alldata = DB::table('application_data')->where('status', '1')->where('application_data.application_status', '>', 1)->get();
                        $wordCountAll = $Alldata->count();
                        $AcceptedByAllUsers = DB::table('application_data')->where('status', '1')->whereIn('application_status', [4, 6, 10, 13])->get();
                        $wordCountAcceptedByAllUsers = $AcceptedByAllUsers->count();
                        $AlldataAccept = DB::table('application_data')->where('status', '1')->where('application_status', '6')->get();
                        $wordCountAcpt = $AlldataAccept->count();
                        $AlldataReject = DB::table('application_data')->where('status', '1')->whereIn('application_status', [7, 8, 11, 14])->get();
                        $wordCountR = $AlldataReject->count();
                        $AllDetails = DB::table('application_data')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->select(
                                'application_data.school_name',
                                'application_data.created_at',
                                'application_data.block',
                                'application_data.district',
                                'application_data.application_status',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                'status.status_name AS status_name'
                            )
                            ->where('application_data.status', '1')
                            ->where('application_data.application_status', '>', 1)
                            ->limit($DivisionCount)
                            ->orderBy('created_at', 'desc')->get();
                        $AllCertified = DB::table('application_data')->where('status', '1')->where('division', $User_Division)->where('application_status', '6')->get();
                        $CountAllCertified = $AllCertified->count();
                        for ($i = 0; $i < $DivisionCount; $i++) {
                            $AllApplication = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_data.application_status', '>', 1)->get();
                            $CountAllApplication = $AllApplication->count();
                            array_push($Total_Application, $CountAllApplication);
                            $AllReject = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->whereIn('application_status', [7, 8, 11, 14])->get();
                            $CountAllReject = $AllReject->count();
                            array_push($Total_Rejection, $CountAllReject);
                            $AllPending = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->wherein('application_status', [2, 3, 4, 9, 12])->get();
                            $CountAllPending = $AllPending->count();
                            array_push($Total_Pending, $CountAllPending);
                            $AllApproved = DB::table('application_data')->where('status', '1')->where('division', $Division[$i]->id)->where('application_status', 6)->get();
                            $CountAllApproved = $AllApproved->count();
                            array_push($Total_Approved, $CountAllApproved);
                        }
                        $Arr_Block_Filter = ['BlockCount' => $DivisionCount, 'Block' => $Division, 'Total_Application' => $Total_Application, 'Total_Pending' => $Total_Pending, 'Total_Approved' => $Total_Approved, 'Total_Verification' => $Total_Verification, 'Total_Rejection' => $Total_Rejection];
                        $wordCount = [
                            'All' => $wordCountAll,
                            'Active' => $wordCountA,
                            'VerifiedByAllUser' => $wordCountAcceptedByAllUsers,
                            'Reject' => $wordCountR,
                            'Pending' => $wordCountP,
                            'Accept' => $wordCountAcpt,
                            'AllCertified' => $AllCertified,
                            'AllDetails' => $AllDetails,
                            'Arr_Block_Filter' => $Arr_Block_Filter
                        ];
                    }
                    if (Auth::user()->user_role == 'school') {
                        // return 'true';
                        $Accepted_Fields = 0;
                        $Rejected_Fields = 0;
                        $pending_Fields = 0;
                        $alltable_col = DB::getSchemaBuilder()->getColumnListing('application_data_verifications');
                        foreach ($alltable_col as $key => $value) {
                            if ($value != "id" && $value != "apllication_id" && $value != "application_data_id" && $value != "school_id" && $value != "total_vrfy_field" && $value != "count_verified_field" && $value != "created_at" && $value != "updated_at" && $value != "created_by_vrfy" && $value != "updated_by_vrfy") {
                                if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                    $Accepted_Fields++;
                                }
                                if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                    $Rejected_Fields++;
                                }
                                if (DB::table('application_data_verifications')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                    $pending_Fields++;
                                }
                            }
                        }
                        $alltable_ext_col = DB::getSchemaBuilder()->getColumnListing('application_data_verification_ext_1s');
                        // echo $alltable_col;
                        foreach ($alltable_ext_col as $key => $value) {
                            if ($value != "id" && $value != "application_data_id" && $value != "application_form_id" && $value != "school_id"  && $value != "created_at" && $value != "updated_at") {
                                if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 2)->exists()) {
                                    $Accepted_Fields++;
                                }
                                if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 3)->exists()) {
                                    $Rejected_Fields++;
                                }
                                if (DB::table('application_data_verification_ext_1s')->where('school_id', Auth::user()->school_id)->where($value, 1)->exists()) {
                                    $pending_Fields++;
                                }
                            }
                        }
                        $UserDistrict = DB::table('private_schools')
                            ->where('id', Auth::user()->school_id)
                            ->value('district');
                        $User_District = $UserDistrict;
                        $Block = Fnd_value::select(
                            'fnd_values.id',
                            'fnd_values.display_value'
                        )->where('fnd_values.parent_value_id', '=', $User_District)
                            ->get();
                        $Application_Status = application_data::where('application_data.school_id', Auth::user()->school_id)
                            ->value('application_data.application_status');
                        // $acceptedted_feild = DB::table('application_data_verifications')
                        // ->select( DB::raw('count(school_name_vrfy,udise_code_vrfy) as accepted_count, school_name_vrfy') )
                        // ->where('application_data_verifications.school_id', Auth::user()->school_id)
                        // ->groupBy('school_name_vrfy')
                        // ->get();
                        // return $acceptedted_feild;
                        $Application_Tracking = DB::table('application_trackings')
                            ->Join('application_statuses', 'application_trackings.application_status_id', '=', 'application_statuses.id')
                            ->select(
                                'application_trackings.application_id',
                                'application_trackings.school_id',
                                'application_trackings.application_status_id',
                                'application_trackings.created_at',
                                'application_trackings.created_by',
                                'application_statuses.status_name AS status_name'
                            )
                            ->where('application_trackings.school_id', Auth::user()->school_id)
                            ->get();
                        $CountApplication_Tracking = $Application_Tracking->count();
                        $feedback = DB::table('application_data')
                            ->select(
                                'application_data.id',
                                'application_data.created_by',
                                'application_data.school_name',
                                'application_data.status',
                                'application_data.feedback_file_upload',
                                'application_data.feedback_from_dsc_to_school',
                                'application_data.verification_date'
                            )
                            ->where('application_data.school_id', Auth::user()->school_id)->where('application_data.status', 1)->get();
                        $All_Meeting_Data_school = DB::table('application_meetings')
                            ->Join('application_data', 'application_data.id', '=', 'application_meetings.application_id')
                            ->Join('fnd_values as fnd_district', 'application_data.district', '=', 'fnd_district.id')
                            ->Join('fnd_values as fnd_block', 'application_data.block', '=', 'fnd_block.id')
                            ->Join('users', 'users.id', '=', 'application_meetings.created_by')
                            // ->Join('application_statuses as status', 'status.id', '=', 'application_data.application_status')
                            ->select(
                                'application_meetings.meeting_date',
                                'application_meetings.meeting_remarks',
                                'application_data.school_name',
                                'users.name',
                                'application_meetings.created_at',
                                'application_meetings.meeting_text',
                                'application_meetings.meeting_remarks',
                                'application_meetings.meeting_documents',
                                'fnd_district.display_value AS disrict',
                                'fnd_block.display_value AS block',
                                // 'status.status_name AS status_name'
                            )
                            // ->where('application_data.application_status', '5')
                            ->where('application_meetings.school_id', Auth::user()->school_id)
                            ->get();
                        foreach ($All_Meeting_Data_school as  $Meeting_Data_school) {
                            $Meeting_Data_school->meeting_documents = json_decode($Meeting_Data_school->meeting_documents);
                        }
                        // if ($All_Meeting_Data_school->meeting_documents != null)
                        // $All_Meeting_Data_school->meeting_documents = json_decode($All_Meeting_Data_school->meeting_documents, true);
                        if ($Application_Status == 1) {
                            $Accepted_Fields = 0;
                            $Rejected_Fields = 0;
                            $pending_Fields = 0;
                        }
                        $wordCount = [
                            'feedback' => $feedback,
                            'Application_Tracking' => $Application_Tracking,
                            'Accepted_Fields' => $Accepted_Fields,
                            'Rejected_Fields' => $Rejected_Fields,
                            'pending_Fields' => $pending_Fields,
                            'All_Meeting_Data_school' => $All_Meeting_Data_school,
                            'Application_Status' => $Application_Status
                        ];
                    }
                    // return [Auth::user()];
                    return view('single_window_school_register')->with('data', $user);
                }
            }
        }
    }
}
