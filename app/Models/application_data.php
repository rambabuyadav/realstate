<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class application_data extends Model
{
    use HasFactory;
    protected $table = 'application_data';
    public $timestamps = false;
}
