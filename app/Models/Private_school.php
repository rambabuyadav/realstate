<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Private_school extends Model
{
    use HasFactory;
    protected $guarded = [''];
}
