<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class application_teacher_data extends Model
{
    use HasFactory;
    protected $table = 'application_teacher_data';
    public $timestamps = false;
}
