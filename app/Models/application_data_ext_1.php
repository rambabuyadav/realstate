<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class application_data_ext_1 extends Model
{
    use HasFactory;
    protected $table = 'application_data_ext_1';
    public $timestamps=false;
}
