<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SessionToken extends Model
{
    use HasFactory;
    protected $table = 'session_tokens';

     protected $fillable = [
        'id',
        'user_id',
        'token',
    ];
}
