<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rte_applicant_students extends Model
{
    use HasFactory;
     protected $guarded = [''];
}
