<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SchoolCertificateController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::get('get-location-from-ip',function(){
//     $ip= \Request::ip();
//     $data = \Location::get($ip);
//     dd($data);
// });

Route::group(['middleware' => ['api']], function () {
    Route::resource('school_register', 'Api\AppSchoolRegisterController');
});
 
Route::get('/generateOTP/mobile/{phone}', 'Api\AppSchoolRegisterController@testsms');
Route::resource('forgot_password', 'Api\AppForgotPasswordController');


Route::post('images_upload', 'Api\UploadController@store');
Route::get('image/{filename}','Api\AppPhotoController@image');


Route::get('applogout', 'Api\AppLogoutController@logout')->middleware('auth:api');
Route::post('applogin', 'Api\AppLoginController@login');
Route::get('student_application', 'Api\AppStudentRegisterController@getStudentDetails');

//Route::post('/aplogin', 'Api\ApplogController@login');\
//Route::post('app-application', 'Api\AppApplicationController@addData');
Route::post('save_application', 'Api\AppApplicationController@AddEditApplicationData');
Route::get('show_data/{id}','Api\AppApplicationController@showAllData');
Route::post('send_message_to_school', 'Api\AppApplicationController@SendMessageToSchool');
Route::post('send_message_to_dc_dse', 'Api\AppApplicationController@SendMessageToDCDSE');
Route::get('application', 'Api\AppApplicationController@getApplicationData');
// Route::get('saveasdraft', 'Api\AppApplicationController@AddEditApplicationData');
Route::get('app_schoolList', 'Api\AppApplicationController@schoolList');
Route::get('app_appeal', 'Api\AppAppealController@show');
Route::post('application_total_fields_verify', 'Api\AppApplicationController@applicationFieldsVarified');
Route::get('/AcceptedField/{status}', 'Api\AppApplicationController@getApplicationfeildData');
Route::get('/downloadStudentPDF/{id}', 'Api\AppApplicationController@downloadStudentPDF');
Route::post('/application-verify', 'Api\AppApplicationController@ApplicationList'); // for advance search and export in excel.
Route::post('/accept_application', 'Api\AppApplicationController@ApplicationAccept');
Route::post('/reject_application', 'Api\AppApplicationController@ApplicationReject');
Route::post('/acceptSchoolFields', 'Api\AppApplicationController@acceptSchoolDetail');
Route::post('/rejectSchoolFields', 'Api\AppApplicationController@rejectSchoolDetail');
Route::post('/application_meeting', 'Api\AppApplicationController@ApplicationMeeting'); // meeting schedule ...
Route::post('/update-application-file', 'Api\AppApplicationController@UpdateApplicationFile'); 
Route::post('/remove-application-file', 'Api\AppApplicationController@RemoveApplicationFile'); 
Route::post('/insert-application-file', 'Api\AppApplicationController@InsertApplicationFile'); 



Route::get('update-message-status/{application_id}', 'Api\AppApplicationController@UpdateMessageStatus');
Route::post('delete-teacher/{teacher_id}','Api\AppApplicationController@DeleteTeacher');
//---------------------Certificate Api-----------------------------------------------------------------------------
Route::post('school_certificate', 'Api\AppSchoolCertificateController@schoolCertificate'); // school certificate download in pdf

//-------------------Dashboard Api---------------------------------------------------------------------------------
Route::post('appdashboard', 'Api\AppDashboardController@index');
Route::get('app_UserProfile/{id}', 'Api\AppDashboardController@EditDataShow');
Route::post('Update_Profile', 'Api\AppDashboardController@EditData');
Route::post('Update_Password', 'Api\AppDashboardController@ChangePassword');

//-----------------------------------------------------------------------------------------------------------------
// Route::get('appAcceptedField/{id}', 'Api\AppApplicationController@showAllData');
Route::post('userlist', 'Api\AppUserController@show');

Route::post('/add_user', 'Api\AppUserController@insert');
Route::post('/edit_user/{id}', 'Api\AppUserController@edit');
Route::post('/Update_user/{id}', 'Api\AppUserController@updateData');
Route::delete('/delete_user/{id}', 'Api\AppUserController@deleteData');
//-----------------------Appeal API-------------------------------------------------------------------------------
Route::post('/appeal-list', 'Api\AppAppealController@listAppeals');
Route::post('/addAppeal', 'Api\AppAppealController@insert');
Route::post('/editAppeal/{id}', 'Api\AppAppealController@edit');
Route::post('/deleteAppeal/{id}', 'Api\AppAppealController@deleteData');
Route::post('/editAppeal', 'Api\AppAppealController@updateData');
Route::post('/appeal-reply', 'Api\AppAppealController@replyAppeal');
Route::post('/acceptRemarkAppeal', 'Api\AppAppealController@acceptRemarkAppeal');
Route::post('/rejectRemarkAppeal', 'Api\AppAppealController@rejectRemarkAppeal');
Route::get('update-appeal-status/{application_id}', 'Api\AppAppealController@UpdateAppealStatus');
Route::post('add-appeal-authority', 'Api\AppAppealController@addAppealAuthority');

//-----------------------Report API-------------------------------------------------------------------------------
Route::get('reportAllApplications', 'Api\AppReportController@reportAllApplications');
Route::get('reportDelayedApplications','Api\AppReportController@reportDelayedApplications');
Route::get('reportRddeAppeals','Api\AppReportController@reportRddeAppeals');
Route::get('reportRegisteredSchool','Api\AppReportController@reportRegisteredSchool');
Route::get('reportPendingApplications','Api\AppReportController@reportPendingApplications');

Route::get('reportRejectedApplications','Api\AppReportController@reportRejectedApplications');
Route::get('reportDpeAppeals','Api\AppReportController@reportDpeAppeals');
Route::get('reportApprovedApplications','Api\AppReportController@reportApprovedApplications');
Route::get('reportVerifiedApplications','Api\AppReportController@reportVerifiedApplications');
Route::get('reportDcMeetings','Api\AppReportController@reportDcMeetings');
Route::get('report_payments','Api\AppReportController@reportPayments');
//------------------Advancesearch API-----------------------------------------------------------------------------
Route::get('/advSearch','Api\AppReportController@advSearch');
Route::get('/advSearchAllReport','Api\AppReportController@advSearchAllReport');
Route::get('/advSearchRDDE','Api\AppReportController@advSearchRDDE');
//------------------Division District Block Api-------------------------------------------------------------------
Route::post('division','Api\AppDivisionDistrictBlockController@divisionList');
Route::post('district','Api\AppDivisionDistrictBlockController@districtList');
Route::post('block','Api\AppDivisionDistrictBlockController@blockList');
//--------------------Settings Api--------------------------------------------------------------------------------
Route::post('divisionList','Api\AppFndSettingsController@divisionList');
Route::post('addDivision','Api\AppFndSettingsController@addDivision');
Route::post('saveDivision','Api\AppFndSettingsController@saveDivisionData');
Route::post('editDivision/{id}','Api\AppFndSettingsController@editDivision');
Route::post('updateDivision/{id}','Api\AppFndSettingsController@updateDivisionData');
Route::post('deleteDivision/{id}','Api\AppFndSettingsController@deleteDivisionData');


Route::post('districtList','Api\AppFndSettingsController@districtList');
Route::post('addDistrict','Api\AppFndSettingsController@addDistrict');
Route::post('saveDistrict','Api\AppFndSettingsController@saveDistrictData');
Route::post('editDistrict/{id}','Api\AppFndSettingsController@editDistrict');
Route::post('updateDistrict/{id}','Api\AppFndSettingsController@updateDistrictData');
Route::post('deleteDistrict/{id}','Api\AppFndSettingsController@deleteDistrictData');

Route::post('blockList','Api\AppFndSettingsController@blockList');
Route::post('addBlock','Api\AppFndSettingsController@addBlock');
Route::post('saveBlock','Api\AppFndSettingsController@saveBlockData');
Route::post('editBlock/{id}','Api\AppFndSettingsController@editBlock');
Route::post('updateBlock/{id}','Api\AppFndSettingsController@updateBlockData');
Route::post('deleteBlock/{id}','Api\AppFndSettingsController@deleteBlockData');

Route::post('addEditFndsettings','Api\AppFndSettingsController@addEditFndsettings');
Route::post('saveFndsettings','Api\AppFndSettingsController@saveFndsettings');
//------------------------------------About Us ,FAQs,  DSE, DEO, Guideline, Slider----------------------------------------------------
// Route::get('About_Us','Api\AppIndexPageController@View_About_Us');
Route::get('About_Us','Api\AppIndexPageController@ViewAboutUs');
Route::post('update-about-us','Api\AppIndexPageController@UpdateAboutUs');
Route::post('contact-us','Api\AppIndexPageController@ContactUs');
Route::post('update-contact-us','Api\AppIndexPageController@UpdateContactUs');
Route::post('FAQs','Api\AppIndexPageController@ViewFAQs');
Route::post('editFAQs/{id}','Api\AppIndexPageController@EditFAQs');
Route::post('updateFAQs','Api\AppIndexPageController@UpdateFAQ');
Route::post('insertFAQs','Api\AppIndexPageController@InsertFAQ');
Route::post('deleteFAQs/{id}','Api\AppIndexPageController@DeleteFAQ');
Route::post('dselist','Api\AppIndexPageController@DSEList');
Route::post('editdse/{id}','Api\AppIndexPageController@Edit_DSE');
Route::post('updatedse','Api\AppIndexPageController@Update_DSE');
Route::post('insertdse','Api\AppIndexPageController@Insert_DSE');
Route::post('deletedse/{id}','Api\AppIndexPageController@DeleteDSE');
Route::post('deolist','Api\AppIndexPageController@DEOList');
Route::post('editdeo/{id}','Api\AppIndexPageController@Edit_DEO');
Route::post('updatedeo','Api\AppIndexPageController@Update_DEO');
Route::post('insertdeo','Api\AppIndexPageController@Insert_DEO');
Route::post('deletedeo/{id}','Api\AppIndexPageController@DeleteDEO');
Route::post('GuideLine','Api\AppIndexPageController@GuideLineList');
Route::post('editGuideLine/{id}','Api\AppIndexPageController@EditGuideLine');
Route::post('Add-GuideLine','Api\AppIndexPageController@AddGuideLine');
Route::post('Insert-GuideLine','Api\AppIndexPageController@InsertGuideLine');
Route::post('Update-GuideLine','Api\AppIndexPageController@UpdateGuideLine');
Route::post('delete-GuideLine/{id}','Api\AppIndexPageController@DeleteGuideLine');

Route::post('Slider-List','Api\AppIndexPageController@SliderList');
Route::post('Get-Slider-List/{id}','Api\AppIndexPageController@EditSlider');
Route::post('Insert-Slider','Api\AppIndexPageController@InsertSlider');
Route::post('Update-Slider','Api\AppIndexPageController@UpdateSlider');
Route::post('delete-Slider/{id}','Api\AppIndexPageController@DeleteSlider');
Route::post('Honble-Person-List','Api\AppIndexPageController@HonblePersonList');
Route::post('Get-HonblePerson/{id}','Api\AppIndexPageController@EditHonblePerson');
Route::post('Insert-Honble-Person','Api\AppIndexPageController@InsertHonblePerson');
Route::post('Update-Honble-Person','Api\AppIndexPageController@UpdateHonblePerson');
Route::post('delete-Honble-Person/{id}','Api\AppIndexPageController@DeleteHonblePerson');

//------------------------------------Single window Login & payment response store ----------------------------
Route::post('single-window-login-check','Api\SingleWindowUserController@checkLogin');

//-----------------------------------Payment API----------------------------------------------------------------

Route::post('Payment_list','Api\AppPaymentController@listPayment');

// Route::post('login', 'PassportController@login');
// Route::post('register', 'PassportController@register');
 
// Route::middleware('auth:api')->group(function () {
//     Route::get('user', 'PassportController@details');
 
//     Route::resource('products', 'ProductController');
// });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
