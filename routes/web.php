	<?php

	use Illuminate\Support\Facades\Route;
	use App\Http\Controllers\Controller;
	use App\Http\Controllers\SchoolRegisterController;
	use App\Http\Controllers\DashboardController;
	use App\Http\Controllers\application_formController;
	use App\Http\Controllers\UserController;
	use App\Http\Controllers\AppealController;
	use App\Http\Controllers\ReportController;
	use App\Http\Controllers\feedbackController;
	use App\Http\Controllers\FndSettingsController;
	use App\Http\Controllers\SchoolCertificateController;
	use App\Http\Controllers\PaymentController;
	use App\Http\Controllers\StudentRegisterController;
	use App\Http\Controllers\ForgotPasswordController;
	use App\Http\Controllers\IndexPageController;
    use App\Http\Controllers\RteFtpController;
    use App\Http\Controllers\CmsController;

/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/

	// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
	// 	return view('dashboard');
	// })->name('dashboard');
	Route::get('/dashboard',function(){
		return view('dashboard');
	})->name('dashboard');
	

	##########################Jcert route start###################
	Route::get('index', [CmsController::class, 'index']);
	##################### About us ###########################
	Route::get('about-us', [CmsController::class, 'aboutUs']);
	##################### Organisation ###########################
	Route::get('organisation/management', [CmsController::class, 'managementPage']);
	Route::get('organisation/senior-functionaries', [CmsController::class, 'seniorFunctionaries']);
	Route::get('organisation/disclosure-under-rti', [CmsController::class, 'disclosureUnderRti']);
	Route::get('organisation/programme-advisory-committee', [CmsController::class, 'programmeAdvisoryCommittee']);
	
    #####################Constituents and Department##########################
	Route::get('constituents/national-institute-education', [CmsController::class, 'nationalInstituteEducation']);
	Route::get('constituents/regional-institutes-education', [CmsController::class, 'regionalInstitutesEducation']);
	Route::get('constituents/central-institute-of-educational-technology', [CmsController::class, 'centralInstituteofEeducationalTechnology']);
	Route::get('constituents/pss-vocational-education', [CmsController::class, 'pssVocationalEducation']);
	
	#####################programmes##############################
	Route::get('programmes/national-talent-examination', [CmsController::class, 'nationalTalentExamination']);
	Route::get('programmes/all-india-school-education-survey', [CmsController::class, 'allindiaSchoolEducationSurvey']);
	Route::get('programmes/teacher-innovation-awards', [CmsController::class, 'teacherInnovationAwards']);
	Route::get('programmes/jn-national-science-exhibition', [CmsController::class, 'jnNationalScienceExhibition']);
	Route::get('programmes/media-programmes', [CmsController::class, 'mediaProgrammes']);
	Route::get('programmes/research-grants-eric', [CmsController::class, 'researchGrantsRric']);
	
	#####################Gallery###################################
	Route::get('gallery/international-yoga', [CmsController::class, 'internationalYoga']);
	Route::get('gallery/kala-utsav', [CmsController::class, 'kalaUtsav']);
	Route::get('gallery/foundationDay', [CmsController::class, 'foundationDay']);
	Route::get('gallery/eRaksha', [CmsController::class, 'eRaksha']);
	Route::get('gallery/healthandwell', [CmsController::class, 'healthandWell']);
	
	########################publication ############################
	Route::get('publication/list-publication', [CmsController::class, 'listPublication']);
	Route::get('publication/ePub', [CmsController::class, 'ePubblication']);
	Route::get('publication/Flipbook', [CmsController::class, 'Flipbook']);
	Route::get('publication/textbook', [CmsController::class, 'textbook']);
	Route::get('publication/state-uts-eBook', [CmsController::class, 'stateUtseBook']);
	Route::get('publication/vocational-education', [CmsController::class, 'vocationalEducation']);

	#######################Announcement #############################
	Route::get('announcement/vacancies', [CmsController::class, 'vacancies']);
	Route::get('announcement/tenders', [CmsController::class, 'tenders']);
	Route::get('announcement/notices', [CmsController::class, 'notices']);
	Route::get('announcement/other_announcements', [CmsController::class, 'otherAnnouncements']);
	Route::get('announcement/seminar', [CmsController::class, 'seminar']);
	
	#############################contacts #############################
	Route::get('contacts/persons-contact', [CmsController::class, 'personContact']);
	Route::get('contacts/public-information-officers', [CmsController::class, 'publicInformationOfficers']);
	Route::get('contacts/telephone-directory', [CmsController::class, 'telephoneDirectory']);

    ##########################End Jcert route ###################

	################################JCERT settings routes#########
	Route::middleware(['auth:sanctum', 'verified', 'prevent-back-history', 'onlyOneUser'])->group(function () {
	Route::get('fndSettings/menus', [FndSettingsController::class, 'menuSettings']);
	Route::get('fndSettings/submenus', [FndSettingsController::class, 'submenuSettings']);

	Route::post('fndSettings/menus/store', [FndSettingsController::class, 'storeMenu']);
	Route::post('fndSettings/submenus/store', [FndSettingsController::class, 'storeSubMenu']);
	Route::get('fndSettings/menus/edit/{id}', [FndSettingsController::class, 'editMenu']);
	Route::get('fndSettings/menus/delete/{id}', [FndSettingsController::class, 'deleteMenu']);



	Route::get('about-us-edit', [FndSettingsController::class, 'aboutUsEdit']);
	Route::get('org-submenu', [FndSettingsController::class, 'aboutUsEdit']);


	Route::get('about-us-cms', [FndSettingsController::class, 'aboutUsEdit']);
	//********************* Organisation Submenu ************************//
	Route::get('organisation-submenu', [FndSettingsController::class, 'organisationSubMenu']);
	Route::get('management-edit', [FndSettingsController::class, 'managementEdit']);
	Route::get('senior-fun-edit', [FndSettingsController::class, 'seniorFunEdit']);
	Route::get('disclosure-rti-edit', [FndSettingsController::class, 'disclosureRtiEdit']);
	Route::get('programme-adv-edit', [FndSettingsController::class, 'progAdvEdit']);
	//********************* constituents Submenu ************************//
	Route::get('constituent-submenu', [FndSettingsController::class, 'constituentSubMenu']);
	Route::get('nie-edit', [FndSettingsController::class, 'nieEdit']);
	Route::get('rie-edit', [FndSettingsController::class, 'rieEdit']);
	Route::get('ciet-edit', [FndSettingsController::class, 'cietEdit']);
	Route::get('pcive-edit', [FndSettingsController::class, 'pciveEdit']);
	//********************* programme Submenu ************************//
	Route::get('programme-submenu', [FndSettingsController::class, 'programmeSubMenu']);
	Route::get('ntse-edit', [FndSettingsController::class, 'ntseEdit']);
	Route::get('aises-edit', [FndSettingsController::class, 'aisesEdit']);
	Route::get('tia-edit', [FndSettingsController::class, 'tiaEdit']);
	Route::get('jnnse-edit', [FndSettingsController::class, 'jnnseEdit']);
	Route::get('media-edit', [FndSettingsController::class, 'mediaEdit']);
	Route::get('research-grants-edit', [FndSettingsController::class, 'researchGrantsEdit']);

	Route::get('management-cms', [FndSettingsController::class, 'mangementCms']);

	//********************* Gallery Submenu ************************//
	Route::get('gallery-submenu', [FndSettingsController::class, 'gallerySubmenu']);
	Route::get('yoga-day-edit', [FndSettingsController::class, 'yogaDayEdit']);
	Route::get('kala-utsav-edit', [FndSettingsController::class, 'KalaUtsavEdit']);
	Route::get('foundation-day-edit', [FndSettingsController::class, 'foundationDayEdit']);
	Route::get('eraksha-edit', [FndSettingsController::class, 'erakshaEdit']);
	Route::get('health-wellness-edit', [FndSettingsController::class, 'healthWellnessEdit']);
	//********************* Publication Submenu ************************//
	Route::get('publication-submenu', [FndSettingsController::class, 'publicationSubmenu']);
	Route::get('publication-edit', [FndSettingsController::class, 'publicationEdit']);
	Route::get('epub-edit', [FndSettingsController::class, 'epubEdit']);
	Route::get('flipbook-edit', [FndSettingsController::class, 'flipbookEdit']);
	Route::get('pdf-edit', [FndSettingsController::class, 'pdfEdit']);
	Route::get('state-ebook-edit', [FndSettingsController::class, 'stateEbookEdit']);
	Route::get('vocational-edit', [FndSettingsController::class, 'vocationalEdit']);
	//********************* Announcement Submenu ************************//
	Route::get('announcement-submenu', [FndSettingsController::class, 'announcementSubmenu']);
	Route::get('vacancy-edit', [FndSettingsController::class, 'vacancyEdit']);
	Route::get('tender-edit', [FndSettingsController::class, 'tenderEdit']);
	Route::get('notice-edit', [FndSettingsController::class, 'noticeEdit']);
	Route::get('other-announcement-edit', [FndSettingsController::class, 'otherAnnouncementEdit']);
	Route::get('seminar-workshop-edit', [FndSettingsController::class, 'seminarWorkshopEdit']);
	//********************* Contact Submenu ************************//
	Route::get('contact-submenu', [FndSettingsController::class, 'contactSubmenu']);
	Route::get('person-contact-edit', [FndSettingsController::class, 'personContactEdit']);
	Route::get('public-info-edit', [FndSettingsController::class, 'publicInfoEdit']);
	Route::get('tel-directory-edit', [FndSettingsController::class, 'telDirectoryEdit']);
	
	########################## Manage Pages settings S####################		
	Route::get('fndSettings/pages', [FndSettingsController::class, 'pageIndexDev']);
	Route::get('fndSettings/pages/manage', [FndSettingsController::class, 'pageIndex']);
	Route::get('fndSettings/pages/addPageUrl', [FndSettingsController::class, 'addPageUrl']);
	Route::get('fndSettings/pages/editPageUrl/{id}', [FndSettingsController::class, 'editPageUrl']);
	Route::post('fndSettings/pages/insertPageUrl', [FndSettingsController::class, 'insertPageUrl']);
	Route::get('fndSettings/pages/listPageData/{id}', [FndSettingsController::class, 'listPageData']);
	Route::get('fndSettings/pages/addPageData/{id}', [FndSettingsController::class, 'addPageData']);
	Route::get('fndSettings/pages/editPageData/{id}', [FndSettingsController::class, 'editPageData']);
	Route::post('fndSettings/pages/insertPageData', [FndSettingsController::class, 'insertPageData']);
	Route::get('fndSettings/pages/getImage/{id}/{key}', [FndSettingsController::class, 'getImage']);
	Route::get('fndSettings/pages/PageContentList/{id}', [FndSettingsController::class, 'PageContentList']);
	Route::post('fndSettings/pages/updatePageContent', [FndSettingsController::class, 'updatePageContent']);
	//Route::get('fndSettings/pages/add', [FndSettingsController::class, 'addPage']);
	});
	##########################ORGANISATION SUB MENUS ROUTES####################
	Route::get('management-cms', [FndSettingsController::class, 'mangementCms']);
		
	Route::post('application-submit/response', [application_formController::class, 'applicationSubmitResponse']);

	Route::get('/', [IndexPageController::class, 'View_Home_Page']);
	Route::get('/View_Home_Page', [IndexPageController::class, 'View_Home_Page']);
	//Route::get('/about-us', [IndexPageController::class, 'View_About_Us']);
	Route::get('/faq', [IndexPageController::class, 'View_FAQs']);
	Route::get('/dse-contact', [IndexPageController::class, 'View_DSE']);
	Route::get('/deo-contact', [IndexPageController::class, 'View_DEO']);

	//Route::get('/dse-contact', function () { return view('home.dse-contact'); });
	//Route::get('/about-us', function () { return view('home.about-us'); });
	//Route::get('/faq', function () { return view('home.faq'); });
	// Route::get('check_email', [SchoolRegisterController::class, 'checkEmail']);
	Route::resource('school_register', SchoolRegisterController::class);
	Route::resource('student_register', StudentRegisterController::class);
	Route::resource('forgot_password', ForgotPasswordController::class);
	Route::get('generateOTP/mobile/{phone}', [SchoolRegisterController::class, 'testsms']);
	Route::get('block_unauth/block_list/{id}', [SchoolRegisterController::class, 'blockList']);
	// Route::get('forgot/password', [ForgotPasswordController::class, 'index']);
	// Route::post('save/forgotpassword', [ForgotPasswordController::class, 'saveForgotPassword']);

	Route::middleware(['auth:sanctum', 'verified', 'prevent-back-history', 'onlyOneUser'])->group(function () {


	#####################Group Chat Message #######################

	Route::post('message', 'App\Http\Controllers\HomeController@sendMessage');  // 
	Route::get('/message/{id}', 'App\Http\Controllers\HomeController@getMessage')->name('message'); 
	Route::get('/ShowMassage/{id}', 'App\Http\Controllers\HomeController@ShowMassage'); 
	Route::get('/messag/{id}', 'App\Http\Controllers\HomeController@getMessag')->name('message'); 
	Route::get('/subscribe', 'App\Http\Controllers\HomeController@subscribe');
	Route::get('/home', 'App\Http\Controllers\HomeController@index');
	Route::delete('/unFollow/{id}', 'App\Http\Controllers\HomeController@remove_user'); 
	/////////////////////  
	Route::get('/group/create', 'App\Http\Controllers\GroupController@create_form');
	Route::post('/group/create', 'App\Http\Controllers\GroupController@create');
	Route::get('/group/join', 'App\Http\Controllers\GroupController@join_form');
	Route::post('/group/join', 'App\Http\Controllers\GroupController@join');

	Route::get('/group/edit/{id}', 'App\Http\Controllers\GroupController@edit');

	Route::post('/group/update/{id}', 'App\Http\Controllers\GroupController@update');

	Route::delete('/group/delete/{id}', 'App\Http\Controllers\GroupController@deleteGroup');

	Route::get('/group/members_list/{id}', 'App\Http\Controllers\GroupController@members_list');

	Route::get('/remove_user/{id}/{user_id}', 'App\Http\Controllers\GroupController@remove_user');

	##############Add user###############
	Route::get('/group-user', 'App\Http\Controllers\HomeController@addUser');
	Route::post('/create-user', 'App\Http\Controllers\HomeController@createUser');
	Route::get('/users-list', 'App\Http\Controllers\HomeController@usersList');
	Route::get('/user/delete/{id}', 'App\Http\Controllers\HomeController@usersDelete');
	Route::get('/group/chat-disabled', 'App\Http\Controllers\HomeController@chatDisabled');

	#####################Group Chat Message #######################


	Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
	Route::post('/user-profile-edit', [DashboardController::class, 'EditDataShow']);
	Route::post('/User-Profile', [DashboardController::class, 'EditData']);
	Route::post('/update-password', [DashboardController::class, 'ChangePassword']); //Ansuman code  use to change password 
	Route::get('/AcceptedField/{status}', [application_formController::class, 'getApplicationfeildData']);

	// Route::get('VerifivationStatus/{id}', [application_formController::class, 'showAllData']);
	// Route::get('AcceptedField/{id}', [application_formController::class, 'showAllData']);
	// Route::get('RejectedField/{id}', [application_formController::class, 'showAllData']);
	// Route::get('PendingField/{id}', [application_formController::class, 'showAllData']);
	//Add User by State
	Route::get('/userlist', [UserController::class, 'show']);
	Route::get('/addUser', [UserController::class, 'index']);
	Route::post('/addUser', [UserController::class, 'insert']);
	Route::get('/edit-user/{id}', [UserController::class, 'edit']);
	Route::post('/deleteUser', [UserController::class, 'deleteData']);
	Route::post('/editUser', [UserController::class, 'updateData']);
	Route::post('/advSearch', [UserController::class, 'advSearch']);

	Route::get('/applicationEdit/{id}', [application_formController::class, 'EditAppForm']);
	Route::get('feedback', [feedbackController::class, 'addData']);
	Route::post('/feedback', [feedbackController::class, 'addData']);
	Route::get('feedbackShow', [feedbackController::class, 'feedbackList']);

	// Route::get('appeal', [AppealController::class, 'show']);
	Route::get('appeal-list', [AppealController::class, 'listAppeals']);
	Route::post('/appeal-list', [AppealController::class, 'listAppeals']); // for advance search and export appeals details.
	Route::get('/addAppeal', [AppealController::class, 'viewadd']);

	Route::post('/addAppeal', [AppealController::class, 'insert']);
	Route::get('/editAppeal/{id}', [AppealController::class, 'edit']);
	Route::get('/deleteAppeal/{id}', [AppealController::class, 'deleteData']);
	Route::post('/editAppeal', [AppealController::class, 'updateData']);
	Route::post('/appeal-reply', [AppealController::class, 'replyAppeal']);

	Route::post('/applicationEdit', [application_formController::class, 'editData']);
	Route::post('feedback_to_school', [application_formController::class, 'editDscFeedback']);
	Route::post('/application', [application_formController::class, 'addData']);
	Route::get('/application', [application_formController::class, 'getApplicationData']);
	Route::post('/addEdit/applicationData', [application_formController::class, 'AddEditApplicationData']);
	Route::post('/application_fields_verified', [application_formController::class, 'applicationFieldsVarified']);
	Route::get('school-certificate', [SchoolCertificateController::class, 'schoolCertificate']); // school certificate
	Route::post('school-certificate', [SchoolCertificateController::class, 'schoolCertificate']); // school certificate
	// Route::get('paySuccess', function () {
	// 	return view('paySuccess');
	// });
	Route::get('show-data/{id}', [application_formController::class, 'showAllData']);

	// Route::get('school', [DashboardController::class, 'SchoolList']);
	// Route::get('application_verify', [application_formController::class, 'ApplicationList']);
	Route::get('application-verify', [application_formController::class, 'ApplicationList']);
	Route::post('/aaplication-meeting', [application_formController::class, 'ApplicationMeeting']);
	Route::post('/application-accept', [application_formController::class, 'ApplicationAccept']);
	Route::post('/application-reject', [application_formController::class, 'ApplicationReject']);
	Route::post('/application-verify', [application_formController::class, 'ApplicationList']); // for advance search and export in excel.
	Route::post('send_message_to_school', [application_formController::class, 'SendMessageToSchool']);
	Route::post('send_message_to_dc_dse', [application_formController::class, 'SendMessageToDCDSE']);
	Route::get('payment-application', [application_formController::class, 'PaymemtApplication']);
	Route::get('DDO/{id}', [application_formController::class, 'DDO_List']);
	Route::post('application_payment', [application_formController::class, 'Application_Payment']);
	Route::post('application_challan', [application_formController::class, 'Application_Challan']);
	Route::post('/pending_payment_paid', [application_formController::class, 'Pending_Payment']);
	Route::post('/challan_verify_paid', [application_formController::class, 'Challan_Verify_Paid']);
	Route::post('/challan_paid_verify', [application_formController::class, 'Challan_Paid_Verify']);
	Route::get('/challan_form/{id}', [application_formController::class, 'AppChallanForm']);
	Route::get('/payment_form/{id}', [application_formController::class, 'AppPaymentForm']);
	
	// Reports routes start......
	Route::get('DGR', [ReportController::class, 'index']);
	Route::get('DGR/block/{id}', [ReportController::class, 'Block']);
	Route::get('DGR/village/{id}', [ReportController::class, 'Village']);
	Route::get('block/block_list/{id}', [ReportController::class, 'blockList']);
	Route::get('district/district_list/{id}', [ReportController::class, 'districtList']);
	Route::post('/all-report/{status_id}', [ReportController::class, 'reportAllApplicationswithStatus']);
	Route::get('/report', [ReportController::class, 'report']);
	Route::get('report-all-applications', [ReportController::class, 'reportAllApplications']);
	Route::post('/report-all-applications', [ReportController::class, 'reportAllApplications']); // for advance search and for export report
	Route::get('report-pending-applications', [ReportController::class, 'reportPendingApplications']);
	Route::post('/report-pending-applications', [ReportController::class, 'reportPendingApplications']); // for advance search and export in excel
	Route::get('report-approved-applications', [ReportController::class, 'reportApprovedApplications']);
	Route::post('/report-approved-applications', [ReportController::class, 'reportApprovedApplications']); // for advance search and excel export.
	Route::get('report-delayed-applications', [ReportController::class, 'reportDelayedApplications']);
	Route::post('/report-delayed-applications', [ReportController::class, 'reportDelayedApplications']); // for advance search and excel export.
	Route::get('reportRejectedApplications', [ReportController::class, 'reportRejectedApplications']);
	Route::post('/reportRejectedApplications', [ReportController::class, 'reportRejectedApplications']); // for advance search and export
	Route::get('report-dc-meetings', [ReportController::class, 'reportDcMeetings']);
	Route::post('/report-dc-meetings', [ReportController::class, 'reportDcMeetings']);  // for advance search and export
	Route::get('report-rdde-appeals', [ReportController::class, 'reportRddeAppeals']);
	Route::post('/report-rdde-appeals', [ReportController::class, 'reportRddeAppeals']); // for rdde report advance search and export excel
	Route::get('report-dpe-appeals', [ReportController::class, 'reportDpeAppeals']);
	Route::post('/report-dpe-appeals', [ReportController::class, 'reportDpeAppeals']);  // for advance search and export
	Route::get('report-registered-school', [ReportController::class, 'reportRegisteredSchool']);
	Route::post('report-registered-school', [ReportController::class, 'reportRegisteredSchool']);  // for advance search and export
	Route::get('report-payments', [ReportController::class, 'reportPayments']);
	Route::post('report-payments', [ReportController::class, 'reportPayments']);  // for advance search and export
	Route::get('report-certified-applications', [ReportController::class, 'reportCertifiedApplications']);
	Route::post('/report-certified-applications', [ReportController::class, 'reportCertifiedApplications']); // for certified report advance search and export excel
	Route::get('school-certificate', [SchoolCertificateController::class, 'schoolCertificate']); // school certificate
	Route::post('school-certificate', [SchoolCertificateController::class, 'schoolCertificate']); // school certificate download in pdf
	Route::get('school', [application_formController::class, 'schoolList']);
	Route::post('school', [application_formController::class, 'schoolList']);
	Route::get('/applicationfeilddata/{status}', [application_formController::class, 'getApplicationfeildData']);
	Route::post('/acceptSchoolFields', [application_formController::class, 'acceptSchoolDetail']);
	Route::post('/rejectSchoolFields', [application_formController::class, 'rejectSchoolDetail']);
	Route::post('/acceptRemark', [AppealController::class, 'acceptRemarkAppeal']);
	Route::post('/rejectRemark', [AppealController::class, 'rejectRemarkAppeal']);
	Route::get('/fndSettings', [FndSettingsController::class, 'index']);
	Route::get('/divisionList', [FndSettingsController::class, 'divisionList']);
	Route::get('/addDivision', [FndSettingsController::class, 'addDivision']);
	Route::post('/saveDivision', [FndSettingsController::class, 'saveDivisionData']);
	Route::get('/editDivision/{id}', [FndSettingsController::class, 'editDivision']);
	Route::post('/updateDivision', [FndSettingsController::class, 'updateDivisionData']);
	Route::get('/deleteDivision/{id}', [FndSettingsController::class, 'deleteDivisionData']);
	Route::get('/districtList', [FndSettingsController::class, 'districtList']);
	Route::get('/addDistrict', [FndSettingsController::class, 'addDistrict']);
	Route::post('/saveDistrict', [FndSettingsController::class, 'saveDistrictData']);
	Route::get('/editDistrict/{id}', [FndSettingsController::class, 'editDistrict']);
	Route::post('/updateDistrict', [FndSettingsController::class, 'updateDistrictData']);
	Route::get('/deleteDistrict/{id}', [FndSettingsController::class, 'deleteDistrictData']);
	Route::get('/blockList', [FndSettingsController::class, 'blockList']);
	Route::get('/addBlock', [FndSettingsController::class, 'addBlock']);
	Route::post('/saveBlock', [FndSettingsController::class, 'saveBlockData']);
	Route::get('/editBlock/{id}', [FndSettingsController::class, 'editBlock']);
	Route::post('/updateBlock', [FndSettingsController::class, 'updateBlockData']);
	Route::get('/deleteBlock/{id}', [FndSettingsController::class, 'deleteBlockData']);
	Route::get('getdistrict/{id}', [FndSettingsController::class, 'getdistrict']);
	Route::get('/addEditFndsettings', [FndSettingsController::class, 'addEditFndsettings']);
	Route::post('/saveFndsettings', [FndSettingsController::class, 'saveFndsettings']);
	Route::get('payment-list', [PaymentController::class, 'listPayment']);
	Route::post('/payment-list', [PaymentController::class, 'listPayment']); // for payment advance search and on excel export
	Route::get('block/block_list/{id}', [ReportController::class, 'blockList']);
	Route::get('district/district_list/{id}', [ReportController::class, 'districtList']);
	// Route::post('/advUserDetails',[DistrictController::class,'advUserDetails']);
	// Route::get('dashboard',[application_formController::class,'feedbackDashboard']);
	Route::get('student-application', [StudentRegisterController::class, 'getStudentDetails']);
	Route::post('addEdit/studentApplicationData', [StudentRegisterController::class, 'addEditStudentData']);
	Route::get('download/pdf/{id}', [application_formController::class, 'downloadStudentPDF']);
	Route::post('application-submit/payment', [application_formController::class, 'applicationSubmitPayment']);
	Route::post('application-payment-form', [application_formController::class, 'ApplicationPaymentForm']);                       
	Route::post('application-challan-form', [application_formController::class, 'ApplicationChallanForm']);                       
	Route::post('/application-challan', [application_formController::class, 'challanPreview']);
	/* ===== Conversation and form extenstion updation routes ===== */
	Route::post('update-application-file', [application_formController::class, 'UpdateApplicationFile']);
	Route::post('remove-application-file', [application_formController::class, 'RemoveApplicationFile']);
	Route::post('insert-application-file', [application_formController::class, 'InsertApplicationFile']);
	Route::get('update-message-status/{application_id}', [application_formController::class, 'UpdateMessageStatus']);
	Route::get('update-appeal-status/{application_id}', [AppealController::class, 'UpdateAppealStatus']);

	// Route::get('/About-us', [IndexPageController::class, 'ViewAboutUs']);
	Route::post('/Update-About-Us', [IndexPageController::class, 'UpdateAboutUs']);
	Route::get('/FAQ', [IndexPageController::class, 'ViewFAQs']);
	Route::get('/editFAQ/{id}', [IndexPageController::class, 'EditFAQs']);
	Route::post('/Update-FAQ', [IndexPageController::class, 'UpdateFAQ']);
	Route::get('/Add-FAQ', [IndexPageController::class, 'AddFAQ']);
	Route::post('/Insert-FAQ', [IndexPageController::class, 'InsertFAQ']);
	Route::get('/delete-FAQ/{id}', [IndexPageController::class, 'DeleteFAQ']);
	Route::get('/DSE-List', [IndexPageController::class, 'DSEList']);
	Route::get('/DEO', [IndexPageController::class, 'DEOList']);
	Route::get('/editDSE/{id}', [IndexPageController::class, 'Edit_DSE']);
	Route::get('/Add-DSE', [IndexPageController::class, 'Add_DSE']);
	Route::post('/Insert-DSE', [IndexPageController::class, 'Insert_DSE']);
	Route::post('/Update-DSE', [IndexPageController::class, 'Update_DSE']);
	Route::get('/editDEO/{id}', [IndexPageController::class, 'Edit_DEO']);
	Route::get('/Add-DEO', [IndexPageController::class, 'Add_DEO']);
	Route::post('/Insert-DEO', [IndexPageController::class, 'Insert_DEO']);
	Route::post('/Update-DEO', [IndexPageController::class, 'Update_DEO']);
	Route::get('/delete-DEO/{id}', [IndexPageController::class, 'DeleteDEO']);
	Route::get('/delete-DSE/{id}', [IndexPageController::class, 'DeleteDSE']);
	Route::get('/Contact_us', [IndexPageController::class, 'ContactUs']);
	Route::post('/Update-Contact-Us', [IndexPageController::class, 'UpdateContactUs']);
	Route::get('/delete-teacher/{teacher_id}', [application_formController::class, 'DeleteTeacher']);


	// For Guide line of user created at 03-03-2021 by Anshuman
	Route::get('/GuideLine', [IndexPageController::class, 'GuideLineList']);
	Route::get('/editGuideLine/{id}', [IndexPageController::class, 'EditGuideLine']);
	Route::get('/Add-GuideLine', [IndexPageController::class, 'AddGuideLine']);
	Route::post('/Insert-GuideLine', [IndexPageController::class, 'InsertGuideLine']);
	Route::post('/Update-GuideLine', [IndexPageController::class, 'UpdateGuideLine']);
	Route::get('/delete-GuideLine/{id}', [IndexPageController::class, 'DeleteGuideLine']);

	// For Slider Page  line of user created at 09-03-2021 by Anshuman
	Route::get('/Slider-List', [IndexPageController::class, 'SliderList']);
	Route::get('/editSlider/{id}', [IndexPageController::class, 'EditSlider']);
	Route::get('/Add-Slider', [IndexPageController::class, 'AddSlider']);
	Route::post('/Insert-Slider', [IndexPageController::class, 'InsertSlider']);
	Route::post('/Update-Slider', [IndexPageController::class, 'UpdateSlider']);
	Route::get('/delete-Slider/{id}', [IndexPageController::class, 'DeleteSlider']);

	// For Honble Person Page  line of user created at 09-03-2021 by Anshuman
	Route::get('/Honble-Person', [IndexPageController::class, 'HonblePersonList']);
	Route::get('/editHonble-Person/{id}', [IndexPageController::class, 'EditHonblePerson']);
	Route::get('/Add-Honble-Person', [IndexPageController::class, 'AddHonblePerson']);
	Route::post('/Insert-Honble-Person', [IndexPageController::class, 'InsertHonblePerson']);
	Route::post('/Update-Honble-Person', [IndexPageController::class, 'UpdateHonblePerson']);
	Route::get('/delete-Honble-Person/{id}', [IndexPageController::class, 'DeleteHonblePerson']);
	
	
	Route::get('/view-ftp', [RteFtpController::class, 'view']);
	Route::get('/view-ftp-data-list/{id}', [RteFtpController::class, 'viewFTPDataList']);
	Route::post('download-ftp-data-list', [RteFtpController::class, 'download_file']);
	Route::post('upload-ftp-data-list', [RteFtpController::class, 'upload_file_in_ftp']);
	Route::get('view-ftp-data/{id}', [RteFtpController::class, 'view_file_in_ftp']);


	//Group chats 
	Route::get('/chat', [ReportController::class, 'chat']);
	

	});


	
