<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Slider;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->delete();
        
        DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => '1631788847.diff1.png',
                'title' => 'First Banner',
                'text' => 'This is first banner for testing',
                
               
            ),
            1 => 
            array (
                'id' => 2,
                'image' => '1615294390.3.jpg',
                'title' => 'Second Banner',
                'text' => 'This is second banner for testing',
            ),
        ));
    }
}
