<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFndValueSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_value_sets', function (Blueprint $table) {
             $table->id();
             $table->string('value_set_name')->nullable();
             $table->integer('parent_value_set_id')->nullable();
             $table->integer('parent_value_id')->nullable();
             $table->text('description')->nullable();
             $table->tinyInteger('validation_type')->default(0);
             $table->integer('default_value_id')->nullable();
             $table->integer('format_type')->nullable();
             $table->integer('maximum_size')->nullable();
             $table->tinyInteger('number_precision')->default(0);
             $table->tinyInteger('uppercase_only_flag')->default(0);
             $table->integer('minimum_value')->nullable();
             $table->integer('maximum_value')->nullable();
             $table->string('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fnd_value_sets');
    }
}
