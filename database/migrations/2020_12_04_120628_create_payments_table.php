<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
             $table->id();
             $table->integer('t_id')->nullable();
             $table->string('payment_title')->nullable();
             $table->text('payment_description')->nullable();
             $table->string('payment_user')->nullable();
             $table->text('payment_rows')->nullable();
             $table->float('payment_amount', 8, 2)->nullable();
             $table->tinyInteger('payment_status')->nullable();
             $table->dateTime('payment_date')->nullable();
             $table->text('payment_success_details')->nullable();
             $table->string('paid_method')->nullable();
             $table->integer('school_id')->nullable();
             $table->string('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
