<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationTeacherDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_teacher_datas', function (Blueprint $table) {
            $table->id();
            $table->integer('application_data_id')->nullable();
            $table->string('teacher_name')->nullable();
            $table->string('teacher_f_h_w_name')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('education_qualification')->nullable();
            $table->string('trainee_qualification')->nullable();
            $table->string('teaching_experience')->nullable();
            $table->string('cls_handed_over')->nullable();
            $table->dateTime('date_of_appointment')->nullable();
            $table->string('trained_or_untrained')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_teacher_datas');
    }
}
