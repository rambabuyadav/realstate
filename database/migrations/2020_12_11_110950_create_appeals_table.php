<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appeals', function (Blueprint $table) {
             $table->id();
             $table->integer('school_id')->nullable();
             $table->tinyInteger('appeal_status')->nullable();
             $table->string('appeal_by')->nullable();
             $table->string('appeal_to')->nullable();
             $table->dateTime('appeal_date')->nullable();
             $table->bigInteger('appeal_number')->nullable();
             $table->text('appeal_text')->nullable();
             $table->tinyInteger('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appeals');
    }
}
