<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
             $table->id();
             $table->integer('application_id')->nullable();
             $table->string('schoo_id')->nullable();
             $table->tinyInteger('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
