<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRteApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rte_applications', function (Blueprint $table) {
             $table->id();
             $table->integer('apllication_id')->nullable();
             $table->integer('application_no')->nullable();
             $table->dateTime('applied_at')->nullable();
             $table->string('applied_by')->nullable();
             $table->tinyInteger('application_status')->nullable();
             $table->tinyInteger('is_verified_deo')->nullable();
             $table->tinyInteger('is_verified_dc')->nullable();
             $table->tinyInteger('status')->nullable();
             $table->string('boundary_wall')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rte_applications');
    }
}
