<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_details', function (Blueprint $table) {
             $table->id();
             $table->integer('user_id')->nullable();
             $table->string('salutation')->nullable();
             $table->string('first_name')->nullable();
             $table->string('middle_name')->nullable();
             $table->string('last_name')->nullable();
             $table->string('user_address')->nullable();
             $table->string('designation')->nullable();
             $table->char('gender')->nullable();
             $table->dateTime('DOB')->nullable();
             $table->string('user_type')->nullable();
             $table->string('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_details');
    }
}
