<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationDataExt1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_data_ext_1s', function (Blueprint $table) {
             $table->id();
             $table->integer('application_id')->nullable();
             $table->integer('application_form_id')->nullable();
             $table->string('class')->nullable();
             $table->integer('number')->nullable();
             $table->float('Avg_length_breadth', 8, 2)->nullable();
             $table->string('boundary_wall')->nullable();
             $table->text('teaching_reading_content')->nullable();
             $table->text('sports_hobbies')->nullable();
             $table->string('books')->nullable();
             $table->string('magazines')->nullable();
             $table->string('type_of_water_facilities')->nullable();
             $table->string('no_of_water_supply')->nullable();
             $table->string('fire_fighting')->nullable();
             $table->integer('lightning_driver_status')->nullable();
             $table->integer('wc_and_urinal_variety')->nullable();
             $table->integer('gents_toilet')->nullable();
             $table->integer('ladies_toilet')->nullable();
             $table->string('teacher_name')->nullable();
             $table->string('post_name')->nullable();
             $table->string('father_husband_wife_name')->nullable();
             $table->dateTime('dob_2')->nullable();
             $table->string('educ_qualification')->nullable();
             $table->string('administrative_qualification')->nullable();
             $table->string('tet')->nullable();
             $table->string('experience')->nullable();
             $table->string('class_assigned')->nullable();
             $table->dateTime('appointment_date')->nullable();
             $table->string('syllabus')->nullable();
             $table->string('inspection_mode')->nullable();
             $table->string('board_in_6_or_before')->nullable();
             $table->string('facilities_access_without_interrupted')->nullable();
             $table->string('all_teaching_material_list')->nullable();
             $table->string('all_sports_equipment_list')->nullable();
             $table->string('type_of_toilet')->nullable();
             $table->string('principle_name')->nullable();
             $table->string('p_f_h_w_name')->nullable();
             $table->dateTime('p_date_of_birth')->nullable();
             $table->string('p_education_qualification')->nullable();
             $table->string('trainee_qualification')->nullable();
             $table->string('teaching_experience')->nullable();
             $table->string('class_handed_over')->nullable();
             $table->dateTime('date_of_appointment')->nullable();
             $table->string('trained_untrained')->nullable();
             $table->string('details_of_curriculum')->nullable();
             $table->string('method_of_inspection')->nullable();
             $table->string('school_board_exam_till_cls_eight')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_data_ext_1s');
    }
}
