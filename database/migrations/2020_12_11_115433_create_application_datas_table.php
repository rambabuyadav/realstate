<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_datas', function (Blueprint $table) {
             $table->id();
             $table->integer('application_id')->nullable();
             $table->string('school_name')->nullable();
             $table->string('udise_code')->nullable();
             $table->string('from_class')->nullable();
             $table->string('upto_class')->nullable();
             $table->string('recognised_by')->nullable();
             $table->string('district')->nullable();
             $table->string('post_office')->nullable();
             $table->string('village_city')->nullable();
             $table->integer('pincode')->nullable();
             $table->string('tehsil')->nullable();
             $table->string('phone_with_std_code')->nullable();
             $table->string('fax_with_std_code')->nullable();
             $table->string('email')->unique()->nullable();
             $table->string('police_station')->nullable();
             $table->integer('challan_number_1_5')->nullable();
             $table->integer('challan_number_1_8')->nullable();
             $table->integer('estd_year')->nullable();
             $table->dateTime('opening_date')->nullable();
             $table->string('society_name')->nullable();
             $table->char('is_society_registered')->nullable();
             $table->dateTime('society_registration_valid_upto')->nullable();
             $table->string('society_elected_by_whom')->nullable();
             $table->dateTime('election_date')->nullable();
             $table->string('school_chairman_name')->nullable();
             $table->string('school_chairman_post')->nullable();
             $table->string('school_chairman_address')->nullable();
             $table->string('school_chairman_phone_number')->nullable();
             $table->string('school_chairman_office')->nullable();
             $table->string('school_chairman_email')->nullable()->unique();
             $table->string('session_year')->nullable();  
             $table->float('income', 8, 2)->nullable();
             $table->float('expenses', 8, 2)->nullable();
             $table->float('surplus_money', 8, 2)->nullable();
             $table->float('reduced_money', 8, 2)->nullable();
             $table->string('class')->nullable();
             $table->float('teaching_fee', 8, 2)->nullable();
             $table->float('registration_fee', 8, 2)->nullable();
             $table->float('enrollment_fee', 8, 2)->nullable();
             $table->float('security_money', 8, 2)->nullable();
             $table->float('annual_charge', 8, 2)->nullable();
             $table->float('development_fee', 8, 2)->nullable();
             $table->float('other_charges', 8, 2)->nullable();
             $table->double('total', 8, 2)->nullable();
             $table->string('medium')->nullable();
             $table->string('type_of_school')->nullable();
             $table->string('supported_agency_name')->nullable();
             $table->integer('agency_supported_percent')->nullable();
             $table->char('is_school_recognised')->nullable();
             $table->string('authority_name')->nullable();
             $table->integer('recognised_number')->nullable();
             $table->char('is_school_on_rented')->nullable();
             $table->float('rakwa_in_acre',8,2)->nullable();
             $table->bigInteger('khata_number')->nullable();
             $table->integer('plot_number')->nullable();
             $table->string('school_building_area')->nullable();
             $table->string('pre_elementary_class')->nullable();
             $table->string('pre_no_of_section')->nullable();
             $table->integer('pre_no_of_student')->nullable();
             $table->string('class_one_to_five')->nullable();
             $table->string('onefive_no_of_section')->nullable();
             $table->string('onefive_no_of_student')->nullable();
             $table->string('class_six_to_eight')->nullable();
             $table->string('sixeight_no_of_section')->nullable();
             $table->string('sixeight_no_of_student')->nullable();
             $table->string('no_of_class')->nullable();
             $table->string('avg_size_cls_room')->nullable();
             $table->string('no_of_office_room')->nullable();
             $table->string('avg_size_of_office_room')->nullable();
             $table->string('no_of_store_room')->nullable();
             $table->string('avg_size_of_store_room')->nullable();
             $table->string('no_of_princpal_room')->nullable();
             $table->string('avg_size_of_principal_room')->nullable();
             $table->string('no_of_kitchen_room')->nullable();
             $table->string('avg_size_of_kitchen_room')->nullable();
             $table->tinyInteger('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_datas');
    }
}
