<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrivateSchoolsTable extends Migration
{
    /**
     * Run the migrations.

     * @return void
     */
    public function up()
    {
        Schema::create('private_schools', function (Blueprint $table) {
             $table->id();
             $table->string('school_name')->nullable();
             $table->string('short_name')->nullable();
             $table->string('school_code')->nullable();
             $table->string('udise_code')->nullable();
             $table->string('udisecode_name')->nullable();
             $table->string('board')->nullable();
             $table->string('school_logo')->nullable();
             $table->string('website')->nullable();
             $table->string('school_type')->nullable();
             $table->string('school_email')->nullable();
             $table->string('contact_person')->nullable();
             $table->string('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('private_schools');
    }
}
