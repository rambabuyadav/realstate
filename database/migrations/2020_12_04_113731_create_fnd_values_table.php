<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFndValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_values', function (Blueprint $table) {
             $table->id();
             $table->integer('value_set_id')->nullable();
             $table->integer('parent_value_id')->nullable();
             $table->string('display_value')->nullable();
             $table->string('display_short_value')->nullable();
             $table->tinyInteger('enabled_flag')->default(1);
             $table->date('enabled_date')->nullable();
             $table->date('disabled_date')->nullable();
             $table->string('sort_order')->nullable();
             $table->string('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fnd_values');
    }
}
