<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('username')->nullable();
            $table->string('user_address')->nullable();
            $table->string('school_id')->nullable();
            $table->string('email')->unique();
            $table->integer('agree-term')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('contact')->nullable();
            $table->string('mobile_otp')->nullable();
            $table->string('email_otp')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->string('user_role')->default('school');;
            $table->string('status')->default(0);
            $table->string('created_by')->nullable();
            $table->string('created_ip')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('updated_ip')->nullable();
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
