<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
             $table->id();
             $table->integer('user_id')->nullable();
             $table->integer('school_id')->nullable();
             $table->string('address_type')->nullable();
             $table->string('school_address')->nullable();
             $table->string('address2')->nullable();
             $table->string('village')->nullable();
             $table->string('panchayat')->nullable();
             $table->string('block')->nullable();
             $table->string('po')->nullable();
             $table->string('ps')->nullable();
             $table->string('cluster')->nullable();
             $table->string('school_city')->nullable();
             $table->string('district')->nullable();
             $table->string('school_state')->nullable();
             $table->tinyInteger('status')->nullable();
             $table->string('created_by')->nullable();
             $table->string('created_ip')->nullable();
             $table->string('updated_by')->nullable();
             $table->string('updated_ip')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
