<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequiredSecondDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('required_second_details', function (Blueprint $table) {
             $table->id();
             $table->integer('apllication_id')->nullable();
             $table->string('class')->nullable();
             $table->string('number')->nullable();
             $table->float('avg_length_breadth',8,2)->nullable();
             $table->string('boundary_wall')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('required_second_details');
    }
}
